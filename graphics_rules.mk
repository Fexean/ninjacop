BUILD_GFX_DIR = build/gfx

# Prevents gbapal files from being deleted
.SECONDARY:


####	GENERAL GRAPHICS RULES	####
$(BUILD_GFX_DIR)/%.4bpp: gfx/%.png;		$(GBAGFX) $< $@
$(BUILD_GFX_DIR)/%.8bpp: gfx/%.png;		$(GBAGFX) $< $@
$(BUILD_GFX_DIR)/%.gbapal: gfx/%.pal;	$(GBAGFX) $< $@
$(BUILD_GFX_DIR)/%.gbapal: gfx/%.png;	$(GBAGFX) $< $@





####	 LEVEL TILESETS		####

LVL_TILESET_BUILD_DIR = $(BUILD_GFX_DIR)/level_tilesets
LVL_TILESET_DIR = gfx/level_tilesets
$(shell mkdir -p $(LVL_TILESET_BUILD_DIR))

$(LVL_TILESET_BUILD_DIR)/airport_stage1.4bpp.lz: $(LVL_TILESET_BUILD_DIR)/airport_stage1.4bpp;	$(GBAGFX) $< $@ -overflow 10 
$(LVL_TILESET_BUILD_DIR)/airport_stage2.4bpp.lz: $(LVL_TILESET_BUILD_DIR)/airport_stage2.4bpp;	$(GBAGFX) $< $@ -overflow 11 
$(LVL_TILESET_BUILD_DIR)/airport_stage3.4bpp.lz: $(LVL_TILESET_BUILD_DIR)/airport_stage3.4bpp;	$(GBAGFX) $< $@ -overflow 10 
$(LVL_TILESET_BUILD_DIR)/airport_stage4.4bpp.lz: $(LVL_TILESET_BUILD_DIR)/airport_stage4.4bpp;	$(GBAGFX) $< $@ -overflow 15 
$(LVL_TILESET_BUILD_DIR)/harbor_stage1.4bpp.lz: $(LVL_TILESET_BUILD_DIR)/harbor_stage1.4bpp;	$(GBAGFX) $< $@ -overflow 10 
$(LVL_TILESET_BUILD_DIR)/harbor_stage2.4bpp.lz: $(LVL_TILESET_BUILD_DIR)/harbor_stage2.4bpp;	$(GBAGFX) $< $@ -overflow 11 
$(LVL_TILESET_BUILD_DIR)/harbor_stage3.4bpp.lz: $(LVL_TILESET_BUILD_DIR)/harbor_stage3.4bpp;	$(GBAGFX) $< $@ -overflow 10 
$(LVL_TILESET_BUILD_DIR)/harbor_stage4.4bpp.lz: $(LVL_TILESET_BUILD_DIR)/harbor_stage4.4bpp;	$(GBAGFX) $< $@ -overflow 12 
$(LVL_TILESET_BUILD_DIR)/bank_stage1.4bpp.lz: $(LVL_TILESET_BUILD_DIR)/bank_stage1.4bpp;	$(GBAGFX) $< $@ -overflow 10 
$(LVL_TILESET_BUILD_DIR)/bank_stage2.4bpp.lz: $(LVL_TILESET_BUILD_DIR)/bank_stage2.4bpp;	$(GBAGFX) $< $@ -overflow 11 
$(LVL_TILESET_BUILD_DIR)/bank_stage3.4bpp.lz: $(LVL_TILESET_BUILD_DIR)/bank_stage3.4bpp;	$(GBAGFX) $< $@ -overflow 10 
$(LVL_TILESET_BUILD_DIR)/bank_stage4.4bpp.lz: $(LVL_TILESET_BUILD_DIR)/bank_stage4.4bpp;	$(GBAGFX) $< $@ -overflow 10 
$(LVL_TILESET_BUILD_DIR)/cave.4bpp.lz: $(LVL_TILESET_BUILD_DIR)/cave.4bpp;	$(GBAGFX) $< $@ -overflow 2 
$(LVL_TILESET_BUILD_DIR)/base_stage1.4bpp.lz: $(LVL_TILESET_BUILD_DIR)/base_stage1.4bpp;	$(GBAGFX) $< $@ -overflow 14 
$(LVL_TILESET_BUILD_DIR)/base_stage2.4bpp.lz: $(LVL_TILESET_BUILD_DIR)/base_stage2.4bpp;	$(GBAGFX) $< $@ -overflow 14 
$(LVL_TILESET_BUILD_DIR)/base_stage3.4bpp.lz: $(LVL_TILESET_BUILD_DIR)/base_stage3.4bpp;	$(GBAGFX) $< $@ -overflow 16 
$(LVL_TILESET_BUILD_DIR)/launch.4bpp.lz: $(LVL_TILESET_BUILD_DIR)/launch.4bpp;	$(GBAGFX) $< $@ -overflow 11 

$(LVL_TILESET_BUILD_DIR)/harbor_turbine.4bpp: $(LVL_TILESET_DIR)/anim/harbor_turbine.png;	$(GBAGFX) $< $@
$(LVL_TILESET_BUILD_DIR)/conveyor.4bpp: $(LVL_TILESET_DIR)/anim/conveyor.png;	$(GBAGFX) $< $@



####	TITLE SCREEN GRAPHICS	####
TITLE_GFX_DIR = gfx/title
TITLE_GFX_BUILD_DIR = $(BUILD_GFX_DIR)/title
$(shell mkdir -p $(TITLE_GFX_BUILD_DIR))

$(TITLE_GFX_BUILD_DIR)/press_start_sprite_tiles.4bpp.lz: $(TITLE_GFX_BUILD_DIR)/press_start_sprite_tiles.4bpp
	$(GBAGFX) $< $@ -overflow 4




####	FONTS	####
FONT_GFX_DIR = gfx/fonts
FONT_GFX_BUILD_DIR = $(BUILD_GFX_DIR)/fonts
$(shell mkdir -p $(FONT_GFX_BUILD_DIR))

$(FONT_GFX_BUILD_DIR)/font0.4bpp.lz: $(FONT_GFX_BUILD_DIR)/font0.4bpp;	$(GBAGFX) $< $@ -overflow 16
$(FONT_GFX_BUILD_DIR)/font1.4bpp.lz: $(FONT_GFX_BUILD_DIR)/font1.4bpp;	$(GBAGFX) $< $@ -overflow 8
$(FONT_GFX_BUILD_DIR)/font2.4bpp.lz: $(FONT_GFX_BUILD_DIR)/font2.4bpp;	$(GBAGFX) $< $@ -overflow 5
$(FONT_GFX_BUILD_DIR)/font3.4bpp.lz: $(FONT_GFX_BUILD_DIR)/font3.4bpp;	$(GBAGFX) $< $@ -overflow 8
$(FONT_GFX_BUILD_DIR)/numbers0.4bpp.lz: $(FONT_GFX_BUILD_DIR)/numbers0.4bpp;	$(GBAGFX) $< $@ -overflow 1
$(FONT_GFX_BUILD_DIR)/buttons.4bpp.lz: $(FONT_GFX_BUILD_DIR)/buttons.4bpp;	$(GBAGFX) $< $@ -overflow 12




####	MENU GRAPHICS	####
MENU_GFX_DIR = gfx/menu
MENU_GFX_BUILD_DIR = $(BUILD_GFX_DIR)/menu
$(shell mkdir -p $(MENU_GFX_BUILD_DIR))

$(MENU_GFX_BUILD_DIR)/scroll_cursor_sprite.4bpp.lz: $(MENU_GFX_BUILD_DIR)/scroll_cursor_sprite.4bpp;	$(GBAGFX) $< $@ -overflow 16
$(MENU_GFX_BUILD_DIR)/scroll_cursor_sprite.4bpp: $(MENU_GFX_DIR)/scroll_cursor_sprite.png;	$(GBAGFX) $< $@ -num_tiles 218
$(MENU_GFX_BUILD_DIR)/menu_bg.4bpp.lz: $(MENU_GFX_BUILD_DIR)/menu_bg.4bpp;	$(GBAGFX) $< $@
$(MENU_GFX_BUILD_DIR)/ninja_bg.4bpp.lz: $(MENU_GFX_BUILD_DIR)/ninja_bg.4bpp;	$(GBAGFX) $< $@ -overflow 6
$(MENU_GFX_BUILD_DIR)/level_select_icons.4bpp.lz: $(MENU_GFX_BUILD_DIR)/level_select_icons.4bpp;	$(GBAGFX) $< $@ -overflow 4
$(MENU_GFX_BUILD_DIR)/level_select_sprites.4bpp.lz: $(MENU_GFX_BUILD_DIR)/level_select_sprites.4bpp;	$(GBAGFX) $< $@ -overflow 14
$(MENU_GFX_BUILD_DIR)/time_attack_cursor_sprite.4bpp.lz: $(MENU_GFX_BUILD_DIR)/time_attack_cursor_sprite.4bpp;	$(GBAGFX) $< $@ -overflow 7

$(MENU_GFX_BUILD_DIR)/menubg.tilemap: $(MENU_GFX_DIR)/menubg_map.bin;	$(GBAGFX) $< $@ -w 30
$(MENU_GFX_BUILD_DIR)/menubg.tilemap.lz: $(MENU_GFX_BUILD_DIR)/menubg.tilemap;	$(GBAGFX) $< $@ -overflow 1

$(MENU_GFX_BUILD_DIR)/ninja.tilemap: $(MENU_GFX_DIR)/ninja_map.bin;	$(GBAGFX) $< $@ -w 30
$(MENU_GFX_BUILD_DIR)/ninja.tilemap.lz: $(MENU_GFX_BUILD_DIR)/ninja.tilemap;	$(GBAGFX) $< $@ -overflow 10

$(MENU_GFX_BUILD_DIR)/level_select_icons.tilemap.lz: $(MENU_GFX_BUILD_DIR)/level_select_icons.tilemap;	$(GBAGFX) $< $@ -overflow 9
$(MENU_GFX_BUILD_DIR)/level_select_icons.tilemap: $(MENU_GFX_DIR)/level_select_icons.bin;	$(GBAGFX) $< $@ -w 30 -h 20

$(MENU_GFX_BUILD_DIR)/continue_lines.4bpp.lz: $(MENU_GFX_BUILD_DIR)/continue_lines.4bpp;	$(GBAGFX) $< $@
$(MENU_GFX_BUILD_DIR)/continue_lines.tilemap: $(MENU_GFX_DIR)/continue_lines.bin;	$(GBAGFX) $< $@ -w 30 -h 20
$(MENU_GFX_BUILD_DIR)/continue_lines.tilemap.lz: $(MENU_GFX_BUILD_DIR)/continue_lines.tilemap;	$(GBAGFX) $< $@ -overflow 12


####	LOGOS	####
LOGO_GFX_DIR = gfx/logos
LOGO_GFX_BUILD_DIR = $(BUILD_GFX_DIR)/logos
$(shell mkdir -p $(LOGO_GFX_BUILD_DIR))

# TODO: Fix, currently this does not produce a matching palette file
$(LOGO_GFX_BUILD_DIR)/hudson.gbapal:	$(LOGO_GFX_DIR)/hudson.png;	$(GBAGFX) $< $@ -num_colors 253

$(LOGO_GFX_BUILD_DIR)/konami.tilemap:	$(LOGO_GFX_DIR)/konami.bin;	$(GBAGFX) $< $@ -w 32 -h 32
$(LOGO_GFX_BUILD_DIR)/konami.tilemap.lz:	$(LOGO_GFX_BUILD_DIR)/konami.tilemap;	$(GBAGFX) $< $@
$(LOGO_GFX_BUILD_DIR)/konami.4bpp.lz:	$(LOGO_GFX_BUILD_DIR)/konami.4bpp;	$(GBAGFX) $< $@ -overflow 6



####	NINJUTSU	####
NINJUTSU_GFX_DIR = gfx/ninjutsu
NINJUTSU_GFX_BUILD_DIR = $(BUILD_GFX_DIR)/ninjutsu
$(shell mkdir -p $(NINJUTSU_GFX_BUILD_DIR))

$(NINJUTSU_GFX_BUILD_DIR)/ninjutsu.tilemap:	$(NINJUTSU_GFX_DIR)/ninjutsu_map.bin;	$(GBAGFX) $< $@ -w 32 -h 32
$(NINJUTSU_GFX_BUILD_DIR)/ninjutsu.tilemap.lz:	$(NINJUTSU_GFX_BUILD_DIR)/ninjutsu.tilemap;	$(GBAGFX) $< $@
$(NINJUTSU_GFX_BUILD_DIR)/ninjutsu.4bpp.lz:	$(NINJUTSU_GFX_BUILD_DIR)/ninjutsu.4bpp;	$(GBAGFX) $< $@




###	HUD	###
HUD_GFX_DIR = gfx/hud
HUD_GFX_BUILD_DIR = $(BUILD_GFX_DIR)/hud
$(shell mkdir -p $(HUD_GFX_BUILD_DIR))

$(HUD_GFX_BUILD_DIR)/hud.4bpp.lz:	$(HUD_GFX_BUILD_DIR)/hud.4bpp;	$(GBAGFX) $< $@ -overflow 5
$(HUD_GFX_BUILD_DIR)/door_symbols.4bpp.lz:	$(HUD_GFX_BUILD_DIR)/door_symbols.4bpp;	$(GBAGFX) $< $@
$(HUD_GFX_BUILD_DIR)/health_charge_bars.4bpp.lz:	$(HUD_GFX_BUILD_DIR)/health_charge_bars.4bpp;	$(GBAGFX) $< $@ -overflow 14


####	LEVEL INTRO		####
LVLINTRO_GFX_DIR = gfx/level_intro
LVLINTRO_GFX_BUILD_DIR = $(BUILD_GFX_DIR)/level_intro
$(shell mkdir -p $(LVLINTRO_GFX_BUILD_DIR))

$(LVLINTRO_GFX_BUILD_DIR)/scroll.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/scroll.tilemap;	$(GBAGFX) $< $@ -overflow 16
$(LVLINTRO_GFX_BUILD_DIR)/scroll.tilemap: $(LVLINTRO_GFX_DIR)/scroll.bin;	$(GBAGFX) $< $@
$(LVLINTRO_GFX_BUILD_DIR)/scroll.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/scroll.4bpp;	$(GBAGFX) $< $@
$(LVLINTRO_GFX_BUILD_DIR)/slice_anim.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/slice_anim.4bpp;	$(GBAGFX) $< $@
$(LVLINTRO_GFX_BUILD_DIR)/scroll2.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/scroll2.4bpp;	$(GBAGFX) $< $@ -overflow 2
$(LVLINTRO_GFX_BUILD_DIR)/mission_complete.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/mission_complete.4bpp; $(GBAGFX) $< $@
$(LVLINTRO_GFX_BUILD_DIR)/mission_complete_fr.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/mission_complete_fr.4bpp; $(GBAGFX) $< $@ -overflow 10
$(LVLINTRO_GFX_BUILD_DIR)/mission_complete_de.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/mission_complete_de.4bpp; $(GBAGFX) $< $@ -overflow 10

$(LVLINTRO_GFX_BUILD_DIR)/%.4bpp: $(LVLINTRO_GFX_DIR)/*/%.png; $(GBAGFX) $< $@
$(LVLINTRO_GFX_BUILD_DIR)/%.gbapal: $(LVLINTRO_GFX_DIR)/*/%.pal; $(GBAGFX) $< $@
$(LVLINTRO_GFX_BUILD_DIR)/%.tilemap: $(LVLINTRO_GFX_DIR)/*/%.bin; $(GBAGFX) $< $@ -w 18 -h 12

$(LVLINTRO_GFX_BUILD_DIR)/bank1.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/bank1.4bpp;	$(GBAGFX) $< $@ -overflow 4
$(LVLINTRO_GFX_BUILD_DIR)/bank2.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/bank2.4bpp;	$(GBAGFX) $< $@ -overflow 6
$(LVLINTRO_GFX_BUILD_DIR)/bank3.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/bank3.4bpp;	$(GBAGFX) $< $@ -overflow 4
$(LVLINTRO_GFX_BUILD_DIR)/bank4.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/bank4.4bpp;	$(GBAGFX) $< $@ -overflow 2
$(LVLINTRO_GFX_BUILD_DIR)/bank1.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/bank1.tilemap;	$(GBAGFX) $< $@ -overflow 2
$(LVLINTRO_GFX_BUILD_DIR)/bank2.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/bank2.tilemap;	$(GBAGFX) $< $@
$(LVLINTRO_GFX_BUILD_DIR)/bank3.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/bank3.tilemap;	$(GBAGFX) $< $@
$(LVLINTRO_GFX_BUILD_DIR)/bank4.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/bank4.tilemap;	$(GBAGFX) $< $@

$(LVLINTRO_GFX_BUILD_DIR)/harbor1.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/harbor1.4bpp;	$(GBAGFX) $< $@ -overflow 10
$(LVLINTRO_GFX_BUILD_DIR)/harbor2.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/harbor2.4bpp;	$(GBAGFX) $< $@ -overflow 14
$(LVLINTRO_GFX_BUILD_DIR)/harbor3.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/harbor3.4bpp;	$(GBAGFX) $< $@ -overflow 8
$(LVLINTRO_GFX_BUILD_DIR)/harbor4.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/harbor4.4bpp;	$(GBAGFX) $< $@ -overflow 8
$(LVLINTRO_GFX_BUILD_DIR)/harbor1.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/harbor1.tilemap;	$(GBAGFX) $< $@ -overflow 3
$(LVLINTRO_GFX_BUILD_DIR)/harbor2.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/harbor2.tilemap;	$(GBAGFX) $< $@ -overflow 2
$(LVLINTRO_GFX_BUILD_DIR)/harbor3.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/harbor3.tilemap;	$(GBAGFX) $< $@ -overflow 1
$(LVLINTRO_GFX_BUILD_DIR)/harbor4.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/harbor4.tilemap;	$(GBAGFX) $< $@ -overflow 3

$(LVLINTRO_GFX_BUILD_DIR)/airport1_1.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/airport1_1.4bpp;	$(GBAGFX) $< $@ -overflow 12
$(LVLINTRO_GFX_BUILD_DIR)/airport1_2.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/airport1_2.4bpp;	$(GBAGFX) $< $@
$(LVLINTRO_GFX_BUILD_DIR)/airport1_3.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/airport1_3.4bpp;	$(GBAGFX) $< $@
$(LVLINTRO_GFX_BUILD_DIR)/airport1_4.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/airport1_4.4bpp;	$(GBAGFX) $< $@
$(LVLINTRO_GFX_BUILD_DIR)/airport1_1.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/airport1_1.tilemap;	$(GBAGFX) $< $@ -overflow 2
$(LVLINTRO_GFX_BUILD_DIR)/airport1_2.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/airport1_2.tilemap;	$(GBAGFX) $< $@ -overflow 2
$(LVLINTRO_GFX_BUILD_DIR)/airport1_3.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/airport1_3.tilemap;	$(GBAGFX) $< $@ -overflow 3
$(LVLINTRO_GFX_BUILD_DIR)/airport1_4.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/airport1_4.tilemap;	$(GBAGFX) $< $@ -overflow 1

$(LVLINTRO_GFX_BUILD_DIR)/airport2_1.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/airport2_1.4bpp;	$(GBAGFX) $< $@ -overflow 4
$(LVLINTRO_GFX_BUILD_DIR)/airport2_2.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/airport2_2.4bpp;	$(GBAGFX) $< $@
$(LVLINTRO_GFX_BUILD_DIR)/airport2_3.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/airport2_3.4bpp;	$(GBAGFX) $< $@ -overflow 12
$(LVLINTRO_GFX_BUILD_DIR)/airport2_4.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/airport2_4.4bpp;	$(GBAGFX) $< $@ -overflow 16
$(LVLINTRO_GFX_BUILD_DIR)/airport2_1.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/airport2_1.tilemap;	$(GBAGFX) $< $@ -overflow 5
$(LVLINTRO_GFX_BUILD_DIR)/airport2_2.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/airport2_2.tilemap;	$(GBAGFX) $< $@ -overflow 4
$(LVLINTRO_GFX_BUILD_DIR)/airport2_3.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/airport2_3.tilemap;	$(GBAGFX) $< $@ -overflow 2
$(LVLINTRO_GFX_BUILD_DIR)/airport2_4.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/airport2_4.tilemap;	$(GBAGFX) $< $@ -overflow 1

$(LVLINTRO_GFX_BUILD_DIR)/cave1.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/cave1.tilemap;	$(GBAGFX) $< $@ -overflow 4
$(LVLINTRO_GFX_BUILD_DIR)/cave2.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/cave2.tilemap;	$(GBAGFX) $< $@ -overflow 2
$(LVLINTRO_GFX_BUILD_DIR)/cave3.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/cave3.tilemap;	$(GBAGFX) $< $@ -overflow 3
$(LVLINTRO_GFX_BUILD_DIR)/cave4.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/cave4.tilemap;	$(GBAGFX) $< $@ -overflow 1
$(LVLINTRO_GFX_BUILD_DIR)/cave1.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/cave1.4bpp;	$(GBAGFX) $< $@ -overflow 8
$(LVLINTRO_GFX_BUILD_DIR)/cave2.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/cave2.4bpp;	$(GBAGFX) $< $@
$(LVLINTRO_GFX_BUILD_DIR)/cave3.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/cave3.4bpp;	$(GBAGFX) $< $@ -overflow 6
$(LVLINTRO_GFX_BUILD_DIR)/cave4.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/cave4.4bpp;	$(GBAGFX) $< $@ -overflow 14
$(LVLINTRO_GFX_BUILD_DIR)/cave.gbapal: $(LVLINTRO_GFX_DIR)/cave/cave.pal;	$(GBAGFX) $< $@

$(LVLINTRO_GFX_BUILD_DIR)/base1.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/base1.4bpp;	$(GBAGFX) $< $@ -overflow 6
$(LVLINTRO_GFX_BUILD_DIR)/base2.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/base2.4bpp;	$(GBAGFX) $< $@ -overflow 14
$(LVLINTRO_GFX_BUILD_DIR)/base3.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/base3.4bpp;	$(GBAGFX) $< $@ -overflow 12
$(LVLINTRO_GFX_BUILD_DIR)/base4.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/base4.4bpp;	$(GBAGFX) $< $@ -overflow 8
$(LVLINTRO_GFX_BUILD_DIR)/base1.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/base1.tilemap;	$(GBAGFX) $< $@ -overflow 4
$(LVLINTRO_GFX_BUILD_DIR)/base2.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/base2.tilemap;	$(GBAGFX) $< $@ -overflow 3
$(LVLINTRO_GFX_BUILD_DIR)/base3.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/base3.tilemap;	$(GBAGFX) $< $@ -overflow 3
$(LVLINTRO_GFX_BUILD_DIR)/base4.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/base4.tilemap;	$(GBAGFX) $< $@ -overflow 1

$(LVLINTRO_GFX_BUILD_DIR)/launch.gbapal: $(LVLINTRO_GFX_DIR)/launch/launch.pal;	$(GBAGFX) $< $@
$(LVLINTRO_GFX_BUILD_DIR)/launch1.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/launch1.4bpp;	$(GBAGFX) $< $@
$(LVLINTRO_GFX_BUILD_DIR)/launch2.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/launch2.4bpp;	$(GBAGFX) $< $@ -overflow 6
$(LVLINTRO_GFX_BUILD_DIR)/launch3.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/launch3.4bpp;	$(GBAGFX) $< $@ -overflow 10
$(LVLINTRO_GFX_BUILD_DIR)/launch4.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/launch4.4bpp;	$(GBAGFX) $< $@ -overflow 6
$(LVLINTRO_GFX_BUILD_DIR)/launch1.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/launch1.tilemap;	$(GBAGFX) $< $@ -overflow 4
$(LVLINTRO_GFX_BUILD_DIR)/launch2.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/launch2.tilemap;	$(GBAGFX) $< $@ -overflow 1
$(LVLINTRO_GFX_BUILD_DIR)/launch3.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/launch3.tilemap;	$(GBAGFX) $< $@ -overflow 2
$(LVLINTRO_GFX_BUILD_DIR)/launch4.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/launch4.tilemap;	$(GBAGFX) $< $@ -overflow 4
$(LVLINTRO_GFX_BUILD_DIR)/launch4_2.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/launch4_2.4bpp;	$(GBAGFX) $< $@ -overflow 6
$(LVLINTRO_GFX_BUILD_DIR)/launch4_2.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/launch4_2.tilemap;	$(GBAGFX) $< $@ -overflow 4

$(LVLINTRO_GFX_BUILD_DIR)/bank_completion.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/bank_completion.4bpp; $(GBAGFX) $< $@ -overflow 10
$(LVLINTRO_GFX_BUILD_DIR)/harbor_completion.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/harbor_completion.4bpp; $(GBAGFX) $< $@ -overflow 4
$(LVLINTRO_GFX_BUILD_DIR)/airport_completion.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/airport_completion.4bpp; $(GBAGFX) $< $@ -overflow 2
$(LVLINTRO_GFX_BUILD_DIR)/cave_completion.4bpp.lz: $(LVLINTRO_GFX_BUILD_DIR)/cave_completion.4bpp; $(GBAGFX) $< $@

$(LVLINTRO_GFX_BUILD_DIR)/bank_completion.tilemap: $(LVLINTRO_GFX_DIR)/bank_completion.bin; $(GBAGFX) $< $@ -w 30 -h 20
$(LVLINTRO_GFX_BUILD_DIR)/bank_completion.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/bank_completion.tilemap; $(GBAGFX) $< $@
$(LVLINTRO_GFX_BUILD_DIR)/harbor_completion.tilemap: $(LVLINTRO_GFX_DIR)/harbor_completion.bin; $(GBAGFX) $< $@ -w 30 -h 20
$(LVLINTRO_GFX_BUILD_DIR)/harbor_completion.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/harbor_completion.tilemap; $(GBAGFX) $< $@
$(LVLINTRO_GFX_BUILD_DIR)/airport_completion.tilemap: $(LVLINTRO_GFX_DIR)/airport_completion.bin; $(GBAGFX) $< $@ -w 30 -h 20
$(LVLINTRO_GFX_BUILD_DIR)/airport_completion.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/airport_completion.tilemap; $(GBAGFX) $< $@
$(LVLINTRO_GFX_BUILD_DIR)/cave_completion.tilemap: $(LVLINTRO_GFX_DIR)/cave_completion.bin; $(GBAGFX) $< $@ -w 30 -h 20
$(LVLINTRO_GFX_BUILD_DIR)/cave_completion.tilemap.lz: $(LVLINTRO_GFX_BUILD_DIR)/cave_completion.tilemap; $(GBAGFX) $< $@



####	INTRO	####
INTRO_GFX_DIR = gfx/intro
INTRO_GFX_BUILD_DIR = $(BUILD_GFX_DIR)/intro
$(shell mkdir -p $(INTRO_GFX_BUILD_DIR))

$(INTRO_GFX_BUILD_DIR)/city1.tilemap: $(INTRO_GFX_DIR)/city1.bin;	$(GBAGFX) $< $@
$(INTRO_GFX_BUILD_DIR)/city2.tilemap: $(INTRO_GFX_DIR)/city2.bin;	$(GBAGFX) $< $@
$(INTRO_GFX_BUILD_DIR)/city3.tilemap: $(INTRO_GFX_DIR)/city3.bin;	$(GBAGFX) $< $@

$(INTRO_GFX_BUILD_DIR)/city1.tilemap.lz: $(INTRO_GFX_BUILD_DIR)/city1.tilemap;	$(GBAGFX) $< $@
$(INTRO_GFX_BUILD_DIR)/city2.tilemap.lz: $(INTRO_GFX_BUILD_DIR)/city2.tilemap;	$(GBAGFX) $< $@
$(INTRO_GFX_BUILD_DIR)/city3.tilemap.lz: $(INTRO_GFX_BUILD_DIR)/city3.tilemap;	$(GBAGFX) $< $@

$(INTRO_GFX_BUILD_DIR)/n_letter.tilemap: $(INTRO_GFX_DIR)/n_letter.bin;	$(GBAGFX) $< $@ -w 30
$(INTRO_GFX_BUILD_DIR)/i_letter.tilemap: $(INTRO_GFX_DIR)/i_letter.bin;	$(GBAGFX) $< $@ -w 30
$(INTRO_GFX_BUILD_DIR)/j_letter.tilemap: $(INTRO_GFX_DIR)/j_letter.bin;	$(GBAGFX) $< $@ -w 30
$(INTRO_GFX_BUILD_DIR)/a_letter.tilemap: $(INTRO_GFX_DIR)/a_letter.bin;	$(GBAGFX) $< $@ -w 30

$(INTRO_GFX_BUILD_DIR)/city1.4bpp.lz: $(INTRO_GFX_BUILD_DIR)/city1.4bpp;	$(GBAGFX) $< $@
$(INTRO_GFX_BUILD_DIR)/city2.4bpp.lz: $(INTRO_GFX_BUILD_DIR)/city2.4bpp;	$(GBAGFX) $< $@
$(INTRO_GFX_BUILD_DIR)/city3.4bpp.lz: $(INTRO_GFX_BUILD_DIR)/city3.4bpp;	$(GBAGFX) $< $@






####	GAME OVER	####
GAMEOVER_GFX_DIR = gfx/game_over
GAMEOVER_GFX_BUILD_DIR = $(BUILD_GFX_DIR)/game_over
$(shell mkdir -p $(GAMEOVER_GFX_BUILD_DIR))

$(GAMEOVER_GFX_BUILD_DIR)/gameover.tilemap: $(GAMEOVER_GFX_DIR)/gameover.bin;	$(GBAGFX) $< $@ -w 30 -h 20
$(GAMEOVER_GFX_BUILD_DIR)/gameover.tilemap.lz: $(GAMEOVER_GFX_BUILD_DIR)/gameover.tilemap;	$(GBAGFX) $< $@
$(GAMEOVER_GFX_BUILD_DIR)/gameover.4bpp.lz: $(GAMEOVER_GFX_BUILD_DIR)/gameover.4bpp;	$(GBAGFX) $< $@ -overflow 2
$(GAMEOVER_GFX_BUILD_DIR)/yes_no_sprite.4bpp.lz: $(GAMEOVER_GFX_BUILD_DIR)/yes_no_sprite.4bpp;	$(GBAGFX) $< $@



####	ENDING		####
ENDING_GFX_DIR = gfx/ending
ENDING_GFX_BUILD_DIR = $(BUILD_GFX_DIR)/ending
$(shell mkdir -p $(ENDING_GFX_BUILD_DIR))

$(ENDING_GFX_BUILD_DIR)/ninja_face.4bpp.lz: $(ENDING_GFX_BUILD_DIR)/ninja_face.4bpp; $(GBAGFX) $< $@
$(ENDING_GFX_BUILD_DIR)/end_city.4bpp.lz: $(ENDING_GFX_BUILD_DIR)/end_city.4bpp; $(GBAGFX) $< $@

$(ENDING_GFX_BUILD_DIR)/ninja_face.tilemap: $(ENDING_GFX_DIR)/ninja_face.bin; $(GBAGFX) $< $@ -w 30 -h 20
$(ENDING_GFX_BUILD_DIR)/ninja_face.tilemap.lz: $(ENDING_GFX_BUILD_DIR)/ninja_face.tilemap; $(GBAGFX) $< $@

$(ENDING_GFX_BUILD_DIR)/end_city.tilemap: $(ENDING_GFX_DIR)/end_city.bin; $(GBAGFX) $< $@ -w 30 -h 29
$(ENDING_GFX_BUILD_DIR)/end_city.tilemap.lz: $(ENDING_GFX_BUILD_DIR)/end_city.tilemap; $(GBAGFX) $< $@

$(ENDING_GFX_BUILD_DIR)/bad_ending_clock.4bpp.lz: $(ENDING_GFX_BUILD_DIR)/bad_ending_clock.4bpp; $(GBAGFX) $< $@
$(ENDING_GFX_BUILD_DIR)/bad_ending_clock.tilemap: $(ENDING_GFX_DIR)/bad_ending_clock.bin; $(GBAGFX) $< $@ -w 30 -h 20
$(ENDING_GFX_BUILD_DIR)/bad_ending_clock.tilemap.lz: $(ENDING_GFX_BUILD_DIR)/bad_ending_clock.tilemap; $(GBAGFX) $< $@


####	UNKNOWN		####
UNK_GFX_DIR = gfx/unknown
UNK_GFX_BUILD_DIR = $(BUILD_GFX_DIR)/unknown
$(shell mkdir -p $(UNK_GFX_BUILD_DIR))


$(UNK_GFX_BUILD_DIR)/unk_820f4f8.tilemap.lz: $(UNK_GFX_BUILD_DIR)/unk_820f4f8.tilemap; $(GBAGFX) $< $@
$(UNK_GFX_BUILD_DIR)/unk_820f4f8.tilemap: $(UNK_GFX_DIR)/unk_820f4f8.bin; $(GBAGFX) $< $@ -w 32 -h 22

$(UNK_GFX_BUILD_DIR)/unk_821f37c.tilemap: $(UNK_GFX_DIR)/unk_821f37c.bin; $(GBAGFX) $< $@ -w 30 -h 20
$(UNK_GFX_BUILD_DIR)/unk_821f37c.tilemap.lz: $(UNK_GFX_BUILD_DIR)/unk_821f37c.tilemap; $(GBAGFX) $< $@

$(UNK_GFX_BUILD_DIR)/unk_821DD30.4bpp.lz: $(UNK_GFX_BUILD_DIR)/unk_821DD30.4bpp; $(GBAGFX) $< $@

$(UNK_GFX_BUILD_DIR)/rocket.4bpp.lz: $(UNK_GFX_BUILD_DIR)/rocket.4bpp; $(GBAGFX) $< $@
$(UNK_GFX_BUILD_DIR)/rocket.tilemap: $(UNK_GFX_DIR)/rocket.bin; $(GBAGFX) $< $@ -w 32 -h 28
$(UNK_GFX_BUILD_DIR)/rocket.tilemap.lz: $(UNK_GFX_BUILD_DIR)/rocket.tilemap; $(GBAGFX) $< $@

$(UNK_GFX_BUILD_DIR)/unused_face_0.4bpp.lz: $(UNK_GFX_BUILD_DIR)/unused_face_0.4bpp; $(GBAGFX) $< $@ -overflow 2
$(UNK_GFX_BUILD_DIR)/unused_face_1.4bpp.lz: $(UNK_GFX_BUILD_DIR)/unused_face_1.4bpp; $(GBAGFX) $< $@ -overflow 6
$(UNK_GFX_BUILD_DIR)/unused_face_2.4bpp.lz: $(UNK_GFX_BUILD_DIR)/unused_face_2.4bpp; $(GBAGFX) $< $@ -overflow 14
$(UNK_GFX_BUILD_DIR)/unused_face_3.4bpp.lz: $(UNK_GFX_BUILD_DIR)/unused_face_3.4bpp; $(GBAGFX) $< $@ -overflow 1

$(UNK_GFX_BUILD_DIR)/unused_face_0.tilemap: $(UNK_GFX_DIR)/unused_face_0.bin; $(GBAGFX) $< $@ -w 30 -h 20
$(UNK_GFX_BUILD_DIR)/unused_face_0.tilemap.lz: $(UNK_GFX_BUILD_DIR)/unused_face_0.tilemap; $(GBAGFX) $< $@ -overflow 11
$(UNK_GFX_BUILD_DIR)/unused_face_1.tilemap: $(UNK_GFX_DIR)/unused_face_1.bin; $(GBAGFX) $< $@ -w 30 -h 20
$(UNK_GFX_BUILD_DIR)/unused_face_1.tilemap.lz: $(UNK_GFX_BUILD_DIR)/unused_face_1.tilemap; $(GBAGFX) $< $@ -overflow 9
$(UNK_GFX_BUILD_DIR)/unused_face_2.tilemap: $(UNK_GFX_DIR)/unused_face_2.bin; $(GBAGFX) $< $@ -w 30 -h 20
$(UNK_GFX_BUILD_DIR)/unused_face_2.tilemap.lz: $(UNK_GFX_BUILD_DIR)/unused_face_2.tilemap; $(GBAGFX) $< $@ -overflow 5
$(UNK_GFX_BUILD_DIR)/unused_face_3.tilemap: $(UNK_GFX_DIR)/unused_face_3.bin; $(GBAGFX) $< $@ -w 30 -h 20
$(UNK_GFX_BUILD_DIR)/unused_face_3.tilemap.lz: $(UNK_GFX_BUILD_DIR)/unused_face_3.tilemap; $(GBAGFX) $< $@ -overflow 7

$(UNK_GFX_BUILD_DIR)/unused_ninja_face.tilemap: $(UNK_GFX_DIR)/unused_ninja_face.bin; $(GBAGFX) $< $@ -w 30 -h 20
$(UNK_GFX_BUILD_DIR)/unused_ninja_face.tilemap.lz: $(UNK_GFX_BUILD_DIR)/unused_ninja_face.tilemap; $(GBAGFX) $< $@
$(UNK_GFX_BUILD_DIR)/unused_ninja_face.4bpp.lz: $(UNK_GFX_BUILD_DIR)/unused_ninja_face.4bpp; $(GBAGFX) $< $@




####	PLAYER		####
PLAYER_GFX_DIR = gfx/player
PLAYER_GFX_BUILD_DIR = $(BUILD_GFX_DIR)/player
$(shell mkdir -p $(PLAYER_GFX_BUILD_DIR))


####	DOOR		####
DOOR_GFX_DIR = gfx/door_opening
DOOR_GFX_BUILD_DIR = $(BUILD_GFX_DIR)/door_opening
$(shell mkdir -p $(DOOR_GFX_BUILD_DIR))
