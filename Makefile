
ifeq ($(strip $(DEVKITARM)),)
$(error "Please set DEVKITARM in your environment. export DEVKITARM=<path to>devkitARM)
endif

.DELETE_ON_ERROR:


SCANINC = tools/scaninc/scaninc
GBAGFX = tools/gbagfx/gbagfx
AS = $(DEVKITARM)/bin/arm-none-eabi-as
ASFLAGS = --defsym $(GAME_VERSION)=1 -mthumb-interwork -march=armv4t
LD = $(DEVKITARM)/bin/arm-none-eabi-ld
OBJCOPY = $(DEVKITARM)/bin/arm-none-eabi-objcopy

BUILDDIR = build/$(BUILD_NAME)

ASMFILES := $(foreach dir,asm,$(notdir $(wildcard $(dir)/*.s)))
DATAFILES := $(foreach dir,data,$(notdir $(wildcard $(dir)/*.s)))
OFILES := $(addprefix $(BUILDDIR)/asm/,$(ASMFILES:.s=.o)) $(addprefix $(BUILDDIR)/data/,$(DATAFILES:.s=.o))


# Dependency scanning
.SECONDEXPANSION:
$(BUILDDIR)/data/%.o: DATA_DEP = $(shell $(SCANINC) -I . data/$(*F).s)
$(BUILDDIR)/asm/%.o: ASM_DEP = $(shell $(SCANINC) -I . asm/$(*F).s)




$(BUILDDIR)/asm/%.o : asm/%.s $$(ASM_DEP)
	@echo asm: $< -> $@
	$(AS) $(ASFLAGS) $< -o $@





$(BUILDDIR)/data/%.o : data/%.s $$(DATA_DEP)
	@echo data: $< -> $@
	$(AS) $(ASFLAGS) $< -o $@




$(BUILD_NAME).elf: $(OFILES) ld.txt sym_iwram.txt sym_ewram.txt
	cp ld.txt $(BUILDDIR)/ld.txt
	cd $(BUILDDIR) && $(LD) -o ../../$(BUILD_NAME).elf -T ld.txt -M=map.txt



$(BUILD_NAME).gba: $(BUILD_NAME).elf
	$(OBJCOPY) -O binary $(BUILD_NAME).elf $(BUILD_NAME).gba



# Create build directories (only if BUILDDIR is set to avoid creating asm & data in build/)
$(shell mkdir -p build build/gfx)
ifneq ($(strip $(BUILD_NAME)),)
$(shell mkdir -p $(BUILDDIR) $(BUILDDIR)/asm $(BUILDDIR)/data)
endif



cop:      	; @$(MAKE) all GAME_VERSION=VERSION_COP BUILD_NAME=ninjacop
fiveo:      ; @$(MAKE) all GAME_VERSION=VERSION_FIVEO BUILD_NAME=ninjafiveo



include graphics_rules.mk



.PHONY: clean all tools compare

all: $(BUILD_NAME).gba

compare:
	@$(MAKE) all GAME_VERSION=VERSION_COP BUILD_NAME=ninjacop
	sha1sum -c cop.sha1

tools:
	cd tools/gbagfx/ && make
	cd tools/scaninc/ && make

clean:
	rm -rf build
	rm *.elf
	rm *.gba
