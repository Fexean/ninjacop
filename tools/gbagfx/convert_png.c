// Copyright (c) 2015 YamaArashi

#include <stdio.h>
#include <setjmp.h>
#include <png.h>
#include "global.h"
#include "convert_png.h"
#include "gfx.h"

static FILE *PngReadOpen(char *path, png_structp *pngStruct, png_infop *pngInfo)
{
    FILE *fp = fopen(path, "rb");

    if (fp == NULL)
        FATAL_ERROR("Failed to open \"%s\" for reading.\n", path);

    unsigned char sig[8];

    if (fread(sig, 8, 1, fp) != 1)
        FATAL_ERROR("Failed to read PNG signature from \"%s\".\n", path);

    if (png_sig_cmp(sig, 0, 8))
        FATAL_ERROR("\"%s\" does not have a valid PNG signature.\n", path);

    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (!png_ptr)
        FATAL_ERROR("Failed to create PNG read struct.\n");

    png_infop info_ptr = png_create_info_struct(png_ptr);

    if (!info_ptr)
        FATAL_ERROR("Failed to create PNG info struct.\n");

    if (setjmp(png_jmpbuf(png_ptr)))
        FATAL_ERROR("Failed to init I/O for reading \"%s\".\n", path);

    png_init_io(png_ptr, fp);
    png_set_sig_bytes(png_ptr, 8);
    png_read_info(png_ptr, info_ptr);

    *pngStruct = png_ptr;
    *pngInfo = info_ptr;

    return fp;
}

static unsigned char *ConvertBitDepth(unsigned char *src, int srcBitDepth, int destBitDepth, int numPixels)
{
    // Round the number of bits up to the next 8 and divide by 8 to get the number of bytes.
    int srcSize = ((numPixels * srcBitDepth + 7) & ~7) / 8;
    int destSize = ((numPixels * destBitDepth + 7) & ~7) / 8;
    unsigned char *output = calloc(destSize, 1);
    unsigned char *dest = output;
    int i;
    int j;
    int destBit = 8 - destBitDepth;

    for (i = 0; i < srcSize; i++)
    {
        unsigned char srcByte = src[i];

        for (j = 8 - srcBitDepth; j >= 0; j -= srcBitDepth)
        {
            unsigned char pixel = (srcByte >> j) % (1 << srcBitDepth);

            if (pixel >= (1 << destBitDepth))
                FATAL_ERROR("Image exceeds the maximum color value for a %ibpp image.\n", destBitDepth);
            *dest |= pixel << destBit;
            destBit -= destBitDepth;
            if (destBit < 0)
            {
                dest++;
                destBit = 8 - destBitDepth;
            }
        }
    }

    return output;
}

void ReadPng(char *path, struct Image *image)
{
    png_structp png_ptr;
    png_infop info_ptr;

    FILE *fp = PngReadOpen(path, &png_ptr, &info_ptr);

    int bit_depth = png_get_bit_depth(png_ptr, info_ptr);

    int color_type = png_get_color_type(png_ptr, info_ptr);

    if (color_type != PNG_COLOR_TYPE_GRAY && color_type != PNG_COLOR_TYPE_PALETTE)
        FATAL_ERROR("\"%s\" has an unsupported color type.\n", path);

    // Check if the image has a palette so that we can tell if the colors need to be inverted later.
    // Don't read the palette because it's not needed for now.
    image->hasPalette = (color_type == PNG_COLOR_TYPE_PALETTE);

    image->width = png_get_image_width(png_ptr, info_ptr);
    image->height = png_get_image_height(png_ptr, info_ptr);

    int rowbytes = png_get_rowbytes(png_ptr, info_ptr);

    image->pixels = malloc(image->height * rowbytes);

    if (image->pixels == NULL)
        FATAL_ERROR("Failed to allocate pixel buffer.\n");

    png_bytepp row_pointers = malloc(image->height * sizeof(png_bytep));

    if (row_pointers == NULL)
        FATAL_ERROR("Failed to allocate row pointers.\n");

    for (int i = 0; i < image->height; i++)
        row_pointers[i] = (png_bytep)(image->pixels + (i * rowbytes));

    if (setjmp(png_jmpbuf(png_ptr)))
        FATAL_ERROR("Error reading from \"%s\".\n", path);

    png_read_image(png_ptr, row_pointers);

    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);

    free(row_pointers);
    fclose(fp);

    if (bit_depth != image->bitDepth)
    {
        unsigned char *src = image->pixels;

        if (bit_depth != 1 && bit_depth != 2 && bit_depth != 4 && bit_depth != 8)
            FATAL_ERROR("Bit depth of image must be 1, 2, 4, or 8.\n");
        image->pixels = ConvertBitDepth(image->pixels, bit_depth, image->bitDepth, image->width * image->height);
        free(src);
        image->bitDepth = bit_depth;
    }
}

void ReadPngPalette(char *path, struct Palette *palette)
{
    png_structp png_ptr;
    png_infop info_ptr;
    png_colorp colors;
    int numColors;

    FILE *fp = PngReadOpen(path, &png_ptr, &info_ptr);

    if (png_get_color_type(png_ptr, info_ptr) != PNG_COLOR_TYPE_PALETTE)
        FATAL_ERROR("The image \"%s\" does not contain a palette.\n", path);

    if (png_get_PLTE(png_ptr, info_ptr, &colors, &numColors) != PNG_INFO_PLTE)
        FATAL_ERROR("Failed to retrieve palette from \"%s\".\n", path);

    if (numColors > 256)
        FATAL_ERROR("Images with more than 256 colors are not supported.\n");

    palette->numColors = numColors;
    for (int i = 0; i < numColors; i++) {
        palette->colors[i].red = colors[i].red;
        palette->colors[i].green = colors[i].green;
        palette->colors[i].blue = colors[i].blue;
    }

    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);

    fclose(fp);
}

void SetPngPalette(png_structp png_ptr, png_infop info_ptr, struct Palette *palette)
{
    png_colorp colors = malloc(palette->numColors * sizeof(png_color));

    if (colors == NULL)
        FATAL_ERROR("Failed to allocate PNG palette.\n");

    for (int i = 0; i < palette->numColors; i++) {
        colors[i].red = palette->colors[i].red;
        colors[i].green = palette->colors[i].green;
        colors[i].blue = palette->colors[i].blue;
    }

    png_set_PLTE(png_ptr, info_ptr, colors, palette->numColors);

    free(colors);
}

void WritePng(char *path, struct Image *image)
{
    FILE *fp = fopen(path, "wb");

    if (fp == NULL)
        FATAL_ERROR("Failed to open \"%s\" for writing.\n", path);

    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (!png_ptr)
        FATAL_ERROR("Failed to create PNG write struct.\n");

    png_infop info_ptr = png_create_info_struct(png_ptr);

    if (!info_ptr)
        FATAL_ERROR("Failed to create PNG info struct.\n");

    if (setjmp(png_jmpbuf(png_ptr)))
        FATAL_ERROR("Failed to init I/O for writing \"%s\".\n", path);

    png_init_io(png_ptr, fp);

    if (setjmp(png_jmpbuf(png_ptr)))
        FATAL_ERROR("Error writing header for \"%s\".\n", path);

	
    png_set_IHDR(png_ptr, info_ptr, image->width, image->height,
        image->bitDepth, PNG_COLOR_TYPE_PALETTE, PNG_INTERLACE_NONE,
        PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    if (image->hasPalette) {
        SetPngPalette(png_ptr, info_ptr, &image->palette);

        if (image->hasTransparency) {
            png_byte trans = 0;
            png_set_tRNS(png_ptr, info_ptr, &trans, 1, 0);
        }
    }else{
	
		#define C(intensity) {intensity, intensity, intensity}
		struct Palette grayscale1Pal = {{C(255), C(0)}, 2};
		struct Palette grayscale4Pal = {{C(255), C(238), C(221), C(204), C(187), C(170), C(153), C(136), C(119), C(102), C(85), C(68), C(51), C(34), C(17), C(0)}, 16};
		struct Palette grayscale8Pal = {{C(255), C(254), C(253), C(252), C(251), C(250), C(249), C(248), C(247), C(246), C(245), C(244), C(243), C(242), C(241), C(240), C(239), C(238), C(237), C(236), C(235), C(234), C(233), C(232), C(231), C(230), C(229), C(228), C(227), C(226), C(225), C(224), C(223), C(222), C(221), C(220), C(219), C(218), C(217), C(216), C(215), C(214), C(213), C(212), C(211), C(210), C(209), C(208), C(207), C(206), C(205), C(204), C(203), C(202), C(201), C(200), C(199), C(198), C(197), C(196), C(195), C(194), C(193), C(192), C(191), C(190), C(189), C(188), C(187), C(186), C(185), C(184), C(183), C(182), C(181), C(180), C(179), C(178), C(177), C(176), C(175), C(174), C(173), C(172), C(171), C(170), C(169), C(168), C(167), C(166), C(165), C(164), C(163), C(162), C(161), C(160), C(159), C(158), C(157), C(156), C(155), C(154), C(153), C(152), C(151), C(150), C(149), C(148), C(147), C(146), C(145), C(144), C(143), C(142), C(141), C(140), C(139), C(138), C(137), C(136), C(135), C(134), C(133), C(132), C(131), C(130), C(129), C(128), C(127), C(126), C(125), C(124), C(123), C(122), C(121), C(120), C(119), C(118), C(117), C(116), C(115), C(114), C(113), C(112), C(111), C(110), C(109), C(108), C(107), C(106), C(105), C(104), C(103), C(102), C(101), C(100), C(99), C(98), C(97), C(96), C(95), C(94), C(93), C(92), C(91), C(90), C(89), C(88), C(87), C(86), C(85), C(84), C(83), C(82), C(81), C(80), C(79), C(78), C(77), C(76), C(75), C(74), C(73), C(72), C(71), C(70), C(69), C(68), C(67), C(66), C(65), C(64), C(63), C(62), C(61), C(60), C(59), C(58), C(57), C(56), C(55), C(54), C(53), C(52), C(51), C(50), C(49), C(48), C(47), C(46), C(45), C(44), C(43), C(42), C(41), C(40), C(39), C(38), C(37), C(36), C(35), C(34), C(33), C(32), C(31), C(30), C(29), C(28), C(27), C(26), C(25), C(24), C(23), C(22), C(21), C(20), C(19), C(18), C(17), C(16), C(15), C(14), C(13), C(12), C(11), C(10), C(9), C(8), C(7), C(6), C(5), C(4), C(3), C(2), C(1), C(0)}, 256};
		#undef C
		
		switch(image->bitDepth){
			case 1:
			SetPngPalette(png_ptr, info_ptr, &grayscale1Pal);
			break;
			
			case 4:
			SetPngPalette(png_ptr, info_ptr, &grayscale4Pal);
			break;
			
			case 8:
			SetPngPalette(png_ptr, info_ptr, &grayscale8Pal);
			break;
		}
		
	}

    png_write_info(png_ptr, info_ptr);

    png_bytepp row_pointers = malloc(image->height * sizeof(png_bytep));

    if (row_pointers == NULL)
        FATAL_ERROR("Failed to allocate row pointers.\n");

    int rowbytes = png_get_rowbytes(png_ptr, info_ptr);

    for (int i = 0; i < image->height; i++)
        row_pointers[i] = (png_bytep)(image->pixels + (i * rowbytes));

    if (setjmp(png_jmpbuf(png_ptr)))
        FATAL_ERROR("Error writing \"%s\".\n", path);

    png_write_image(png_ptr, row_pointers);

    if (setjmp(png_jmpbuf(png_ptr)))
        FATAL_ERROR("Error ending write of \"%s\".\n", path);

    png_write_end(png_ptr, NULL);

    fclose(fp);

    png_destroy_write_struct(&png_ptr, &info_ptr);
    free(row_pointers);
}
