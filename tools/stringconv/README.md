# stringconv
Converts a csv file filled with strings to the encoding used by ninja cop. Each generated string is written to its own bin file.

This is unfinished and not currently used in the disassembly.


## CSV format

1 String per row. First column contains the string, second the font number and third the file in which the result is written to.

Example:

    EMERGENCY!;3;build/strings/emergency.bin
    Continue?;3;build/strings/continue.bin