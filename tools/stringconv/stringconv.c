#include <stdio.h>
#include <assert.h>

const int gEndianTest = 1;
#define IS_LITTLE_ENDIAN() ( (*(char*)&gEndianTest) != 0 )

extern const short* gFontTables[4];

char toLowerCase(char x){
	if(x >= 'A' && x <= 'Z'){
		x += 'a'-'A';
	}
	return x;
}

const char* handleSpecialChar(const char* str, short* output, int outputPos){
	short data;
	
	if(str[1] == 'x' && str[6] == ']'){	// "[x1234]" => 0x1234
		sscanf(str+2, "%4hx", &data);
		output[outputPos] = data;
		return str + 7;
	}else if(str[2] == ':' && str[5] == ']'){	// "[a:43]" => 'a' 0x43
		char character = toLowerCase(str[1]) - 'a';
		sscanf(str+3, "%2hx", &data);
		data <<= 8;
		output[outputPos] = data | character;
		return str+6;
	}
	printf("Invalid special char: %s\n", str);
	return str+1;
}

void createStringFile(const char* str, unsigned int font, const char* filename){
	if(font > 3){
		printf("Invalid font: %d\n", font);
		return;
	}
	
	FILE* fp = fopen(filename, "wb");
	if(!fp){
		printf("Unable to open file: %s\n", filename);
		return;
	}
	
	short stringbuf[100];
	int i = 0;
	const short* fontTable = gFontTables[font];
	while(*str){
		char character = toLowerCase(*str);
		
		if(character == '['){
			str = handleSpecialChar(str, stringbuf, i);
			i++;
			continue;
		}

		stringbuf[i++]= fontTable[character];
		str++;
	}
	stringbuf[i] = 0x00FF;
	fwrite(stringbuf, 2, i+1, fp);
	fclose(fp);
}

int main(int argc, char* argv[]){
	assert(IS_LITTLE_ENDIAN());
	
	int font = 0;
	char strBuf[100];
	char nameBuf[100];

	if(argc == 2){
		FILE* fp = fopen(argv[1], "r");
		while(fp && !feof(fp)){
			fscanf(fp, "%99[^;];%d;%99[^;\n]\n", strBuf, &font, nameBuf);
			printf("%s %d %s\n", strBuf, font, nameBuf);
			createStringFile(strBuf, font, nameBuf);
		}
	}else{
		printf("USAGE: %s <csvfile>\n", argv[0]);
	}
	return 0;
}