

.globl font0Pal
font0Pal:
.incbin "build/gfx/fonts/font0.gbapal"

.globl font0Tiles
font0Tiles:
.incbin "build/gfx/fonts/font0.4bpp.lz"



.globl font1Pal
font1Pal:
.incbin "build/gfx/fonts/font1.gbapal"

.globl font1tiles
font1tiles:
.incbin "build/gfx/fonts/font1.4bpp.lz"




.globl font2pal
font2pal:
.incbin "build/gfx/fonts/font2.gbapal"

.globl font2tiles
font2tiles:
.incbin "build/gfx/fonts/font2.4bpp.lz"




.globl font3pal
font3pal:
.incbin "build/gfx/fonts/font3.gbapal"


.globl font3tiles
font3tiles:
.incbin "build/gfx/fonts/font3.4bpp.lz"





@Used on game over screen

.globl dword_8208DA0
dword_8208DA0:
.incbin "build/gfx/fonts/numbers0.gbapal"

.globl score_numberTiles
score_numberTiles:
.incbin "build/gfx/fonts/numbers0.4bpp.lz"





@Sprites?

.globl unk_82090b8
unk_82090b8:
.long 0x1, 0x0, 0x0, 0x0


.globl dword_82090C8
dword_82090C8:
.long 0x50000000, 0x1f


.globl unk_82090d0
unk_82090d0:
.long 0x1, 0x0, 0x0, 0x4


.globl dword_82090E0
dword_82090E0:
.long 0x50000000, 0x1f


.globl unk_82090e8
unk_82090e8:
.long 0x1, 0x0, 0x0, 0x8, 0x50000000, 0x1f



@Used on level select

.globl buttonTiles
buttonTiles:
.incbin "build/gfx/fonts/buttons.4bpp.lz"


.globl dword_820918C
dword_820918C:
.incbin "build/gfx/fonts/buttons.gbapal"

