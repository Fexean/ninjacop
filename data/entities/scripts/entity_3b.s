.globl entity_3b_scr
entity_3b_scr:
	
	call scr_8009b30
	wait_frame 
	goto scr_8008dd8
	
scr_8008dd8:
	set_anim 0x0
	call scr_80092c4
	
scr_8008de8:
	call_if_attacked scr_800915c
	goto_if_asm (sub_80218F0+1), scr_8009574
	goto_if_not_player_x scr_8008e3c
	branchPlayerVulnerable scr_8008e14
	goto scr_8008e20
	
scr_8008e14:
	goto_if_y_between2 0x50, 0x100, scr_8008ff4
	
scr_8008e20:
	call scr_80092d8
	call scr_8009134
	wait_frame 
	goto scr_8008de8
	
scr_8008e3c:
	call scr_8008f38
	wait_frame 
	goto scr_8008de8
	
scr_8008e50:
	set_anim 0x0
	call scr_80092c4
	
scr_8008e60:
	call_if_attacked scr_800915c
	goto_if_asm (sub_80218F0+1), scr_8009574
	faceplayer 
	branchPlayerVulnerable scr_8008e88
	goto scr_8008eac
	
scr_8008e88:
	goto_if_y_between2 0x0, 0x48, scr_8008ff4
	goto_if_y_between2 0x80, 0x100, scr_8008ff4
	goto_if_near_player_x scr_8008ff4, 0x40
	
scr_8008eac:
	call scr_80092d8
	wait_frame 
	goto scr_8008e60
	
scr_8008ec0:
	set_anim 0xd
	call scr_80092c4
	
scr_8008ed0:
	call_if_attacked scr_800915c
	goto_if_asm (sub_80218F0+1), scr_8009574
	goto_if_not_player_x scr_8008f24
	branchPlayerVulnerable scr_8008efc
	goto scr_8008f08
	
scr_8008efc:
	goto_if_y_between2 0x0, 0x78, scr_8008ff4
	
scr_8008f08:
	call scr_80092d8
	call scr_8009148
	wait_frame 
	goto scr_8008ed0
	
scr_8008f24:
	call scr_8008f38
	wait_frame 
	goto scr_8008ed0
	
scr_8008f38:
	set_vel_x 0x0
	set_vel_y 0x0
	call scr_8008fa0
	goto_if_player_x scr_8008f94
	set_anim 0x7
	
scr_8008f60:
	call_if_attacked scr_800915c
	BranchHighBit scr_8008f88, 0x0
	goto_if_anim_done scr_8008f90
	wait_frame 
	goto scr_8008f60
	
scr_8008f88:
	ClearHighBit 0x0
	
scr_8008f90:
	toggle_dir 
	
scr_8008f94:
	set_anim 0x0
	return 
	
scr_8008fa0:
	set_anim 0xa
	LoadDifficulty 0x3c, 0x1e, 0x1
	
scr_8008fb4:
	call_if_attacked scr_800915c
	BranchHighBit scr_8008fe8, 0x0
	goto_if_player_x scr_8008fe8
	cmd_0E scr_8008fe8, 0x0
	wait_frame 
	goto scr_8008fb4
	
scr_8008fe8:
	ClearHighBit 0x0
	return 
	
scr_8008ff4:
	call scr_80090e0
	
scr_8008ffc:
	goto_if_asm (sub_80218F0+1), scr_8009590
	goto_if_y_between2 0x80, 0x100, scr_8009040
	goto_if_y_between2 0x50, 0x80, scr_8009030
	call scr_8009050
	goto scr_8008dd8
	
scr_8009030:
	call scr_8009080
	goto scr_8008e50
	
scr_8009040:
	call scr_80090b0
	goto scr_8008ec0
	
scr_8009050:
	goto_if_x_between2 0x0, 0x80, scr_800906c
	SetInitPos 0x38, 0x10
	goto scr_8009074
	
scr_800906c:
	SetInitPos 0xc8, 0x10
	
scr_8009074:
	faceplayer 
	goto scr_8009114
	
scr_8009080:
	goto_if_x_between2 0x0, 0x80, scr_800909c
	SetInitPos 0x18, 0x50
	goto scr_80090a4
	
scr_800909c:
	SetInitPos 0xe8, 0x50
	
scr_80090a4:
	faceplayer 
	goto scr_8009114
	
scr_80090b0:
	SetInitPos 0x80, 0xb8
	faceplayer 
	goto scr_8009114
	call scr_80090e0
	SetInitPos 0x80, 0x70
	faceplayer 
	goto scr_8009114
	
scr_80090e0:
	reset_velocity 
	disable_collision 
	SetBit4Field44 
	play_sound 0x18c
	set_anim2 0xe
	ClearHighBit 0x0
	call scr_8009548
	hidesprite 
	return 
	
scr_8009114:
	showsprite 
	set_anim 0xf
	call scr_8009548
	enable_collision 
	ClearBit4Field44 
	return 
	
scr_8009134:
	set_anim 0x8
	cmd_3D_setVelXDirBased 0x80
	return 
	
scr_8009148:
	set_anim 0xd
	callasm (sub_80218B8+1)
	return 
	
scr_800915c:
	ClearHighBit 0x0
	ClearBit7Field44 
	ifChildFlag23 scr_8009188
	BranchChildYRange scr_8009188, 0x20, 0x64
	call scr_80092ac
	return 
	
scr_8009188:
	cmd_62 
	cmd_9C 
	cmd_D2 0x5a0059, 0x59
	cmd_6C scr_8009230
	cmd_C0 scr_80091b8, 0x32
	SetHighBit 0xf
	
scr_80091b8:
	cmd_DA unk_82dfc88
	ifChildFlag21 scr_80091d8
	ifChildFlag23 scr_80091f0
	goto scr_80091e4
	
scr_80091d8:
	play_sound 0x17f
	return 
	
scr_80091e4:
	play_sound 0x180
	return 
	
scr_80091f0:
	play_sound 0x181
	set_anim 0x4
	SetHighBit 0x0
	
scr_8009208:
	goto_if_attacked scr_800915c
	goto_if_anim_done scr_8009224
	wait_frame 
	goto scr_8009208
	
scr_8009224:
	set_anim 0x0
	return 
	
scr_8009230:
	callasm (sub_8033184+1)
	set_anim 0x4
	play_sound 0x189
	reset_velocity 
	SetBit4Field44 
	disable_collision 
	SetInit4E 0x0
	
scr_800925c:
	goto_if_collide scr_8009270
	wait_frame 
	goto scr_800925c
	
scr_8009270:
	set_anim 0x1b
	call scr_8009548
	clear_local 
	
scr_8009284:
	for scr_800929c, 0x1e
	wait_frame 
	goto scr_8009284
	
scr_800929c:
	complete_lvl 
	
scr_80092a0:
	wait_frame 
	goto scr_80092a0
	
scr_80092ac:
	cmd_BC 0x71, 0x0, 0x0
	play_sound 0x18d
	return 
	
scr_80092c4:
	load_rand 0x3c
	add_local 0x1e
	return 
	
scr_80092d8:
	cmd_0E scr_80092e8, 0x0
	return 
	
scr_80092e8:
	goto_if_difficulty 0x0, scr_8009300
	goto_if_rng scr_80094bc, 0x14
	
scr_8009300:
	reset_velocity 
	goto_if_far_player_y scr_8009324, 0x10
	goto_if_rng scr_8009324, 0x32
	goto scr_8009334
	
scr_8009324:
	call scr_8009354
	goto scr_8009344
	
scr_8009334:
	call scr_80093f0
	goto scr_8009344
	
scr_8009344:
	set_anim 0x0
	goto scr_80092c4
	
scr_8009354:
	set_anim 0x1
	clear_local 
	
scr_8009360:
	call_if_attacked scr_800915c
	BranchHighBit scr_80093e4, 0x0
	for scr_800938c, 0x31
	wait_frame 
	goto scr_8009360
	
scr_800938c:
	play_sound 0x17c
	cmd_48 0x38, 0x20, 0x300, 0x0, 0x62
	cmd_48 0x18, 0x24, 0x0, 0x0, 0x6c
	
scr_80093bc:
	call_if_attacked scr_800915c
	BranchHighBit scr_80093e4, 0x0
	goto_if_anim_done scr_80093e4
	wait_frame 
	goto scr_80093bc
	
scr_80093e4:
	ClearHighBit 0x0
	return 
	
scr_80093f0:
	set_anim 0x2
	clear_local 
	
scr_80093fc:
	call_if_attacked scr_800915c
	BranchHighBit scr_80094b0, 0x0
	for scr_8009428, 0x30
	wait_frame 
	goto scr_80093fc
	
scr_8009428:
	cmd_48 0x18, 0x24, 0x0, 0x0, 0x6c
	goto_if_y_between 0x50, 0x100, scr_800946c
	play_sound 0x17d
	cmd_48 0x18, 0x0, 0x300, 0x0, 0x63
	goto scr_8009488
	
scr_800946c:
	play_sound 0x17c
	cmd_48 0x18, 0x10, 0x300, 0x0, 0x62
	
scr_8009488:
	call_if_attacked scr_800915c
	BranchHighBit scr_80094b0, 0x0
	goto_if_anim_done scr_80094b0
	wait_frame 
	goto scr_8009488
	
scr_80094b0:
	ClearHighBit 0x0
	return 
	
scr_80094bc:
	call scr_80090e0
	goto_if_y_between2 0x50, 0x80, scr_80094fc
	goto_if_x_between2 0x0, 0x80, scr_80094ec
	SetInitPos 0xc8, 0x10
	goto scr_8009528
	
scr_80094ec:
	SetInitPos 0x38, 0x10
	goto scr_8009528
	
scr_80094fc:
	goto_if_x_between2 0x0, 0x80, scr_8009518
	SetInitPos 0xe8, 0x50
	goto scr_8009528
	
scr_8009518:
	SetInitPos 0x18, 0x50
	goto scr_8009528
	
scr_8009528:
	cmd_3C_modifyDir 
	call scr_8009114
	call scr_8009300
	ClearField62 
	goto scr_8008ff4
	
scr_8009548:
	call_if_attacked scr_800915c
	BranchHighBit scr_8009570, 0x0
	goto_if_anim_done scr_8009570
	wait_frame 
	goto scr_8009548
	
scr_8009570:
	return 
	
scr_8009574:
	BranchHighBit scr_8009840, 0x3
	SetHighBit 0x3
	call scr_80090e0
	
scr_8009590:
	SetInitPos 0x80, 0x70
	faceplayer 
	call scr_8009114
	callasm (sub_80219DC+1)
	set_anim 0x10
	call scr_8009820
	set_anim 0x11
	call scr_8009820
	set_anim 0x11
	goto_if_difficulty 0x0, scr_8009618
	cmd_C0 scr_8009618, 0x33
	cmd_C0 scr_8009674, 0x1a
	goto_if_difficulty 0x1, scr_800972c
	goto scr_80096d0
	call scr_8008220
	return 
	
scr_8009618:
	goto_if_rng scr_800964c, 0x32
	call scr_80097ac
	
scr_800962c:
	call_if_attacked scr_80092ac
	cmd_D3 0x1, 0x0, scr_8009788
	wait_frame 
	goto scr_800962c
	
scr_800964c:
	call scr_80097cc
	
scr_8009654:
	call_if_attacked scr_80092ac
	cmd_D3 0x0, 0x0, scr_8009788
	wait_frame 
	goto scr_8009654
	
scr_8009674:
	goto_if_rng scr_80096a8, 0x32
	call scr_80097ac
	
scr_8009688:
	call_if_attacked scr_80092ac
	cmd_D3 0x1, 0x1, scr_8009788
	wait_frame 
	goto scr_8009688
	
scr_80096a8:
	call scr_80097cc
	
scr_80096b0:
	call_if_attacked scr_80092ac
	cmd_D3 0x0, 0x1, scr_8009788
	wait_frame 
	goto scr_80096b0
	
scr_80096d0:
	goto_if_rng scr_8009704, 0x32
	call scr_80097ac
	
scr_80096e4:
	call_if_attacked scr_80092ac
	cmd_D3 0x1, 0x2, scr_8009788
	wait_frame 
	goto scr_80096e4
	
scr_8009704:
	call scr_80097cc
	
scr_800970c:
	call_if_attacked scr_80092ac
	cmd_D3 0x0, 0x2, scr_8009788
	wait_frame 
	goto scr_800970c
	
scr_800972c:
	goto_if_rng scr_8009760, 0x32
	call scr_80097ac
	
scr_8009740:
	call_if_attacked scr_80092ac
	cmd_D3 0x1, 0x4, scr_8009788
	wait_frame 
	goto scr_8009740
	
scr_8009760:
	call scr_80097cc
	
scr_8009768:
	call_if_attacked scr_80092ac
	cmd_D3 0x0, 0x4, scr_8009788
	wait_frame 
	goto scr_8009768
	
scr_8009788:
	callasm (sub_8021A0C+1)
	callasm (sub_8021960+1)
	goto scr_8008ff4
	call scr_80092ac
	return 
	
scr_80097ac:
	play_sound 0x18a
	set_anim 0x19
	call scr_8009820
	goto scr_80097ec
	
scr_80097cc:
	play_sound 0x18a
	set_anim 0x17
	call scr_8009820
	goto scr_80097ec
	
scr_80097ec:
	SetBit4Field44 
	disable_collision 
	callasm (sub_8021BB0+1)
	clear_local 
	
scr_8009800:
	for scr_8009818, 0x2d
	wait_frame 
	goto scr_8009800
	
scr_8009818:
	hidesprite 
	return 
	
scr_8009820:
	call_if_attacked scr_80092ac
	goto_if_anim_done scr_800983c
	wait_frame 
	goto scr_8009820
	
scr_800983c:
	return 
	
scr_8009840:
	ClearHighBit 0x3
	ClearHighBit 0x2
	call scr_80090e0
	callasm (sub_8021A80+1)
	faceplayer 
	SetHighBit 0x1
	faceplayer 
	call scr_8009114
	set_anim 0x10
	SetBit4Field44 
	
scr_8009884:
	goto_if_attacked scr_80098cc
	goto_if_anim_done scr_80098a0
	wait_frame 
	goto scr_8009884
	
scr_80098a0:
	set_anim 0x14
	call scr_8009a20
	
scr_80098b0:
	goto_if_attacked scr_80098cc
	call scr_8009a60
	wait_frame 
	goto scr_80098b0
	
scr_80098cc:
	ClearHighBit 0x1
	SetHighBit 0x2
	cmd_62 
	cmd_9C 
	cmd_D2 0x5a0059, 0x59
	play_sound_cond 0x17f, 0x180, 0x181
	cmd_6C scr_8009230
	set_anim 0x12
	SetBit4Field44 
	disable_collision 
	
scr_8009914:
	goto_if_anim_done scr_8009928
	wait_frame 
	goto scr_8009914
	
scr_8009928:
	ClearField62 
	goto scr_8008ffc
	
	
	
	
