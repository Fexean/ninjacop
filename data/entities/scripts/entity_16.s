.globl entity_16_scr
entity_16_scr:
	
	set_anim 0x23
	wait_frame 
	
scr_80024ac:
	call scr_80046ac
	load_rand 0x78
	add_local 0x3c
	
scr_80024c4:
	goto_if_attacked scr_8002508
	cmd_0E scr_80024ec, 0x0
	goto_if_not_player_x scr_8002524
	wait_frame 
	goto scr_80024c4
	
scr_80024ec:
	call scr_8004ef0
	call scr_8004d64
	wait_frame 
	goto scr_80024ac
	
scr_8002508:
	call scr_80040dc
	set_anim 0x23
	wait_frame 
	goto scr_80024c4
	
scr_8002524:
	toggle_dir 
	goto scr_8002548
	
scr_8002530:
	call scr_80046ac
	load_rand 0x78
	add_local 0x3c
	
scr_8002548:
	goto_if_attacked scr_800258c
	goto_if_not_player_x scr_80025a8
	cmd_0E scr_8002570, 0x0
	wait_frame 
	goto scr_8002548
	
scr_8002570:
	call scr_8004e78
	call scr_8004afc
	wait_frame 
	goto scr_8002530
	
scr_800258c:
	call scr_80040dc
	set_anim 0x23
	wait_frame 
	goto scr_8002548
	
scr_80025a8:
	toggle_dir 
	goto scr_80024c4
	
	
	
	
