.globl entity_8b_scr
entity_8b_scr:
	
	SetBit0Init52 
	wait_frame 
	cmd_A7 0x61
	set_dir 0x1
	
scr_8008744:
	ClearBit4Field44 
	enable_collision 
	callasm (sub_80215A4+1)
	cmd_6C scr_80087dc
	BranchHighBit scr_8008788, 0x5
	call_if_attacked scr_800878c
	branchIfParentHighFlag scr_80087a4, 0xb
	wait_frame 
	goto scr_8008744
	
scr_8008788:
	destroy 
	
scr_800878c:
	cmd_BC 0x58, 0x0, 0x0
	play_sound 0xf1
	return 
	
scr_80087a4:
	SetBit4Field44 
	reset_velocity 
	branchNotParentHighFlag scr_80087c4, 0xb
	wait_frame 
	goto scr_80087a4
	
scr_80087c4:
	ClearBit4Field44 
	callasm (sub_80215A4+1)
	wait_frame 
	goto scr_8008744
	
scr_80087dc:
	goto scr_8008878
	
scr_80087e4:
	call scr_80088a4
	callasm (sub_8021814+1)
	cmd_62 
	cmd_9C 
	cmd_6C scr_8008868
	return 
	SetBit4Field44 
	SetHighBit 0xb
	cmd_A8 
	clear_local 
	
scr_800881c:
	call_if_attacked scr_8008850
	for scr_800883c, 0x3c
	wait_frame 
	goto scr_800881c
	
scr_800883c:
	ClearBit4Field44 
	VelocityInit20_24 
	ClearHighBit 0xb
	return 
	
scr_8008850:
	cmd_BC 0x58, 0x0, 0x0
	play_sound 0xf1
	return 
	
scr_8008868:
	setParentHighFlag 0x4
	play_sound 0x188
	
scr_8008878:
	SetBit4Field44 
	disable_collision 
	SetBit0Init52 
	callasm (sub_80217A0+1)
	
scr_800888c:
	goto_if_offscreen scr_80088a0
	wait_frame 
	goto scr_800888c
	
scr_80088a0:
	destroy 
	
scr_80088a4:
	cmd_D2 0x5a0059, 0x59
	play_sound_cond 0x17f, 0x180, 0x181
	return 
	
	
	
	
