.globl entity_8e_scr
entity_8e_scr:
	
	SetBit4Field44 
	disable_collision 
	set_dir 0x1
	play_sound 0x141
	BranchIfParentId 0x9, scr_800151c
	BranchIfParentId 0xb, scr_800151c
	switch_language scr_80014dc, scr_80014ec, scr_80014fc
	
scr_80014dc:
	set_anim 0x0
	goto scr_8001504
	
scr_80014ec:
	set_anim 0x1
	goto scr_8001504
	
scr_80014fc:
	set_anim 0x2
	
scr_8001504:
	goto_if_anim_done scr_8001518
	wait_frame 
	goto scr_8001504
	
scr_8001518:
	destroy 
	
scr_800151c:
	switch_language scr_800152c, scr_800153c, scr_800154c
	
scr_800152c:
	set_anim 0x3
	goto scr_8001554
	
scr_800153c:
	set_anim 0x4
	goto scr_8001554
	
scr_800154c:
	set_anim 0x5
	
scr_8001554:
	goto_if_anim_done scr_8001568
	wait_frame 
	goto scr_8001554
	
scr_8001568:
	destroy 
	
	
	
	
