.globl entity_90_scr
entity_90_scr:
	
	faceplayer 
	hidesprite 
	disable_collision 
	SetBit4Field44 
	
scr_8009944:
	branchIfParentHighFlag scr_800995c, 0x1
	wait_frame 
	goto scr_8009944
	
scr_800995c:
	set_anim 0xf
	showsprite 
	
scr_8009968:
	goto_if_anim_done scr_800997c
	wait_frame 
	goto scr_8009968
	
scr_800997c:
	set_anim 0x10
	enable_collision 
	
scr_8009988:
	branchNotParentHighFlag scr_80099f0, 0x1
	goto_if_attacked scr_80099e8
	goto_if_anim_done scr_80099b0
	wait_frame 
	goto scr_8009988
	
scr_80099b0:
	set_anim 0x14
	call scr_8009a20
	
scr_80099c0:
	branchNotParentHighFlag scr_80099f0, 0x1
	goto_if_attacked scr_80099e8
	call scr_8009a60
	wait_frame 
	goto scr_80099c0
	
scr_80099e8:
	play_sound 0x18c
	
scr_80099f0:
	SetBit4Field44 
	disable_collision 
	SetHighBit 0x2
	set_anim 0xe
	
scr_8009a08:
	goto_if_anim_done scr_8009a1c
	wait_frame 
	goto scr_8009a08
	
scr_8009a1c:
	destroy 
	
scr_8009a20:
	set_anim 0x14
	SwitchDifficulty scr_8009a38, scr_8009a38, scr_8009a4c
	
scr_8009a38:
	load_rand 0x3c
	add_local 0x5a
	return 
	
scr_8009a4c:
	load_rand 0x3c
	add_local 0x3c
	return 
	
scr_8009a60:
	branchIfAnimation 0x15, scr_8009a7c
	cmd_0E scr_8009aa8, 0x28
	return 
	
scr_8009a7c:
	cmd_0E scr_8009a8c, 0x0
	return 
	
scr_8009a8c:
	create_child_ent 0x1, 0x1d, 0x0, 0x0, 0x6d
	goto scr_8009a20
	
scr_8009aa8:
	set_anim 0x15
	return 
	
	
	
	
