.globl entity_6d_scr
entity_6d_scr:
	
	play_sound 0x15a
	callasm (sub_8021B44+1)
	
scr_8009ac4:
	branchIfParentHighFlag scr_8009af4, 0x2
	goto_if_attacked scr_8009af4
	goto_if_field44_bit6 scr_8009b04
	goto_if_collide scr_8009b04
	wait_frame 
	goto scr_8009ac4
	
scr_8009af4:
	set_anim 0x1
	goto scr_8009b0c
	
scr_8009b04:
	set_anim 0x2
	
scr_8009b0c:
	reset_velocity 
	SetBit4Field44 
	disable_collision 
	
scr_8009b18:
	goto_if_anim_done scr_8009b2c
	wait_frame 
	goto scr_8009b18
	
scr_8009b2c:
	destroy 
	
scr_8009b30:
	hidesprite 
	freeze_player 
	wait_frame 
	showsprite 
	set_anim 0xf
	
scr_8009b48:
	goto_if_anim_done scr_8009b5c
	wait_frame 
	goto scr_8009b48
	
scr_8009b5c:
	play_sound 0x18a
	set_anim 0x10
	
scr_8009b6c:
	goto_if_anim_done scr_8009b80
	wait_frame 
	goto scr_8009b6c
	
scr_8009b80:
	set_anim 0x11
	
scr_8009b88:
	cmd_D3 0x0, 0x3, scr_8009ba0
	wait_frame 
	goto scr_8009b88
	
scr_8009ba0:
	set_anim 0x13
	play_sound 0x178
	call scr_80090e0
	SetInitPos 0xc8, 0x10
	faceplayer 
	call scr_8009114
	set_anim 0x0
	unfreeze_player 
	callasm (sub_8010D68+1)
	return 
	
	
	
	
