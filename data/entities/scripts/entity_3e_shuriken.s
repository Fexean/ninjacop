.globl entity_3e_shuriken_scr
entity_3e_shuriken_scr:

SetHighBit 0xa

shuriken_loop:
	goto_if_offscreen shuriken_offscreen
	goto_if_collide shuriken_stuck_on_wall
	goto_if_field44_bit4 shuriken_destroy	@enemy collision
	wait_frame 
	goto shuriken_loop

shuriken_stuck_on_wall:
	goto_if_offset_collision shuriken_offscreen, 0x8, 0x0, 0xffff
	set_anim 0x1
	SetBit4Field44 @remove hitbox
	wait_frames 0x1e

shuriken_offscreen:
	hidesprite 
	SetBit4Field44 
	disable_collision 
	wait_frames 0x1
	destroy 

shuriken_destroy:
	destroy 




