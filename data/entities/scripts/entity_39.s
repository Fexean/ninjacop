.globl entity_39_scr
entity_39_scr:
	
	call scr_8007cb0
	goto scr_8007004
	wait_frame 
	callasm (sub_8010D68+1)
	
scr_8007004:
	wait_frame 
	
scr_8007008:
	call scr_80070ec
	goto_if_asm (sub_8020AB8+1), scr_8007088
	goto_if_asm (sub_801FFF4+1), scr_8007048
	goto_if_asm (sub_8020020+1), scr_80070a4
	goto_if_not_player_x scr_80070d0
	wait_frame 
	goto scr_8007008
	
scr_8007048:
	call scr_8007ab0
	goto_if_difficulty 0x0, scr_800707c
	goto_if_difficulty 0x1, scr_800707c
	goto_if_rng scr_800707c, 0x28
	call scr_8007ab0
	
scr_800707c:
	wait_frame 
	goto scr_8007008
	
scr_8007088:
	call scr_8007558
	call scr_800765c
	wait_frame 
	goto scr_8007008
	
scr_80070a4:
	goto_if_y_between 0x8, 0x18, scr_80070bc
	wait_frame 
	goto scr_8007008
	
scr_80070bc:
	call scr_8007ad4
	wait_frame 
	goto scr_8007008
	
scr_80070d0:
	goto_if_far_player_y scr_80070e0, 0x20
	toggle_dir 
	
scr_80070e0:
	wait_frame 
	goto scr_8007008
	
scr_80070ec:
	set_anim2 0x1
	clear_local 
	goto_if_not_ground scr_8007374
	
scr_8007100:
	call_if_attacked scr_8007490
	BranchHighBit scr_80071cc, 0x0
	for scr_800712c, 0xc
	wait_frame 
	goto scr_8007100
	
scr_800712c:
	set_anim 0x2
	cmd_3D_setVelXDirBased 0x80
	cmd_C0 scr_8007150, 0x33
	cmd_3D_setVelXDirBased 0x100
	
scr_8007150:
	call_if_attacked scr_8007490
	BranchHighBit scr_80071cc, 0x0
	goto_if_not_ground scr_8007374
	goto_if_anim_done scr_800718c
	cmd_71 scr_80071d8, 0x11
	wait_frame 
	goto scr_8007150
	
scr_800718c:
	set_anim 0x1
	set_vel_x 0x0
	cmd_71 scr_80071d8, 0x11
	cmd_73 scr_8007264, 0xffff
	cmd_73 scr_80072f4, 0x10
	goto_if_not_ground scr_8007374
	return 
	
scr_80071cc:
	ClearHighBit 0x0
	return 
	
scr_80071d8:
	set_vel_x 0x0
	call scr_8007788
	BranchHighBit scr_8007258, 0x0
	call scr_800779c
	cmd_3D_setVelXDirBased 0x200
	set_vel_y_if_ground 0x5
	wait_frame 
	
scr_8007210:
	call_if_attacked scr_8007490
	BranchHighBit scr_8007258, 0x0
	goto_if_ground scr_8007238
	wait_frame 
	goto scr_8007210
	
scr_8007238:
	set_vel_x 0x0
	call scr_80077b0
	ClearHighBit 0x0
	toggle_dir 
	return 
	
scr_8007258:
	ClearHighBit 0x0
	return 
	
scr_8007264:
	toggle_dir 
	call scr_8007788
	BranchHighBit scr_80072e8, 0x0
	call scr_800779c
	cmd_3D_setVelXDirBased 0x200
	set_vel_y_if_ground 0x7
	cmd_26 0x20, 0x700
	wait_frame 
	
scr_80072a4:
	call_if_attacked scr_8007490
	BranchHighBit scr_80072e8, 0x0
	goto_if_ground scr_80072cc
	wait_frame 
	goto scr_80072a4
	
scr_80072cc:
	set_vel_x 0x0
	cmd_26 0x0, 0x700
	call scr_80077b0
	
scr_80072e8:
	ClearHighBit 0x0
	return 
	
scr_80072f4:
	call scr_8007788
	BranchHighBit scr_8007368, 0x0
	call scr_800779c
	cmd_3D_setVelXDirBased 0x100
	set_vel_y_if_ground 0x6
	wait_frame 
	
scr_8007324:
	call_if_attacked scr_8007490
	BranchHighBit scr_8007368, 0x0
	goto_if_ground scr_800734c
	wait_frame 
	goto scr_8007324
	
scr_800734c:
	set_vel_x 0x0
	call scr_80077b0
	ClearHighBit 0x0
	return 
	
scr_8007368:
	ClearHighBit 0x0
	return 
	
scr_8007374:
	call_if_attacked scr_8007490
	BranchHighBit scr_80073ac, 0x0
	goto_if_ground scr_800739c
	wait_frame 
	goto scr_8007374
	
scr_800739c:
	set_vel_x 0x0
	call scr_80077b0
	
scr_80073ac:
	ClearHighBit 0x0
	return 
	
scr_80073b8:
	ClearHighBit 0x0
	return 
	
scr_80073c4:
	SetBit4Field44 
	ClearBit7Field44 
	kill_parent 
	goto_if_player_x scr_80073dc
	toggle_dir 
	
scr_80073dc:
	call scr_8007464
	cmd_3D_setVelXDirBased 0xfffffe00
	cmd_3E_Init30_28_ReverseMe 0x10, 0x700
	clear_local 
	
scr_80073fc:
	for scr_8007414, 0x20
	wait_frame 
	goto scr_80073fc
	
scr_8007414:
	cmd_3E_Init30_28_ReverseMe 0x0, 0x700
	enable_collision 
	ClearBit4Field44 
	SetHighBit 0x0
	return 
	ClearBit7Field44 
	cmd_62 
	cmd_9C 
	play_sound_cond 0x15c, 0x15d, 0x15e
	cmd_D2 0x5a0059, 0x59
	cmd_6C scr_80074d4
	return 
	
scr_8007464:
	goto_if_not_player_x scr_8007484
	goto_if_near_player_y scr_8007484, 0x1c
	set_anim 0x17
	return 
	
scr_8007484:
	set_anim 0x18
	return 
	
scr_8007490:
	play_sound_cond 0x15c, 0x15d, 0x15e
	cmd_D2 0x5a0059, 0x59
	cmd_62 
	cmd_9C 
	ClearBit7Field44 
	cmd_6C scr_80074d4
	cmd_DA unk_82BF1C4
	ifChildFlag21 scr_80073b8
	goto scr_80073c4
	
scr_80074d4:
	callasm (sub_8033184+1)
	kill_parent 
	SetBit4Field44 
	disable_collision 
	call scr_8007464
	set_vel_x 0x0
	set_vel_y 0x0
	cmd_25 0x0, 0x700
	cmd_26 0x0, 0x700
	
scr_8007518:
	goto_if_ground scr_800752c
	wait_frame 
	goto scr_8007518
	
scr_800752c:
	set_anim 0x16
	
scr_8007534:
	goto_if_anim_done scr_8007548
	wait_frame 
	goto scr_8007534
	
scr_8007548:
	complete_lvl 
	
scr_800754c:
	wait_frame 
	goto scr_800754c
	
scr_8007558:
	ClearBit7Field44 
	set_vel_x 0x0
	set_anim 0xf
	
scr_800756c:
	call_if_attacked scr_8007640
	goto_if_anim_done scr_8007588
	wait_frame 
	goto scr_800756c
	
scr_8007588:
	play_sound 0x155
	set_anim 0x10
	callasm (sub_80210E8+1)
	SetBit0Init52 
	clear_local 
	
scr_80075a8:
	call_if_attacked scr_8007640
	for scr_80075c8, 0x1e
	wait_frame 
	goto scr_80075a8
	
scr_80075c8:
	cmd_25 0x0, 0x700
	cmd_26 0x0, 0x700
	set_vel_x 0x0
	set_vel_y 0x0
	ClearBit0Init52 
	
scr_80075f4:
	call_if_attacked scr_8007640
	goto_if_ground scr_8007610
	wait_frame 
	goto scr_80075f4
	
scr_8007610:
	play_sound 0x156
	set_anim 0x11
	
scr_8007620:
	call_if_attacked scr_8007640
	goto_if_anim_done scr_800763c
	wait_frame 
	goto scr_8007620
	
scr_800763c:
	return 
	
scr_8007640:
	play_sound_cond 0xf1, 0xf3, 0xf3
	cmd_D2 0x580058, 0x58
	return 
	
scr_800765c:
	play_sound 0x159
	set_anim 0x13
	call scr_8007a48
	BranchHighBit scr_800773c, 0x0
	set_anim 0x14
	clear_local 
	call scr_8005584
	goto_if_difficulty 0x0, scr_80076a8
	goto scr_80076dc
	
scr_80076a8:
	call_if_attacked scr_8007490
	BranchHighBit scr_800773c, 0x0
	for scr_8007710, 0x168
	call scr_8007748
	wait_frame 
	goto scr_80076a8
	
scr_80076dc:
	call_if_attacked scr_8007490
	BranchHighBit scr_800773c, 0x0
	for scr_8007710, 0x1e0
	call scr_8007748
	wait_frame 
	goto scr_80076dc
	
scr_8007710:
	set_anim 0x15
	call scr_8007a48
	BranchHighBit scr_800773c, 0x0
	set_anim 0x19
	goto scr_8007bb4
	
scr_800773c:
	ClearHighBit 0x0
	return 
	
scr_8007748:
	goto_if_difficulty 0x2, scr_8007764
	for scr_8007774, 0xf
	return 
	
scr_8007764:
	for scr_8007774, 0xa
	return 
	
scr_8007774:
	play_sound 0x15a
	callasm (sub_8020B10+1)
	return 
	
scr_8007788:
	set_anim 0x3
	call scr_8007a48
	return 
	
scr_800779c:
	play_sound 0x155
	set_anim 0x4
	return 
	
scr_80077b0:
	play_sound 0x156
	set_anim 0x5
	call scr_8007a48
	return 
	
scr_80077cc:
	set_anim 0x6
	call scr_8007a48
	BranchHighBit scr_8007890, 0x0
	cmd_49 0x8, 0x2c, 0x5d
	set_anim 0x7
	set_local 0x14
	goto_if_difficulty 0x0, scr_8007818
	set_local 0xa
	
scr_8007818:
	call scr_8007a74
	BranchHighBit scr_8007890, 0x0
	set_anim 0x8
	set_local 0x2
	call scr_8007a74
	BranchHighBit scr_8007890, 0x0
	kill_parent 
	play_sound 0x157
	cmd_49 0x28, 0x2c, 0x5e
	set_local 0x1e
	call scr_8007a74
	BranchHighBit scr_8007890, 0x0
	kill_parent 
	call scr_8007a48
	
scr_8007890:
	kill_parent 
	ClearHighBit 0x0
	return 
	
scr_80078a0:
	set_anim 0x9
	call scr_8007a48
	BranchHighBit scr_8007964, 0x0
	cmd_49 0x8, 0x14, 0x5d
	set_anim 0xa
	set_local 0x14
	goto_if_difficulty 0x0, scr_80078ec
	set_local 0xa
	
scr_80078ec:
	call scr_8007a74
	BranchHighBit scr_8007964, 0x0
	set_anim 0xb
	set_local 0x2
	BranchHighBit scr_8007964, 0x0
	call scr_8007a74
	kill_parent 
	play_sound 0x157
	cmd_49 0x28, 0x14, 0x5e
	set_local 0x1e
	call scr_8007a74
	BranchHighBit scr_8007964, 0x0
	kill_parent 
	call scr_8007a48
	
scr_8007964:
	kill_parent 
	ClearHighBit 0x0
	return 
	
scr_8007974:
	set_anim 0xc
	call scr_8007a48
	BranchHighBit scr_8007a38, 0x0
	cmd_49 0x8, 0xc, 0x5d
	set_anim 0xd
	set_local 0x14
	goto_if_difficulty 0x0, scr_80079c0
	set_local 0xa
	
scr_80079c0:
	call scr_8007a74
	BranchHighBit scr_8007a38, 0x0
	set_anim 0xe
	set_local 0x2
	call scr_8007a74
	BranchHighBit scr_8007a38, 0x0
	kill_parent 
	play_sound 0x157
	cmd_49 0x28, 0xc, 0x5e
	set_local 0x1e
	call scr_8007a74
	BranchHighBit scr_8007a38, 0x0
	kill_parent 
	call scr_8007a48
	
scr_8007a38:
	kill_parent 
	ClearHighBit 0x0
	return 
	
scr_8007a48:
	call_if_attacked scr_8007490
	BranchHighBit scr_8007a70, 0x0
	goto_if_anim_done scr_8007a70
	wait_frame 
	goto scr_8007a48
	
scr_8007a70:
	return 
	
scr_8007a74:
	call_if_attacked scr_8007490
	BranchHighBit scr_8007aac, 0x0
	DecStack 
	cmd_0E scr_8007aa8, 0x0
	IncStack 
	wait_frame 
	goto scr_8007a74
	
scr_8007aa8:
	IncStack 
	
scr_8007aac:
	return 
	
scr_8007ab0:
	set_vel_x 0x0
	goto_if_far_player_y scr_80077cc, 0x20
	cmd_94 scr_8007974
	goto scr_80078a0
	
scr_8007ad4:
	set_vel_x 0x0
	set_anim 0xf
	ClearHighBit 0x1
	call scr_8007a48
	BranchHighBit scr_8007b6c, 0x0
	play_sound 0x155
	set_anim 0x10
	set_vel_y_if_ground 0x6
	cmd_49 0x0, 0x49, 0x5e
	wait_frame 
	
scr_8007b28:
	call_if_attacked scr_8007490
	BranchHighBit scr_8007b6c, 0x0
	goto_if_ground scr_8007b50
	wait_frame 
	goto scr_8007b28
	
scr_8007b50:
	kill_parent 
	play_sound 0x156
	set_anim 0x11
	call scr_8007a48
	
scr_8007b6c:
	kill_parent 
	ClearHighBit 0x0
	BranchHighBit scr_8007b88, 0x1
	return 
	
scr_8007b88:
	set_anim 0x12
	call scr_8007a48
	BranchHighBit scr_8007b6c, 0x0
	set_anim 0x1a
	goto scr_8007bb4
	
scr_8007bb4:
	set_local 0x1c
	call scr_8007a74
	BranchHighBit scr_8007c1c, 0x0
	play_sound 0x195
	call scr_8007a48
	BranchHighBit scr_8007c1c, 0x0
	play_sound 0x154
	set_anim 0x1b
	call scr_8007a48
	BranchHighBit scr_8007c1c, 0x0
	set_anim 0x0
	return 
	
scr_8007c1c:
	return 
	
	
	
	
