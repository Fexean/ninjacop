.globl entity_2c_scr
entity_2c_scr:
	
	wait_frame 
	clear_local 
	
scr_8006834:
	call_if_attacked scr_8006a10
	goto_if_asm (sub_8020030+1), scr_80068ac
	
scr_8006848:
	call scr_80068fc
	BranchHighBit scr_8006874, 0x3
	BranchHighBit scr_8006890, 0x0
	wait_frame 
	goto scr_8006834
	
scr_8006874:
	ClearHighBit 0x3
	goto_if_not_player_x scr_8006890
	wait_frame 
	goto scr_8006834
	
scr_8006890:
	ClearHighBit 0x0
	call scr_8006ae0
	wait_frame 
	goto scr_8006834
	
scr_80068ac:
	goto_if_not_player_x scr_8006890
	goto_if_offscreen scr_8006848
	call scr_8006c70
	clear_local 
	
scr_80068c8:
	call_if_attacked scr_8006a10
	for scr_80068e8, 0x3c
	wait_frame 
	goto scr_80068c8
	
scr_80068e8:
	wait_frame 
	goto_if_player_x scr_8006848
	goto scr_8006834
	
scr_80068fc:
	BranchNotHighBit scr_800691c, 0x3
	goto_if_not_player_x scr_8006960
	goto_if_asm (sub_8020030+1), scr_8006960
	
scr_800691c:
	call scr_8006964
	BranchHighBit scr_8006960, 0x0
	BranchNotHighBit scr_8006950, 0x3
	goto_if_not_player_x scr_8006960
	goto_if_asm (sub_8020030+1), scr_8006960
	
scr_8006950:
	ClearHighBit 0x3
	call scr_8006974
	
scr_8006960:
	return 
	
scr_8006964:
	set_anim 0x1
	goto scr_8006984
	
scr_8006974:
	set_anim 0x2
	goto scr_8006984
	
scr_8006984:
	setObjIdVelX 
	
scr_8006988:
	call_if_attacked scr_8006a10
	cmd_74 scr_80069bc
	cmd_75 scr_80069bc
	cmd_12 scr_80069bc
	goto_if_anim_done scr_80069d0
	wait_frame 
	goto scr_8006988
	
scr_80069bc:
	SetHighBit 0x0
	set_vel_x 0x0
	return 
	
scr_80069d0:
	set_vel_x 0x0
	clear_local 
	goto_if_offscreen scr_80069ec
	play_sound 0x135
	
scr_80069ec:
	call_if_attacked scr_8006a10
	for scr_8006a0c, 0x12
	wait_frame 
	goto scr_80069ec
	
scr_8006a0c:
	return 
	
scr_8006a10:
	ClearBit7Field44 
	call scr_8006a94
	callasm (setObjCodeBlink+1)
	cmd_60 
	cmd_6C scr_8006a3c
	SetHighBit 0x3
	return 
	
scr_8006a3c:
	play_sound 0x11c
	SetHighBit 0x2
	set_vel_x 0x0
	disable_collision 
	SetBit4Field44 
	kill 
	cmd_48 0x0, 0x0, 0x0, 0x0, 0x75
	set_anim 0x7
	
scr_8006a7c:
	goto_if_anim_done scr_8006a90
	wait_frame 
	goto scr_8006a7c
	
scr_8006a90:
	destroy 
	
scr_8006a94:
	play_sound_cond 0x13c, 0x13d, 0x13d
	cmd_D2 0x5a0059, 0x59
	return 
	cmd_BC 0x58, 0x0, 0x0
	ifChildFlag21 scr_8006ad4
	play_sound 0xf3
	goto scr_8006adc
	
scr_8006ad4:
	play_sound 0xf1
	
scr_8006adc:
	return 
	
scr_8006ae0:
	set_vel_x 0x0
	set_anim 0x6
	call scr_8006c50
	toggle_dir 
	return 
	
scr_8006b00:
	set_vel_x 0x0
	set_anim 0x3
	call scr_8006c50
	LoadDifficulty 0x1e, 0xf, 0x1
	
scr_8006b24:
	call_if_attacked scr_8006a10
	cmd_0E scr_8006b44, 0x0
	wait_frame 
	goto scr_8006b24
	
scr_8006b44:
	call scr_8006b58
	set_anim 0x5
	return 
	
scr_8006b58:
	callasm (sub_8020E8C+1)
	set_anim 0x4
	clear_local 
	
scr_8006b6c:
	call_if_attacked scr_8006a10
	for scr_8006b8c, 0x4
	wait_frame 
	goto scr_8006b6c
	
scr_8006b8c:
	play_sound 0x136
	callasm (sub_8020F3C+1)
	call scr_8006c50
	return 
	
scr_8006ba8:
	set_vel_x 0x0
	set_anim 0x3
	call scr_8006c50
	set_anim 0x5
	LoadDifficulty 0x1e, 0xa, 0x1
	
scr_8006bd4:
	call_if_attacked scr_8006a10
	cmd_0E scr_8006bf4, 0x0
	wait_frame 
	goto scr_8006bd4
	
scr_8006bf4:
	set_anim 0x4
	clear_local 
	
scr_8006c00:
	call_if_attacked scr_8006a10
	for scr_8006c20, 0x10
	wait_frame 
	goto scr_8006c00
	
scr_8006c20:
	cmd_48 0x10, 0x24, 0x0, 0x0, 0x54
	play_sound 0x13a
	callasm (sub_8020B5C+1)
	call scr_8006c50
	return 
	
scr_8006c50:
	call_if_attacked scr_8006a10
	goto_if_anim_done scr_8006c6c
	wait_frame 
	goto scr_8006c50
	
scr_8006c6c:
	return 
	
scr_8006c70:
	set_vel_x 0x0
	cmd_94 scr_8006d1c
	set_anim 0xc
	clear_local 
	
scr_8006c8c:
	call_if_attacked scr_8006a10
	for scr_8006cac, 0x16
	wait_frame 
	goto scr_8006c8c
	
scr_8006cac:
	play_sound 0x138
	cmd_48 0x12, 0x24, 0x400, 0x0, 0x52
	call scr_8006c50
	
scr_8006cd0:
	set_anim 0xd
	call_if_attacked scr_8006db8
	BranchHighBit scr_8006cf8, 0x1
	wait_frame 
	goto scr_8006cd0
	
scr_8006cf8:
	ClearHighBit 0x1
	set_anim 0xe
	call scr_8006c50
	set_anim 0x0
	return 
	
scr_8006d1c:
	set_anim 0x9
	clear_local 
	
scr_8006d28:
	call_if_attacked scr_8006a10
	for scr_8006d48, 0x23
	wait_frame 
	goto scr_8006d28
	
scr_8006d48:
	play_sound 0x138
	cmd_48 0x12, 0xc, 0x400, 0x0, 0x52
	call scr_8006c50
	
scr_8006d6c:
	set_anim 0xa
	call_if_attacked scr_8006db8
	BranchHighBit scr_8006d94, 0x1
	wait_frame 
	goto scr_8006d6c
	
scr_8006d94:
	ClearHighBit 0x1
	set_anim 0xb
	call scr_8006c50
	set_anim 0x0
	return 
	
scr_8006db8:
	call scr_8006a94
	callasm (setObjCodeBlink+1)
	cmd_60 
	cmd_6C scr_8006dd8
	return 
	
scr_8006dd8:
	SetHighBit 0x2
	set_vel_x 0x0
	disable_collision 
	SetBit4Field44 
	kill 
	cmd_48 0x0, 0x0, 0x0, 0x0, 0x75
	set_anim 0x8
	
scr_8006e10:
	goto_if_anim_done scr_8006e24
	wait_frame 
	goto scr_8006e10
	
scr_8006e24:
	hidesprite 
	
scr_8006e28:
	BranchHighBit scr_8006e40, 0x1
	wait_frame 
	goto scr_8006e28
	
scr_8006e40:
	destroy 
	
	
	
	
