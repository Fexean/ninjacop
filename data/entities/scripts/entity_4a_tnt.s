@animations:
@	0 = spinning
@	1 = horizontal
@	2 = diagonal
@	3 = vertical
@	4 = horizontal, lit
@	5 = glowing
@	6 = shatter

.globl entity_4a_scr
entity_4a_scr:
	
	
tnt_loop:
	goto_if_attacked tnt_shatter
	goto_if_field44_bit6 tnt_player_bounce
	goto_if_ground tnt_first_landing
	wait_frame 
	goto tnt_loop
	
tnt_first_landing:
	set_vel_y 0x280
	
tnt_hop:
	goto_if_attacked tnt_shatter
	goto_if_ground tnt_start_glowing
	wait_frame 
	goto tnt_hop
	
tnt_start_glowing:
	set_anim 0x5
	set_vel_x 0x0
	SetBit4Field44 
	
tnt_fuse:
	for tnt_explode, 0x5a
	goto_if_attacked tnt_shatter
	wait_frame 
	goto tnt_fuse
	
tnt_explode:
	play_sound 0x11c
	create_child_ent 0x0, 0x0, 0x0, 0x0, 0x4c
	destroy 
	
tnt_player_bounce:
	callasm (negateEntityXVelocity+1)
	wait_frame 
	goto tnt_hop
	
tnt_shatter:
	reset_velocity 
	SetInit4E 0x1
	set_anim 0x6
	
tnt_waitdeath:
	goto_if_anim_done scr_80034fc
	wait_frame 
	goto tnt_waitdeath
	
scr_80034fc:
	destroy 
	
	
	
	
