.globl entity_68_scr
entity_68_scr:
	
	SetBit4Field44 
	wait_frames 0x14
	goto scr_8003dc8
	
scr_8003d7c:
	set_anim 0x0
	clear_local 
	SwitchDifficulty scr_8003d98, scr_8003da8, scr_8003db8
	
scr_8003d98:
	wait_frames 0x140
	goto scr_8003dc8
	
scr_8003da8:
	wait_frames 0x50
	goto scr_8003dc8
	
scr_8003db8:
	wait_frames 0x5
	goto scr_8003dc8
	
scr_8003dc8:
	callasm (sub_8016138+1)
	wait_frames 0x28
	set_anim 0x1
	clear_local 
	wait_frames 0x1e
	play_sound 0x1aa
	set_anim 0x2
	clear_local 
	
scr_8003e00:
	goto_if_anim_done scr_8003e14
	wait_frame 
	goto scr_8003e00
	
scr_8003e14:
	ClearBit4Field44 
	set_anim 0x3
	clear_local 
	wait_frames 0x5a
	SetBit4Field44 
	set_anim 0x4
	
scr_8003e38:
	goto_if_anim_done scr_8003e4c
	wait_frame 
	goto scr_8003e38
	
scr_8003e4c:
	stop_sound 0x1aa
	callasm (sub_801618C+1)
	goto scr_8003d7c
	set_anim 0x0
	set_vel_x 0x0
	return 
	
scr_8003e78:
	setObjIdVelX 
	cmd_75 scr_8003ea0
	set_anim 0x1
	cmd_74 scr_8003ea0
	cmd_52 scr_8003ea0
	return 
	
scr_8003ea0:
	SetBit7Field44 
	return 
	
scr_8003ea8:
	cmd_93 scr_8003ee8
	cmd_91 
	cmd_75 scr_8003ee0
	cmd_74 scr_8003ee8
	cmd_71 scr_800450c, 0x0
	setObjIdVelX2 
	set_anim 0x24
	return 
	
scr_8003ee0:
	cmd_80 scr_800441c
	
scr_8003ee8:
	set_anim 0x22
	set_vel_x 0x0
	return 
	
scr_8003efc:
	setObjIdVelX3 
	cmd_75 scr_8003f28
	set_anim 0x25
	cmd_74 scr_8003f28
	cmd_71 scr_8003f28, 0x0
	return 
	
scr_8003f28:
	toggle_dir 
	set_anim 0x22
	set_vel_x 0x0
	return 
	
scr_8003f40:
	cmd_3C_modifyDir 
	setObjIdVelX 
	cmd_73 scr_80043a4, 0x10
	set_anim 0x1
	cmd_71 scr_8004494, 0x0
	return 
	
scr_8003f6c:
	call scr_80056dc
	set_vel_x 0x0
	set_vel_y 0x0
	cmd_60 
	cmd_6C scr_8004134
	ClearBit7Field44 
	SetBit4Field44 
	callasm (sub_80205D4+1)
	
scr_8003fa0:
	goto_if_anim_done scr_8003fbc
	goto_if_attacked scr_8003f6c
	wait_frame 
	goto scr_8003fa0
	
scr_8003fbc:
	ClearBit4Field44 
	return 
	
scr_8003fc4:
	call scr_80056dc
	set_vel_x 0x0
	set_vel_y 0x0
	cmd_60 
	cmd_3D_setVelXDirBased 0xffffff00
	set_vel_y 0x300
	cmd_6C scr_8004134
	ClearBit7Field44 
	SetBit4Field44 
	callasm (sub_80205D4+1)
	wait_frame 
	
scr_800400c:
	goto_if_attacked scr_8003fc4
	goto_if_anim_done scr_8004030
	goto_if_ground scr_8004038
	wait_frame 
	goto scr_800400c
	
scr_8004030:
	ClearBit4Field44 
	return 
	
scr_8004038:
	set_vel_x 0x0
	set_vel_y 0x0
	wait_frame 
	goto scr_800400c
	
scr_8004054:
	call scr_80056dc
	set_vel_x 0x0
	set_vel_y 0x0
	cmd_60 
	set_vel_y 0x300
	cmd_6C scr_8004134
	ClearBit7Field44 
	SetBit4Field44 
	callasm (sub_80205D4+1)
	wait_frame 
	
scr_8004094:
	goto_if_anim_done scr_80040b8
	goto_if_attacked scr_8004054
	goto_if_ground scr_80040c0
	wait_frame 
	goto scr_8004094
	
scr_80040b8:
	ClearBit4Field44 
	return 
	
scr_80040c0:
	set_vel_x 0x0
	set_vel_y 0x0
	wait_frame 
	goto scr_8004094
	
scr_80040dc:
	call scr_80056dc
	set_vel_x 0x0
	set_vel_y 0x0
	cmd_60 
	cmd_6C scr_8004134
	ClearBit7Field44 
	SetBit4Field44 
	callasm (sub_8020648+1)
	
scr_8004110:
	goto_if_attacked scr_80040dc
	goto_if_anim_done scr_800412c
	wait_frame 
	goto scr_8004110
	
scr_800412c:
	ClearBit4Field44 
	return 
	
scr_8004134:
	SetBit4Field44 
	disable_collision 
	kill 
	cmd_70 scr_80041b0, 0x13
	
scr_800414c:
	ifChildFlag21 scr_80041c4
	ifChildFlag23 scr_800419c
	branchIfChildHighFlag scr_800419c, 0x2
	branchIfChildHighFlag scr_8004188, 0x0
	goto_if_rng scr_80042bc, 0x1e
	goto scr_80041c4
	
scr_8004188:
	goto_if_rng scr_80042bc, 0x3c
	goto scr_80041c4
	
scr_800419c:
	goto_if_rng scr_80042ec, 0x3c
	goto scr_80041c4
	
scr_80041b0:
	goto_if_rng scr_800420c, 0x3c
	goto scr_800414c
	
scr_80041c4:
	callasm (sub_8020748+1)
	
scr_80041cc:
	goto_if_anim_done scr_80041e0
	wait_frame 
	goto scr_80041cc
	
scr_80041e0:
	cmd_92 
	
scr_80041e4:
	clear_local 
	callasm (setObjCodeBlink+1)
	
scr_80041f0:
	for scr_8004208, 0x1e
	wait_frame 
	goto scr_80041f0
	
scr_8004208:
	destroy 
	
scr_800420c:
	PlaySongObjID 0x11f, 0x12b
	set_anim 0x11
	
scr_800421c:
	goto_if_anim_done scr_8004230
	wait_frame 
	goto scr_800421c
	
scr_8004230:
	cmd_A6 0x1
	set_anim 0x12
	
scr_8004240:
	goto_if_anim_done scr_8004254
	wait_frame 
	goto scr_8004240
	
scr_8004254:
	cmd_92 
	SetBit0Init52 
	SetInit4E 0x0
	cmd_26 0x0, 0x400
	
scr_8004270:
	cmd_70 scr_8004288, 0x14
	wait_frame 
	goto scr_8004270
	
scr_8004288:
	PlaySongObjID 0x120, 0x12c
	ClearBit0Init52 
	set_anim 0x13
	
scr_800429c:
	goto_if_anim_done scr_80042b0
	wait_frame 
	goto scr_800429c
	
scr_80042b0:
	cmd_92 
	goto scr_80041e4
	
scr_80042bc:
	SetInit4E 0x1
	PlaySongObjID 0x11f, 0x12b
	set_anim 0x14
	goto_if_player_x scr_8004330
	set_anim 0x17
	goto scr_800431c
	
scr_80042ec:
	SetInit4E 0x1
	PlaySongObjID 0x11f, 0x12b
	set_anim 0x1a
	goto_if_player_x scr_8004330
	set_anim 0x1d
	goto scr_800431c
	
scr_800431c:
	cmd_D8 0x200020, 0x19
	goto scr_8004344
	
scr_8004330:
	cmd_D8 0x20ffe0, 0x18
	goto scr_8004344
	
scr_8004344:
	goto_if_anim_done scr_8004358
	wait_frame 
	goto scr_8004344
	
scr_8004358:
	next_anim 
	
scr_800435c:
	goto_if_ground scr_8004370
	wait_frame 
	goto scr_800435c
	
scr_8004370:
	cmd_92 
	PlaySongObjID 0x120, 0x12c
	reset_velocity 
	next_anim 
	
scr_8004384:
	goto_if_anim_done scr_8004398
	wait_frame 
	goto scr_8004384
	
scr_8004398:
	cmd_92 
	goto scr_80041e4
	
scr_80043a4:
	cmd_70 scr_8004418, 0x0
	set_vel_x 0x0
	set_vel_y 0x0
	set_anim 0x3
	SetInit4E 0x1
	clear_local 
	clearLateFields 
	
scr_80043d8:
	goto_if_asm (sub_802044C+1), scr_80043f8
	call_if_attacked scr_8004584
	wait_frame 
	goto scr_80043d8
	
scr_80043f8:
	SetInit4E 0x0
	return 
	SetInit4E 0x0
	clear_local 
	goto scr_8003fc4
	
scr_8004418:
	return 
	
scr_800441c:
	cmd_70 scr_8004490, 0x0
	set_vel_x 0x0
	set_vel_y 0x0
	set_anim 0x5
	SetInit4E 0x1
	clear_local 
	clearLateFields 
	
scr_8004450:
	goto_if_asm (sub_802044C+1), scr_8004470
	call_if_attacked scr_8004584
	wait_frame 
	goto scr_8004450
	
scr_8004470:
	SetInit4E 0x0
	return 
	SetInit4E 0x0
	clear_local 
	goto scr_8003fc4
	
scr_8004490:
	return 
	
scr_8004494:
	cmd_70 scr_8004508, 0x0
	set_vel_x 0x0
	set_vel_y 0x0
	set_anim 0x4
	SetInit4E 0x1
	clear_local 
	clearLateFields 
	
scr_80044c8:
	goto_if_asm (sub_8020510+1), scr_80044e8
	call_if_attacked scr_8004584
	wait_frame 
	goto scr_80044c8
	
scr_80044e8:
	SetInit4E 0x0
	return 
	SetInit4E 0x0
	clear_local 
	goto scr_8004054
	
scr_8004508:
	return 
	
scr_800450c:
	cmd_70 scr_8004580, 0x0
	set_vel_x 0x0
	set_vel_y 0x0
	set_anim 0x6
	SetInit4E 0x1
	clear_local 
	clearLateFields 
	
scr_8004540:
	goto_if_asm (sub_8020510+1), scr_8004560
	call_if_attacked scr_8004584
	wait_frame 
	goto scr_8004540
	
scr_8004560:
	SetInit4E 0x0
	return 
	SetInit4E 0x0
	clear_local 
	goto scr_8004054
	
scr_8004580:
	return 
	
scr_8004584:
	call scr_80056dc
	cmd_60 
	cmd_6C scr_8004134
	return 
	
scr_800459c:
	set_vel_x 0x0
	set_anim2 0x26
	
scr_80045ac:
	goto_if_anim_done scr_80045c8
	goto_if_attacked scr_80045dc
	wait_frame 
	goto scr_80045ac
	
scr_80045c8:
	toggle_dir 
	set_anim 0x24
	setObjIdVelX2 
	return 
	
scr_80045dc:
	call scr_80040dc
	goto scr_80045c8
	
scr_80045ec:
	set_vel_x 0x0
	set_anim 0x20
	goto_if_anim_done scr_8004618
	goto_if_attacked scr_8004624
	wait_frame 
	goto scr_80045ec
	
scr_8004618:
	set_anim 0x22
	return 
	
scr_8004624:
	call scr_80040dc
	goto scr_8004618
	
scr_8004634:
	set_vel_x 0x0
	set_anim 0x21
	goto_if_anim_done scr_8004660
	goto_if_attacked scr_8004664
	wait_frame 
	goto scr_8004634
	
scr_8004660:
	return 
	
scr_8004664:
	goto scr_80040dc
	
scr_800466c:
	set_anim 0x2a
	
scr_8004674:
	goto_if_attacked scr_8004698
	goto_if_anim_done scr_8004690
	wait_frame 
	goto scr_8004674
	
scr_8004690:
	wait_frame 
	return 
	
scr_8004698:
	call scr_80040dc
	set_anim 0x22
	return 
	
scr_80046ac:
	set_anim 0x2b
	
scr_80046b4:
	goto_if_attacked scr_80046dc
	goto_if_anim_done scr_80046d0
	wait_frame 
	goto scr_80046b4
	
scr_80046d0:
	set_anim 0x23
	return 
	
scr_80046dc:
	call scr_80040dc
	goto scr_80046d0
	
scr_80046ec:
	set_anim 0x2d
	
scr_80046f4:
	goto_if_attacked scr_8004714
	goto_if_anim_done scr_8004710
	wait_frame 
	goto scr_80046f4
	
scr_8004710:
	return 
	
scr_8004714:
	call scr_80040dc
	return 
	
scr_8004720:
	set_anim 0x2e
	
scr_8004728:
	goto_if_attacked scr_8004748
	goto_if_anim_done scr_8004744
	wait_frame 
	goto scr_8004728
	
scr_8004744:
	return 
	
scr_8004748:
	call scr_80040dc
	return 
	
scr_8004754:
	goto_if_onscreen scr_8004764
	wait_frame 
	return 
	
scr_8004764:
	set_anim2 0x27
	clear_local 
	
scr_8004770:
	goto_if_attacked scr_80047cc
	for scr_8004790, 0x1a
	wait_frame 
	goto scr_8004770
	
scr_8004790:
	play_sound 0x118
	cmd_48 0x11, 0x16, 0x0, 0x0, 0x45
	
scr_80047ac:
	goto_if_attacked scr_80047cc
	goto_if_anim_done scr_80047c8
	wait_frame 
	goto scr_80047ac
	
scr_80047c8:
	return 
	
scr_80047cc:
	goto scr_80040dc
	
scr_80047d4:
	goto_if_onscreen scr_80047e4
	wait_frame 
	return 
	
scr_80047e4:
	set_anim2 0x28
	
scr_80047ec:
	goto_if_attacked scr_8004848
	for scr_800480c, 0x10
	wait_frame 
	goto scr_80047ec
	
scr_800480c:
	play_sound 0x118
	cmd_48 0x11, 0xe, 0x0, 0x0, 0x45
	
scr_8004828:
	goto_if_attacked scr_8004848
	goto_if_anim_done scr_8004844
	wait_frame 
	goto scr_8004828
	
scr_8004844:
	return 
	
scr_8004848:
	goto scr_80040dc
	
scr_8004850:
	goto_if_onscreen scr_8004860
	wait_frame 
	return 
	
scr_8004860:
	set_anim2 0x29
	clear_local 
	
scr_800486c:
	goto_if_attacked scr_80048cc
	for scr_800488c, 0x10
	wait_frame 
	goto scr_800486c
	
scr_800488c:
	play_sound 0x119
	cmd_48 0x4, 0x23, 0x0, 0x300, 0x46
	
scr_80048a8:
	goto_if_attacked scr_80048d4
	goto_if_anim_done scr_80048c4
	wait_frame 
	goto scr_80048a8
	
scr_80048c4:
	goto scr_80045ec
	
scr_80048cc:
	goto scr_80040dc
	
scr_80048d4:
	goto scr_8003f6c
	
scr_80048dc:
	goto_if_onscreen scr_80048ec
	wait_frame 
	return 
	
scr_80048ec:
	set_anim2 0x2f
	clear_local 
	
scr_80048f8:
	goto_if_attacked scr_8004958
	for scr_8004918, 0x1e
	wait_frame 
	goto scr_80048f8
	
scr_8004918:
	play_sound 0x119
	cmd_48 0x12, 0x16, 0x300, 0x0, 0x47
	
scr_8004934:
	goto_if_attacked scr_8004960
	goto_if_anim_done scr_8004950
	wait_frame 
	goto scr_8004934
	
scr_8004950:
	goto scr_80045ec
	
scr_8004958:
	goto scr_80040dc
	
scr_8004960:
	goto scr_8003f6c
	
scr_8004968:
	goto_if_onscreen scr_8004978
	wait_frame 
	return 
	
scr_8004978:
	cmd_76 0x150015, scr_80049c8
	set_anim2 0x27
	play_sound 0x11a
	BranchID2 scr_80049b4
	cmd_48 0x15, 0x16, 0x300, 0x0, 0x48
	return 
	
scr_80049b4:
	cmd_48 0xe, 0x14, 0x300, 0x0, 0x48
	
scr_80049c8:
	return 
	
scr_80049cc:
	BranchID2 scr_8004a34
	goto_if_onscreen scr_80049e4
	wait_frame 
	return 
	
scr_80049e4:
	clear_local 
	
scr_80049e8:
	goto_if_attacked scr_8004a14
	call scr_8004a1c
	for scr_8004a10, 0x30
	wait_frame 
	goto scr_80049e8
	
scr_8004a10:
	return 
	
scr_8004a14:
	goto scr_80040dc
	
scr_8004a1c:
	for scr_8004a2c, 0xf
	return 
	
scr_8004a2c:
	goto scr_8004968
	
scr_8004a34:
	goto_if_onscreen scr_8004a44
	wait_frame 
	return 
	
scr_8004a44:
	clear_local 
	
scr_8004a48:
	goto_if_attacked scr_8004a74
	call scr_8004a7c
	for scr_8004a70, 0x2d
	wait_frame 
	goto scr_8004a48
	
scr_8004a70:
	return 
	
scr_8004a74:
	goto scr_80040dc
	
scr_8004a7c:
	for scr_8004a8c, 0x7
	return 
	
scr_8004a8c:
	goto scr_8004968
	
scr_8004a94:
	goto_if_onscreen scr_8004aa4
	wait_frame 
	return 
	
scr_8004aa4:
	cmd_76 0xa0011, scr_8004af8
	set_anim2 0x28
	play_sound 0x11a
	BranchID2 scr_8004ae0
	cmd_48 0xb, 0xa, 0x300, 0x0, 0x48
	return 
	
scr_8004ae0:
	cmd_48 0xf, 0xa, 0x300, 0x0, 0x48
	return 
	
scr_8004af8:
	return 
	
scr_8004afc:
	BranchID2 scr_8004b64
	goto_if_onscreen scr_8004b14
	wait_frame 
	return 
	
scr_8004b14:
	clear_local 
	
scr_8004b18:
	goto_if_attacked scr_8004b44
	call scr_8004b4c
	for scr_8004b40, 0x30
	wait_frame 
	goto scr_8004b18
	
scr_8004b40:
	return 
	
scr_8004b44:
	goto scr_80040dc
	
scr_8004b4c:
	for scr_8004b5c, 0xf
	return 
	
scr_8004b5c:
	goto scr_8004a94
	
scr_8004b64:
	goto_if_onscreen scr_8004b74
	wait_frame 
	return 
	
scr_8004b74:
	clear_local 
	
scr_8004b78:
	goto_if_attacked scr_8004ba4
	call scr_8004bac
	for scr_8004ba0, 0x2d
	wait_frame 
	goto scr_8004b78
	
scr_8004ba0:
	return 
	
scr_8004ba4:
	goto scr_80040dc
	
scr_8004bac:
	for scr_8004bbc, 0x7
	return 
	
scr_8004bbc:
	goto scr_8004a94
	
scr_8004bc4:
	goto_if_onscreen scr_8004bd4
	wait_frame 
	return 
	
scr_8004bd4:
	cmd_76 0x28000e, scr_8004c24
	set_anim2 0x29
	play_sound 0x11a
	BranchID2 scr_8004c10
	cmd_48 0xe, 0x2a, 0x219, 0x219, 0x49
	return 
	
scr_8004c10:
	cmd_48 0xa, 0x1c, 0x219, 0x219, 0x49
	
scr_8004c24:
	return 
	
scr_8004c28:
	BranchID2 scr_8004c98
	goto_if_onscreen scr_8004c40
	wait_frame 
	return 
	
scr_8004c40:
	clear_local 
	call scr_8005584
	
scr_8004c4c:
	goto_if_attacked scr_8004c78
	call scr_8004c80
	for scr_8004c74, 0x30
	wait_frame 
	goto scr_8004c4c
	
scr_8004c74:
	return 
	
scr_8004c78:
	goto scr_80040dc
	
scr_8004c80:
	for scr_8004c90, 0xf
	return 
	
scr_8004c90:
	goto scr_8004bc4
	
scr_8004c98:
	goto_if_onscreen scr_8004ca8
	wait_frame 
	return 
	
scr_8004ca8:
	clear_local 
	call scr_8005584
	
scr_8004cb4:
	goto_if_attacked scr_8004ce0
	call scr_8004ce8
	for scr_8004cdc, 0x2d
	wait_frame 
	goto scr_8004cb4
	
scr_8004cdc:
	return 
	
scr_8004ce0:
	goto scr_80040dc
	
scr_8004ce8:
	for scr_8004cf8, 0x7
	return 
	
scr_8004cf8:
	goto scr_8004bc4
	
scr_8004d00:
	goto_if_onscreen scr_8004d10
	wait_frame 
	return 
	
scr_8004d10:
	cmd_76 0x160015, scr_8004d60
	set_anim2 0x37
	play_sound 0x11a
	BranchID2 scr_8004d4c
	cmd_48 0x15, 0x16, 0x300, 0x0, 0x48
	return 
	
scr_8004d4c:
	cmd_48 0xe, 0x14, 0x300, 0x0, 0x48
	
scr_8004d60:
	return 
	
scr_8004d64:
	BranchID2 scr_8004dd4
	goto_if_onscreen scr_8004d7c
	wait_frame 
	return 
	
scr_8004d7c:
	clear_local 
	call scr_8005584
	
scr_8004d88:
	goto_if_attacked scr_8004db4
	call scr_8004dbc
	for scr_8004db0, 0x30
	wait_frame 
	goto scr_8004d88
	
scr_8004db0:
	return 
	
scr_8004db4:
	goto scr_80040dc
	
scr_8004dbc:
	for scr_8004dcc, 0xf
	return 
	
scr_8004dcc:
	goto scr_8004d00
	
scr_8004dd4:
	goto_if_onscreen scr_8004de4
	wait_frame 
	return 
	
scr_8004de4:
	clear_local 
	call scr_8005584
	
scr_8004df0:
	goto_if_attacked scr_8004e1c
	call scr_8004e24
	for scr_8004e18, 0x2d
	wait_frame 
	goto scr_8004df0
	
scr_8004e18:
	return 
	
scr_8004e1c:
	goto scr_80040dc
	
scr_8004e24:
	for scr_8004e34, 0x7
	return 
	
scr_8004e34:
	goto scr_8004d00
	
scr_8004e3c:
	set_anim 0x2f
	
scr_8004e44:
	goto_if_attacked scr_8004e64
	goto_if_anim_done scr_8004e60
	wait_frame 
	goto scr_8004e44
	
scr_8004e60:
	return 
	
scr_8004e64:
	call scr_80040dc
	set_anim 0x33
	return 
	
scr_8004e78:
	set_anim 0x31
	
scr_8004e80:
	goto_if_attacked scr_8004ea0
	goto_if_anim_done scr_8004e9c
	wait_frame 
	goto scr_8004e80
	
scr_8004e9c:
	return 
	
scr_8004ea0:
	call scr_80040dc
	set_anim 0x35
	return 
	
scr_8004eb4:
	set_anim 0x30
	
scr_8004ebc:
	goto_if_attacked scr_8004edc
	goto_if_anim_done scr_8004ed8
	wait_frame 
	goto scr_8004ebc
	
scr_8004ed8:
	return 
	
scr_8004edc:
	call scr_80040dc
	set_anim 0x34
	return 
	
scr_8004ef0:
	set_anim 0x32
	
scr_8004ef8:
	goto_if_attacked scr_8004f18
	goto_if_anim_done scr_8004f14
	wait_frame 
	goto scr_8004ef8
	
scr_8004f14:
	return 
	
scr_8004f18:
	call scr_80040dc
	set_anim 0x36
	return 
	
scr_8004f2c:
	goto_if_onscreen scr_8004f3c
	wait_frame 
	return 
	
scr_8004f3c:
	cmd_76 0x20000a, scr_8004fa8
	set_anim2 0x27
	clear_local 
	callasm (sub_8020810+1)
	
scr_8004f5c:
	goto_if_attacked scr_8004fb4
	for scr_8004f7c, 0x1e
	wait_frame 
	goto scr_8004f5c
	
scr_8004f7c:
	play_sound 0x11b
	cmd_4B 0xa, 0x20
	
scr_8004f8c:
	goto_if_attacked scr_8004fc8
	goto_if_anim_done scr_8004fa8
	wait_frame 
	goto scr_8004f8c
	
scr_8004fa8:
	set_anim 0x22
	return 
	
scr_8004fb4:
	cmd_48 0xa, 0x10, 0x0, 0x0, 0x4a
	
scr_8004fc8:
	goto scr_80040dc
	
scr_8004fd0:
	goto_if_onscreen scr_8004fe0
	wait_frame 
	return 
	
scr_8004fe0:
	set_anim 0x22
	clear_local 
	call scr_8005584
	
scr_8004ff4:
	goto_if_attacked scr_8005020
	call scr_8005028
	for scr_800501c, 0x3e
	wait_frame 
	goto scr_8004ff4
	
scr_800501c:
	return 
	
scr_8005020:
	goto scr_80040dc
	
scr_8005028:
	for scr_8005038, 0x1e
	return 
	
scr_8005038:
	goto scr_8004f2c
	
scr_8005040:
	goto_if_onscreen scr_8005050
	wait_frame 
	return 
	
scr_8005050:
	cmd_76 0x170008, scr_80050c0
	set_anim2 0x28
	clear_local 
	callasm (sub_8020810+1)
	
scr_8005070:
	faceplayer 
	goto_if_attacked scr_80050c4
	for scr_8005094, 0x1e
	wait_frame 
	goto scr_8005070
	
scr_8005094:
	play_sound 0x11b
	cmd_4B 0x8, 0x17
	
scr_80050a4:
	goto_if_attacked scr_80050c4
	goto_if_anim_done scr_80050c0
	wait_frame 
	goto scr_80050a4
	
scr_80050c0:
	return 
	
scr_80050c4:
	goto scr_80040dc
	
scr_80050cc:
	goto_if_onscreen scr_80050dc
	wait_frame 
	return 
	
scr_80050dc:
	set_anim 0x23
	clear_local 
	call scr_8005584
	
scr_80050f0:
	goto_if_attacked scr_800511c
	call scr_8005124
	for scr_8005118, 0x3e
	wait_frame 
	goto scr_80050f0
	
scr_8005118:
	return 
	
scr_800511c:
	goto scr_80040dc
	
scr_8005124:
	for scr_8005134, 0x1e
	return 
	
scr_8005134:
	goto scr_8005040
	
scr_800513c:
	goto_if_onscreen scr_800514c
	wait_frame 
	return 
	
scr_800514c:
	set_anim2 0x29
	clear_local 
	
scr_8005158:
	goto_if_attacked scr_80051ac
	for scr_8005178, 0x16
	wait_frame 
	goto scr_8005158
	
scr_8005178:
	cmd_48 0xfff8, 0xd, 0x0, 0x0, 0x4b
	
scr_800518c:
	goto_if_attacked scr_80051ac
	goto_if_anim_done scr_80051a8
	wait_frame 
	goto scr_800518c
	
scr_80051a8:
	return 
	
scr_80051ac:
	goto scr_80040dc
	
scr_80051b4:
	goto_if_onscreen scr_80051c4
	wait_frame 
	return 
	
scr_80051c4:
	call scr_8005244
	callasm (sub_802038C+1)
	clear_local 
	call scr_8005584
	play_sound 0x128
	
scr_80051e8:
	goto_if_attacked scr_8005220
	for scr_8005210, 0x3c
	call scr_8005228
	wait_frame 
	goto scr_80051e8
	
scr_8005210:
	clear_local 
	call scr_8005274
	return 
	
scr_8005220:
	goto scr_80040dc
	
scr_8005228:
	for scr_8005238, 0x8
	return 
	
scr_8005238:
	callasm (sub_80203CC+1)
	return 
	
scr_8005244:
	callasm (sub_8020304+1)
	
scr_800524c:
	goto_if_attacked scr_800526c
	goto_if_anim_done scr_8005268
	wait_frame 
	goto scr_800524c
	
scr_8005268:
	return 
	
scr_800526c:
	goto scr_80040dc
	
scr_8005274:
	callasm (sub_80203AC+1)
	
scr_800527c:
	goto_if_attacked scr_80052a4
	goto_if_anim_done scr_8005298
	wait_frame 
	goto scr_800527c
	
scr_8005298:
	set_anim 0x0
	return 
	
scr_80052a4:
	goto scr_80040dc
	
scr_80052ac:
	goto_if_onscreen scr_80052bc
	wait_frame 
	return 
	
scr_80052bc:
	cmd_76 0x150016, scr_8005368
	play_sound 0x126
	set_anim 0x31
	clear_local 
	
scr_80052dc:
	goto_if_attacked scr_8005374
	for scr_80052fc, 0x14
	wait_frame 
	goto scr_80052dc
	
scr_80052fc:
	set_anim 0x32
	
scr_8005304:
	goto_if_attacked scr_8005374
	for scr_8005324, 0x14
	wait_frame 
	goto scr_8005304
	
scr_8005324:
	play_sound 0x127
	set_anim 0x27
	cmd_48 0x16, 0x13, 0x400, 0x0, 0x4d
	
scr_8005348:
	goto_if_attacked scr_8005374
	for scr_8005368, 0x14
	wait_frame 
	goto scr_8005348
	
scr_8005368:
	set_anim 0x0
	return 
	
scr_8005374:
	goto scr_80040dc
	
scr_800537c:
	goto_if_onscreen scr_800538c
	wait_frame 
	return 
	
scr_800538c:
	cmd_76 0xa0019, scr_8005438
	play_sound 0x126
	set_anim 0x36
	clear_local 
	
scr_80053ac:
	goto_if_attacked scr_800543c
	for scr_80053cc, 0x14
	wait_frame 
	goto scr_80053ac
	
scr_80053cc:
	set_anim 0x37
	
scr_80053d4:
	goto_if_attacked scr_800543c
	for scr_80053f4, 0x14
	wait_frame 
	goto scr_80053d4
	
scr_80053f4:
	play_sound 0x127
	set_anim 0x28
	cmd_48 0x19, 0xa, 0x400, 0x0, 0x4d
	
scr_8005418:
	goto_if_attacked scr_800543c
	for scr_8005438, 0x14
	wait_frame 
	goto scr_8005418
	
scr_8005438:
	return 
	
scr_800543c:
	goto scr_80040dc
	
scr_8005444:
	goto_if_onscreen scr_8005454
	wait_frame 
	return 
	
scr_8005454:
	call scr_800550c
	play_sound 0x126
	set_anim 0x3b
	clear_local 
	
scr_8005470:
	goto_if_attacked scr_8005504
	for scr_8005490, 0x14
	wait_frame 
	goto scr_8005470
	
scr_8005490:
	set_anim 0x3c
	
scr_8005498:
	goto_if_attacked scr_8005504
	for scr_80054b8, 0x14
	wait_frame 
	goto scr_8005498
	
scr_80054b8:
	play_sound 0x127
	set_anim 0x29
	cmd_48 0x0, 0x22, 0x0, 0x400, 0x4e
	
scr_80054dc:
	goto_if_attacked scr_8005504
	for scr_80054fc, 0x14
	wait_frame 
	goto scr_80054dc
	
scr_80054fc:
	goto scr_8005548
	
scr_8005504:
	goto scr_80040dc
	
scr_800550c:
	set_anim 0x39
	
scr_8005514:
	goto_if_attacked scr_8005534
	goto_if_anim_done scr_8005530
	wait_frame 
	goto scr_8005514
	
scr_8005530:
	return 
	
scr_8005534:
	call scr_80040dc
	set_anim 0x3a
	return 
	
scr_8005548:
	set_anim 0x3d
	
scr_8005550:
	goto_if_attacked scr_8005570
	goto_if_anim_done scr_800556c
	wait_frame 
	goto scr_8005550
	
scr_800556c:
	return 
	
scr_8005570:
	call scr_80040dc
	set_anim 0x0
	return 
	
scr_8005584:
	clear_local 
	return 
	clear_local 
	
scr_8005590:
	set_anim 0x0
	call_if_attacked scr_8003f6c
	for scr_80055b8, 0x5a
	wait_frame 
	goto scr_8005590
	
scr_80055b8:
	return 
	clear_local 
	
scr_80055c0:
	set_anim 0x0
	call_if_attacked scr_8003f6c
	for scr_80055e8, 0x3c
	wait_frame 
	goto scr_80055c0
	
scr_80055e8:
	return 
	clear_local 
	
scr_80055f0:
	set_anim 0x0
	call_if_attacked scr_8003f6c
	for scr_8005618, 0x78
	wait_frame 
	goto scr_80055f0
	
scr_8005618:
	return 
	
scr_800561c:
	clear_local 
	
scr_8005620:
	set_anim 0x22
	call_if_attacked scr_80040dc
	for scr_8005648, 0x5a
	wait_frame 
	goto scr_8005620
	
scr_8005648:
	return 
	
scr_800564c:
	clear_local 
	
scr_8005650:
	set_anim 0x22
	call_if_attacked scr_80040dc
	for scr_8005678, 0x3c
	wait_frame 
	goto scr_8005650
	
scr_8005678:
	return 
	clear_local 
	
scr_8005680:
	set_anim 0x22
	call_if_attacked scr_80040dc
	for scr_80056a8, 0x78
	wait_frame 
	goto scr_8005680
	
scr_80056a8:
	return 
	
scr_80056ac:
	cmd_7D scr_80056bc
	faceplayer 
	return 
	
scr_80056bc:
	set_dir 0x1
	goto_if_rng scr_80056d8, 0x32
	set_dir 0x0
	
scr_80056d8:
	return 
	
scr_80056dc:
	cmd_D2 0x5a0059, 0x59
	play_sound_cond 0x11d, 0x11e, 0x11e
	return 
	
	
	
	
