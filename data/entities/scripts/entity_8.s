.globl entity_8_scr
entity_8_scr:
	
	cmd_9A scr_8000cf4
	create_child_ent 0x0, 0x0, 0x0, 0x0, 0x6f
	
scr_8000fbc:
	SetHighBit 0xf
	ClearHighBit 0x1
	ClearHighBit 0x2
	set_anim 0x38
	wait_frame 
	clear_local 
	set_anim 0x38
	
scr_8000fec:
	goto_if_attacked scr_8001024
	goto_if_field44_bit6 scr_8001010
	cmd_7A scr_8001010
	wait_frame 
	goto scr_8000fec
	
scr_8001010:
	wait_frame 
	goto_if_offscreen scr_8000fec
	goto scr_800105c
	
scr_8001024:
	call scr_80012d4
	goto scr_80010d4
	goto_if_offscreen scr_8001058
	ClearHighBit 0x0
	create_child_ent 0x0, 0x0, 0x0, 0x0, 0x6f
	
scr_8001058:
	return 
	
scr_800105c:
	SetHighBit 0x2
	ClearHighBit 0xf
	create_child_ent 0x0, 0x22, 0x0, 0x0, 0x6e
	faceplayer 
	set_anim 0x39
	clear_local 
	
scr_8001090:
	goto_if_attacked scr_80010cc
	goto_if_anim_done scr_80010ac
	wait_frame 
	goto scr_8001090
	
scr_80010ac:
	goto_if_attacked scr_80010cc
	for scr_80011d0, 0xa
	wait_frame 
	goto scr_80010ac
	
scr_80010cc:
	call scr_8001348
	
scr_80010d4:
	ClearHighBit 0x2
	SetHighBit 0x1
	ClearHighBit 0xf
	load_rand 0x3c
	add_local 0x28
	set_anim 0x41
	
scr_8001104:
	goto_if_attacked scr_800113c
	goto_if_not_player_x scr_8001150
	cmd_7C scr_8001124
	cmd_7F scr_8001164
	
scr_8001124:
	cmd_0E scr_80011d0, 0x0
	wait_frame 
	goto scr_8001104
	
scr_800113c:
	call scr_80012d4
	wait_frame 
	goto scr_80010d4
	
scr_8001150:
	call scr_8001458
	wait_frame 
	goto scr_8001104
	
scr_8001164:
	clear_local 
	
scr_8001168:
	faceplayer 
	goto_if_attacked scr_800113c
	cmd_7C scr_80010d4
	cmd_7E scr_80010d4
	for scr_800119c, 0x78
	wait_frame 
	goto scr_8001168
	
scr_800119c:
	set_anim 0x3e
	
scr_80011a4:
	goto_if_attacked scr_800113c
	cmd_7C scr_80010d4
	cmd_7E scr_80010d4
	goto_if_anim_done scr_8000fbc
	wait_frame 
	goto scr_80011a4
	
scr_80011d0:
	SetHighBit 0x2
	goto_if_offscreen scr_8001274
	clear_local 
	set_anim 0x39
	
scr_80011ec:
	goto_if_attacked scr_8001284
	goto_if_anim_done scr_8001208
	wait_frame 
	goto scr_80011ec
	
scr_8001208:
	goto_if_attacked scr_8001284
	for scr_8001228, 0xf
	wait_frame 
	goto scr_8001208
	
scr_8001228:
	goto_if_attacked scr_8001284
	call scr_8001420
	for scr_8001250, 0x30
	wait_frame 
	goto scr_8001228
	
scr_8001250:
	set_anim 0x3f
	
scr_8001258:
	goto_if_attacked scr_8001284
	goto_if_anim_done scr_8001274
	wait_frame 
	goto scr_8001258
	
scr_8001274:
	ClearHighBit 0x2
	goto scr_80010d4
	
scr_8001284:
	call scr_8001348
	goto scr_80010d4
	
scr_8001294:
	SetHighBit 0x0
	play_sound 0x142
	kill_hostage 
	cmd_99 
	set_anim 0x42
	
scr_80012b4:
	goto_if_anim_done scr_80012c8
	wait_frame 
	goto scr_80012b4
	
scr_80012c8:
	ClearBit7Field44 
	goto scr_8000cf4
	
scr_80012d4:
	ifChildFlag23 scr_8001348
	goto_if_player_x scr_8001294
	
scr_80012e4:
	call scr_80056dc
	clear_local 
	set_anim2 0x44
	cmd_60 
	cmd_6C scr_80013d8
	ClearBit7Field44 
	SetBit4Field44 
	
scr_800130c:
	goto_if_attacked scr_80012e4
	goto_if_anim_done scr_8001328
	wait_frame 
	goto scr_800130c
	
scr_8001328:
	goto_if_attacked scr_80012e4
	for scr_80013d0, 0xa
	wait_frame 
	goto scr_8001328
	
scr_8001348:
	call scr_80056dc
	clear_local 
	set_anim2 0x43
	cmd_60 
	cmd_6C scr_80013d8
	ClearBit7Field44 
	SetBit4Field44 
	
scr_8001370:
	goto_if_attacked scr_8001348
	goto_if_anim_done scr_800138c
	wait_frame 
	goto scr_8001370
	
scr_800138c:
	goto_if_attacked scr_8001348
	for scr_80013ac, 0xa
	wait_frame 
	goto scr_800138c
	
scr_80013ac:
	set_anim 0x3e
	goto_if_attacked scr_8001348
	goto_if_anim_done scr_80013d0
	wait_frame 
	goto scr_80013ac
	
scr_80013d0:
	ClearBit4Field44 
	return 
	
scr_80013d8:
	SetHighBit 0x0
	save_hostage 
	cmd_99 
	SetBit4Field44 
	disable_collision 
	
scr_80013f0:
	goto_if_anim_done scr_8001404
	wait_frame 
	goto scr_80013f0
	
scr_8001404:
	create_child_ent 0x0, 0x0, 0x0, 0x0, 0x8e
	goto scr_800414c
	
scr_8001420:
	for scr_8001430, 0xf
	return 
	
scr_8001430:
	set_anim 0x3b
	play_sound 0x11a
	cmd_48 0x9, 0x16, 0x300, 0x0, 0x48
	return 
	
scr_8001458:
	set_anim 0x45
	
scr_8001460:
	goto_if_attacked scr_800148c
	goto_if_anim_done scr_800147c
	wait_frame 
	goto scr_8001460
	
scr_800147c:
	toggle_dir 
	set_anim 0x41
	return 
	
scr_800148c:
	call scr_8001348
	goto scr_800147c
	
	
	
	
