.globl entity_31_scr
entity_31_scr:
	
	set_anim 0x0
	wait_frame 
	
scr_8009bf0:
	goto_if_attacked scr_8009c9c
	goto_if_offscreen scr_8009c0c
	goto_if_asm (sub_8021E00+1), scr_8009c18
	
scr_8009c0c:
	wait_frame 
	goto scr_8009bf0
	
scr_8009c18:
	play_sound 0x1af
	faceplayer 
	set_anim 0x3
	set_vel_y 0xfffffec0
	cmd_3D_setVelXDirBased 0x40
	
scr_8009c3c:
	goto_if_offset_collision scr_8009c98, 0x0, 0x0, 0xffff
	goto_if_attacked scr_8009c9c
	goto_if_near_player_y scr_8009c6c, 0x18
	wait_frame 
	goto scr_8009c3c
	
scr_8009c6c:
	callasm (sub_8021E38+1)
	
scr_8009c74:
	goto_if_offscreen scr_8009c98
	goto_if_attacked scr_8009c9c
	callasm (sub_8021E40+1)
	wait_frame 
	goto scr_8009c74
	
scr_8009c98:
	destroy 
	
scr_8009c9c:
	play_sound 0x1b0
	cmd_D2 0x5a0059, 0x59
	disable_collision 
	SetBit4Field44 
	set_anim 0x5
	set_vel_x 0x0
	set_vel_y 0x0
	SetInit4E 0x0
	kill 
	
scr_8009cdc:
	goto_if_offscreen scr_8009cf0
	wait_frame 
	goto scr_8009cdc
	
scr_8009cf0:
	destroy 
	
	
	
	
