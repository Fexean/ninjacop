.globl entity_32_scr
entity_32_scr:
	
	load_rand 0x78
	add_local 0x3c
	wait_frame 
	
scr_8009d08:
	cmd_0E scr_8009d48, 0x0
	call_if_attacked scr_8009e68
	wait_frame 
	goto scr_8009d08
	
scr_8009d28:
	cmd_0E scr_8009dc0, 0x0
	call_if_attacked scr_8009e68
	wait_frame 
	goto scr_8009d28
	
scr_8009d48:
	goto_if_offscreen scr_8009d88
	call scr_8009e38
	play_sound 0x1a3
	create_child_ent 0x0, 0xd, 0xffffff00, 0x100, 0x5f
	create_child_ent 0x0, 0xd, 0x100, 0x100, 0x5f
	
scr_8009d88:
	load_rand 0x3c
	add_local 0x1e
	goto_if_difficulty 0x2, scr_8009db4
	load_rand 0x78
	add_local 0x3c
	
scr_8009db4:
	wait_frame 
	goto scr_8009d28
	
scr_8009dc0:
	goto_if_offscreen scr_8009e00
	call scr_8009e38
	play_sound 0x1a3
	create_child_ent 0x0, 0xd, 0xfffffe00, 0x100, 0x5f
	create_child_ent 0x0, 0xd, 0x200, 0x100, 0x5f
	
scr_8009e00:
	load_rand 0x3c
	add_local 0x1e
	goto_if_difficulty 0x2, scr_8009e2c
	load_rand 0x78
	add_local 0x3c
	
scr_8009e2c:
	wait_frame 
	goto scr_8009d08
	
scr_8009e38:
	set_anim 0x1
	
scr_8009e40:
	goto_if_attacked scr_8009e68
	goto_if_anim_done scr_8009e5c
	wait_frame 
	goto scr_8009e40
	
scr_8009e5c:
	set_anim 0x0
	return 
	
scr_8009e68:
	cmd_D2 0x5a0059, 0x59
	cmd_60 
	cmd_6C scr_8009ec4
	play_sound 0x1a4
	SetBit4Field44 
	ClearBit7Field44 
	set_anim 0x2
	
scr_8009e98:
	goto_if_attacked scr_8009e68
	goto_if_anim_done scr_8009eb4
	wait_frame 
	goto scr_8009e98
	
scr_8009eb4:
	set_anim 0x0
	ClearBit4Field44 
	return 
	
scr_8009ec4:
	play_sound 0x1a5
	disable_collision 
	SetBit4Field44 
	SetInit4E 0x0
	kill 
	
scr_8009ee0:
	goto_if_ground scr_8009ef4
	wait_frame 
	goto scr_8009ee0
	
scr_8009ef4:
	set_anim 0x3
	
scr_8009efc:
	goto_if_anim_done scr_8009f10
	wait_frame 
	goto scr_8009efc
	
scr_8009f10:
	destroy 
	
	
	
	
