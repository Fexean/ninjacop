.globl entity_8a_scr
entity_8a_scr:
	
	cmd_A6 0x5f02
	callasm (sub_802131C+1)
	callasm (sub_8021544+1)
	wait_frame 
	branchIfParentHighFlag scr_8008d08, 0xa
	clear_local 
	
scr_80085d0:
	branchIfParentHighFlag scr_800871c, 0x0
	call_if_attacked scr_80087e4
	callasm (sub_802138C+1)
	goto_if_collide scr_8008600
	wait_frame 
	goto scr_80085d0
	
scr_8008600:
	for scr_8008618, 0xa
	wait_frame 
	goto scr_80085d0
	
scr_8008618:
	branchIfParentHighFlag scr_800871c, 0x0
	call_if_attacked scr_80087e4
	callasm (sub_802138C+1)
	goto_if_y_between 0x58, 0x68, scr_800864c
	wait_frame 
	goto scr_8008618
	
scr_800864c:
	goto_if_x_between 0x30, 0xc0, scr_8008664
	wait_frame 
	goto scr_8008618
	
scr_8008664:
	SetBit0Init52 
	callasm (sub_8021640+1)
	LoadDifficulty 0x180, 0x100, 0x80
	
scr_800867c:
	branchIfParentHighFlag scr_800871c, 0x0
	call_if_attacked scr_80087e4
	callasm (sub_8021660+1)
	cmd_0E scr_80086b0, 0x0
	wait_frame 
	goto scr_800867c
	
scr_80086b0:
	callasm (sub_80216E8+1)
	
scr_80086b8:
	call_if_attacked scr_80087e4
	cmd_51 scr_80086d4
	wait_frame 
	goto scr_80086b8
	
scr_80086d4:
	set_vel_x 0x0
	set_vel_y 0x0
	clear_local 
	
scr_80086e8:
	for scr_8008700, 0x50
	wait_frame 
	goto scr_80086e8
	
scr_8008700:
	callasm (sub_8021794+1)
	setParentHighFlag 0x5
	SetHighBit 0x5
	destroy 
	
scr_800871c:
	disable_collision 
	SetBit4Field44 
	goto scr_80086b0
	
	
	
	
