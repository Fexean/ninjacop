.globl entity_8f_scr
entity_8f_scr:
	
	set_dir 0x0
	cmd_A6 0x9102
	SetInit4E 0x1
	hidesprite 
	goto scr_800d520
	wait_frame 
	
scr_800c548:
	hidesprite 
	call_if_attacked scr_800c5cc
	branchIfParentHighFlag scr_800c574, 0x1
	callasm (sub_8022768+1)
	wait_frame 
	goto scr_800c548
	
scr_800c574:
	call scr_800c764
	clearParentHighFlag 0x1
	wait_frame 
	goto scr_800c548
	
scr_800c590:
	cmd_62 
	cmd_9C 
	cmd_D2 0x5a0059, 0x59
	play_sound_cond 0x173, 0x174, 0x175
	cmd_6C scr_800c6d0
	cmd_DA unk_832e064
	setParentHighFlag 0x3
	return 
	
scr_800c5cc:
	call scr_800c590
	branchIfParentHighFlag scr_800c5ec, 0x5
	goto_if_asm (sub_8022710+1), scr_800d0a0
	
scr_800c5ec:
	return 
	
scr_800c5f0:
	call scr_800c590
	ifChildFlag21 scr_800c618
	SetBit4Field44 
	disable_collision 
	call scr_800c64c
	ClearBit4Field44 
	enable_collision 
	
scr_800c618:
	return 
	
scr_800c61c:
	call scr_800c590
	ifChildFlag23 scr_800c630
	return 
	
scr_800c630:
	SetBit4Field44 
	disable_collision 
	call scr_800c64c
	ClearBit4Field44 
	enable_collision 
	return 
	
scr_800c64c:
	SetHighBit 0x4
	reset_velocity 
	set_anim 0x7
	cmd_26 0xffffffcf, 0x400
	cmd_3D_setVelXDirBased 0xfffffe80
	set_vel_y 0x380
	ClearBit0Init52 
	wait_frame 
	
scr_800c684:
	goto_if_ground scr_800c698
	wait_frame 
	goto scr_800c684
	
scr_800c698:
	play_sound 0x169
	set_anim 0x9
	reset_velocity 
	
scr_800c6ac:
	goto_if_anim_done scr_800c6c0
	wait_frame 
	goto scr_800c6ac
	
scr_800c6c0:
	clear_local 
	wait_frames 0x1e
	return 
	
scr_800c6d0:
	callasm (sub_8033184+1)
	disable_collision 
	SetBit4Field44 
	
scr_800c6e0:
	branchNotParentHighFlag scr_800c6f8, 0x5
	wait_frame 
	goto scr_800c6e0
	
scr_800c6f8:
	branchIfParentHighFlag scr_800c70c, 0x1
	callasm (sub_8022768+1)
	
scr_800c70c:
	play_sound 0x168
	cmd_A6 0x8f02
	showsprite 
	setParentHighFlag 0x2
	call scr_800c64c
	set_anim 0xd
	
scr_800c738:
	goto_if_anim_done scr_800c74c
	wait_frame 
	goto scr_800c738
	
scr_800c74c:
	setParentHighFlag 0x6
	
scr_800c754:
	wait_frame 
	goto scr_800c754
	destroy 
	
scr_800c764:
	showsprite 
	call scr_800c7dc
	SetBit0Init52 
	CopyInitPos 
	clear_local 
	
scr_800c77c:
	call_if_attacked scr_800c5f0
	BranchHighBit scr_800c7c8, 0x4
	goto_if_y_between2 0x178, 0x200, scr_800cb34
	call scr_800c7f0
	BranchHighBit scr_800c7c8, 0x4
	for_not scr_800c7c8, 0xa
	wait_frame 
	goto scr_800c77c
	
scr_800c7c8:
	ClearHighBit 0x4
	call scr_800ca64
	return 
	
scr_800c7dc:
	cmd_A6 0x8f02
	set_anim 0x5
	return 
	
scr_800c7f0:
	faceplayer 
	reset_velocity 
	play_sound 0x164
	cmd_3D_setVelXDirBased 0xc0
	goto_if_y_between 0x0, 0xaf, scr_800c834
	goto_if_y_between 0xb0, 0xef, scr_800c858
	goto_if_y_between 0xf0, 0x127, scr_800c8b8
	goto scr_800c918
	
scr_800c834:
	cmd_D4 0x1400b8
	cmd_3D_setVelXDirBased 0x180
	goto_if_y_between2 0x0, 0xa8, scr_800c968
	goto scr_800c9a8
	
scr_800c858:
	goto_if_y_between2 0x0, 0xaf, scr_800c884
	goto_if_y_between2 0xb0, 0xef, scr_800c898
	goto_if_y_between2 0xf0, 0x127, scr_800c8a8
	goto scr_800c8a8
	
scr_800c884:
	cmd_26 0xffffff9e, 0x700
	goto scr_800c968
	
scr_800c898:
	cmd_D4 0x1400f0
	goto scr_800c9a8
	
scr_800c8a8:
	cmd_D4 0x1400f8
	goto scr_800c9e4
	
scr_800c8b8:
	goto_if_y_between2 0x0, 0xaf, scr_800c8e4
	goto_if_y_between2 0xb0, 0xef, scr_800c8e4
	goto_if_y_between2 0xf0, 0x127, scr_800c8f8
	goto scr_800c908
	
scr_800c8e4:
	cmd_26 0xffffff9e, 0x700
	goto scr_800c9a8
	
scr_800c8f8:
	cmd_D4 0x140130
	goto scr_800c9e4
	
scr_800c908:
	cmd_D4 0x140138
	goto scr_800ca20
	
scr_800c918:
	goto_if_y_between2 0x0, 0xaf, scr_800c944
	goto_if_y_between2 0xb0, 0xef, scr_800c944
	goto_if_y_between2 0xf0, 0x127, scr_800c944
	goto scr_800c958
	
scr_800c944:
	cmd_26 0xffffff9e, 0x700
	goto scr_800c9e4
	
scr_800c958:
	cmd_D4 0x140178
	goto scr_800ca20
	
scr_800c968:
	ClearBit0Init52 
	wait_frame 
	
scr_800c970:
	call_if_attacked scr_800c5f0
	BranchHighBit scr_800c9a4, 0x4
	goto_if_y_between 0x48, 0x50, scr_800ca5c
	goto_if_ground scr_800ca5c
	wait_frame 
	goto scr_800c970
	
scr_800c9a4:
	return 
	
scr_800c9a8:
	wait_frame 
	
scr_800c9ac:
	call_if_attacked scr_800c5f0
	BranchHighBit scr_800c9e0, 0x4
	goto_if_y_between 0xb0, 0xb8, scr_800ca5c
	goto_if_ground scr_800ca5c
	wait_frame 
	goto scr_800c9ac
	
scr_800c9e0:
	return 
	
scr_800c9e4:
	wait_frame 
	
scr_800c9e8:
	call_if_attacked scr_800c5f0
	BranchHighBit scr_800ca1c, 0x4
	goto_if_y_between 0xf0, 0xf8, scr_800ca5c
	goto_if_ground scr_800ca5c
	wait_frame 
	goto scr_800c9e8
	
scr_800ca1c:
	return 
	
scr_800ca20:
	wait_frame 
	
scr_800ca24:
	call_if_attacked scr_800c5f0
	BranchHighBit scr_800ca58, 0x4
	goto_if_y_between 0x130, 0x138, scr_800ca5c
	goto_if_ground scr_800ca5c
	wait_frame 
	goto scr_800ca24
	
scr_800ca58:
	return 
	
scr_800ca5c:
	reset_velocity 
	return 
	
scr_800ca64:
	set_anim 0x2
	SetBit0Init52 
	reset_velocity 
	cmd_26 0xffffff9e, 0x700
	
scr_800ca80:
	goto_if_y_between 0x0, 0x80, scr_800ca98
	wait_frame 
	goto scr_800ca80
	
scr_800ca98:
	ClearBit0Init52 
	wait_frame 
	
scr_800caa0:
	goto_if_ground scr_800cab4
	wait_frame 
	goto scr_800caa0
	
scr_800cab4:
	reset_velocity 
	SetBit0Init52 
	cmd_3C_modifyDir 
	set_anim 0xa
	play_sound 0x162
	callasm (sub_80227DC+1)
	cmd_D7 0xa08002, 0x1e8002
	
scr_800cae4:
	cmd_50 scr_800caf8
	wait_frame 
	goto scr_800cae4
	
scr_800caf8:
	reset_velocity 
	cmd_A6 0x9102
	callasm (sub_8022768+1)
	set_anim 0x4
	play_sound 0x163
	
scr_800cb1c:
	goto_if_anim_done scr_800cb30
	wait_frame 
	goto scr_800cb1c
	
scr_800cb30:
	return 
	
scr_800cb34:
	call scr_800cb9c
	BranchHighBit scr_800cb68, 0x4
	
scr_800cb48:
	BranchHighBit scr_800cb68, 0x4
	goto_if_y_between2 0x0, 0x170, scr_800ca64
	call scr_800ce48
	
scr_800cb68:
	BranchNotHighBit scr_800cb48, 0x4
	ClearHighBit 0x4
	goto_if_y_between 0x0, 0x148, scr_800cb94
	wait_frame 
	goto scr_800cb34
	
scr_800cb94:
	goto scr_800ca64
	
scr_800cb9c:
	SetInit4E 0x1
	reset_velocity 
	SetBit0Init52 
	play_sound 0x162
	set_anim 0x3
	goto_if_x_between2 0x100, 0x200, scr_800cbdc
	cmd_D7 0x1a00160, 0x280180
	goto scr_800cbe8
	
scr_800cbdc:
	cmd_D7 0x1a000a0, 0x280180
	
scr_800cbe8:
	clear_local 
	
scr_800cbec:
	call_if_attacked scr_800c61c
	BranchHighBit scr_800cc74, 0x4
	for scr_800cc18, 0x28
	wait_frame 
	goto scr_800cbec
	
scr_800cc18:
	ClearBit0Init52 
	wait_frame 
	
scr_800cc20:
	call_if_attacked scr_800c61c
	BranchHighBit scr_800cc74, 0x4
	goto_if_ground scr_800cc48
	wait_frame 
	goto scr_800cc20
	
scr_800cc48:
	reset_velocity 
	set_anim 0x4
	play_sound 0x163
	faceplayer 
	
scr_800cc60:
	goto_if_anim_done scr_800cc74
	wait_frame 
	goto scr_800cc60
	
scr_800cc74:
	reset_velocity 
	return 
	
scr_800cc7c:
	ClearBit0Init52 
	wait_frame 
	
scr_800cc84:
	call_if_attacked scr_800c61c
	BranchHighBit scr_800ccf4, 0x4
	goto_if_ground scr_800ccac
	wait_frame 
	goto scr_800cc84
	
scr_800ccac:
	faceplayer 
	reset_velocity 
	set_anim 0x6
	play_sound 0x163
	
scr_800ccc4:
	call_if_attacked scr_800c61c
	BranchHighBit scr_800ccf4, 0x4
	goto_if_anim_done scr_800ccec
	wait_frame 
	goto scr_800ccc4
	
scr_800ccec:
	set_anim 0x0
	
scr_800ccf4:
	return 
	
scr_800ccf8:
	ClearBit0Init52 
	wait_frame 
	
scr_800cd00:
	call_if_attacked scr_800c61c
	BranchHighBit scr_800cd34, 0x4
	goto_if_ground scr_800cd28
	wait_frame 
	goto scr_800cd00
	
scr_800cd28:
	reset_velocity 
	play_sound 0x163
	
scr_800cd34:
	return 
	
scr_800cd38:
	set_dir 0x1
	goto_if_x_between 0x0, 0x100, scr_800cd54
	set_dir 0x0
	
scr_800cd54:
	return 
	
scr_800cd58:
	reset_velocity 
	set_anim 0x5
	play_sound 0x164
	cmd_3E_Init30_28_ReverseMe 0x80, 0x400
	
scr_800cd78:
	call_if_attacked scr_800c61c
	BranchHighBit scr_800cde0, 0x4
	cmd_74 scr_800cda0
	wait_frame 
	goto scr_800cd78
	
scr_800cda0:
	faceplayer 
	reset_velocity 
	set_anim 0x6
	
scr_800cdb0:
	call_if_attacked scr_800c61c
	BranchHighBit scr_800cde0, 0x4
	goto_if_anim_done scr_800cdd8
	wait_frame 
	goto scr_800cdb0
	
scr_800cdd8:
	set_anim 0x0
	
scr_800cde0:
	return 
	
scr_800cde4:
	faceplayer 
	reset_velocity 
	set_anim 0x5
	play_sound 0x164
	cmd_D7 0x1c08001, 0x148001
	return 
	
scr_800ce0c:
	set_anim 0x5
	play_sound 0x164
	goto_if_dir scr_800ce38, 0x0
	cmd_D7 0x1e00160, 0x190180
	return 
	
scr_800ce38:
	cmd_D7 0x1e00098, 0x190180
	return 
	
scr_800ce48:
	SwitchDifficulty scr_800ce58, scr_800ce74, scr_800ce90
	
scr_800ce58:
	rng_switch 0x5, scr_800ceb4, scr_800cf14, scr_800cf14, scr_800cf24, scr_800cf24
	
scr_800ce74:
	rng_switch 0x5, scr_800ceb4, scr_800cf14, scr_800cf24, scr_800cf8c, scr_800cfd0
	
scr_800ce90:
	rng_switch 0x7, scr_800ceb4, scr_800cf14, scr_800cf24, scr_800cf4c, scr_800cf8c, scr_800cfd0, scr_800d03c
	
scr_800ceb4:
	goto_if_x_between2 0x0, 0x98, scr_800cf10
	goto_if_x_between2 0x160, 0x200, scr_800cf10
	call scr_800cde4
	BranchHighBit scr_800cf10, 0x4
	call scr_800ccf8
	BranchHighBit scr_800cf10, 0x4
	call scr_800ce0c
	BranchHighBit scr_800cf10, 0x4
	goto scr_800cc7c
	
scr_800cf10:
	return 
	
scr_800cf14:
	call scr_800cd38
	goto scr_800cd58
	
scr_800cf24:
	call scr_800cd38
	call scr_800ce0c
	BranchHighBit scr_800cf48, 0x4
	goto scr_800cc7c
	
scr_800cf48:
	return 
	
scr_800cf4c:
	call scr_800cd38
	call scr_800ce0c
	BranchHighBit scr_800cf88, 0x4
	call scr_800ccf8
	BranchHighBit scr_800cf88, 0x4
	toggle_dir 
	goto scr_800cd58
	
scr_800cf88:
	return 
	
scr_800cf8c:
	call scr_800cd38
	set_anim 0x5
	play_sound 0x164
	cmd_D8 0x280028, 0x14
	call scr_800ccf8
	BranchHighBit scr_800cfcc, 0x4
	goto scr_800cd58
	
scr_800cfcc:
	return 
	
scr_800cfd0:
	call scr_800cd38
	set_local 0x2
	
scr_800cfe0:
	set_anim 0x5
	play_sound 0x164
	cmd_D8 0x280030, 0xf
	call scr_800ccf8
	BranchHighBit scr_800d038, 0x4
	cmd_0E scr_800d028, 0x0
	wait_frame 
	goto scr_800cfe0
	
scr_800d028:
	call scr_800ce0c
	goto scr_800cc7c
	
scr_800d038:
	return 
	
scr_800d03c:
	goto_if_x_between2 0x0, 0x98, scr_800d09c
	goto_if_x_between2 0x160, 0x200, scr_800d09c
	call scr_800cde4
	BranchHighBit scr_800d09c, 0x4
	call scr_800ccf8
	BranchHighBit scr_800d09c, 0x4
	goto_if_x_between2 0x0, 0xa0, scr_800d09c
	goto_if_x_between2 0x158, 0x200, scr_800d09c
	goto scr_800cd58
	
scr_800d09c:
	return 
	
scr_800d0a0:
	branchIfParentHighFlag scr_800d0b0, 0x1
	CopyInitPos 
	
scr_800d0b0:
	showsprite 
	setParentHighFlag 0x4
	setParentHighFlag 0x1
	ClearBit7Field44 
	ClearField62 
	SetBit4Field44 
	disable_collision 
	call scr_800c64c
	ClearBit4Field44 
	enable_collision 
	ClearHighBit 0x4
	call scr_800d114
	call scr_800ca64
	clearParentHighFlag 0x1
	ClearHighBit 0x4
	goto scr_800c548
	
scr_800d114:
	showsprite 
	call scr_800d178
	SetBit0Init52 
	load_rand 0x4
	add_local 0x8
	
scr_800d134:
	call_if_attacked scr_800c5f0
	BranchHighBit scr_800d174, 0x4
	call scr_800d18c
	BranchHighBit scr_800d174, 0x4
	cmd_0E scr_800d174, 0x0
	wait_frame 
	goto scr_800d134
	
scr_800d174:
	return 
	
scr_800d178:
	cmd_A6 0x8f02
	set_anim 0x5
	return 
	
scr_800d18c:
	faceplayer 
	reset_velocity 
	play_sound 0x164
	cmd_3D_setVelXDirBased 0xe0
	goto_if_y_between 0x0, 0xaf, scr_800d1d0
	goto_if_y_between 0xb0, 0xef, scr_800d390
	goto_if_y_between 0xf0, 0x127, scr_800d3f0
	goto scr_800c918
	
scr_800d1d0:
	goto_if_y_between2 0xb0, 0x200, scr_800d1fc
	goto_if_difficulty 0x0, scr_800d1fc
	cmd_C0 scr_800d1fc, 0x32
	goto scr_800d220
	
scr_800d1fc:
	cmd_3D_setVelXDirBased 0x180
	cmd_D4 0x1400b8
	goto_if_y_between2 0x0, 0xa8, scr_800c968
	goto scr_800c9a8
	
scr_800d220:
	goto_if_y_between 0x0, 0x48, scr_800d278
	rng_switch 0x5, scr_800d248, scr_800d260, scr_800d278, scr_800d2f0, scr_800d2f0
	
scr_800d248:
	cmd_D4 0x1400b8
	cmd_D5 0x288001
	goto scr_800c968
	
scr_800d260:
	cmd_D4 0xf0080
	cmd_D5 0x1e8001
	goto scr_800c968
	
scr_800d278:
	faceplayer 
	goto_if_dir scr_800d2bc, 0x0
	goto_if_x_between2 0x0, 0x100, scr_800d2a8
	cmd_D7 0xb001b0, 0x1e0050
	goto scr_800c968
	
scr_800d2a8:
	cmd_D7 0xb00100, 0x140050
	goto scr_800c968
	
scr_800d2bc:
	goto_if_x_between2 0x100, 0x200, scr_800d2dc
	cmd_D7 0xb00050, 0x1e0050
	goto scr_800c968
	
scr_800d2dc:
	cmd_D7 0xb00100, 0x140050
	goto scr_800c968
	
scr_800d2f0:
	faceplayer 
	goto_if_dir scr_800d344, 0x0
	goto_if_x_between 0x180, 0x200, scr_800d248
	cmd_3E_Init30_28_ReverseMe 0x80, 0x400
	
scr_800d318:
	call_if_attacked scr_800c5f0
	BranchHighBit scr_800d388, 0x4
	goto_if_x_between 0x1a8, 0x1b8, scr_800d388
	wait_frame 
	goto scr_800d318
	
scr_800d344:
	goto_if_x_between 0x0, 0x80, scr_800d248
	cmd_3E_Init30_28_ReverseMe 0x80, 0x400
	
scr_800d35c:
	call_if_attacked scr_800c5f0
	BranchHighBit scr_800d388, 0x4
	goto_if_x_between 0x48, 0x58, scr_800d388
	wait_frame 
	goto scr_800d35c
	
scr_800d388:
	return 
	return 
	
scr_800d390:
	goto_if_y_between2 0x0, 0xaf, scr_800d3bc
	goto_if_y_between2 0xb0, 0xef, scr_800d3d0
	goto_if_y_between2 0xf0, 0x127, scr_800d3e0
	goto scr_800d3e0
	
scr_800d3bc:
	cmd_26 0xffffff9e, 0x700
	goto scr_800c968
	
scr_800d3d0:
	cmd_D4 0xf00f0
	goto scr_800c9a8
	
scr_800d3e0:
	cmd_D4 0xf00f8
	goto scr_800c9e4
	
scr_800d3f0:
	goto_if_y_between2 0x0, 0xaf, scr_800d41c
	goto_if_y_between2 0xb0, 0xef, scr_800d41c
	goto_if_y_between2 0xf0, 0x127, scr_800d430
	goto scr_800d440
	
scr_800d41c:
	cmd_26 0xffffff9e, 0x700
	goto scr_800c9a8
	
scr_800d430:
	cmd_D4 0xf0130
	goto scr_800c9e4
	
scr_800d440:
	cmd_D4 0xf0138
	goto scr_800ca20
	goto_if_y_between2 0x0, 0xaf, scr_800d47c
	goto_if_y_between2 0xb0, 0xef, scr_800d47c
	goto_if_y_between2 0xf0, 0x127, scr_800d47c
	goto scr_800d490
	
scr_800d47c:
	cmd_26 0xffffff9e, 0x700
	goto scr_800c9e4
	
scr_800d490:
	cmd_D4 0xf0178
	goto scr_800ca20
	
scr_800d4a0:
	freeze_player 
	hidesprite 
	wait_frame 
	
scr_800d4ac:
	BranchHighBit scr_800d4c4, 0x7
	wait_frame 
	goto scr_800d4ac
	
scr_800d4c4:
	blink_ent 
	wait_frames 0x3c
	play_sound 0x16a
	set_anim 0x7
	
scr_800d4e0:
	goto_if_anim_done scr_800d4f4
	wait_frame 
	goto scr_800d4e0
	
scr_800d4f4:
	set_anim 0xa
	
scr_800d4fc:
	goto_if_anim_done scr_800d510
	wait_frame 
	goto scr_800d4fc
	
scr_800d510:
	unfreeze_player 
	callasm (sub_8010D68+1)
	return 
	
scr_800d520:
	SetInitPos 0x15b, 0x50
	showsprite 
	set_anim 0x0
	wait_frame 
	wait_frames 0x1e
	callasm (sub_8022768+1)
	CopyInitPos 
	SetInitPos 0x15b, 0x50
	set_anim 0xc
	play_sound 0x162
	cmd_D7 0x90015b, 0x28007c
	wait_frames 0x28
	
scr_800d578:
	cmd_51 scr_800d58c
	wait_frame 
	goto scr_800d578
	
scr_800d58c:
	play_sound 0x163
	reset_velocity 
	set_anim 0x4
	
scr_800d5a0:
	goto_if_anim_done scr_800d5b4
	wait_frame 
	goto scr_800d5a0
	
scr_800d5b4:
	setParentHighFlag 0x7
	wait_frames 0x1e
	goto scr_800c548
