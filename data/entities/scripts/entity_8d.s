.globl entity_8d_scr
entity_8d_scr:
	
	cmd_A6 0x7f02
	set_dir 0x0
	set_anim 0x1
	SetBit4Field44 
	wait_frame 
	
scr_80088e0:
	set_anim 0x0
	
scr_80088e8:
	callasm (sub_8021C94+1)
	call_if_attacked scr_8008934
	branchIfParentHighFlag scr_8008964, 0x3
	branchIfParentHighFlag scr_8008a9c, 0x6
	branchIfParentHighFlag scr_8008ab8, 0x7
	branchIfParentHighFlag scr_8008b40, 0x8
	wait_frame 
	goto scr_80088e8
	
scr_8008934:
	ifChildFlag23 scr_8008960
	cmd_64 
	call scr_8008b14
	cmd_6C scr_8008a60
	play_sound 0x145
	setParentHighFlag 0x1
	
scr_8008960:
	return 
	
scr_8008964:
	set_anim 0x1
	
scr_800896c:
	goto_if_anim_done scr_8008980
	wait_frame 
	goto scr_800896c
	
scr_8008980:
	set_anim 0x2
	
scr_8008988:
	call_if_attacked scr_80089cc
	branchIfParentHighFlag scr_80089b0, 0x3
	branchIfParentHighFlag scr_80089bc, 0x3
	goto scr_80088e0
	
scr_80089b0:
	wait_frame 
	goto scr_8008988
	
scr_80089bc:
	disable_collision 
	
scr_80089c0:
	wait_frame 
	goto scr_80089c0
	
scr_80089cc:
	ifChildFlag23 scr_8008a20
	branchIfParentHighFlag scr_8008a20, 0x4
	cmd_64 
	call scr_8008b14
	cmd_6C scr_8008a24
	play_sound 0x145
	set_anim 0x3
	
scr_8008a04:
	goto_if_anim_done scr_8008a18
	wait_frame 
	goto scr_8008a04
	
scr_8008a18:
	set_anim 0x2
	
scr_8008a20:
	return 
	
scr_8008a24:
	play_sound 0x147
	freeze_player 
	setParentHighFlag 0x0
	set_anim 0x4
	
scr_8008a40:
	goto_if_anim_done scr_8008a54
	wait_frame 
	goto scr_8008a40
	
scr_8008a54:
	wait_frame 
	goto scr_8008a54
	
scr_8008a60:
	play_sound 0x147
	freeze_player 
	setParentHighFlag 0x0
	set_anim 0x5
	
scr_8008a7c:
	goto_if_anim_done scr_8008a90
	wait_frame 
	goto scr_8008a7c
	
scr_8008a90:
	wait_frame 
	goto scr_8008a90
	
scr_8008a9c:
	showsprite 
	disable_collision 
	set_anim 0x6
	
scr_8008aac:
	wait_frame 
	goto scr_8008aac
	
scr_8008ab8:
	offsetPosition2 0xfff0, 0x0
	
scr_8008ac0:
	branchIfParentHighFlag scr_8008a9c, 0x6
	branchIfParentHighFlag scr_8008ae8, 0x7
	offsetPosition2 0x10, 0x0
	goto scr_80088e0
	
scr_8008ae8:
	wait_frame 
	goto scr_8008ac0
	ifChildFlag23 scr_8008b10
	cmd_64 
	call scr_8008b14
	cmd_6C scr_8008a60
	
scr_8008b10:
	return 
	
scr_8008b14:
	cmd_D2 0x5a0059, 0x59
	ifChildFlag21 scr_8008b34
	play_sound 0x11e
	return 
	
scr_8008b34:
	play_sound 0x11d
	return 
	
scr_8008b40:
	disable_collision 
	
scr_8008b44:
	branchIfParentHighFlag scr_8008a9c, 0x6
	branchIfParentHighFlag scr_8008b6c, 0x8
	toggle_dir 
	enable_collision 
	goto scr_80088e0
	
scr_8008b6c:
	wait_frame 
	goto scr_8008b44
	
scr_8008b78:
	freeze_player 
	
scr_8008b7c:
	branchPlayerVulnerable scr_8008b90
	wait_frame 
	goto scr_8008b7c
	
scr_8008b90:
	FadeScreen 0x30, 0x3f, 0x0
	clear_local 
	
scr_8008b9c:
	for scr_8008bb4, 0x1e
	wait_frame 
	goto scr_8008b9c
	
scr_8008bb4:
	showsprite 
	reset_velocity 
	SetInit4E 0x1
	ClearBit0Init52 
	set_anim 0xb
	SetInitPos 0x80, 0x78
	set_dir 0x1
	set_ent_callback (sub_80211C0+1)
	cmd_C4 0x3f0030
	play_sound 0x184
	wait_frame 
	
scr_8008bfc:
	goto_if_ground scr_8008c10
	wait_frame 
	goto scr_8008bfc
	
scr_8008c10:
	reset_velocity 
	clear_ent_callback 
	set_anim 0xc
	complete_lvl 
	
scr_8008c24:
	wait_frame 
	goto scr_8008c24
	
scr_8008c30:
	freeze_player 
	SetHighBit 0xa
	SetHighBit 0x3
	callasm (sub_8021C44+1)
	SetInitPos 0xffe2, 0x40
	callasm (sub_8021250+1)
	SetInitPos 0xd0, 0x20
	hidesprite 
	wait_frame 
	
scr_8008c6c:
	BranchHighBit scr_8008c84, 0x5
	wait_frame 
	goto scr_8008c6c
	
scr_8008c84:
	ClearHighBit 0x5
	showsprite 
	enable_collision 
	ClearBit4Field44 
	call scr_80081c4
	set_anim 0x13
	ClearHighBit 0x3
	
scr_8008cb0:
	goto_if_anim_done scr_8008cc4
	wait_frame 
	goto scr_8008cb0
	
scr_8008cc4:
	play_sound 0x17c
	set_anim 0x2
	
scr_8008cd4:
	goto_if_anim_done scr_8008ce8
	wait_frame 
	goto scr_8008cd4
	
scr_8008ce8:
	set_anim 0x12
	unfreeze_player 
	ClearHighBit 0xa
	callasm (sub_8010D68+1)
	return 
	
scr_8008d08:
	reset_velocity 
	SetBit0Init52 
	play_sound 0x17e
	cmd_D5 0x5a0088
	callasm (sub_8021544+1)
	wait_frames 0x5a
	play_sound 0x17e
	callasm (sub_8021640+1)
	
scr_8008d40:
	for scr_8008d60, 0x95
	callasm (sub_8021660+1)
	wait_frame 
	goto scr_8008d40
	
scr_8008d60:
	play_sound 0x17e
	callasm (sub_80216E8+1)
	
scr_8008d70:
	cmd_50 scr_8008d84
	wait_frame 
	goto scr_8008d70
	
scr_8008d84:
	set_vel_x 0x0
	set_vel_y 0x0
	clear_local 
	wait_frames 0x50
	setParentHighFlag 0x5
	SetHighBit 0x5
	destroy 
	reset_velocity 
	wait_frames 0x50
	return 
	
	
	
	
