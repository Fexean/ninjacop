.globl entity_3a_scr
entity_3a_scr:
	
	call scr_8008c30
	set_anim 0x12
	wait_frame 
	clear_local 
	
scr_8007d28:
	ClearBit4Field44 
	enable_collision 
	clear_local 
	call scr_8008238
	
scr_8007d3c:
	BranchHighBit scr_8007dd0, 0x0
	BranchHighBit scr_8007d94, 0x1
	call_if_attacked scr_8007f24
	call scr_800824c
	goto_if_asm (sub_80218F0+1), scr_8007d8c
	goto_if_not_player_x scr_8007da8
	call scr_800857c
	wait_frame 
	goto scr_8007d3c
	
scr_8007d8c:
	goto scr_8007e44
	
scr_8007d94:
	call scr_80080ac
	wait_frame 
	goto scr_8007d3c
	
scr_8007da8:
	call scr_80084a0
	wait_frame 
	goto scr_8007d3c
	call scr_800857c
	wait_frame 
	goto scr_8007d3c
	
scr_8007dd0:
	set_vel_x 0x0
	disable_collision 
	SetBit4Field44 
	set_anim 0x0
	clear_local 
	
scr_8007dec:
	for scr_8007e04, 0x3c
	wait_frame 
	goto scr_8007dec
	
scr_8007e04:
	set_anim 0x9
	play_sound 0x178
	clear_local 
	
scr_8007e18:
	for scr_8007e30, 0x3c
	wait_frame 
	goto scr_8007e18
	
scr_8007e30:
	do_hostage_dmg 0xffb0
	
scr_8007e38:
	wait_frame 
	goto scr_8007e38
	
scr_8007e44:
	set_vel_x 0x0
	SetHighBit 0x3
	call scr_8008110
	callasm (sub_8021250+1)
	clear_local 
	
scr_8007e68:
	call_if_attacked scr_8008220
	for scr_8007e88, 0x3c
	wait_frame 
	goto scr_8007e68
	
scr_8007e88:
	hidesprite 
	disable_collision 
	SetBit4Field44 
	
scr_8007e94:
	BranchHighBit scr_8007eb8, 0x5
	BranchHighBit scr_80080e4, 0x4
	wait_frame 
	goto scr_8007e94
	
scr_8007eb8:
	ClearHighBit 0x5
	showsprite 
	enable_collision 
	ClearBit4Field44 
	call scr_80081c4
	BranchHighBit scr_8007dd0, 0x0
	set_anim 0x13
	ClearHighBit 0x3
	
scr_8007ef0:
	call_if_attacked scr_8008220
	goto_if_anim_done scr_8007f0c
	wait_frame 
	goto scr_8007ef0
	
scr_8007f0c:
	set_anim 0x12
	callasm (sub_8021960+1)
	goto scr_8007d28
	
scr_8007f24:
	ClearBit7Field44 
	set_vel_x 0x0
	BranchHighBit scr_8007dd0, 0x0
	BranchHighBit scr_8008004, 0x1
	ifChildFlag23 scr_8007f68
	BranchChildYRange scr_8007f68, 0x20, 0x64
	call scr_8008220
	return 
	
scr_8007f68:
	call scr_80088a4
	cmd_62 
	cmd_9C 
	cmd_6C scr_8008008
	cmd_DA unk_82dfc88
	cmd_C0 scr_8007f9c, 0x32
	SetHighBit 0xf
	
scr_8007f9c:
	ClearHighBit 0x9
	ifChildFlag21 scr_8008004
	ifChildFlag23 scr_8007fbc
	goto scr_8008004
	
scr_8007fbc:
	SetHighBit 0x9
	set_anim 0x3
	goto_if_player_x scr_8007fdc
	set_anim 0x4
	
scr_8007fdc:
	goto_if_attacked scr_8007f24
	goto_if_anim_done scr_8007ff8
	wait_frame 
	goto scr_8007fdc
	
scr_8007ff8:
	set_anim 0x12
	return 
	
scr_8008004:
	return 
	
scr_8008008:
	callasm (sub_8033184+1)
	SetHighBit 0x6
	SetBit4Field44 
	disable_collision 
	play_sound 0x182
	set_anim 0x5
	clear_local 
	
scr_8008034:
	for scr_800804c, 0x3c
	wait_frame 
	goto scr_8008034
	
scr_800804c:
	set_anim 0x6
	
scr_8008054:
	goto_if_anim_done scr_8008068
	wait_frame 
	goto scr_8008054
	
scr_8008068:
	play_sound 0x183
	cmd_48 0x0, 0x0, 0x0, 0x0, 0x70
	clear_local 
	
scr_8008088:
	for scr_80080a0, 0x8
	wait_frame 
	goto scr_8008088
	
scr_80080a0:
	hidesprite 
	goto scr_8008b78
	
scr_80080ac:
	set_vel_x 0x0
	ClearHighBit 0x1
	set_anim 0x14
	
scr_80080c4:
	goto_if_anim_done scr_80080d8
	wait_frame 
	goto scr_80080c4
	
scr_80080d8:
	set_anim 0x12
	return 
	
scr_80080e4:
	callasm (sub_8033184+1)
	clear_local 
	
scr_80080f0:
	for scr_8008108, 0x3c
	wait_frame 
	goto scr_80080f0
	
scr_8008108:
	goto scr_8008b78
	
scr_8008110:
	set_anim 0xd
	
scr_8008118:
	call_if_attacked scr_8008220
	goto_if_anim_done scr_8008134
	wait_frame 
	goto scr_8008118
	
scr_8008134:
	set_anim 0xe
	
scr_800813c:
	call_if_attacked scr_8008220
	goto_if_anim_done scr_8008158
	wait_frame 
	goto scr_800813c
	
scr_8008158:
	play_sound 0x179
	callasm (sub_802113C+1)
	clear_local 
	
scr_800816c:
	call_if_attacked scr_8008220
	for scr_800818c, 0x12
	wait_frame 
	goto scr_800816c
	
scr_800818c:
	callasm (sub_8021164+1)
	set_anim 0xf
	
scr_800819c:
	call_if_attacked scr_8008220
	goto_if_anim_done scr_80081b8
	wait_frame 
	goto scr_800819c
	
scr_80081b8:
	set_anim 0x10
	return 
	
scr_80081c4:
	play_sound 0x17a
	callasm (sub_8021198+1)
	set_anim 0x11
	
scr_80081dc:
	call_if_attacked scr_8008220
	goto_if_anim_done scr_80081f8
	wait_frame 
	goto scr_80081dc
	
scr_80081f8:
	clear_local 
	
scr_80081fc:
	call_if_attacked scr_8008220
	for scr_800821c, 0x12
	wait_frame 
	goto scr_80081fc
	
scr_800821c:
	return 
	
scr_8008220:
	cmd_BC 0x71, 0x0, 0x0
	play_sound 0x18d
	return 
	
scr_8008238:
	load_rand 0x78
	add_local 0x3c
	return 
	
scr_800824c:
	cmd_0E scr_800825c, 0x0
	return 
	
scr_800825c:
	SetHighBit 0x7
	set_vel_x 0x0
	cmd_94 scr_80082a0
	goto_if_rng scr_80082a0, 0x1e
	call scr_80082cc
	goto scr_80082a8
	call scr_8008404
	goto scr_80082a8
	
scr_80082a0:
	call scr_8008368
	
scr_80082a8:
	ClearHighBit 0x7
	BranchHighBit scr_8007dd0, 0x0
	set_anim 0x12
	goto scr_8008238
	
scr_80082cc:
	set_anim 0x1
	clear_local 
	
scr_80082d8:
	call_if_attacked scr_8007f24
	BranchHighBit scr_800835c, 0x9
	for scr_8008304, 0x31
	wait_frame 
	goto scr_80082d8
	
scr_8008304:
	play_sound 0x17c
	cmd_48 0x38, 0x20, 0x300, 0x0, 0x62
	cmd_48 0x18, 0x24, 0x0, 0x0, 0x6c
	
scr_8008334:
	call_if_attacked scr_8007f24
	BranchHighBit scr_800835c, 0x9
	goto_if_anim_done scr_800835c
	wait_frame 
	goto scr_8008334
	
scr_800835c:
	set_anim 0x12
	return 
	
scr_8008368:
	set_anim 0x2
	clear_local 
	
scr_8008374:
	call_if_attacked scr_8007f24
	BranchHighBit scr_80083f8, 0x9
	for scr_80083a0, 0x2c
	wait_frame 
	goto scr_8008374
	
scr_80083a0:
	play_sound 0x17d
	cmd_48 0x18, 0x0, 0x300, 0x0, 0x63
	cmd_48 0x18, 0x24, 0x0, 0x0, 0x6c
	
scr_80083d0:
	call_if_attacked scr_8007f24
	BranchHighBit scr_80083f8, 0x9
	goto_if_anim_done scr_80083f8
	wait_frame 
	goto scr_80083d0
	
scr_80083f8:
	set_anim 0x12
	return 
	
scr_8008404:
	set_anim 0x1
	clear_local 
	
scr_8008410:
	call_if_attacked scr_8007f24
	BranchHighBit scr_8008494, 0x9
	for scr_800843c, 0x27
	wait_frame 
	goto scr_8008410
	
scr_800843c:
	play_sound 0x17c
	cmd_48 0x38, 0x35, 0x219, 0x219, 0x64
	cmd_48 0x18, 0x24, 0x0, 0x0, 0x6c
	
scr_800846c:
	call_if_attacked scr_8007f24
	BranchHighBit scr_8008494, 0x9
	goto_if_anim_done scr_8008494
	wait_frame 
	goto scr_800846c
	
scr_8008494:
	set_anim 0x12
	return 
	
scr_80084a0:
	call scr_8008514
	goto_if_player_x scr_80084f8
	SetHighBit 0x8
	set_anim 0x7
	
scr_80084c0:
	call_if_attacked scr_8007f24
	BranchHighBit scr_80084f4, 0x9
	BranchHighBit scr_8007dd0, 0x0
	goto_if_anim_done scr_80084f4
	wait_frame 
	goto scr_80084c0
	
scr_80084f4:
	toggle_dir 
	
scr_80084f8:
	set_anim 0x12
	ClearHighBit 0x8
	ClearHighBit 0x1
	return 
	
scr_8008514:
	set_vel_x 0x0
	set_anim 0xa
	LoadDifficulty 0x3c, 0x1e, 0x1
	
scr_8008530:
	call_if_attacked scr_8007f24
	BranchHighBit scr_8008570, 0x9
	BranchHighBit scr_8007dd0, 0x0
	goto_if_player_x scr_8008570
	cmd_0E scr_8008570, 0x0
	wait_frame 
	goto scr_8008530
	
scr_8008570:
	ClearHighBit 0x9
	return 
	
scr_800857c:
	set_anim 0x8
	cmd_3D_setVelXDirBased 0x50
	goto_if_difficulty 0x0, scr_80085a0
	cmd_3D_setVelXDirBased 0x80
	
scr_80085a0:
	return 
	
	
	
	
