@	animations:
@	1 = idle
@	2 = turn around
@	3 = jump
@	4 = falling down
@	5 = landing
@	6 = damage taken
@	7 = damage taken 2
@	8 = falling down on back
@	9 = turn on its side
@	10 = lie on back


.globl entity_38_scr
entity_38_scr:
	
	wait_frame 
	
frog_main:
	goto_if_rng frog_jump_prepare, 0x28
	set_anim 0x1
	load_rand 0x1e
	add_local 0x3c
	
scr_800a0a8:
	goto_if_attacked scr_800a0d8
	cmd_0E scr_800a0c8, 0x0
	wait_frame 
	goto scr_800a0a8
	
scr_800a0c8:
	set_anim 0x0
	goto frog_main
	
scr_800a0d8:
	call scr_800a1c0
	goto frog_main
	
frog_jump_prepare:
	faceplayer 
	set_anim 0x3
	clear_local 
	
frog_jump_waitbefore:
	goto_if_attacked frog_death_start
	for frog_jump, 0xa
	wait_frame 
	goto frog_jump_waitbefore
	
frog_jump:
	cmd_3D_setVelXDirBased 0x180
	set_vel_y 0x600
	
frog_jump_waitfor_peak:
	goto_if_attacked frog_death_start
	goto_if_asm (entity_is_falling+1), frog_jump_falling
	wait_frame 
	goto frog_jump_waitfor_peak
	
frog_jump_falling:
	set_anim 0x4
	
frog_jump_falling2:
	goto_if_attacked frog_death_start
	goto_if_ground frog_jump_land
	wait_frame 
	goto frog_jump_falling2
	
frog_jump_land:
	set_vel_x 0x0
	set_vel_y 0x0
	set_anim 0x5
	
scr_800a184:
	goto_if_attacked frog_death_start
	goto_if_anim_done scr_800a1a0
	wait_frame 
	goto scr_800a184
	
scr_800a1a0:
	set_anim 0x0
	goto frog_main



frog_death_start:
	call scr_800a1c0
	goto frog_main
	
scr_800a1c0:
	cmd_D2 0x5a0059, 0x59
	set_vel_x 0x0
	set_vel_y 0x0
	cmd_60 
	cmd_6C scr_800a288
	play_sound 0x193
	ClearBit7Field44 
	SetBit4Field44 
	cmd_70 scr_800a238, 0x0
	set_anim 0x6
	
scr_800a20c:
	goto_if_attacked scr_800a1c0
	goto_if_anim_done scr_800a228
	wait_frame 
	goto scr_800a20c
	
scr_800a228:
	set_anim 0x0
	ClearBit4Field44 
	return 
	
scr_800a238:
	set_anim 0x7
	
scr_800a240:
	goto_if_attacked scr_800a1c0
	goto_if_ground scr_800a25c
	wait_frame 
	goto scr_800a240
	
scr_800a25c:
	goto_if_attacked scr_800a1c0
	goto_if_anim_done scr_800a278
	wait_frame 
	goto scr_800a25c
	
scr_800a278:
	set_anim 0x0
	ClearBit4Field44 
	return 
	
scr_800a288:
	play_sound 0x194
	SetBit4Field44 
	disable_collision 
	kill 
	cmd_70 scr_800a2c8, 0x0
	set_anim 0x8
	
scr_800a2b0:
	goto_if_anim_done scr_800a2c4
	wait_frame 
	goto scr_800a2b0
	
scr_800a2c4:
	destroy 
	
scr_800a2c8:
	set_anim 0x9
	
frog_falling:
	goto_if_ground frog_waitdeath
	wait_frame 
	goto frog_falling
	
frog_waitdeath:
	goto_if_anim_done scr_800a2f8
	wait_frame 
	goto frog_waitdeath
	
scr_800a2f8:
	destroy 
	
	
	
	
