.globl entity_12_scr
entity_12_scr:
	
	SetHighBit 0xf
	set_anim 0x0
	set_anim 0x0
	wait_frame 
	
scr_800200c:
	goto_if_far_player_x scr_8002060, 0x104
	goto_if_far_player_y scr_8002060, 0xb4
	showsprite 
	cmd_6E 
	set_vel_x 0x0
	set_anim 0x2
	wait_frame 
	goto_if_attacked scr_8002074
	goto_if_field44_bit6 scr_800207c
	cmd_7C scr_800207c
	goto scr_800200c
	
scr_8002060:
	hidesprite 
	cmd_6D 
	wait_frame 
	goto scr_800200c
	
scr_8002074:
	call scr_8003f6c
	
scr_800207c:
	create_child_ent 0x0, 0x22, 0x0, 0x0, 0x6e
	call scr_80020d4
	
scr_8002098:
	goto_if_attacked scr_8002074
	goto_if_field44_bit6 scr_800207c
	cmd_7A scr_800207c
	cmd_50 scr_80020cc
	call scr_8003f40
	wait_frame 
	goto scr_8002098
	
scr_80020cc:
	goto scr_800200c
	
scr_80020d4:
	faceplayer 
	call scr_80045ec
	cmd_91 
	ClearHighBit 0xf
	
scr_80020ec:
	clear_local 
	
scr_80020f0:
	call_if_attacked scr_80040dc
	call scr_8003ea8
	cmd_7D scr_8002134
	clear_local 
	goto_if_not_player_x scr_8002154
	goto_if_near_player_x scr_8002178, 0x64
	cmd_74 scr_8002178
	wait_frame 
	goto scr_80020f0
	
scr_8002134:
	for_not scr_8002168, 0x78
	cmd_95 scr_8002178
	wait_frame 
	goto scr_80020f0
	
scr_8002154:
	call scr_800459c
	wait_frame 
	goto scr_80020f0
	
scr_8002168:
	cmd_92 
	SetHighBit 0xf
	return 
	
scr_8002178:
	set_vel_x 0x0
	faceplayer 
	cmd_95 scr_80021d4
	cmd_94 scr_80021ac
	call scr_8004e3c
	call scr_80049cc
	goto scr_80021e4
	
scr_80021ac:
	call scr_80046ec
	call scr_8004e78
	call scr_8004afc
	call scr_8004720
	goto scr_80021e4
	
scr_80021d4:
	call scr_8004eb4
	call scr_8004c28
	
scr_80021e4:
	call scr_800466c
	call scr_800564c
	goto scr_80020ec
	
	
	
	
