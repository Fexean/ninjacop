.globl entity_3d_scr
entity_3d_scr:
	
	SetInit4E 0x1
	set_anim 0x0
	callasm (sub_80226E4+1)
	call scr_800d4a0
	goto scr_800bd74
	wait_frame 
	callasm (sub_8010D68+1)
	
scr_800bd74:
	wait_frame 
	
scr_800bd78:
	BranchHighBit scr_800c3a8, 0x2
	BranchHighBit scr_800c4c0, 0x4
	goto_if_y_between2 0xb0, 0x200, scr_800be10
	BranchHighBit scr_800bdc4, 0x3
	call scr_800be30
	goto_if_player_x scr_800bdf0
	wait_frame 
	goto scr_800bd78
	
scr_800bdc4:
	ClearHighBit 0x3
	goto_if_player_x scr_800bde4
	call scr_800c1c8
	goto scr_800bdf0
	
scr_800bde4:
	wait_frame 
	goto scr_800bd78
	
scr_800bdf0:
	goto_if_far_player_x scr_800be04, 0x88
	call scr_800c228
	
scr_800be04:
	wait_frame 
	goto scr_800bd78
	
scr_800be10:
	SetBit4Field44 
	disable_collision 
	call scr_800c418
	ClearBit4Field44 
	enable_collision 
	goto scr_800bd78
	
scr_800be30:
	ClearHighBit 0x8
	goto_if_difficulty 0x0, scr_800be5c
	goto_if_difficulty 0x1, scr_800be5c
	goto_if_rng scr_800c008, 0xa
	
scr_800be5c:
	goto_if_dir scr_800be7c, 0x0
	goto_if_x_between 0x188, 0x200, scr_800bfe8
	goto scr_800be88
	
scr_800be7c:
	goto_if_x_between 0x0, 0x78, scr_800bfe8
	
scr_800be88:
	set_anim 0x2
	
scr_800be90:
	call_if_attacked scr_800c218
	BranchHighBit scr_800c3a8, 0x2
	BranchHighBit scr_800c4c0, 0x4
	goto_if_anim_done scr_800bec4
	wait_frame 
	goto scr_800be90
	
scr_800bec4:
	play_sound 0x16b
	cmd_D8 0x300030, 0x18
	wait_frame 
	
scr_800bedc:
	SetHighBit 0x5
	clear_local 
	
scr_800bee8:
	call_if_attacked scr_800c218
	for scr_800bf08, 0x18
	wait_frame 
	goto scr_800bee8
	
scr_800bf08:
	set_anim 0x3
	
scr_800bf10:
	call_if_attacked scr_800c218
	goto_if_ground scr_800bf2c
	wait_frame 
	goto scr_800bf10
	
scr_800bf2c:
	clear_local 
	reset_velocity 
	ClearHighBit 0x5
	play_sound 0x16c
	set_anim 0x4
	BranchHighBit scr_800bf68, 0x8
	callasm (sub_8022A78+1)
	goto scr_800bf70
	
scr_800bf68:
	callasm (sub_8022A84+1)
	
scr_800bf70:
	call_if_attacked scr_800c218
	BranchHighBit scr_800c3a8, 0x2
	BranchHighBit scr_800c4c0, 0x4
	goto_if_anim_done scr_800bfa4
	wait_frame 
	goto scr_800bf70
	
scr_800bfa4:
	set_anim 0x0
	call_if_attacked scr_800c218
	BranchHighBit scr_800c3a8, 0x2
	BranchHighBit scr_800c4c0, 0x4
	for scr_800bfe4, 0x1e
	wait_frame 
	goto scr_800bfa4
	
scr_800bfe4:
	return 
	
scr_800bfe8:
	goto_if_not_player_x scr_800c1c8
	goto_if_y_between2 0x0, 0x48, scr_800c1c8
	ClearHighBit 0x5
	return 
	
scr_800c008:
	SetHighBit 0x8
	goto_if_player_x scr_800c020
	call scr_800c1c8
	
scr_800c020:
	set_anim 0x2
	clear_local 
	
scr_800c02c:
	call_if_attacked scr_800c218
	BranchHighBit scr_800c3a8, 0x2
	BranchHighBit scr_800c4c0, 0x4
	for scr_800c064, 0x6
	wait_frame 
	goto scr_800c02c
	
scr_800c064:
	setFlag10 
	clear_local 
	
scr_800c06c:
	call_if_attacked scr_800c218
	BranchHighBit scr_800c3a8, 0x2
	BranchHighBit scr_800c4c0, 0x4
	for scr_800c0a4, 0x1e
	wait_frame 
	goto scr_800c06c
	
scr_800c0a4:
	clearFlag10 
	
scr_800c0a8:
	call_if_attacked scr_800c218
	BranchHighBit scr_800c3a8, 0x2
	BranchHighBit scr_800c4c0, 0x4
	goto_if_anim_done scr_800c0dc
	wait_frame 
	goto scr_800c0a8
	
scr_800c0dc:
	play_sound 0x16b
	goto_if_x_between2 0x0, 0x8f, scr_800c134
	goto_if_x_between2 0x90, 0xbf, scr_800c148
	goto_if_x_between2 0xc0, 0xef, scr_800c15c
	goto_if_x_between2 0xf0, 0x11f, scr_800c170
	goto_if_x_between2 0x120, 0x14f, scr_800c184
	goto_if_x_between2 0x150, 0x187, scr_800c198
	goto scr_800c1ac
	
scr_800c134:
	cmd_D7 0xb00070, 0x1e0050
	goto scr_800c1c0
	
scr_800c148:
	cmd_D7 0xb000a0, 0x1e0050
	goto scr_800c1c0
	
scr_800c15c:
	cmd_D7 0xb000d0, 0x1e0050
	goto scr_800c1c0
	
scr_800c170:
	cmd_D7 0xb00100, 0x1e0050
	goto scr_800c1c0
	
scr_800c184:
	cmd_D7 0xb00130, 0x1e0050
	goto scr_800c1c0
	
scr_800c198:
	cmd_D7 0xb00160, 0x1e0050
	goto scr_800c1c0
	
scr_800c1ac:
	cmd_D7 0xb00190, 0x1e0050
	goto scr_800c1c0
	
scr_800c1c0:
	goto scr_800bedc
	
scr_800c1c8:
	set_anim 0x10
	
scr_800c1d0:
	call_if_attacked scr_800c218
	BranchHighBit scr_800c3a8, 0x2
	BranchHighBit scr_800c4c0, 0x4
	goto_if_anim_done scr_800c204
	wait_frame 
	goto scr_800c1d0
	
scr_800c204:
	callasm (sub_8022820+1)
	set_anim 0x0
	return 
	
scr_800c218:
	cmd_BC 0x58, 0x0, 0x0
	return 
	
scr_800c228:
	goto_if_near_player_x scr_800c388, 0x1f
	goto_if_not_player_x scr_800c388
	goto_if_difficulty 0x1, scr_800c260
	goto_if_difficulty 0x2, scr_800c260
	goto_if_y_between2 0x0, 0x48, scr_800c388
	
scr_800c260:
	play_sound 0x16a
	set_anim 0x7
	
scr_800c270:
	call_if_attacked scr_800c218
	BranchHighBit scr_800c3a8, 0x2
	BranchHighBit scr_800c4c0, 0x4
	goto_if_anim_done scr_800c2a4
	wait_frame 
	goto scr_800c270
	
scr_800c2a4:
	goto_if_near_player_x scr_800c314, 0x1f
	goto_if_not_player_x scr_800c314
	callasm (sub_8022854+1)
	clear_local 
	call scr_8005584
	play_sound 0x16d
	
scr_800c2d4:
	call_if_attacked scr_800c218
	BranchHighBit scr_800c3a8, 0x2
	BranchHighBit scr_800c4c0, 0x4
	for scr_800c314, 0xb4
	call scr_800c38c
	wait_frame 
	goto scr_800c2d4
	
scr_800c314:
	set_anim 0xa
	
scr_800c31c:
	call_if_attacked scr_800c218
	BranchHighBit scr_800c3a8, 0x2
	BranchHighBit scr_800c4c0, 0x4
	goto_if_anim_done scr_800c350
	wait_frame 
	goto scr_800c31c
	
scr_800c350:
	call_if_attacked scr_800c218
	BranchHighBit scr_800c3a8, 0x2
	BranchHighBit scr_800c4c0, 0x4
	for scr_800c388, 0x3c
	wait_frame 
	goto scr_800c350
	
scr_800c388:
	return 
	
scr_800c38c:
	for scr_800c39c, 0x8
	return 
	
scr_800c39c:
	callasm (sub_80228A4+1)
	return 
	
scr_800c3a8:
	set_anim 0xe
	SetBit4Field44 
	disable_collision 
	
scr_800c3b8:
	goto_if_anim_done scr_800c3cc
	wait_frame 
	goto scr_800c3b8
	
scr_800c3cc:
	BranchHighBit scr_800c3e4, 0x6
	wait_frame 
	goto scr_800c3cc
	
scr_800c3e4:
	play_sound 0x16f
	blink_ent 
	clear_local 
	wait_frames 0x1e
	hidesprite 
	complete_lvl 
	SetHighBit 0x0
	
scr_800c40c:
	wait_frame 
	goto scr_800c40c
	
scr_800c418:
	set_anim 0xb
	
scr_800c420:
	call_if_attacked scr_800c218
	BranchHighBit scr_800c3a8, 0x2
	goto_if_anim_done scr_800c448
	wait_frame 
	goto scr_800c420
	
scr_800c448:
	SetHighBit 0x1
	
scr_800c450:
	set_anim 0xc
	
scr_800c458:
	call_if_attacked scr_800c218
	BranchHighBit scr_800c3a8, 0x2
	BranchNotHighBit scr_800c484, 0x1
	wait_frame 
	goto scr_800c458
	
scr_800c484:
	set_anim 0xd
	
scr_800c48c:
	call_if_attacked scr_800c218
	BranchHighBit scr_800c3a8, 0x2
	goto_if_anim_done scr_800c4b4
	wait_frame 
	goto scr_800c48c
	
scr_800c4b4:
	set_anim 0x0
	return 
	
scr_800c4c0:
	ClearField62 
	disable_collision 
	SetBit4Field44 
	ClearHighBit 0x4
	set_anim 0xe
	
scr_800c4dc:
	call_if_attacked scr_800c218
	BranchHighBit scr_800c3a8, 0x2
	goto_if_anim_done scr_800c504
	wait_frame 
	goto scr_800c4dc
	
scr_800c504:
	call scr_800c450
	enable_collision 
	ClearBit4Field44 
	ClearField62 
	goto scr_800bd78
	
	
	
	
