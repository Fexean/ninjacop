.globl entity_6f_scr
entity_6f_scr:
	
	SetBit4Field44 
	disable_collision 
	SetBit0Init52 
	set_dir 0x1
	
scr_8001580:
	clear_local 
	switch_language scr_8001594, scr_80015a4, scr_80015b4
	
scr_8001594:
	set_anim 0x0
	goto scr_80015c4
	
scr_80015a4:
	set_anim 0x1
	goto scr_80015c4
	
scr_80015b4:
	set_anim 0x2
	goto scr_80015c4
	
scr_80015c4:
	showsprite 
	for scr_8001604, 0x1e
	branchIfParentHighFlag scr_8001644, 0x2
	branchIfParentHighFlag scr_8001678, 0x1
	branchIfParentHighFlag scr_8001764, 0x0
	wait_frame 
	goto scr_80015c4
	
scr_8001604:
	hidesprite 
	for scr_80015c4, 0xa
	branchIfParentHighFlag scr_8001644, 0x2
	branchIfParentHighFlag scr_8001678, 0x1
	branchIfParentHighFlag scr_8001764, 0x0
	wait_frame 
	goto scr_8001604
	
scr_8001644:
	hidesprite 
	branchNotParentHighFlag scr_80015c4, 0x2
	branchIfParentHighFlag scr_8001678, 0x1
	branchIfParentHighFlag scr_8001764, 0x0
	wait_frame 
	goto scr_8001644
	
scr_8001678:
	clear_local 
	switch_language scr_800168c, scr_800169c, scr_80016ac
	
scr_800168c:
	set_anim 0x3
	goto scr_80016bc
	
scr_800169c:
	set_anim 0x4
	goto scr_80016bc
	
scr_80016ac:
	set_anim 0x5
	goto scr_80016bc
	
scr_80016bc:
	showsprite 
	branchIfParentHighFlag scr_8001730, 0x2
	branchNotParentHighFlag scr_8001580, 0x1
	branchIfParentHighFlag scr_8001764, 0x0
	wait_frame 
	goto scr_80016bc
	
scr_80016f0:
	hidesprite 
	for scr_80016bc, 0xa
	branchIfParentHighFlag scr_8001730, 0x2
	branchNotParentHighFlag scr_8001580, 0x1
	branchIfParentHighFlag scr_8001764, 0x0
	wait_frame 
	goto scr_80016f0
	
scr_8001730:
	hidesprite 
	branchNotParentHighFlag scr_80016bc, 0x2
	branchNotParentHighFlag scr_8001580, 0x1
	branchIfParentHighFlag scr_8001764, 0x0
	wait_frame 
	goto scr_8001730
	
scr_8001764:
	destroy 
	
	
	
	
