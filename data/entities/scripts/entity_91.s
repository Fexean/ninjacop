.globl entity_91_scr
entity_91_scr:
	
	cmd_A6 0xa202
	set_anim 0x0
	
scr_800a940:
	call_if_attacked scr_800aa6c
	wait_frame 
	goto scr_800a940
	
scr_800a954:
	set_anim 0x1
	create_child_ent 0xfff3, 0x1b, 0x0, 0x0, 0x92
	create_child_ent 0x8, 0xe, 0x0, 0x0, 0x93
	
scr_800a984:
	call_if_attacked scr_800aacc
	wait_frame 
	goto scr_800a984
	
scr_800a998:
	set_anim 0x2
	create_child_ent 0x8, 0x15, 0x0, 0x0, 0x92
	create_child_ent 0xfffd, 0xf, 0x0, 0x0, 0x93
	
scr_800a9c8:
	call_if_attacked scr_800ab2c
	wait_frame 
	goto scr_800a9c8
	
scr_800a9dc:
	set_anim 0x3
	create_child_ent 0xfff8, 0x13, 0x0, 0x0, 0x92
	create_child_ent 0xfff6, 0xc, 0x0, 0x0, 0x93
	
scr_800aa0c:
	call_if_attacked scr_800ab8c
	wait_frame 
	goto scr_800aa0c
	
scr_800aa20:
	callasm (sub_8032DC8+1)
	callasm (sub_8033184+1)
	freeze_player 
	SetBit4Field44 
	disable_collision 
	SetHighBit 0x0
	call scr_800ac24
	set_anim 0x4
	wait_frames 0x3c
	complete_lvl 
	
scr_800aa60:
	wait_frame 
	goto scr_800aa60
	
scr_800aa6c:
	ClearBit7Field44 
	ifChildFlag23 scr_800aac8
	play_sound_cond 0xf1, 0x1b2, 0x0
	cmd_D2 0x5a0058, 0x0
	ifChildFlag21 scr_800aac8
	cmd_60 
	cmd_BF scr_800aab4, 0x4b
	call scr_800abe8
	return 
	
scr_800aab4:
	call scr_800abf8
	ClearField62 
	goto scr_800a954
	
scr_800aac8:
	return 
	
scr_800aacc:
	ClearBit7Field44 
	ifChildFlag23 scr_800ab28
	play_sound_cond 0xf1, 0x1b2, 0x0
	cmd_D2 0x5a0058, 0x0
	ifChildFlag21 scr_800ab28
	cmd_60 
	cmd_BF scr_800ab14, 0x32
	call scr_800abe8
	return 
	
scr_800ab14:
	call scr_800abf8
	ClearField62 
	goto scr_800a998
	
scr_800ab28:
	return 
	
scr_800ab2c:
	ClearBit7Field44 
	ifChildFlag23 scr_800ab88
	play_sound_cond 0xf1, 0x1b2, 0x0
	cmd_D2 0x5a0058, 0x0
	ifChildFlag21 scr_800ab88
	cmd_60 
	cmd_BF scr_800ab74, 0x19
	call scr_800abe8
	return 
	
scr_800ab74:
	call scr_800abf8
	ClearField62 
	goto scr_800a9dc
	
scr_800ab88:
	return 
	
scr_800ab8c:
	ClearBit7Field44 
	ifChildFlag23 scr_800abe4
	play_sound_cond 0xf1, 0x1b2, 0x0
	cmd_D2 0x5a0058, 0x0
	ifChildFlag21 scr_800abe4
	cmd_60 
	cmd_6C scr_800abd0
	call scr_800abe8
	return 
	
scr_800abd0:
	call scr_800abf8
	ClearField62 
	goto scr_800aa20
	
scr_800abe4:
	return 
	
scr_800abe8:
	cmd_BC 0x95, 0x0, 0x0
	return 
	
scr_800abf8:
	cmd_BC 0x95, 0x0, 0x0
	create_child_ent 0x0, 0x0, 0x0, 0x0, 0x74
	play_sound 0x19f
	return 
	
scr_800ac24:
	cmd_9B 0x95, 0x10
	cmd_9B 0x74, 0x0
	play_sound 0x1b3
	wait_frames 0x1e
	cmd_9B 0xfff80095, 0x18
	cmd_9B 0xfff80074, 0x8
	play_sound 0x1b3
	wait_frames 0x1e
	cmd_9B 0x80095, 0x8
	cmd_9B 0x80074, 0xfff8
	play_sound 0x1b3
	wait_frames 0x1e
	cmd_9B 0x80095, 0x18
	cmd_9B 0x80074, 0x8
	play_sound 0x1b3
	wait_frames 0x1e
	cmd_9B 0xfff80095, 0x8
	cmd_9B 0xfff80074, 0xfff8
	play_sound 0x1b3
	wait_frames 0x19
	cmd_9B 0x95, 0x18
	cmd_9B 0x74, 0x8
	play_sound 0x1b3
	wait_frames 0x14
	cmd_9B 0x80095, 0x10
	cmd_9B 0x80074, 0x0
	play_sound 0x1b3
	wait_frames 0x14
	cmd_9B 0x95, 0x8
	cmd_9B 0x74, 0xfff8
	play_sound 0x1b3
	wait_frames 0x14
	cmd_9B 0xfff80095, 0x10
	cmd_9B 0xfff80074, 0x0
	play_sound 0x1b3
	wait_frames 0x1
	return 
	
	
	
	
