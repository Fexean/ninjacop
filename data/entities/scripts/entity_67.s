.globl entity_67_scr
entity_67_scr:
	
	SetBit4Field44 
	disable_collision 
	ClearBit0Init52 
	cmd_3D_setVelXDirBased 0x100
	SetInit4E 0x0
	wait_frame 
	
scr_800bbdc:
	goto_if_ground scr_800bbf0
	wait_frame 
	goto scr_800bbdc
	
scr_800bbf0:
	reset_velocity 
	set_anim 0x1
	
scr_800bbfc:
	wait_frame 
	goto scr_800bbfc
	
scr_800bc08:
	call_if_attacked scr_800b6e4
	goto_if_anim_done scr_800bc24
	wait_frame 
	goto scr_800bc08
	
scr_800bc24:
	return 
	
scr_800bc28:
	set_dir 0x1
	cmd_48 0x10, 0xffdd, 0x0, 0x0, 0x67
	set_dir 0x0
	cmd_48 0x10, 0xffdd, 0x0, 0x0, 0x67
	set_dir 0x1
	return 
	
scr_800bc6c:
	freeze_player 
	SetHighBit 0x4
	hidesprite 
	wait_frame 
	play_sound 0x1af
	callasm (sub_8022658+1)
	wait_frames 0xb4
	ClearHighBit 0x4
	showsprite 
	play_sound 0x190
	set_anim 0x3
	wait_frames 0x3c
	set_anim 0x4
	
scr_800bcc4:
	goto_if_anim_done scr_800bcd8
	wait_frame 
	goto scr_800bcc4
	
scr_800bcd8:
	set_anim 0x0
	unfreeze_player 
	callasm (sub_8010D68+1)
	return 
	
scr_800bcf0:
	SetBit4Field44 
	disable_collision 
	cmd_3C_modifyDir 
	
scr_800bcfc:
	cmd_51 scr_800bd10
	wait_frame 
	goto scr_800bcfc
	
scr_800bd10:
	callasm (sub_80226D4+1)
	reset_velocity 
	
scr_800bd1c:
	branchNotParentHighFlag scr_800bd34, 0x4
	wait_frame 
	goto scr_800bd1c
	
scr_800bd34:
	wait_frames 0x14
	destroy 
	
	
	
	
