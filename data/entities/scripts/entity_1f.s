.globl entity_1f_scr
entity_1f_scr:
	
	SetHighBit 0xf
	set_anim 0x0
	wait_frame 
	
scr_8002e24:
	goto_if_far_player_x scr_8002e78, 0x104
	goto_if_far_player_y scr_8002e78, 0xb4
	showsprite 
	cmd_6E 
	set_vel_x 0x0
	set_anim 0x2
	wait_frame 
	goto_if_attacked scr_8002e8c
	goto_if_field44_bit6 scr_8002e94
	cmd_7C scr_8002e94
	goto scr_8002e24
	
scr_8002e78:
	hidesprite 
	cmd_6D 
	wait_frame 
	goto scr_8002e24
	
scr_8002e8c:
	call scr_8003f6c
	
scr_8002e94:
	create_child_ent 0x0, 0x22, 0x0, 0x0, 0x6e
	call scr_8002eec
	
scr_8002eb0:
	goto_if_attacked scr_8002e8c
	goto_if_field44_bit6 scr_8002e94
	cmd_7A scr_8002e94
	cmd_50 scr_8002ee4
	call scr_8003f40
	wait_frame 
	goto scr_8002eb0
	
scr_8002ee4:
	goto scr_8002e24
	
scr_8002eec:
	faceplayer 
	cmd_91 
	ClearHighBit 0xf
	clear_local 
	
scr_8002f00:
	call_if_attacked scr_80040dc
	call scr_8003ea8
	cmd_7D scr_8002f3c
	clear_local 
	goto_if_not_player_x scr_8002f54
	goto_if_near_player_x scr_8002f78, 0x60
	wait_frame 
	goto scr_8002f00
	
scr_8002f3c:
	for_not scr_8002f68, 0x78
	wait_frame 
	goto scr_8002f00
	
scr_8002f54:
	call scr_800459c
	wait_frame 
	goto scr_8002f00
	
scr_8002f68:
	cmd_92 
	SetHighBit 0xf
	return 
	
scr_8002f78:
	clear_local 
	
scr_8002f7c:
	call_if_attacked scr_80040dc
	goto_if_far_player_x scr_8002fcc, 0x30
	goto_if_far_player_x scr_8002eec, 0x60
	cmd_7D scr_8002eec
	faceplayer 
	toggle_dir 
	call scr_8003efc
	for scr_8002fcc, 0x78
	wait_frame 
	goto scr_8002f7c
	
scr_8002fcc:
	set_vel_x 0x0
	faceplayer 
	goto_if_far_player_y scr_800301c, 0x20
	cmd_94 scr_8002ffc
	call scr_80052ac
	goto scr_800302c
	
scr_8002ffc:
	call scr_80046ec
	call scr_800537c
	call scr_8004720
	goto scr_800302c
	
scr_800301c:
	cmd_7F scr_800302c
	call scr_8005444
	
scr_800302c:
	set_anim 0x0
	call scr_800564c
	goto scr_8002eec
	
	
	
	
