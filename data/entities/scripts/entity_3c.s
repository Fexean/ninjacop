.globl entity_3c_scr
entity_3c_scr:
	
	call scr_800bc6c
	wait_frame 
	call scr_800b3f0
	goto scr_800aed8
	
scr_800aed8:
	call scr_800b6b8
	goto_if_y_between2 0x0, 0x40, scr_800af00
	goto_if_y_between2 0x48, 0x90, scr_800af1c
	goto scr_800afc0
	
scr_800af00:
	goto_if_rng scr_800aed8, 0x32
	call scr_800b3f0
	goto scr_800aed8
	
scr_800af1c:
	goto_if_asm (sub_8021F74+1), scr_800af34
	goto_if_rng scr_800af48, 0x28
	
scr_800af34:
	goto_if_x_between2 0x0, 0x78, scr_800af58
	goto scr_800af74
	
scr_800af48:
	call scr_800b3f0
	goto scr_800aed8
	
scr_800af58:
	goto_if_rng scr_800af90, 0x14
	call scr_800b224
	goto scr_800b0fc
	
scr_800af74:
	goto_if_rng scr_800afa8, 0x14
	call scr_800b1f8
	goto scr_800b000
	
scr_800af90:
	call scr_800b1f8
	call scr_800b480
	goto scr_800b000
	
scr_800afa8:
	call scr_800b224
	call scr_800b480
	goto scr_800b0fc
	
scr_800afc0:
	goto_if_asm (sub_8021F74+1), scr_800aff0
	goto_if_rng scr_800afe0, 0x28
	goto scr_800aff0
	
scr_800afe0:
	call scr_800b3f0
	goto scr_800aed8
	
scr_800aff0:
	call scr_800b480
	goto scr_800aed8
	
scr_800b000:
	call scr_800b6b8
	goto_if_y_between2 0x0, 0x40, scr_800b028
	goto_if_y_between2 0x48, 0x90, scr_800b044
	goto scr_800b0a0
	
scr_800b028:
	goto_if_rng scr_800b000, 0x32
	call scr_800b3f0
	goto scr_800b000
	
scr_800b044:
	goto_if_asm (sub_8021F74+1), scr_800b05c
	goto_if_rng scr_800b070, 0x1e
	
scr_800b05c:
	goto_if_x_between2 0x80, 0x100, scr_800b090
	goto scr_800b080
	
scr_800b070:
	call scr_800b3f0
	goto scr_800b000
	
scr_800b080:
	call scr_800b480
	goto scr_800b000
	
scr_800b090:
	call scr_800b2a8
	goto scr_800b0fc
	
scr_800b0a0:
	goto_if_asm (sub_8021F74+1), scr_800b0d0
	goto_if_rng scr_800b0c0, 0x28
	goto scr_800b0d0
	
scr_800b0c0:
	call scr_800b3f0
	goto scr_800b000
	
scr_800b0d0:
	call scr_800b250
	goto_if_rng scr_800b0ec, 0x14
	goto scr_800aed8
	
scr_800b0ec:
	call scr_800b480
	goto scr_800aed8
	
scr_800b0fc:
	call scr_800b6b8
	goto_if_y_between2 0x0, 0x40, scr_800b124
	goto_if_y_between2 0x48, 0x90, scr_800b140
	goto scr_800b19c
	
scr_800b124:
	goto_if_rng scr_800b0fc, 0x32
	call scr_800b3f0
	goto scr_800b0fc
	
scr_800b140:
	goto_if_asm (sub_8021F74+1), scr_800b158
	goto_if_rng scr_800b16c, 0x1e
	
scr_800b158:
	goto_if_x_between2 0x0, 0x78, scr_800b18c
	goto scr_800b17c
	
scr_800b16c:
	call scr_800b3f0
	goto scr_800b0fc
	
scr_800b17c:
	call scr_800b480
	goto scr_800b0fc
	
scr_800b18c:
	call scr_800b2b8
	goto scr_800b000
	
scr_800b19c:
	goto_if_asm (sub_8021F74+1), scr_800b1cc
	goto_if_rng scr_800b1bc, 0x28
	goto scr_800b1cc
	
scr_800b1bc:
	call scr_800b3f0
	goto scr_800b0fc
	
scr_800b1cc:
	call scr_800b27c
	goto_if_rng scr_800b1e8, 0x14
	goto scr_800aed8
	
scr_800b1e8:
	call scr_800b480
	goto scr_800aed8
	
scr_800b1f8:
	call scr_800b394
	cmd_26 0x62, 0xa00
	set_vel_x 0xfffffe20
	set_vel_y 0xfffff693
	goto scr_800b2f0
	
scr_800b224:
	call scr_800b394
	cmd_26 0x62, 0xa00
	set_vel_x 0x1e0
	set_vel_y 0xfffff693
	goto scr_800b318
	
scr_800b250:
	call scr_800b394
	cmd_26 0x62, 0xa00
	set_vel_x 0x1e0
	set_vel_y 0xfffffbb0
	goto scr_800b2c8
	
scr_800b27c:
	call scr_800b394
	cmd_26 0x62, 0xa00
	set_vel_x 0xfffffe20
	set_vel_y 0xfffffbb0
	goto scr_800b2c8
	
scr_800b2a8:
	call scr_800b250
	goto scr_800b224
	
scr_800b2b8:
	call scr_800b27c
	goto scr_800b1f8
	
scr_800b2c8:
	call_if_attacked scr_800b3d0
	goto_if_x_between 0x7f, 0x81, scr_800b340
	goto_if_collide scr_800b340
	wait_frame 
	goto scr_800b2c8
	
scr_800b2f0:
	call_if_attacked scr_800b3d0
	goto_if_x_between 0x3f, 0x41, scr_800b340
	goto_if_collide scr_800b340
	wait_frame 
	goto scr_800b2f0
	
scr_800b318:
	call_if_attacked scr_800b3d0
	goto_if_x_between 0xbf, 0xc1, scr_800b340
	goto_if_collide scr_800b340
	wait_frame 
	goto scr_800b318
	
scr_800b340:
	ClearBit0Init52 
	set_vel_x 0x0
	
scr_800b34c:
	call_if_attacked scr_800b3d0
	goto_if_collide scr_800b368
	wait_frame 
	goto scr_800b34c
	
scr_800b368:
	reset_velocity 
	set_anim 0x4
	
scr_800b374:
	call_if_attacked scr_800b6e4
	goto_if_anim_done scr_800b390
	wait_frame 
	goto scr_800b374
	
scr_800b390:
	return 
	
scr_800b394:
	ClearBit0Init52 
	set_anim 0x2
	
scr_800b3a0:
	call_if_attacked scr_800b3d0
	goto_if_anim_done scr_800b3bc
	wait_frame 
	goto scr_800b3a0
	
scr_800b3bc:
	set_anim 0x3
	play_sound 0x190
	return 
	
scr_800b3d0:
	ClearBit7Field44 
	cmd_BC 0x58, 0x0, 0x0
	play_sound_cond 0xf1, 0xf3, 0xf3
	return 
	
scr_800b3f0:
	set_anim 0x1
	
scr_800b3f8:
	call_if_attacked scr_800b6e4
	for scr_800b418, 0x24
	wait_frame 
	goto scr_800b3f8
	
scr_800b418:
	play_sound 0x171
	
scr_800b420:
	call_if_attacked scr_800b6e4
	goto_if_anim_done scr_800b43c
	wait_frame 
	goto scr_800b420
	
scr_800b43c:
	call scr_800b458
	set_anim 0x0
	ClearHighBit 0x7
	return 
	
scr_800b458:
	goto_if_y_between2 0x98, 0x100, scr_800b470
	callasm (sub_80221B0+1)
	return 
	
scr_800b470:
	callasm (sub_8022204+1)
	return 
	return 
	
scr_800b480:
	ClearHighBit 0x2
	ClearHighBit 0x3
	ClearHighBit 0x6
	ClearHighBit 0x5
	goto_if_far_player_y scr_800b504, 0x48
	call scr_800b5fc
	set_anim 0x5
	
scr_800b4bc:
	call_if_attacked scr_800b6e4
	goto_if_anim_done scr_800b4d8
	wait_frame 
	goto scr_800b4bc
	
scr_800b4d8:
	SetHighBit 0x3
	call scr_800b620
	goto scr_800b634
	ClearHighBit 0x1
	set_anim 0x0
	return 
	
scr_800b504:
	SetHighBit 0x6
	call scr_800b5fc
	set_anim 0x8
	call scr_800bc08
	set_vel_y 0xfffffa78
	cmd_26 0x62, 0x700
	clear_local 
	
scr_800b53c:
	call_if_attacked scr_800b6e4
	for scr_800b55c, 0xe
	wait_frame 
	goto scr_800b53c
	
scr_800b55c:
	reset_velocity 
	set_anim 0x9
	clear_local 
	
scr_800b56c:
	call_if_attacked scr_800b6e4
	for scr_800b58c, 0xe
	wait_frame 
	goto scr_800b56c
	
scr_800b58c:
	SetHighBit 0x3
	call scr_800b620
	call scr_800bc08
	cmd_26 0x62, 0x700
	
scr_800b5b0:
	call_if_attacked scr_800b6e4
	goto_if_collide scr_800b5cc
	wait_frame 
	goto scr_800b5b0
	
scr_800b5cc:
	reset_velocity 
	set_anim 0xa
	call scr_800bc08
	goto scr_800b634
	ClearHighBit 0x1
	set_anim 0x0
	return 
	
scr_800b5fc:
	goto_if_y_between 0xa8, 0x100, scr_800b614
	callasm (sub_8022258+1)
	return 
	
scr_800b614:
	callasm (sub_8022318+1)
	return 
	
scr_800b620:
	play_sound 0x18e
	SetHighBit 0x5
	return 
	
scr_800b634:
	set_anim 0x6
	
scr_800b63c:
	call_if_attacked scr_800b6e4
	BranchHighBit scr_800b658, 0x3
	goto scr_800b664
	
scr_800b658:
	wait_frame 
	goto scr_800b63c
	
scr_800b664:
	set_anim 0x7
	
scr_800b66c:
	call_if_attacked scr_800b6e4
	goto_if_anim_done scr_800b688
	wait_frame 
	goto scr_800b66c
	
scr_800b688:
	set_anim 0x0
	return 
	ClearHighBit 0x1
	BranchHighBit scr_800b66c, 0x3
	set_anim 0x6
	goto scr_800b63c
	
scr_800b6b8:
	callasm (sub_80225C8+1)
	
scr_800b6c0:
	call_if_attacked scr_800b6e4
	goto_if_asm (sub_8022634+1), scr_800b6e0
	wait_frame 
	goto scr_800b6c0
	
scr_800b6e0:
	return 
	
scr_800b6e4:
	ClearHighBit 0x1
	cmd_62 
	cmd_9C 
	cmd_D2 0x5a0059, 0x59
	play_sound_cond 0x173, 0x174, 0x175
	cmd_6C scr_800b8c8
	ClearBit7Field44 
	BranchHighBit scr_800b72c, 0x7
	ifChildFlag22 scr_800b738
	
scr_800b72c:
	callasm (sub_8022510+1)
	return 
	
scr_800b738:
	ClearField62 
	SetBit4Field44 
	SetBit0Init52 
	SetHighBit 0x1
	SetHighBit 0x2
	reset_velocity 
	SetInit4E 0x0
	set_anim 0xb
	callasm (sub_8022510+1)
	
scr_800b770:
	call_if_attacked scr_800b7bc
	goto_if_y_between 0x0, 0x90, scr_800b790
	wait_frame 
	goto scr_800b770
	
scr_800b790:
	set_anim 0xc
	ClearBit4Field44 
	goto_if_x_between 0x30, 0x50, scr_800b854
	goto_if_x_between 0x70, 0x90, scr_800b7f4
	goto scr_800b88c
	
scr_800b7bc:
	ClearBit7Field44 
	callasm (sub_8022510+1)
	cmd_62 
	cmd_9C 
	cmd_D2 0x5a0059, 0x59
	play_sound_cond 0x173, 0x174, 0x175
	cmd_6C scr_800b8c8
	return 
	
scr_800b7f4:
	SetInit4E 0x1
	cmd_26 0x30, 0x700
	set_vel_y 0xfffffccd
	goto_if_x_between2 0x80, 0x100, scr_800b838
	set_vel_x 0x222
	call scr_800b318
	ClearField62 
	goto scr_800b0fc
	
scr_800b838:
	set_vel_x 0xfffffdde
	call scr_800b2f0
	ClearField62 
	goto scr_800b000
	
scr_800b854:
	SetInit4E 0x1
	cmd_26 0x30, 0x700
	set_vel_y 0xfffffccd
	set_vel_x 0x222
	call scr_800b2c8
	ClearField62 
	goto scr_800aed8
	
scr_800b88c:
	SetInit4E 0x1
	cmd_26 0x30, 0x700
	set_vel_y 0xfffffccd
	set_vel_x 0xfffffdde
	call scr_800b2c8
	ClearField62 
	goto scr_800aed8
	return 
	
scr_800b8c8:
	callasm (sub_8033184+1)
	play_sound 0x176
	SetHighBit 0x0
	SetBit4Field44 
	disable_collision 
	set_anim 0xd
	BranchHighBit scr_800b904, 0x3
	call scr_800bc28
	
scr_800b904:
	goto_if_anim_done scr_800b918
	wait_frame 
	goto scr_800b904
	
scr_800b918:
	ClearBit0Init52 
	SetInit4E 0x0
	reset_velocity 
	wait_frame 
	
scr_800b92c:
	goto_if_ground scr_800b940
	wait_frame 
	goto scr_800b92c
	
scr_800b940:
	play_sound 0x177
	SetInit4E 0x1
	reset_velocity 
	set_anim 0xe
	offsetPosition 0x0, 0xffda
	
scr_800b964:
	goto_if_anim_done scr_800b978
	wait_frame 
	goto scr_800b964
	
scr_800b978:
	complete_lvl 
	
scr_800b97c:
	wait_frame 
	goto scr_800b97c
	
	
	
	
