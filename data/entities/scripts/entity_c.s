.globl entity_c_scr
entity_c_scr:
	
	SetHighBit 0xf
	set_anim 0x0
	wait_frame 
	
scr_8001870:
	goto_if_far_player_x scr_80018c4, 0x104
	goto_if_far_player_y scr_80018c4, 0xb4
	showsprite 
	cmd_6E 
	set_vel_x 0x0
	set_anim 0x2
	wait_frame 
	goto_if_attacked scr_80018d8
	goto_if_field44_bit6 scr_80018e0
	cmd_7C scr_80018e0
	goto scr_8001870
	
scr_80018c4:
	hidesprite 
	cmd_6D 
	wait_frame 
	goto scr_8001870
	
scr_80018d8:
	call scr_8003f6c
	
scr_80018e0:
	create_child_ent 0x0, 0x22, 0x0, 0x0, 0x6e
	call scr_8001938
	
scr_80018fc:
	goto_if_attacked scr_80018d8
	goto_if_field44_bit6 scr_80018e0
	cmd_7A scr_80018e0
	cmd_50 scr_8001930
	call scr_8003f40
	wait_frame 
	goto scr_80018fc
	
scr_8001930:
	goto scr_8001870
	
scr_8001938:
	faceplayer 
	cmd_91 
	ClearHighBit 0xf
	call scr_80045ec
	clear_local 
	
scr_8001954:
	call_if_attacked scr_80040dc
	call scr_8003ea8
	cmd_7D scr_8001990
	clear_local 
	goto_if_not_player_x scr_80019a8
	goto_if_near_player_x scr_80019d4, 0x58
	wait_frame 
	goto scr_8001954
	
scr_8001990:
	for_not scr_80019bc, 0x78
	wait_frame 
	goto scr_8001954
	
scr_80019a8:
	call scr_800459c
	wait_frame 
	goto scr_8001954
	
scr_80019bc:
	cmd_92 
	SetHighBit 0xf
	call scr_8004634
	return 
	
scr_80019d4:
	clear_local 
	
scr_80019d8:
	call_if_attacked scr_80040dc
	goto_if_far_player_x scr_8001a28, 0x28
	goto_if_far_player_x scr_8001938, 0x60
	cmd_7D scr_8001938
	faceplayer 
	toggle_dir 
	call scr_8003efc
	for scr_8001a28, 0x78
	wait_frame 
	goto scr_80019d8
	
scr_8001a28:
	set_vel_x 0x0
	faceplayer 
	call scr_8004f2c
	goto scr_8001938
	
	
	
	
