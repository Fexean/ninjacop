.globl entity_88_scr
entity_88_scr:
	
	wait_frame 
	cmd_A7 0x81
	call scr_800a768
	call scr_800a780
	call scr_800a674
	
scr_800a4fc:
	call_if_attacked scr_800a6c0
	call scr_800a5ac
	set_anim 0x1
	VelocityInit20_24 
	setInit20_24 0x80, 0x0
	cmd_AB scr_800a534
	wait_frame 
	goto scr_800a4fc
	
scr_800a534:
	setInit20_24 0xff80, 0x0
	wait_frame 
	call scr_800a7b4
	VelocityInit20_24 
	
scr_800a54c:
	call_if_attacked scr_800a6c0
	call scr_800a5ac
	call scr_800a780
	set_anim 0x1
	VelocityInit20_24 
	setInit20_24 0xff80, 0x0
	cmd_AB scr_800a58c
	wait_frame 
	goto scr_800a54c
	
scr_800a58c:
	setInit20_24 0x80, 0x0
	wait_frame 
	call scr_800a7b4
	VelocityInit20_24 
	goto scr_800a4fc
	
scr_800a5ac:
	cmd_0E scr_800a5bc, 0x0
	return 
	
scr_800a5bc:
	goto_if_offscreen scr_800a65c
	setInit20_24 0x0, 0x0
	wait_frame 
	VelocityInit20_24 
	SetHighBit 0x0
	set_anim 0x4
	
scr_800a5e4:
	goto_if_attacked scr_800a664
	goto_if_anim_done scr_800a600
	wait_frame 
	goto scr_800a5e4
	
scr_800a600:
	play_sound 0x19d
	create_child_ent 0xfff7, 0xf, 0x0, 0x0, 0x61
	create_child_ent 0x8, 0xf, 0x0, 0x0, 0x60
	set_anim 0x3
	
scr_800a638:
	goto_if_attacked scr_800a664
	goto_if_anim_done scr_800a654
	wait_frame 
	goto scr_800a638
	
scr_800a654:
	ClearHighBit 0x0
	
scr_800a65c:
	goto scr_800a674
	
scr_800a664:
	call scr_800a6c0
	goto scr_800a674
	
scr_800a674:
	SwitchDifficulty scr_800a684, scr_800a698, scr_800a6ac
	
scr_800a684:
	load_rand 0x78
	add_local 0x50
	return 
	
scr_800a698:
	load_rand 0x5a
	add_local 0x46
	return 
	
scr_800a6ac:
	load_rand 0x3c
	add_local 0x3c
	return 
	
scr_800a6c0:
	setInit20_24 0x0, 0x0
	wait_frame 
	VelocityInit20_24 
	play_sound 0x19e
	cmd_BC 0x58, 0x0, 0x0
	cmd_60 
	cmd_6C scr_800a718
	set_anim 0x2
	
scr_800a6f8:
	goto_if_attacked scr_800a6c0
	goto_if_anim_done scr_800a714
	wait_frame 
	goto scr_800a6f8
	
scr_800a714:
	return 
	
scr_800a718:
	play_sound 0x19f
	SetBit4Field44 
	disable_collision 
	kill 
	create_child_ent 0x0, 0x0, 0x0, 0x0, 0x74
	clear_local 
	
scr_800a744:
	for scr_800a75c, 0x8
	wait_frame 
	goto scr_800a744
	
scr_800a75c:
	SetHighBit 0x3
	destroy 
	
scr_800a768:
	create_child_ent 0x0, 0x0, 0x0, 0x0, 0x89
	return 
	
scr_800a780:
	goto_if_dir scr_800a7a0, 0x0
	SetHighBit 0x1
	ClearHighBit 0x2
	return 
	
scr_800a7a0:
	ClearHighBit 0x1
	SetHighBit 0x2
	return 
	
scr_800a7b4:
	BranchHighBit scr_800a7d4, 0x2
	ClearHighBit 0x1
	SetHighBit 0x2
	return 
	
scr_800a7d4:
	ClearHighBit 0x2
	SetHighBit 0x1
	return 
	
	
	
	
