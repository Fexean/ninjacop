.globl entity_0_scr
entity_0_scr:
	
	SetHighBit 0xf
	set_anim 0x0
	set_anim 0x0
	wait_frame 
	
scr_800060c:
	goto_if_far_player_x scr_8000660, 0x104
	goto_if_far_player_y scr_8000660, 0xb4
	showsprite 
	cmd_6E 
	set_vel_x 0x0
	set_anim 0x2
	wait_frame 
	goto_if_attacked scr_8000674
	goto_if_field44_bit6 scr_800067c
	cmd_7C scr_800067c
	goto scr_800060c
	
scr_8000660:
	hidesprite 
	cmd_6D 
	wait_frame 
	goto scr_800060c
	
scr_8000674:
	call scr_8003f6c
	
scr_800067c:
	create_child_ent 0x0, 0x22, 0x0, 0x0, 0x6e
	call scr_80006d4
	
scr_8000698:
	goto_if_attacked scr_8000674
	goto_if_field44_bit6 scr_800067c
	cmd_7A scr_800067c
	cmd_50 scr_80006cc
	call scr_8003f40
	wait_frame 
	goto scr_8000698
	
scr_80006cc:
	goto scr_800060c
	
scr_80006d4:
	ClearHighBit 0xf
	faceplayer 
	call scr_80045ec
	cmd_91 
	
scr_80006ec:
	clear_local 
	
scr_80006f0:
	call_if_attacked scr_80040dc
	call scr_8003ea8
	cmd_7D scr_8000734
	clear_local 
	goto_if_not_player_x scr_8000760
	goto_if_near_player_x scr_800078c, 0x18
	cmd_74 scr_800078c
	wait_frame 
	goto scr_80006f0
	
scr_8000734:
	for_not scr_8000774, 0x78
	goto_if_far_player_x scr_8000754, 0xc
	cmd_7E scr_800078c
	
scr_8000754:
	wait_frame 
	goto scr_80006f0
	
scr_8000760:
	call scr_800459c
	wait_frame 
	goto scr_80006f0
	
scr_8000774:
	cmd_92 
	SetHighBit 0xf
	call scr_8004634
	return 
	
scr_800078c:
	set_vel_x 0x0
	goto_if_near_player_y scr_80007a8, 0x20
	cmd_7E scr_80007ec
	
scr_80007a8:
	goto_if_far_player_x scr_80007fc, 0x19
	cmd_94 scr_80007cc
	call scr_8004754
	goto scr_8000804
	
scr_80007cc:
	call scr_80046ec
	call scr_80047d4
	call scr_8004720
	goto scr_8000804
	
scr_80007ec:
	call scr_8004850
	goto scr_8000804
	
scr_80007fc:
	call scr_80048dc
	
scr_8000804:
	call scr_800564c
	goto scr_80006ec
	
	
	
