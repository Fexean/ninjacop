.globl entity_2a_scr
entity_2a_scr:
	
	call scr_80065dc
	goto scr_8005a04
	
scr_8005770:
	wait_frame 
	cmd_91 
	
scr_8005778:
	call_if_attacked scr_8006308
	cmd_96 scr_80057c4
	goto_if_near_player_x scr_80057d4, 0xa
	faceplayer 
	goto_if_near_player_x scr_80057e8, 0x20
	goto_if_near_player_x scr_80057f8, 0x50
	
scr_80057b0:
	call scr_8005be0
	wait_frame 
	goto scr_8005778
	
scr_80057c4:
	call scr_8005820
	goto scr_80057b0
	
scr_80057d4:
	call scr_8005888
	wait_frame 
	goto scr_8005778
	
scr_80057e8:
	call scr_80058b8
	goto scr_80057b0
	
scr_80057f8:
	call scr_80058dc
	goto scr_80057b0
	cmd_92 
	call scr_8006630
	clear_local 
	goto scr_8005770
	
scr_8005820:
	set_vel_x 0x0
	call scr_8005d40
	
scr_8005830:
	clear_local 
	set_anim 0x30
	
scr_800583c:
	call_if_attacked scr_8005da8
	BranchHighBit scr_800587c, 0x0
	for scr_8005868, 0x3c
	wait_frame 
	goto scr_800583c
	
scr_8005868:
	cmd_96 scr_8005830
	call scr_8005d80
	return 
	
scr_800587c:
	ClearHighBit 0x0
	return 
	
scr_8005888:
	set_vel_x 0x0
	set_anim 0x2
	cmd_7C scr_80058a8
	cmd_92 
	return 
	
scr_80058a8:
	faceplayer 
	cmd_91 
	goto scr_80058b8
	
scr_80058b8:
	cmd_7B scr_80058d8
	set_vel_x 0x0
	call scr_8005cd0
	call scr_80064a0
	
scr_80058d8:
	return 
	
scr_80058dc:
	goto_if_asm (sub_802099C+1), scr_80058f0
	goto scr_80058f8
	
scr_80058f0:
	call scr_8005de8
	
scr_80058f8:
	return 
	wait_frame 
	goto scr_80056f8
	
scr_8005908:
	wait_frame 
	cmd_91 
	
scr_8005910:
	call_if_attacked scr_8006308
	cmd_96 scr_800595c
	goto_if_near_player_x scr_800596c, 0xa
	faceplayer 
	goto_if_near_player_x scr_80059a8, 0x20
	goto_if_near_player_x scr_80059b8, 0x50
	
scr_8005948:
	call scr_8005be0
	wait_frame 
	goto scr_8005910
	
scr_800595c:
	call scr_8005820
	goto scr_8005948
	
scr_800596c:
	cmd_7E scr_8005988
	
scr_8005974:
	call scr_8005888
	wait_frame 
	goto scr_8005910
	
scr_8005988:
	goto_if_far_player_y scr_8005974, 0x40
	call scr_80060a8
	wait_frame 
	goto scr_8005910
	
scr_80059a8:
	call scr_80058b8
	goto scr_8005948
	
scr_80059b8:
	call scr_80059e0
	goto scr_8005948
	cmd_92 
	call scr_8006630
	clear_local 
	goto scr_8005908
	
scr_80059e0:
	goto_if_asm (sub_802099C+1), scr_80059f0
	return 
	
scr_80059f0:
	call scr_8005f20
	call scr_80064a0
	return 
	
scr_8005a04:
	wait_frame 
	cmd_91 
	
scr_8005a0c:
	call_if_attacked scr_8006308
	cmd_96 scr_8005a4c
	goto_if_near_player_x scr_8005a5c, 0xa
	faceplayer 
	goto_if_near_player_x scr_8005a98, 0x40
	
scr_8005a38:
	call scr_8005c58
	wait_frame 
	goto scr_8005a0c
	
scr_8005a4c:
	call scr_8005820
	goto scr_8005a38
	
scr_8005a5c:
	cmd_7E scr_8005a78
	
scr_8005a64:
	call scr_8005888
	wait_frame 
	goto scr_8005a0c
	
scr_8005a78:
	goto_if_far_player_y scr_8005a64, 0x40
	call scr_80060a8
	wait_frame 
	goto scr_8005a0c
	
scr_8005a98:
	cmd_7B scr_8005a38
	call scr_800614c
	call scr_80064a0
	goto scr_8005a38
	cmd_92 
	call scr_8006630
	clear_local 
	goto scr_8005a04
	
scr_8005ad0:
	cmd_70 scr_8005b60, 0x0
	set_anim 0x3
	setObjIdVelX2 
	cmd_80 scr_8005b00
	set_vel_y_if_ground 0x6
	goto scr_8005b08
	
scr_8005b00:
	set_vel_y_if_ground 0x4
	
scr_8005b08:
	wait_frame 
	
scr_8005b0c:
	goto_if_attacked scr_8005b58
	goto_if_ground scr_8005b28
	wait_frame 
	goto scr_8005b0c
	
scr_8005b28:
	set_vel_x 0x0
	set_anim 0x32
	
scr_8005b38:
	goto_if_attacked scr_8005b58
	goto_if_anim_done scr_8005b54
	wait_frame 
	goto scr_8005b38
	
scr_8005b54:
	return 
	
scr_8005b58:
	goto scr_8006308
	
scr_8005b60:
	return 
	
scr_8005b64:
	cmd_70 scr_8005bdc, 0x0
	set_anim 0x4
	setObjIdVelX2 
	set_vel_y_if_ground 0x3
	wait_frame 
	
scr_8005b88:
	goto_if_attacked scr_8005bd4
	goto_if_ground scr_8005ba4
	wait_frame 
	goto scr_8005b88
	
scr_8005ba4:
	set_vel_x 0x0
	set_anim 0x33
	
scr_8005bb4:
	goto_if_attacked scr_8005bd4
	goto_if_anim_done scr_8005bd0
	wait_frame 
	goto scr_8005bb4
	
scr_8005bd0:
	return 
	
scr_8005bd4:
	goto scr_8006308
	
scr_8005bdc:
	return 
	
scr_8005be0:
	goto_if_attacked scr_8006308
	cmd_93 scr_8005c44
	setObjIdVelX2 
	cmd_73 scr_8005c3c, 0x10
	cmd_73 scr_8005c3c, 0xffff
	set_anim 0x1
	cmd_71 scr_8005c44, 0x11
	cmd_71 scr_8005c44, 0x12
	cmd_71 scr_8005b64, 0x0
	return 
	
scr_8005c3c:
	cmd_81 scr_8005ad0
	
scr_8005c44:
	set_anim 0x0
	set_vel_x 0x0
	return 
	
scr_8005c58:
	goto_if_attacked scr_8006308
	cmd_93 scr_8005cbc
	setObjIdVelX2 
	cmd_73 scr_8005cb4, 0x10
	cmd_73 scr_8005cb4, 0xffff
	set_anim 0x24
	cmd_71 scr_8005cbc, 0x11
	cmd_71 scr_8005cbc, 0x12
	cmd_71 scr_8005b64, 0x0
	return 
	
scr_8005cb4:
	cmd_81 scr_8005ad0
	
scr_8005cbc:
	set_anim 0x0
	set_vel_x 0x0
	return 
	
scr_8005cd0:
	set_anim2 0x27
	clear_local 
	
scr_8005cdc:
	goto_if_attacked scr_8005d38
	for scr_8005cfc, 0x11
	wait_frame 
	goto scr_8005cdc
	
scr_8005cfc:
	play_sound 0x12f
	cmd_48 0x18, 0xf, 0x0, 0x0, 0x44
	
scr_8005d18:
	goto_if_attacked scr_8005d38
	goto_if_anim_done scr_8005d34
	wait_frame 
	goto scr_8005d18
	
scr_8005d34:
	return 
	
scr_8005d38:
	goto scr_8006308
	
scr_8005d40:
	set_anim 0x2f
	
scr_8005d48:
	call_if_attacked scr_8005da8
	BranchHighBit scr_8005d74, 0x0
	goto_if_anim_done scr_8005d70
	wait_frame 
	goto scr_8005d48
	
scr_8005d70:
	return 
	
scr_8005d74:
	ClearHighBit 0x0
	return 
	
scr_8005d80:
	set_anim 0x31
	
scr_8005d88:
	goto_if_attacked scr_8006308
	goto_if_anim_done scr_8005da4
	wait_frame 
	goto scr_8005d88
	
scr_8005da4:
	return 
	
scr_8005da8:
	goto_if_child_id scr_8005dd0, 0x3e
	goto_if_child_id scr_8005dd0, 0x3f
	SetHighBit 0x0
	goto scr_8006308
	
scr_8005dd0:
	cmd_BC 0x58, 0x0, 0x0
	play_sound 0xf1
	return 
	
scr_8005de8:
	set_vel_x 0x0
	set_anim 0x34
	
scr_8005df8:
	goto_if_attacked scr_8005f08
	goto_if_anim_done scr_8005e14
	wait_frame 
	goto scr_8005df8
	
scr_8005e14:
	play_sound 0x12d
	set_anim 0x36
	cmd_49 0x0, 0x0, 0x56
	callasm (sub_8020A58+1)
	wait_frame 
	
scr_8005e3c:
	call_if_attacked scr_8005da8
	BranchHighBit scr_8005f08, 0x0
	for scr_8005e68, 0x12
	wait_frame 
	goto scr_8005e3c
	
scr_8005e68:
	SetInit4E 0x1
	cmd_A8 
	
scr_8005e74:
	call_if_attacked scr_8005da8
	BranchHighBit scr_8005f08, 0x0
	for scr_8005ea0, 0xa
	wait_frame 
	goto scr_8005e74
	
scr_8005ea0:
	SetInit4E 0x0
	VelocityInit20_24 
	
scr_8005eac:
	call_if_attacked scr_8005da8
	BranchHighBit scr_8005f08, 0x0
	goto_if_ground scr_8005ed4
	wait_frame 
	goto scr_8005eac
	
scr_8005ed4:
	set_vel_x 0x0
	kill_parent 
	set_anim 0x38
	
scr_8005ee8:
	goto_if_attacked scr_8006308
	goto_if_anim_done scr_8005f04
	wait_frame 
	goto scr_8005ee8
	
scr_8005f04:
	return 
	
scr_8005f08:
	SetInit4E 0x0
	ClearHighBit 0x0
	kill_parent 
	return 
	
scr_8005f20:
	set_vel_x 0x0
	set_anim 0x35
	
scr_8005f30:
	goto_if_attacked scr_800600c
	goto_if_anim_done scr_8005f4c
	wait_frame 
	goto scr_8005f30
	
scr_8005f4c:
	play_sound 0x12d
	set_vel_x 0x0
	cmd_3E_Init30_28_ReverseMe 0x80, 0x400
	set_anim 0x36
	cmd_49 0x0, 0x0, 0x56
	
scr_8005f7c:
	call_if_attacked scr_8005da8
	BranchHighBit scr_800600c, 0x0
	call scr_800601c
	BranchHighBit scr_8005fc4, 0x1
	goto_if_player_x scr_8005fb8
	goto_if_far_player_x scr_8005fc4, 0x20
	
scr_8005fb8:
	wait_frame 
	goto scr_8005f7c
	
scr_8005fc4:
	kill_parent 
	ClearHighBit 0x1
	set_vel_x 0x0
	cmd_25 0x0, 0x700
	set_anim 0x38
	
scr_8005fec:
	goto_if_attacked scr_800600c
	goto_if_anim_done scr_8006008
	wait_frame 
	goto scr_8005fec
	
scr_8006008:
	return 
	
scr_800600c:
	kill_parent 
	ClearHighBit 0x0
	return 
	
scr_800601c:
	cmd_73 scr_800605c, 0x10
	cmd_73 scr_8006050, 0xffff
	cmd_71 scr_8006050, 0x11
	cmd_71 scr_8006050, 0x12
	return 
	
scr_8006050:
	SetHighBit 0x1
	return 
	
scr_800605c:
	cmd_81 scr_8006078
	cmd_80 scr_8006090
	SetHighBit 0x1
	return 
	
scr_8006078:
	cmd_70 scr_80060a4, 0x0
	set_vel_y_if_ground 0x6
	return 
	
scr_8006090:
	cmd_70 scr_80060a4, 0x0
	set_vel_y_if_ground 0x4
	
scr_80060a4:
	return 
	
scr_80060a8:
	set_vel_x 0x0
	set_anim 0x34
	
scr_80060b8:
	goto_if_attacked scr_8006140
	goto_if_anim_done scr_80060d4
	wait_frame 
	goto scr_80060b8
	
scr_80060d4:
	play_sound 0x12d
	set_anim 0x36
	set_vel_y_if_ground 0x6
	wait_frame 
	
scr_80060f0:
	call_if_attacked scr_8005da8
	BranchHighBit scr_8006140, 0x0
	goto_if_ground scr_8006118
	wait_frame 
	goto scr_80060f0
	
scr_8006118:
	set_anim 0x38
	
scr_8006120:
	goto_if_attacked scr_8006308
	goto_if_anim_done scr_800613c
	wait_frame 
	goto scr_8006120
	
scr_800613c:
	return 
	
scr_8006140:
	ClearHighBit 0x0
	return 
	
scr_800614c:
	setObjIdVelX2 
	set_anim 0x28
	clear_local 
	
scr_800615c:
	call scr_8006210
	goto_if_attacked scr_8006208
	for scr_8006184, 0x10
	wait_frame 
	goto scr_800615c
	
scr_8006184:
	play_sound 0x12f
	cmd_48 0x14, 0xf, 0x0, 0x0, 0x44
	
scr_80061a0:
	call scr_8006210
	goto_if_attacked scr_8006208
	goto_if_anim_done scr_80061c4
	wait_frame 
	goto scr_80061a0
	
scr_80061c4:
	set_vel_x 0x0
	set_anim 0x2a
	
scr_80061d4:
	goto_if_attacked scr_8006208
	goto_if_anim_done scr_80061f0
	wait_frame 
	goto scr_80061d4
	
scr_80061f0:
	return 
	set_vel_x 0x0
	wait_frame 
	goto scr_80061d4
	
scr_8006208:
	goto scr_8006308
	
scr_8006210:
	cmd_71 scr_800622c, 0x11
	cmd_71 scr_800622c, 0x12
	return 
	
scr_800622c:
	set_anim 0x0
	set_vel_x 0x0
	return 
	
scr_8006240:
	ClearHighBit 0x0
	set_vel_x 0x0
	play_sound 0x12d
	set_anim 0x37
	faceplayer 
	toggle_dir 
	setObjIdVelX2 
	toggle_dir 
	set_vel_y_if_ground 0x6
	wait_frame 
	
scr_800627c:
	call_if_attacked scr_8005da8
	BranchHighBit scr_80062fc, 0x0
	goto_if_ground scr_80062b4
	toggle_dir 
	cmd_52 scr_80062e4
	toggle_dir 
	wait_frame 
	goto scr_800627c
	
scr_80062b4:
	set_vel_x 0x0
	set_anim 0x38
	
scr_80062c4:
	goto_if_attacked scr_8006308
	goto_if_anim_done scr_80062e0
	wait_frame 
	goto scr_80062c4
	
scr_80062e0:
	return 
	
scr_80062e4:
	toggle_dir 
	set_vel_x 0x0
	wait_frame 
	goto scr_800627c
	
scr_80062fc:
	ClearHighBit 0x0
	return 
	
scr_8006308:
	reset_velocity 
	call scr_80056dc
	kill_parent 
	SetInit4E 0x0
	cmd_70 scr_8006378, 0x0
	reset_velocity 
	cmd_60 
	ClearBit7Field44 
	SetBit4Field44 
	cmd_6C scr_8006420
	callasm (sub_80206B4+1)
	
scr_800634c:
	goto_if_anim_done scr_8006368
	goto_if_attacked scr_8006308
	wait_frame 
	goto scr_800634c
	
scr_8006368:
	ClearBit4Field44 
	goto_if_yellowNinja scr_8006240
	return 
	
scr_8006378:
	set_vel_x 0x0
	set_vel_y 0x0
	set_anim2 0xc
	ClearBit7Field44 
	SetBit4Field44 
	cmd_60 
	cmd_6C scr_80063f8
	wait_frame 
	
scr_80063a8:
	goto_if_attacked scr_8006378
	goto_if_ground scr_80063c4
	wait_frame 
	goto scr_80063a8
	
scr_80063c4:
	set_anim 0x33
	
scr_80063cc:
	goto_if_attacked scr_8006308
	goto_if_anim_done scr_80063e8
	wait_frame 
	goto scr_80063cc
	
scr_80063e8:
	cmd_6C scr_80063f8
	ClearBit4Field44 
	return 
	
scr_80063f8:
	SetBit4Field44 
	disable_collision 
	
scr_8006400:
	wait_frame 
	goto_if_ground scr_8006414
	goto scr_8006400
	
scr_8006414:
	kill 
	goto scr_8006448
	
scr_8006420:
	reset_velocity 
	SetBit4Field44 
	disable_collision 
	kill 
	branchNotChildHighFlag scr_8006448, 0x0
	goto_if_rng scr_8006470, 0x46
	
scr_8006448:
	callasm (sub_80207C0+1)
	
scr_8006450:
	goto_if_anim_done scr_8006464
	wait_frame 
	goto scr_8006450
	
scr_8006464:
	cmd_92 
	goto scr_80041e4
	
scr_8006470:
	SetInit4E 0x1
	play_sound 0x133
	set_anim 0x14
	goto_if_player_x scr_8004330
	set_anim 0x17
	goto scr_800431c
	
scr_80064a0:
	faceplayer 
	set_anim 0x3d
	
scr_80064ac:
	goto_if_attacked scr_800655c
	cmd_96 scr_8006554
	goto_if_anim_done scr_80064d0
	wait_frame 
	goto scr_80064ac
	
scr_80064d0:
	setObjIdVelX3 
	set_vel_y 0x400
	play_sound 0x12d
	set_anim 0x3e
	wait_frame 
	
scr_80064f0:
	goto_if_attacked scr_800655c
	cmd_96 scr_8006554
	goto_if_ground scr_8006518
	wait_frame 
	goto scr_80064f0
	toggle_dir 
	
scr_8006518:
	reset_velocity 
	set_anim 0x40
	
scr_8006524:
	goto_if_attacked scr_800655c
	cmd_96 scr_8006554
	goto_if_anim_done scr_8006548
	wait_frame 
	goto scr_8006524
	
scr_8006548:
	set_anim 0x0
	return 
	
scr_8006554:
	goto scr_8005820
	
scr_800655c:
	goto scr_8006308
	
scr_8006564:
	SetBit4Field44 
	disable_collision 
	set_anim 0x39
	
scr_8006574:
	goto_if_ground scr_8006588
	wait_frame 
	goto scr_8006574
	
scr_8006588:
	set_anim 0x3a
	play_sound 0x149
	ClearBit4Field44 
	enable_collision 
	
scr_80065a0:
	goto_if_anim_done scr_80065bc
	goto_if_attacked scr_80065c8
	wait_frame 
	goto scr_80065a0
	
scr_80065bc:
	set_anim 0x0
	return 
	
scr_80065c8:
	call scr_8006308
	set_anim 0x0
	return 
	
scr_80065dc:
	SetBit4Field44 
	disable_collision 
	play_sound 0x148
	set_anim 0x3b
	goto_if_difficulty 0x0, scr_8006608
	set_anim 0x3c
	
scr_8006608:
	goto_if_anim_done scr_800661c
	wait_frame 
	goto scr_8006608
	
scr_800661c:
	set_anim 0x0
	ClearBit4Field44 
	enable_collision 
	return 
	
scr_8006630:
	reset_velocity 
	set_anim 0x2
	
scr_800663c:
	goto_if_attacked scr_8006660
	cmd_96 scr_8006668
	cmd_7C scr_8006668
	wait_frame 
	goto scr_800663c
	
scr_8006660:
	call scr_8006308
	
scr_8006668:
	return 
	
	
	
	
