.globl entity_18_scr
entity_18_scr:
	
	SetHighBit 0xf
	set_anim 0x0
	wait_frame 
	
scr_800267c:
	goto_if_far_player_x scr_80026d0, 0x104
	goto_if_far_player_y scr_80026d0, 0xb4
	showsprite 
	cmd_6E 
	set_vel_x 0x0
	set_anim 0x2
	wait_frame 
	goto_if_attacked scr_80026e4
	goto_if_field44_bit6 scr_80026ec
	cmd_7C scr_80026ec
	goto scr_800267c
	
scr_80026d0:
	hidesprite 
	cmd_6D 
	wait_frame 
	goto scr_800267c
	
scr_80026e4:
	call scr_8003f6c
	
scr_80026ec:
	create_child_ent 0x0, 0x22, 0x0, 0x0, 0x6e
	call scr_8002744
	
scr_8002708:
	goto_if_attacked scr_80026e4
	goto_if_field44_bit6 scr_80026ec
	cmd_7A scr_80026ec
	cmd_50 scr_800273c
	call scr_8003f40
	wait_frame 
	goto scr_8002708
	
scr_800273c:
	goto scr_800267c
	
scr_8002744:
	faceplayer 
	cmd_91 
	ClearHighBit 0xf
	call scr_80045ec
	clear_local 
	
scr_8002760:
	call_if_attacked scr_80040dc
	call scr_8003ea8
	cmd_7D scr_800279c
	clear_local 
	goto_if_not_player_x scr_80027b4
	goto_if_near_player_x scr_80027d8, 0x58
	wait_frame 
	goto scr_8002760
	
scr_800279c:
	for_not scr_80027c8, 0x78
	wait_frame 
	goto scr_8002760
	
scr_80027b4:
	call scr_800459c
	wait_frame 
	goto scr_8002760
	
scr_80027c8:
	cmd_92 
	SetHighBit 0xf
	return 
	
scr_80027d8:
	clear_local 
	
scr_80027dc:
	call_if_attacked scr_80040dc
	goto_if_far_player_x scr_800282c, 0x28
	goto_if_far_player_x scr_8002744, 0x58
	cmd_7D scr_8002744
	faceplayer 
	toggle_dir 
	call scr_8003efc
	for scr_800282c, 0x78
	wait_frame 
	goto scr_80027dc
	
scr_800282c:
	set_vel_x 0x0
	faceplayer 
	call scr_8004fd0
	goto scr_8002744
	
	
	
	
