.globl entity_10_scr
entity_10_scr:
	
	SetHighBit 0xf
	set_anim 0x0
	set_anim 0x0
	wait_frame 
	
scr_8001cf4:
	goto_if_far_player_x scr_8001d48, 0x104
	goto_if_far_player_y scr_8001d48, 0xb4
	showsprite 
	cmd_6E 
	set_vel_x 0x0
	set_anim 0x2
	wait_frame 
	goto_if_attacked scr_8001d5c
	goto_if_field44_bit6 scr_8001d64
	cmd_7C scr_8001d64
	goto scr_8001cf4
	
scr_8001d48:
	hidesprite 
	cmd_6D 
	wait_frame 
	goto scr_8001cf4
	
scr_8001d5c:
	call scr_8003f6c
	
scr_8001d64:
	create_child_ent 0x0, 0x22, 0x0, 0x0, 0x6e
	call scr_8001dbc
	
scr_8001d80:
	goto_if_attacked scr_8001d5c
	goto_if_field44_bit6 scr_8001d64
	cmd_7A scr_8001d64
	cmd_50 scr_8001db4
	call scr_8003f40
	wait_frame 
	goto scr_8001d80
	
scr_8001db4:
	goto scr_8001cf4
	
scr_8001dbc:
	faceplayer 
	call scr_80045ec
	cmd_91 
	ClearHighBit 0xf
	
scr_8001dd4:
	clear_local 
	
scr_8001dd8:
	call_if_attacked scr_80040dc
	call scr_8003ea8
	cmd_7D scr_8001e1c
	clear_local 
	goto_if_not_player_x scr_8001e48
	goto_if_near_player_x scr_8001e6c, 0x18
	cmd_74 scr_8001e6c
	wait_frame 
	goto scr_8001dd8
	
scr_8001e1c:
	for_not scr_8001e5c, 0x78
	goto_if_far_player_x scr_8001e3c, 0xc
	cmd_7E scr_8001e6c
	
scr_8001e3c:
	wait_frame 
	goto scr_8001dd8
	
scr_8001e48:
	call scr_800459c
	wait_frame 
	goto scr_8001dd8
	
scr_8001e5c:
	cmd_92 
	SetHighBit 0xf
	return 
	
scr_8001e6c:
	set_vel_x 0x0
	goto_if_near_player_y scr_8001e88, 0x20
	cmd_7E scr_8001ecc
	
scr_8001e88:
	goto_if_far_player_x scr_8001edc, 0x19
	cmd_94 scr_8001eac
	call scr_8004754
	goto scr_8001ee4
	
scr_8001eac:
	call scr_80046ec
	call scr_80047d4
	call scr_8004720
	goto scr_8001ee4
	
scr_8001ecc:
	call scr_8004850
	goto scr_8001ee4
	
scr_8001edc:
	call scr_80048dc
	
scr_8001ee4:
	call scr_800564c
	goto scr_8001dd4
	
	
	
	
