

@ Sets entity's velocity/acceleration to 0, sets MaxVelocity to 0x700
.macro reset_velocity 
	.Lstart_reset_velocity_\@:
	.2byte 0
	.byte (.Lend_reset_velocity_\@-.Lstart_reset_velocity_\@)
	.align 2
	.Lend_reset_velocity_\@:
.endm

.macro SetVelX_and_dir arg0, arg1
	.Lstart_SetVelX_and_dir_\@:
	.2byte 1
	.byte (.Lend_SetVelX_and_dir_\@-.Lstart_SetVelX_and_dir_\@)
	.byte \arg0
	.align 2
	.byte \arg1
	.align 2
	.Lend_SetVelX_and_dir_\@:
.endm

.macro set_dir arg0
	.Lstart_set_dir_\@:
	.2byte 2
	.byte (.Lend_set_dir_\@-.Lstart_set_dir_\@)
	.align 2
	.byte \arg0
	.align 2
	.Lend_set_dir_\@:
.endm

@ flips lowest bit of dir
.macro toggle_dir 
	.Lstart_toggle_dir_\@:
	.2byte 3
	.byte (.Lend_toggle_dir_\@-.Lstart_toggle_dir_\@)
	.align 2
	.Lend_toggle_dir_\@:
.endm

@ Pauses script execution for n frames
.macro wait_frames n
	.Lstart_wait_frames_\@:
	.2byte 4
	.byte (.Lend_wait_frames_\@-.Lstart_wait_frames_\@)
	.align 2
	.short \n
	.align 2
	.Lend_wait_frames_\@:
.endm

@ Branches to script if value is larger or equal to local var.
@ Local var is incremented by 1 after comparison
.macro for script, value
	.Lstart_for_\@:
	.2byte 5
	.byte (.Lend_for_\@-.Lstart_for_\@)
	.align 2
	.long \script
	.short \value
	.align 2
	.Lend_for_\@:
.endm


@ Same as above but calls script instead of branching
@ Unused
.macro for_call script, value
	.Lstart_for_call_\@:
	.2byte 6
	.byte (.Lend_for_call_\@-.Lstart_for_call_\@)
	.align 2
	.long \script
	.short \value
	.align 2
	.Lend_for_call_\@:
.endm

@ Sets local var to value
.macro set_local value
	.Lstart_set_local_\@:
	.2byte 7
	.byte (.Lend_set_local_\@-.Lstart_set_local_\@)
	.align 2
	.short \value
	.align 2
	.Lend_set_local_\@:
.endm

@ Sets local var to 0
.macro clear_local 
	.Lstart_clear_local_\@:
	.2byte 8
	.byte (.Lend_clear_local_\@-.Lstart_clear_local_\@)
	.align 2
	.Lend_clear_local_\@:
.endm

@ Sets local var to a random value between [0, max]
.macro load_rand max
	.Lstart_load_rand_\@:
	.2byte 9
	.byte (.Lend_load_rand_\@-.Lstart_load_rand_\@)
	.align 2
	.short \max
	.align 2
	.Lend_load_rand_\@:
.endm

@ Unused
@ slowly sets local var to arg0 by incrementing it by 1 repeatedly (???)
.macro cmd_a arg0
	.Lstart_cmd_a_\@:
	.2byte 10
	.byte (.Lend_cmd_a_\@-.Lstart_cmd_a_\@)
	.align 2
	.short \arg0
	.align 2
	.Lend_cmd_a_\@:
.endm

@ Increments local var, branches to script and resets local if local is equal to value
@ UNUSED 
.macro for_not script, value
	.Lstart_for_not_\@:
	.2byte 11
	.byte (.Lend_for_not_\@-.Lstart_for_not_\@)
	.align 2
	.long \script
	.short \value
	.align 2
	.Lend_for_not_\@:
.endm

@ Increments local var, calls script and resets local if local is equal to value 
.macro for_not_call arg0, arg1
	.Lstart_for_not_call_\@:
	.2byte 12
	.byte (.Lend_for_not_call_\@-.Lstart_for_not_call_\@)
	.align 2
	.long \arg0
	.short \arg1
	.align 2
	.Lend_for_not_call_\@:
.endm

@ Unused
.macro cmd_d_FAIL 
	.Lstart_cmd_d_FAIL_\@:
	.2byte 13
	.byte (.Lend_cmd_d_FAIL_\@-.Lstart_cmd_d_FAIL_\@)
	.align 2
	.Lend_cmd_d_FAIL_\@:
.endm

@ Decrements local var, branches to script and resets local if local is equal to value 
.macro cmd_0E script, value
	.Lstart_cmd_0E_\@:
	.2byte 14
	.byte (.Lend_cmd_0E_\@-.Lstart_cmd_0E_\@)
	.align 2
	.long \script
	.short \value
	.align 2
	.Lend_cmd_0E_\@:
.endm

@ Decrements local var, calls script and resets local if local is equal to value 
@ Unused
.macro cmd_f script, value
	.Lstart_cmd_f_\@:
	.2byte 15
	.byte (.Lend_cmd_f_\@-.Lstart_cmd_f_\@)
	.align 2
	.long \script
	.short \value
	.align 2
	.Lend_cmd_f_\@:
.endm

@ Adds value to local var
.macro add_local value
	.Lstart_add_local_\@:
	.2byte 16
	.byte (.Lend_add_local_\@-.Lstart_add_local_\@)
	.align 2
	.short \value
	.align 2
	.Lend_add_local_\@:
.endm

@ Pauses script until entity has 2 directions (?)
@ Unused
.macro cmd_11_FAIL 
	.Lstart_cmd_11_FAIL_\@:
	.2byte 17
	.byte (.Lend_cmd_11_FAIL_\@-.Lstart_cmd_11_FAIL_\@)
	.align 2
	.Lend_cmd_11_FAIL_\@:
.endm

@ Branch to script if entity has 2 directions (?)
.macro cmd_12 script
	.Lstart_cmd_12_\@:
	.2byte 18
	.byte (.Lend_cmd_12_\@-.Lstart_cmd_12_\@)
	.align 2
	.long \script
	.Lend_cmd_12_\@:
.endm

@ Calls script depending on dir & collisiondir
@ Unused
.macro cmd_13_FAIL 
	.Lstart_cmd_13_FAIL_\@:
	.2byte 19
	.byte (.Lend_cmd_13_FAIL_\@-.Lstart_cmd_13_FAIL_\@)
	.align 2
	.Lend_cmd_13_FAIL_\@:
.endm


@ Branches to the start of the script
@ unused
.macro reset 
	.Lstart_reset_\@:
	.2byte 20
	.byte (.Lend_reset_\@-.Lstart_reset_\@)
	.align 2
	.Lend_reset_\@:
.endm

@ Sets entitys y velocity to vel if the entity is touching the ground
.macro set_vel_y_if_ground vel
	.Lstart_set_vel_y_if_ground_\@:
	.2byte 21
	.byte (.Lend_set_vel_y_if_ground_\@-.Lstart_set_vel_y_if_ground_\@)
	.align 2
	.byte \vel
	.align 2
	.Lend_set_vel_y_if_ground_\@:
.endm

@ Pauses script until entity collides with the ground
@ unused
.macro wait_for_ground 
	.Lstart_wait_for_ground_\@:
	.2byte 22
	.byte (.Lend_wait_for_ground_\@-.Lstart_wait_for_ground_\@)
	.align 2
	.Lend_wait_for_ground_\@:
.endm

@ Branches to script if the entity is on the ground
.macro goto_if_ground script
	.Lstart_goto_if_ground_\@:
	.2byte 23
	.byte (.Lend_goto_if_ground_\@-.Lstart_goto_if_ground_\@)
	.align 2
	.long \script
	.Lend_goto_if_ground_\@:
.endm

@ Calls script if the entity is on the ground
@ UNUSED
.macro call_if_ground script
	.Lstart_call_if_ground_\@:
	.2byte 24
	.byte (.Lend_call_if_ground_\@-.Lstart_call_if_ground_\@)
	.align 2
	.long \script
	.Lend_call_if_ground_\@:
.endm

@ Calls script
@ Increments the callstack index which is used to access local variable/script return address
.macro call script
	.Lstart_call_\@:
	.2byte 25
	.byte (.Lend_call_\@-.Lstart_call_\@)
	.align 2
	.long \script
	.Lend_call_\@:
.endm

@ Returns from called script. Does nothing if no calls have been made
@ Decrements the callstack index
.macro return 
	.Lstart_return_\@:
	.2byte 26
	.byte (.Lend_return_\@-.Lstart_return_\@)
	.align 2
	.Lend_return_\@:
.endm

@ Branches to script
.macro goto script
	.Lstart_goto_\@:
	.2byte 27
	.byte (.Lend_goto_\@-.Lstart_goto_\@)
	.align 2
	.long \script
	.Lend_goto_\@:
.endm

@ Pauses script until next frame
.macro wait_frame 
	.Lstart_wait_frame_\@:
	.2byte 28
	.byte (.Lend_wait_frame_\@-.Lstart_wait_frame_\@)
	.align 2
	.Lend_wait_frame_\@:
.endm

@ Destroys entity and ends script execution
.macro destroy 
	.Lstart_destroy_\@:
	.2byte 29
	.byte (.Lend_destroy_\@-.Lstart_destroy_\@)
	.align 2
	.Lend_destroy_\@:
.endm

@ Spawns the item to be dropped by the entity(if any), gives player special charge and sets the entity's deletion flag
.macro kill 
	.Lstart_kill_\@:
	.2byte 30
	.byte (.Lend_kill_\@-.Lstart_kill_\@)
	.align 2
	.Lend_kill_\@:
.endm

@ Calls compiled code, pointer to the entity is passed to the function as argument in r0
.macro callasm func
	.Lstart_callasm_\@:
	.2byte 31
	.byte (.Lend_callasm_\@-.Lstart_callasm_\@)
	.align 2
	.long \func
	.Lend_callasm_\@:
.endm


@ Pauses script execution.
@ Calls compiled code every frame until it returns a non-zero value. 
@ Unused
.macro wait_callasm func
	.Lstart_wait_callasm_\@:
	.2byte 32
	.byte (.Lend_wait_callasm_\@-.Lstart_wait_callasm_\@)
	.align 2
	.long \func
	.Lend_wait_callasm_\@:
.endm

@ Calls func, branches to script if func returns non-zero
.macro goto_if_asm func, script
	.Lstart_goto_if_asm_\@:
	.2byte 33
	.byte (.Lend_goto_if_asm_\@-.Lstart_goto_if_asm_\@)
	.align 2
	.long \func
	.long \script
	.Lend_goto_if_asm_\@:
.endm

@Calls func, calls script if func returns non-zero
@ unused
.macro call_if_asm arg0, arg1
	.Lstart_call_if_asm_\@:
	.2byte 34
	.byte (.Lend_call_if_asm_\@-.Lstart_call_if_asm_\@)
	.align 2
	.long \arg0
	.long \arg1
	.Lend_call_if_asm_\@:
.endm

@ Sets entity callback if it is currently 0. Does nothing otherwise.
@ (The callback is called every frame after script execution)
.macro set_ent_callback func
	.Lstart_set_ent_callback_\@:
	.2byte 35
	.byte (.Lend_set_ent_callback_\@-.Lstart_set_ent_callback_\@)
	.align 2
	.long \func
	.Lend_set_ent_callback_\@:
.endm

@ sets entity callback to 0
.macro clear_ent_callback 
	.Lstart_clear_ent_callback_\@:
	.2byte 36
	.byte (.Lend_clear_ent_callback_\@-.Lstart_clear_ent_callback_\@)
	.align 2
	.Lend_clear_ent_callback_\@:
.endm

@ sets x-acceleration to acc if abs(ent->xvel) < arg1, else it is set to 0
.macro cmd_25 acc, arg1
	.Lstart_cmd_25_\@:
	.2byte 37
	.byte (.Lend_cmd_25_\@-.Lstart_cmd_25_\@)
	.align 2
	.long \acc
	.long \arg1
	.Lend_cmd_25_\@:
.endm

.macro cmd_26 arg0, arg1
	.Lstart_cmd_26_\@:
	.2byte 38
	.byte (.Lend_cmd_26_\@-.Lstart_cmd_26_\@)
	.align 2
	.long \arg0
	.long \arg1
	.Lend_cmd_26_\@:
.endm

.macro cmd_27_FAIL 
	.Lstart_cmd_27_FAIL_\@:
	.2byte 39
	.byte (.Lend_cmd_27_FAIL_\@-.Lstart_cmd_27_FAIL_\@)
	.align 2
	.Lend_cmd_27_FAIL_\@:
.endm

.macro cmd_28_FAIL 
	.Lstart_cmd_28_FAIL_\@:
	.2byte 40
	.byte (.Lend_cmd_28_FAIL_\@-.Lstart_cmd_28_FAIL_\@)
	.align 2
	.Lend_cmd_28_FAIL_\@:
.endm

@ Sets the x component of entity's velocity
.macro set_vel_x vel
	.Lstart_set_vel_x_\@:
	.2byte 41
	.byte (.Lend_set_vel_x_\@-.Lstart_set_vel_x_\@)
	.align 2
	.long \vel
	.Lend_set_vel_x_\@:
.endm

@ Sets the y component of entity's velocity
.macro set_vel_y vel
	.Lstart_set_vel_y_\@:
	.2byte 42
	.byte (.Lend_set_vel_y_\@-.Lstart_set_vel_y_\@)
	.align 2
	.long \vel
	.Lend_set_vel_y_\@:
.endm

@ Branch to script if player's x is within maxDist of entity's x
.macro goto_if_near_player_x script, maxDist
	.Lstart_goto_if_near_player_x_\@:
	.2byte 43
	.byte (.Lend_goto_if_near_player_x_\@-.Lstart_goto_if_near_player_x_\@)
	.align 2
	.long \script
	.short \maxDist
	.align 2
	.Lend_goto_if_near_player_x_\@:
.endm

@ Branch to script if player's y is within maxDist of entity's y
.macro goto_if_near_player_y script, maxDist
	.Lstart_goto_if_near_player_y_\@:
	.2byte 44
	.byte (.Lend_goto_if_near_player_y_\@-.Lstart_goto_if_near_player_y_\@)
	.align 2
	.long \script
	.short \maxDist
	.align 2
	.Lend_goto_if_near_player_y_\@:
.endm

@ Branch to script if player's x is farther than maxDist away from entity's x
.macro goto_if_far_player_x script, maxDist
	.Lstart_goto_if_far_player_x_\@:
	.2byte 45
	.byte (.Lend_goto_if_far_player_x_\@-.Lstart_goto_if_far_player_x_\@)
	.align 2
	.long \script
	.short \maxDist
	.align 2
	.Lend_goto_if_far_player_x_\@:
.endm

@ Branch to script if player's y is farther than maxDist away from entity's y
.macro goto_if_far_player_y script, maxDist
	.Lstart_goto_if_far_player_y_\@:
	.2byte 46
	.byte (.Lend_goto_if_far_player_y_\@-.Lstart_goto_if_far_player_y_\@)
	.align 2
	.long \script
	.short \maxDist
	.align 2
	.Lend_goto_if_far_player_y_\@:
.endm

@ Branch to arg0 if entity's x is not arg1
@ unused
.macro goto_if_not_x arg0, arg1
	.Lstart_goto_if_not_x_\@:
	.2byte 47
	.byte (.Lend_goto_if_not_x_\@-.Lstart_goto_if_not_x_\@)
	.align 2
	.long \arg0
	.short \arg1
	.align 2
	.Lend_goto_if_not_x_\@:
.endm

@ Branch to arg0 if entity's y is not arg1
@ unused
.macro goto_if_not_y arg0, arg1
	.Lstart_goto_if_not_y_\@:
	.2byte 48
	.byte (.Lend_goto_if_not_y_\@-.Lstart_goto_if_not_y_\@)
	.align 2
	.long \arg0
	.short \arg1
	.align 2
	.Lend_goto_if_not_y_\@:
.endm

@ Branch to arg0 if entity's x is arg1
.macro goto_if_x arg0, arg1
	.Lstart_goto_if_x_\@:
	.2byte 49
	.byte (.Lend_goto_if_x_\@-.Lstart_goto_if_x_\@)
	.align 2
	.long \arg0
	.short \arg1
	.align 2
	.Lend_goto_if_x_\@:
.endm

@ Branch to arg0 if entity's y is arg1
.macro goto_if_y arg0, arg1
	.Lstart_goto_if_y_\@:
	.2byte 50
	.byte (.Lend_goto_if_y_\@-.Lstart_goto_if_y_\@)
	.align 2
	.long \arg0
	.short \arg1
	.align 2
	.Lend_goto_if_y_\@:
.endm

.macro goto_if_x_between arg0, arg1, arg2
	.Lstart_goto_if_x_between_\@:
	.2byte 51
	.byte (.Lend_goto_if_x_between_\@-.Lstart_goto_if_x_between_\@)
	.align 2
	.short \arg0
	.short \arg1
	.long \arg2
	.align 2
	.Lend_goto_if_x_between_\@:
.endm

.macro goto_if_y_between arg0, arg1, arg2
	.Lstart_goto_if_y_between_\@:
	.2byte 52
	.byte (.Lend_goto_if_y_between_\@-.Lstart_goto_if_y_between_\@)
	.align 2
	.short \arg0
	.short \arg1
	.long \arg2
	.Lend_goto_if_y_between_\@:
.endm

.macro goto_if_x_between2 arg0, arg1, arg2
	.Lstart_goto_if_x_between2_\@:
	.2byte 53
	.byte (.Lend_goto_if_x_between2_\@-.Lstart_goto_if_x_between2_\@)
	.align 2
	.short \arg0
	.short \arg1
	.long \arg2
	.Lend_goto_if_x_between2_\@:
.endm

.macro goto_if_y_between2 arg0, arg1, arg2
	.Lstart_goto_if_y_between2_\@:
	.2byte 54
	.byte (.Lend_goto_if_y_between2_\@-.Lstart_goto_if_y_between2_\@)
	.align 2
	.short \arg0
	.short \arg1
	.long \arg2
	.Lend_goto_if_y_between2_\@:
.endm

.macro goto_if_dir arg0, arg1
	.Lstart_goto_if_dir_\@:
	.2byte 55
	.byte (.Lend_goto_if_dir_\@-.Lstart_goto_if_dir_\@)
	.align 2
	.long \arg0
	.byte \arg1
	.align 2
	.Lend_goto_if_dir_\@:
.endm

.macro goto_if_player_x arg0
	.Lstart_goto_if_player_x_\@:
	.2byte 56
	.byte (.Lend_goto_if_player_x_\@-.Lstart_goto_if_player_x_\@)
	.align 2
	.long \arg0
	.Lend_goto_if_player_x_\@:
.endm

.macro goto_if_not_player_x arg0
	.Lstart_goto_if_not_player_x_\@:
	.2byte 57
	.byte (.Lend_goto_if_not_player_x_\@-.Lstart_goto_if_not_player_x_\@)
	.align 2
	.long \arg0
	.Lend_goto_if_not_player_x_\@:
.endm

.macro goto_if_not_ground arg0
	.Lstart_goto_if_not_ground_\@:
	.2byte 58
	.byte (.Lend_goto_if_not_ground_\@-.Lstart_goto_if_not_ground_\@)
	.align 2
	.long \arg0
	.Lend_goto_if_not_ground_\@:
.endm

.macro faceplayer 
	.Lstart_faceplayer_\@:
	.2byte 59
	.byte (.Lend_faceplayer_\@-.Lstart_faceplayer_\@)
	.align 2
	.Lend_faceplayer_\@:
.endm

.macro cmd_3C_modifyDir 
	.Lstart_cmd_3C_modifyDir_\@:
	.2byte 60
	.byte (.Lend_cmd_3C_modifyDir_\@-.Lstart_cmd_3C_modifyDir_\@)
	.align 2
	.Lend_cmd_3C_modifyDir_\@:
.endm

.macro cmd_3D_setVelXDirBased arg0
	.Lstart_cmd_3D_setVelXDirBased_\@:
	.2byte 61
	.byte (.Lend_cmd_3D_setVelXDirBased_\@-.Lstart_cmd_3D_setVelXDirBased_\@)
	.align 2
	.long \arg0
	.Lend_cmd_3D_setVelXDirBased_\@:
.endm

.macro cmd_3E_Init30_28_ReverseMe arg0, arg1
	.Lstart_cmd_3E_Init30_28_ReverseMe_\@:
	.2byte 62
	.byte (.Lend_cmd_3E_Init30_28_ReverseMe_\@-.Lstart_cmd_3E_Init30_28_ReverseMe_\@)
	.align 2
	.long \arg0
	.long \arg1
	.Lend_cmd_3E_Init30_28_ReverseMe_\@:
.endm

@ Makes entity's sprite invisible
.macro hidesprite 
	.Lstart_hidesprite_\@:
	.2byte 63
	.byte (.Lend_hidesprite_\@-.Lstart_hidesprite_\@)
	.align 2
	.Lend_hidesprite_\@:
.endm

@ Makes entity's sprite visible
.macro showsprite 
	.Lstart_showsprite_\@:
	.2byte 64
	.byte (.Lend_showsprite_\@-.Lstart_showsprite_\@)
	.align 2
	.Lend_showsprite_\@:
.endm

@ Sets entity's sprite's animation id to newId
.macro set_anim newId
	.Lstart_set_anim_\@:
	.2byte 65
	.byte (.Lend_set_anim_\@-.Lstart_set_anim_\@)
	.align 2
	.short \newId
	.align 2
	.Lend_set_anim_\@:
.endm

@ Sets entity's sprite's animation id to newId even if it is already newId
.macro set_anim2 newId
	.Lstart_set_anim2_\@:
	.2byte 66
	.byte (.Lend_set_anim2_\@-.Lstart_set_anim2_\@)
	.align 2
	.short \newId
	.align 2
	.Lend_set_anim2_\@:
.endm

.macro next_anim 
	.Lstart_next_anim_\@:
	.2byte 67
	.byte (.Lend_next_anim_\@-.Lstart_next_anim_\@)
	.align 2
	.Lend_next_anim_\@:
.endm

@ Sets animation but doesn't modify ent->animationFrameRelated
@ unused
.macro set_anim3 arg0
	.Lstart_set_anim3_\@:
	.2byte 68
	.byte (.Lend_set_anim3_\@-.Lstart_set_anim3_\@)
	.align 2
	.short \arg0
	.align 2
	.Lend_set_anim3_\@:
.endm

.macro goto_if_onscreen arg0
	.Lstart_goto_if_onscreen_\@:
	.2byte 69
	.byte (.Lend_goto_if_onscreen_\@-.Lstart_goto_if_onscreen_\@)
	.align 2
	.long \arg0
	.Lend_goto_if_onscreen_\@:
.endm

.macro goto_if_offscreen arg0
	.Lstart_goto_if_offscreen_\@:
	.2byte 70
	.byte (.Lend_goto_if_offscreen_\@-.Lstart_goto_if_offscreen_\@)
	.align 2
	.long \arg0
	.Lend_goto_if_offscreen_\@:
.endm

.macro create_child_ent xofs, yofs, velx, vely, type
	.Lstart_create_child_ent_\@:
	.2byte 71
	.byte (.Lend_create_child_ent_\@-.Lstart_create_child_ent_\@)
	.align 2
	.short \xofs
	.short \yofs
	.long \velx
	.long \vely
	.short \type
	.align 2
	.Lend_create_child_ent_\@:
.endm

.macro cmd_48 arg0, arg1, arg2, arg3, arg4
	.Lstart_cmd_48_\@:
	.2byte 72
	.byte (.Lend_cmd_48_\@-.Lstart_cmd_48_\@)
	.align 2
	.short \arg0
	.short \arg1
	.long \arg2
	.long \arg3
	.short \arg4
	.align 2
	.Lend_cmd_48_\@:
.endm

.macro cmd_49 arg0, arg1, arg2
	.Lstart_cmd_49_\@:
	.2byte 73
	.byte (.Lend_cmd_49_\@-.Lstart_cmd_49_\@)
	.align 2
	.short \arg0
	.short \arg1
	.short \arg2
	.align 2
	.Lend_cmd_49_\@:
.endm

@ Destroys parent entity if it exists
.macro kill_parent 
	.Lstart_kill_parent_\@:
	.2byte 74
	.byte (.Lend_kill_parent_\@-.Lstart_kill_parent_\@)
	.align 2
	.Lend_kill_parent_\@:
.endm

.macro cmd_4B arg0, arg1
	.Lstart_cmd_4B_\@:
	.2byte 75
	.byte (.Lend_cmd_4B_\@-.Lstart_cmd_4B_\@)
	.align 2
	.short \arg0
	.short \arg1
	.align 2
	.Lend_cmd_4B_\@:
.endm

@ Unused
.macro cmd_4c_FAIL 
	.Lstart_cmd_4c_FAIL_\@:
	.2byte 76
	.byte (.Lend_cmd_4c_FAIL_\@-.Lstart_cmd_4c_FAIL_\@)
	.align 2
	.Lend_cmd_4c_FAIL_\@:
.endm

.macro SetInit4E arg0
	.Lstart_SetInit4E_\@:
	.2byte 77
	.byte (.Lend_SetInit4E_\@-.Lstart_SetInit4E_\@)
	.align 2
	.short \arg0
	.align 2
	.Lend_SetInit4E_\@:
.endm



@ Branch to scipt with a propability of (100-percentage)%
.macro goto_if_rng script, percentage
	.Lstart_goto_if_rng_\@:
	.2byte 78
	.byte (.Lend_goto_if_rng_\@-.Lstart_goto_if_rng_\@)
	.align 2
	.long \script
	.byte \percentage
	.align 2
	.Lend_goto_if_rng_\@:
.endm

@ Call scipt with a propability of (100-percentage)%
@ Unused
.macro call_if_rng script, percentage
	.Lstart_call_if_rng_\@:
	.2byte 79
	.byte (.Lend_call_if_rng_\@-.Lstart_call_if_rng_\@)
	.align 2
	.long \script
	.byte \percentage
	.align 2
	.Lend_call_if_rng_\@:
.endm

.macro cmd_50 arg0
	.Lstart_cmd_50_\@:
	.2byte 80
	.byte (.Lend_cmd_50_\@-.Lstart_cmd_50_\@)
	.align 2
	.long \arg0
	.Lend_cmd_50_\@:
.endm

.macro cmd_51 arg0
	.Lstart_cmd_51_\@:
	.2byte 81
	.byte (.Lend_cmd_51_\@-.Lstart_cmd_51_\@)
	.align 2
	.long \arg0
	.Lend_cmd_51_\@:
.endm

.macro cmd_52 arg0
	.Lstart_cmd_52_\@:
	.2byte 82
	.byte (.Lend_cmd_52_\@-.Lstart_cmd_52_\@)
	.align 2
	.long \arg0
	.Lend_cmd_52_\@:
.endm

.macro CopyInitPos 
	.Lstart_CopyInitPos_\@:
	.2byte 83
	.byte (.Lend_CopyInitPos_\@-.Lstart_CopyInitPos_\@)
	.align 2
	.Lend_CopyInitPos_\@:
.endm

.macro teleport_to_parent 
	.Lstart_teleport_to_parent_\@:
	.2byte 84
	.byte (.Lend_teleport_to_parent_\@-.Lstart_teleport_to_parent_\@)
	.align 2
	.Lend_teleport_to_parent_\@:
.endm

.macro cmd_55 
	.Lstart_cmd_55_\@:
	.2byte 85
	.byte (.Lend_cmd_55_\@-.Lstart_cmd_55_\@)
	.align 2
	.Lend_cmd_55_\@:
.endm

.macro goto_if_field44_bit6 arg0
	.Lstart_goto_if_field44_bit6_\@:
	.2byte 86
	.byte (.Lend_goto_if_field44_bit6_\@-.Lstart_goto_if_field44_bit6_\@)
	.align 2
	.long \arg0
	.Lend_goto_if_field44_bit6_\@:
.endm

.macro goto_if_field44_bit4 arg0
	.Lstart_goto_if_field44_bit4_\@:
	.2byte 87
	.byte (.Lend_goto_if_field44_bit4_\@-.Lstart_goto_if_field44_bit4_\@)
	.align 2
	.long \arg0
	.Lend_goto_if_field44_bit4_\@:
.endm

.macro goto_if_attacked arg0
	.Lstart_goto_if_attacked_\@:
	.2byte 88
	.byte (.Lend_goto_if_attacked_\@-.Lstart_goto_if_attacked_\@)
	.align 2
	.long \arg0
	.Lend_goto_if_attacked_\@:
.endm

.macro call_if_attacked arg0
	.Lstart_call_if_attacked_\@:
	.2byte 89
	.byte (.Lend_call_if_attacked_\@-.Lstart_call_if_attacked_\@)
	.align 2
	.long \arg0
	.Lend_call_if_attacked_\@:
.endm

.macro ifChildFlag23 arg0
	.Lstart_ifChildFlag23_\@:
	.2byte 90
	.byte (.Lend_ifChildFlag23_\@-.Lstart_ifChildFlag23_\@)
	.align 2
	.long \arg0
	.Lend_ifChildFlag23_\@:
.endm

.macro If_flag7_and_child_flag23 arg0
	.Lstart_If_flag7_and_child_flag23_\@:
	.2byte 91
	.byte (.Lend_If_flag7_and_child_flag23_\@-.Lstart_If_flag7_and_child_flag23_\@)
	.align 2
	.long \arg0
	.Lend_If_flag7_and_child_flag23_\@:
.endm

.macro ifChildFlag22 arg0
	.Lstart_ifChildFlag22_\@:
	.2byte 92
	.byte (.Lend_ifChildFlag22_\@-.Lstart_ifChildFlag22_\@)
	.align 2
	.long \arg0
	.Lend_ifChildFlag22_\@:
.endm

.macro if_f7_and_childF22 arg0
	.Lstart_if_f7_and_childF22_\@:
	.2byte 93
	.byte (.Lend_if_f7_and_childF22_\@-.Lstart_if_f7_and_childF22_\@)
	.align 2
	.long \arg0
	.Lend_if_f7_and_childF22_\@:
.endm

.macro ifChildFlag21 arg0
	.Lstart_ifChildFlag21_\@:
	.2byte 94
	.byte (.Lend_ifChildFlag21_\@-.Lstart_ifChildFlag21_\@)
	.align 2
	.long \arg0
	.Lend_ifChildFlag21_\@:
.endm

.macro if_f7_and_ChildFlag21 arg0
	.Lstart_if_f7_and_ChildFlag21_\@:
	.2byte 95
	.byte (.Lend_if_f7_and_ChildFlag21_\@-.Lstart_if_f7_and_ChildFlag21_\@)
	.align 2
	.long \arg0
	.Lend_if_f7_and_ChildFlag21_\@:
.endm

.macro cmd_60 
	.Lstart_cmd_60_\@:
	.2byte 96
	.byte (.Lend_cmd_60_\@-.Lstart_cmd_60_\@)
	.align 2
	.Lend_cmd_60_\@:
.endm


@ adds arg0 (signed) to player's health
@ unused
.macro add_player_hp arg0
	.Lstart_add_player_hp_\@:
	.2byte 97
	.byte (.Lend_add_player_hp_\@-.Lstart_add_player_hp_\@)
	.align 2
	.short \arg0
	.align 2
	.Lend_add_player_hp_\@:
.endm

.macro cmd_62 
	.Lstart_cmd_62_\@:
	.2byte 98
	.byte (.Lend_cmd_62_\@-.Lstart_cmd_62_\@)
	.align 2
	.Lend_cmd_62_\@:
.endm


@ adds arg0 (signed) to player's charge
@ unused
.macro add_player_charge arg0
	.Lstart_add_player_charge_\@:
	.2byte 99
	.byte (.Lend_add_player_charge_\@-.Lstart_add_player_charge_\@)
	.align 2
	.short \arg0
	.align 2
	.Lend_add_player_charge_\@:
.endm

.macro cmd_64 
	.Lstart_cmd_64_\@:
	.2byte 100
	.byte (.Lend_cmd_64_\@-.Lstart_cmd_64_\@)
	.align 2
	.Lend_cmd_64_\@:
.endm

.macro do_hostage_dmg arg0
	.Lstart_do_hostage_dmg_\@:
	.2byte 101
	.byte (.Lend_do_hostage_dmg_\@-.Lstart_do_hostage_dmg_\@)
	.align 2
	.short \arg0
	.align 2
	.Lend_do_hostage_dmg_\@:
.endm

.macro SetBit4Field44 
	.Lstart_SetBit4Field44_\@:
	.2byte 102
	.byte (.Lend_SetBit4Field44_\@-.Lstart_SetBit4Field44_\@)
	.align 2
	.Lend_SetBit4Field44_\@:
.endm

.macro ClearBit4Field44 
	.Lstart_ClearBit4Field44_\@:
	.2byte 103
	.byte (.Lend_ClearBit4Field44_\@-.Lstart_ClearBit4Field44_\@)
	.align 2
	.Lend_ClearBit4Field44_\@:
.endm

.macro SetBit7Field44 
	.Lstart_SetBit7Field44_\@:
	.2byte 104
	.byte (.Lend_SetBit7Field44_\@-.Lstart_SetBit7Field44_\@)
	.align 2
	.Lend_SetBit7Field44_\@:
.endm

.macro ClearBit7Field44 
	.Lstart_ClearBit7Field44_\@:
	.2byte 105
	.byte (.Lend_ClearBit7Field44_\@-.Lstart_ClearBit7Field44_\@)
	.align 2
	.Lend_ClearBit7Field44_\@:
.endm

.macro disable_collision 
	.Lstart_disable_collision_\@:
	.2byte 106
	.byte (.Lend_disable_collision_\@-.Lstart_disable_collision_\@)
	.align 2
	.Lend_disable_collision_\@:
.endm

.macro enable_collision 
	.Lstart_enable_collision_\@:
	.2byte 107
	.byte (.Lend_enable_collision_\@-.Lstart_enable_collision_\@)
	.align 2
	.Lend_enable_collision_\@:
.endm

.macro cmd_6C arg0
	.Lstart_cmd_6C_\@:
	.2byte 108
	.byte (.Lend_cmd_6C_\@-.Lstart_cmd_6C_\@)
	.align 2
	.long \arg0
	.Lend_cmd_6C_\@:
.endm

.macro cmd_6D 
	.Lstart_cmd_6D_\@:
	.2byte 109
	.byte (.Lend_cmd_6D_\@-.Lstart_cmd_6D_\@)
	.align 2
	.Lend_cmd_6D_\@:
.endm

.macro cmd_6E 
	.Lstart_cmd_6E_\@:
	.2byte 110
	.byte (.Lend_cmd_6E_\@-.Lstart_cmd_6E_\@)
	.align 2
	.Lend_cmd_6E_\@:
.endm

.macro goto_if_offset_collision arg0, arg1, arg2, arg3
	.Lstart_goto_if_offset_collision_\@:
	.2byte 111
	.byte (.Lend_goto_if_offset_collision_\@-.Lstart_goto_if_offset_collision_\@)
	.align 2
	.long \arg0
	.short \arg1
	.short \arg2
	.short \arg3
	.align 2
	.Lend_goto_if_offset_collision_\@:
.endm

.macro cmd_70 arg0, arg1
	.Lstart_cmd_70_\@:
	.2byte 112
	.byte (.Lend_cmd_70_\@-.Lstart_cmd_70_\@)
	.align 2
	.long \arg0
	.short \arg1
	.align 2
	.Lend_cmd_70_\@:
.endm

.macro cmd_71 arg0, arg1
	.Lstart_cmd_71_\@:
	.2byte 113
	.byte (.Lend_cmd_71_\@-.Lstart_cmd_71_\@)
	.align 2
	.long \arg0
	.long \arg1
	.Lend_cmd_71_\@:
.endm

.macro cmd_72_FAIL 
	.Lstart_cmd_72_FAIL_\@:
	.2byte 114
	.byte (.Lend_cmd_72_FAIL_\@-.Lstart_cmd_72_FAIL_\@)
	.align 2
	.Lend_cmd_72_FAIL_\@:
.endm

.macro cmd_73 arg0, arg1
	.Lstart_cmd_73_\@:
	.2byte 115
	.byte (.Lend_cmd_73_\@-.Lstart_cmd_73_\@)
	.align 2
	.long \arg0
	.long \arg1
	.Lend_cmd_73_\@:
.endm

.macro cmd_74 arg0
	.Lstart_cmd_74_\@:
	.2byte 116
	.byte (.Lend_cmd_74_\@-.Lstart_cmd_74_\@)
	.align 2
	.long \arg0
	.Lend_cmd_74_\@:
.endm

.macro cmd_75 arg0
	.Lstart_cmd_75_\@:
	.2byte 117
	.byte (.Lend_cmd_75_\@-.Lstart_cmd_75_\@)
	.align 2
	.long \arg0
	.Lend_cmd_75_\@:
.endm

.macro cmd_76 arg0, arg1
	.Lstart_cmd_76_\@:
	.2byte 118
	.byte (.Lend_cmd_76_\@-.Lstart_cmd_76_\@)
	.align 2
	.long \arg0
	.long \arg1
	.Lend_cmd_76_\@:
.endm

.macro cmd_77_FAIL 
	.Lstart_cmd_77_FAIL_\@:
	.2byte 119
	.byte (.Lend_cmd_77_FAIL_\@-.Lstart_cmd_77_FAIL_\@)
	.align 2
	.Lend_cmd_77_FAIL_\@:
.endm

.macro cmd_78_FAIL 
	.Lstart_cmd_78_FAIL_\@:
	.2byte 120
	.byte (.Lend_cmd_78_FAIL_\@-.Lstart_cmd_78_FAIL_\@)
	.align 2
	.Lend_cmd_78_FAIL_\@:
.endm

.macro unbox 
	.Lstart_unbox_\@:
	.2byte 121
	.byte (.Lend_unbox_\@-.Lstart_unbox_\@)
	.align 2
	.Lend_unbox_\@:
.endm

.macro cmd_7A arg0
	.Lstart_cmd_7A_\@:
	.2byte 122
	.byte (.Lend_cmd_7A_\@-.Lstart_cmd_7A_\@)
	.align 2
	.long \arg0
	.Lend_cmd_7A_\@:
.endm

.macro cmd_7B arg0
	.Lstart_cmd_7B_\@:
	.2byte 123
	.byte (.Lend_cmd_7B_\@-.Lstart_cmd_7B_\@)
	.align 2
	.long \arg0
	.Lend_cmd_7B_\@:
.endm

.macro cmd_7C arg0
	.Lstart_cmd_7C_\@:
	.2byte 124
	.byte (.Lend_cmd_7C_\@-.Lstart_cmd_7C_\@)
	.align 2
	.long \arg0
	.Lend_cmd_7C_\@:
.endm

.macro cmd_7D arg0
	.Lstart_cmd_7D_\@:
	.2byte 125
	.byte (.Lend_cmd_7D_\@-.Lstart_cmd_7D_\@)
	.align 2
	.long \arg0
	.Lend_cmd_7D_\@:
.endm

.macro cmd_7E arg0
	.Lstart_cmd_7E_\@:
	.2byte 126
	.byte (.Lend_cmd_7E_\@-.Lstart_cmd_7E_\@)
	.align 2
	.long \arg0
	.Lend_cmd_7E_\@:
.endm

.macro cmd_7F arg0
	.Lstart_cmd_7F_\@:
	.2byte 127
	.byte (.Lend_cmd_7F_\@-.Lstart_cmd_7F_\@)
	.align 2
	.long \arg0
	.Lend_cmd_7F_\@:
.endm

.macro cmd_80 arg0
	.Lstart_cmd_80_\@:
	.2byte 128
	.byte (.Lend_cmd_80_\@-.Lstart_cmd_80_\@)
	.align 2
	.long \arg0
	.Lend_cmd_80_\@:
.endm

.macro cmd_81 arg0
	.Lstart_cmd_81_\@:
	.2byte 129
	.byte (.Lend_cmd_81_\@-.Lstart_cmd_81_\@)
	.align 2
	.long \arg0
	.Lend_cmd_81_\@:
.endm

.macro goto_if_anim_done arg0
	.Lstart_goto_if_anim_done_\@:
	.2byte 130
	.byte (.Lend_goto_if_anim_done_\@-.Lstart_goto_if_anim_done_\@)
	.align 2
	.long \arg0
	.Lend_goto_if_anim_done_\@:
.endm

.macro setFlag10 
	.Lstart_setFlag10_\@:
	.2byte 131
	.byte (.Lend_setFlag10_\@-.Lstart_setFlag10_\@)
	.align 2
	.Lend_setFlag10_\@:
.endm

.macro clearFlag10 
	.Lstart_clearFlag10_\@:
	.2byte 132
	.byte (.Lend_clearFlag10_\@-.Lstart_clearFlag10_\@)
	.align 2
	.Lend_clearFlag10_\@:
.endm

.macro setConstSize 
	.Lstart_setConstSize_\@:
	.2byte 133
	.byte (.Lend_setConstSize_\@-.Lstart_setConstSize_\@)
	.align 2
	.Lend_setConstSize_\@:
.endm

.macro setConstSize2 
	.Lstart_setConstSize2_\@:
	.2byte 134
	.byte (.Lend_setConstSize2_\@-.Lstart_setConstSize2_\@)
	.align 2
	.Lend_setConstSize2_\@:
.endm

.macro goto_if_collide arg0
	.Lstart_goto_if_collide_\@:
	.2byte 135
	.byte (.Lend_goto_if_collide_\@-.Lstart_goto_if_collide_\@)
	.align 2
	.long \arg0
	.Lend_goto_if_collide_\@:
.endm

.macro ClearBit0Init52 
	.Lstart_ClearBit0Init52_\@:
	.2byte 136
	.byte (.Lend_ClearBit0Init52_\@-.Lstart_ClearBit0Init52_\@)
	.align 2
	.Lend_ClearBit0Init52_\@:
.endm

.macro SetBit0Init52 
	.Lstart_SetBit0Init52_\@:
	.2byte 137
	.byte (.Lend_SetBit0Init52_\@-.Lstart_SetBit0Init52_\@)
	.align 2
	.Lend_SetBit0Init52_\@:
.endm

@ randomly branches to one of the provided scripts
.macro rng_switch scriptCount, scripts:vararg
	.Lstart_rng_switch_\@:
	.2byte 138
	.byte (.Lend_rng_switch_\@-.Lstart_rng_switch_\@)
	.align 2
	.long \scriptCount
	.Lend_rng_switch_\@:
	.long \scripts
.endm

.macro cmd_8b_FAIL arg0
	.Lstart_cmd_8b_FAIL_\@:
	.2byte 139
	.byte (.Lend_cmd_8b_FAIL_\@-.Lstart_cmd_8b_FAIL_\@)
	.align 2
	.long \arg0
	.Lend_cmd_8b_FAIL_\@:
.endm

.macro offsetPosition arg0, arg1
	.Lstart_offsetPosition_\@:
	.2byte 140
	.byte (.Lend_offsetPosition_\@-.Lstart_offsetPosition_\@)
	.align 2
	.short \arg0
	.short \arg1
	.align 2
	.Lend_offsetPosition_\@:
.endm

.macro offsetPosition2 arg0, arg1
	.Lstart_offsetPosition2_\@:
	.2byte 141
	.byte (.Lend_offsetPosition2_\@-.Lstart_offsetPosition2_\@)
	.align 2
	.short \arg0
	.short \arg1
	.align 2
	.Lend_offsetPosition2_\@:
.endm

.macro setObjIdVelX 
	.Lstart_setObjIdVelX_\@:
	.2byte 142
	.byte (.Lend_setObjIdVelX_\@-.Lstart_setObjIdVelX_\@)
	.align 2
	.Lend_setObjIdVelX_\@:
.endm

.macro setObjIdVelX2 
	.Lstart_setObjIdVelX2_\@:
	.2byte 143
	.byte (.Lend_setObjIdVelX2_\@-.Lstart_setObjIdVelX2_\@)
	.align 2
	.Lend_setObjIdVelX2_\@:
.endm

.macro setObjIdVelX3 
	.Lstart_setObjIdVelX3_\@:
	.2byte 144
	.byte (.Lend_setObjIdVelX3_\@-.Lstart_setObjIdVelX3_\@)
	.align 2
	.Lend_setObjIdVelX3_\@:
.endm

.macro cmd_91 
	.Lstart_cmd_91_\@:
	.2byte 145
	.byte (.Lend_cmd_91_\@-.Lstart_cmd_91_\@)
	.align 2
	.Lend_cmd_91_\@:
.endm

.macro cmd_92 
	.Lstart_cmd_92_\@:
	.2byte 146
	.byte (.Lend_cmd_92_\@-.Lstart_cmd_92_\@)
	.align 2
	.Lend_cmd_92_\@:
.endm

.macro cmd_93 arg0
	.Lstart_cmd_93_\@:
	.2byte 147
	.byte (.Lend_cmd_93_\@-.Lstart_cmd_93_\@)
	.align 2
	.long \arg0
	.Lend_cmd_93_\@:
.endm

.macro cmd_94 arg0
	.Lstart_cmd_94_\@:
	.2byte 148
	.byte (.Lend_cmd_94_\@-.Lstart_cmd_94_\@)
	.align 2
	.long \arg0
	.Lend_cmd_94_\@:
.endm

.macro cmd_95 arg0
	.Lstart_cmd_95_\@:
	.2byte 149
	.byte (.Lend_cmd_95_\@-.Lstart_cmd_95_\@)
	.align 2
	.long \arg0
	.Lend_cmd_95_\@:
.endm

.macro cmd_96 arg0
	.Lstart_cmd_96_\@:
	.2byte 150
	.byte (.Lend_cmd_96_\@-.Lstart_cmd_96_\@)
	.align 2
	.long \arg0
	.Lend_cmd_96_\@:
.endm

.macro save_hostage 
	.Lstart_save_hostage_\@:
	.2byte 151
	.byte (.Lend_save_hostage_\@-.Lstart_save_hostage_\@)
	.align 2
	.Lend_save_hostage_\@:
.endm

.macro kill_hostage 
	.Lstart_kill_hostage_\@:
	.2byte 152
	.byte (.Lend_kill_hostage_\@-.Lstart_kill_hostage_\@)
	.align 2
	.Lend_kill_hostage_\@:
.endm

.macro cmd_99 
	.Lstart_cmd_99_\@:
	.2byte 153
	.byte (.Lend_cmd_99_\@-.Lstart_cmd_99_\@)
	.align 2
	.Lend_cmd_99_\@:
.endm

.macro cmd_9A arg0
	.Lstart_cmd_9A_\@:
	.2byte 154
	.byte (.Lend_cmd_9A_\@-.Lstart_cmd_9A_\@)
	.align 2
	.long \arg0
	.Lend_cmd_9A_\@:
.endm

.macro cmd_9B arg0, arg1
	.Lstart_cmd_9B_\@:
	.2byte 155
	.byte (.Lend_cmd_9B_\@-.Lstart_cmd_9B_\@)
	.align 2
	.long \arg0
	.long \arg1
	.Lend_cmd_9B_\@:
.endm

.macro cmd_9C 
	.Lstart_cmd_9C_\@:
	.2byte 156
	.byte (.Lend_cmd_9C_\@-.Lstart_cmd_9C_\@)
	.align 2
	.Lend_cmd_9C_\@:
.endm

.macro cmd_9d_FAIL 
	.Lstart_cmd_9d_FAIL_\@:
	.2byte 157
	.byte (.Lend_cmd_9d_FAIL_\@-.Lstart_cmd_9d_FAIL_\@)
	.align 2
	.Lend_cmd_9d_FAIL_\@:
.endm

.macro cmd_9e_FAIL 
	.Lstart_cmd_9e_FAIL_\@:
	.2byte 158
	.byte (.Lend_cmd_9e_FAIL_\@-.Lstart_cmd_9e_FAIL_\@)
	.align 2
	.Lend_cmd_9e_FAIL_\@:
.endm

.macro cmd_9f_FAIL 
	.Lstart_cmd_9f_FAIL_\@:
	.2byte 159
	.byte (.Lend_cmd_9f_FAIL_\@-.Lstart_cmd_9f_FAIL_\@)
	.align 2
	.Lend_cmd_9f_FAIL_\@:
.endm


@ starts level end sequence
.macro complete_lvl 
	.Lstart_complete_lvl_\@:
	.2byte 160
	.byte (.Lend_complete_lvl_\@-.Lstart_complete_lvl_\@)
	.align 2
	.Lend_complete_lvl_\@:
.endm

@ branches if the current entity is a yellow ninja
.macro goto_if_yellowNinja arg0
	.Lstart_goto_if_yellowNinja_\@:
	.2byte 161
	.byte (.Lend_goto_if_yellowNinja_\@-.Lstart_goto_if_yellowNinja_\@)
	.align 2
	.long \arg0
	.Lend_goto_if_yellowNinja_\@:
.endm

@ if a child exists and its id is id, goto script
.macro goto_if_child_id script, id
	.Lstart_goto_if_child_id_\@:
	.2byte 162
	.byte (.Lend_goto_if_child_id_\@-.Lstart_goto_if_child_id_\@)
	.align 2
	.long \script
	.long \id
	.Lend_goto_if_child_id_\@:
.endm

.macro cmd_a3_FAIL 
	.Lstart_cmd_a3_FAIL_\@:
	.2byte 163
	.byte (.Lend_cmd_a3_FAIL_\@-.Lstart_cmd_a3_FAIL_\@)
	.align 2
	.Lend_cmd_a3_FAIL_\@:
.endm


.macro play_sound sound
	.Lstart_play_sound_\@:
	.2byte 164
	.byte (.Lend_play_sound_\@-.Lstart_play_sound_\@)
	.align 2
	.short \sound
	.align 2
	.Lend_play_sound_\@:
.endm

.macro stop_sound sound
	.Lstart_stop_sound_\@:
	.2byte 165
	.byte (.Lend_stop_sound_\@-.Lstart_stop_sound_\@)
	.align 2
	.short \sound
	.align 2
	.Lend_stop_sound_\@:
.endm

.macro cmd_A6 arg0
	.Lstart_cmd_A6_\@:
	.2byte 166
	.byte (.Lend_cmd_A6_\@-.Lstart_cmd_A6_\@)
	.align 2
	.long \arg0
	.Lend_cmd_A6_\@:
.endm

.macro cmd_A7 arg0
	.Lstart_cmd_A7_\@:
	.2byte 167
	.byte (.Lend_cmd_A7_\@-.Lstart_cmd_A7_\@)
	.align 2
	.long \arg0
	.Lend_cmd_A7_\@:
.endm

.macro cmd_A8 
	.Lstart_cmd_A8_\@:
	.2byte 168
	.byte (.Lend_cmd_A8_\@-.Lstart_cmd_A8_\@)
	.align 2
	.Lend_cmd_A8_\@:
.endm

.macro VelocityInit20_24 
	.Lstart_VelocityInit20_24_\@:
	.2byte 169
	.byte (.Lend_VelocityInit20_24_\@-.Lstart_VelocityInit20_24_\@)
	.align 2
	.Lend_VelocityInit20_24_\@:
.endm

.macro cmd_aa_FAIL 
	.Lstart_cmd_aa_FAIL_\@:
	.2byte 170
	.byte (.Lend_cmd_aa_FAIL_\@-.Lstart_cmd_aa_FAIL_\@)
	.align 2
	.Lend_cmd_aa_FAIL_\@:
.endm

.macro cmd_AB arg0
	.Lstart_cmd_AB_\@:
	.2byte 171
	.byte (.Lend_cmd_AB_\@-.Lstart_cmd_AB_\@)
	.align 2
	.long \arg0
	.Lend_cmd_AB_\@:
.endm

.macro setInit20_24 arg0, arg1
	.Lstart_setInit20_24_\@:
	.2byte 172
	.byte (.Lend_setInit20_24_\@-.Lstart_setInit20_24_\@)
	.align 2
	.short \arg0
	.short \arg1
	.align 2
	.Lend_setInit20_24_\@:
.endm

.macro cmd_ad_FAIL 
	.Lstart_cmd_ad_FAIL_\@:
	.2byte 173
	.byte (.Lend_cmd_ad_FAIL_\@-.Lstart_cmd_ad_FAIL_\@)
	.align 2
	.Lend_cmd_ad_FAIL_\@:
.endm

.macro SetHighBit arg0
	.Lstart_SetHighBit_\@:
	.2byte 174
	.byte (.Lend_SetHighBit_\@-.Lstart_SetHighBit_\@)
	.align 2
	.short \arg0
	.align 2
	.Lend_SetHighBit_\@:
.endm

.macro ClearHighBit arg0
	.Lstart_ClearHighBit_\@:
	.2byte 175
	.byte (.Lend_ClearHighBit_\@-.Lstart_ClearHighBit_\@)
	.align 2
	.short \arg0
	.align 2
	.Lend_ClearHighBit_\@:
.endm

.macro BranchHighBit arg0, arg1
	.Lstart_BranchHighBit_\@:
	.2byte 176
	.byte (.Lend_BranchHighBit_\@-.Lstart_BranchHighBit_\@)
	.align 2
	.long \arg0
	.short \arg1
	.align 2
	.Lend_BranchHighBit_\@:
.endm

.macro BranchNotHighBit arg0, arg1
	.Lstart_BranchNotHighBit_\@:
	.2byte 177
	.byte (.Lend_BranchNotHighBit_\@-.Lstart_BranchNotHighBit_\@)
	.align 2
	.long \arg0
	.short \arg1
	.align 2
	.Lend_BranchNotHighBit_\@:
.endm

.macro setParentHighFlag arg0
	.Lstart_setParentHighFlag_\@:
	.2byte 178
	.byte (.Lend_setParentHighFlag_\@-.Lstart_setParentHighFlag_\@)
	.align 2
	.short \arg0
	.align 2
	.Lend_setParentHighFlag_\@:
.endm

.macro clearParentHighFlag arg0
	.Lstart_clearParentHighFlag_\@:
	.2byte 179
	.byte (.Lend_clearParentHighFlag_\@-.Lstart_clearParentHighFlag_\@)
	.align 2
	.short \arg0
	.align 2
	.Lend_clearParentHighFlag_\@:
.endm

.macro branchIfParentHighFlag arg0, arg1
	.Lstart_branchIfParentHighFlag_\@:
	.2byte 180
	.byte (.Lend_branchIfParentHighFlag_\@-.Lstart_branchIfParentHighFlag_\@)
	.align 2
	.long \arg0
	.short \arg1
	.align 2
	.Lend_branchIfParentHighFlag_\@:
.endm

.macro branchNotParentHighFlag arg0, arg1
	.Lstart_branchNotParentHighFlag_\@:
	.2byte 181
	.byte (.Lend_branchNotParentHighFlag_\@-.Lstart_branchNotParentHighFlag_\@)
	.align 2
	.long \arg0
	.short \arg1
	.align 2
	.Lend_branchNotParentHighFlag_\@:
.endm

.macro branchIfChildHighFlag arg0, arg1
	.Lstart_branchIfChildHighFlag_\@:
	.2byte 182
	.byte (.Lend_branchIfChildHighFlag_\@-.Lstart_branchIfChildHighFlag_\@)
	.align 2
	.long \arg0
	.short \arg1
	.align 2
	.Lend_branchIfChildHighFlag_\@:
.endm

.macro branchNotChildHighFlag arg0, arg1
	.Lstart_branchNotChildHighFlag_\@:
	.2byte 183
	.byte (.Lend_branchNotChildHighFlag_\@-.Lstart_branchNotChildHighFlag_\@)
	.align 2
	.long \arg0
	.short \arg1
	.align 2
	.Lend_branchNotChildHighFlag_\@:
.endm

.macro IncStack 
	.Lstart_IncStack_\@:
	.2byte 184
	.byte (.Lend_IncStack_\@-.Lstart_IncStack_\@)
	.align 2
	.Lend_IncStack_\@:
.endm

.macro DecStack 
	.Lstart_DecStack_\@:
	.2byte 185
	.byte (.Lend_DecStack_\@-.Lstart_DecStack_\@)
	.align 2
	.Lend_DecStack_\@:
.endm


@ sets script return address to arg0
@ unused
.macro set_ret arg0
	.Lstart_set_ret_\@:
	.2byte 186
	.byte (.Lend_set_ret_\@-.Lstart_set_ret_\@)
	.align 2
	.long \arg0
	.Lend_set_ret_\@:
.endm

.macro ClearField62 
	.Lstart_ClearField62_\@:
	.2byte 187
	.byte (.Lend_ClearField62_\@-.Lstart_ClearField62_\@)
	.align 2
	.Lend_ClearField62_\@:
.endm

.macro cmd_BC arg0, arg1, arg2
	.Lstart_cmd_BC_\@:
	.2byte 188
	.byte (.Lend_cmd_BC_\@-.Lstart_cmd_BC_\@)
	.align 2
	.short \arg0
	.short \arg1
	.short \arg2
	.align 2
	.Lend_cmd_BC_\@:
.endm

.macro cmd_BD arg0
	.Lstart_cmd_BD_\@:
	.2byte 189
	.byte (.Lend_cmd_BD_\@-.Lstart_cmd_BD_\@)
	.align 2
	.long \arg0
	.Lend_cmd_BD_\@:
.endm

.macro cmd_BE 
	.Lstart_cmd_BE_\@:
	.2byte 190
	.byte (.Lend_cmd_BE_\@-.Lstart_cmd_BE_\@)
	.align 2
	.Lend_cmd_BE_\@:
.endm

.macro cmd_BF arg0, arg1
	.Lstart_cmd_BF_\@:
	.2byte 191
	.byte (.Lend_cmd_BF_\@-.Lstart_cmd_BF_\@)
	.align 2
	.long \arg0
	.short \arg1
	.align 2
	.Lend_cmd_BF_\@:
.endm

.macro cmd_C0 arg0, arg1
	.Lstart_cmd_C0_\@:
	.2byte 192
	.byte (.Lend_cmd_C0_\@-.Lstart_cmd_C0_\@)
	.align 2
	.long \arg0
	.short \arg1
	.align 2
	.Lend_cmd_C0_\@:
.endm

.macro BranchID3 arg0
	.Lstart_BranchID3_\@:
	.2byte 193
	.byte (.Lend_BranchID3_\@-.Lstart_BranchID3_\@)
	.align 2
	.long \arg0
	.Lend_BranchID3_\@:
.endm

.macro BranchID2 arg0
	.Lstart_BranchID2_\@:
	.2byte 194
	.byte (.Lend_BranchID2_\@-.Lstart_BranchID2_\@)
	.align 2
	.long \arg0
	.Lend_BranchID2_\@:
.endm

.macro FadeScreen arg0, arg1, color
	.Lstart_FadeScreen_\@:
	.2byte 195
	.byte (.Lend_FadeScreen_\@-.Lstart_FadeScreen_\@)
	.align 2
	.short \arg0
	.byte \arg1
	.byte \color
	.align 2
	.Lend_FadeScreen_\@:
.endm

.macro cmd_C4 arg0
	.Lstart_cmd_C4_\@:
	.2byte 196
	.byte (.Lend_cmd_C4_\@-.Lstart_cmd_C4_\@)
	.align 2
	.long \arg0
	.Lend_cmd_C4_\@:
.endm

.macro SetInitPos arg0, arg1
	.Lstart_SetInitPos_\@:
	.2byte 197
	.byte (.Lend_SetInitPos_\@-.Lstart_SetInitPos_\@)
	.align 2
	.short \arg0
	.short \arg1
	.align 2
	.Lend_SetInitPos_\@:
.endm

.macro freeze_player 
	.Lstart_freeze_player_\@:
	.2byte 198
	.byte (.Lend_freeze_player_\@-.Lstart_freeze_player_\@)
	.align 2
	.Lend_freeze_player_\@:
.endm

.macro unfreeze_player 
	.Lstart_unfreeze_player_\@:
	.2byte 199
	.byte (.Lend_unfreeze_player_\@-.Lstart_unfreeze_player_\@)
	.align 2
	.Lend_unfreeze_player_\@:
.endm

.macro branchPlayerVulnerable arg0
	.Lstart_branchPlayerVulnerable_\@:
	.2byte 200
	.byte (.Lend_branchPlayerVulnerable_\@-.Lstart_branchPlayerVulnerable_\@)
	.align 2
	.long \arg0
	.Lend_branchPlayerVulnerable_\@:
.endm


@ calls one of the args depending on the difficulty setting
.macro SwitchDifficultyCall easy, normal, hard
	.Lstart_SwitchDifficultyCall_\@:
	.2byte 201
	.byte (.Lend_SwitchDifficultyCall_\@-.Lstart_SwitchDifficultyCall_\@)
	.align 2
	.long \easy
	.long \normal
	.long \hard
	.Lend_SwitchDifficultyCall_\@:
.endm


@ branches to one of the args depending on the difficulty setting
.macro SwitchDifficulty easy, normal, hard
	.Lstart_SwitchDifficulty_\@:
	.2byte 202
	.byte (.Lend_SwitchDifficulty_\@-.Lstart_SwitchDifficulty_\@)
	.align 2
	.long \easy
	.long \normal
	.long \hard
	.Lend_SwitchDifficulty_\@:
.endm


@ branches to arg1 if difficulty==arg0
.macro goto_if_difficulty arg0, arg1
	.Lstart_goto_if_difficulty_\@:
	.2byte 203
	.byte (.Lend_goto_if_difficulty_\@-.Lstart_goto_if_difficulty_\@)
	.align 2
	.short \arg0
	.align 2
	.long \arg1
	.Lend_goto_if_difficulty_\@:
.endm

@ loads difficulty to local var
.macro LoadDifficulty arg0, arg1, arg2
	.Lstart_LoadDifficulty_\@:
	.2byte 204
	.byte (.Lend_LoadDifficulty_\@-.Lstart_LoadDifficulty_\@)
	.align 2
	.short \arg0
	.short \arg1
	.short \arg2
	.align 2
	.Lend_LoadDifficulty_\@:
.endm

@ branches to one of the args based on language
.macro switch_language english, french, german
	.Lstart_switch_language_\@:
	.2byte 205
	.byte (.Lend_switch_language_\@-.Lstart_switch_language_\@)
	.align 2
	.long \english
	.long \french
	.long \german
	.Lend_switch_language_\@:
.endm

.macro BranchChildXRange arg0, arg1, arg2
	.Lstart_BranchChildXRange_\@:
	.2byte 206
	.byte (.Lend_BranchChildXRange_\@-.Lstart_BranchChildXRange_\@)
	.align 2
	.long \arg0
	.short \arg1
	.short \arg2
	.align 2
	.Lend_BranchChildXRange_\@:
.endm

.macro BranchChildYRange arg0, arg1, arg2
	.Lstart_BranchChildYRange_\@:
	.2byte 207
	.byte (.Lend_BranchChildYRange_\@-.Lstart_BranchChildYRange_\@)
	.align 2
	.long \arg0
	.short \arg1
	.short \arg2
	.align 2
	.Lend_BranchChildYRange_\@:
.endm

@ if field44 bit 21 is set, arg0 is played
@ if field44 bit 23 is set, arg1 is played
@ else arg2 is played
.macro play_sound_cond arg0, arg1, arg2
	.Lstart_play_sound_cond_\@:
	.2byte 208
	.byte (.Lend_play_sound_cond_\@-.Lstart_play_sound_cond_\@)
	.align 2
	.short \arg0
	.short \arg1
	.short \arg2
	.align 2
	.Lend_play_sound_cond_\@:
.endm

@ plays arg1 if entityID > 0xF, else plays arg0
.macro PlaySongObjID arg0, arg1
	.Lstart_PlaySongObjID_\@:
	.2byte 209
	.byte (.Lend_PlaySongObjID_\@-.Lstart_PlaySongObjID_\@)
	.align 2
	.short \arg0
	.short \arg1
	.align 2
	.Lend_PlaySongObjID_\@:
.endm

.macro cmd_D2 arg0, arg1
	.Lstart_cmd_D2_\@:
	.2byte 210
	.byte (.Lend_cmd_D2_\@-.Lstart_cmd_D2_\@)
	.align 2
	.long \arg0
	.long \arg1
	.Lend_cmd_D2_\@:
.endm

.macro cmd_D3 arg0, arg1, arg2
	.Lstart_cmd_D3_\@:
	.2byte 211
	.byte (.Lend_cmd_D3_\@-.Lstart_cmd_D3_\@)
	.align 2
	.short \arg0
	.short \arg1
	.long \arg2
	.Lend_cmd_D3_\@:
.endm

.macro cmd_D4 arg0
	.Lstart_cmd_D4_\@:
	.2byte 212
	.byte (.Lend_cmd_D4_\@-.Lstart_cmd_D4_\@)
	.align 2
	.long \arg0
	.Lend_cmd_D4_\@:
.endm

.macro cmd_D5 arg0
	.Lstart_cmd_D5_\@:
	.2byte 213
	.byte (.Lend_cmd_D5_\@-.Lstart_cmd_D5_\@)
	.align 2
	.long \arg0
	.Lend_cmd_D5_\@:
.endm

.macro cmd_d6_FAIL 
	.Lstart_cmd_d6_FAIL_\@:
	.2byte 214
	.byte (.Lend_cmd_d6_FAIL_\@-.Lstart_cmd_d6_FAIL_\@)
	.align 2
	.Lend_cmd_d6_FAIL_\@:
.endm

.macro cmd_D7 arg0, arg1
	.Lstart_cmd_D7_\@:
	.2byte 215
	.byte (.Lend_cmd_D7_\@-.Lstart_cmd_D7_\@)
	.align 2
	.long \arg0
	.long \arg1
	.Lend_cmd_D7_\@:
.endm

.macro cmd_D8 arg0, arg1
	.Lstart_cmd_D8_\@:
	.2byte 216
	.byte (.Lend_cmd_D8_\@-.Lstart_cmd_D8_\@)
	.align 2
	.long \arg0
	.long \arg1
	.Lend_cmd_D8_\@:
.endm

@ Makes entity blink for 30 frames
.macro blink_ent 
	.Lstart_blink_ent_\@:
	.2byte 217
	.byte (.Lend_blink_ent_\@-.Lstart_blink_ent_\@)
	.align 2
	.Lend_blink_ent_\@:
.endm

@ Plays palette animation on entity's pal for 30 frames
.macro cmd_DA palanim
	.Lstart_cmd_DA_\@:
	.2byte 218
	.byte (.Lend_cmd_DA_\@-.Lstart_cmd_DA_\@)
	.align 2
	.long \palanim
	.Lend_cmd_DA_\@:
.endm

.macro branchIfAnimation arg0, arg1
	.Lstart_branchIfAnimation_\@:
	.2byte 219
	.byte (.Lend_branchIfAnimation_\@-.Lstart_branchIfAnimation_\@)
	.align 2
	.short \arg0
	.align 2
	.long \arg1
	.Lend_branchIfAnimation_\@:
.endm

.macro BranchIfParentId arg0, arg1
	.Lstart_BranchIfParentId_\@:
	.2byte 220
	.byte (.Lend_BranchIfParentId_\@-.Lstart_BranchIfParentId_\@)
	.align 2
	.short \arg0
	.align 2
	.long \arg1
	.Lend_BranchIfParentId_\@:
.endm

.macro branchIfObjID arg0, arg1
	.Lstart_branchIfObjID_\@:
	.2byte 221
	.byte (.Lend_branchIfObjID_\@-.Lstart_branchIfObjID_\@)
	.align 2
	.short \arg0
	.align 2
	.long \arg1
	.Lend_branchIfObjID_\@:
.endm

@ Identical to goto
@ unused
.macro goto2 arg0
	.Lstart_goto2_\@:
	.2byte 222
	.byte (.Lend_goto2_\@-.Lstart_goto2_\@)
	.align 2
	.long \arg0
	.Lend_goto2_\@:
.endm

.macro clearLateFields 
	.Lstart_clearLateFields_\@:
	.2byte 223
	.byte (.Lend_clearLateFields_\@-.Lstart_clearLateFields_\@)
	.align 2
	.Lend_clearLateFields_\@:
.endm





.macro setflag flag
	.if \flag==0
	.elseif \flag==1
	.elseif \flag==2
	.elseif \flag==3
	.elseif \flag==4
		SetBit4Field44
	.elseif \flag==5
		disable_collision
	.elseif \flag==6
	.elseif \flag==7
		SetBit7Field44
	.elseif \flag==10
		setFlag10
	endif
.endm
