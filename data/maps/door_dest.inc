
@ This file contains destinations of doors for each room, formatted like:
@ struct{
@     u8 doorCollision, roomId, doorId, padding;
@ };

@ each door list is terminated with 0x0000FFFF
@ TODO: place these in room directories

.globl baseDoorDestsStage3Room0
baseDoorDestsStage3Room0:
.long 0xffff


.globl bankDoorDestsStage1Room0
bankDoorDestsStage1Room0:
.long 0x140, 0x241, 0x860, 0xffff


.globl bankDoorDestsStage1Room1
bankDoorDestsStage1Room1:
.long 0x10040, 0xffff


.globl bankDoorDestsStage1Room2
bankDoorDestsStage1Room2:
.long 0x20041, 0xffff


.globl bankDoorDestsStage2Room0
bankDoorDestsStage2Room0:
.long 0x140, 0x241, 0x342, 0x443, 0x860, 0xffff


.globl bankDoorDestsStage2Room1
bankDoorDestsStage2Room1:
.long 0x10040, 0xffff


.globl bankDoorDestsStage2Room2
bankDoorDestsStage2Room2:
.long 0x20041, 0xffff


.globl bankDoorDestsStage2Room3
bankDoorDestsStage2Room3:
.long 0x30042, 0xffff


.globl bankDoorDestsStage2Room4
bankDoorDestsStage2Room4:
.long 0x40043, 0xffff


.globl bankDoorDestsStage3Room0
bankDoorDestsStage3Room0:
.long 0x140, 0x241, 0x361, 0x462, 0x860, 0xffff


.globl bankDoorDestsStage3Room1
bankDoorDestsStage3Room1:
.long 0x10040, 0xffff


.globl bankDoorDestsStage3Room2
bankDoorDestsStage3Room2:
.long 0x20041, 0xffff


.globl bankDoorDestsStage3Room3
bankDoorDestsStage3Room3:
.long 0x30061, 0xffff


.globl bankDoorDestsStage3Room4
bankDoorDestsStage3Room4:
.long 0x40062, 0xffff


.globl bankDoorDestsStage1
bankDoorDestsStage1:
.long bankDoorDestsStage1Room0
.long bankDoorDestsStage1Room1
.long bankDoorDestsStage1Room2


.globl bankDoorDestsStage2
bankDoorDestsStage2:
.long bankDoorDestsStage2Room0
.long bankDoorDestsStage2Room1
.long bankDoorDestsStage2Room2
.long bankDoorDestsStage2Room3
.long bankDoorDestsStage2Room4


.globl bankDoorDestsStage3
bankDoorDestsStage3:
.long bankDoorDestsStage3Room0
.long bankDoorDestsStage3Room1
.long bankDoorDestsStage3Room2
.long bankDoorDestsStage3Room3
.long bankDoorDestsStage3Room4


.globl baseDoorDestsStage3
baseDoorDestsStage3:
.long baseDoorDestsStage3Room0


.globl doorDests_bank
doorDests_bank:
.long bankDoorDestsStage1
.long bankDoorDestsStage2
.long bankDoorDestsStage3
.long baseDoorDestsStage3


.globl harborDoorDestsStage1Room0
harborDoorDestsStage1Room0:
.long 0x140, 0x860, 0xffff


.globl harborDoorDestsStage1Room1
harborDoorDestsStage1Room1:
.long 0x10040, 0x241, 0x10242, 0xffff


.globl harborDoorDestsStage1Room2
harborDoorDestsStage1Room2:
.long 0x10141, 0x20142, 0x361, 0x462, 0xffff


.globl harborDoorDestsStage1Room3
harborDoorDestsStage1Room3:
.long 0x20261, 0xffff


.globl harborDoorDestsStage1Room4
harborDoorDestsStage1Room4:
.long 0x30262, 0xffff


.globl harborDoorDestsStage1
harborDoorDestsStage1:
.long harborDoorDestsStage1Room0
.long harborDoorDestsStage1Room1
.long harborDoorDestsStage1Room2
.long harborDoorDestsStage1Room3
.long harborDoorDestsStage1Room4


.globl harborDoorDestsStage2Room0
harborDoorDestsStage2Room0:
.long 0x261, 0x162, 0x860, 0xffff


.globl harborDoorDestsStage2Room1
harborDoorDestsStage2Room1:
.long 0x20062, 0xffff


.globl harborDoorDestsStage2Room2
harborDoorDestsStage2Room2:
.long 0x10061, 0x340, 0xffff


.globl harborDoorDestsStage2Room3
harborDoorDestsStage2Room3:
.long 0x10240, 0xffff


.globl harborDoorDestsStage2
harborDoorDestsStage2:
.long harborDoorDestsStage2Room0
.long harborDoorDestsStage2Room1
.long harborDoorDestsStage2Room2
.long harborDoorDestsStage2Room3


.globl harborDoorDestsStage3Room0
harborDoorDestsStage3Room0:
.long 0x140, 0x361, 0x263, 0x462, 0x860, 0xffff


.globl harborDoorDestsStage3Room1
harborDoorDestsStage3Room1:
.long 0x10040, 0xffff


.globl harborDoorDestsStage3Room2
harborDoorDestsStage3Room2:
.long 0x20063, 0xffff


.globl harborDoorDestsStage3Room3
harborDoorDestsStage3Room3:
.long 0x30061, 0xffff


.globl harborDoorDestsStage3Room4
harborDoorDestsStage3Room4:
.long 0x40062, 0xffff


.globl harborDoorDestsStage3
harborDoorDestsStage3:
.long harborDoorDestsStage3Room0
.long harborDoorDestsStage3Room1
.long harborDoorDestsStage3Room2
.long harborDoorDestsStage3Room3
.long harborDoorDestsStage3Room4


.globl doorDests_harbor
doorDests_harbor:
.long harborDoorDestsStage1
.long harborDoorDestsStage2
.long harborDoorDestsStage3
.long baseDoorDestsStage3


.globl airportDoorDestsStage1Room0
airportDoorDestsStage1Room0:
.long 0x140, 0x10141, 0x261, 0x363, 0x860, 0xffff


.globl airportDoorDestsStage1Room1
airportDoorDestsStage1Room1:
.long 0x10040, 0x20041, 0xffff


.globl airportDoorDestsStage1Room2
airportDoorDestsStage1Room2:
.long 0x30061, 0xffff


.globl airportDoorDestsStage1Room3
airportDoorDestsStage1Room3:
.long 0x40063, 0xffff


.globl airportDoorDestsStage1
airportDoorDestsStage1:
.long airportDoorDestsStage1Room0
.long airportDoorDestsStage1Room1
.long airportDoorDestsStage1Room2
.long airportDoorDestsStage1Room3


.globl airportDoorDestsStage2Room0
airportDoorDestsStage2Room0:
.long 0x343, 0x161, 0xffff


.globl airportDoorDestsStage2Room1
airportDoorDestsStage2Room1:
.long 0x20061, 0x860, 0xffff


.globl airportDoorDestsStage2Room2
airportDoorDestsStage2Room2:
.long 0xffff


.globl airportDoorDestsStage2Room3
airportDoorDestsStage2Room3:
.long 0x10043, 0x544, 0x445, 0xffff


.globl airportDoorDestsStage2Room4
airportDoorDestsStage2Room4:
.long 0x20345, 0xffff


.globl airportDoorDestsStage2Room5
airportDoorDestsStage2Room5:
.long 0x10344, 0x641, 0x742, 0x10662, 0x10763, 0xffff


.globl airportDoorDestsStage2Room6
airportDoorDestsStage2Room6:
.long 0x10541, 0x20562, 0xffff


.globl airportDoorDestsStage2Room7
airportDoorDestsStage2Room7:
.long 0x30542, 0x40563, 0xffff


.globl airportDoorDestsStage2
airportDoorDestsStage2:
.long airportDoorDestsStage2Room0
.long airportDoorDestsStage2Room1
.long airportDoorDestsStage2Room2
.long airportDoorDestsStage2Room3
.long airportDoorDestsStage2Room4
.long airportDoorDestsStage2Room5
.long airportDoorDestsStage2Room6
.long airportDoorDestsStage2Room7


.globl airportDoorDestsStage3Room0
airportDoorDestsStage3Room0:
.long 0x140, 0x263, 0x361, 0x860, 0xffff


.globl airportDoorDestsStage3Room1
airportDoorDestsStage3Room1:
.long 0x30040, 0xffff


.globl airportDoorDestsStage3Room2
airportDoorDestsStage3Room2:
.long 0x20063, 0xffff


.globl airportDoorDestsStage3Room3
airportDoorDestsStage3Room3:
.long 0x10061, 0x441, 0xffff


.globl airportDoorDestsStage3Room4
airportDoorDestsStage3Room4:
.long 0x10341, 0x562, 0xffff


.globl airportDoorDestsStage3Room5
airportDoorDestsStage3Room5:
.long 0x10462, 0xffff


.globl airportDoorDestsStage3
airportDoorDestsStage3:
.long airportDoorDestsStage3Room0
.long airportDoorDestsStage3Room1
.long airportDoorDestsStage3Room2
.long airportDoorDestsStage3Room3
.long airportDoorDestsStage3Room4
.long airportDoorDestsStage3Room5


.globl doorDests_airport
doorDests_airport:
.long airportDoorDestsStage1
.long airportDoorDestsStage2
.long airportDoorDestsStage3
.long baseDoorDestsStage3


.globl caveDoorDestsStage1Room0
caveDoorDestsStage1Room0:
.long 0x361, 0x262, 0x163, 0x860, 0xffff


.globl caveDoorDestsStage1Room1
caveDoorDestsStage1Room1:
.long 0x30063, 0xffff


.globl caveDoorDestsStage1Room2
caveDoorDestsStage1Room2:
.long 0x20062, 0xffff


.globl caveDoorDestsStage1Room3
caveDoorDestsStage1Room3:
.long 0x10061, 0xffff


.globl caveDoorDestsStage1
caveDoorDestsStage1:
.long caveDoorDestsStage1Room0
.long caveDoorDestsStage1Room1
.long caveDoorDestsStage1Room2
.long caveDoorDestsStage1Room3


.globl caveDoorDestsStage2Room0
caveDoorDestsStage2Room0:
.long 0x340, 0x441, 0x10342, 0x10443, 0x860, 0xffff


.globl caveDoorDestsStage2Room1
caveDoorDestsStage2Room1:
.long 0x20361, 0xffff


.globl caveDoorDestsStage2Room2
caveDoorDestsStage2Room2:
.long 0x20462, 0xffff


.globl caveDoorDestsStage2Room3
caveDoorDestsStage2Room3:
.long 0x161, 0x10040, 0x30042, 0xffff


.globl caveDoorDestsStage2Room4
caveDoorDestsStage2Room4:
.long 0x262, 0x20041, 0x40043, 0xffff


.globl caveDoorDestsStage2
caveDoorDestsStage2:
.long caveDoorDestsStage2Room0
.long caveDoorDestsStage2Room1
.long caveDoorDestsStage2Room2
.long caveDoorDestsStage2Room3
.long caveDoorDestsStage2Room4


.globl caveDoorDestsStage3Room0
caveDoorDestsStage3Room0:
.long 0x140, 0x241, 0x361, 0x462, 0xffff


.globl caveDoorDestsStage3Room1
caveDoorDestsStage3Room1:
.long 0x10040, 0xffff


.globl caveDoorDestsStage3Room2
caveDoorDestsStage3Room2:
.long 0x20041, 0xffff


.globl caveDoorDestsStage3Room3
caveDoorDestsStage3Room3:
.long 0x30061, 0x860, 0xffff


.globl caveDoorDestsStage3Room4
caveDoorDestsStage3Room4:
.long 0x40062, 0xffff


.globl caveDoorDestsStage3
caveDoorDestsStage3:
.long caveDoorDestsStage3Room0
.long caveDoorDestsStage3Room1
.long caveDoorDestsStage3Room2
.long caveDoorDestsStage3Room3
.long caveDoorDestsStage3Room4


.globl doorDests_cave
doorDests_cave:
.long caveDoorDestsStage1
.long caveDoorDestsStage2
.long caveDoorDestsStage3
.long baseDoorDestsStage3


.globl baseDoorDestsStage1Room0
baseDoorDestsStage1Room0:
.long 0x860, 0xffff


.globl baseDoorDestsStage1
baseDoorDestsStage1:
.long baseDoorDestsStage1Room0


.globl baseDoorDestsStage2Room0
baseDoorDestsStage2Room0:
.long 0x860, 0xffff


.globl baseDoorDestsStage2
baseDoorDestsStage2:
.long baseDoorDestsStage2Room0


.globl doorDests_base
doorDests_base:
.long baseDoorDestsStage1
.long baseDoorDestsStage2
.long baseDoorDestsStage3


.globl doorWarpDestinations
doorWarpDestinations:
.long doorDests_bank
.long doorDests_harbor
.long doorDests_airport
.long doorDests_cave
.long doorDests_base
.long doorDests_bank
