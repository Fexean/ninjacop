
@ This file contains door positions for each door of each room.
@ They are formatted like: struct{ u16 x, u16 y;};
@ The first door position in room0 is used as the player's starting position.
@ TODO: place these in room directories


.globl bankDoorsStage1Room0
bankDoorsStage1Room0:
.long 0x100010, 0x800280, 0x100428


.globl bankDoorsStage1Room1
bankDoorsStage1Room1:
.long 0x100078


.globl bankDoorsStage1Room2
bankDoorsStage1Room2:
.long 0x100018


.globl bankDoorsStage1
bankDoorsStage1:
.long bankDoorsStage1Room0
.long bankDoorsStage1Room1
.long bankDoorsStage1Room2


.globl bankDoorsStage2Room0
bankDoorsStage2Room0:
.long 0x100010, 0x800080, 0x1000c8, 0xd00388, 0x700388


.globl bankDoorsStage2Room1
bankDoorsStage2Room1:
.long 0x100078


.globl bankDoorsStage2Room2
bankDoorsStage2Room2:
.long 0x100018


.globl bankDoorsStage2Room3
bankDoorsStage2Room3:
.long 0x100018


.globl bankDoorsStage2Room4
bankDoorsStage2Room4:
.long 0x700078


.globl bankDoorsStage2
bankDoorsStage2:
.long bankDoorsStage2Room0
.long bankDoorsStage2Room1
.long bankDoorsStage2Room2
.long bankDoorsStage2Room3
.long bankDoorsStage2Room4


.globl bankDoorsStage3Room0
bankDoorsStage3Room0:
.long 0x100010, 0x100260, 0x900260, 0x1002e8, 0x8000b8, 0x6002c8


.globl bankDoorsStage3Room1
bankDoorsStage3Room1:
.long 0x100018


.globl bankDoorsStage3Room2
bankDoorsStage3Room2:
.long 0x100078


.globl bankDoorsStage3Room3
bankDoorsStage3Room3:
.long 0x100080


.globl bankDoorsStage3Room4
bankDoorsStage3Room4:
.long 0x100080


.globl bankDoorsStage3
bankDoorsStage3:
.long bankDoorsStage3Room0
.long bankDoorsStage3Room1
.long bankDoorsStage3Room2
.long bankDoorsStage3Room3
.long bankDoorsStage3Room4


.globl bankDoorsStage4Room0
bankDoorsStage4Room0:
.long 0x100050, 0xa00050


.globl bankDoorsStage4
bankDoorsStage4:
.long bankDoorsStage4Room0


.globl bankDoors
bankDoors:
.long bankDoorsStage1
.long bankDoorsStage2
.long bankDoorsStage3
.long bankDoorsStage4


.globl harborDoorsStage1Room0
harborDoorsStage1Room0:
.long 0x100010, 0x1001e8


.globl harborDoorsStage1Room1
harborDoorsStage1Room1:
.long 0x100080, 0x11000f0, 0x1c000f0


.globl harborDoorsStage1Room2
harborDoorsStage1Room2:
.long 0x100010, 0xc00010, 0xc001f0, 0x1001f0


.globl harborDoorsStage1Room3
harborDoorsStage1Room3:
.long 0x200078


.globl harborDoorsStage1Room4
harborDoorsStage1Room4:
.long 0x700078


.globl harborDoorsStage1
harborDoorsStage1:
.long harborDoorsStage1Room0
.long harborDoorsStage1Room1
.long harborDoorsStage1Room2
.long harborDoorsStage1Room3
.long harborDoorsStage1Room4


.globl harborDoorsStage2Room0
harborDoorsStage2Room0:
.long 0x100010, 0x980010, 0x9803f0, 0x5000d0


.globl harborDoorsStage2Room1
harborDoorsStage2Room1:
.long 0x100080


.globl harborDoorsStage2Room2
harborDoorsStage2Room2:
.long 0x680080, 0x1c00080


.globl harborDoorsStage2Room3
harborDoorsStage2Room3:
.long 0x100010


.globl harborDoorsStage2
harborDoorsStage2:
.long harborDoorsStage2Room0
.long harborDoorsStage2Room1
.long harborDoorsStage2Room2
.long harborDoorsStage2Room3


.globl harborDoorsStage3Room0
harborDoorsStage3Room0:
.long 0x500100, 0x1b00010, 0x1b001f0, 0x600010, 0x6001f0


.globl harborDoorsStage3Room1
harborDoorsStage3Room1:
.long 0x100080


.globl harborDoorsStage3Room2
harborDoorsStage3Room2:
.long 0xd00010


.globl harborDoorsStage3Room3
harborDoorsStage3Room3:
.long 0xd00080


.globl harborDoorsStage3Room4
harborDoorsStage3Room4:
.long 0x900010


.globl harborDoorsStage3
harborDoorsStage3:
.long harborDoorsStage3Room0
.long harborDoorsStage3Room1
.long harborDoorsStage3Room2
.long harborDoorsStage3Room3
.long harborDoorsStage3Room4


.globl harborDoorsStage4Room0
harborDoorsStage4Room0:
.long 0x5000a0


.globl harborDoorsStage4
harborDoorsStage4:
.long harborDoorsStage4Room0


.globl harborDoors
harborDoors:
.long harborDoorsStage1
.long harborDoorsStage2
.long harborDoorsStage3
.long harborDoorsStage4


.globl airportDoorsStage1Room0
airportDoorsStage1Room0:
.long 0x100018, 0x1002d8, 0x18802d8, 0xe002e8, 0xe00018


.globl airportDoorsStage1Room1
airportDoorsStage1Room1:
.long 0x100080, 0x1880080, 0x800060


.globl airportDoorsStage1Room2
airportDoorsStage1Room2:
.long 0xc80018


.globl airportDoorsStage1Room3
airportDoorsStage1Room3:
.long 0x1001e8


.globl airportDoorsStage1
airportDoorsStage1:
.long airportDoorsStage1Room0
.long airportDoorsStage1Room1
.long airportDoorsStage1Room2
.long airportDoorsStage1Room3


.globl airportDoorsStage2Room0
airportDoorsStage2Room0:
.long 0x500018, 0x500180, 0x5002e8


.globl airportDoorsStage2Room1
airportDoorsStage2Room1:
.long 0x100018, 0x1800d8


.globl airportDoorsStage2Room2
airportDoorsStage2Room2:
.long 0x180018, 0x1800d8


.globl airportDoorsStage2Room3
airportDoorsStage2Room3:
.long 0x100080, 0xe80080, 0x1c00080


.globl airportDoorsStage2Room4
airportDoorsStage2Room4:
.long 0x100080


.globl airportDoorsStage2Room5
airportDoorsStage2Room5:
.long 0x900100, 0x480010, 0xd00010, 0x4801f0, 0xd001f0


.globl airportDoorsStage2Room6
airportDoorsStage2Room6:
.long 0x4800f0, 0xd000f0


.globl airportDoorsStage2Room7
airportDoorsStage2Room7:
.long 0x480010, 0xd00010


.globl airportDoorsStage2
airportDoorsStage2:
.long airportDoorsStage2Room0
.long airportDoorsStage2Room1
.long airportDoorsStage2Room2
.long airportDoorsStage2Room3
.long airportDoorsStage2Room4
.long airportDoorsStage2Room5
.long airportDoorsStage2Room6
.long airportDoorsStage2Room7


.globl airportDoorsStage3Room0
airportDoorsStage3Room0:
.long 0x200018, 0x2000b8, 0x200268, 0x2004b8, 0x4000e8


.globl airportDoorsStage3Room1
airportDoorsStage3Room1:
.long 0x100078


.globl airportDoorsStage3Room2
airportDoorsStage3Room2:
.long 0x100078


.globl airportDoorsStage3Room3
airportDoorsStage3Room3:
.long 0x6000d8, 0x1000d8


.globl airportDoorsStage3Room4
airportDoorsStage3Room4:
.long 0x1000d8, 0x6000d8


.globl airportDoorsStage3Room5
airportDoorsStage3Room5:
.long 0x100078


.globl airportDoorsStage3
airportDoorsStage3:
.long airportDoorsStage3Room0
.long airportDoorsStage3Room1
.long airportDoorsStage3Room2
.long airportDoorsStage3Room3
.long airportDoorsStage3Room4
.long airportDoorsStage3Room5


.globl airportDoorsStage4Room0
airportDoorsStage4Room0:
.long 0x200010


.globl airportDoorsStage4
airportDoorsStage4:
.long airportDoorsStage4Room0


.globl airportDoors
airportDoors:
.long airportDoorsStage1
.long airportDoorsStage2
.long airportDoorsStage3
.long airportDoorsStage4


.globl caveDoorsStage1Room0
caveDoorsStage1Room0:
.long 0x280080, 0x280020, 0x2800e0, 0x2c80018


.globl caveDoorsStage1Room1
caveDoorsStage1Room1:
.long 0x1800e8, 0x1280098


.globl caveDoorsStage1Room2
caveDoorsStage1Room2:
.long 0x100020


.globl caveDoorsStage1Room3
caveDoorsStage1Room3:
.long 0x100080


.globl caveDoorsStage1
caveDoorsStage1:
.long caveDoorsStage1Room0
.long caveDoorsStage1Room1
.long caveDoorsStage1Room2
.long caveDoorsStage1Room3


.globl caveDoorsStage2Room0
caveDoorsStage2Room0:
.long 0x1980100, 0x10800e8, 0x1080118, 0x5800e8, 0x580118


.globl caveDoorsStage2Room1
caveDoorsStage2Room1:
.long 0x100078


.globl caveDoorsStage2Room2
caveDoorsStage2Room2:
.long 0x100078


.globl caveDoorsStage2Room3
caveDoorsStage2Room3:
.long 0xc000e0, 0x1000e0, 0x14000a8


.globl caveDoorsStage2Room4
caveDoorsStage2Room4:
.long 0xc00020, 0x100020, 0xa800d8


.globl caveDoorsStage2
caveDoorsStage2:
.long caveDoorsStage2Room0
.long caveDoorsStage2Room1
.long caveDoorsStage2Room2
.long caveDoorsStage2Room3
.long caveDoorsStage2Room4


.globl caveDoorsStage3Room0
caveDoorsStage3Room0:
.long 0x480100, 0x1800018, 0x18001e8, 0x480018, 0x4801e8


.globl caveDoorsStage3Room1
caveDoorsStage3Room1:
.long 0xc80080


.globl caveDoorsStage3Room2
caveDoorsStage3Room2:
.long 0x80080


.globl caveDoorsStage3Room3
caveDoorsStage3Room3:
.long 0x800e8


.globl caveDoorsStage3Room4
caveDoorsStage3Room4:
.long 0x100028


.globl caveDoorsStage3
caveDoorsStage3:
.long caveDoorsStage3Room0
.long caveDoorsStage3Room1
.long caveDoorsStage3Room2
.long caveDoorsStage3Room3
.long caveDoorsStage3Room4


.globl caveDoorsStage4Room0
caveDoorsStage4Room0:
.long 0x480018


.globl caveDoorsStage4
caveDoorsStage4:
.long caveDoorsStage4Room0


.globl caveDoors
caveDoors:
.long caveDoorsStage1
.long caveDoorsStage2
.long caveDoorsStage3
.long caveDoorsStage4


.globl baseDoorsStage1Room0
baseDoorsStage1Room0:
.long 0x1800018, 0xb00260


.globl baseDoorsStage1
baseDoorsStage1:
.long baseDoorsStage1Room0


.globl baseDoorsStage2Room0
baseDoorsStage2Room0:
.long 0x4a00080, 0x2000080, 0x2500010


.globl baseDoorsStage2
baseDoorsStage2:
.long baseDoorsStage2Room0


.globl baseDoorsStage3Room0
baseDoorsStage3Room0:
.long 0x100038


.globl baseDoorsStage3
baseDoorsStage3:
.long baseDoorsStage3Room0


.globl baseDoors
baseDoors:
.long baseDoorsStage1
.long baseDoorsStage2
.long baseDoorsStage3


.globl launchDoorsStage1Room0
launchDoorsStage1Room0:
.long 0x100080


.globl launchStage0
launchStage0:
.long launchDoorsStage1Room0


.globl launchDoors
launchDoors:
.long launchStage0


.globl doorPositions
doorPositions:
.long bankDoors
.long harborDoors
.long airportDoors
.long caveDoors
.long baseDoors
.long launchDoors
