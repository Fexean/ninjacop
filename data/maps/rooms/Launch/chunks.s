.globl LAUNCH_STAGE1_ROOM0_LAYER0_5
LAUNCH_STAGE1_ROOM0_LAYER0_5:
.incbin "data/maps/rooms/Launch/Stage_1/Room_0/LAYER0_5.tilemap.lz"


.globl LAUNCH_STAGE1_ROOM0_COLLISION_5
LAUNCH_STAGE1_ROOM0_COLLISION_5:
.incbin "data/maps/rooms/Launch/Stage_1/Room_0/COLLISION_5.tilemap.lz"


.globl LAUNCH_STAGE1_ROOM0_TILESET
LAUNCH_STAGE1_ROOM0_TILESET:
.incbin "build/gfx/level_tilesets/launch.4bpp.lz"


.globl LAUNCH_STAGE1_ROOM0_PALETTE
LAUNCH_STAGE1_ROOM0_PALETTE:
.incbin "build/gfx/level_tilesets/launch.gbapal"


.globl LAUNCH_STAGE1_ROOM0_LAYER0_4
LAUNCH_STAGE1_ROOM0_LAYER0_4:
.incbin "data/maps/rooms/Launch/Stage_1/Room_0/LAYER0_4.tilemap.lz"


.globl LAUNCH_STAGE1_ROOM0_COLLISION_4
LAUNCH_STAGE1_ROOM0_COLLISION_4:
.incbin "data/maps/rooms/Launch/Stage_1/Room_0/COLLISION_4.tilemap.lz"

.globl LAUNCH_STAGE1_ROOM0_LAYER0_3
LAUNCH_STAGE1_ROOM0_LAYER0_3:
.incbin "data/maps/rooms/Launch/Stage_1/Room_0/LAYER0_3.tilemap.lz"


.globl LAUNCH_STAGE1_ROOM0_COLLISION_3
LAUNCH_STAGE1_ROOM0_COLLISION_3:
.incbin "data/maps/rooms/Launch/Stage_1/Room_0/COLLISION_3.tilemap.lz"


.globl LAUNCH_STAGE1_ROOM0_LAYER0_2
LAUNCH_STAGE1_ROOM0_LAYER0_2:
.incbin "data/maps/rooms/Launch/Stage_1/Room_0/LAYER0_2.tilemap.lz"


.globl LAUNCH_STAGE1_ROOM0_COLLISION_2
LAUNCH_STAGE1_ROOM0_COLLISION_2:
.incbin "data/maps/rooms/Launch/Stage_1/Room_0/COLLISION_2.tilemap.lz"


.globl LAUNCH_STAGE1_ROOM0_LAYER0_1
LAUNCH_STAGE1_ROOM0_LAYER0_1:
.incbin "data/maps/rooms/Launch/Stage_1/Room_0/LAYER0_1.tilemap.lz"


.globl LAUNCH_STAGE1_ROOM0_COLLISION_1
LAUNCH_STAGE1_ROOM0_COLLISION_1:
.incbin "data/maps/rooms/Launch/Stage_1/Room_0/COLLISION_1.tilemap.lz"


.globl LAUNCH_STAGE1_ROOM0_LAYER0_0
LAUNCH_STAGE1_ROOM0_LAYER0_0:
.incbin "data/maps/rooms/Launch/Stage_1/Room_0/LAYER0_0.tilemap.lz"


.globl LAUNCH_STAGE1_ROOM0_COLLISION_0
LAUNCH_STAGE1_ROOM0_COLLISION_0:
.incbin "data/maps/rooms/Launch/Stage_1/Room_0/COLLISION_0.tilemap.lz"


.globl LAUNCH_STAGE1_ROOM0_LAYER1_5
LAUNCH_STAGE1_ROOM0_LAYER1_5:
.incbin "data/maps/rooms/Launch/Stage_1/Room_0/LAYER1_5.tilemap.lz"
.incbin "data/maps/dummy.bin"


.globl LAUNCH_STAGE1_ROOM0_LAYER1_4
LAUNCH_STAGE1_ROOM0_LAYER1_4:
.incbin "data/maps/rooms/Launch/Stage_1/Room_0/LAYER1_4.tilemap.lz"


.globl dword_80D4574
dword_80D4574:
.incbin "data/maps/dummy.bin"

.globl LAUNCH_STAGE1_ROOM0_LAYER1_3
LAUNCH_STAGE1_ROOM0_LAYER1_3:
.incbin "data/maps/rooms/Launch/Stage_1/Room_0/LAYER1_3.tilemap.lz"


.globl dword_80D481C
dword_80D481C:
.incbin "data/maps/dummy.bin"

.globl LAUNCH_STAGE1_ROOM0_LAYER1_2
LAUNCH_STAGE1_ROOM0_LAYER1_2:
.incbin "data/maps/rooms/Launch/Stage_1/Room_0/LAYER1_2.tilemap.lz"


.globl dword_80D4ABC
dword_80D4ABC:
.incbin "data/maps/dummy.bin"

.globl LAUNCH_STAGE1_ROOM0_LAYER1_1
LAUNCH_STAGE1_ROOM0_LAYER1_1:
.incbin "data/maps/rooms/Launch/Stage_1/Room_0/LAYER1_1.tilemap.lz"


.globl dword_80D4D6C
dword_80D4D6C:
.incbin "data/maps/dummy.bin"

.globl LAUNCH_STAGE1_ROOM0_LAYER1_0
LAUNCH_STAGE1_ROOM0_LAYER1_0:
.incbin "data/maps/rooms/Launch/Stage_1/Room_0/LAYER1_0.tilemap.lz"
.incbin "data/maps/dummy.bin"


.globl LAUNCH_STAGE1_ROOM0_PARALLAX_CHUNK_0
LAUNCH_STAGE1_ROOM0_PARALLAX_CHUNK_0:
.incbin "data/maps/rooms/Launch/Stage_1/Room_0/PARALLAX_CHUNK_0.bin"
.incbin "data/maps/dummy.bin"


.globl LAUNCH_STAGE1_ROOM0_PARALLAX_CHUNK_1
LAUNCH_STAGE1_ROOM0_PARALLAX_CHUNK_1:
.incbin "data/maps/rooms/Launch/Stage_1/Room_0/PARALLAX_CHUNK_1.bin"
.incbin "data/maps/dummy.bin"

.globl unk_80d5fc4
unk_80d5fc4:
.long 0x3, 0x8480003, 0x108d0003, 0x1cf60003, 0x295c0003, 0x35bf0003, 0x3dff0003, 0x529f0006, 0x3dff0003, 0x35bf0003, 0x295c0003, 0x1cf60003, 0x108d0003, 0x8480003, 0x3, 0x0


.globl unk_80d6004
unk_80d6004:
.long 0x3, 0x20003, 0x70003, 0xf0003, 0x140003, 0x4370003, 0xc7a0003, 0x14bf0006, 0xc7a0003, 0x4370003, 0x140003, 0xf0003, 0x70003, 0x20003, 0x3, 0x0


.globl off_80D6044
off_80D6044:
.long unk_80d5fc4, 0x1e
.long unk_80d6004, 0x1f, 0x0, 0x0


.globl dword_80D605C
dword_80D605C:
.long 0x31ee000a, 0x2d8b0005, 0x31ee0005, 0x2d8b0005, 0x31ee0014, 0x2d8b0005, 0x31ee0005, 0x2d8b0005, 0x31ee000a, 0x0


.globl unk_80d6084
unk_80d6084:
.long 0x2e53000a, 0x29ce0005, 0x2e530005, 0x29ce0005, 0x2e530014, 0x29ce0005, 0x2e530005, 0x29ce0005, 0x2e53000a, 0x0


.globl unk_80d60ac
unk_80d60ac:
.long 0x1212000a, 0x56d0005, 0x12120005, 0x56d0005, 0x12120014, 0x56d0005, 0x12120005, 0x56d0005, 0x1212000a, 0x0


.globl unk_80d60d4
unk_80d60d4:
.long 0x26b7000a, 0x15f10005, 0x26b70005, 0x15f10005, 0x26b70014, 0x15f10005, 0x26b70005, 0x15f10005, 0x26b7000a, 0x0


.globl unk_80d60fc
unk_80d60fc:
.long 0x3b5c000a, 0x2a950005, 0x3b5c0005, 0x2a950005, 0x3b5c0014, 0x2a950005, 0x3b5c0005, 0x2a950005, 0x3b5c000a, 0x0


.globl unk_80d6124
unk_80d6124:
.long 0x4fff000a, 0x3b180005, 0x4fff0005, 0x3b180005, 0x4fff0014, 0x3b180005, 0x4fff0005, 0x3b180005, 0x4fff000a, 0x0


.globl off_80D614C
off_80D614C:
.long dword_80D605C, 0x68
.long unk_80d6084, 0x69
.long unk_80d60ac, 0x6a
.long unk_80d60d4, 0x6b
.long unk_80d60fc, 0x6c
.long unk_80d6124, 0x6d, 0x0, 0x0
