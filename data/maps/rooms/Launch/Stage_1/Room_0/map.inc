LAUNCH_STAGE1_ROOM0_MAPDATA:
.byte 0x1@ height
.byte 0x6	@ width
.2byte 0xa	@ unk2
.4byte LAUNCH_STAGE1_ROOM0_PALETTE	@ palette
.2byte 0x1	@ tilesCompressed
.2byte 0x190	@ uncompTileCount
.4byte LAUNCH_STAGE1_ROOM0_TILESET	@ tileset
.byte 0x20	@ chunk width
.byte 0x20	@ chunk height
.2byte 0x1	@ mapsCompressed
.4byte LAUNCH_STAGE1_ROOM0_LAYER0_0	@ layer0 chunk
.4byte LAUNCH_STAGE1_ROOM0_LAYER0_1	@ layer0 chunk
.4byte LAUNCH_STAGE1_ROOM0_LAYER0_2	@ layer0 chunk
.4byte LAUNCH_STAGE1_ROOM0_LAYER0_3	@ layer0 chunk
.4byte LAUNCH_STAGE1_ROOM0_LAYER0_4	@ layer0 chunk
.4byte LAUNCH_STAGE1_ROOM0_LAYER0_5	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte LAUNCH_STAGE1_ROOM0_LAYER1_0	@ layer1 chunk
.4byte LAUNCH_STAGE1_ROOM0_LAYER1_1	@ layer1 chunk
.4byte LAUNCH_STAGE1_ROOM0_LAYER1_2	@ layer1 chunk
.4byte LAUNCH_STAGE1_ROOM0_LAYER1_3	@ layer1 chunk
.4byte LAUNCH_STAGE1_ROOM0_LAYER1_4	@ layer1 chunk
.4byte LAUNCH_STAGE1_ROOM0_LAYER1_5	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0x1	@ collisionCompressed
.4byte LAUNCH_STAGE1_ROOM0_COLLISION_0	@ collision chunk
.4byte LAUNCH_STAGE1_ROOM0_COLLISION_1	@ collision chunk
.4byte LAUNCH_STAGE1_ROOM0_COLLISION_2	@ collision chunk
.4byte LAUNCH_STAGE1_ROOM0_COLLISION_3	@ collision chunk
.4byte LAUNCH_STAGE1_ROOM0_COLLISION_4	@ collision chunk
.4byte LAUNCH_STAGE1_ROOM0_COLLISION_5	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte (levelfunc_launch0+1)	@ load callbacks
.4byte (levelfunc_launch1+1)	@ load callbacks
.4byte 0	@ load callbacks
.4byte 0	@ load callbacks
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk


LAUNCH_STAGE1_ROOM0_HEADER:
.2byte 0x1a	@ bitfield
.2byte 0x5	@ tilesetStart
.4byte LAUNCH_STAGE1_ROOM0_MAPDATA	@ mapdata
.4byte launchParallaxStruct	@ scroll
.2byte 0x100	@ height
.2byte 0x600	@ width

