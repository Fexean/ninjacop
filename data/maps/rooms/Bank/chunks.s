
.globl BANK_STAGE1_ROOM0_LAYER0_0
BANK_STAGE1_ROOM0_LAYER0_0:
.incbin "data/maps/rooms/Bank/Stage_1/Room_0/LAYER0_0.tilemap.lz"


.globl BANK_STAGE1_ROOM0_COLLISION_0
BANK_STAGE1_ROOM0_COLLISION_0:
.incbin "data/maps/rooms/Bank/Stage_1/Room_0/COLLISION_0.tilemap.lz"


.globl BANK_STAGE1_ROOM2_TILESET
BANK_STAGE1_ROOM2_TILESET:
.incbin "build/gfx/level_tilesets/bank_stage1.4bpp.lz"


.globl BANK_STAGE1_ROOM2_PALETTE
BANK_STAGE1_ROOM2_PALETTE:
.incbin "build/gfx/level_tilesets/bank_stage1.gbapal"


.globl BANK_STAGE1_ROOM0_LAYER0_1
BANK_STAGE1_ROOM0_LAYER0_1:
.incbin "data/maps/rooms/Bank/Stage_1/Room_0/LAYER0_1.tilemap.lz"


.globl BANK_STAGE1_ROOM0_COLLISION_1
BANK_STAGE1_ROOM0_COLLISION_1:
.incbin "data/maps/rooms/Bank/Stage_1/Room_0/COLLISION_1.tilemap.lz"


.globl BANK_STAGE1_ROOM0_LAYER0_2
BANK_STAGE1_ROOM0_LAYER0_2:
.incbin "data/maps/rooms/Bank/Stage_1/Room_0/LAYER0_2.tilemap.lz"


.globl BANK_STAGE1_ROOM0_COLLISION_2
BANK_STAGE1_ROOM0_COLLISION_2:
.incbin "data/maps/rooms/Bank/Stage_1/Room_0/COLLISION_2.tilemap.lz"


.globl BANK_STAGE1_ROOM0_LAYER0_3
BANK_STAGE1_ROOM0_LAYER0_3:
.incbin "data/maps/rooms/Bank/Stage_1/Room_0/LAYER0_3.tilemap.lz"


.globl BANK_STAGE1_ROOM0_COLLISION_3
BANK_STAGE1_ROOM0_COLLISION_3:
.incbin "data/maps/rooms/Bank/Stage_1/Room_0/COLLISION_3.tilemap.lz"


.globl BANK_STAGE1_ROOM0_LAYER0_4
BANK_STAGE1_ROOM0_LAYER0_4:
.incbin "data/maps/rooms/Bank/Stage_1/Room_0/LAYER0_4.tilemap.lz"


.globl BANK_STAGE1_ROOM0_COLLISION_4
BANK_STAGE1_ROOM0_COLLISION_4:
.incbin "data/maps/rooms/Bank/Stage_1/Room_0/COLLISION_4.tilemap.lz"


.globl BANK_STAGE1_ROOM1_LAYER0_0
BANK_STAGE1_ROOM1_LAYER0_0:
.incbin "data/maps/rooms/Bank/Stage_1/Room_1/LAYER0_0.tilemap.lz"


.globl BANK_STAGE1_ROOM1_COLLISION_0
BANK_STAGE1_ROOM1_COLLISION_0:
.incbin "data/maps/rooms/Bank/Stage_1/Room_1/COLLISION_0.tilemap.lz"


.globl BANK_STAGE1_ROOM2_LAYER0_0
BANK_STAGE1_ROOM2_LAYER0_0:
.incbin "data/maps/rooms/Bank/Stage_1/Room_2/LAYER0_0.tilemap.lz"


.globl BANK_STAGE1_ROOM2_COLLISION_0
BANK_STAGE1_ROOM2_COLLISION_0:
.incbin "data/maps/rooms/Bank/Stage_1/Room_2/COLLISION_0.tilemap.lz"


.globl BANK_STAGE1_ROOM0_PARALLAX_CHUNK_0
BANK_STAGE1_ROOM0_PARALLAX_CHUNK_0:
.incbin "data/maps/rooms/Bank/Stage_3/Room_0/PARALLAX_CHUNK_0.bin"


.globl BANK_STAGE1_ROOM0_LAYER1_0
BANK_STAGE1_ROOM0_LAYER1_0:
.incbin "data/maps/rooms/Bank/Stage_1/Room_0/LAYER1_0.tilemap.lz"

.globl BANK_STAGE1_ROOM0_LAYER1_1
BANK_STAGE1_ROOM0_LAYER1_1:
.incbin "data/maps/rooms/Bank/Stage_1/Room_0/LAYER1_1.tilemap.lz"

.globl BANK_STAGE1_ROOM0_LAYER1_2
BANK_STAGE1_ROOM0_LAYER1_2:
.incbin "data/maps/rooms/Bank/Stage_1/Room_0/LAYER1_2.tilemap.lz"

.globl BANK_STAGE1_ROOM0_LAYER1_3
BANK_STAGE1_ROOM0_LAYER1_3:
.incbin "data/maps/rooms/Bank/Stage_1/Room_0/LAYER1_3.tilemap.lz"


.globl BANK_STAGE1_ROOM0_LAYER1_4
BANK_STAGE1_ROOM0_LAYER1_4:
.incbin "data/maps/rooms/Bank/Stage_1/Room_0/LAYER1_4.tilemap.lz"

.globl BANK_STAGE1_ROOM1_LAYER1_0
BANK_STAGE1_ROOM1_LAYER1_0:
.incbin "data/maps/rooms/Bank/Stage_1/Room_1/LAYER1_0.tilemap.lz" 


.globl BANK_STAGE1_ROOM2_LAYER1_0
BANK_STAGE1_ROOM2_LAYER1_0:
.incbin "data/maps/rooms/Bank/Stage_1/Room_2/LAYER1_0.tilemap.lz"


.globl unk_8043d5c
unk_8043d5c:
.long 0x11000f, 0xc740009, 0x14b60009, 0x1cf8000f, 0x14b60009, 0xc740009, 0x0


.globl unk_8043d78
unk_8043d78:
.long 0x874000f, 0x14d70009, 0x1d190009, 0x255b000f, 0x1d190009, 0x14d70009, 0x0


.globl unk_8043d94
unk_8043d94:
.long 0x14d7000f, 0x21390009, 0x297b0009, 0x31bd000f, 0x297b0009, 0x21390009, 0x0


.globl unk_8043db0
unk_8043db0:
.long 0x213a000f, 0x2d9b0009, 0x35dd0009, 0x3e1f000f, 0x35dd0009, 0x2d9b0009, 0x0


.globl unk_8043dcc
unk_8043dcc:
.long 0x2d9d000f, 0x39fe0009, 0x423e0009, 0x4a7f000f, 0x423e0009, 0x39fe0009, 0x0


.globl off_8043DE8
off_8043DE8:
.long unk_8043d5c, 0x86
.long unk_8043d78, 0x87
.long unk_8043d94, 0x88
.long unk_8043db0, 0x89
.long unk_8043dcc, 0x8a, 0x0, 0x0


.globl BANK_STAGE2_ROOM0_LAYER0_0
BANK_STAGE2_ROOM0_LAYER0_0:
.incbin "data/maps/rooms/Bank/Stage_2/Room_0/LAYER0_0.tilemap.lz"


.globl BANK_STAGE2_ROOM0_COLLISION_0
BANK_STAGE2_ROOM0_COLLISION_0:
.incbin "data/maps/rooms/Bank/Stage_2/Room_0/COLLISION_0.tilemap.lz"


.globl BANK_STAGE2_ROOM4_TILESET
BANK_STAGE2_ROOM4_TILESET:
.incbin "build/gfx/level_tilesets/bank_stage2.4bpp.lz"

.globl BANK_STAGE2_ROOM1_PALETTE
BANK_STAGE2_ROOM1_PALETTE:
.incbin "build/gfx/level_tilesets/bank_stage2.gbapal"


.globl BANK_STAGE2_ROOM0_LAYER1_0
BANK_STAGE2_ROOM0_LAYER1_0:
.incbin "data/maps/rooms/Bank/Stage_2/Room_0/LAYER1_0.tilemap.lz"


.globl BANK_STAGE2_ROOM0_LAYER0_1
BANK_STAGE2_ROOM0_LAYER0_1:
.incbin "data/maps/rooms/Bank/Stage_2/Room_0/LAYER0_1.tilemap.lz"


.globl BANK_STAGE2_ROOM0_COLLISION_1
BANK_STAGE2_ROOM0_COLLISION_1:
.incbin "data/maps/rooms/Bank/Stage_2/Room_0/COLLISION_1.tilemap.lz"


.globl BANK_STAGE2_ROOM0_LAYER1_1
BANK_STAGE2_ROOM0_LAYER1_1:
.incbin "data/maps/rooms/Bank/Stage_2/Room_0/LAYER1_1.tilemap.lz"


.globl BANK_STAGE2_ROOM0_LAYER0_2
BANK_STAGE2_ROOM0_LAYER0_2:
.incbin "data/maps/rooms/Bank/Stage_2/Room_0/LAYER0_2.tilemap.lz"


.globl BANK_STAGE2_ROOM0_COLLISION_2
BANK_STAGE2_ROOM0_COLLISION_2:
.incbin "data/maps/rooms/Bank/Stage_2/Room_0/COLLISION_2.tilemap.lz"


.globl BANK_STAGE2_ROOM0_LAYER1_2
BANK_STAGE2_ROOM0_LAYER1_2:
.incbin "data/maps/rooms/Bank/Stage_2/Room_0/LAYER1_2.tilemap.lz"


.globl BANK_STAGE2_ROOM0_LAYER0_3
BANK_STAGE2_ROOM0_LAYER0_3:
.incbin "data/maps/rooms/Bank/Stage_2/Room_0/LAYER0_3.tilemap.lz"


.globl BANK_STAGE2_ROOM0_COLLISION_3
BANK_STAGE2_ROOM0_COLLISION_3:
.incbin "data/maps/rooms/Bank/Stage_2/Room_0/COLLISION_3.tilemap.lz"


.globl BANK_STAGE2_ROOM0_LAYER1_3
BANK_STAGE2_ROOM0_LAYER1_3:
.incbin "data/maps/rooms/Bank/Stage_2/Room_0/LAYER1_3.tilemap.lz"


.globl BANK_STAGE2_ROOM1_LAYER0_0
BANK_STAGE2_ROOM1_LAYER0_0:
.incbin "data/maps/rooms/Bank/Stage_2/Room_1/LAYER0_0.tilemap.lz"


.globl BANK_STAGE2_ROOM1_COLLISION_0
BANK_STAGE2_ROOM1_COLLISION_0:
.incbin "data/maps/rooms/Bank/Stage_2/Room_1/COLLISION_0.tilemap.lz"


.globl BANK_STAGE2_ROOM1_LAYER1_0
BANK_STAGE2_ROOM1_LAYER1_0:
.incbin "data/maps/rooms/Bank/Stage_2/Room_1/LAYER1_0.tilemap.lz"

.globl BANK_STAGE2_ROOM2_LAYER0_0
BANK_STAGE2_ROOM2_LAYER0_0:
.incbin "data/maps/rooms/Bank/Stage_2/Room_2/LAYER0_0.tilemap.lz"


.globl BANK_STAGE2_ROOM2_COLLISION_0
BANK_STAGE2_ROOM2_COLLISION_0:
.incbin "data/maps/rooms/Bank/Stage_2/Room_2/COLLISION_0.tilemap.lz"


.globl BANK_STAGE2_ROOM3_LAYER0_0
BANK_STAGE2_ROOM3_LAYER0_0:
.incbin "data/maps/rooms/Bank/Stage_2/Room_3/LAYER0_0.tilemap.lz"


.globl BANK_STAGE2_ROOM3_COLLISION_0
BANK_STAGE2_ROOM3_COLLISION_0:
.incbin "data/maps/rooms/Bank/Stage_2/Room_3/COLLISION_0.tilemap.lz"


.globl BANK_STAGE2_ROOM3_LAYER1_0
BANK_STAGE2_ROOM3_LAYER1_0:
.incbin "data/maps/rooms/Bank/Stage_2/Room_3/LAYER1_0.tilemap.lz"


.globl BANK_STAGE2_ROOM4_LAYER0_0
BANK_STAGE2_ROOM4_LAYER0_0:
.incbin "data/maps/rooms/Bank/Stage_2/Room_4/LAYER0_0.tilemap.lz"


.globl BANK_STAGE2_ROOM4_COLLISION_0
BANK_STAGE2_ROOM4_COLLISION_0:
.incbin "data/maps/rooms/Bank/Stage_2/Room_4/COLLISION_0.tilemap.lz"

.globl BANK_STAGE2_ROOM4_LAYER1_0
BANK_STAGE2_ROOM4_LAYER1_0:
.incbin "data/maps/rooms/Bank/Stage_2/Room_4/LAYER1_0.tilemap.lz"


.globl BANK_STAGE2_ROOM2_LAYER1_0
BANK_STAGE2_ROOM2_LAYER1_0:
.incbin "data/maps/rooms/Bank/Stage_2/Room_2/LAYER1_0.tilemap.lz"


.globl unk_804afdc
unk_804afdc:
.long 0x11000f, 0xc740009, 0x14b60009, 0x1cf8000f, 0x14b60009, 0xc740009, 0x0


.globl unk_804aff8
unk_804aff8:
.long 0x874000f, 0x14d70009, 0x1d190009, 0x255b000f, 0x1d190009, 0x14d70009, 0x0


.globl unk_804b014
unk_804b014:
.long 0x14d7000f, 0x21390009, 0x297b0009, 0x31bd000f, 0x297b0009, 0x21390009, 0x0


.globl unk_804b030
unk_804b030:
.long 0x213a000f, 0x2d9b0009, 0x35dd0009, 0x3e1f000f, 0x35dd0009, 0x2d9b0009, 0x0


.globl unk_804b04c
unk_804b04c:
.long 0x2d9d000f, 0x39fe0009, 0x423e0009, 0x4a7f000f, 0x423e0009, 0x39fe0009, 0x0


.globl off_804B068
off_804B068:
.long unk_804afdc, 0x86
.long unk_804aff8, 0x87
.long unk_804b014, 0x88
.long unk_804b030, 0x89
.long unk_804b04c, 0x8a, 0x0, 0x0


.globl BANK_STAGE3_ROOM0_LAYER0_0
BANK_STAGE3_ROOM0_LAYER0_0:
.incbin "data/maps/rooms/Bank/Stage_3/Room_0/LAYER0_0.tilemap.lz"


.globl BANK_STAGE3_ROOM0_COLLISION_0
BANK_STAGE3_ROOM0_COLLISION_0:
.incbin "data/maps/rooms/Bank/Stage_3/Room_0/COLLISION_0.tilemap.lz"


.globl BANK_STAGE3_ROOM4_TILESET
BANK_STAGE3_ROOM4_TILESET:
.incbin "build/gfx/level_tilesets/bank_stage3.4bpp.lz"


.globl BANK_STAGE3_ROOM3_PALETTE
BANK_STAGE3_ROOM3_PALETTE:
.incbin "build/gfx/level_tilesets/bank_stage3.gbapal"


.globl BANK_STAGE3_ROOM0_LAYER1_0
BANK_STAGE3_ROOM0_LAYER1_0:
.incbin "data/maps/rooms/Bank/Stage_3/Room_0/LAYER1_0.tilemap.lz"


.globl BANK_STAGE3_ROOM0_LAYER0_1
BANK_STAGE3_ROOM0_LAYER0_1:
.incbin "data/maps/rooms/Bank/Stage_3/Room_0/LAYER0_1.tilemap.lz"


.globl BANK_STAGE3_ROOM0_COLLISION_1
BANK_STAGE3_ROOM0_COLLISION_1:
.incbin "data/maps/rooms/Bank/Stage_3/Room_0/COLLISION_1.tilemap.lz"


.globl BANK_STAGE3_ROOM0_LAYER1_1
BANK_STAGE3_ROOM0_LAYER1_1:
.incbin "data/maps/rooms/Bank/Stage_3/Room_0/LAYER1_1.tilemap.lz"


.globl BANK_STAGE3_ROOM0_LAYER0_2
BANK_STAGE3_ROOM0_LAYER0_2:
.incbin "data/maps/rooms/Bank/Stage_3/Room_0/LAYER0_2.tilemap.lz"



.globl BANK_STAGE3_ROOM0_COLLISION_2
BANK_STAGE3_ROOM0_COLLISION_2:
.incbin "data/maps/rooms/Bank/Stage_3/Room_0/COLLISION_2.tilemap.lz"


.globl BANK_STAGE3_ROOM0_LAYER1_2
BANK_STAGE3_ROOM0_LAYER1_2:
.incbin "data/maps/rooms/Bank/Stage_3/Room_0/LAYER1_2.tilemap.lz"


.globl BANK_STAGE3_ROOM1_LAYER0_0
BANK_STAGE3_ROOM1_LAYER0_0:
.incbin "data/maps/rooms/Bank/Stage_3/Room_1/LAYER0_0.tilemap.lz"


.globl BANK_STAGE3_ROOM1_COLLISION_0
BANK_STAGE3_ROOM1_COLLISION_0:
.incbin "data/maps/rooms/Bank/Stage_3/Room_1/COLLISION_0.tilemap.lz"


.globl BANK_STAGE3_ROOM1_LAYER1_0
BANK_STAGE3_ROOM1_LAYER1_0:
.incbin "data/maps/rooms/Bank/Stage_3/Room_1/LAYER1_0.tilemap.lz"

.globl BANK_STAGE3_ROOM2_LAYER0_0
BANK_STAGE3_ROOM2_LAYER0_0:
.incbin "data/maps/rooms/Bank/Stage_3/Room_2/LAYER0_0.tilemap.lz"


.globl BANK_STAGE3_ROOM2_COLLISION_0
BANK_STAGE3_ROOM2_COLLISION_0:
.incbin "data/maps/rooms/Bank/Stage_3/Room_2/COLLISION_0.tilemap.lz"


.globl BANK_STAGE3_ROOM2_LAYER1_0
BANK_STAGE3_ROOM2_LAYER1_0:
.incbin "data/maps/rooms/Bank/Stage_3/Room_2/LAYER1_0.tilemap.lz"

.globl BANK_STAGE3_ROOM3_LAYER0_1
BANK_STAGE3_ROOM3_LAYER0_1:
.incbin "data/maps/rooms/Bank/Stage_3/Room_3/LAYER0_1.tilemap.lz"


.globl BANK_STAGE3_ROOM3_COLLISION_1
BANK_STAGE3_ROOM3_COLLISION_1:
.incbin "data/maps/rooms/Bank/Stage_3/Room_3/COLLISION_1.tilemap.lz"


.globl BANK_STAGE3_ROOM3_LAYER1_1
BANK_STAGE3_ROOM3_LAYER1_1:
.incbin "data/maps/rooms/Bank/Stage_3/Room_3/LAYER1_1.tilemap.lz"

.globl BANK_STAGE3_ROOM3_LAYER0_0
BANK_STAGE3_ROOM3_LAYER0_0:
.incbin "data/maps/rooms/Bank/Stage_3/Room_3/LAYER0_0.tilemap.lz"


.globl BANK_STAGE3_ROOM3_COLLISION_0
BANK_STAGE3_ROOM3_COLLISION_0:
.incbin "data/maps/rooms/Bank/Stage_3/Room_3/COLLISION_0.tilemap.lz"


.globl BANK_STAGE3_ROOM3_LAYER1_0
BANK_STAGE3_ROOM3_LAYER1_0:
.incbin "data/maps/rooms/Bank/Stage_3/Room_3/LAYER1_0.tilemap.lz"


.globl BANK_STAGE3_ROOM4_LAYER0_1
BANK_STAGE3_ROOM4_LAYER0_1:
.incbin "data/maps/rooms/Bank/Stage_3/Room_4/LAYER0_1.tilemap.lz"


.globl BANK_STAGE3_ROOM4_COLLISION_1
BANK_STAGE3_ROOM4_COLLISION_1:
.incbin "data/maps/rooms/Bank/Stage_3/Room_4/COLLISION_1.tilemap.lz"


.globl BANK_STAGE3_ROOM4_LAYER1_1
BANK_STAGE3_ROOM4_LAYER1_1:
.incbin "data/maps/rooms/Bank/Stage_3/Room_4/LAYER1_1.tilemap.lz"


.globl BANK_STAGE3_ROOM4_LAYER0_0
BANK_STAGE3_ROOM4_LAYER0_0:
.incbin "data/maps/rooms/Bank/Stage_3/Room_4/LAYER0_0.tilemap.lz"


.globl BANK_STAGE3_ROOM4_COLLISION_0
BANK_STAGE3_ROOM4_COLLISION_0:
.incbin "data/maps/rooms/Bank/Stage_3/Room_4/COLLISION_0.tilemap.lz"


.globl BANK_STAGE3_ROOM4_LAYER1_0
BANK_STAGE3_ROOM4_LAYER1_0:
.incbin "data/maps/rooms/Bank/Stage_3/Room_4/LAYER1_0.tilemap.lz"




.globl unk_8053a40
unk_8053a40:
.long 0x11000f, 0xc740009, 0x14b60009, 0x1cf8000f, 0x14b60009, 0xc740009, 0x0


.globl unk_8053a5c
unk_8053a5c:
.long 0x874000f, 0x14d70009, 0x1d190009, 0x255b000f, 0x1d190009, 0x14d70009, 0x0


.globl unk_8053a78
unk_8053a78:
.long 0x14d7000f, 0x21390009, 0x297b0009, 0x31bd000f, 0x297b0009, 0x21390009, 0x0


.globl unk_8053a94
unk_8053a94:
.long 0x213a000f, 0x2d9b0009, 0x35dd0009, 0x3e1f000f, 0x35dd0009, 0x2d9b0009, 0x0


.globl unk_8053ab0
unk_8053ab0:
.long 0x2d9d000f, 0x39fe0009, 0x423e0009, 0x4a7f000f, 0x423e0009, 0x39fe0009, 0x0


.globl off_8053ACC
off_8053ACC:
.long unk_8053a40, 0x86
.long unk_8053a5c, 0x87
.long unk_8053a78, 0x88
.long unk_8053a94, 0x89
.long unk_8053ab0, 0x8a, 0x0, 0x0


.globl BANK_STAGE4_ROOM0_LAYER0_0
BANK_STAGE4_ROOM0_LAYER0_0:
.incbin "data/maps/rooms/Bank/Stage_4/Room_0/LAYER0_0.tilemap.lz"


.globl BANK_STAGE4_ROOM0_COLLISION_0
BANK_STAGE4_ROOM0_COLLISION_0:
.incbin "data/maps/rooms/Bank/Stage_4/Room_0/COLLISION_0.tilemap.lz"

.globl BANK_STAGE4_ROOM0_TILESET
BANK_STAGE4_ROOM0_TILESET:
.incbin "build/gfx/level_tilesets/bank_stage4.4bpp.lz"


.globl BANK_STAGE4_ROOM0_PALETTE
BANK_STAGE4_ROOM0_PALETTE:
.incbin "build/gfx/level_tilesets/bank_stage4.gbapal"


.globl BANK_STAGE4_ROOM0_PARALLAX_CHUNK_0
BANK_STAGE4_ROOM0_PARALLAX_CHUNK_0:
.incbin "data/maps/rooms/Bank/Stage_4/Room_0/PARALLAX_CHUNK_0.bin"



.globl unk_8056724
unk_8056724:
.long 0x1f0006, 0x170006, 0xf0006, 0x170006, 0x0


.globl unk_8056738
unk_8056738:
.long 0x170006, 0x1f0006, 0x170006, 0xf0006, 0x0


.globl unk_805674c
unk_805674c:
.long 0xf0006, 0x170006, 0x1f0006, 0x170006, 0x0

.globl unk_8056760
unk_8056760:
.long 0x170006, 0xf0006, 0x170006, 0x1f0006, 0x0

@ Unused
.long unk_8056724, 0xbc
.long unk_8056738, 0xbd
.long unk_805674c, 0xbe
.long unk_8056760, 0xbf, 0x0, 0x0
