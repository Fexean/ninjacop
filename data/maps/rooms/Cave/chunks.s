
.globl CAVE_STAGE1_ROOM0_LAYER0_2
CAVE_STAGE1_ROOM0_LAYER0_2:
.incbin "data/maps/rooms/Cave/Stage_1/Room_0/LAYER0_2.tilemap.lz"

.globl CAVE_STAGE1_ROOM0_COLLISION_2
CAVE_STAGE1_ROOM0_COLLISION_2:
.incbin "data/maps/rooms/Cave/Stage_1/Room_0/COLLISION_2.tilemap.lz"

.globl CAVE_STAGE1_ROOM0_TILESET
CAVE_STAGE1_ROOM0_TILESET:
.incbin "build/gfx/level_tilesets/cave.4bpp.lz"

.globl CAVE_STAGE3_ROOM3_PALETTE
CAVE_STAGE3_ROOM3_PALETTE:
.incbin "build/gfx/level_tilesets/cave.gbapal"


.globl CAVE_STAGE1_ROOM0_LAYER0_1
CAVE_STAGE1_ROOM0_LAYER0_1:
.incbin "data/maps/rooms/Cave/Stage_1/Room_0/LAYER0_1.tilemap.lz"


.globl CAVE_STAGE1_ROOM0_COLLISION_1
CAVE_STAGE1_ROOM0_COLLISION_1:
.incbin "data/maps/rooms/Cave/Stage_1/Room_0/COLLISION_1.tilemap.lz"


.globl CAVE_STAGE1_ROOM0_LAYER0_0
CAVE_STAGE1_ROOM0_LAYER0_0:
.incbin "data/maps/rooms/Cave/Stage_1/Room_0/LAYER0_0.tilemap.lz"


.globl CAVE_STAGE1_ROOM0_COLLISION_0
CAVE_STAGE1_ROOM0_COLLISION_0:
.incbin "data/maps/rooms/Cave/Stage_1/Room_0/COLLISION_0.tilemap.lz"


.globl CAVE_STAGE1_ROOM1_LAYER0_1
CAVE_STAGE1_ROOM1_LAYER0_1:
.incbin "data/maps/rooms/Cave/Stage_1/Room_1/LAYER0_1.tilemap.lz"


.globl CAVE_STAGE1_ROOM1_COLLISION_1
CAVE_STAGE1_ROOM1_COLLISION_1:
.incbin "data/maps/rooms/Cave/Stage_1/Room_1/COLLISION_1.tilemap.lz"

.globl CAVE_STAGE1_ROOM1_LAYER0_0
CAVE_STAGE1_ROOM1_LAYER0_0:
.incbin "data/maps/rooms/Cave/Stage_1/Room_1/LAYER0_0.tilemap.lz"


.globl CAVE_STAGE1_ROOM1_COLLISION_0
CAVE_STAGE1_ROOM1_COLLISION_0:
.incbin "data/maps/rooms/Cave/Stage_1/Room_1/COLLISION_0.tilemap.lz"


.globl CAVE_STAGE1_ROOM2_LAYER0_0
CAVE_STAGE1_ROOM2_LAYER0_0:
.incbin "data/maps/rooms/Cave/Stage_1/Room_2/LAYER0_0.tilemap.lz"


.globl CAVE_STAGE1_ROOM2_COLLISION_0
CAVE_STAGE1_ROOM2_COLLISION_0:
.incbin "data/maps/rooms/Cave/Stage_1/Room_2/COLLISION_0.tilemap.lz"

.globl CAVE_STAGE1_ROOM2_LAYER0_1
CAVE_STAGE1_ROOM2_LAYER0_1:
.incbin "data/maps/rooms/Cave/Stage_1/Room_2/LAYER0_1.tilemap.lz"


.globl CAVE_STAGE1_ROOM2_COLLISION_1
CAVE_STAGE1_ROOM2_COLLISION_1:
.incbin "data/maps/rooms/Cave/Stage_1/Room_2/COLLISION_1.tilemap.lz"

.globl CAVE_STAGE1_ROOM3_LAYER0_0
CAVE_STAGE1_ROOM3_LAYER0_0:
.incbin "data/maps/rooms/Cave/Stage_1/Room_3/LAYER0_0.tilemap.lz"


.globl CAVE_STAGE1_ROOM3_COLLISION_0
CAVE_STAGE1_ROOM3_COLLISION_0:
.incbin "data/maps/rooms/Cave/Stage_1/Room_3/COLLISION_0.tilemap.lz"


.globl CAVE_STAGE2_ROOM0_LAYER0_0
CAVE_STAGE2_ROOM0_LAYER0_0:
.incbin "data/maps/rooms/Cave/Stage_2/Room_0/LAYER0_0.tilemap.lz"


.globl CAVE_STAGE2_ROOM0_COLLISION_0
CAVE_STAGE2_ROOM0_COLLISION_0:
.incbin "data/maps/rooms/Cave/Stage_2/Room_0/COLLISION_0.tilemap.lz"


.globl CAVE_STAGE2_ROOM0_LAYER0_1
CAVE_STAGE2_ROOM0_LAYER0_1:
.incbin "data/maps/rooms/Cave/Stage_2/Room_0/LAYER0_1.tilemap.lz"


.globl CAVE_STAGE2_ROOM0_COLLISION_1
CAVE_STAGE2_ROOM0_COLLISION_1:
.incbin "data/maps/rooms/Cave/Stage_2/Room_0/COLLISION_1.tilemap.lz"


.globl CAVE_STAGE2_ROOM0_LAYER0_2
CAVE_STAGE2_ROOM0_LAYER0_2:
.incbin "data/maps/rooms/Cave/Stage_2/Room_0/LAYER0_2.tilemap.lz"


.globl CAVE_STAGE2_ROOM0_COLLISION_2
CAVE_STAGE2_ROOM0_COLLISION_2:
.incbin "data/maps/rooms/Cave/Stage_2/Room_0/COLLISION_2.tilemap.lz"


.globl CAVE_STAGE2_ROOM0_LAYER0_3
CAVE_STAGE2_ROOM0_LAYER0_3:
.incbin "data/maps/rooms/Cave/Stage_2/Room_0/LAYER0_3.tilemap.lz"


.globl CAVE_STAGE2_ROOM0_COLLISION_3
CAVE_STAGE2_ROOM0_COLLISION_3:
.incbin "data/maps/rooms/Cave/Stage_2/Room_0/COLLISION_3.tilemap.lz"

.globl CAVE_STAGE2_ROOM3_LAYER0_0
CAVE_STAGE2_ROOM3_LAYER0_0:
.incbin "data/maps/rooms/Cave/Stage_2/Room_3/LAYER0_0.tilemap.lz"


.globl CAVE_STAGE2_ROOM3_COLLISION_0
CAVE_STAGE2_ROOM3_COLLISION_0:
.incbin "data/maps/rooms/Cave/Stage_2/Room_3/COLLISION_0.tilemap.lz"


.globl CAVE_STAGE2_ROOM3_LAYER0_1
CAVE_STAGE2_ROOM3_LAYER0_1:
.incbin "data/maps/rooms/Cave/Stage_2/Room_3/LAYER0_1.tilemap.lz"


.globl CAVE_STAGE2_ROOM3_COLLISION_1
CAVE_STAGE2_ROOM3_COLLISION_1:
.incbin "data/maps/rooms/Cave/Stage_2/Room_3/COLLISION_1.tilemap.lz"


.globl CAVE_STAGE2_ROOM4_LAYER0_0
CAVE_STAGE2_ROOM4_LAYER0_0:
.incbin "data/maps/rooms/Cave/Stage_2/Room_4/LAYER0_0.tilemap.lz"


.globl CAVE_STAGE2_ROOM4_COLLISION_0
CAVE_STAGE2_ROOM4_COLLISION_0:
.incbin "data/maps/rooms/Cave/Stage_2/Room_4/COLLISION_0.tilemap.lz"

.globl CAVE_STAGE2_ROOM4_LAYER0_1
CAVE_STAGE2_ROOM4_LAYER0_1:
.incbin "data/maps/rooms/Cave/Stage_2/Room_4/LAYER0_1.tilemap.lz"


.globl CAVE_STAGE2_ROOM4_COLLISION_1
CAVE_STAGE2_ROOM4_COLLISION_1:
.incbin "data/maps/rooms/Cave/Stage_2/Room_4/COLLISION_1.tilemap.lz"


.globl CAVE_STAGE2_ROOM1_LAYER0_0
CAVE_STAGE2_ROOM1_LAYER0_0:
.incbin "data/maps/rooms/Cave/Stage_2/Room_1/LAYER0_0.tilemap.lz"


.globl CAVE_STAGE2_ROOM1_COLLISION_0
CAVE_STAGE2_ROOM1_COLLISION_0:
.incbin "data/maps/rooms/Cave/Stage_2/Room_1/COLLISION_0.tilemap.lz"


.globl CAVE_STAGE2_ROOM2_LAYER0_0
CAVE_STAGE2_ROOM2_LAYER0_0:
.incbin "data/maps/rooms/Cave/Stage_2/Room_2/LAYER0_0.tilemap.lz"



.globl CAVE_STAGE2_ROOM2_COLLISION_0
CAVE_STAGE2_ROOM2_COLLISION_0:
.incbin "data/maps/rooms/Cave/Stage_2/Room_2/COLLISION_0.tilemap.lz"


.globl CAVE_STAGE4_ROOM0_LAYER0_0
CAVE_STAGE4_ROOM0_LAYER0_0:
.incbin "data/maps/rooms/Cave/Stage_4/Room_0/LAYER0_0.tilemap.lz"


.globl CAVE_STAGE4_ROOM0_COLLISION_0
CAVE_STAGE4_ROOM0_COLLISION_0:
.incbin "data/maps/rooms/Cave/Stage_4/Room_0/COLLISION_0.tilemap.lz"


.globl CAVE_STAGE1_ROOM0_LAYER1_0
CAVE_STAGE1_ROOM0_LAYER1_0:
.incbin "data/maps/rooms/Cave/Stage_1/Room_0/LAYER1_0.tilemap.lz"


.globl CAVE_STAGE1_ROOM0_LAYER1_1
CAVE_STAGE1_ROOM0_LAYER1_1:
.incbin "data/maps/rooms/Cave/Stage_1/Room_0/LAYER1_1.tilemap.lz"

.globl CAVE_STAGE1_ROOM0_LAYER1_2
CAVE_STAGE1_ROOM0_LAYER1_2:
.incbin "data/maps/rooms/Cave/Stage_1/Room_0/LAYER1_2.tilemap.lz"

.globl CAVE_STAGE1_ROOM1_LAYER1_0
CAVE_STAGE1_ROOM1_LAYER1_0:
.incbin "data/maps/rooms/Cave/Stage_1/Room_1/LAYER1_0.tilemap.lz"

.globl CAVE_STAGE1_ROOM1_LAYER1_1
CAVE_STAGE1_ROOM1_LAYER1_1:
.incbin "data/maps/rooms/Cave/Stage_1/Room_1/LAYER1_1.tilemap.lz"

.globl CAVE_STAGE1_ROOM2_LAYER1_0
CAVE_STAGE1_ROOM2_LAYER1_0:
.incbin "data/maps/rooms/Cave/Stage_1/Room_2/LAYER1_0.tilemap.lz"

.globl CAVE_STAGE1_ROOM2_LAYER1_1
CAVE_STAGE1_ROOM2_LAYER1_1:
.incbin "data/maps/rooms/Cave/Stage_1/Room_2/LAYER1_1.tilemap.lz"


.globl CAVE_STAGE1_ROOM3_LAYER1_0
CAVE_STAGE1_ROOM3_LAYER1_0:
.incbin "data/maps/rooms/Cave/Stage_1/Room_3/LAYER1_0.tilemap.lz"

.globl CAVE_STAGE2_ROOM0_LAYER1_0
CAVE_STAGE2_ROOM0_LAYER1_0:
.incbin "data/maps/rooms/Cave/Stage_2/Room_0/LAYER1_0.tilemap.lz"

.globl CAVE_STAGE2_ROOM0_LAYER1_1
CAVE_STAGE2_ROOM0_LAYER1_1:
.incbin "data/maps/rooms/Cave/Stage_2/Room_0/LAYER1_1.tilemap.lz"

.globl CAVE_STAGE2_ROOM0_LAYER1_2
CAVE_STAGE2_ROOM0_LAYER1_2:
.incbin "data/maps/rooms/Cave/Stage_2/Room_0/LAYER1_2.tilemap.lz"


.globl CAVE_STAGE2_ROOM0_LAYER1_3
CAVE_STAGE2_ROOM0_LAYER1_3:
.incbin "data/maps/rooms/Cave/Stage_2/Room_0/LAYER1_3.tilemap.lz"


.globl CAVE_STAGE2_ROOM1_LAYER1_0
CAVE_STAGE2_ROOM1_LAYER1_0:
.incbin "data/maps/rooms/Cave/Stage_2/Room_1/LAYER1_0.tilemap.lz"

.globl CAVE_STAGE2_ROOM2_LAYER1_0
CAVE_STAGE2_ROOM2_LAYER1_0:
.incbin "data/maps/rooms/Cave/Stage_2/Room_2/LAYER1_0.tilemap.lz"

.globl CAVE_STAGE2_ROOM3_LAYER1_0
CAVE_STAGE2_ROOM3_LAYER1_0:
.incbin "data/maps/rooms/Cave/Stage_2/Room_3/LAYER1_0.tilemap.lz"

.globl CAVE_STAGE2_ROOM3_LAYER1_1
CAVE_STAGE2_ROOM3_LAYER1_1:
.incbin "data/maps/rooms/Cave/Stage_2/Room_3/LAYER1_1.tilemap.lz"

.globl CAVE_STAGE2_ROOM4_LAYER1_0
CAVE_STAGE2_ROOM4_LAYER1_0:
.incbin "data/maps/rooms/Cave/Stage_2/Room_4/LAYER1_0.tilemap.lz"

.globl CAVE_STAGE2_ROOM4_LAYER1_1
CAVE_STAGE2_ROOM4_LAYER1_1:
.incbin "data/maps/rooms/Cave/Stage_2/Room_4/LAYER1_1.tilemap.lz"


.globl CAVE_STAGE4_ROOM0_LAYER1_0
CAVE_STAGE4_ROOM0_LAYER1_0:
.incbin "data/maps/rooms/Cave/Stage_4/Room_0/LAYER1_0.tilemap.lz"


.globl CAVE_STAGE1_ROOM0_LAYER2_0
CAVE_STAGE1_ROOM0_LAYER2_0:
.incbin "data/maps/rooms/Cave/Stage_1/Room_0/LAYER2_0.tilemap.lz"


.globl CAVE_STAGE1_ROOM0_LAYER2_1
CAVE_STAGE1_ROOM0_LAYER2_1:
.incbin "data/maps/rooms/Cave/Stage_1/Room_0/LAYER2_1.tilemap.lz"




.globl CAVE_STAGE1_ROOM0_LAYER2_2
CAVE_STAGE1_ROOM0_LAYER2_2:
.incbin "data/maps/rooms/Cave/Stage_1/Room_0/LAYER2_2.tilemap.lz"


.globl CAVE_STAGE1_ROOM1_LAYER2_0
CAVE_STAGE1_ROOM1_LAYER2_0:
.incbin "data/maps/rooms/Cave/Stage_1/Room_1/LAYER2_0.tilemap.lz"

.globl CAVE_STAGE1_ROOM1_LAYER2_1
CAVE_STAGE1_ROOM1_LAYER2_1:
.incbin "data/maps/rooms/Cave/Stage_1/Room_1/LAYER2_1.tilemap.lz"

.globl CAVE_STAGE1_ROOM2_LAYER2_0
CAVE_STAGE1_ROOM2_LAYER2_0:
.incbin "data/maps/rooms/Cave/Stage_1/Room_2/LAYER2_0.tilemap.lz"

.globl CAVE_STAGE1_ROOM2_LAYER2_1
CAVE_STAGE1_ROOM2_LAYER2_1:
.incbin "data/maps/rooms/Cave/Stage_1/Room_2/LAYER2_1.tilemap.lz"


.globl CAVE_STAGE1_ROOM3_LAYER2_0
CAVE_STAGE1_ROOM3_LAYER2_0:
.incbin "data/maps/rooms/Cave/Stage_1/Room_3/LAYER2_0.tilemap.lz"

.globl CAVE_STAGE2_ROOM0_LAYER2_0
CAVE_STAGE2_ROOM0_LAYER2_0:
.incbin "data/maps/rooms/Cave/Stage_2/Room_0/LAYER2_0.tilemap.lz"

.globl CAVE_STAGE2_ROOM0_LAYER2_1
CAVE_STAGE2_ROOM0_LAYER2_1:
.incbin "data/maps/rooms/Cave/Stage_2/Room_0/LAYER2_1.tilemap.lz"

.globl CAVE_STAGE2_ROOM0_LAYER2_2
CAVE_STAGE2_ROOM0_LAYER2_2:
.incbin "data/maps/rooms/Cave/Stage_2/Room_0/LAYER2_2.tilemap.lz"


.globl CAVE_STAGE2_ROOM0_LAYER2_3
CAVE_STAGE2_ROOM0_LAYER2_3:
.incbin "data/maps/rooms/Cave/Stage_2/Room_0/LAYER2_3.tilemap.lz"


.globl CAVE_STAGE2_ROOM1_LAYER2_0
CAVE_STAGE2_ROOM1_LAYER2_0:
.incbin "data/maps/rooms/Cave/Stage_2/Room_1/LAYER2_0.tilemap.lz"

.globl CAVE_STAGE2_ROOM2_LAYER2_0
CAVE_STAGE2_ROOM2_LAYER2_0:
.incbin "data/maps/rooms/Cave/Stage_2/Room_2/LAYER2_0.tilemap.lz"

.globl CAVE_STAGE2_ROOM3_LAYER2_0
CAVE_STAGE2_ROOM3_LAYER2_0:
.incbin "data/maps/rooms/Cave/Stage_2/Room_3/LAYER2_0.tilemap.lz"


.globl CAVE_STAGE2_ROOM3_LAYER2_1
CAVE_STAGE2_ROOM3_LAYER2_1:
.incbin "data/maps/rooms/Cave/Stage_2/Room_3/LAYER2_1.tilemap.lz"


.globl CAVE_STAGE2_ROOM4_LAYER2_0
CAVE_STAGE2_ROOM4_LAYER2_0:
.incbin "data/maps/rooms/Cave/Stage_2/Room_4/LAYER2_0.tilemap.lz"

.globl CAVE_STAGE2_ROOM4_LAYER2_1
CAVE_STAGE2_ROOM4_LAYER2_1:
.incbin "data/maps/rooms/Cave/Stage_2/Room_4/LAYER2_1.tilemap.lz"

.globl CAVE_STAGE4_ROOM0_LAYER2_0
CAVE_STAGE4_ROOM0_LAYER2_0:
.incbin "data/maps/rooms/Cave/Stage_4/Room_0/LAYER2_0.tilemap.lz"

.globl CAVE_STAGE3_ROOM0_LAYER0_0
CAVE_STAGE3_ROOM0_LAYER0_0:
.incbin "data/maps/rooms/Cave/Stage_3/Room_0/LAYER0_0.tilemap.lz"


.globl CAVE_STAGE3_ROOM0_COLLISION_0
CAVE_STAGE3_ROOM0_COLLISION_0:
.incbin "data/maps/rooms/Cave/Stage_3/Room_0/COLLISION_0.tilemap.lz"


.globl CAVE_STAGE3_ROOM0_LAYER0_1
CAVE_STAGE3_ROOM0_LAYER0_1:
.incbin "data/maps/rooms/Cave/Stage_3/Room_0/LAYER0_1.tilemap.lz"


.globl CAVE_STAGE3_ROOM0_COLLISION_1
CAVE_STAGE3_ROOM0_COLLISION_1:
.incbin "data/maps/rooms/Cave/Stage_3/Room_0/COLLISION_1.tilemap.lz"


.globl CAVE_STAGE3_ROOM0_LAYER0_2
CAVE_STAGE3_ROOM0_LAYER0_2:
.incbin "data/maps/rooms/Cave/Stage_3/Room_0/LAYER0_2.tilemap.lz"


.globl CAVE_STAGE3_ROOM0_COLLISION_2
CAVE_STAGE3_ROOM0_COLLISION_2:
.incbin "data/maps/rooms/Cave/Stage_3/Room_0/COLLISION_2.tilemap.lz"

.globl CAVE_STAGE3_ROOM0_LAYER0_3
CAVE_STAGE3_ROOM0_LAYER0_3:
.incbin "data/maps/rooms/Cave/Stage_3/Room_0/LAYER0_3.tilemap.lz"


.globl CAVE_STAGE3_ROOM0_COLLISION_3
CAVE_STAGE3_ROOM0_COLLISION_3:
.incbin "data/maps/rooms/Cave/Stage_3/Room_0/COLLISION_3.tilemap.lz"

.globl CAVE_STAGE3_ROOM1_LAYER0_0
CAVE_STAGE3_ROOM1_LAYER0_0:
.incbin "data/maps/rooms/Cave/Stage_3/Room_1/LAYER0_0.tilemap.lz"


.globl CAVE_STAGE3_ROOM1_COLLISION_0
CAVE_STAGE3_ROOM1_COLLISION_0:
.incbin "data/maps/rooms/Cave/Stage_3/Room_1/COLLISION_0.tilemap.lz"

.globl CAVE_STAGE3_ROOM2_LAYER0_0
CAVE_STAGE3_ROOM2_LAYER0_0:
.incbin "data/maps/rooms/Cave/Stage_3/Room_2/LAYER0_0.tilemap.lz"


.globl CAVE_STAGE3_ROOM2_COLLISION_0
CAVE_STAGE3_ROOM2_COLLISION_0:
.incbin "data/maps/rooms/Cave/Stage_3/Room_2/COLLISION_0.tilemap.lz"


.globl CAVE_STAGE3_ROOM3_LAYER0_0
CAVE_STAGE3_ROOM3_LAYER0_0:
.incbin "data/maps/rooms/Cave/Stage_3/Room_3/LAYER0_0.tilemap.lz"


.globl CAVE_STAGE3_ROOM3_COLLISION_0
CAVE_STAGE3_ROOM3_COLLISION_0:
.incbin "data/maps/rooms/Cave/Stage_3/Room_3/COLLISION_0.tilemap.lz"


.globl CAVE_STAGE3_ROOM3_LAYER0_1
CAVE_STAGE3_ROOM3_LAYER0_1:
.incbin "data/maps/rooms/Cave/Stage_3/Room_3/LAYER0_1.tilemap.lz"


.globl CAVE_STAGE3_ROOM3_COLLISION_1
CAVE_STAGE3_ROOM3_COLLISION_1:
.incbin "data/maps/rooms/Cave/Stage_3/Room_3/COLLISION_1.tilemap.lz"

.globl CAVE_STAGE3_ROOM4_LAYER0_0
CAVE_STAGE3_ROOM4_LAYER0_0:
.incbin "data/maps/rooms/Cave/Stage_3/Room_4/LAYER0_0.tilemap.lz"


.globl CAVE_STAGE3_ROOM4_COLLISION_0
CAVE_STAGE3_ROOM4_COLLISION_0:
.incbin "data/maps/rooms/Cave/Stage_3/Room_4/COLLISION_0.tilemap.lz"


.globl CAVE_STAGE3_ROOM4_LAYER0_1
CAVE_STAGE3_ROOM4_LAYER0_1:
.incbin "data/maps/rooms/Cave/Stage_3/Room_4/LAYER0_1.tilemap.lz"


.globl CAVE_STAGE3_ROOM4_COLLISION_1
CAVE_STAGE3_ROOM4_COLLISION_1:
.incbin "data/maps/rooms/Cave/Stage_3/Room_4/COLLISION_1.tilemap.lz"


.globl CAVE_STAGE3_ROOM0_LAYER1_0
CAVE_STAGE3_ROOM0_LAYER1_0:
.incbin "data/maps/rooms/Cave/Stage_3/Room_0/LAYER1_0.tilemap.lz"

.globl CAVE_STAGE3_ROOM0_LAYER1_1
CAVE_STAGE3_ROOM0_LAYER1_1:
.incbin "data/maps/rooms/Cave/Stage_3/Room_0/LAYER1_1.tilemap.lz"

.globl CAVE_STAGE3_ROOM0_LAYER1_2
CAVE_STAGE3_ROOM0_LAYER1_2:
.incbin "data/maps/rooms/Cave/Stage_3/Room_0/LAYER1_2.tilemap.lz"

.globl CAVE_STAGE3_ROOM0_LAYER1_3
CAVE_STAGE3_ROOM0_LAYER1_3:
.incbin "data/maps/rooms/Cave/Stage_3/Room_0/LAYER1_3.tilemap.lz"


.globl CAVE_STAGE3_ROOM1_LAYER1_0
CAVE_STAGE3_ROOM1_LAYER1_0:
.incbin "data/maps/rooms/Cave/Stage_3/Room_1/LAYER1_0.tilemap.lz"

.globl CAVE_STAGE3_ROOM2_LAYER1_0
CAVE_STAGE3_ROOM2_LAYER1_0:
.incbin "data/maps/rooms/Cave/Stage_3/Room_2/LAYER1_0.tilemap.lz"

.globl CAVE_STAGE3_ROOM3_LAYER1_0
CAVE_STAGE3_ROOM3_LAYER1_0:
.incbin "data/maps/rooms/Cave/Stage_3/Room_3/LAYER1_0.tilemap.lz"


.globl CAVE_STAGE3_ROOM3_LAYER1_1
CAVE_STAGE3_ROOM3_LAYER1_1:
.incbin "data/maps/rooms/Cave/Stage_3/Room_3/LAYER1_1.tilemap.lz"


.globl CAVE_STAGE3_ROOM4_LAYER1_0
CAVE_STAGE3_ROOM4_LAYER1_0:
.incbin "data/maps/rooms/Cave/Stage_3/Room_4/LAYER1_0.tilemap.lz"

.globl CAVE_STAGE3_ROOM4_LAYER1_1
CAVE_STAGE3_ROOM4_LAYER1_1:
.incbin "data/maps/rooms/Cave/Stage_3/Room_4/LAYER1_1.tilemap.lz"

.globl CAVE_STAGE3_ROOM0_LAYER2_0
CAVE_STAGE3_ROOM0_LAYER2_0:
.incbin "data/maps/rooms/Cave/Stage_3/Room_0/LAYER2_0.tilemap.lz"

.globl CAVE_STAGE3_ROOM0_LAYER2_1
CAVE_STAGE3_ROOM0_LAYER2_1:
.incbin "data/maps/rooms/Cave/Stage_3/Room_0/LAYER2_1.tilemap.lz"

.globl CAVE_STAGE3_ROOM0_LAYER2_2
CAVE_STAGE3_ROOM0_LAYER2_2:
.incbin "data/maps/rooms/Cave/Stage_3/Room_0/LAYER2_2.tilemap.lz"

.globl CAVE_STAGE3_ROOM0_LAYER2_3
CAVE_STAGE3_ROOM0_LAYER2_3:
.incbin "data/maps/rooms/Cave/Stage_3/Room_0/LAYER2_3.tilemap.lz"


.globl CAVE_STAGE3_ROOM1_LAYER2_0
CAVE_STAGE3_ROOM1_LAYER2_0:
.incbin "data/maps/rooms/Cave/Stage_3/Room_1/LAYER2_0.tilemap.lz"

.globl CAVE_STAGE3_ROOM2_LAYER2_0
CAVE_STAGE3_ROOM2_LAYER2_0:
.incbin "data/maps/rooms/Cave/Stage_3/Room_2/LAYER2_0.tilemap.lz"


.globl CAVE_STAGE3_ROOM3_LAYER2_0
CAVE_STAGE3_ROOM3_LAYER2_0:
.incbin "data/maps/rooms/Cave/Stage_3/Room_3/LAYER2_0.tilemap.lz"

.globl CAVE_STAGE3_ROOM3_LAYER2_1
CAVE_STAGE3_ROOM3_LAYER2_1:
.incbin "data/maps/rooms/Cave/Stage_3/Room_3/LAYER2_1.tilemap.lz"

.globl CAVE_STAGE3_ROOM4_LAYER2_0
CAVE_STAGE3_ROOM4_LAYER2_0:
.incbin "data/maps/rooms/Cave/Stage_3/Room_4/LAYER2_0.tilemap.lz"

.globl CAVE_STAGE3_ROOM4_LAYER2_1
CAVE_STAGE3_ROOM4_LAYER2_1:
.incbin "data/maps/rooms/Cave/Stage_3/Room_4/LAYER2_1.tilemap.lz"
.incbin "data/maps/dummy.bin"

.globl unk_80ba068
unk_80ba068:
.long 0x7f960002, 0x7f750002, 0x7f540002, 0x7f330002, 0x7f120002, 0x7af10002, 0x7ad00002, 0x7af10002, 0x7f120002, 0x7f330002, 0x7f540002, 0x7f750002, 0x0


.globl unk_80BA09C
unk_80BA09C:
.long 0x7f330002, 0x7f120002, 0x7af10002, 0x7ad00002, 0x7af10002, 0x7f120002, 0x7f330002, 0x7f540002, 0x7f750002, 0x7f960002, 0x7f750002, 0x7f540002, 0x0


.globl unk_80BA0D0
unk_80BA0D0:
.long 0x7ad00002, 0x7af10002, 0x7f120002, 0x7f330002, 0x7f540002, 0x7f750002, 0x7f960002, 0x7f750002, 0x7f540002, 0x7f330002, 0x7f120002, 0x7af10002, 0x0


.globl off_80BA104
off_80BA104:
.long unk_80ba068, 0x62
.long unk_80BA09C, 0x63
.long unk_80BA0D0, 0x64, 0x0, 0x0


.globl unk_80BA124
unk_80BA124:
.long 0x77350002, 0x73140002, 0x6ed20002, 0x6a900002, 0x666f0002, 0x5e2d0002, 0x55eb0002, 0x51a90002, 0x49880002, 0x41250002, 0x4da90002, 0x5e2d0002, 0x0


.globl unk_80BA158
unk_80BA158:
.long 0x6a900002, 0x666f0002, 0x5e2d0002, 0x55eb0002, 0x51a90002, 0x49880002, 0x41250002, 0x4da90002, 0x5e2d0002, 0x77350002, 0x73140002, 0x6ed20002, 0x0


.globl unk_80BA18C
unk_80BA18C:
.long 0x55eb0002, 0x51a90002, 0x49880002, 0x41250002, 0x4da90002, 0x5e2d0002, 0x77350002, 0x73140002, 0x6ed20002, 0x6a900002, 0x666f0002, 0x5e2d0002, 0x0


.globl unk_80BA1C0
unk_80BA1C0:
.long 0x41250002, 0x4da90002, 0x5e2d0002, 0x77350002, 0x73140002, 0x6ed20002, 0x6a900002, 0x666f0002, 0x5e2d0002, 0x55eb0002, 0x51a90002, 0x49880002, 0x0


.globl off_80BA1F4
off_80BA1F4:
.long unk_80BA124, 0x5b
.long unk_80BA158, 0x5c
.long unk_80BA18C, 0x5d
.long unk_80BA1C0, 0x5e, 0x0, 0x0


.globl unk_80BA21C
unk_80BA21C:
.long 0xe000f, 0x8500009, 0x10920009, 0x18d4000f, 0x10920009, 0x8500009, 0x0


.globl unk_80BA238
unk_80BA238:
.long 0x11000f, 0x8530009, 0x10950009, 0x18d7000f, 0x10950009, 0x8530009, 0x0


.globl unk_80BA254
unk_80BA254:
.long 0x874000f, 0x10b60009, 0x18f80009, 0x213a000f, 0x18f80009, 0x10b60009, 0x0


.globl unk_80BA270
unk_80BA270:
.long 0x14d7000f, 0x1d190009, 0x255b0009, 0x2d9d000f, 0x255b0009, 0x1d190009, 0x0


.globl unk_80BA28C
unk_80BA28C:
.long 0x213a000f, 0x297c0009, 0x31be0009, 0x39ff000f, 0x31be0009, 0x297c0009, 0x0


.globl off_80BA2A8
off_80BA2A8:
.long unk_80BA21C, 0x36
.long unk_80BA238, 0x37
.long unk_80BA254, 0x38
.long unk_80BA270, 0x39
.long unk_80BA28C, 0x3a, 0x0, 0x0


.globl unk_80BA2D8
unk_80BA2D8:
.long 0x7fff000a, 0x7bbd0006, 0x777a0004, 0x73380004, 0x6f150004, 0x6ad30006, 0x624e0014, 0x66900006, 0x6ad30004, 0x6f150004, 0x73380004, 0x777a0006, 0x7fff000a, 0x0


.globl unk_80BA310
unk_80BA310:
.long 0x7735000a, 0x77350006, 0x77350004, 0x77350004, 0x77350004, 0x77350006, 0x77350014, 0x77350006, 0x77350004, 0x77350004, 0x77350004, 0x77350006, 0x7735000a, 0x0


.globl unk_80BA348
unk_80BA348:
.long 0x624e000a, 0x66900006, 0x6ad30004, 0x6f150004, 0x73380004, 0x777a0006, 0x7fff0014, 0x7bbd0006, 0x777a0004, 0x73380004, 0x6f150004, 0x6ad30006, 0x624e000a, 0x0


.globl off_80BA380
off_80BA380:
.long unk_80BA2D8, 0x51
.long unk_80BA310, 0x52
.long unk_80BA348, 0x53, 0x0, 0x0

