HARBOR_STAGE2_ROOM1_MAPDATA:
.byte 0x1@ height
.byte 0x2	@ width
.2byte 0xa	@ unk2
.4byte HARBOR_STAGE2_ROOM1_PALETTE	@ palette
.2byte 0x1	@ tilesCompressed
.2byte 0x190	@ uncompTileCount
.4byte HARBOR_STAGE2_ROOM0_TILESET	@ tileset
.byte 0x20	@ chunk width
.byte 0x20	@ chunk height
.2byte 0x1	@ mapsCompressed
.4byte HARBOR_STAGE2_ROOM1_LAYER0_0	@ layer0 chunk
.4byte HARBOR_STAGE2_ROOM1_LAYER0_1	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte HARBOR_STAGE2_ROOM1_LAYER1_0	@ layer1 chunk
.4byte HARBOR_STAGE2_ROOM1_LAYER1_1	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0x1	@ collisionCompressed
.4byte HARBOR_STAGE2_ROOM1_COLLISION_0	@ collision chunk
.4byte HARBOR_STAGE2_ROOM1_COLLISION_1	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte (levelfunc_activateHarborFlamePal+1)	@ load callbacks
.4byte 0	@ load callbacks
.4byte 0	@ load callbacks
.4byte 0	@ load callbacks
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk


HARBOR_STAGE2_ROOM1_HEADER:
.2byte 0x1a	@ bitfield
.2byte 0x5	@ tilesetStart
.4byte HARBOR_STAGE2_ROOM1_MAPDATA	@ mapdata
.4byte 0	@ scroll
.2byte 0x100	@ height
.2byte 0x200	@ width

