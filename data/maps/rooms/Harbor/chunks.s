
.globl HARBOR_STAGE1_ROOM0_LAYER0_0
HARBOR_STAGE1_ROOM0_LAYER0_0:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_0/LAYER0_0.tilemap.lz"


.globl HARBOR_STAGE1_ROOM0_COLLISION_0
HARBOR_STAGE1_ROOM0_COLLISION_0:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_0/COLLISION_0.tilemap.lz"


.globl HARBOR_STAGE1_ROOM1_TILESET
HARBOR_STAGE1_ROOM1_TILESET:
.incbin "build/gfx/level_tilesets/harbor_stage1.4bpp.lz"


.globl HARBOR_STAGE1_ROOM0_PALETTE
HARBOR_STAGE1_ROOM0_PALETTE:
.incbin "build/gfx/level_tilesets/harbor_stage1.gbapal"


.globl HARBOR_STAGE1_ROOM0_LAYER0_1
HARBOR_STAGE1_ROOM0_LAYER0_1:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_0/LAYER0_1.tilemap.lz"


.globl HARBOR_STAGE1_ROOM0_COLLISION_1
HARBOR_STAGE1_ROOM0_COLLISION_1:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_0/COLLISION_1.tilemap.lz"


.globl HARBOR_STAGE1_ROOM1_LAYER0_1
HARBOR_STAGE1_ROOM1_LAYER0_1:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_1/LAYER0_1.tilemap.lz"


.globl HARBOR_STAGE1_ROOM1_COLLISION_1
HARBOR_STAGE1_ROOM1_COLLISION_1:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_1/COLLISION_1.tilemap.lz"


.globl HARBOR_STAGE1_ROOM1_LAYER0_0
HARBOR_STAGE1_ROOM1_LAYER0_0:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_1/LAYER0_0.tilemap.lz"


.globl HARBOR_STAGE1_ROOM1_COLLISION_0
HARBOR_STAGE1_ROOM1_COLLISION_0:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_1/COLLISION_0.tilemap.lz"

.globl HARBOR_STAGE1_ROOM2_LAYER0_0
HARBOR_STAGE1_ROOM2_LAYER0_0:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_2/LAYER0_0.tilemap.lz"


.globl HARBOR_STAGE1_ROOM2_COLLISION_0
HARBOR_STAGE1_ROOM2_COLLISION_0:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_2/COLLISION_0.tilemap.lz"


.globl HARBOR_STAGE1_ROOM2_LAYER0_1
HARBOR_STAGE1_ROOM2_LAYER0_1:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_2/LAYER0_1.tilemap.lz"


.globl HARBOR_STAGE1_ROOM2_COLLISION_1
HARBOR_STAGE1_ROOM2_COLLISION_1:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_2/COLLISION_1.tilemap.lz"


.globl HARBOR_STAGE1_ROOM3_LAYER0_0
HARBOR_STAGE1_ROOM3_LAYER0_0:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_3/LAYER0_0.tilemap.lz"


.globl HARBOR_STAGE1_ROOM3_COLLISION_0
HARBOR_STAGE1_ROOM3_COLLISION_0:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_3/COLLISION_0.tilemap.lz"

.globl HARBOR_STAGE1_ROOM4_LAYER0_0
HARBOR_STAGE1_ROOM4_LAYER0_0:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_4/LAYER0_0.tilemap.lz"


.globl HARBOR_STAGE1_ROOM4_COLLISION_0
HARBOR_STAGE1_ROOM4_COLLISION_0:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_4/COLLISION_0.tilemap.lz"


.globl HARBOR_STAGE1_ROOM0_LAYER1_0
HARBOR_STAGE1_ROOM0_LAYER1_0:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_0/LAYER1_0.tilemap.lz"

.globl HARBOR_STAGE1_ROOM0_LAYER1_1
HARBOR_STAGE1_ROOM0_LAYER1_1:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_0/LAYER1_1.tilemap.lz"


.globl HARBOR_STAGE1_ROOM1_LAYER1_1
HARBOR_STAGE1_ROOM1_LAYER1_1:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_1/LAYER1_1.tilemap.lz"

.globl HARBOR_STAGE1_ROOM1_LAYER1_0
HARBOR_STAGE1_ROOM1_LAYER1_0:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_1/LAYER1_0.tilemap.lz"


.globl HARBOR_STAGE1_ROOM3_LAYER1_0
HARBOR_STAGE1_ROOM3_LAYER1_0:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_3/LAYER1_0.tilemap.lz"


.globl HARBOR_STAGE1_ROOM4_LAYER1_0
HARBOR_STAGE1_ROOM4_LAYER1_0:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_4/LAYER1_0.tilemap.lz"


.globl HARBOR_STAGE1_ROOM2_LAYER1_0
HARBOR_STAGE1_ROOM2_LAYER1_0:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_2/LAYER1_0.tilemap.lz"



.globl HARBOR_STAGE1_ROOM2_LAYER1_1
HARBOR_STAGE1_ROOM2_LAYER1_1:
.incbin "data/maps/rooms/Harbor/Stage_1/Room_2/LAYER1_1.tilemap.lz"


.globl unk_805ede4
unk_805ede4:
.long 0x11000f, 0xc740009, 0x18d70009, 0x253a000f, 0x18d70009, 0xc740009, 0x0


.globl unk_805ee00
unk_805ee00:
.long 0x874000f, 0x14d70009, 0x213a0009, 0x2d9d000f, 0x213a0009, 0x14d70009, 0x0


.globl unk_805ee1c
unk_805ee1c:
.long 0x1d1b000f, 0x297e0009, 0x35df0009, 0x423f000f, 0x35df0009, 0x297e0009, 0x0


.globl unk_805ee38
unk_805ee38:
.long 0x25df000f, 0x323f0009, 0x3e9f0009, 0x4aff000f, 0x3e9f0009, 0x323f0009, 0x0


.globl unk_805ee54
unk_805ee54:
.long 0x471f000f, 0x537f0009, 0x5fbf0009, 0x6bff000f, 0x5fbf0009, 0x537f0009, 0x0


.globl off_805EE70
off_805EE70:
.long unk_805ede4, 0x56
.long unk_805ee00, 0x57
.long unk_805ee1c, 0x58
.long unk_805ee38, 0x59
.long unk_805ee54, 0x5a, 0x0, 0x0


.globl dword_805EEA0
dword_805EEA0:
.long 0x2, 0x60002, 0xc0002, 0x120002, 0x180002, 0x1e0002, 0xbf0002, 0x15f0002, 0x1ff0002, 0x29f0002, 0x33f0002, 0x29f0002, 0x1ff0002, 0x15f0002, 0xbf0002, 0x1e0002
.long 0x180002, 0x120002, 0xc0002, 0x60002, 0x0


.globl unk_805eef4
unk_805eef4:
.long 0x2, 0x40002, 0x80002, 0xc0002, 0x100002, 0x140002, 0x180002, 0x1c0002, 0x1f0002, 0x9f0002, 0x11f0002, 0x9f0002, 0x1f0002, 0x1c0002, 0x180002, 0x140002
.long 0x100002, 0xc0002, 0x80002, 0x40002, 0x0


.globl off_805EF48
off_805EF48:
.long dword_805EEA0, 0x88
.long unk_805eef4, 0x89, 0x0, 0x0


.globl dword_805EF60
dword_805EF60:
.long 0x29f0001, 0x1ff0001, 0x15f0001, 0xbf0001, 0x1e0001, 0x180001, 0x120001, 0xc0001, 0x60001, 0x1, 0x60001, 0xc0001, 0x120001, 0x180001, 0x1e0001, 0xbf0001
.long 0x15f0001, 0x1ff0001, 0x29f0001, 0x33f0001, 0x29f0001, 0x1ff0001, 0x15f0001, 0xbf0001, 0x1e0001, 0x180001, 0x120001, 0xc0001, 0x60001, 0x1, 0x60001, 0xc0001
.long 0x120001, 0x180001, 0x1e0001, 0xbf0001, 0x15f0001, 0x1ff0001, 0x29f0001, 0x33f0001, 0xffff, 0x0


.globl unk_805f008
unk_805f008:
.long 0x9f0001, 0x1f0001, 0x1c0001, 0x180001, 0x140001, 0x100001, 0xc0001, 0x80001, 0x40001, 0x1, 0x40001, 0x80001, 0xc0001, 0x100001, 0x140001, 0x180001
.long 0x1c0001, 0x1f0001, 0x9f0001, 0x11f0001, 0x9f0001, 0x1f0001, 0x1c0001, 0x180001, 0x140001, 0x100001, 0xc0001, 0x80001, 0x40001, 0x1, 0x40001, 0x80001
.long 0xc0001, 0x100001, 0x140001, 0x180001, 0x1c0001, 0x1f0001, 0x9f0001, 0x11f0001, 0xffff, 0x0


.globl off_805F0B0
off_805F0B0:
.long dword_805EF60, 0x88
.long unk_805f008, 0x89, 0x0, 0x0


.globl HARBOR_STAGE2_ROOM0_LAYER0_0
HARBOR_STAGE2_ROOM0_LAYER0_0:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_0/LAYER0_0.tilemap.lz"


.globl HARBOR_STAGE2_ROOM0_COLLISION_0
HARBOR_STAGE2_ROOM0_COLLISION_0:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_0/COLLISION_0.tilemap.lz"


.globl HARBOR_STAGE2_ROOM0_TILESET
HARBOR_STAGE2_ROOM0_TILESET:
.incbin "build/gfx/level_tilesets/harbor_stage2.4bpp.lz"


.globl HARBOR_STAGE2_ROOM1_PALETTE
HARBOR_STAGE2_ROOM1_PALETTE:
.incbin "build/gfx/level_tilesets/harbor_stage2.gbapal"


.globl HARBOR_STAGE2_ROOM0_LAYER0_1
HARBOR_STAGE2_ROOM0_LAYER0_1:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_0/LAYER0_1.tilemap.lz"


.globl HARBOR_STAGE2_ROOM0_COLLISION_1
HARBOR_STAGE2_ROOM0_COLLISION_1:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_0/COLLISION_1.tilemap.lz"


.globl HARBOR_STAGE2_ROOM0_LAYER0_2
HARBOR_STAGE2_ROOM0_LAYER0_2:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_0/LAYER0_2.tilemap.lz"


.globl HARBOR_STAGE2_ROOM0_COLLISION_2
HARBOR_STAGE2_ROOM0_COLLISION_2:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_0/COLLISION_2.tilemap.lz"


.globl HARBOR_STAGE2_ROOM0_LAYER0_3
HARBOR_STAGE2_ROOM0_LAYER0_3:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_0/LAYER0_3.tilemap.lz"


.globl HARBOR_STAGE2_ROOM0_COLLISION_3
HARBOR_STAGE2_ROOM0_COLLISION_3:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_0/COLLISION_3.tilemap.lz"


.globl HARBOR_STAGE2_ROOM1_LAYER0_1
HARBOR_STAGE2_ROOM1_LAYER0_1:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_1/LAYER0_1.tilemap.lz"


.globl HARBOR_STAGE2_ROOM1_COLLISION_1
HARBOR_STAGE2_ROOM1_COLLISION_1:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_1/COLLISION_1.tilemap.lz"


.globl HARBOR_STAGE2_ROOM1_LAYER0_0
HARBOR_STAGE2_ROOM1_LAYER0_0:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_1/LAYER0_0.tilemap.lz"


.globl HARBOR_STAGE2_ROOM1_COLLISION_0
HARBOR_STAGE2_ROOM1_COLLISION_0:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_1/COLLISION_0.tilemap.lz"


.globl HARBOR_STAGE2_ROOM2_LAYER0_1
HARBOR_STAGE2_ROOM2_LAYER0_1:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_2/LAYER0_1.tilemap.lz"


.globl HARBOR_STAGE2_ROOM2_COLLISION_1
HARBOR_STAGE2_ROOM2_COLLISION_1:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_2/COLLISION_1.tilemap.lz"


.globl HARBOR_STAGE2_ROOM2_LAYER0_0
HARBOR_STAGE2_ROOM2_LAYER0_0:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_2/LAYER0_0.tilemap.lz"


.globl HARBOR_STAGE2_ROOM2_COLLISION_0
HARBOR_STAGE2_ROOM2_COLLISION_0:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_2/COLLISION_0.tilemap.lz"


.globl HARBOR_STAGE2_ROOM3_LAYER0_0
HARBOR_STAGE2_ROOM3_LAYER0_0:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_3/LAYER0_0.tilemap.lz"


.globl HARBOR_STAGE2_ROOM3_COLLISION_0
HARBOR_STAGE2_ROOM3_COLLISION_0:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_3/COLLISION_0.tilemap.lz"


.globl HARBOR_STAGE2_ROOM0_LAYER1_0
HARBOR_STAGE2_ROOM0_LAYER1_0:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_0/LAYER1_0.tilemap.lz"

.globl HARBOR_STAGE2_ROOM0_LAYER1_1
HARBOR_STAGE2_ROOM0_LAYER1_1:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_0/LAYER1_1.tilemap.lz"

.globl HARBOR_STAGE2_ROOM0_LAYER1_2
HARBOR_STAGE2_ROOM0_LAYER1_2:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_0/LAYER1_2.tilemap.lz"

.globl HARBOR_STAGE2_ROOM0_LAYER1_3
HARBOR_STAGE2_ROOM0_LAYER1_3:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_0/LAYER1_3.tilemap.lz"

.globl HARBOR_STAGE2_ROOM1_LAYER1_1
HARBOR_STAGE2_ROOM1_LAYER1_1:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_1/LAYER1_1.tilemap.lz"

.globl HARBOR_STAGE2_ROOM1_LAYER1_0
HARBOR_STAGE2_ROOM1_LAYER1_0:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_1/LAYER1_0.tilemap.lz"

.globl HARBOR_STAGE2_ROOM2_LAYER1_1
HARBOR_STAGE2_ROOM2_LAYER1_1:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_2/LAYER1_1.tilemap.lz"

.globl HARBOR_STAGE2_ROOM2_LAYER1_0
HARBOR_STAGE2_ROOM2_LAYER1_0:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_2/LAYER1_0.tilemap.lz"

.globl HARBOR_STAGE2_ROOM3_LAYER1_0
HARBOR_STAGE2_ROOM3_LAYER1_0:
.incbin "data/maps/rooms/Harbor/Stage_2/Room_3/LAYER1_0.tilemap.lz"


.globl dword_8068DD4
dword_8068DD4:
.incbin "data/maps/dummy.bin"

.globl unk_8068e54
unk_8068e54:
.long 0x11000f, 0xc740009, 0x18d70009, 0x253a000f, 0x18d70009, 0xc740009, 0x0


.globl unk_8068e70
unk_8068e70:
.long 0x874000f, 0x14d70009, 0x213a0009, 0x2d9d000f, 0x213a0009, 0x14d70009, 0x0


.globl unk_8068e8c
unk_8068e8c:
.long 0x1d1b000f, 0x297e0009, 0x35df0009, 0x423f000f, 0x35df0009, 0x297e0009, 0x0


.globl unk_8068ea8
unk_8068ea8:
.long 0x25df000f, 0x323f0009, 0x3e9f0009, 0x4aff000f, 0x3e9f0009, 0x323f0009, 0x0


.globl unk_8068ec4
unk_8068ec4:
.long 0x471f000f, 0x537f0009, 0x5fbf0009, 0x6bff000f, 0x5fbf0009, 0x537f0009, 0x0


.globl off_8068EE0
off_8068EE0:
.long unk_8068e54, 0x56
.long unk_8068e70, 0x57
.long unk_8068e8c, 0x58
.long unk_8068ea8, 0x59
.long unk_8068ec4, 0x5a, 0x0, 0x0


.globl dword_8068F10
dword_8068F10:
.long 0x2, 0x60002, 0xc0002, 0x120002, 0x180002, 0x1e0002, 0xbf0002, 0x15f0002, 0x1ff0002, 0x29f0002, 0x33f0002, 0x29f0002, 0x1ff0002, 0x15f0002, 0xbf0002, 0x1e0002
.long 0x180002, 0x120002, 0xc0002, 0x60002, 0x0


.globl unk_8068f64
unk_8068f64:
.long 0x2, 0x40002, 0x80002, 0xc0002, 0x100002, 0x140002, 0x180002, 0x1c0002, 0x1f0002, 0x9f0002, 0x11f0002, 0x9f0002, 0x1f0002, 0x1c0002, 0x180002, 0x140002
.long 0x100002, 0xc0002, 0x80002, 0x40002, 0x0


.globl off_8068FB8
off_8068FB8:
.long dword_8068F10, 0xa8
.long unk_8068f64, 0xa9, 0x0, 0x0


.globl dword_8068FD0
dword_8068FD0:
.long 0x29f0001, 0x1ff0001, 0x15f0001, 0xbf0001, 0x1e0001, 0x180001, 0x120001, 0xc0001, 0x60001, 0x1, 0x60001, 0xc0001, 0x120001, 0x180001, 0x1e0001, 0xbf0001
.long 0x15f0001, 0x1ff0001, 0x29f0001, 0x33f0001, 0x29f0001, 0x1ff0001, 0x15f0001, 0xbf0001, 0x1e0001, 0x180001, 0x120001, 0xc0001, 0x60001, 0x1, 0x60001, 0xc0001
.long 0x120001, 0x180001, 0x1e0001, 0xbf0001, 0x15f0001, 0x1ff0001, 0x29f0001, 0x33f0001, 0xffff, 0x0


.globl unk_8069078
unk_8069078:
.long 0x9f0001, 0x1f0001, 0x1c0001, 0x180001, 0x140001, 0x100001, 0xc0001, 0x80001, 0x40001, 0x1, 0x40001, 0x80001, 0xc0001, 0x100001, 0x140001, 0x180001
.long 0x1c0001, 0x1f0001, 0x9f0001, 0x11f0001, 0x9f0001, 0x1f0001, 0x1c0001, 0x180001, 0x140001, 0x100001, 0xc0001, 0x80001, 0x40001, 0x1, 0x40001, 0x80001
.long 0xc0001, 0x100001, 0x140001, 0x180001, 0x1c0001, 0x1f0001, 0x9f0001, 0x11f0001, 0xffff, 0x0


.globl off_8069120
off_8069120:
.long dword_8068FD0, 0xa8
.long unk_8069078, 0xa9, 0x0, 0x0


.globl HARBOR_STAGE3_ROOM0_LAYER0_2
HARBOR_STAGE3_ROOM0_LAYER0_2:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_0/LAYER0_2.tilemap.lz"


.globl HARBOR_STAGE3_ROOM0_COLLISION_2
HARBOR_STAGE3_ROOM0_COLLISION_2:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_0/COLLISION_2.tilemap.lz"


.globl HARBOR_STAGE3_ROOM3_TILESET
HARBOR_STAGE3_ROOM3_TILESET:
.incbin "build/gfx/level_tilesets/harbor_stage3.4bpp.lz"


.globl HARBOR_STAGE3_ROOM2_PALETTE
HARBOR_STAGE3_ROOM2_PALETTE:
.incbin "build/gfx/level_tilesets/harbor_stage3.gbapal"


.globl HARBOR_STAGE3_ROOM0_LAYER0_3
HARBOR_STAGE3_ROOM0_LAYER0_3:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_0/LAYER0_3.tilemap.lz"


.globl HARBOR_STAGE3_ROOM0_COLLISION_3
HARBOR_STAGE3_ROOM0_COLLISION_3:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_0/COLLISION_3.tilemap.lz"


.globl HARBOR_STAGE3_ROOM0_LAYER0_0
HARBOR_STAGE3_ROOM0_LAYER0_0:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_0/LAYER0_0.tilemap.lz"


.globl HARBOR_STAGE3_ROOM0_COLLISION_0
HARBOR_STAGE3_ROOM0_COLLISION_0:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_0/COLLISION_0.tilemap.lz"


.globl HARBOR_STAGE3_ROOM0_LAYER0_1
HARBOR_STAGE3_ROOM0_LAYER0_1:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_0/LAYER0_1.tilemap.lz"


.globl HARBOR_STAGE3_ROOM0_COLLISION_1
HARBOR_STAGE3_ROOM0_COLLISION_1:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_0/COLLISION_1.tilemap.lz"


.globl HARBOR_STAGE3_ROOM1_LAYER0_0
HARBOR_STAGE3_ROOM1_LAYER0_0:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_1/LAYER0_0.tilemap.lz"


.globl HARBOR_STAGE3_ROOM1_COLLISION_0
HARBOR_STAGE3_ROOM1_COLLISION_0:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_1/COLLISION_0.tilemap.lz"


.globl HARBOR_STAGE3_ROOM2_LAYER0_0
HARBOR_STAGE3_ROOM2_LAYER0_0:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_2/LAYER0_0.tilemap.lz"


.globl HARBOR_STAGE3_ROOM2_COLLISION_0
HARBOR_STAGE3_ROOM2_COLLISION_0:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_2/COLLISION_0.tilemap.lz"


.globl HARBOR_STAGE3_ROOM3_LAYER0_1
HARBOR_STAGE3_ROOM3_LAYER0_1:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_3/LAYER0_1.tilemap.lz"


.globl HARBOR_STAGE3_ROOM3_COLLISION_1
HARBOR_STAGE3_ROOM3_COLLISION_1:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_3/COLLISION_1.tilemap.lz"


.globl HARBOR_STAGE3_ROOM3_LAYER0_0
HARBOR_STAGE3_ROOM3_LAYER0_0:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_3/LAYER0_0.tilemap.lz"


.globl HARBOR_STAGE3_ROOM3_COLLISION_0
HARBOR_STAGE3_ROOM3_COLLISION_0:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_3/COLLISION_0.tilemap.lz"


.globl HARBOR_STAGE3_ROOM4_LAYER0_0
HARBOR_STAGE3_ROOM4_LAYER0_0:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_4/LAYER0_0.tilemap.lz"


.globl HARBOR_STAGE3_ROOM4_COLLISION_0
HARBOR_STAGE3_ROOM4_COLLISION_0:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_4/COLLISION_0.tilemap.lz"


.globl HARBOR_STAGE3_ROOM4_LAYER0_1
HARBOR_STAGE3_ROOM4_LAYER0_1:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_4/LAYER0_1.tilemap.lz"


.globl HARBOR_STAGE3_ROOM4_COLLISION_1
HARBOR_STAGE3_ROOM4_COLLISION_1:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_4/COLLISION_1.tilemap.lz"


.globl HARBOR_STAGE3_ROOM0_LAYER1_0
HARBOR_STAGE3_ROOM0_LAYER1_0:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_0/LAYER1_0.tilemap.lz"


.globl dword_807300C
dword_807300C:
.incbin "data/maps/dummy.bin"


.globl HARBOR_STAGE3_ROOM0_LAYER1_1
HARBOR_STAGE3_ROOM0_LAYER1_1:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_0/LAYER1_1.tilemap.lz"


.globl dword_80732C4
dword_80732C4:
.incbin "data/maps/dummy.bin"

.globl HARBOR_STAGE3_ROOM1_LAYER1_0
HARBOR_STAGE3_ROOM1_LAYER1_0:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_1/LAYER1_0.tilemap.lz"


.globl dword_807357C
dword_807357C:
.incbin "data/maps/dummy.bin"

.globl HARBOR_STAGE3_ROOM3_LAYER1_1
HARBOR_STAGE3_ROOM3_LAYER1_1:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_3/LAYER1_1.tilemap.lz"


.globl dword_8073834
dword_8073834:
.incbin "data/maps/dummy.bin"


.globl HARBOR_STAGE3_ROOM4_LAYER1_0
HARBOR_STAGE3_ROOM4_LAYER1_0:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_4/LAYER1_0.tilemap.lz"


.globl dword_8073AD4
dword_8073AD4:
.incbin "data/maps/dummy.bin"


.globl HARBOR_STAGE3_ROOM4_LAYER1_1
HARBOR_STAGE3_ROOM4_LAYER1_1:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_4/LAYER1_1.tilemap.lz"


.globl dword_8073D74
dword_8073D74:
.incbin "data/maps/dummy.bin"


.globl HARBOR_STAGE3_ROOM0_LAYER1_2
HARBOR_STAGE3_ROOM0_LAYER1_2:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_0/LAYER1_2.tilemap.lz"


.globl dword_8073FFC
dword_8073FFC:
.incbin "data/maps/dummy.bin"


.globl HARBOR_STAGE3_ROOM0_LAYER1_3
HARBOR_STAGE3_ROOM0_LAYER1_3:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_0/LAYER1_3.tilemap.lz"


.globl dword_8074284
dword_8074284:
.incbin "data/maps/dummy.bin"


.globl HARBOR_STAGE3_ROOM2_LAYER1_0
HARBOR_STAGE3_ROOM2_LAYER1_0:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_2/LAYER1_0.tilemap.lz"


.globl dword_8074524
dword_8074524:
.incbin "data/maps/dummy.bin"


.globl HARBOR_STAGE3_ROOM3_LAYER1_0
HARBOR_STAGE3_ROOM3_LAYER1_0:
.incbin "data/maps/rooms/Harbor/Stage_3/Room_3/LAYER1_0.tilemap.lz"


.globl dword_80747BC
dword_80747BC:
.incbin "data/maps/dummy.bin"


.globl unk_807483c
unk_807483c:
.long 0x11000f, 0xc740009, 0x18d70009, 0x253a000f, 0x18d70009, 0xc740009, 0x0


.globl unk_8074858
unk_8074858:
.long 0x874000f, 0x14d70009, 0x213a0009, 0x2d9d000f, 0x213a0009, 0x14d70009, 0x0


.globl unk_8074874
unk_8074874:
.long 0x1d1b000f, 0x297e0009, 0x35df0009, 0x423f000f, 0x35df0009, 0x297e0009, 0x0


.globl unk_8074890
unk_8074890:
.long 0x25df000f, 0x323f0009, 0x3e9f0009, 0x4aff000f, 0x3e9f0009, 0x323f0009, 0x0


.globl unk_80748ac
unk_80748ac:
.long 0x471f000f, 0x537f0009, 0x5fbf0009, 0x6bff000f, 0x5fbf0009, 0x537f0009, 0x0


.globl off_80748C8
off_80748C8:
.long unk_807483c, 0x56
.long unk_8074858, 0x57
.long unk_8074874, 0x58
.long unk_8074890, 0x59
.long unk_80748ac, 0x5a, 0x0, 0x0


.globl dword_80748F8
dword_80748F8:
.long 0x2, 0x60002, 0xc0002, 0x120002, 0x180002, 0x1e0002, 0xbf0002, 0x15f0002, 0x1ff0002, 0x29f0002, 0x33f0002, 0x29f0002, 0x1ff0002, 0x15f0002, 0xbf0002, 0x1e0002
.long 0x180002, 0x120002, 0xc0002, 0x60002, 0x0


.globl unk_807494c
unk_807494c:
.long 0x2, 0x40002, 0x80002, 0xc0002, 0x100002, 0x140002, 0x180002, 0x1c0002, 0x1f0002, 0x9f0002, 0x11f0002, 0x9f0002, 0x1f0002, 0x1c0002, 0x180002, 0x140002
.long 0x100002, 0xc0002, 0x80002, 0x40002, 0x0


.globl off_80749A0
off_80749A0:
.long dword_80748F8, 0xa8
.long unk_807494c, 0xa9, 0x0, 0x0


.globl dword_80749B8
dword_80749B8:
.long 0x29f0001, 0x1ff0001, 0x15f0001, 0xbf0001, 0x1e0001, 0x180001, 0x120001, 0xc0001, 0x60001, 0x1, 0x60001, 0xc0001, 0x120001, 0x180001, 0x1e0001, 0xbf0001
.long 0x15f0001, 0x1ff0001, 0x29f0001, 0x33f0001, 0x29f0001, 0x1ff0001, 0x15f0001, 0xbf0001, 0x1e0001, 0x180001, 0x120001, 0xc0001, 0x60001, 0x1, 0x60001, 0xc0001
.long 0x120001, 0x180001, 0x1e0001, 0xbf0001, 0x15f0001, 0x1ff0001, 0x29f0001, 0x33f0001, 0xffff, 0x0


.globl unk_8074a60
unk_8074a60:
.long 0x9f0001, 0x1f0001, 0x1c0001, 0x180001, 0x140001, 0x100001, 0xc0001, 0x80001, 0x40001, 0x1, 0x40001, 0x80001, 0xc0001, 0x100001, 0x140001, 0x180001
.long 0x1c0001, 0x1f0001, 0x9f0001, 0x11f0001, 0x9f0001, 0x1f0001, 0x1c0001, 0x180001, 0x140001, 0x100001, 0xc0001, 0x80001, 0x40001, 0x1, 0x40001, 0x80001
.long 0xc0001, 0x100001, 0x140001, 0x180001, 0x1c0001, 0x1f0001, 0x9f0001, 0x11f0001, 0xffff, 0x0


.globl off_8074B08
off_8074B08:
.long dword_80749B8, 0xa8
.long unk_8074a60, 0xa9, 0x0, 0x0


.globl HARBOR_STAGE4_ROOM0_LAYER0_2
HARBOR_STAGE4_ROOM0_LAYER0_2:
.incbin "data/maps/rooms/Harbor/Stage_4/Room_0/LAYER0_2.tilemap.lz"


.globl HARBOR_STAGE4_ROOM0_COLLISION_2
HARBOR_STAGE4_ROOM0_COLLISION_2:
.incbin "data/maps/rooms/Harbor/Stage_4/Room_0/COLLISION_2.tilemap.lz"


.globl HARBOR_STAGE4_ROOM0_TILESET
HARBOR_STAGE4_ROOM0_TILESET:
.incbin "build/gfx/level_tilesets/harbor_stage4.4bpp.lz"


.globl HARBOR_STAGE4_ROOM0_PALETTE
HARBOR_STAGE4_ROOM0_PALETTE:
.incbin "build/gfx/level_tilesets/harbor_stage4.gbapal"


.globl HARBOR_STAGE4_ROOM0_LAYER0_3
HARBOR_STAGE4_ROOM0_LAYER0_3:
.incbin "data/maps/rooms/Harbor/Stage_4/Room_0/LAYER0_3.tilemap.lz"


.globl HARBOR_STAGE4_ROOM0_COLLISION_3
HARBOR_STAGE4_ROOM0_COLLISION_3:
.incbin "data/maps/rooms/Harbor/Stage_4/Room_0/COLLISION_3.tilemap.lz"


.globl HARBOR_STAGE4_ROOM0_LAYER0_0
HARBOR_STAGE4_ROOM0_LAYER0_0:
.incbin "data/maps/rooms/Harbor/Stage_4/Room_0/LAYER0_0.tilemap.lz"


.globl HARBOR_STAGE4_ROOM0_COLLISION_0
HARBOR_STAGE4_ROOM0_COLLISION_0:
.incbin "data/maps/rooms/Harbor/Stage_4/Room_0/COLLISION_0.tilemap.lz"

.globl HARBOR_STAGE4_ROOM0_LAYER0_1
HARBOR_STAGE4_ROOM0_LAYER0_1:
.incbin "data/maps/rooms/Harbor/Stage_4/Room_0/LAYER0_1.tilemap.lz"


.globl HARBOR_STAGE4_ROOM0_COLLISION_1
HARBOR_STAGE4_ROOM0_COLLISION_1:
.incbin "data/maps/rooms/Harbor/Stage_4/Room_0/COLLISION_1.tilemap.lz"


.globl HARBOR_STAGE4_ROOM0_LAYER1_2
HARBOR_STAGE4_ROOM0_LAYER1_2:
.incbin "data/maps/rooms/Harbor/Stage_4/Room_0/LAYER1_3.tilemap.lz"


.globl dword_8079560
dword_8079560:
.incbin "data/maps/dummy.bin"

.globl HARBOR_STAGE4_ROOM0_LAYER1_0
HARBOR_STAGE4_ROOM0_LAYER1_0:
.incbin "data/maps/rooms/Harbor/Stage_4/Room_0/LAYER1_1.tilemap.lz"


.globl dword_80797D8
dword_80797D8:
.incbin "data/maps/dummy.bin"

.globl unk_8079858
unk_8079858:
.long 0x7fff0002, 0x7bbc0002, 0x73590002, 0x6f160002, 0x6ad30002, 0x66910002, 0x5e2e0002, 0x55a80002, 0x0


.globl unk_807987c
unk_807987c:
.long 0x55a80002, 0x5e2e0002, 0x66910002, 0x6ad30002, 0x6f160002, 0x73590002, 0x7bbc0002, 0x7fff0002, 0x0


.globl unk_80798a0
unk_80798a0:
.long 0x0, 0x0


.globl unk_80798a8
unk_80798a8:
.long 0x0, 0x0


.globl off_80798B0
off_80798B0:
.long unk_8079858, 0x71
.long unk_807987c, 0x72
.long unk_80798a0, 0x73
.long unk_80798a8, 0x74, 0x0, 0x0
