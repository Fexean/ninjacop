
.globl AIRPORT_STAGE1_ROOM0_PARALLAX_CHUNK_0
AIRPORT_STAGE1_ROOM0_PARALLAX_CHUNK_0:
.incbin "data/maps/rooms/Airport/Stage_1/Room_0/PARALLAX_CHUNK_0.bin"


.globl dword_8079E68
dword_8079E68:
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE1_ROOM1_TILESET
AIRPORT_STAGE1_ROOM1_TILESET:
.incbin "build/gfx/level_tilesets/airport_stage1.4bpp.lz"


.globl AIRPORT_STAGE1_ROOM1_PALETTE
AIRPORT_STAGE1_ROOM1_PALETTE:
.incbin "build/gfx/level_tilesets/airport_stage1.gbapal"


.globl AIRPORT_STAGE1_ROOM0_LAYER0_3
AIRPORT_STAGE1_ROOM0_LAYER0_3:
Airport_Stage_1_Room_0_LAYER0_3:
.incbin "data/maps/rooms/Airport/Stage_1/Room_0/LAYER0_3.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM0_COLLISION_3
AIRPORT_STAGE1_ROOM0_COLLISION_3:
.incbin "data/maps/rooms/Airport/Stage_1/Room_0/COLLISION_3.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM0_LAYER0_4
AIRPORT_STAGE1_ROOM0_LAYER0_4:
.incbin "data/maps/rooms/Airport/Stage_1/Room_0/LAYER0_4.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM0_COLLISION_4
AIRPORT_STAGE1_ROOM0_COLLISION_4:
.incbin "data/maps/rooms/Airport/Stage_1/Room_0/COLLISION_4.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM0_LAYER0_5
AIRPORT_STAGE1_ROOM0_LAYER0_5:
.incbin "data/maps/rooms/Airport/Stage_1/Room_0/LAYER0_5.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM0_COLLISION_5
AIRPORT_STAGE1_ROOM0_COLLISION_5:
.incbin "data/maps/rooms/Airport/Stage_1/Room_0/COLLISION_5.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM0_LAYER0_0
AIRPORT_STAGE1_ROOM0_LAYER0_0:
.incbin "data/maps/rooms/Airport/Stage_1/Room_0/LAYER0_0.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM0_COLLISION_0
AIRPORT_STAGE1_ROOM0_COLLISION_0:
.incbin "data/maps/rooms/Airport/Stage_1/Room_0/COLLISION_0.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM0_LAYER0_1
AIRPORT_STAGE1_ROOM0_LAYER0_1:
.incbin "data/maps/rooms/Airport/Stage_1/Room_0/LAYER0_1.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM0_COLLISION_1
AIRPORT_STAGE1_ROOM0_COLLISION_1:
.incbin "data/maps/rooms/Airport/Stage_1/Room_0/COLLISION_1.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM0_LAYER0_2
AIRPORT_STAGE1_ROOM0_LAYER0_2:
.incbin "data/maps/rooms/Airport/Stage_1/Room_0/LAYER0_2.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM0_COLLISION_2
AIRPORT_STAGE1_ROOM0_COLLISION_2:
.incbin "data/maps/rooms/Airport/Stage_1/Room_0/COLLISION_2.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM0_LAYER1_3
AIRPORT_STAGE1_ROOM0_LAYER1_3:
.incbin "data/maps/rooms/Airport/Stage_1/Room_0/LAYER1_3.tilemap.lz"


.globl dword_807FA50
dword_807FA50:
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE1_ROOM0_LAYER1_4
AIRPORT_STAGE1_ROOM0_LAYER1_4:
.incbin "data/maps/rooms/Airport/Stage_1/Room_0/LAYER1_4.tilemap.lz"


.globl dword_807FD58
dword_807FD58:
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE1_ROOM0_LAYER1_5
AIRPORT_STAGE1_ROOM0_LAYER1_5:
.incbin "data/maps/rooms/Airport/Stage_1/Room_0/LAYER1_5.tilemap.lz"


.globl dword_8080088
dword_8080088:
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE1_ROOM0_LAYER1_0
AIRPORT_STAGE1_ROOM0_LAYER1_0:
.incbin "data/maps/rooms/Airport/Stage_1/Room_0/LAYER1_0.tilemap.lz"


.globl dword_8080330
dword_8080330:
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE1_ROOM0_LAYER1_1
AIRPORT_STAGE1_ROOM0_LAYER1_1:
.incbin "data/maps/rooms/Airport/Stage_1/Room_0/LAYER1_1.tilemap.lz"


.globl dword_80805A8
dword_80805A8:
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE1_ROOM0_LAYER1_2
AIRPORT_STAGE1_ROOM0_LAYER1_2:
.incbin "data/maps/rooms/Airport/Stage_1/Room_0/LAYER1_2.tilemap.lz"
.incbin "data/maps/dummy.bin"


.globl AIRPORT_STAGE1_ROOM1_LAYER0_1
AIRPORT_STAGE1_ROOM1_LAYER0_1:
.incbin "data/maps/rooms/Airport/Stage_1/Room_1/LAYER0_1.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM1_COLLISION_1
AIRPORT_STAGE1_ROOM1_COLLISION_1:
.incbin "data/maps/rooms/Airport/Stage_1/Room_1/COLLISION_1.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM1_LAYER0_0
AIRPORT_STAGE1_ROOM1_LAYER0_0:
.incbin "data/maps/rooms/Airport/Stage_1/Room_1/LAYER0_0.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM1_COLLISION_0
AIRPORT_STAGE1_ROOM1_COLLISION_0:
.incbin "data/maps/rooms/Airport/Stage_1/Room_1/COLLISION_0.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM1_LAYER1_1
AIRPORT_STAGE1_ROOM1_LAYER1_1:
.incbin "data/maps/rooms/Airport/Stage_1/Room_1/LAYER1_1.tilemap.lz"


.globl dword_80819A8
dword_80819A8:
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE1_ROOM1_LAYER1_0
AIRPORT_STAGE1_ROOM1_LAYER1_0:
.incbin "data/maps/rooms/Airport/Stage_1/Room_1/LAYER1_0.tilemap.lz"

.globl AIRPORT_STAGE1_ROOM2_LAYER0_0
AIRPORT_STAGE1_ROOM2_LAYER0_0:
.incbin "data/maps/rooms/Airport/Stage_1/Room_2/LAYER0_0.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM2_COLLISION_0
AIRPORT_STAGE1_ROOM2_COLLISION_0:
.incbin "data/maps/rooms/Airport/Stage_1/Room_2/COLLISION_0.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM2_LAYER1_0
AIRPORT_STAGE1_ROOM2_LAYER1_0:
.incbin "data/maps/rooms/Airport/Stage_1/Room_2/LAYER1_0.tilemap.lz"
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE1_ROOM3_LAYER0_0
AIRPORT_STAGE1_ROOM3_LAYER0_0:
.incbin "data/maps/rooms/Airport/Stage_1/Room_3/LAYER0_0.tilemap.lz"

.globl AIRPORT_STAGE1_ROOM3_COLLISION_0
AIRPORT_STAGE1_ROOM3_COLLISION_0:
.incbin "data/maps/rooms/Airport/Stage_1/Room_3/COLLISION_0.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM3_PARALLAX_CHUNK_0
AIRPORT_STAGE1_ROOM3_PARALLAX_CHUNK_0:
.incbin "data/maps/rooms/Airport/Stage_1/Room_3/PARALLAX_CHUNK_0.bin"


.globl dword_8082F60
dword_8082F60:
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE1_ROOM3_LAYER1_0
AIRPORT_STAGE1_ROOM3_LAYER1_0:
.incbin "data/maps/rooms/Airport/Stage_1/Room_3/LAYER1_0.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM3_LAYER0_1
AIRPORT_STAGE1_ROOM3_LAYER0_1:
.incbin "data/maps/rooms/Airport/Stage_1/Room_3/LAYER0_1.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM3_COLLISION_1
AIRPORT_STAGE1_ROOM3_COLLISION_1:
.incbin "data/maps/rooms/Airport/Stage_1/Room_3/COLLISION_1.tilemap.lz"


.globl AIRPORT_STAGE1_ROOM3_LAYER1_1
AIRPORT_STAGE1_ROOM3_LAYER1_1:
.incbin "data/maps/rooms/Airport/Stage_1/Room_3/LAYER1_1.tilemap.lz"
.incbin "data/maps/dummy.bin"

.globl unk_8083b08
unk_8083b08:
.long 0x11000f, 0xc740009, 0x14b60009, 0x1cf8000f, 0x14b60009, 0xc740009, 0x0


.globl unk_8083b24
unk_8083b24:
.long 0x874000f, 0x14d70009, 0x1d190009, 0x255b000f, 0x1d190009, 0x14d70009, 0x0


.globl unk_8083b40
unk_8083b40:
.long 0x14d7000f, 0x21390009, 0x297b0009, 0x31bd000f, 0x297b0009, 0x21390009, 0x0


.globl unk_8083b5c
unk_8083b5c:
.long 0x213a000f, 0x2d9b0009, 0x35dd0009, 0x3e1f000f, 0x35dd0009, 0x2d9b0009, 0x0


.globl unk_8083b78
unk_8083b78:
.long 0x2d9d000f, 0x39fe0009, 0x423e0009, 0x4a7f000f, 0x423e0009, 0x39fe0009, 0x0


.globl off_8083B94
off_8083B94:
.long unk_8083b08, 0x86
.long unk_8083b24, 0x87
.long unk_8083b40, 0x88
.long unk_8083b5c, 0x89
.long unk_8083b78, 0x8a, 0x0, 0x0


.globl AIRPORT_STAGE2_ROOM0_PARALLAX_CHUNK_0
AIRPORT_STAGE2_ROOM0_PARALLAX_CHUNK_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_0/PARALLAX_CHUNK_0.bin"


.globl AIRPORT_STAGE2_ROOM0_TILESET
AIRPORT_STAGE2_ROOM0_TILESET:
.incbin "build/gfx/level_tilesets/airport_stage2.4bpp.lz"


.globl AIRPORT_STAGE2_ROOM0_PALETTE
AIRPORT_STAGE2_ROOM0_PALETTE:
.incbin "build/gfx/level_tilesets/airport_stage2.gbapal"


.globl AIRPORT_STAGE2_ROOM0_LAYER0_0
AIRPORT_STAGE2_ROOM0_LAYER0_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_0/LAYER0_0.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM0_COLLISION_0
AIRPORT_STAGE2_ROOM0_COLLISION_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_0/COLLISION_0.tilemap.lz"

.globl AIRPORT_STAGE2_ROOM0_LAYER1_0
AIRPORT_STAGE2_ROOM0_LAYER1_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_0/LAYER1_0.tilemap.lz"
.incbin "data/maps/dummy.bin"


.globl AIRPORT_STAGE2_ROOM0_LAYER0_1
AIRPORT_STAGE2_ROOM0_LAYER0_1:
.incbin "data/maps/rooms/Airport/Stage_2/Room_0/LAYER0_1.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM0_COLLISION_1
AIRPORT_STAGE2_ROOM0_COLLISION_1:
.incbin "data/maps/rooms/Airport/Stage_2/Room_0/COLLISION_1.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM0_LAYER1_1
AIRPORT_STAGE2_ROOM0_LAYER1_1:
.incbin "data/maps/rooms/Airport/Stage_2/Room_0/LAYER1_1.tilemap.lz"
.incbin "data/maps/dummy.bin"


.globl AIRPORT_STAGE2_ROOM0_LAYER0_2
AIRPORT_STAGE2_ROOM0_LAYER0_2:
.incbin "data/maps/rooms/Airport/Stage_2/Room_0/LAYER0_2.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM0_COLLISION_2
AIRPORT_STAGE2_ROOM0_COLLISION_2:
.incbin "data/maps/rooms/Airport/Stage_2/Room_0/COLLISION_2.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM0_LAYER1_2
AIRPORT_STAGE2_ROOM0_LAYER1_2:
.incbin "data/maps/rooms/Airport/Stage_2/Room_0/LAYER1_2.tilemap.lz"
.incbin "data/maps/dummy.bin"


.globl AIRPORT_STAGE2_ROOM3_LAYER0_1
AIRPORT_STAGE2_ROOM3_LAYER0_1:
.incbin "data/maps/rooms/Airport/Stage_2/Room_3/LAYER0_1.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM3_COLLISION_1
AIRPORT_STAGE2_ROOM3_COLLISION_1:
.incbin "data/maps/rooms/Airport/Stage_2/Room_3/COLLISION_1.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM3_LAYER0_0
AIRPORT_STAGE2_ROOM3_LAYER0_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_3/LAYER0_0.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM3_COLLISION_0
AIRPORT_STAGE2_ROOM3_COLLISION_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_3/COLLISION_0.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM3_LAYER1_1
AIRPORT_STAGE2_ROOM3_LAYER1_1:
.incbin "data/maps/rooms/Airport/Stage_2/Room_3/LAYER1_1.tilemap.lz"


.globl dword_808A880
dword_808A880:
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE2_ROOM3_LAYER1_0
AIRPORT_STAGE2_ROOM3_LAYER1_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_3/LAYER1_0.tilemap.lz"
.incbin "data/maps/dummy.bin"


.globl AIRPORT_STAGE2_ROOM4_LAYER0_0
AIRPORT_STAGE2_ROOM4_LAYER0_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_4/LAYER0_0.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM4_COLLISION_0
AIRPORT_STAGE2_ROOM4_COLLISION_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_4/COLLISION_0.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM4_PARALLAX_CHUNK_0
AIRPORT_STAGE2_ROOM4_PARALLAX_CHUNK_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_4/PARALLAX_CHUNK_0.bin"


.globl dword_808B5E0
dword_808B5E0:
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE2_ROOM4_LAYER1_0
AIRPORT_STAGE2_ROOM4_LAYER1_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_4/LAYER1_0.tilemap.lz"
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE2_ROOM5_LAYER0_0
AIRPORT_STAGE2_ROOM5_LAYER0_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_5/LAYER0_0.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM5_COLLISION_0
AIRPORT_STAGE2_ROOM5_COLLISION_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_5/COLLISION_0.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM5_LAYER0_1
AIRPORT_STAGE2_ROOM5_LAYER0_1:
.incbin "data/maps/rooms/Airport/Stage_2/Room_5/LAYER0_1.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM5_COLLISION_1
AIRPORT_STAGE2_ROOM5_COLLISION_1:
.incbin "data/maps/rooms/Airport/Stage_2/Room_5/COLLISION_1.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM5_LAYER1_0
AIRPORT_STAGE2_ROOM5_LAYER1_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_5/LAYER1_0.tilemap.lz"


.globl dword_808C954
dword_808C954:
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE2_ROOM5_LAYER1_1
AIRPORT_STAGE2_ROOM5_LAYER1_1:
.incbin "data/maps/rooms/Airport/Stage_2/Room_5/LAYER1_1.tilemap.lz"
.incbin "data/maps/dummy.bin"



.globl AIRPORT_STAGE2_ROOM6_LAYER0_0
AIRPORT_STAGE2_ROOM6_LAYER0_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_6/LAYER0_0.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM6_COLLISION_0
AIRPORT_STAGE2_ROOM6_COLLISION_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_6/COLLISION_0.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM6_LAYER1_0
AIRPORT_STAGE2_ROOM6_LAYER1_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_6/LAYER1_0.tilemap.lz"
.incbin "data/maps/dummy.bin"



.globl unk_808d5f4
unk_808d5f4:
.incbin "data/maps/rooms/Airport/Stage_2/Room_7/LAYER0_0.tilemap.lz"


.globl dword_808DC2C
dword_808DC2C:
.incbin "data/maps/rooms/Airport/Stage_2/Room_7/COLLISION_0.tilemap.lz"


.globl unk_808dccc
unk_808dccc:
.incbin "data/maps/rooms/Airport/Stage_2/Room_7/LAYER1_0.tilemap.lz"
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE2_ROOM1_LAYER0_0
AIRPORT_STAGE2_ROOM1_LAYER0_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_2/LAYER0_0.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM1_COLLISION_0
AIRPORT_STAGE2_ROOM1_COLLISION_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_2/COLLISION_0.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM1_LAYER0_1
AIRPORT_STAGE2_ROOM1_LAYER0_1:
.incbin "data/maps/rooms/Airport/Stage_2/Room_2/LAYER0_1.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM1_COLLISION_1
AIRPORT_STAGE2_ROOM1_COLLISION_1:
.incbin "data/maps/rooms/Airport/Stage_2/Room_2/COLLISION_1.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM2_LAYER0_2
AIRPORT_STAGE2_ROOM2_LAYER0_2:
.incbin "data/maps/rooms/Airport/Stage_2/Room_2/LAYER0_2.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM2_COLLISION_2
AIRPORT_STAGE2_ROOM2_COLLISION_2:
.incbin "data/maps/rooms/Airport/Stage_2/Room_2/COLLISION_2.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM1_LAYER0_3
AIRPORT_STAGE2_ROOM1_LAYER0_3:
.incbin "data/maps/rooms/Airport/Stage_2/Room_2/LAYER0_3.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM2_COLLISION_3
AIRPORT_STAGE2_ROOM2_COLLISION_3:
.incbin "data/maps/rooms/Airport/Stage_2/Room_2/COLLISION_3.tilemap.lz"


.globl AIRPORT_STAGE2_ROOM2_LAYER1_0
AIRPORT_STAGE2_ROOM2_LAYER1_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_2/LAYER1_0.tilemap.lz"

.globl dword_808F208
dword_808F208:
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE2_ROOM2_LAYER1_1
AIRPORT_STAGE2_ROOM2_LAYER1_1:
.incbin "data/maps/rooms/Airport/Stage_2/Room_2/LAYER1_1.tilemap.lz"


.globl dword_808F578
dword_808F578:
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE2_ROOM2_LAYER1_2
AIRPORT_STAGE2_ROOM2_LAYER1_2:
.incbin "data/maps/rooms/Airport/Stage_2/Room_2/LAYER1_2.tilemap.lz"


.globl dword_808F8E8
dword_808F8E8:
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE2_ROOM2_LAYER1_3
AIRPORT_STAGE2_ROOM2_LAYER1_3:
.incbin "data/maps/rooms/Airport/Stage_2/Room_2/LAYER1_3.tilemap.lz"
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE2_ROOM1_PARALLAX_CHUNK_0
AIRPORT_STAGE2_ROOM1_PARALLAX_CHUNK_0:
.incbin "data/maps/rooms/Airport/Stage_2/Room_2/PARALLAX_CHUNK_0.bin"


.globl dword_8090168
dword_8090168:
.incbin "data/maps/dummy.bin"

.globl unk_80901e8
unk_80901e8:
.long 0x11000f, 0xc740009, 0x14b60009, 0x1cf8000f, 0x14b60009, 0xc740009, 0x0


.globl unk_8090204
unk_8090204:
.long 0x874000f, 0x14d70009, 0x1d190009, 0x255b000f, 0x1d190009, 0x14d70009, 0x0


.globl unk_8090220
unk_8090220:
.long 0x14d7000f, 0x21390009, 0x297b0009, 0x31bd000f, 0x297b0009, 0x21390009, 0x0


.globl unk_809023c
unk_809023c:
.long 0x213a000f, 0x2d9b0009, 0x35dd0009, 0x3e1f000f, 0x35dd0009, 0x2d9b0009, 0x0


.globl unk_8090258
unk_8090258:
.long 0x2d9d000f, 0x39fe0009, 0x423e0009, 0x4a7f000f, 0x423e0009, 0x39fe0009, 0x0


.globl off_8090274
off_8090274:
.long unk_80901e8, 0x86
.long unk_8090204, 0x87
.long unk_8090220, 0x88
.long unk_809023c, 0x89
.long unk_8090258, 0x8a, 0x0, 0x0


.globl AIRPORT_STAGE3_ROOM0_LAYER0_0
AIRPORT_STAGE3_ROOM0_LAYER0_0:
.incbin "data/maps/rooms/Airport/Stage_3/Room_0/LAYER0_0.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM0_COLLISION_0
AIRPORT_STAGE3_ROOM0_COLLISION_0:
.incbin "data/maps/rooms/Airport/Stage_3/Room_0/COLLISION_0.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM2_TILESET
AIRPORT_STAGE3_ROOM2_TILESET:
.incbin "build/gfx/level_tilesets/airport_stage3.4bpp.lz"


.globl AIRPORT_STAGE3_ROOM0_PALETTE
AIRPORT_STAGE3_ROOM0_PALETTE:
.incbin "build/gfx/level_tilesets/airport_stage3.gbapal"


.globl AIRPORT_STAGE3_ROOM0_LAYER1_0
AIRPORT_STAGE3_ROOM0_LAYER1_0:
.incbin "data/maps/rooms/Airport/Stage_3/Room_0/LAYER1_0.tilemap.lz"
.incbin "data/maps/dummy.bin"


.globl AIRPORT_STAGE3_ROOM0_LAYER0_1
AIRPORT_STAGE3_ROOM0_LAYER0_1:
.incbin "data/maps/rooms/Airport/Stage_3/Room_0/LAYER0_1.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM0_COLLISION_1
AIRPORT_STAGE3_ROOM0_COLLISION_1:
.incbin "data/maps/rooms/Airport/Stage_3/Room_0/COLLISION_1.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM0_LAYER1_1
AIRPORT_STAGE3_ROOM0_LAYER1_1:
.incbin "data/maps/rooms/Airport/Stage_3/Room_0/LAYER1_1.tilemap.lz"
.incbin "data/maps/dummy.bin"


.globl AIRPORT_STAGE3_ROOM0_LAYER0_2
AIRPORT_STAGE3_ROOM0_LAYER0_2:
.incbin "data/maps/rooms/Airport/Stage_3/Room_0/LAYER0_2.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM0_COLLISION_2
AIRPORT_STAGE3_ROOM0_COLLISION_2:
.incbin "data/maps/rooms/Airport/Stage_3/Room_0/COLLISION_2.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM0_LAYER1_2
AIRPORT_STAGE3_ROOM0_LAYER1_2:
.incbin "data/maps/rooms/Airport/Stage_3/Room_0/LAYER1_2.tilemap.lz"
.incbin "data/maps/dummy.bin"


.globl AIRPORT_STAGE3_ROOM0_LAYER0_3
AIRPORT_STAGE3_ROOM0_LAYER0_3:
.incbin "data/maps/rooms/Airport/Stage_3/Room_0/LAYER0_3.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM0_COLLISION_3
AIRPORT_STAGE3_ROOM0_COLLISION_3:
.incbin "data/maps/rooms/Airport/Stage_3/Room_0/COLLISION_3.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM0_LAYER1_3
AIRPORT_STAGE3_ROOM0_LAYER1_3:
.incbin "data/maps/rooms/Airport/Stage_3/Room_0/LAYER1_3.tilemap.lz"
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE3_ROOM0_LAYER0_4
AIRPORT_STAGE3_ROOM0_LAYER0_4:
.incbin "data/maps/rooms/Airport/Stage_3/Room_0/LAYER0_4.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM0_COLLISION_4
AIRPORT_STAGE3_ROOM0_COLLISION_4:
.incbin "data/maps/rooms/Airport/Stage_3/Room_0/COLLISION_4.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM0_LAYER1_4
AIRPORT_STAGE3_ROOM0_LAYER1_4:
.incbin "data/maps/rooms/Airport/Stage_3/Room_0/LAYER1_4.tilemap.lz"
.incbin "data/maps/dummy.bin"


.globl AIRPORT_STAGE3_ROOM2_LAYER0_0
AIRPORT_STAGE3_ROOM2_LAYER0_0:
.incbin "data/maps/rooms/Airport/Stage_3/Room_2/LAYER0_0.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM2_COLLISION_0
AIRPORT_STAGE3_ROOM2_COLLISION_0:
.incbin "data/maps/rooms/Airport/Stage_3/Room_2/COLLISION_0.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM1_LAYER0_0
AIRPORT_STAGE3_ROOM1_LAYER0_0:
.incbin "data/maps/rooms/Airport/Stage_3/Room_1/LAYER0_0.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM1_COLLISION_0
AIRPORT_STAGE3_ROOM1_COLLISION_0:
.incbin "data/maps/rooms/Airport/Stage_3/Room_1/COLLISION_0.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM3_LAYER0_0
AIRPORT_STAGE3_ROOM3_LAYER0_0:
.incbin "data/maps/rooms/Airport/Stage_3/Room_3/LAYER0_0.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM3_COLLISION_0
AIRPORT_STAGE3_ROOM3_COLLISION_0:
.incbin "data/maps/rooms/Airport/Stage_3/Room_3/COLLISION_0.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM3_LAYER1_0
AIRPORT_STAGE3_ROOM3_LAYER1_0:
.incbin "data/maps/rooms/Airport/Stage_3/Room_3/LAYER1_0.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM4_LAYER0_0
AIRPORT_STAGE3_ROOM4_LAYER0_0:
.incbin "data/maps/rooms/Airport/Stage_3/Room_4/LAYER0_0.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM4_COLLISION_0
AIRPORT_STAGE3_ROOM4_COLLISION_0:
.incbin "data/maps/rooms/Airport/Stage_3/Room_4/COLLISION_0.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM4_LAYER1_0
AIRPORT_STAGE3_ROOM4_LAYER1_0:
.incbin "data/maps/rooms/Airport/Stage_3/Room_4/LAYER1_0.tilemap.lz"



.globl AIRPORT_STAGE3_ROOM5_LAYER0_0
AIRPORT_STAGE3_ROOM5_LAYER0_0:
.incbin "data/maps/rooms/Airport/Stage_3/Room_5/LAYER0_0.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM5_COLLISION_0
AIRPORT_STAGE3_ROOM5_COLLISION_0:
.incbin "data/maps/rooms/Airport/Stage_3/Room_5/COLLISION_0.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM5_LAYER1_0
AIRPORT_STAGE3_ROOM5_LAYER1_0:
.incbin "data/maps/rooms/Airport/Stage_3/Room_5/LAYER1_0.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM0_PARALLAX_CHUNK_0
AIRPORT_STAGE3_ROOM0_PARALLAX_CHUNK_0:
.incbin "data/maps/rooms/Airport/Stage_3/Room_0/PARALLAX_CHUNK_0.bin"
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE3_ROOM1_LAYER1_0
AIRPORT_STAGE3_ROOM1_LAYER1_0:
.incbin "data/maps/rooms/Airport/Stage_3/Room_1/LAYER1_0.tilemap.lz"


.globl AIRPORT_STAGE3_ROOM2_LAYER1_0
AIRPORT_STAGE3_ROOM2_LAYER1_0:
.incbin "data/maps/rooms/Airport/Stage_3/Room_2/LAYER1_0.tilemap.lz"


.globl unk_8098344
unk_8098344:
.long 0x11000f, 0xc740009, 0x14b60009, 0x1cf8000f, 0x14b60009, 0xc740009, 0x0


.globl unk_8098360
unk_8098360:
.long 0x874000f, 0x14d70009, 0x1d190009, 0x255b000f, 0x1d190009, 0x14d70009, 0x0


.globl unk_809837c
unk_809837c:
.long 0x14d7000f, 0x21390009, 0x297b0009, 0x31bd000f, 0x297b0009, 0x21390009, 0x0


.globl unk_8098398
unk_8098398:
.long 0x213a000f, 0x2d9b0009, 0x35dd0009, 0x3e1f000f, 0x35dd0009, 0x2d9b0009, 0x0


.globl unk_80983b4
unk_80983b4:
.long 0x2d9d000f, 0x39fe0009, 0x423e0009, 0x4a7f000f, 0x423e0009, 0x39fe0009, 0x0


.globl off_80983D0
off_80983D0:
.long unk_8098344, 0x76
.long unk_8098360, 0x77
.long unk_809837c, 0x78
.long unk_8098398, 0x79
.long unk_80983b4, 0x7a, 0x0, 0x0


.globl AIRPORT_STAGE4_ROOM0_PARALLAX_CHUNK_0
AIRPORT_STAGE4_ROOM0_PARALLAX_CHUNK_0:
.incbin "data/maps/rooms/Airport/Stage_4/Room_0/PARALLAX_CHUNK_0.bin"
.incbin "data/maps/dummy.bin"

.globl AIRPORT_STAGE4_ROOM0_TILESET
AIRPORT_STAGE4_ROOM0_TILESET:
.incbin "build/gfx/level_tilesets/airport_stage4.4bpp.lz"


.globl AIRPORT_STAGE4_ROOM0_PALETTE
AIRPORT_STAGE4_ROOM0_PALETTE:
.incbin "build/gfx/level_tilesets/airport_stage4.gbapal"


.globl AIRPORT_STAGE4_ROOM0_LAYER0_0
AIRPORT_STAGE4_ROOM0_LAYER0_0:
.incbin "data/maps/rooms/Airport/Stage_4/Room_0/LAYER0_0.tilemap.lz"



.globl AIRPORT_STAGE4_ROOM0_COLLISION_0
AIRPORT_STAGE4_ROOM0_COLLISION_0:
.incbin "data/maps/rooms/Airport/Stage_4/Room_0/COLLISION_0.tilemap.lz"


.globl AIRPORT_STAGE4_ROOM0_LAYER1_0
AIRPORT_STAGE4_ROOM0_LAYER1_0:
.incbin "data/maps/rooms/Airport/Stage_4/Room_0/LAYER1_0.tilemap.lz"


.globl unk_809b508
unk_809b508:
.long 0x2460005, 0x1ac0005, 0x580005, 0xf60005, 0x2320005, 0x22e0005, 0x0


.globl unk_809b524
unk_809b524:
.long 0x580005, 0xf60005, 0x2320005, 0x22e0005, 0x2460005, 0x1ac0005, 0x0


.globl unk_809b540
unk_809b540:
.long 0x2320005, 0x22e0005, 0x2460005, 0x1ac0005, 0x580005, 0xf60005, 0x0


.globl unk_809B55C
unk_809B55C:
.long unk_809b508, 0x58
.long unk_809b524, 0x59
.long unk_809b540, 0x5a, 0x0, 0x0


.globl unk_809b57c
unk_809b57c:
.long 0x2460010, 0x9e60003, 0x11670003, 0x18e70003, 0x10cc0003, 0x8920003, 0x580010, 0x8920003, 0x10cc0003, 0x18e70003, 0x114b0003, 0x9ae0003, 0x2320010, 0x9ae0003, 0x114b0003, 0x18e70003
.long 0x11670003, 0x9e60003, 0x0


.globl unk_809b5c8
unk_809b5c8:
.long 0x580010, 0x8920003, 0x10cc0003, 0x18e70003, 0x114b0003, 0x9ae0003, 0x2320010, 0x9ae0003, 0x114b0003, 0x18e70003, 0x11670003, 0x9e60003, 0x2460010, 0x9e60003, 0x11670003, 0x18e70003
.long 0x10cc0003, 0x8920003, 0x0


.globl unk_809b614
unk_809b614:
.long 0x2320010, 0x9ae0003, 0x114b0003, 0x18e70003, 0x11670003, 0x9e60003, 0x2460010, 0x9e60003, 0x11670003, 0x18e70003, 0x10cc0003, 0x8920003, 0x580010, 0x8920003, 0x10cc0003, 0x18e70003
.long 0x114b0003, 0x9ae0003, 0x0


.globl byte_809B660
byte_809B660:
.long unk_809b57c, 0x5c
.long unk_809b5c8, 0x5d
.long unk_809b614, 0x5e, 0x0, 0x0


.globl unk_809B680
unk_809B680:
.long 0x246001e, 0x58001e, 0x232001e, 0x0


.globl unk_809B690
unk_809B690:
.long 0x58001e, 0x232001e, 0x246001e, 0x0


.globl unk_809B6A0
unk_809B6A0:
.long 0x232001e, 0x246001e, 0x58001e, 0x0


.globl off_809B6B0
off_809B6B0:
.long unk_809B680, 0xc
.long unk_809B690, 0xd
.long unk_809B6A0, 0xe, 0x0, 0x0


.globl unk_809B6D0
unk_809B6D0:
.long 0x18e70008, 0x2460008, 0x2460008, 0x2460008, 0x2460008, 0x2460008, 0x0


.globl unk_809B6EC
unk_809B6EC:
.long 0x18e70008, 0x18e70008, 0x2320008, 0x2320008, 0x2320008, 0x18e70008, 0x0


.globl unk_809B708
unk_809B708:
.long 0x18e70008, 0x18e70008, 0x18e70008, 0x580008, 0x18e70008, 0x18e70008, 0x0


.globl unk_809B724
unk_809B724:
.long 0x246000a, 0x246000a, 0x246000a, 0x18e7000a, 0x246000a, 0x246000a, 0x0


.globl unk_809B740
unk_809B740:
.long 0x232000a, 0x232000a, 0x18e7000a, 0x18e7000a, 0x18e7000a, 0x232000a, 0x0


.globl unk_809B75C
unk_809B75C:
.long 0x58000a, 0x18e7000a, 0x18e7000a, 0x18e7000a, 0x18e7000a, 0x18e7000a, 0x0


.globl off_809B778
off_809B778:
.long unk_809B6D0, 0x6a
.long unk_809B6EC, 0x6b
.long unk_809B708, 0x6c
.long unk_809B724, 0x6d
.long unk_809B740, 0x6e
.long unk_809B75C, 0x6f, 0x0, 0x0


.globl unk_809B7B0
unk_809B7B0:
.long 0x2460008, 0x1a40008, 0x1230008, 0x8, 0x1230008, 0x1a40008, 0x0


.globl unk_809B7CC
unk_809B7CC:
.long 0x2c0008


.globl a2X2
a2X2:
.long 0x320008, 0x580008, 0x320008, 0x2c0008, 0x8, 0x0


.globl unk_809B7E8
unk_809B7E8:
.long 0x1090008, 0x8, 0x1090008, 0x1ad0008, 0x2320008, 0x1ad0008, 0x0


.globl off_809B804
off_809B804:
.long unk_809B7B0, 0x7c
.long unk_809B7CC, 0x7d
.long unk_809B7E8, 0x7e, 0x0, 0x0
