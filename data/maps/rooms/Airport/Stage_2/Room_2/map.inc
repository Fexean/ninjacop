AIRPORT_STAGE2_ROOM2_MAPDATA:
.byte 0x4@ height
.byte 0x1	@ width
.2byte 0xa	@ unk2
.4byte AIRPORT_STAGE2_ROOM0_PALETTE	@ palette
.2byte 0x1	@ tilesCompressed
.2byte 0x400	@ uncompTileCount
.4byte AIRPORT_STAGE2_ROOM0_TILESET	@ tileset
.byte 0x20	@ chunk width
.byte 0x14	@ chunk height
.2byte 0x1	@ mapsCompressed
.4byte AIRPORT_STAGE2_ROOM1_LAYER0_0	@ layer0 chunk
.4byte AIRPORT_STAGE2_ROOM1_LAYER0_1	@ layer0 chunk
.4byte AIRPORT_STAGE2_ROOM2_LAYER0_2	@ layer0 chunk
.4byte AIRPORT_STAGE2_ROOM1_LAYER0_3	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte AIRPORT_STAGE2_ROOM2_LAYER1_0	@ layer1 chunk
.4byte AIRPORT_STAGE2_ROOM2_LAYER1_1	@ layer1 chunk
.4byte AIRPORT_STAGE2_ROOM2_LAYER1_2	@ layer1 chunk
.4byte AIRPORT_STAGE2_ROOM2_LAYER1_3	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0x1	@ collisionCompressed
.4byte AIRPORT_STAGE2_ROOM1_COLLISION_0	@ collision chunk
.4byte AIRPORT_STAGE2_ROOM1_COLLISION_1	@ collision chunk
.4byte AIRPORT_STAGE2_ROOM2_COLLISION_2	@ collision chunk
.4byte AIRPORT_STAGE2_ROOM2_COLLISION_3	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ load callbacks
.4byte 0	@ load callbacks
.4byte 0	@ load callbacks
.4byte 0	@ load callbacks
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk


AIRPORT_STAGE2_ROOM2_HEADER:
.2byte 0x2a	@ bitfield
.2byte 0x5	@ tilesetStart
.4byte AIRPORT_STAGE2_ROOM2_MAPDATA	@ mapdata
.4byte AIRPORT_STAGE2_ROOM2_PARALLAX	@ scroll
.2byte 0x400	@ height
.2byte 0xa0	@ width

