AIRPORT_STAGE1_ROOM3_MAPDATA:
.byte 0x2@ height
.byte 0x1	@ width
.2byte 0x0	@ unk2
.4byte AIRPORT_STAGE1_ROOM1_PALETTE	@ palette
.2byte 0x1	@ tilesCompressed
.2byte 0x190	@ uncompTileCount
.4byte AIRPORT_STAGE1_ROOM1_TILESET	@ tileset
.byte 0x20	@ chunk width
.byte 0x20	@ chunk height
.2byte 0x1	@ mapsCompressed
.4byte AIRPORT_STAGE1_ROOM3_LAYER0_0	@ layer0 chunk
.4byte AIRPORT_STAGE1_ROOM3_LAYER0_1	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte 0	@ layer0 chunk
.4byte AIRPORT_STAGE1_ROOM3_LAYER1_0	@ layer1 chunk
.4byte AIRPORT_STAGE1_ROOM3_LAYER1_1	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0	@ layer1 chunk
.4byte 0x1	@ collisionCompressed
.4byte AIRPORT_STAGE1_ROOM3_COLLISION_0	@ collision chunk
.4byte AIRPORT_STAGE1_ROOM3_COLLISION_1	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ collision chunk
.4byte 0	@ load callbacks
.4byte 0	@ load callbacks
.4byte 0	@ load callbacks
.4byte 0	@ load callbacks
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk
.4byte 0	@ layer2 chunk


AIRPORT_STAGE1_ROOM3_HEADER:
.2byte 0x1a	@ bitfield
.2byte 0x5	@ tilesetStart
.4byte AIRPORT_STAGE1_ROOM3_MAPDATA	@ mapdata
.4byte AIRPORT_STAGE1_ROOM3_PARALLAX	@ scroll
.2byte 0x200	@ height
.2byte 0x100	@ width

