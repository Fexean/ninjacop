
@ This file contains initial camera states for each door in each room, formatted like:
@ struct{
@     u16 cameraX, cameraY, mode, padding;
@ };

@ TODO: place these in room directories


.globl bankCamStatesStage1Room0
bankCamStatesStage1Room0:
.long 0x9f0000, 0x0, 0xd80208, 0x0, 0x9f03b0, 0x0


.globl bankCamStatesStage1Room1
bankCamStatesStage1Room1:
.long 0x9f0000, 0x0


.globl bankCamStatesStage1Room2
bankCamStatesStage1Room2:
.long 0x9f0000, 0x0


.globl bankCamStatesStage1
bankCamStatesStage1:
.long bankCamStatesStage1Room0
.long bankCamStatesStage1Room1
.long bankCamStatesStage1Room2


.globl bankCamStatesStage2Room0
bankCamStatesStage2Room0:
.long 0x9f0000, 0x0
.long 0xdf0008, 0x0, 0x9f0050, 0x0, 0xff0310, 0x0, 0xcf0310, 0x0


.globl bankCamStatesStage2Room1
bankCamStatesStage2Room1:
.long 0x9f0000, 0x0


.globl bankCamStatesStage2Room2
bankCamStatesStage2Room2:
.long 0x9f0000, 0x0


.globl bankCamStatesStage2Room3
bankCamStatesStage2Room3:
.long 0x9f0000, 0x0


.globl bankCamStatesStage2Room4
bankCamStatesStage2Room4:
.long 0x9f0000, 0x0


.globl bankCamStatesStage2
bankCamStatesStage2:
.long bankCamStatesStage2Room0
.long bankCamStatesStage2Room1
.long bankCamStatesStage2Room2
.long bankCamStatesStage2Room3
.long bankCamStatesStage2Room4


.globl bankCamStatesStage3Room0
bankCamStatesStage3Room0:
.long 0x9f0000, 0x0, 0x9f01e8, 0x0, 0xef01e8, 0x0, 0x9f0210, 0x0
.long 0xdf0040, 0x0, 0xbf0210, 0x0


.globl bankCamStatesStage3Room1
bankCamStatesStage3Room1:
.long 0x9f0000, 0x0


.globl bankCamStatesStage3Room2
bankCamStatesStage3Room2:
.long 0x9f0000, 0x0


.globl bankCamStatesStage3Room3
bankCamStatesStage3Room3:
.long 0x9f0008, 0x0


.globl bankCamStatesStage3Room4
bankCamStatesStage3Room4:
.long 0x9f0008, 0x0


.globl bankCamStatesStage4Room0
bankCamStatesStage4Room0:
.long 0xff0008, 0x6, 0xff0000, 0x4


.globl bankCamStatesStage3
bankCamStatesStage3:
.long bankCamStatesStage3Room0
.long bankCamStatesStage3Room1
.long bankCamStatesStage3Room2
.long bankCamStatesStage3Room3
.long bankCamStatesStage3Room4


.globl bankCamStatesStage4
bankCamStatesStage4:
.long bankCamStatesStage4Room0


.globl bankCamStates
bankCamStates:
.long bankCamStatesStage1
.long bankCamStatesStage2
.long bankCamStatesStage3
.long bankCamStatesStage4


.globl harborCamStatesStage1Room0
harborCamStatesStage1Room0:
.long 0x9f0000, 0x0, 0x9f0110, 0x0


.globl harborCamStatesStage1Room1
harborCamStatesStage1Room1:
.long 0x9f0008, 0x0, 0x16f0010, 0x0, 0x1ff0010, 0x0


.globl harborCamStatesStage1Room2
harborCamStatesStage1Room2:
.long 0x9f0000, 0x0, 0xff0000, 0x0, 0xff0110, 0x0, 0x9f0110, 0x0


.globl harborCamStatesStage1Room3
harborCamStatesStage1Room3:
.long 0x9f0000, 0x0


.globl harborCamStatesStage1Room4
harborCamStatesStage1Room4:
.long 0x9f0000, 0x0


.globl harborCamStatesStage1
harborCamStatesStage1:
.long harborCamStatesStage1Room0
.long harborCamStatesStage1Room1
.long harborCamStatesStage1Room2
.long harborCamStatesStage1Room3
.long harborCamStatesStage1Room4


.globl harborCamStatesStage2Room0
harborCamStatesStage2Room0:
.long 0x9f0000, 0x0, 0xf70000, 0x0, 0xf70310, 0x0
.long 0xaf0058, 0x0


.globl harborCamStatesStage2Room1
harborCamStatesStage2Room1:
.long 0x9f0008, 0x0


.globl harborCamStatesStage2Room2
harborCamStatesStage2Room2:
.long 0xc70008, 0x0, 0x1ff0008, 0x0


.globl harborCamStatesStage2Room3
harborCamStatesStage2Room3:
.long 0x9f0000, 0x0


.globl harborCamStatesStage2
harborCamStatesStage2:
.long harborCamStatesStage2Room0
.long harborCamStatesStage2Room1
.long harborCamStatesStage2Room2
.long harborCamStatesStage2Room3


.globl harborCamStatesStage3Room0
harborCamStatesStage3Room0:
.long 0xaf0088, 0x0, 0x1ff0000, 0x0, 0x1ff0110, 0x0, 0xbf0000, 0x0, 0xbf0110, 0x0


.globl harborCamStatesStage3Room1
harborCamStatesStage3Room1:
.long 0x9f0008, 0x0


.globl harborCamStatesStage3Room2
harborCamStatesStage3Room2:
.long 0xff0000, 0x0


.globl harborCamStatesStage3Room3
harborCamStatesStage3Room3:
.long 0x12f0008, 0x0


.globl harborCamStatesStage3Room4
harborCamStatesStage3Room4:
.long 0xef0000, 0x0


.globl harborCamStatesStage3
harborCamStatesStage3:
.long harborCamStatesStage3Room0
.long harborCamStatesStage3Room1
.long harborCamStatesStage3Room2
.long harborCamStatesStage3Room3
.long harborCamStatesStage3Room4


.globl harborCamStatesStage4Room0
harborCamStatesStage4Room0:
.long 0xb70088, 0x6


.globl harborCamStatesStage4
harborCamStatesStage4:
.long harborCamStatesStage4Room0


.globl harborCamStates
harborCamStates:
.long harborCamStatesStage1
.long harborCamStatesStage2
.long harborCamStatesStage3
.long harborCamStatesStage4


.globl airportCamStatesStage1Room0
airportCamStatesStage1Room0:
.long 0x9f0000, 0x0, 0x9f0210, 0x0, 0x1ef0210, 0x0, 0x13f0210, 0x0, 0x13f0000, 0x0


.globl airportCamStatesStage1Room1
airportCamStatesStage1Room1:
.long 0x9f0008, 0x0, 0x1e70008, 0x0, 0xdf0000, 0x0


.globl airportCamStatesStage1Room2
airportCamStatesStage1Room2:
.long 0xff0000, 0x0


.globl airportCamStatesStage1Room3
airportCamStatesStage1Room3:
.long 0x9f0110, 0x0


.globl airportCamStatesStage1
airportCamStatesStage1:
.long airportCamStatesStage1Room0
.long airportCamStatesStage1Room1
.long airportCamStatesStage1Room2
.long airportCamStatesStage1Room3


.globl airportCamStatesStage2Room0
airportCamStatesStage2Room0:
.long 0xaf0000, 0x0, 0xaf0108, 0x0, 0xaf0210, 0x0


.globl airportCamStatesStage2Room1
airportCamStatesStage2Room1:
.long 0x9f0000, 0x0, 0x9f0000, 0x0


.globl airportCamStatesStage2Room2
airportCamStatesStage2Room2:
.long 0x9f0000, 0x0, 0x9f0000, 0x0


.globl airportCamStatesStage2Room3
airportCamStatesStage2Room3:
.long 0x9f0008, 0x0, 0x1470080, 0x0, 0x1ff0080, 0x0


.globl airportCamStatesStage2Room4
airportCamStatesStage2Room4:
.long 0x9f0008, 0x0


.globl airportCamStatesStage2Room5
airportCamStatesStage2Room5:
.long 0xf00088, 0x0, 0xa80000, 0x0, 0x1000000, 0x0, 0xa80110, 0x0, 0x1000110, 0x0


.globl airportCamStatesStage2Room6
airportCamStatesStage2Room6:
.long 0xa80010, 0x0, 0x1000010, 0x0


.globl airportCamStatesStage2Room7
airportCamStatesStage2Room7:
.long 0xa80000, 0x0, 0x1000000, 0x0


.globl airportCamStatesStage2
airportCamStatesStage2:
.long airportCamStatesStage2Room0
.long airportCamStatesStage2Room1
.long airportCamStatesStage2Room2
.long airportCamStatesStage2Room3
.long airportCamStatesStage2Room4
.long airportCamStatesStage2Room5
.long airportCamStatesStage2Room6
.long airportCamStatesStage2Room7


.globl airportCamStatesStage3Room0
airportCamStatesStage3Room0:
.long 0x9f0000, 0x0, 0x9f0040, 0x0, 0x9f01f0, 0x0, 0x9f0410, 0x0, 0x9f0070, 0x0


.globl airportCamStatesStage3Room1
airportCamStatesStage3Room1:
.long 0x9f0000, 0x0


.globl airportCamStatesStage3Room2
airportCamStatesStage3Room2:
.long 0x9f0000, 0x0


.globl airportCamStatesStage3Room3
airportCamStatesStage3Room3:
.long 0x9f0000, 0x0


.globl airportCamStatesStage3Room4
airportCamStatesStage3Room4:
.long 0x9f0000, 0x0


.globl airportCamStatesStage3Room5
airportCamStatesStage3Room5:
.long 0x9f0000, 0x0


.globl airportCamStatesStage3
airportCamStatesStage3:
.long airportCamStatesStage3Room0
.long airportCamStatesStage3Room1
.long airportCamStatesStage3Room2
.long airportCamStatesStage3Room3
.long airportCamStatesStage3Room4
.long airportCamStatesStage3Room5


.globl airportCamStatesStage4Room0
airportCamStatesStage4Room0:
.long 0x9f0000, 0x6


.globl airportCamStatesStage4
airportCamStatesStage4:
.long airportCamStatesStage4Room0


.globl airportCamStates
airportCamStates:
.long airportCamStatesStage1
.long airportCamStatesStage2
.long airportCamStatesStage3
.long airportCamStatesStage4


.globl caveCamStatesStage1Room0
caveCamStatesStage1Room0:
.long 0x9f0008, 0x0, 0x9f0000, 0x0, 0x9f0010, 0x0, 0x2ff0000, 0x0


.globl caveCamStatesStage1Room1
caveCamStatesStage1Room1:
.long 0x9f0010, 0x0, 0x18f0010, 0x0


.globl caveCamStatesStage1Room2
caveCamStatesStage1Room2:
.long 0x9f0000, 0x0


.globl caveCamStatesStage1Room3
caveCamStatesStage1Room3:
.long 0x9f0008, 0x0


.globl caveCamStatesStage1
caveCamStatesStage1:
.long caveCamStatesStage1Room0
.long caveCamStatesStage1Room1
.long caveCamStatesStage1Room2
.long caveCamStatesStage1Room3


.globl caveCamStatesStage2Room0
caveCamStatesStage2Room0:
.long 0x1f70088, 0x0, 0x1670070, 0x0, 0x16700a0, 0x0, 0xb70070, 0x0, 0xb700a0, 0x0


.globl caveCamStatesStage2Room1
caveCamStatesStage2Room1:
.long 0x9f0000, 0x0


.globl caveCamStatesStage2Room2
caveCamStatesStage2Room2:
.long 0x9f0000, 0x0


.globl caveCamStatesStage2Room3
caveCamStatesStage2Room3:
.long 0x11f0010, 0x0, 0x9f0010, 0x0, 0x19f0010, 0x0


.globl caveCamStatesStage2Room4
caveCamStatesStage2Room4:
.long 0xff0000, 0x0, 0x9f0000, 0x0
.long 0xff0060, 0x0


.globl caveCamStatesStage2
caveCamStatesStage2:
.long caveCamStatesStage2Room0
.long caveCamStatesStage2Room1
.long caveCamStatesStage2Room2
.long caveCamStatesStage2Room3
.long caveCamStatesStage2Room4


.globl caveCamStatesStage3Room0
caveCamStatesStage3Room0:
.long 0xa70088, 0x0, 0x1df0000, 0x0, 0x1df0110, 0x0, 0xa70000, 0x0, 0xa70110, 0x0


.globl caveCamStatesStage3Room1
caveCamStatesStage3Room1:
.long 0xff0008, 0x0


.globl caveCamStatesStage3Room2
caveCamStatesStage3Room2:
.long 0x9f0008, 0x0


.globl caveCamStatesStage3Room3
caveCamStatesStage3Room3:
.long 0x9f0010, 0x0


.globl caveCamStatesStage3Room4
caveCamStatesStage3Room4:
.long 0x9f0000, 0x0


.globl caveCamStatesStage3
caveCamStatesStage3:
.long caveCamStatesStage3Room0
.long caveCamStatesStage3Room1
.long caveCamStatesStage3Room2
.long caveCamStatesStage3Room3
.long caveCamStatesStage3Room4


.globl caveCamStatesStage4Room0
caveCamStatesStage4Room0:
.long 0xf70008, 0x6


.globl caveCamStatesStage4
caveCamStatesStage4:
.long caveCamStatesStage4Room0


.globl caveCamStates
caveCamStates:
.long caveCamStatesStage1
.long caveCamStatesStage2
.long caveCamStatesStage3
.long caveCamStatesStage4


.globl baseCamStatesStage1Room0
baseCamStatesStage1Room0:
.long 0x1df0000, 0x0, 0x10f01e8, 0x0


.globl baseCamStatesStage1
baseCamStatesStage1:
.long baseCamStatesStage1Room0


.globl baseCamStatesStage2Room0
baseCamStatesStage2Room0:
.long 0x4ff0008, 0x0, 0x25f0008, 0x0, 0x2af0000, 0x0


.globl baseCamStatesStage2
baseCamStatesStage2:
.long baseCamStatesStage2Room0


.globl baseCamStatesStage3Room0
baseCamStatesStage3Room0:
.long 0xff0008, 0x6


.globl baseCamStatesStage3
baseCamStatesStage3:
.long baseCamStatesStage3Room0


.globl baseCamStates
baseCamStates:
.long baseCamStatesStage1
.long baseCamStatesStage2
.long baseCamStatesStage3


.globl launchCamStatesStage1Room0
launchCamStatesStage1Room0:
.long 0x9f0008, 0x0


.globl launchCamStatesStage1
launchCamStatesStage1:
.long launchCamStatesStage1Room0


.globl launchCamStates
launchCamStates:
.long launchCamStatesStage1


.globl initialCameraStates
initialCameraStates:
.long bankCamStates
.long harborCamStates
.long airportCamStates
.long caveCamStates
.long baseCamStates
.long launchCamStates
