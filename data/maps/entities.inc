
.include "constants/entity.inc"



entities_bank_stage_1_room_0:
.include "data/maps/rooms/Bank/Stage_1/Room_0/entities.inc"

entities_bank_stage_1_room_1:
.include "data/maps/rooms/Bank/Stage_1/Room_1/entities.inc"

entities_bank_stage_1_room_2:
.include "data/maps/rooms/Bank/Stage_1/Room_2/entities.inc"

entities_bank_stage_2_room_0:
.include "data/maps/rooms/Bank/Stage_2/Room_0/entities.inc"

entities_bank_stage_2_room_1:
.include "data/maps/rooms/Bank/Stage_2/Room_1/entities.inc"

entities_bank_stage_2_room_2:
.include "data/maps/rooms/Bank/Stage_2/Room_2/entities.inc"

entities_bank_stage_2_room_3:
.include "data/maps/rooms/Bank/Stage_2/Room_3/entities.inc"

entities_bank_stage_2_room_4:
.include "data/maps/rooms/Bank/Stage_2/Room_4/entities.inc"

entities_bank_stage_3_room_0:
.include "data/maps/rooms/Bank/Stage_3/Room_0/entities.inc"

entities_bank_stage_3_room_1:
.include "data/maps/rooms/Bank/Stage_3/Room_1/entities.inc"

entities_bank_stage_3_room_2:
.include "data/maps/rooms/Bank/Stage_3/Room_2/entities.inc"

entities_bank_stage_3_room_3:
.include "data/maps/rooms/Bank/Stage_3/Room_3/entities.inc"

entities_bank_stage_3_room_4:
.include "data/maps/rooms/Bank/Stage_3/Room_4/entities.inc"

entities_bank_stage_4_room_0:
.include "data/maps/rooms/Bank/Stage_4/Room_0/entities.inc"

entities_harbor_stage_1_room_0:
.include "data/maps/rooms/Harbor/Stage_1/Room_0/entities.inc"

entities_harbor_stage_1_room_1:
.include "data/maps/rooms/Harbor/Stage_1/Room_1/entities.inc"

entities_harbor_stage_1_room_2:
.include "data/maps/rooms/Harbor/Stage_1/Room_2/entities.inc"

entities_harbor_stage_1_room_3:
.include "data/maps/rooms/Harbor/Stage_1/Room_3/entities.inc"

entities_harbor_stage_1_room_4:
.include "data/maps/rooms/Harbor/Stage_1/Room_4/entities.inc"

entities_harbor_stage_2_room_0:
.include "data/maps/rooms/Harbor/Stage_2/Room_0/entities.inc"

entities_harbor_stage_2_room_1:
.include "data/maps/rooms/Harbor/Stage_2/Room_1/entities.inc"

entities_harbor_stage_2_room_2:
.include "data/maps/rooms/Harbor/Stage_2/Room_2/entities.inc"

entities_harbor_stage_2_room_3:
.include "data/maps/rooms/Harbor/Stage_2/Room_3/entities.inc"

entities_harbor_stage_3_room_0:
.include "data/maps/rooms/Harbor/Stage_3/Room_0/entities.inc"

entities_harbor_stage_3_room_1:
.include "data/maps/rooms/Harbor/Stage_3/Room_1/entities.inc"

entities_harbor_stage_3_room_2:
.include "data/maps/rooms/Harbor/Stage_3/Room_2/entities.inc"

entities_harbor_stage_3_room_3:
.include "data/maps/rooms/Harbor/Stage_3/Room_3/entities.inc"

entities_harbor_stage_3_room_4:
.include "data/maps/rooms/Harbor/Stage_3/Room_4/entities.inc"

entities_harbor_stage_4_room_0:
.include "data/maps/rooms/Harbor/Stage_4/Room_0/entities.inc"

entities_airport_stage_1_room_0:
.include "data/maps/rooms/Airport/Stage_1/Room_0/entities.inc"

entities_airport_stage_1_room_1:
.include "data/maps/rooms/Airport/Stage_1/Room_1/entities.inc"

entities_airport_stage_1_room_2:
.include "data/maps/rooms/Airport/Stage_1/Room_2/entities.inc"

entities_airport_stage_1_room_3:
.include "data/maps/rooms/Airport/Stage_1/Room_3/entities.inc"

entities_airport_stage_2_room_0:
.include "data/maps/rooms/Airport/Stage_2/Room_0/entities.inc"

entities_airport_stage_2_room_1:
.include "data/maps/rooms/Airport/Stage_2/Room_1/entities.inc"

entities_airport_stage_2_room_2:
.include "data/maps/rooms/Airport/Stage_2/Room_2/entities.inc"

entities_airport_stage_2_room_3:
.include "data/maps/rooms/Airport/Stage_2/Room_3/entities.inc"

entities_airport_stage_2_room_4:
.include "data/maps/rooms/Airport/Stage_2/Room_4/entities.inc"

entities_airport_stage_2_room_5:
.include "data/maps/rooms/Airport/Stage_2/Room_5/entities.inc"

entities_airport_stage_2_room_6:
.include "data/maps/rooms/Airport/Stage_2/Room_6/entities.inc"

entities_airport_stage_2_room_7:
.include "data/maps/rooms/Airport/Stage_2/Room_7/entities.inc"

entities_airport_stage_3_room_0:
.include "data/maps/rooms/Airport/Stage_3/Room_0/entities.inc"

entities_airport_stage_3_room_1:
.include "data/maps/rooms/Airport/Stage_3/Room_1/entities.inc"

entities_airport_stage_3_room_2:
.include "data/maps/rooms/Airport/Stage_3/Room_2/entities.inc"

entities_airport_stage_3_room_3:
.include "data/maps/rooms/Airport/Stage_3/Room_3/entities.inc"

entities_airport_stage_3_room_4:
.include "data/maps/rooms/Airport/Stage_3/Room_4/entities.inc"

entities_airport_stage_3_room_5:
.include "data/maps/rooms/Airport/Stage_3/Room_5/entities.inc"

@entities_airport_stage_3_room_6:
@.include "data/maps/rooms/Airport/Stage_3/Room_6/entities.inc"

entities_airport_stage_4_room_0:
.include "data/maps/rooms/Airport/Stage_4/Room_0/entities.inc"

entities_cave_stage_1_room_0:
.include "data/maps/rooms/Cave/Stage_1/Room_0/entities.inc"

entities_cave_stage_1_room_1:
.include "data/maps/rooms/Cave/Stage_1/Room_1/entities.inc"

entities_cave_stage_1_room_2:
.include "data/maps/rooms/Cave/Stage_1/Room_2/entities.inc"

entities_cave_stage_1_room_3:
.include "data/maps/rooms/Cave/Stage_1/Room_3/entities.inc"

entities_cave_stage_2_room_0:
.include "data/maps/rooms/Cave/Stage_2/Room_0/entities.inc"

entities_cave_stage_2_room_1:
.include "data/maps/rooms/Cave/Stage_2/Room_1/entities.inc"

entities_cave_stage_2_room_2:
.include "data/maps/rooms/Cave/Stage_2/Room_2/entities.inc"

entities_cave_stage_2_room_3:
.include "data/maps/rooms/Cave/Stage_2/Room_3/entities.inc"

entities_cave_stage_2_room_4:
.include "data/maps/rooms/Cave/Stage_2/Room_4/entities.inc"

entities_cave_stage_3_room_0:
.include "data/maps/rooms/Cave/Stage_3/Room_0/entities.inc"

entities_cave_stage_3_room_1:
.include "data/maps/rooms/Cave/Stage_3/Room_1/entities.inc"

entities_cave_stage_3_room_2:
.include "data/maps/rooms/Cave/Stage_3/Room_2/entities.inc"

entities_cave_stage_3_room_3:
.include "data/maps/rooms/Cave/Stage_3/Room_3/entities.inc"

entities_cave_stage_3_room_4:
.include "data/maps/rooms/Cave/Stage_3/Room_4/entities.inc"

entities_cave_stage_4_room_0:
.include "data/maps/rooms/Cave/Stage_4/Room_0/entities.inc"

entities_base_stage_1_room_0:
.include "data/maps/rooms/Base/Stage_1/Room_0/entities.inc"

entities_base_stage_2_room_0:
.include "data/maps/rooms/Base/Stage_2/Room_0/entities.inc"

entities_base_stage_3_room_0:
.include "data/maps/rooms/Base/Stage_3/Room_0/entities.inc"

entities_launch_stage_1_room_0:
.include "data/maps/rooms/Launch/Stage_1/Room_0/entities.inc"



entlist_Bank_0:
.long entities_bank_stage_1_room_0
.long entities_bank_stage_1_room_1
.long entities_bank_stage_1_room_2

entlist_Bank_1:
.long entities_bank_stage_2_room_0
.long entities_bank_stage_2_room_1
.long entities_bank_stage_2_room_2
.long entities_bank_stage_2_room_3
.long entities_bank_stage_2_room_4

entlist_Bank_2:
.long entities_bank_stage_3_room_0
.long entities_bank_stage_3_room_1
.long entities_bank_stage_3_room_2
.long entities_bank_stage_3_room_3
.long entities_bank_stage_3_room_4

entlist_Bank_3:
.long entities_bank_stage_4_room_0

entlist_Bank:
.long entlist_Bank_0
.long entlist_Bank_1
.long entlist_Bank_2
.long entlist_Bank_3

entlist_Harbor_0:
.long entities_harbor_stage_1_room_0
.long entities_harbor_stage_1_room_1
.long entities_harbor_stage_1_room_2
.long entities_harbor_stage_1_room_3
.long entities_harbor_stage_1_room_4

entlist_Harbor_1:
.long entities_harbor_stage_2_room_0
.long entities_harbor_stage_2_room_1
.long entities_harbor_stage_2_room_2
.long entities_harbor_stage_2_room_3

entlist_Harbor_2:
.long entities_harbor_stage_3_room_0
.long entities_harbor_stage_3_room_1
.long entities_harbor_stage_3_room_2
.long entities_harbor_stage_3_room_3
.long entities_harbor_stage_3_room_4

entlist_Harbor_3:
.long entities_harbor_stage_4_room_0

entlist_Harbor:
.long entlist_Harbor_0
.long entlist_Harbor_1
.long entlist_Harbor_2
.long entlist_Harbor_3

entlist_Airport_0:
.long entities_airport_stage_1_room_0
.long entities_airport_stage_1_room_1
.long entities_airport_stage_1_room_2
.long entities_airport_stage_1_room_3

entlist_Airport_1:
.long entities_airport_stage_2_room_0
.long entities_airport_stage_2_room_1
.long entities_airport_stage_2_room_2
.long entities_airport_stage_2_room_3
.long entities_airport_stage_2_room_4
.long entities_airport_stage_2_room_5
.long entities_airport_stage_2_room_6
.long entities_airport_stage_2_room_7

entlist_Airport_2:
.long entities_airport_stage_3_room_0
.long entities_airport_stage_3_room_1
.long entities_airport_stage_3_room_2
.long entities_airport_stage_3_room_3
.long entities_airport_stage_3_room_4
.long entities_airport_stage_3_room_5
@.long entities_airport_stage_3_room_6

entlist_Airport_3:
.long entities_airport_stage_4_room_0

entlist_Airport:
.long entlist_Airport_0
.long entlist_Airport_1
.long entlist_Airport_2
.long entlist_Airport_3

entlist_Cave_0:
.long entities_cave_stage_1_room_0
.long entities_cave_stage_1_room_1
.long entities_cave_stage_1_room_2
.long entities_cave_stage_1_room_3

entlist_Cave_1:
.long entities_cave_stage_2_room_0
.long entities_cave_stage_2_room_1
.long entities_cave_stage_2_room_2
.long entities_cave_stage_2_room_3
.long entities_cave_stage_2_room_4

entlist_Cave_2:
.long entities_cave_stage_3_room_0
.long entities_cave_stage_3_room_1
.long entities_cave_stage_3_room_2
.long entities_cave_stage_3_room_3
.long entities_cave_stage_3_room_4

entlist_Cave_3:
.long entities_cave_stage_4_room_0

entlist_Cave:
.long entlist_Cave_0
.long entlist_Cave_1
.long entlist_Cave_2
.long entlist_Cave_3

entlist_Base_0:
.long entities_base_stage_1_room_0

entlist_Base_1:
.long entities_base_stage_2_room_0

entlist_Base_2:
.long entities_base_stage_3_room_0

entlist_Base:
.long entlist_Base_0
.long entlist_Base_1
.long entlist_Base_2

entlist_Launch_0:
.long entities_launch_stage_1_room_0

entlist_Launch:
.long entlist_Launch_0


.globl levelEntities
levelEntities:
.long entlist_Bank
.long entlist_Harbor
.long entlist_Airport
.long entlist_Cave
.long entlist_Base
.long entlist_Launch

