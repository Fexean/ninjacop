.include "asm/macros.inc"

.globl levelintro_bank_tiles1
levelintro_bank_tiles1:
.incbin "build/gfx/level_intro/bank1.4bpp.lz"

.globl levelintro_bank_tiles2
levelintro_bank_tiles2:
.incbin "build/gfx/level_intro/bank2.4bpp.lz"

.globl levelintro_bank_tiles3
levelintro_bank_tiles3:
.incbin "build/gfx/level_intro/bank3.4bpp.lz"

.globl levelintro_bank_tiles4
levelintro_bank_tiles4:
.incbin "build/gfx/level_intro/bank4.4bpp.lz"

.globl levelintro_bank_map1
levelintro_bank_map1:
.incbin "build/gfx/level_intro/bank1.tilemap.lz"
.fill 0x1EC

.globl levelintro_bank_pal
levelintro_bank_pal:
.incbin "build/gfx/level_intro/bank.gbapal"

.globl levelintro_bank_map2
levelintro_bank_map2:
.incbin "build/gfx/level_intro/bank2.tilemap.lz"
.fill 0x1F0

.globl levelintro_bank_map3
levelintro_bank_map3:
.incbin "build/gfx/level_intro/bank3.tilemap.lz"
.fill 0x1CC

.globl levelintro_bank_map4
levelintro_bank_map4:
.incbin "build/gfx/level_intro/bank4.tilemap.lz"
.fill 0x1E4

.globl levelintro_harbor_map1
levelintro_harbor_map1:
.incbin "build/gfx/level_intro/harbor1.tilemap.lz"
.fill 488

.globl levelintro_harbor_tiles1
levelintro_harbor_tiles1:
.incbin "build/gfx/level_intro/harbor1.4bpp.lz"

.globl levelintro_harbor_pal
levelintro_harbor_pal:
.incbin "build/gfx/level_intro/harbor.gbapal"

.globl levelintro_harbor_map2
levelintro_harbor_map2:
.incbin "build/gfx/level_intro/harbor2.tilemap.lz"
.fill 488

.globl levelintro_harbor_tiles2
levelintro_harbor_tiles2:
.incbin "build/gfx/level_intro/harbor2.4bpp.lz"

.globl levelintro_harbor_map3
levelintro_harbor_map3:
.incbin "build/gfx/level_intro/harbor3.tilemap.lz"
.fill 456

.globl levelintro_harbor_tiles3
levelintro_harbor_tiles3:
.incbin "build/gfx/level_intro/harbor3.4bpp.lz"

.globl levelintro_harbor_map4
levelintro_harbor_map4:
.incbin "build/gfx/level_intro/harbor4.tilemap.lz"
.fill 488

.globl levelintro_harbor_tiles4
levelintro_harbor_tiles4:
.incbin "build/gfx/level_intro/harbor4.4bpp.lz"

.globl levelintro_airport_tiles1
levelintro_airport_tiles1:
.incbin "build/gfx/level_intro/airport1_1.4bpp.lz"

.globl levelintro_airport_tiles2
levelintro_airport_tiles2:
.incbin "build/gfx/level_intro/airport1_2.4bpp.lz"

.globl levelintro_airport_tiles3
levelintro_airport_tiles3:
.incbin "build/gfx/level_intro/airport1_3.4bpp.lz"

.globl levelintro_airport_tiles4
levelintro_airport_tiles4:
.incbin "build/gfx/level_intro/airport1_4.4bpp.lz"

.globl levelintro_airport_map1
levelintro_airport_map1:
.incbin "build/gfx/level_intro/airport1_1.tilemap.lz"
.fill 484

.globl levelintro_airport_pal
levelintro_airport_pal:
.incbin "build/gfx/level_intro/airport1.gbapal"

.globl levelintro_airport_map2
levelintro_airport_map2:
.incbin "build/gfx/level_intro/airport1_2.tilemap.lz"
.fill 492

.globl levelintro_airport_map3
levelintro_airport_map3:
.incbin "build/gfx/level_intro/airport1_3.tilemap.lz"
.fill 488

.globl levelintro_airport_map4
levelintro_airport_map4:
.incbin "build/gfx/level_intro/airport1_4.tilemap.lz"
.fill 488

.globl levelintro_airport2_tiles1
levelintro_airport2_tiles1:
.incbin "build/gfx/level_intro/airport2_1.4bpp.lz"

.globl levelintro_airport2_tiles2
levelintro_airport2_tiles2:
.incbin "build/gfx/level_intro/airport2_2.4bpp.lz"

.globl levelintro_airport2_tiles3
levelintro_airport2_tiles3:
.incbin "build/gfx/level_intro/airport2_3.4bpp.lz"

.globl levelintro_airport2_tiles4
levelintro_airport2_tiles4:
.incbin "build/gfx/level_intro/airport2_4.4bpp.lz"

.globl levelintro_airport2_map1
levelintro_airport2_map1:
.incbin "build/gfx/level_intro/airport2_1.tilemap.lz"
.fill 484

.globl levelintro_airport2_pal
levelintro_airport2_pal:
.incbin "build/gfx/level_intro/airport2.gbapal"

.globl levelintro_airport2_map2
levelintro_airport2_map2:
.incbin "build/gfx/level_intro/airport2_2.tilemap.lz"
.fill 412

.globl levelintro_airport2_map3
levelintro_airport2_map3:
.incbin "build/gfx/level_intro/airport2_3.tilemap.lz"
.fill 480

.globl levelintro_airport2_map4
levelintro_airport2_map4:
.incbin "build/gfx/level_intro/airport2_4.tilemap.lz"
.fill 412

.globl levelintro_cave_map1
levelintro_cave_map1:
.incbin "build/gfx/level_intro/cave1.tilemap.lz"
.fill 484

.globl levelintro_cave_tiles1
levelintro_cave_tiles1:
.incbin "build/gfx/level_intro/cave1.4bpp.lz"

.globl levelintro_cave_pal
levelintro_cave_pal:
.incbin "build/gfx/level_intro/cave.gbapal"

.globl levelintro_cave_map2
levelintro_cave_map2:
.incbin "build/gfx/level_intro/cave2.tilemap.lz"
.fill 492

.globl levelintro_cave_tiles2
levelintro_cave_tiles2:
.incbin "build/gfx/level_intro/cave2.4bpp.lz"

.globl levelintro_cave_map3
levelintro_cave_map3:
.incbin "build/gfx/level_intro/cave3.tilemap.lz"
.fill 492

.globl levelintro_cave_tiles3
levelintro_cave_tiles3:
.incbin "build/gfx/level_intro/cave3.4bpp.lz"

.globl levelintro_cave_map4
levelintro_cave_map4:
.incbin "build/gfx/level_intro/cave4.tilemap.lz"
.fill 488

.globl levelintro_cave_tiles4
levelintro_cave_tiles4:
.incbin "build/gfx/level_intro/cave4.4bpp.lz"

.globl levelintro_base_map2
levelintro_base_map2:
.incbin "build/gfx/level_intro/base2.tilemap.lz"
.fill 460

.globl levelintro_base_tiles2
levelintro_base_tiles2:
.incbin "build/gfx/level_intro/base2.4bpp.lz"

.globl levelintro_base_pal
levelintro_base_pal:
.incbin "build/gfx/level_intro/base.gbapal"

.globl levelintro_base_map3
levelintro_base_map3:
.incbin "build/gfx/level_intro/base3.tilemap.lz"
.fill 484

.globl levelintro_base_tiles3
levelintro_base_tiles3:
.incbin "build/gfx/level_intro/base3.4bpp.lz"

.globl levelintro_base_map1
levelintro_base_map1:
.incbin "build/gfx/level_intro/base1.tilemap.lz"
.fill 472

.globl levelintro_base_tiles1
levelintro_base_tiles1:
.incbin "build/gfx/level_intro/base1.4bpp.lz"

.globl levelintro_base_map4
levelintro_base_map4:
.incbin "build/gfx/level_intro/base4.tilemap.lz"
.fill 456

.globl levelintro_base_tiles4
levelintro_base_tiles4:
.incbin "build/gfx/level_intro/base4.4bpp.lz"

.globl levelintro_launch_pal
levelintro_launch_pal:
.incbin "build/gfx/level_intro/launch.gbapal"

.globl levelintro_launch_map2
levelintro_launch_map2:
.incbin "build/gfx/level_intro/launch2.tilemap.lz"
.fill 476

.globl levelintro_launch_tiles2
levelintro_launch_tiles2:
.incbin "build/gfx/level_intro/launch2.4bpp.lz"

.globl levelintro_launch_map3
levelintro_launch_map3:
.incbin "build/gfx/level_intro/launch3.tilemap.lz"
.fill 440

.globl levelintro_launch_tiles3
levelintro_launch_tiles3:
.incbin "build/gfx/level_intro/launch3.4bpp.lz"

.globl levelintro_launch_map4
levelintro_launch_map4:
.incbin "build/gfx/level_intro/launch4.tilemap.lz"
.fill 304

.globl levelintro_launch_tiles4
levelintro_launch_tiles4:
.incbin "build/gfx/level_intro/launch4.4bpp.lz"

.globl levelintro_launch_map1
levelintro_launch_map1:
.incbin "build/gfx/level_intro/launch1.tilemap.lz"
.fill 468

.globl levelintro_launch_tiles1
levelintro_launch_tiles1:
.incbin "build/gfx/level_intro/launch1.4bpp.lz"

.globl unk_81f0224
unk_81f0224:
.incbin "build/gfx/level_intro/launch4_2.tilemap.lz"
.fill 304

.globl unk_81f048c
unk_81f048c:
.incbin "build/gfx/level_intro/launch4_2.4bpp.lz"

.globl levelintro_scrollMap
levelintro_scrollMap:
.incbin "build/gfx/level_intro/scroll.tilemap.lz"
.fill 0xCC

.globl levelintro_borderTiles
levelintro_borderTiles:
.incbin "build/gfx/level_intro/scroll.4bpp.lz"

.globl levelintro_borderPal
levelintro_borderPal:
.incbin "build/gfx/level_intro/scroll.gbapal"


@ Unreferenced sprite?
.long 0x4, 0x0, 0x0, 0x0, 0x28000088, 0x1f, 0x0
.long 0x20000000, 0x1f, 0x4, 0xe0000048, 0x1f, 0x24, 0xe0000008, 0x1f


.include "gfx/level_intro/scroll_anim/scrollAnim_0.spr"
.include "gfx/level_intro/scroll_anim/scrollAnim_1.spr"
.include "gfx/level_intro/scroll_anim/scrollAnim_2.spr"
.include "gfx/level_intro/scroll_anim/scrollAnim_3.spr"
.include "gfx/level_intro/scroll_anim/scrollAnim_4.spr"
.include "gfx/level_intro/scroll_anim/scrollAnim_5.spr"


.globl unk_81F120C
unk_81F120C:
.incbin "build/gfx/level_intro/scroll2.4bpp.lz"


.include "gfx/level_intro/scroll_anim/scrollAnim.anim"


.globl unused_ending_ninja_face_map
unused_ending_ninja_face_map:
.incbin "build/gfx/unknown/unused_ninja_face.tilemap.lz"
.fill 804

.globl unused_ending_ninja_face_tiles
unused_ending_ninja_face_tiles:
.incbin "build/gfx/unknown/unused_ninja_face.4bpp.lz"

.globl ending_ninja_face_pal
ending_ninja_face_pal:
.incbin "build/gfx/unknown/unused_ninja_face.gbapal"


.include "gfx/level_intro/slice_anim/sliceAnim_0.spr"
.include "gfx/level_intro/slice_anim/sliceAnim_1.spr"
.include "gfx/level_intro/slice_anim/sliceAnim_2.spr"
.include "gfx/level_intro/slice_anim/sliceAnim_3.spr"
.include "gfx/level_intro/slice_anim/sliceAnim_4.spr"
.include "gfx/level_intro/slice_anim/sliceAnim_5.spr"
.include "gfx/level_intro/slice_anim/sliceAnim_6.spr"
.include "gfx/level_intro/slice_anim/sliceAnim_7.spr"
.include "gfx/level_intro/slice_anim/sliceAnim_8.spr"
.include "gfx/level_intro/slice_anim/sliceAnim_9.spr"
.fill 0xC

.globl levelintro_sliceEffectTiles
levelintro_sliceEffectTiles:
.incbin "build/gfx/level_intro/slice_anim.4bpp.lz"

.include "gfx/level_intro/slice_anim/sliceAnim.anim"


.globl unusedFace0Map
unusedFace0Map:
.incbin "build/gfx/unknown/unused_face_0.tilemap.lz"
.fill 292

.globl unusedFace0Tiles
unusedFace0Tiles:
.incbin "build/gfx/unknown/unused_face_0.4bpp.lz"

.globl unusedFacePal
unusedFacePal:
.incbin "build/gfx/unknown/unused_face.gbapal"

.globl unusedFace1Map
unusedFace1Map:
.incbin "build/gfx/unknown/unused_face_1.tilemap.lz"
.fill 276

.globl unusedFace1Tiles
unusedFace1Tiles:
.incbin "build/gfx/unknown/unused_face_1.4bpp.lz"

.globl unusedFace2Map
unusedFace2Map:
.incbin "build/gfx/unknown/unused_face_2.tilemap.lz"
.fill 300

.globl unusedFace2Tiles
unusedFace2Tiles:
.incbin "build/gfx/unknown/unused_face_2.4bpp.lz"

.globl unusedFace3Map
unusedFace3Map:
.incbin "build/gfx/unknown/unused_face_3.tilemap.lz"
.fill 280

.globl unusedFace3Tiles
unusedFace3Tiles:
.incbin "build/gfx/unknown/unused_face_3.4bpp.lz"


.globl bankCompletionMap
bankCompletionMap:
.incbin "build/gfx/level_intro/bank_completion.tilemap.lz"
.fill 1332

.globl bankCompletionTiles
bankCompletionTiles:
.incbin "build/gfx/level_intro/bank_completion.4bpp.lz"

.globl levelCompletionPal
levelCompletionPal:
.incbin "build/gfx/level_intro/level_completion.gbapal"

.globl harborCompletionMap
harborCompletionMap:
.incbin "build/gfx/level_intro/harbor_completion.tilemap.lz"
.fill 1356

.globl harborCompletionTiles
harborCompletionTiles:
.incbin "build/gfx/level_intro/harbor_completion.4bpp.lz"

.globl airportCompletionMap
airportCompletionMap:
.incbin "build/gfx/level_intro/airport_completion.tilemap.lz"
.fill 1284


.globl airportCompletionTiles
airportCompletionTiles:
.incbin "build/gfx/level_intro/airport_completion.4bpp.lz"

.globl caveCompletionMap
caveCompletionMap:
.incbin "build/gfx/level_intro/cave_completion.tilemap.lz"
.fill 1272


.globl caveCompletionTiles
caveCompletionTiles:
.incbin "build/gfx/level_intro/cave_completion.4bpp.lz"

.include "gfx/level_intro/mission_complete_anim/missionCompleteAnim_0.spr"
.include "gfx/level_intro/mission_complete_anim/missionCompleteAnim_1.spr"
.include "gfx/level_intro/mission_complete_anim/missionCompleteAnim_2.spr"
.include "gfx/level_intro/mission_complete_anim/missionCompleteAnim_3.spr"
.include "gfx/level_intro/mission_complete_anim/missionCompleteAnim_4.spr"
.include "gfx/level_intro/mission_complete_anim/missionCompleteAnim_5.spr"

.globl missionCompleteTiles
missionCompleteTiles:
.incbin "build/gfx/level_intro/mission_complete.4bpp.lz"

.include "gfx/level_intro/mission_complete_anim/missionCompleteAnim.anim"


.globl missionCompleteAnim_de_0
missionCompleteAnim_de_0:
.long 0x0, 0x0, 0x0

.globl missionCompleteAnim_de_1
missionCompleteAnim_de_1:
.long 0x5, 0x0, 0x0
.long 0xe00000, 0x9000003e, 0x1f, 0xc00008, 0xa000003e, 0x1f, 0x800018, 0xb000003e, 0x1f, 0x400038, 0xb000003e, 0x1f, 0x58, 0xb000003e, 0x1f

.globl missionCompleteAnim_de_2
missionCompleteAnim_de_2:
.long 0x5, 0x0, 0x0
.long 0xe00000, 0x90000041, 0x1f, 0xc00008, 0xa0000041, 0x1f, 0x800018, 0xb0000041, 0x1f, 0x400038, 0xb0000041, 0x1f, 0x58, 0xb0000041, 0x1f

.globl missionCompleteAnim_de_3
missionCompleteAnim_de_3:
.long 0x5, 0x0, 0x0
.long 0xe00000, 0x9000003f, 0x1f, 0xc00008, 0xa000003f, 0x1f, 0x800018, 0xb000003f, 0x1f, 0x400038, 0xb000003f, 0x1f, 0x58, 0xb000003f, 0x1f

.globl missionCompleteAnim_de_4
missionCompleteAnim_de_4:
.long 0x5, 0x0, 0x0
.long 0xe00000, 0x90000041, 0x1f, 0xc00008, 0xa0000041, 0x1f, 0x800018, 0xb0000041, 0x1f, 0x400038, 0xb0000041, 0x1f, 0x58, 0xb0000041, 0x1f

.globl missionCompleteAnim_de_5
missionCompleteAnim_de_5:
.long 0x5, 0x0, 0x0, 0xe00000, 0x90000040, 0x1f, 0xc00008, 0xa0000040, 0x1f, 0x800018, 0xb0000040, 0x1f, 0x400038, 0xb0000040, 0x1f, 0x58
.long 0xb0000040, 0x1f

@ Unreferenced
.globl missionCompleteTiles_de
missionCompleteTiles_de:
.incbin "build/gfx/level_intro/mission_complete_de.4bpp.lz"

@ Unreferenced
.globl missionCompleteAnim_de
missionCompleteAnim_de:
.long missionCompleteAnim_de_0, 0x2, 0x0
.long missionCompleteAnim_de_1, 0x1, 0x0
.long missionCompleteAnim_de_2, 0x1, 0x0
.long missionCompleteAnim_de_3, 0x1, 0x0
.long missionCompleteAnim_de_4, 0x2, 0x0
.long missionCompleteAnim_de_5, 0x2, 0x0
.long 0x0, 0xffff, 0x0


.globl missionCompleteAnim_fr_0
missionCompleteAnim_fr_0:
.long 0x0, 0x0, 0x0


.globl missionCompleteAnim_fr_1
missionCompleteAnim_fr_1:
.long 0x5, 0x0, 0x0
.long 0xe00000, 0x9000003e, 0x1f, 0xc00008, 0xa000003e, 0x1f, 0x800018, 0xb000003e, 0x1f, 0x400038, 0xb000003e, 0x1f, 0x58, 0xb000003e, 0x1f


.globl missionCompleteAnim_fr_2
missionCompleteAnim_fr_2:
.long 0x5, 0x0, 0x0
.long 0xe00000, 0x90000041, 0x1f, 0xc00008, 0xa0000041, 0x1f, 0x800018, 0xb0000041, 0x1f, 0x400038, 0xb0000041, 0x1f, 0x58, 0xb0000041, 0x1f


.globl missionCompleteAnim_fr_3
missionCompleteAnim_fr_3:
.long 0x5, 0x0, 0x0
.long 0xe00000, 0x9000003f, 0x1f, 0xc00008, 0xa000003f, 0x1f, 0x800018, 0xb000003f, 0x1f, 0x400038, 0xb000003f, 0x1f, 0x58, 0xb000003f, 0x1f


.globl missionCompleteAnim_fr_4
missionCompleteAnim_fr_4:
.long 0x5, 0x0, 0x0
.long 0xe00000, 0x90000041, 0x1f, 0xc00008, 0xa0000041, 0x1f, 0x800018, 0xb0000041, 0x1f, 0x400038, 0xb0000041, 0x1f, 0x58, 0xb0000041, 0x1f


.globl missionCompleteAnim_fr_5
missionCompleteAnim_fr_5:
.long 0x5, 0x0, 0x0, 0xe00000, 0x90000040, 0x1f, 0xc00008, 0xa0000040, 0x1f, 0x800018, 0xb0000040, 0x1f, 0x400038, 0xb0000040, 0x1f, 0x58
.long 0xb0000040, 0x1f


@ Unreferenced
.globl missionCompleteTiles_fr
missionCompleteTiles_fr:
.incbin "build/gfx/level_intro/mission_complete_fr.4bpp.lz"

.globl missionCompleteAnim_fr
missionCompleteAnim_fr:
.long missionCompleteAnim_fr_0, 0x2, 0x0
.long missionCompleteAnim_fr_1, 0x1, 0x0
.long missionCompleteAnim_fr_2, 0x1, 0x0
.long missionCompleteAnim_fr_3, 0x1, 0x0
.long missionCompleteAnim_fr_4, 0x2, 0x0
.long missionCompleteAnim_fr_5, 0x2, 0x0, 0x0, 0xffff, 0x0


.globl levelintro_bank
levelintro_bank:
.long levelintro_borderTiles
.long levelintro_borderPal
.long levelintro_bank_tiles1
.long levelintro_bank_tiles2
.long levelintro_bank_tiles3
.long levelintro_bank_tiles4, 0x1b001b00, 0x1b001b00
.long levelintro_bank_pal
.long levelintro_scrollMap
.long levelintro_bank_map1
.long levelintro_bank_map2
.long levelintro_bank_map3
.long levelintro_bank_map4


.globl levelintro_harbor
levelintro_harbor:
.long levelintro_borderTiles
.long levelintro_borderPal
.long levelintro_harbor_tiles1
.long levelintro_harbor_tiles2
.long levelintro_harbor_tiles3
.long levelintro_harbor_tiles4, 0x1b001b00, 0x1b001b00
.long levelintro_harbor_pal
.long levelintro_scrollMap
.long levelintro_harbor_map1
.long levelintro_harbor_map2
.long levelintro_harbor_map3
.long levelintro_harbor_map4


.globl levelintro_airport
levelintro_airport:
.long levelintro_borderTiles
.long levelintro_borderPal
.long levelintro_airport_tiles1
.long levelintro_airport_tiles2
.long levelintro_airport_tiles3
.long levelintro_airport_tiles4, 0x1b001b00, 0x1b001b00
.long levelintro_airport_pal
.long levelintro_scrollMap
.long levelintro_airport_map1
.long levelintro_airport_map2
.long levelintro_airport_map3
.long levelintro_airport_map4


.globl levelintro_airport2
levelintro_airport2:
.long levelintro_borderTiles
.long levelintro_borderPal
.long levelintro_airport2_tiles1
.long levelintro_airport2_tiles2
.long levelintro_airport2_tiles3
.long levelintro_airport2_tiles4, 0x1b001b00, 0x1b001b00
.long levelintro_airport2_pal
.long levelintro_scrollMap
.long levelintro_airport2_map1
.long levelintro_airport2_map2
.long levelintro_airport2_map3
.long levelintro_airport2_map4


.globl levelintro_cave
levelintro_cave:
.long levelintro_borderTiles
.long levelintro_borderPal
.long levelintro_cave_tiles1
.long levelintro_cave_tiles2
.long levelintro_cave_tiles3
.long levelintro_cave_tiles4, 0x1b001b00, 0x1b001b00
.long levelintro_cave_pal
.long levelintro_scrollMap
.long levelintro_cave_map1
.long levelintro_cave_map2
.long levelintro_cave_map3
.long levelintro_cave_map4


.globl levelintro_base
levelintro_base:
.long levelintro_borderTiles
.long levelintro_borderPal
.long levelintro_base_tiles1
.long levelintro_base_tiles2
.long levelintro_base_tiles3
.long levelintro_base_tiles4, 0x1b001b00, 0x1b001b00
.long levelintro_base_pal
.long levelintro_scrollMap
.long levelintro_base_map1
.long levelintro_base_map2
.long levelintro_base_map3
.long levelintro_base_map4


.globl levelintro_launch
levelintro_launch:
.long levelintro_borderTiles
.long levelintro_borderPal
.long levelintro_launch_tiles1
.long levelintro_launch_tiles2
.long levelintro_launch_tiles3
.long levelintro_launch_tiles4, 0x1b001b00, 0x1b001b00
.long levelintro_launch_pal
.long levelintro_scrollMap
.long levelintro_launch_map1
.long levelintro_launch_map2
.long levelintro_launch_map3
.long levelintro_launch_map4


.globl levelintro_launch_clone
levelintro_launch_clone:
.long levelintro_borderTiles
.long levelintro_borderPal
.long levelintro_launch_tiles1
.long levelintro_launch_tiles2
.long levelintro_launch_tiles3
.long unk_81f048c, 0x1b001b00, 0x1b001b00
.long levelintro_launch_pal
.long levelintro_scrollMap
.long levelintro_launch_map1
.long levelintro_launch_map2
.long levelintro_launch_map3
.long unk_81f0224


.globl scrollIntroStructs
scrollIntroStructs:
.long levelintro_bank
.long levelintro_harbor
.long levelintro_airport
.long levelintro_cave
.long levelintro_base
.long levelintro_launch
.long levelintro_airport2
.long levelintro_launch_clone


.globl unusedFace0
unusedFace0:
.long unusedFace0Tiles
.long unusedFace0Map
.long unusedFacePal, 0x1005a, 0x0


.globl unusedFace1
unusedFace1:
.long unusedFace1Tiles
.long unusedFace1Map
.long unusedFacePal, 0x1005a, 0x0


.globl unusedFace2
unusedFace2:
.long unusedFace2Tiles
.long unusedFace2Map
.long unusedFacePal, 0x1005a, 0x0


.globl unusedFace3
unusedFace3:
.long unusedFace3Tiles
.long unusedFace3Map
.long unusedFacePal, 0x1005a, 0x0


.globl unusedFaces
unusedFaces:
.long unusedFace0
.long unusedFace1
.long unusedFace2
.long unusedFace3
.long unusedFace2
.long unusedFace2
.long 0x0


.globl levelCompletion_Bank
levelCompletion_Bank:
.long bankCompletionTiles
.long bankCompletionMap
.long levelCompletionPal, 0x50258, 0x0


.globl levelCompletion_Harbor
levelCompletion_Harbor:
.long harborCompletionTiles
.long harborCompletionMap
.long levelCompletionPal, 0x50258, 0x0


.globl levelCompletion_Airport
levelCompletion_Airport:
.long airportCompletionTiles
.long airportCompletionMap
.long levelCompletionPal, 0x50258, 0x0


.globl levelCompletion_Cave
levelCompletion_Cave:
.long caveCompletionTiles
.long caveCompletionMap
.long levelCompletionPal, 0x50258, 0x0


.globl levelCompletionImages
levelCompletionImages:
.long levelCompletion_Bank
.long levelCompletion_Harbor
.long levelCompletion_Airport
.long levelCompletion_Cave
.long 0x0
.long 0x0

