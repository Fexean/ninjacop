@ There are lots of chunks that decompress to only 0s for example dword_8068DD4 (maybe empty bg layers), a lot of these chunks are incbinned within other chunks and not labeled like they should be.
@ They are unreferenced so including them should not create problems.
@ There is also dummy2.bin which is similar but used in airport stages




@These contain palette animation data and incbins of tilemaps, collisionmaps, tilesets, palettes
.include "data/maps/rooms/Bank/chunks.s"
.include "data/maps/rooms/Harbor/chunks.s"
.include "data/maps/rooms/Airport/chunks.s"
.include "data/maps/rooms/Cave/chunks.s"
.include "data/maps/rooms/Base/chunks.s"
.include "data/maps/rooms/Launch/chunks.s"



.globl dword_80D6184
dword_80D6184:
.long 0x182000f, 0x1e30003, 0x2430003, 0x304001e, 0x2430003, 0x1e30003, 0x182000f, 0x0


.globl off_80D61A4
off_80D61A4:
.long dword_80D6184, 0x37, 0x0, 0x0


.globl dword_80D61B4
dword_80D61B4:
.long 0x2, 0x60002, 0xc0002, 0x120002, 0x180002, 0x1e0002, 0xbf0002, 0x15f0002, 0x1ff0002, 0x29f0002, 0x33f0002, 0x29f0002, 0x1ff0002, 0x15f0002, 0xbf0002, 0x1e0002
.long 0x180002, 0x120002, 0xc0002, 0x60002, 0x0


.globl unk_80d6208
unk_80d6208:
.long 0x2, 0x40002, 0x80002, 0xc0002, 0x100002, 0x140002, 0x180002, 0x1c0002, 0x1f0002, 0x9f0002, 0x11f0002, 0x9f0002, 0x1f0002, 0x1c0002, 0x180002, 0x140002
.long 0x100002, 0xc0002, 0x80002, 0x40002, 0x0


.globl off_80D625C
off_80D625C:
.long dword_80D61B4, 0x71
.long unk_80d6208, 0x72, 0x0, 0x0


.globl dword_80D6274
dword_80D6274:
.long 0x29f0001, 0x1ff0001, 0x15f0001, 0xbf0001, 0x1e0001, 0x180001, 0x120001, 0xc0001, 0x60001, 0x1, 0x60001, 0xc0001, 0x120001, 0x180001, 0x1e0001, 0xbf0001
.long 0x15f0001, 0x1ff0001, 0x29f0001, 0x33f0001, 0x29f0001, 0x1ff0001, 0x15f0001, 0xbf0001, 0x1e0001, 0x180001, 0x120001, 0xc0001, 0x60001, 0x1, 0x60001, 0xc0001
.long 0x120001, 0x180001, 0x1e0001, 0xbf0001, 0x15f0001, 0x1ff0001, 0x29f0001, 0x33f0001, 0xffff, 0x0


.globl unk_80d631c
unk_80d631c:
.long 0x9f0001, 0x1f0001, 0x1c0001, 0x180001, 0x140001, 0x100001, 0xc0001, 0x80001, 0x40001, 0x1, 0x40001, 0x80001, 0xc0001, 0x100001, 0x140001, 0x180001
.long 0x1c0001, 0x1f0001, 0x9f0001, 0x11f0001, 0x9f0001, 0x1f0001, 0x1c0001, 0x180001, 0x140001, 0x100001, 0xc0001, 0x80001, 0x40001, 0x1, 0x40001, 0x80001
.long 0xc0001, 0x100001, 0x140001, 0x180001, 0x1c0001, 0x1f0001, 0x9f0001, 0x11f0001, 0xffff, 0x0


.globl off_80D63C4
off_80D63C4:
.long dword_80D6274, 0x71
.long unk_80d631c, 0x72, 0x0, 0x0




.globl field8_bank_s0_r0
field8_bank_s0_r0:
.long 0x400001, 0x20000100, 0x1000020, 0x100
.long BANK_STAGE1_ROOM0_PARALLAX_CHUNK_0, 0x0, 0x0, 0x0


.include "data/maps/rooms/Bank/Stage_1/Room_0/map.inc"
.include "data/maps/rooms/Bank/Stage_1/Room_1/map.inc"
.include "data/maps/rooms/Bank/Stage_1/Room_2/map.inc"
.include "data/maps/rooms/Bank/Stage_2/Room_0/map.inc"
.include "data/maps/rooms/Bank/Stage_2/Room_1/map.inc"
.include "data/maps/rooms/Bank/Stage_2/Room_2/map.inc"
.include "data/maps/rooms/Bank/Stage_2/Room_3/map.inc"
.include "data/maps/rooms/Bank/Stage_2/Room_4/map.inc"
.include "data/maps/rooms/Bank/Stage_3/Room_0/map.inc"
.include "data/maps/rooms/Bank/Stage_3/Room_1/map.inc"
.include "data/maps/rooms/Bank/Stage_3/Room_2/map.inc"
.include "data/maps/rooms/Bank/Stage_3/Room_3/map.inc"
.include "data/maps/rooms/Bank/Stage_3/Room_4/map.inc"


.globl BANK_STAGE4_ROOM0_PARALLAX
BANK_STAGE4_ROOM0_PARALLAX:
.long 0x400001, 0x20000100, 0x1000020, 0x100
.long BANK_STAGE4_ROOM0_PARALLAX_CHUNK_0, 0x0, 0x0, 0x0


.include "data/maps/rooms/Bank/Stage_4/Room_0/map.inc"


.globl BANK_STAGE1_HEADERS
BANK_STAGE1_HEADERS:
.long BANK_STAGE1_ROOM0_HEADER
.long BANK_STAGE1_ROOM1_HEADER
.long BANK_STAGE1_ROOM2_HEADER


.globl BANK_STAGE2_HEADERS
BANK_STAGE2_HEADERS:
.long BANK_STAGE2_ROOM0_HEADER
.long BANK_STAGE2_ROOM1_HEADER
.long BANK_STAGE2_ROOM2_HEADER
.long BANK_STAGE2_ROOM3_HEADER
.long BANK_STAGE2_ROOM4_HEADER


.globl BANK_STAGE3_HEADERS
BANK_STAGE3_HEADERS:
.long BANK_STAGE3_ROOM0_HEADER
.long BANK_STAGE3_ROOM1_HEADER
.long BANK_STAGE3_ROOM2_HEADER
.long BANK_STAGE3_ROOM3_HEADER
.long BANK_STAGE3_ROOM4_HEADER


.globl BANK_STAGE4_HEADERS
BANK_STAGE4_HEADERS:
.long BANK_STAGE4_ROOM0_HEADER


.globl mapList_bank
mapList_bank:
.long BANK_STAGE1_HEADERS
.long BANK_STAGE2_HEADERS
.long BANK_STAGE3_HEADERS
.long BANK_STAGE4_HEADERS

.include "data/maps/rooms/Harbor/Stage_1/Room_0/map.inc"
.include "data/maps/rooms/Harbor/Stage_1/Room_1/map.inc"
.include "data/maps/rooms/Harbor/Stage_1/Room_2/map.inc"
.include "data/maps/rooms/Harbor/Stage_1/Room_3/map.inc"
.include "data/maps/rooms/Harbor/Stage_1/Room_4/map.inc"

.globl HARBOR_STAGE1_HEADERS
HARBOR_STAGE1_HEADERS:
.long HARBOR_STAGE1_ROOM0_HEADER
.long HARBOR_STAGE1_ROOM1_HEADER
.long HARBOR_STAGE1_ROOM2_HEADER
.long HARBOR_STAGE1_ROOM3_HEADER
.long HARBOR_STAGE1_ROOM4_HEADER

.include "data/maps/rooms/Harbor/Stage_2/Room_0/map.inc"
.include "data/maps/rooms/Harbor/Stage_2/Room_1/map.inc"
.include "data/maps/rooms/Harbor/Stage_2/Room_2/map.inc"
.include "data/maps/rooms/Harbor/Stage_2/Room_3/map.inc"


.globl HARBOR_STAGE2_HEADERS
HARBOR_STAGE2_HEADERS:
.long HARBOR_STAGE2_ROOM0_HEADER
.long HARBOR_STAGE2_ROOM1_HEADER
.long HARBOR_STAGE2_ROOM2_HEADER
.long HARBOR_STAGE2_ROOM3_HEADER

.include "data/maps/rooms/Harbor/Stage_3/Room_0/map.inc"
.include "data/maps/rooms/Harbor/Stage_3/Room_1/map.inc"
.include "data/maps/rooms/Harbor/Stage_3/Room_2/map.inc"
.include "data/maps/rooms/Harbor/Stage_3/Room_3/map.inc"
.include "data/maps/rooms/Harbor/Stage_3/Room_4/map.inc"

.globl HARBOR_STAGE3_HEADERS
HARBOR_STAGE3_HEADERS:
.long HARBOR_STAGE3_ROOM0_HEADER
.long HARBOR_STAGE3_ROOM1_HEADER
.long HARBOR_STAGE3_ROOM2_HEADER
.long HARBOR_STAGE3_ROOM3_HEADER
.long HARBOR_STAGE3_ROOM4_HEADER

.include "data/maps/rooms/Harbor/Stage_4/Room_0/map.inc"


.globl harbor_boss
HARBOR_STAGE4_HEADERS:
.long HARBOR_STAGE4_ROOM0_HEADER


.globl mapList_harbor
mapList_harbor:
.long HARBOR_STAGE1_HEADERS
.long HARBOR_STAGE2_HEADERS
.long HARBOR_STAGE3_HEADERS
.long HARBOR_STAGE4_HEADERS


.globl AIRPORT_STAGE1_ROOM0_PARALLAX
AIRPORT_STAGE1_ROOM0_PARALLAX:
.long 0x800001, 0x20000050, 0x1000020, 0x100
.long AIRPORT_STAGE1_ROOM0_PARALLAX_CHUNK_0, 0x0, 0x0, 0x0

.include "data/maps/rooms/Airport/Stage_1/Room_0/map.inc"
.include "data/maps/rooms/Airport/Stage_1/Room_1/map.inc"
.include "data/maps/rooms/Airport/Stage_1/Room_2/map.inc"


.globl AIRPORT_STAGE1_ROOM3_PARALLAX
AIRPORT_STAGE1_ROOM3_PARALLAX:
.long 0x800001, 0x20000050, 0x1000020, 0x100
.long AIRPORT_STAGE1_ROOM3_PARALLAX_CHUNK_0, 0x0, 0x0, 0x0

.include "data/maps/rooms/Airport/Stage_1/Room_3/map.inc"

.globl AIRPORT_STAGE1_HEADERS
AIRPORT_STAGE1_HEADERS:
.long AIRPORT_STAGE1_ROOM0_HEADER
.long AIRPORT_STAGE1_ROOM1_HEADER
.long AIRPORT_STAGE1_ROOM2_HEADER
.long AIRPORT_STAGE1_ROOM3_HEADER


.globl AIRPORT_STAGE2_ROOM0_PARALLAX
AIRPORT_STAGE2_ROOM0_PARALLAX:
.long 0x800001, 0x20000100, 0x1000020, 0x100
.long AIRPORT_STAGE2_ROOM0_PARALLAX_CHUNK_0, 0x0, 0x0, 0x0

.include "data/maps/rooms/Airport/Stage_2/Room_0/map.inc"


.globl AIRPORT_STAGE2_ROOM2_PARALLAX
AIRPORT_STAGE2_ROOM2_PARALLAX:
.long 0x800001, 0x20000000, 0x1000020, 0x100
.long AIRPORT_STAGE2_ROOM1_PARALLAX_CHUNK_0, 0x0, 0x0, 0x0

.include "data/maps/rooms/Airport/Stage_2/Room_2/map.inc"
.include "data/maps/rooms/Airport/Stage_2/Room_3/map.inc"


.globl AIRPORT_STAGE2_ROOM4_PARALLAX
AIRPORT_STAGE2_ROOM4_PARALLAX:
.long 0x400000, 0x20000100, 0x1000020, 0x100
.long AIRPORT_STAGE2_ROOM4_PARALLAX_CHUNK_0, 0x0, 0x0, 0x0

.include "data/maps/rooms/Airport/Stage_2/Room_4/map.inc"
.include "data/maps/rooms/Airport/Stage_2/Room_5/map.inc"
.include "data/maps/rooms/Airport/Stage_2/Room_6/map.inc"
.include "data/maps/rooms/Airport/Stage_2/Room_7/map.inc"


.globl AIRPORT_STAGE2_HEADERS
AIRPORT_STAGE2_HEADERS:
.long AIRPORT_STAGE2_ROOM0_HEADER
.long AIRPORT_STAGE2_ROOM2_HEADER
.long AIRPORT_STAGE2_ROOM2_HEADER
.long AIRPORT_STAGE2_ROOM3_HEADER
.long AIRPORT_STAGE2_ROOM4_HEADER
.long AIRPORT_STAGE2_ROOM5_HEADER
.long AIRPORT_STAGE2_ROOM6_HEADER
.long AIRPORT_STAGE2_ROOM7_HEADER


.globl AIRPORT_STAGE3_ROOM0_PARALLAX
AIRPORT_STAGE3_ROOM0_PARALLAX:
.long 0x4000002, 0x20000000, 0x1000020, 0x100
.long AIRPORT_STAGE3_ROOM0_PARALLAX_CHUNK_0, 0x0, 0x0, 0x0

.include "data/maps/rooms/Airport/Stage_3/Room_0/map.inc"
.include "data/maps/rooms/Airport/Stage_3/Room_1/map.inc"
.include "data/maps/rooms/Airport/Stage_3/Room_2/map.inc"
.include "data/maps/rooms/Airport/Stage_3/Room_3/map.inc"
.include "data/maps/rooms/Airport/Stage_3/Room_4/map.inc"
.include "data/maps/rooms/Airport/Stage_3/Room_5/map.inc"


.globl AIRPORT_STAGE3_HEADERS
AIRPORT_STAGE3_HEADERS:
.long AIRPORT_STAGE3_ROOM0_HEADER
.long AIRPORT_STAGE3_ROOM1_HEADER
.long AIRPORT_STAGE3_ROOM2_HEADER
.long AIRPORT_STAGE3_ROOM3_HEADER
.long AIRPORT_STAGE3_ROOM4_HEADER
.long AIRPORT_STAGE3_ROOM5_HEADER


.globl AIRPORT_STAGE4_ROOM0_PARALLAX
AIRPORT_STAGE4_ROOM0_PARALLAX:
.long 0x4000002, 0x20000000, 0x1000020, 0x100
.long AIRPORT_STAGE4_ROOM0_PARALLAX_CHUNK_0, 0x0, 0x0, 0x0

.include "data/maps/rooms/Airport/Stage_4/Room_0/map.inc"


.globl AIRPORT_STAGE4_HEADERS
AIRPORT_STAGE4_HEADERS:
.long AIRPORT_STAGE4_ROOM0_HEADER


.globl mapList_airport
mapList_airport:
.long AIRPORT_STAGE1_HEADERS
.long AIRPORT_STAGE2_HEADERS
.long AIRPORT_STAGE3_HEADERS
.long AIRPORT_STAGE4_HEADERS

.include "data/maps/rooms/Cave/Stage_1/Room_0/map.inc"
.include "data/maps/rooms/Cave/Stage_1/Room_1/map.inc"
.include "data/maps/rooms/Cave/Stage_1/Room_2/map.inc"
.include "data/maps/rooms/Cave/Stage_1/Room_3/map.inc"


.globl CAVE_STAGE1_HEADERS
CAVE_STAGE1_HEADERS:
.long CAVE_STAGE1_ROOM0_HEADER
.long CAVE_STAGE1_ROOM1_HEADER
.long CAVE_STAGE1_ROOM2_HEADER
.long CAVE_STAGE1_ROOM3_HEADER

.include "data/maps/rooms/Cave/Stage_2/Room_0/map.inc"
.include "data/maps/rooms/Cave/Stage_2/Room_1/map.inc"
.include "data/maps/rooms/Cave/Stage_2/Room_2/map.inc"
.include "data/maps/rooms/Cave/Stage_2/Room_3/map.inc"
.include "data/maps/rooms/Cave/Stage_2/Room_4/map.inc"


.globl CAVE_STAGE2_HEADERS
CAVE_STAGE2_HEADERS:
.long CAVE_STAGE2_ROOM0_HEADER
.long CAVE_STAGE2_ROOM1_HEADER
.long CAVE_STAGE2_ROOM2_HEADER
.long CAVE_STAGE2_ROOM3_HEADER
.long CAVE_STAGE2_ROOM4_HEADER

.include "data/maps/rooms/Cave/Stage_3/Room_0/map.inc"
.include "data/maps/rooms/Cave/Stage_3/Room_1/map.inc"
.include "data/maps/rooms/Cave/Stage_3/Room_2/map.inc"
.include "data/maps/rooms/Cave/Stage_3/Room_3/map.inc"
.include "data/maps/rooms/Cave/Stage_3/Room_4/map.inc"


.globl CAVE_STAGE3_HEADERS
CAVE_STAGE3_HEADERS:
.long CAVE_STAGE3_ROOM0_HEADER
.long CAVE_STAGE3_ROOM1_HEADER
.long CAVE_STAGE3_ROOM2_HEADER
.long CAVE_STAGE3_ROOM3_HEADER
.long CAVE_STAGE3_ROOM4_HEADER

.include "data/maps/rooms/Cave/Stage_4/Room_0/map.inc"

.globl CAVE_STAGE4_HEADERS
CAVE_STAGE4_HEADERS:
.long CAVE_STAGE4_ROOM0_HEADER


.globl mapList_cave
mapList_cave:
.long CAVE_STAGE1_HEADERS
.long CAVE_STAGE2_HEADERS
.long CAVE_STAGE3_HEADERS
.long CAVE_STAGE4_HEADERS


.globl BASE_STAGE1_ROOM0_PARALLAX
BASE_STAGE1_ROOM0_PARALLAX:
.long 0x800001, 0x200000c0, 0x1000020, 0x100
.long BASE_STAGE1_ROOM0_PARALLAX_CHUNK_0, 0x0, 0x0, 0x0

.include "data/maps/rooms/Base/Stage_1/Room_0/map.inc"


.globl BASE_STAGE1_HEADERS
BASE_STAGE1_HEADERS:
.long BASE_STAGE1_ROOM0_HEADER


.globl BASE_STAGE2_ROOM0_PARALLAX
BASE_STAGE2_ROOM0_PARALLAX:
.long 0x800001, 0x200000c0, 0x1000020, 0x100
.long BASE_STAGE2_ROOM0_PARALLAX_CHUNK_0, 0x0, 0x0, 0x0

.include "data/maps/rooms/Base/Stage_2/Room_0/map.inc"


.globl BASE_STAGE2_HEADERS
BASE_STAGE2_HEADERS:
.long BASE_STAGE2_ROOM0_HEADER

.include "data/maps/rooms/Base/Stage_3/Room_0/map.inc"


.globl BASE_STAGE3_HEADERS
BASE_STAGE3_HEADERS:
.long BASE_STAGE3_ROOM0_HEADER


.globl mapList_base
mapList_base:
.long BASE_STAGE1_HEADERS
.long BASE_STAGE2_HEADERS
.long BASE_STAGE3_HEADERS


.globl launchParallaxStruct
launchParallaxStruct:
.long 0x1000001, 0x20020080, 0x1000020, 0x200
.long LAUNCH_STAGE1_ROOM0_PARALLAX_CHUNK_0
.long LAUNCH_STAGE1_ROOM0_PARALLAX_CHUNK_1, 0x0, 0x0

.include "data/maps/rooms/Launch/Stage_1/Room_0/map.inc"


.globl LAUNCH_STAGE1_HEADERS
LAUNCH_STAGE1_HEADERS:
.long LAUNCH_STAGE1_ROOM0_HEADER


.globl mapList_launch
mapList_launch:
.long LAUNCH_STAGE1_HEADERS


.globl maps_DD828
maps_DD828:
.long mapList_bank
.long mapList_harbor
.long mapList_airport
.long mapList_cave
.long mapList_base
.long mapList_launch

