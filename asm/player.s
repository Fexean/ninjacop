.include "asm/macros.inc"


@needs work


thumb_func_global tryApplyConveyorMovement
tryApplyConveyorMovement: @ 0x08017104
	push {r4, r5, lr}
	add r1, r0, #0
	ldr r0, [r1, #4]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	ldr r1, [r1, #8]
	asr r1, r1, #8
	sub r1, #1
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl getCollision
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	add r0, r5, #0
	bl isCollisionConveyer
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _08017130
	mov r0, #0
	b _08017152
_08017130:
	ldr r0, _08017158
	ldr r4, [r0]
	ldr r3, _0801715C
	ldr r2, _08017160
	mov r1, #0xf
	and r1, r5
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r2, [r2, #0x11]
	add r0, r0, r2
	lsl r0, r0, #2
	add r0, r0, r3
	ldr r1, [r4, #0x30]
	ldr r0, [r0]
	add r1, r1, r0
	str r1, [r4, #0x30]
	mov r0, #1
_08017152:
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
_08017158: .4byte 0x03004750
_0801715C: .4byte dword_80F699C
_08017160: .4byte 0x03003D40



	thumb_func_global sub_8017164
sub_8017164: @ 0x08017164
	push {lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl getCollision
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	bl isCollisionSolid2
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	pop {r1}
	bx r1
	.align 2, 0



thumb_func_global sub_08017184
sub_08017184: @ 0x08017184
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	ldr r1, [r4, #4]
	lsl r1, r1, #8
	lsr r1, r1, #0x10
	ldrh r2, [r4, #8]
	bl sub_08012A08
	cmp r0, #0
	bne _080171A2
	mov r0, #0
	b _080171AA
_080171A2:
	ldr r0, [r4, #4]
	ldr r1, _080171B0
	and r0, r1
	mvn r0, r0
_080171AA:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
_080171B0: .4byte 0x000007FF



thumb_func_global sub_80171B4
sub_80171B4: @ 0x080171B4
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	ldr r1, [r4, #4]
	asr r1, r1, #8
	ldrh r2, [r4, #0xa]
	sub r1, r1, r2
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldrh r2, [r4, #8]
	bl sub_08012A08
	cmp r0, #0
	bne _080171D8
	mov r0, #0
	b _080171EA
_080171D8:
	mov r1, #0xa
	ldrsh r0, [r4, r1]
	lsl r0, r0, #8
	ldr r1, [r4, #4]
	sub r1, r1, r0
	ldr r0, _080171F0
	and r1, r0
	add r0, #1
	sub r0, r0, r1
_080171EA:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
_080171F0: .4byte 0x000007FF
	thumb_func_global sub_80171F4
sub_80171F4: @ 0x080171F4
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	ldr r1, [r4, #4]
	lsl r1, r1, #8
	lsr r1, r1, #0x10
	ldrh r2, [r4, #0xa]
	bl sub_08012A84
	cmp r0, #0
	bne _08017212
	mov r0, #0
	b _0801721C
_08017212:
	ldr r1, [r4]
	ldr r0, _08017224
	and r1, r0
	add r0, #1
	sub r0, r0, r1
_0801721C:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
_08017224: .4byte 0x000007FF
	thumb_func_global sub_8017228
sub_8017228: @ 0x08017228
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4]
	asr r0, r0, #8
	ldrh r1, [r4, #8]
	add r0, r0, r1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r1, [r4, #4]
	lsl r1, r1, #8
	lsr r1, r1, #0x10
	ldrh r2, [r4, #0xa]
	bl sub_08012A84
	cmp r0, #0
	bne _0801724C
	mov r0, #0
	b _0801725C
_0801724C:
	mov r0, #8
	ldrsh r1, [r4, r0]
	lsl r1, r1, #8
	ldr r0, [r4]
	add r0, r0, r1
	ldr r1, _08017264
	and r0, r1
	mvn r0, r0
_0801725C:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
_08017264: .4byte 0x000007FF



thumb_func_global nullsub_4
nullsub_4: @ 0x08017268
	bx lr
	.align 2, 0

thumb_func_global nullsub_5
nullsub_5: @ 0x0801726C
	bx lr
	.align 2, 0

thumb_func_global player_init_maybe
player_init_maybe: @ 0x08017270
	push {r4, r5, r6, r7, lr}
	sub sp, #8
	add r0, sp, #4
	mov r5, #0
	strh r5, [r0]
	ldr r1, _08017398
	str r0, [r1]
	ldr r6, _0801739C
	str r6, [r1, #4]
	ldr r0, _080173A0
	str r0, [r1, #8]
	ldr r0, [r1, #8]
	bl getCurrentPowerUpPal
	mov r1, #2
	bl LoadSpritePalette
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	mov r0, #0x80
	str r0, [sp]
	mov r0, #0
	mov r1, #0
	mov r2, #0x20
	mov r3, #2
	bl createSprite
	bl initPlayerInit
	str r0, [r6]
	lsl r0, r4, #0x10
	asr r0, r0, #0x10
	bl initGrapplingHook
	bl createPlayerAttackObjs
	ldr r3, _080173A4
	ldrb r0, [r3, #1]
	ldrb r1, [r3, #2]
	ldrb r2, [r3, #3]
	ldrb r3, [r3, #4]
	bl getDoorCoords
	ldr r3, [r6]
	mov r2, #0
	ldrsh r1, [r0, r2]
	mov r7, #2
	ldrsh r2, [r0, r7]
	add r0, r3, #0
	bl cameraRelativeXY
	ldr r0, [r6]
	mov r1, #7
	neg r1, r1
	mov r2, #0x20
	str r2, [sp]
	mov r2, #0x1f
	mov r3, #0xe
	bl setObjSize
	ldr r0, [r6]
	ldr r1, _080173A8
	bl setObjCollisionCallback
	ldr r0, [r6]
	mov r1, #0xe0
	lsl r1, r1, #3
	bl setObjMaxVelocityX
	ldr r0, [r6]
	mov r1, #0xc0
	lsl r1, r1, #3
	bl setObjMaxVelocityY
	ldr r0, [r6]
	bl clearObjForces
	ldr r0, [r6]
	ldr r1, _080173AC
	bl setObjDrawCallback
	ldr r0, [r6]
	ldr r0, [r0]
	add r0, #0x22
	strb r4, [r0]
	ldr r0, [r6]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #2
	orr r0, r2
	strb r0, [r1]
	add r2, r6, #0
	add r2, #0x4a
	ldrb r0, [r2]
	mov r1, #1
	orr r0, r1
	strb r0, [r2]
	mov r0, #1
	strh r0, [r6, #0x10]
	mov r0, #0xff
	strh r0, [r6, #0xa]
	add r1, r6, #0
	add r1, #0xa6
	strh r0, [r1]
	mov r0, #0
	bl sub_80190F4
	mov r0, #0
	bl setPlayerSprite
	bl sub_08018FF0
	bl sub_08018D68
	bl sub_08018C68
	ldr r0, _080173B0
	strb r5, [r0]
	mov r0, #0
	bl setPlayerPaletteMaybe
	bl isUsingInvulnSpecial
	cmp r0, #0
	beq _08017382
	add r1, r6, #0
	add r1, #0xa2
	mov r0, #0xd
	strb r0, [r1]
	add r1, #2
	mov r0, #0
	strh r0, [r1]
	mov r0, #5
	bl setPlayerPaletteMaybe
_08017382:
	bl nullsub_4
	ldr r0, _080173B4
	mov r1, #3
	bl createTask
	add sp, #8
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_08017398: .4byte 0x040000D4
_0801739C: .4byte 0x03004750
_080173A0: .4byte 0x81000060
_080173A4: .4byte 0x03003D40
_080173A8: .4byte (sub_801B8FC+1)
_080173AC: .4byte (sub_8018988+1)
_080173B0: .4byte 0x03001D70
_080173B4: .4byte (task_playerHandler+1)

thumb_func_global task_playerHandler
task_playerHandler: @ 0x080173B8
	push {lr}
	bl sub_0801B4C4
	bl sub_080192A0
	ldr r1, _08017420
	ldr r0, _08017424
	ldrh r0, [r0, #4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl call_r0
	bl sub_0801938C
	bl sub_08019CA4
	bl sub_080193D4
	bl sub_08019BD0
	bl exec_gPlayerInit_44
	bl sub_08018FF0
	bl sub_0801A8D4
	bl sub_0801B378
	bl sub_0801B9D0
	bl sub_0801BC28
	bl sub_0801925C
	bl sub_0801BC08
	bl sub_0801CB44
	bl sub_0801B3D4
	bl player_launchSiteTimerDeath
	bl sub_0801C790
	bl sub_0801C880
	bl nullsub_5
	pop {r0}
	bx r0
	.align 2, 0
_08017420: .4byte playerMovementFunctions
_08017424: .4byte 0x03004750

thumb_func_global player_idle
player_idle: @ 0x08017428
	push {r4, r5, lr}
	ldr r4, _08017444
	ldr r0, [r4]
	bl clearObjForces
	ldr r0, [r4]
	add r0, #0x4d
	ldrb r0, [r0]
	cmp r0, #0
	beq _08017448
	mov r0, #0
	bl sub_0801C240
	b _080175D2
	.align 2, 0
_08017444: .4byte 0x03004750
_08017448:
	mov r0, #0
	bl sub_0801A894
	cmp r0, #0
	beq _08017454
	b _080175D2
_08017454:
	bl sub_0801394C
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, #0
	beq _08017478
	add r0, r4, #0
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #2
	and r0, r1
	cmp r0, #0
	beq _08017472
	b _080175D2
_08017472:
	bl removeCameraPlayerControl
	b _080175D2
_08017478:
	add r0, r4, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #0x40
	and r0, r1
	cmp r0, #0
	beq _080174D8
	bl getPlayerHealth
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _080174D8
	bl sub_0801C328
	bl sub_0801C908
	cmp r0, #0
	beq _080174D8
	mov r0, #0x19
	bl sub_80190F4
	mov r0, #7
	bl setPlayerSprite
	ldr r0, _080174BC
	ldrh r1, [r0, #0x18]
	ldr r0, [r4]
	ldr r0, [r0, #4]
	lsl r0, r0, #8
	asr r0, r0, #0x10
	cmp r1, r0
	bge _080174C0
	strh r5, [r4, #0x10]
	b _080174C4
	.align 2, 0
_080174BC: .4byte 0x030046F0
_080174C0:
	mov r0, #1
	strh r0, [r4, #0x10]
_080174C4:
	ldr r0, _080174D4
	add r0, #0x22
	ldrb r1, [r0]
	mov r2, #0x10
	orr r1, r2
	strb r1, [r0]
	b _080175D2
	.align 2, 0
_080174D4: .4byte 0x03004750
_080174D8:
	ldr r4, _080174F0
	add r0, r4, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #2
	and r0, r1
	cmp r0, #0
	beq _080174F4
	bl sub_080138A4
	b _080175D2
	.align 2, 0
_080174F0: .4byte 0x03004750
_080174F4:
	add r0, r4, #0
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #0x80
	and r0, r1
	cmp r0, #0
	beq _08017522
	bl sub_0801C328
	add r0, r4, #0
	add r0, #0x22
	ldrb r1, [r0]
	mov r2, #1
	orr r1, r2
	strb r1, [r0]
	mov r0, #1
	bl sub_80190F4
	mov r0, #5
	mov r1, #2
	bl sub_80191B0
	b _0801753E
_08017522:
	mov r0, #0x30
	and r0, r1
	cmp r0, #0
	beq _0801753E
	bl sub_0801C328
	bl sub_0801C344
	mov r0, #2
	bl sub_80190F4
	mov r0, #7
	bl setPlayerSprite
_0801753E:
	bl sub_0801C1B0
	cmp r0, #0
	beq _08017556
	bl sub_0801C328
	mov r0, #0
	bl setPlayerSprite
	bl activateKillSpecial
	b _080175D2
_08017556:
	bl canActivateInvulnSpecial
	cmp r0, #0
	beq _08017564
	bl activateInvulnSpecial
	b _080175D2
_08017564:
	ldr r2, _08017590
	add r0, r2, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #1
	and r0, r1
	cmp r0, #0
	beq _08017594
	add r0, r2, #0
	add r0, #0x4a
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _08017594
	bl sub_0801C328
	bl sub_0801C4C4
	b _080175D2
	.align 2, 0
_08017590: .4byte 0x03004750
_08017594:
	add r0, r2, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _080175BA
	add r0, r2, #0
	add r0, #0x4a
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _080175BA
	bl sub_0801C328
	bl sub_0801C518
	b _080175D2
_080175BA:
	add r0, r2, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _080175D2
	bl sub_0801C328
	mov r0, #0
	bl sub_0801C450
_080175D2:
	pop {r4, r5}
	pop {r0}
	bx r0

thumb_func_global player_crouchProneSlide
player_crouchProneSlide: @ 0x080175D8
	push {r4, lr}
	ldr r4, _080175F4
	ldr r0, [r4]
	bl clearObjForces
	ldr r0, [r4]
	add r0, #0x4d
	ldrb r0, [r0]
	cmp r0, #0
	beq _080175F8
	mov r0, #0
	bl sub_0801C240
	b _08017738
	.align 2, 0
_080175F4: .4byte 0x03004750
_080175F8:
	mov r0, #1
	bl sub_0801A894
	cmp r0, #0
	beq _08017604
	b _08017738
_08017604:
	bl sub_0801394C
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08017626
	add r0, r4, #0
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #2
	and r0, r1
	cmp r0, #0
	beq _08017620
	b _08017738
_08017620:
	bl removeCameraPlayerControl
	b _08017738
_08017626:
	add r0, r4, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #2
	and r0, r1
	cmp r0, #0
	beq _0801763C
	bl sub_080138A4
	b _08017738
_0801763C:
	add r0, r4, #0
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #0x80
	and r0, r1
	cmp r0, #0
	bne _08017670
	bl sub_0801C638
	cmp r0, #0
	bne _08017670
	add r2, r4, #0
	add r2, #0x22
	ldrb r1, [r2]
	mov r0, #2
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	mov r0, #0
	bl sub_80190F4
	mov r0, #6
	mov r1, #0
	bl sub_80191B0
	b _08017738
_08017670:
	ldr r4, _080176A8
	add r0, r4, #0
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #0x30
	and r0, r1
	cmp r0, #0
	beq _08017692
	bl sub_0801C344
	mov r0, #4
	bl sub_80190F4
	mov r0, #0xb
	mov r1, #0xc
	bl sub_80191B0
_08017692:
	bl sub_0801C1B0
	cmp r0, #0
	beq _080176AC
	mov r0, #2
	bl setPlayerSprite
	bl activateKillSpecial
	b _08017760
	.align 2, 0
_080176A8: .4byte 0x03004750
_080176AC:
	bl canActivateInvulnSpecial
	cmp r0, #0
	beq _080176BA
	bl activateInvulnSpecial
	b _08017738
_080176BA:
	add r0, r4, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _080176DC
	add r0, r4, #0
	add r0, #0x4a
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _080176DC
	bl sub_0801C590
	b _08017738
_080176DC:
	ldr r2, _08017704
	add r0, r2, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #1
	and r0, r1
	cmp r0, #0
	beq _08017708
	add r0, r2, #0
	add r0, #0x4a
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _08017708
	bl sub_0801C55C
	b _08017738
	.align 2, 0
_08017704: .4byte 0x03004750
_08017708:
	ldr r4, _08017768
	add r0, r4, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _08017738
	mov r0, #0
	bl set_player_18
	ldrh r0, [r4, #0x10]
	strh r0, [r4, #0x14]
	mov r0, #3
	bl sub_80190F4
	mov r0, #8
	mov r1, #9
	bl sub_80191B0
	mov r0, #0x80
	lsl r0, r0, #1
	bl playSong
_08017738:
	ldr r2, _08017768
	add r0, r2, #0
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #0x80
	and r0, r1
	cmp r0, #0
	beq _08017760
	add r0, r2, #0
	add r0, #0xa8
	ldrh r0, [r0]
	cmp r0, #0
	bne _08017760
	ldr r0, [r2]
	bl sub_0801F590
	cmp r0, #0
	beq _08017760
	bl grab_item_structured
_08017760:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_08017768: .4byte 0x03004750

thumb_func_global player_walk
player_walk: @ 0x0801776C
	push {r4, r5, lr}
	sub sp, #0xc
	ldr r0, _080177A8
	ldr r1, [r0, #4]
	ldr r0, [r0]
	str r0, [sp, #4]
	str r1, [sp, #8]
	ldr r1, _080177AC
	mov r0, sp
	mov r2, #4
	bl memcpy
	ldr r4, _080177B0
	ldr r0, [r4]
	bl clearObjForces
	ldr r0, [r4]
	add r0, #0x4d
	ldrb r0, [r0]
	cmp r0, #0
	beq _080177B4
	ldrh r0, [r4, #0x10]
	lsl r0, r0, #2
	add r1, sp, #4
	add r0, r0, r1
	ldr r0, [r0]
	bl sub_0801C240
	b _08017928
	.align 2, 0
_080177A8: .4byte dword_813CF64
_080177AC: .4byte dword_813CF6C
_080177B0: .4byte 0x03004750
_080177B4:
	mov r0, #2
	bl sub_0801A894
	add r5, r0, #0
	cmp r5, #0
	beq _080177C2
	b _08017928
_080177C2:
	add r0, r4, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #0x40
	and r0, r1
	cmp r0, #0
	beq _08017820
	bl getPlayerHealth
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08017820
	bl sub_0801C908
	cmp r0, #0
	beq _08017820
	mov r0, #0x19
	bl sub_80190F4
	mov r0, #7
	bl setPlayerSprite
	ldr r0, _08017804
	ldrh r1, [r0, #0x18]
	ldr r0, [r4]
	ldr r0, [r0, #4]
	lsl r0, r0, #8
	asr r0, r0, #0x10
	cmp r1, r0
	bge _08017808
	strh r5, [r4, #0x10]
	b _0801780C
	.align 2, 0
_08017804: .4byte 0x030046F0
_08017808:
	mov r0, #1
	strh r0, [r4, #0x10]
_0801780C:
	ldr r0, _0801781C
	add r0, #0x22
	ldrb r1, [r0]
	mov r2, #0x10
	orr r1, r2
	strb r1, [r0]
	b _08017928
	.align 2, 0
_0801781C: .4byte 0x03004750
_08017820:
	ldr r2, _08017834
	add r0, r2, #0
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #0x20
	and r0, r1
	cmp r0, #0
	beq _08017838
	mov r0, #0
	b _08017842
	.align 2, 0
_08017834: .4byte 0x03004750
_08017838:
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	beq _08017868
	mov r0, #1
_08017842:
	strh r0, [r2, #0x10]
	mov r0, #0x80
	and r0, r1
	cmp r0, #0
	beq _08017874
	add r0, r2, #0
	add r0, #0x22
	ldrb r1, [r0]
	mov r2, #1
	orr r1, r2
	strb r1, [r0]
	mov r0, #1
	bl sub_80190F4
	mov r0, #5
	mov r1, #2
	bl sub_80191B0
	b _08017874
_08017868:
	mov r0, #0
	bl sub_80190F4
	mov r0, #0
	bl setPlayerSprite
_08017874:
	bl sub_0801C1B0
	cmp r0, #0
	beq _080178A0
	ldr r0, _0801789C
	add r0, #0x22
	ldrb r2, [r0]
	mov r1, #2
	neg r1, r1
	and r1, r2
	strb r1, [r0]
	mov r0, #0
	bl sub_80190F4
	mov r0, #0
	bl setPlayerSprite
	bl activateKillSpecial
	b _08017928
	.align 2, 0
_0801789C: .4byte 0x03004750
_080178A0:
	bl canActivateInvulnSpecial
	cmp r0, #0
	beq _080178AE
	bl activateInvulnSpecial
	b _08017928
_080178AE:
	ldr r2, _080178D4
	add r0, r2, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #1
	and r0, r1
	cmp r0, #0
	beq _080178D8
	add r0, r2, #0
	add r0, #0x4a
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _080178D8
	bl sub_0801C4C4
	b _08017928
	.align 2, 0
_080178D4: .4byte 0x03004750
_080178D8:
	add r0, r2, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _080178FA
	add r0, r2, #0
	add r0, #0x4a
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _080178FA
	bl sub_0801C518
	b _08017928
_080178FA:
	add r0, r2, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _08017918
	ldrh r0, [r2, #0x10]
	lsl r0, r0, #2
	add r1, sp, #4
	add r0, r0, r1
	ldr r0, [r0]
	bl sub_0801C450
	b _08017928
_08017918:
	ldr r0, [r2]
	ldrh r1, [r2, #0x10]
	lsl r1, r1, #2
	add r2, sp, #4
	add r1, r1, r2
	ldr r1, [r1]
	bl setObjVelocity_X
_08017928:
	add sp, #0xc
	pop {r4, r5}
	pop {r0}
	bx r0

thumb_func_global player_slide
player_slide: @ 0x08017930
	push {r4, r5, lr}
	sub sp, #8
	ldr r0, _08017968
	ldr r1, [r0, #4]
	ldr r0, [r0]
	str r0, [sp]
	str r1, [sp, #4]
	bl inc_player_18
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldr r4, _0801796C
	ldr r0, [r4]
	bl clearObjForces
	ldr r2, [r4]
	add r0, r2, #0
	add r0, #0x4d
	ldrb r0, [r0]
	cmp r0, #0
	beq _08017970
	ldrh r0, [r4, #0x10]
	lsl r0, r0, #2
	add r0, sp
	ldr r0, [r0]
	bl sub_0801C240
	b _08017A3E
	.align 2, 0
_08017968: .4byte dword_813CF70
_0801796C: .4byte 0x03004750
_08017970:
	cmp r5, #0xd
	bhi _08017986
	cmp r5, #6
	bls _0801799A
	add r0, r4, #0
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	bne _0801799A
_08017986:
	add r0, r2, #0
	bl clearObjVel_Accel
	ldrh r0, [r4, #0x14]
	strh r0, [r4, #0x10]
	mov r0, #0xa
	mov r1, #8
	bl sub_0801C2D4
	b _08017A3E
_0801799A:
	ldr r4, _080179BC
	ldr r0, [r4]
	ldrh r1, [r4, #0x10]
	lsl r1, r1, #2
	add r1, sp
	ldr r1, [r1]
	bl setObjVelocity_X
	add r0, r4, #0
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #0x20
	and r0, r1
	cmp r0, #0
	beq _080179C0
	mov r0, #0
	b _080179CA
	.align 2, 0
_080179BC: .4byte 0x03004750
_080179C0:
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	beq _080179CC
	mov r0, #1
_080179CA:
	strh r0, [r4, #0x14]
_080179CC:
	bl sub_0801C1B0
	cmp r0, #0
	beq _080179E6
	mov r0, #1
	bl sub_80190F4
	mov r0, #2
	bl setPlayerSprite
	bl activateKillSpecial
	b _08017A3E
_080179E6:
	bl canActivateInvulnSpecial
	cmp r0, #0
	beq _080179F4
	bl activateInvulnSpecial
	b _08017A3E
_080179F4:
	ldr r2, _08017A18
	add r0, r2, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _08017A1C
	add r0, r2, #0
	add r0, #0x4a
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _08017A1C
	bl sub_0801C590
	b _08017A3E
	.align 2, 0
_08017A18: .4byte 0x03004750
_08017A1C:
	add r0, r2, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #1
	and r0, r1
	cmp r0, #0
	beq _08017A3E
	add r0, r2, #0
	add r0, #0x4a
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _08017A3E
	bl sub_0801C55C
_08017A3E:
	add sp, #8
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global player_crawl
player_crawl: @ 0x08017A48
	push {r4, lr}
	sub sp, #0xc
	ldr r0, _08017A84
	ldr r1, [r0, #4]
	ldr r0, [r0]
	str r0, [sp, #4]
	str r1, [sp, #8]
	ldr r1, _08017A88
	mov r0, sp
	mov r2, #4
	bl memcpy
	ldr r4, _08017A8C
	ldr r0, [r4]
	bl clearObjForces
	ldr r0, [r4]
	add r0, #0x4d
	ldrb r0, [r0]
	cmp r0, #0
	beq _08017A90
	ldrh r0, [r4, #0x10]
	lsl r0, r0, #2
	add r1, sp, #4
	add r0, r0, r1
	ldr r0, [r0]
	bl sub_0801C240
	b _08017BA4
	.align 2, 0
_08017A84: .4byte dword_813CF78
_08017A88: .4byte dword_813CF6C
_08017A8C: .4byte 0x03004750
_08017A90:
	mov r0, #1
	bl sub_0801A894
	cmp r0, #0
	beq _08017A9C
	b _08017BA4
_08017A9C:
	add r0, r4, #0
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #0x80
	and r0, r1
	cmp r0, #0
	bne _08017AD0
	bl sub_0801C638
	cmp r0, #0
	bne _08017AD0
	add r2, r4, #0
	add r2, #0x22
	ldrb r1, [r2]
	mov r0, #2
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	mov r0, #0
	bl sub_80190F4
	mov r0, #6
	mov r1, #0
	bl sub_80191B0
	b _08017BA4
_08017AD0:
	ldr r2, _08017AE8
	add r0, r2, #0
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #0x20
	and r0, r1
	cmp r0, #0
	beq _08017AEC
	mov r0, #0
	strh r0, [r2, #0x10]
	b _08017B08
	.align 2, 0
_08017AE8: .4byte 0x03004750
_08017AEC:
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	beq _08017AFA
	mov r0, #1
	strh r0, [r2, #0x10]
	b _08017B08
_08017AFA:
	mov r0, #1
	bl sub_80190F4
	mov r0, #0xd
	mov r1, #2
	bl sub_80191B0
_08017B08:
	bl canActivateInvulnSpecial
	cmp r0, #0
	beq _08017B16
	bl activateInvulnSpecial
	b _08017BA4
_08017B16:
	ldr r2, _08017B3C
	add r0, r2, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #2
	and r0, r1
	add r4, r2, #0
	cmp r0, #0
	beq _08017B40
	add r0, r4, #0
	add r0, #0x4a
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _08017B40
	bl sub_0801C590
	b _08017BA4
	.align 2, 0
_08017B3C: .4byte 0x03004750
_08017B40:
	add r0, r4, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #1
	and r0, r1
	cmp r0, #0
	beq _08017B64
	add r0, r4, #0
	add r0, #0x4a
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _08017B64
	bl sub_0801C55C
	b _08017BA4
_08017B64:
	add r0, r4, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _08017B94
	mov r0, #0
	bl set_player_18
	ldrh r0, [r4, #0x10]
	strh r0, [r4, #0x14]
	mov r0, #3
	bl sub_80190F4
	mov r0, #8
	mov r1, #9
	bl sub_80191B0
	mov r0, #0x80
	lsl r0, r0, #1
	bl playSong
	b _08017BA4
_08017B94:
	ldr r0, [r4]
	ldrh r1, [r4, #0x10]
	lsl r1, r1, #2
	add r2, sp, #4
	add r1, r1, r2
	ldr r1, [r1]
	bl setObjVelocity_X
_08017BA4:
	add sp, #0xc
	pop {r4}
	pop {r0}
	bx r0

thumb_func_global player_jump
player_jump: @ 0x08017BAC
	push {r4, r5, r6, r7, lr}
	bl inc_player_18
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	ldr r4, _08017BC8
	ldr r0, [r4]
	add r0, #0x4d
	ldrb r0, [r0]
	cmp r0, #0
	bne _08017BCC
	bl sub_0801C6AC
	b _08017D0E
	.align 2, 0
_08017BC8: .4byte 0x03004750
_08017BCC:
	add r6, r4, #0
	add r6, #0x4a
	ldrb r1, [r6]
	mov r0, #0x20
	and r0, r1
	cmp r0, #0
	beq _08017BF0
	add r2, r4, #0
	add r2, #0xa0
	add r0, r4, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #1
	and r0, r1
	ldrh r1, [r2]
	orr r0, r1
	strh r0, [r2]
_08017BF0:
	mov r0, #3
	bl sub_0801A894
	add r5, r0, #0
	cmp r5, #0
	bne _08017C86
	bl sub_0801C1B0
	cmp r0, #0
	beq _08017C16
	mov r0, #7
	bl sub_80190F4
	mov r0, #0x10
	bl setPlayerSprite
	bl activateKillSpecial
	b _08017D0E
_08017C16:
	bl canActivateInvulnSpecial
	cmp r0, #0
	beq _08017C24
	bl activateInvulnSpecial
	b _08017C86
_08017C24:
	add r0, r4, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #1
	and r0, r1
	cmp r0, #0
	beq _08017C54
	add r0, r4, #0
	add r0, #0xb0
	ldrh r0, [r0]
	sub r0, #1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #1
	bls _08017C54
	ldrb r1, [r6]
	mov r0, #0x21
	and r0, r1
	cmp r0, #1
	bne _08017C54
	bl sub_0801C5C4
	b _08017C86
_08017C54:
	ldr r2, _08017CD4
	add r0, r2, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _08017C86
	add r0, r2, #0
	add r0, #0xb0
	ldrh r0, [r0]
	sub r0, #1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #1
	bls _08017C86
	add r0, r2, #0
	add r0, #0x4a
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _08017C86
	bl sub_0801C608
_08017C86:
	add r0, r5, #0
	bl sub_0801C36C
	ldr r2, _08017CD4
	add r0, r2, #0
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	bne _08017CD8
	cmp r7, #5
	beq _08017CAE
	ldr r0, [r2]
	ldr r0, [r0, #0x1c]
	cmp r0, #0
	bge _08017CAA
	neg r0, r0
_08017CAA:
	cmp r0, #0x61
	bgt _08017D0E
_08017CAE:
	ldr r4, _08017CD4
	ldr r0, [r4]
	mov r1, #0
	bl setObjVelocity_Y
	ldr r0, [r4]
	mov r1, #0x62
	bl setObjAccelerationY
	mov r0, #6
	bl sub_80190F4
	mov r0, #0xf
	bl setPlayerSprite
	mov r0, #0x10
	bl set_player_18
	b _08017D0E
	.align 2, 0
_08017CD4: .4byte 0x03004750
_08017CD8:
	cmp r7, #0x1d
	bhi _08017CEA
	ldr r0, [r2]
	ldr r0, [r0, #0x1c]
	cmp r0, #0
	bge _08017CE6
	neg r0, r0
_08017CE6:
	cmp r0, #0x61
	bgt _08017D0E
_08017CEA:
	ldr r4, _08017D14
	ldr r0, [r4]
	mov r1, #0
	bl setObjVelocity_Y
	ldr r0, [r4]
	mov r1, #0x62
	bl setObjAccelerationY
	mov r0, #6
	bl sub_80190F4
	mov r0, #0xf
	bl setPlayerSprite
	mov r0, #0x10
	bl set_player_18
_08017D0E:
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_08017D14: .4byte 0x03004750

thumb_func_global player_fallFromJump
player_fallFromJump: @ 0x08017D18
	push {r4, r5, r6, lr}
	ldr r4, _08017D2C
	ldr r0, [r4]
	add r0, #0x4d
	ldrb r0, [r0]
	cmp r0, #0
	bne _08017D30
	bl sub_0801C6AC
	b _08017E2E
	.align 2, 0
_08017D2C: .4byte 0x03004750
_08017D30:
	add r6, r4, #0
	add r6, #0x4a
	ldrb r1, [r6]
	mov r0, #0x20
	and r0, r1
	cmp r0, #0
	beq _08017D54
	add r2, r4, #0
	add r2, #0xa0
	add r0, r4, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #1
	and r0, r1
	ldrh r1, [r2]
	orr r0, r1
	strh r0, [r2]
_08017D54:
	mov r0, #3
	bl sub_0801A894
	add r5, r0, #0
	cmp r5, #0
	bne _08017DEA
	bl sub_0801C1B0
	cmp r0, #0
	beq _08017D7A
	mov r0, #7
	bl sub_80190F4
	mov r0, #0x10
	bl setPlayerSprite
	bl activateKillSpecial
	b _08017E2E
_08017D7A:
	bl canActivateInvulnSpecial
	cmp r0, #0
	beq _08017D88
	bl activateInvulnSpecial
	b _08017DEA
_08017D88:
	add r0, r4, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #1
	and r0, r1
	cmp r0, #0
	beq _08017DB8
	add r0, r4, #0
	add r0, #0xb0
	ldrh r0, [r0]
	sub r0, #1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #1
	bls _08017DB8
	ldrb r1, [r6]
	mov r0, #0x21
	and r0, r1
	cmp r0, #1
	bne _08017DB8
	bl sub_0801C5C4
	b _08017DEA
_08017DB8:
	ldr r2, _08017E20
	add r0, r2, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _08017DEA
	add r0, r2, #0
	add r0, #0xb0
	ldrh r0, [r0]
	sub r0, #1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #1
	bls _08017DEA
	add r0, r2, #0
	add r0, #0x4a
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _08017DEA
	bl sub_0801C608
_08017DEA:
	add r0, r5, #0
	bl sub_0801C36C
	bl dec_player_18
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _08017E24
	ldr r2, _08017E20
	add r3, r2, #0
	add r3, #0x4a
	ldrb r1, [r3]
	mov r0, #0x21
	neg r0, r0
	and r0, r1
	strb r0, [r3]
	ldr r0, [r2]
	mov r1, #0
	bl setObjAccelerationY
	mov r0, #7
	bl sub_80190F4
	mov r0, #0x10
	bl setPlayerSprite
	b _08017E2E
	.align 2, 0
_08017E20: .4byte 0x03004750
_08017E24:
	ldr r0, _08017E34
	ldr r0, [r0]
	mov r1, #0x62
	bl setObjAccelerationY
_08017E2E:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_08017E34: .4byte 0x03004750

thumb_func_global player_fall
player_fall: @ 0x08017E38
	push {r4, r5, lr}
	ldr r1, _08017E4C
	ldr r0, [r1]
	add r0, #0x4d
	ldrb r0, [r0]
	cmp r0, #0
	bne _08017E50
	bl sub_0801C6AC
	b _08017F22
	.align 2, 0
_08017E4C: .4byte 0x03004750
_08017E50:
	add r0, r1, #0
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x1c
	cmp r0, #0
	blt _08017E66
	ldrh r0, [r1, #0x1e]
	cmp r0, #0x63
	bhi _08017E66
	add r0, #1
	strh r0, [r1, #0x1e]
_08017E66:
	mov r0, #3
	bl sub_0801A894
	add r5, r0, #0
	cmp r5, #0
	bne _08017F1C
	ldr r4, _08017EA4
	add r3, r4, #0
	add r3, #0xa0
	ldrh r1, [r3]
	cmp r1, #0
	beq _08017E8A
	add r2, r4, #0
	add r2, #0x9a
	ldrh r0, [r2]
	orr r0, r1
	strh r0, [r2]
	strh r5, [r3]
_08017E8A:
	bl sub_0801C1B0
	cmp r0, #0
	beq _08017EA8
	mov r0, #7
	bl sub_80190F4
	mov r0, #0x10
	bl setPlayerSprite
	bl activateKillSpecial
	b _08017F22
	.align 2, 0
_08017EA4: .4byte 0x03004750
_08017EA8:
	bl canActivateInvulnSpecial
	cmp r0, #0
	beq _08017EB6
	bl activateInvulnSpecial
	b _08017F1C
_08017EB6:
	add r0, r4, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #1
	and r0, r1
	cmp r0, #0
	beq _08017EEA
	add r0, r4, #0
	add r0, #0xb0
	ldrh r0, [r0]
	sub r0, #1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #1
	bls _08017EEA
	add r0, r4, #0
	add r0, #0x4a
	ldrb r1, [r0]
	mov r0, #0x21
	and r0, r1
	cmp r0, #1
	bne _08017EEA
	bl sub_0801C5C4
	b _08017F1C
_08017EEA:
	ldr r2, _08017F28
	add r0, r2, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _08017F1C
	add r0, r2, #0
	add r0, #0xb0
	ldrh r0, [r0]
	sub r0, #1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #1
	bls _08017F1C
	add r0, r2, #0
	add r0, #0x4a
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _08017F1C
	bl sub_0801C608
_08017F1C:
	add r0, r5, #0
	bl sub_0801C36C
_08017F22:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_08017F28: .4byte 0x03004750

thumb_func_global player_ledgeHang
player_ledgeHang: @ 0x08017F2C
	push {r4, r5, r6, lr}
	sub sp, #8
	ldr r1, _08017F6C
	mov r0, sp
	mov r2, #4
	bl memcpy
	add r4, sp, #4
	ldr r1, _08017F70
	add r0, r4, #0
	mov r2, #4
	bl memcpy
	bl sub_0801394C
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	add r6, r4, #0
	cmp r5, #0
	beq _08017F78
	ldr r0, _08017F74
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #2
	and r0, r1
	cmp r0, #0
	beq _08017F66
	b _080180C0
_08017F66:
	bl removeCameraPlayerControl
	b _080180C0
	.align 2, 0
_08017F6C: .4byte dword_813CF80
_08017F70: .4byte dword_813CF6C
_08017F74: .4byte 0x03004750
_08017F78:
	bl sub_0801C1B0
	cmp r0, #0
	beq _08017FA4
	ldr r1, _08017FA0
	ldr r0, [r1]
	add r0, #0x4e
	strb r5, [r0]
	ldr r2, [r1]
	ldr r0, [r1, #0x34]
	ldr r1, [r1, #0x38]
	str r0, [r2, #4]
	str r1, [r2, #8]
	mov r0, #0
	bl sub_0801C240
	bl activateKillSpecial
	b _080180C0
	.align 2, 0
_08017FA0: .4byte 0x03004750
_08017FA4:
	bl canActivateInvulnSpecial
	cmp r0, #0
	beq _08017FB2
	bl activateInvulnSpecial
	b _080180C0
_08017FB2:
	ldr r0, _08018000
	add r1, r0, #0
	add r1, #0x9a
	ldrh r0, [r0, #0x10]
	lsl r0, r0, #1
	add r0, sp
	ldrh r1, [r1]
	ldrh r0, [r0]
	and r0, r1
	cmp r0, #0
	bne _08017FE4
	mov r0, #0x80
	and r0, r1
	cmp r0, #0
	bne _08017FE4
	bl getHealthBarValue
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _08018004
	bl getPlayerHealth
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _08018004
_08017FE4:
	ldr r2, _08018000
	ldr r0, [r2]
	add r0, #0x4e
	mov r1, #0
	strb r1, [r0]
	ldr r3, [r2]
	ldr r0, [r2, #0x34]
	ldr r1, [r2, #0x38]
	str r0, [r3, #4]
	str r1, [r3, #8]
	mov r0, #0
	bl sub_0801C240
	b _080180C0
	.align 2, 0
_08018000: .4byte 0x03004750
_08018004:
	ldr r4, _0801805C
	add r1, r4, #0
	add r1, #0x9a
	ldrh r0, [r4, #0x10]
	lsl r0, r0, #1
	add r0, r6, r0
	ldrh r1, [r1]
	ldrh r0, [r0]
	and r0, r1
	cmp r0, #0
	bne _08018026
	mov r0, #0x40
	and r0, r1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, #0
	beq _08018060
_08018026:
	ldr r0, [r4]
	bl clearObjForces
	mov r0, #9
	bl sub_80190F4
	mov r0, #0x13
	mov r1, #2
	bl sub_80191B0
	ldr r2, [r4]
	ldr r0, [r4, #0x2c]
	ldr r1, [r4, #0x30]
	str r0, [r2, #4]
	str r1, [r2, #8]
	ldr r0, [r4]
	add r0, #0x4d
	mov r1, #0
	strb r1, [r0]
	add r2, r4, #0
	add r2, #0x22
	ldrb r0, [r2]
	mov r1, #1
	orr r0, r1
	strb r0, [r2]
	b _080180C0
	.align 2, 0
_0801805C: .4byte 0x03004750
_08018060:
	mov r6, #1
	add r0, r6, #0
	and r0, r1
	cmp r0, #0
	beq _080180B2
	ldr r0, [r4]
	bl clearObjForces
	add r0, r4, #0
	add r0, #0xac
	str r5, [r0]
	ldr r0, [r4]
	add r0, #0x4e
	strb r5, [r0]
	ldr r2, [r4]
	ldr r0, [r4, #0x34]
	ldr r1, [r4, #0x38]
	str r0, [r2, #4]
	str r1, [r2, #8]
	mov r0, #0x1b
	bl sub_80190F4
	mov r0, #0x15
	mov r1, #0x10
	bl sub_80191B0
	mov r0, #0xa
	bl set_player_18
	add r0, r4, #0
	add r0, #0xb2
	strh r6, [r0]
	strh r5, [r4, #0x1e]
	sub r0, #0x12
	strh r5, [r0]
	ldr r0, [r4]
	mov r1, #0x82
	lsl r1, r1, #3
	bl setObjVelocity_Y
	b _080180C0
_080180B2:
	mov r0, #0x80
	lsl r0, r0, #2
	and r0, r1
	cmp r0, #0
	beq _080180C0
	bl sub_080138A4
_080180C0:
	add sp, #8
	pop {r4, r5, r6}
	pop {r0}
	bx r0

thumb_func_global player_climbLedge
player_climbLedge: @ 0x080180C8
	push {r4, r5, r6, lr}
	ldr r5, _0801811C
	add r0, r5, #0
	add r0, #0x23
	ldrb r0, [r0]
	lsr r6, r0, #7
	cmp r6, #0
	bne _0801810A
	ldr r0, [r5]
	bl clearObjForces
	add r2, r5, #0
	add r2, #0x22
	ldrb r0, [r2]
	mov r1, #1
	orr r0, r1
	strb r0, [r2]
	mov r4, #0
	strh r6, [r5, #0x1c]
	bl _3005773_clearBits3_5
	mov r0, #1
	bl sub_80190F4
	mov r0, #2
	bl setPlayerSprite
	ldr r0, [r5]
	add r0, #0x4e
	strb r4, [r0]
	ldr r0, [r5]
	add r0, #0x4d
	strb r4, [r0]
_0801810A:
	bl canActivateInvulnSpecial
	cmp r0, #0
	beq _08018116
	bl activateInvulnSpecial
_08018116:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_0801811C: .4byte 0x03004750

thumb_func_global player_hangHook
player_hangHook: @ 0x08018120
	push {r4, lr}
	bl sub_0801D7B8
	ldr r4, _08018140
	ldr r1, [r4]
	add r0, r1, #0
	add r0, #0x4d
	ldrb r0, [r0]
	cmp r0, #0
	bne _08018144
	bl sub_08018F4C
	bl sub_0801A93C
	b _0801833C
	.align 2, 0
_08018140: .4byte 0x03004750
_08018144:
	add r0, r1, #0
	bl clearObjForces
	mov r0, #4
	bl sub_0801A894
	cmp r0, #0
	beq _08018156
	b _0801829E
_08018156:
	bl sub_0801394C
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08018194
	add r0, r4, #0
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #2
	and r0, r1
	cmp r0, #0
	beq _0801818A
	ldr r1, _08018190
	add r0, r1, #0
	add r0, #0x57
	ldrb r0, [r0]
	cmp r0, #0
	beq _0801817E
	b _08018324
_0801817E:
	add r0, r1, #0
	add r0, #0x29
	ldrb r0, [r0]
	cmp r0, #0
	bne _0801818A
	b _08018324
_0801818A:
	bl removeCameraPlayerControl
	b _08018324
	.align 2, 0
_08018190: .4byte 0x03004810
_08018194:
	ldr r1, _080181C0
	add r0, r1, #0
	add r0, #0x29
	ldrb r0, [r0]
	cmp r0, #0
	beq _080181AA
	add r0, r1, #0
	add r0, #0x57
	ldrb r0, [r0]
	cmp r0, #0
	beq _080181C4
_080181AA:
	add r0, r4, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #2
	and r0, r1
	cmp r0, #0
	beq _080181C4
	bl sub_080138A4
	b _08018324
	.align 2, 0
_080181C0: .4byte 0x03004810
_080181C4:
	ldr r2, _080181D8
	add r0, r2, #0
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #0x20
	and r0, r1
	cmp r0, #0
	beq _080181DC
	mov r0, #0
	b _080181EA
	.align 2, 0
_080181D8: .4byte 0x03004750
_080181DC:
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	beq _080181E8
	mov r0, #1
	b _080181EA
_080181E8:
	ldrh r0, [r2, #0x10]
_080181EA:
	strh r0, [r2, #0x16]
	bl sub_0801C1B0
	cmp r0, #0
	beq _0801820A
	bl grapplingHookDeath
	mov r0, #7
	bl sub_80190F4
	mov r0, #0x10
	bl setPlayerSprite
	bl activateKillSpecial
	b _0801833C
_0801820A:
	bl canActivateInvulnSpecial
	add r4, r0, #0
	cmp r4, #0
	beq _0801821A
	bl activateInvulnSpecial
	b _08018250
_0801821A:
	ldr r2, _08018270
	add r0, r2, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _08018250
	add r3, r2, #0
	add r3, #0x4a
	ldrb r1, [r3]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _08018250
	mov r0, #8
	orr r0, r1
	strb r0, [r3]
	add r1, r2, #0
	add r1, #0x45
	mov r0, #0xf
	strb r0, [r1]
	ldr r0, _08018274
	add r0, #0x54
	strb r4, [r0]
	bl sub_8018B34
_08018250:
	ldr r1, _08018270
	add r2, r1, #0
	add r2, #0xb4
	ldrb r0, [r2]
	cmp r0, #0
	bne _08018298
	add r0, r1, #0
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #0x20
	and r0, r1
	cmp r0, #0
	beq _08018278
	bl grapple_leftHeld
	b _08018284
	.align 2, 0
_08018270: .4byte 0x03004750
_08018274: .4byte 0x03004810
_08018278:
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	beq _08018284
	bl sub_0801D684
_08018284:
	ldr r0, _08018294
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #0x40
	and r0, r1
	cmp r0, #0
	bne _080182E2
	b _080182F0
	.align 2, 0
_08018294: .4byte 0x03004750
_08018298:
	sub r0, #1
	strb r0, [r2]
	b _08018324
_0801829E:
	bl canActivateInvulnSpecial
	cmp r0, #0
	beq _080182AA
	bl activateInvulnSpecial
_080182AA:
	add r1, r4, #0
	add r1, #0xb4
	ldrb r0, [r1]
	cmp r0, #0
	bne _08018320
	add r0, r4, #0
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #0x20
	and r0, r1
	cmp r0, #0
	beq _080182C8
	bl grapple_leftHeld
	b _080182D4
_080182C8:
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	beq _080182D4
	bl sub_0801D684
_080182D4:
	ldr r0, _080182EC
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #0x40
	and r0, r1
	cmp r0, #0
	beq _080182F0
_080182E2:
	mov r0, #0xc0
	lsl r0, r0, #1
	bl sub_801D71C
	b _08018300
	.align 2, 0
_080182EC: .4byte 0x03004750
_080182F0:
	mov r0, #0x80
	and r0, r1
	cmp r0, #0
	beq _08018300
	mov r0, #0xc0
	lsl r0, r0, #1
	bl sub_801D748
_08018300:
	ldr r0, _0801831C
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #1
	and r0, r1
	cmp r0, #0
	beq _08018324
	mov r0, #0xa0
	lsl r0, r0, #3
	bl sub_801D71C
	b _08018324
	.align 2, 0
_0801831C: .4byte 0x03004750
_08018320:
	sub r0, #1
	strb r0, [r1]
_08018324:
	bl getHealthBarValue
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _0801833C
	bl getPlayerHealth
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _0801833C
	bl grapplingHookDeath
_0801833C:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global player_slowLanding
player_slowLanding: @ 0x08018344
	push {r4, lr}
	ldr r4, _08018360
	ldr r0, [r4]
	bl clearObjForces
	ldr r0, [r4]
	add r0, #0x4d
	ldrb r0, [r0]
	cmp r0, #0
	beq _08018364
	mov r0, #0
	bl sub_0801C240
	b _08018374
	.align 2, 0
_08018360: .4byte 0x03004750
_08018364:
	bl dec_player_18
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _08018374
	mov r0, #1
	bl sub_80190F4
_08018374:
	bl canActivateInvulnSpecial
	cmp r0, #0
	beq _08018380
	bl activateInvulnSpecial
_08018380:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global player_enterDoor
player_enterDoor: @ 0x08018388
	push {lr}
	ldr r0, _080183A0
	add r0, #0x23
	ldrb r0, [r0]
	lsr r0, r0, #7
	cmp r0, #0
	bne _0801839C
	ldr r0, _080183A4
	bl setCurrentTaskFunc
_0801839C:
	pop {r0}
	bx r0
	.align 2, 0
_080183A0: .4byte 0x03004750
_080183A4: .4byte (sub_8026018+1)



thumb_func_global sub_80183A8
sub_80183A8:
	push {r4, lr}
	ldr r4, _080183E8
	ldr r3, [r4]
	add r0, r3, #0
	add r0, #0x4d
	ldrb r2, [r0]
	cmp r2, #0
	bne _080183F8
	ldrh r1, [r4, #0x1a]
	mov r0, #2
	and r0, r1
	add r0, #0x5a
	strh r0, [r4, #0x1a]
	add r0, r4, #0
	add r0, #0xb2
	strh r2, [r0]
	add r0, r3, #0
	bl clearObjForces
	add r0, r4, #0
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x1f
	cmp r0, #0
	beq _080183EC
	mov r0, #1
	bl sub_80190F4
	mov r0, #2
	bl setPlayerSprite
	b _080183F8
	.align 2, 0
_080183E8: .4byte 0x03004750
_080183EC:
	mov r0, #0
	bl sub_80190F4
	mov r0, #0
	bl setPlayerSprite
_080183F8:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
	
	
	
thumb_func_global player_death
player_death: @ 0x08018400
	push {r4, r5, lr}
	sub sp, #8
	ldr r0, _08018478
	ldr r1, [r0, #4]
	ldr r0, [r0]
	str r0, [sp]
	str r1, [sp, #4]
	bl nullsub_6
	ldr r4, _0801847C
	ldr r0, [r4]
	bl clearObjForces
	ldrh r0, [r4, #6]
	cmp r0, #0xc8
	beq _08018480
	ldr r2, [r4]
	add r0, r2, #0
	add r0, #0x4d
	ldrb r0, [r0]
	cmp r0, #1
	bne _08018480
	add r0, r4, #0
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x1f
	add r5, r4, #0
	add r5, #0x24
	cmp r0, #0
	beq _08018456
	ldrb r0, [r5]
	mov r1, #8
	orr r0, r1
	strb r0, [r5]
	ldr r0, [r2, #8]
	lsl r0, r0, #8
	asr r0, r0, #0x10
	add r0, #8
	mov r1, #8
	neg r1, r1
	and r0, r1
	lsl r0, r0, #8
	str r0, [r4, #0x3c]
_08018456:
	mov r0, #0xf
	bl sub_80190F4
	add r0, r4, #0
	add r0, #0x23
	ldrb r0, [r0]
	lsl r0, r0, #0x1d
	lsr r0, r0, #0x1f
	add r0, #0xc9
	bl setPlayerSprite
	ldrb r0, [r5]
	mov r1, #0x10
	orr r0, r1
	strb r0, [r5]
	b _08018522
	.align 2, 0
_08018478: .4byte dword_813CF84
_0801847C: .4byte 0x03004750
_08018480:
	ldr r4, _080184C0
	add r5, r4, #0
	add r5, #0x23
	ldrb r0, [r5]
	lsr r0, r0, #7
	cmp r0, #0
	bne _08018504
	mov r0, #0x78
	bl set_player_18
	bl sub_0801CBEC
	bl sub_080199B8
	cmp r0, #0
	beq _080184C8
	ldrb r0, [r5]
	mov r1, #1
	orr r0, r1
	strb r0, [r5]
	mov r0, #0x12
	bl sub_80190F4
	ldr r0, [r4]
	ldr r1, _080184C4
	bl setObjCollisionCallback
	ldr r0, [r4]
	add r0, #0x4e
	mov r1, #1
	strb r1, [r0]
	b _08018522
	.align 2, 0
_080184C0: .4byte 0x03004750
_080184C4: .4byte (sub_801B87C+1)
_080184C8:
	ldrb r0, [r5]
	lsl r0, r0, #0x1d
	cmp r0, #0
	bge _080184FC
	ldrh r0, [r4, #6]
	cmp r0, #0xb9
	beq _080184EE
	cmp r0, #0xb9
	bgt _080184E0
	cmp r0, #0xb7
	beq _080184E6
	b _080184FC
_080184E0:
	cmp r0, #0xcc
	beq _080184F6
	b _080184FC
_080184E6:
	mov r0, #0xcf
	bl setPlayerSprite
	b _080184FC
_080184EE:
	mov r0, #0xd0
	bl setPlayerSprite
	b _080184FC
_080184F6:
	mov r0, #0xd1
	bl setPlayerSprite
_080184FC:
	mov r0, #0x11
	bl sub_80190F4
	b _08018522
_08018504:
	ldrh r0, [r4, #6]
	cmp r0, #0xc8
	beq _08018522
	ldrh r0, [r4, #0x18]
	cmp r0, #0
	beq _08018522
	ldr r0, [r4]
	ldrh r1, [r4, #0x10]
	lsl r1, r1, #2
	add r1, sp
	ldr r1, [r1]
	bl setObjVelocity_X
	bl dec_player_18
_08018522:
	add sp, #8
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global player_fallingDeath
player_fallingDeath: @ 0x0801852C
	push {r4, lr}
	bl nullsub_6
	ldr r4, _08018570
	ldr r0, [r4]
	mov r1, #0
	bl setObjVelocity_X
	ldr r0, [r4]
	mov r1, #0
	bl setObjAccelerationX
	ldr r0, [r4]
	add r0, #0x4d
	ldrb r0, [r0]
	cmp r0, #0
	bne _08018568
	mov r0, #0x10
	bl sub_80190F4
	add r0, r4, #0
	add r0, #0x23
	ldrb r1, [r0]
	lsl r1, r1, #0x1d
	lsr r0, r1, #0x1f
	add r0, #0xcb
	lsr r1, r1, #0x1f
	add r1, #0xcb
	bl sub_80191B0
_08018568:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_08018570: .4byte 0x03004750

thumb_func_global player_fallingDeathFire
player_fallingDeathFire: @ 0x08018574
	push {r4, r5, lr}
	bl nullsub_6
	ldr r5, _080185C0
	ldr r0, [r5]
	bl clearObjForces
	add r4, r5, #0
	add r4, #0x23
	ldrb r0, [r4]
	lsr r0, r0, #7
	cmp r0, #0
	bne _08018602
	mov r0, #0x78
	bl set_player_18
	bl sub_0801CBEC
	bl sub_080199B8
	cmp r0, #0
	beq _080185C8
	ldrb r0, [r4]
	mov r1, #1
	orr r0, r1
	strb r0, [r4]
	mov r0, #0x12
	bl sub_80190F4
	ldr r0, [r5]
	ldr r1, _080185C4
	bl setObjCollisionCallback
	ldr r0, [r5]
	add r0, #0x4e
	mov r1, #1
	strb r1, [r0]
	b _08018602
	.align 2, 0
_080185C0: .4byte 0x03004750
_080185C4: .4byte (sub_801B87C+1)
_080185C8:
	ldrb r0, [r4]
	lsl r0, r0, #0x1d
	cmp r0, #0
	bge _080185FC
	ldrh r0, [r5, #6]
	cmp r0, #0xb9
	beq _080185EE
	cmp r0, #0xb9
	bgt _080185E0
	cmp r0, #0xb7
	beq _080185E6
	b _080185FC
_080185E0:
	cmp r0, #0xcc
	beq _080185F6
	b _080185FC
_080185E6:
	mov r0, #0xcf
	bl setPlayerSprite
	b _080185FC
_080185EE:
	mov r0, #0xd0
	bl setPlayerSprite
	b _080185FC
_080185F6:
	mov r0, #0xd1
	bl setPlayerSprite
_080185FC:
	mov r0, #0x11
	bl sub_80190F4
_08018602:
	pop {r4, r5}
	pop {r0}
	bx r0

thumb_func_global player_death2
player_death2: @ 0x08018608
	push {r4, lr}
	bl nullsub_6
	ldr r4, _08018644
	ldr r0, [r4]
	bl clearObjForces
	bl dec_player_18
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, #0
	bne _0801863C
	ldr r0, [r4]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	ldr r0, _08018648
	strb r3, [r0, #0x10]
	ldr r0, _0801864C
	bl setCurrentTaskFunc
_0801863C:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_08018644: .4byte 0x03004750
_08018648: .4byte 0x03003D40
_0801864C: .4byte (sub_8026018+1)

thumb_func_global player_death_drown
player_death_drown: @ 0x08018650
	push {r4, lr}
	sub sp, #8
	ldr r0, _08018674
	ldr r1, [r0, #4]
	ldr r0, [r0]
	str r0, [sp]
	str r1, [sp, #4]
	ldr r0, _08018678
	add r0, #0x24
	ldrb r0, [r0]
	lsl r0, r0, #0x1b
	cmp r0, #0
	bge _0801867C
	mov r0, #0xce
	bl setPlayerSprite
	b _08018682
	.align 2, 0
_08018674: .4byte dword_813CF8C
_08018678: .4byte 0x03004750
_0801867C:
	mov r0, #0xcd
	bl setPlayerSprite
_08018682:
	bl nullsub_6
	ldr r4, _080186D4
	ldr r0, [r4]
	ldr r1, _080186D8
	ldr r1, [r1]
	mov r2, #0x3f
	and r1, r2
	lsr r1, r1, #5
	lsl r1, r1, #2
	add r1, sp
	ldr r1, [r1]
	bl setObjVelocity_X
	ldr r0, [r4]
	mov r1, #0x28
	bl setObjVelocity_Y
	bl dec_player_18
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, #0
	bne _080186CC
	ldr r0, [r4]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	ldr r0, _080186DC
	strb r3, [r0, #0x10]
	ldr r0, _080186E0
	bl setCurrentTaskFunc
_080186CC:
	add sp, #8
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_080186D4: .4byte 0x03004750
_080186D8: .4byte 0x03002080
_080186DC: .4byte 0x03003D40
_080186E0: .4byte (sub_8026018+1)

thumb_func_global player_killSpecialStart
player_killSpecialStart: @ 0x080186E4
	push {lr}
	ldr r0, _080186FC
	add r0, #0x23
	ldrb r0, [r0]
	lsr r0, r0, #7
	cmp r0, #0
	bne _080186F6
	bl sub_0801AABC
_080186F6:
	pop {r0}
	bx r0
	.align 2, 0
_080186FC: .4byte 0x03004750

thumb_func_global player_killSpecial
player_killSpecial: @ 0x08018700
	push {r4, lr}
	ldr r4, _08018734
	ldrh r0, [r4, #0x18]
	cmp r0, #0
	bne _08018742
	ldr r0, [r4]
	bl clearObjForces
	mov r0, #1
	bl sub_0801B300
	cmp r0, #0
	beq _08018766
	add r1, r4, #0
	add r1, #0x64
	ldrb r0, [r1]
	add r0, #1
	strb r0, [r1]
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #7
	bne _08018738
	bl sub_0801AB70
	b _0801876A
	.align 2, 0
_08018734: .4byte 0x03004750
_08018738:
	bl sub_0801AB08
	bl sub_0801AD5C
	b _08018766
_08018742:
	cmp r0, #1
	bne _0801875E
	ldr r0, [r4]
	ldr r1, [r4, #0x54]
	ldr r2, [r0, #4]
	sub r1, r1, r2
	bl setObjVelocity_X
	ldr r0, [r4]
	ldr r1, [r4, #0x58]
	ldr r2, [r0, #8]
	sub r1, r1, r2
	bl setObjVelocity_Y
_0801875E:
	bl dec_player_18
	bl sub_0801AD5C
_08018766:
	bl sub_0801ADA8
_0801876A:
	pop {r4}
	pop {r0}
	bx r0

thumb_func_global player_killSpecialEnd
player_killSpecialEnd: @ 0x08018770
	push {lr}
	ldr r0, _08018788
	add r0, #0x23
	ldrb r0, [r0]
	lsr r0, r0, #7
	cmp r0, #0
	bne _08018782
	bl sub_0801ABD0
_08018782:
	pop {r0}
	bx r0
	.align 2, 0
_08018788: .4byte 0x03004750


	
	thumb_func_global sub_801878C
sub_801878C: @ 0x0801878C
	push {r4, lr}
	ldr r4, _080187C0
	ldr r0, [r4]
	bl clearObjForces
	add r0, r4, #0
	add r0, #0x23
	ldrb r0, [r0]
	lsr r0, r0, #7
	cmp r0, #0
	bne _080187BA
	ldr r0, [r4]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	bl sub_801CCB8
	mov r0, #0x17
	bl sub_80190F4
_080187BA:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_080187C0: .4byte 0x03004750



	thumb_func_global sub_80187C4
sub_80187C4: @ 0x080187C4
	push {lr}
	bl isCameraPosCloneDifferent
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _0801882C
	ldr r0, _08018830
	ldr r0, [r0]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	ldr r2, _08018834
	ldr r0, [r2]
	ldr r1, _08018838
	and r0, r1
	mov r1, #0x81
	lsl r1, r1, #0xa
	cmp r0, r1
	bne _080187FE
	ldrb r0, [r2]
	cmp r0, #2
	beq _080187FE
	mov r0, #0x3c
	bl set_player_18
_080187FE:
	mov r0, #0xc2
	mov r1, #0xc2
	bl sub_80191B0
	mov r0, #0x18
	bl sub_80190F4
	ldr r2, _08018834
	ldr r0, [r2]
	ldr r1, _08018838
	and r0, r1
	mov r1, #0x81
	lsl r1, r1, #0xa
	cmp r0, r1
	bne _08018822
	ldrb r0, [r2]
	cmp r0, #2
	bne _0801882C
_08018822:
	bl stopSoundTracks
	mov r0, #5
	bl playSong
_0801882C:
	pop {r0}
	bx r0
	.align 2, 0
_08018830: .4byte 0x03004750
_08018834: .4byte 0x03003D40
_08018838: .4byte 0x00FFFF00



	thumb_func_global sub_801883C
sub_801883C: @ 0x0801883C
	push {lr}
	ldr r0, _0801887C
	add r0, #0x23
	ldrb r0, [r0]
	lsr r0, r0, #7
	cmp r0, #0
	bne _0801888E
	bl dec_player_18
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	ldr r3, _08018880
	ldr r0, [r3]
	ldr r1, _08018884
	and r0, r1
	mov r1, #0x81
	lsl r1, r1, #0xa
	cmp r0, r1
	bne _08018888
	ldrb r0, [r3]
	cmp r0, #2
	beq _08018888
	cmp r2, #0
	beq _08018888
	cmp r2, #0x3a
	bne _0801888E
	mov r0, #5
	mov r1, #2
	bl sub_80191B0
	b _0801888E
	.align 2, 0
_0801887C: .4byte 0x03004750
_08018880: .4byte 0x03003D40
_08018884: .4byte 0x00FFFF00
_08018888:
	ldr r0, _08018894
	bl setCurrentTaskFunc
_0801888E:
	pop {r0}
	bx r0
	.align 2, 0
_08018894: .4byte (sub_8026018+1)
	
	
	
	
	

thumb_func_global player_door2
player_door2: @ 0x08018898
	push {r4, lr}
	sub sp, #8
	ldr r0, _08018904
	ldr r1, [r0, #4]
	ldr r0, [r0]
	str r0, [sp]
	str r1, [sp, #4]
	ldr r4, _08018908
	ldr r0, [r4]
	bl clearObjForces
	ldrh r0, [r4, #0x1a]
	cmp r0, #0
	beq _080188D6
	ldr r0, [r4]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	add r2, r4, #0
	add r2, #0x22
	ldrb r1, [r2]
	mov r0, #9
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	mov r0, #0
	strh r0, [r4, #0x1a]
_080188D6:
	ldr r0, _0801890C
	ldrh r1, [r0, #0x18]
	ldr r2, [r4]
	ldr r0, [r2, #4]
	lsl r0, r0, #8
	asr r0, r0, #0x10
	sub r0, r1, r0
	cmp r0, #0
	bge _080188EA
	neg r0, r0
_080188EA:
	cmp r0, #1
	bgt _08018910
	lsl r0, r1, #8
	str r0, [r2, #4]
	mov r0, #0xc
	bl sub_80190F4
	mov r0, #0xad
	mov r1, #0xad
	bl sub_80191B0
	b _0801891E
	.align 2, 0
_08018904: .4byte dword_813CF64
_08018908: .4byte 0x03004750
_0801890C: .4byte 0x030046F0
_08018910:
	ldrh r0, [r4, #0x10]
	lsl r0, r0, #2
	add r0, sp
	ldr r1, [r0]
	add r0, r2, #0
	bl setObjVelocity_X
_0801891E:
	add sp, #8
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global player_landJmpAttack
player_landJmpAttack: @ 0x08018928
	push {r4, lr}
	mov r0, #3
	bl sub_0801A894
	ldr r4, _08018970
	add r0, r4, #0
	add r0, #0x45
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	cmp r0, #5
	bgt _08018956
	mov r0, #5
	mov r1, #0xc
	bl sub_0801C2D4
	add r1, r4, #0
	add r1, #0x21
	mov r0, #3
	strb r0, [r1]
	mov r0, #0xfe
	bl playSong
_08018956:
	ldr r0, [r4]
	bl clearObjForces
	bl canActivateInvulnSpecial
	cmp r0, #0
	beq _08018968
	bl activateInvulnSpecial
_08018968:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_08018970: .4byte 0x03004750

thumb_func_global exec_gPlayerInit_44
exec_gPlayerInit_44: @ 0x08018974
	push {lr}
	ldr r0, _08018984
	ldr r0, [r0]
	bl runObjPhysics
	pop {r0}
	bx r0
	.align 2, 0
_08018984: .4byte 0x03004750




thumb_func_global sub_8018988
sub_8018988: @ 0x08018988
	push {r4, lr}
	add r4, r0, #0
	bl sub_8016DF0
	add r0, r4, #0
	bl sub_8016E10
	ldr r0, _080189B0
	add r0, #0xb0
	ldrh r0, [r0]
	cmp r0, #2
	bgt _080189A8
	cmp r0, #1
	blt _080189A8
	bl sub_801E238
_080189A8:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_080189B0: .4byte 0x03004750
	thumb_func_global sub_80189B4
sub_80189B4: @ 0x080189B4
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x1c
	add r4, r0, #0
	mov r5, #0
	ldr r0, _08018AE8
	ldrh r0, [r0, #0x10]
	mov sl, r5
	cmp r0, #0
	bne _080189D4
	mov r0, #1
	neg r0, r0
	mov sl, r0
_080189D4:
	add r0, r4, #0
	bl updateVelocity
	ldr r0, [r4, #0x18]
	ldr r1, [r4, #0x1c]
	str r0, [r4, #0x20]
	str r1, [r4, #0x24]
	ldr r0, [r4, #4]
	ldr r1, [r4, #8]
	str r0, [r4, #0xc]
	str r1, [r4, #0x10]
	add r1, r4, #0
	add r1, #0x20
	str r1, [sp, #0x14]
	add r2, r4, #0
	add r2, #0x24
	str r2, [sp, #0x18]
	mov r0, #0x4d
	add r0, r0, r4
	mov r8, r0
	add r7, sp, #0xc
	mov sb, sp
_08018A00:
	ldr r0, [sp, #0x14]
	bl sub_8017040
	str r0, [sp, #0xc]
	ldr r0, [sp, #0x18]
	bl sub_8017040
	str r0, [r7, #4]
	ldr r2, [r4, #0x40]
	add r0, r4, #0
	add r1, r7, #0
	bl call_r2
	add r6, r0, #0
	ldr r0, [r4, #4]
	ldr r1, [sp, #0xc]
	add r0, r0, r1
	str r0, [r4, #4]
	ldr r1, [r4, #8]
	ldr r2, [r7, #4]
	add r1, r1, r2
	str r1, [r4, #8]
	add r0, sl
	mov r2, #0xb8
	lsl r2, r2, #5
	add r1, r1, r2
	bl sub_801D440
	cmp r0, #0
	beq _08018B0C
	bl sub_08019234
	mov r1, r8
	ldrb r0, [r1]
	cmp r0, #1
	bne _08018B06
	bl sub_801E428
	ldr r6, _08018AE8
	ldr r0, [r6]
	bl clearObjForces
	bl sub_8018DA4
	mov r2, #0x38
	ldrsh r1, [r4, r2]
	lsl r1, r1, #8
	ldr r0, [r4, #4]
	add r0, r0, r1
	str r0, [sp]
	mov r0, #0x3a
	ldrsh r1, [r4, r0]
	lsl r1, r1, #8
	ldr r0, [r4, #8]
	add r0, r0, r1
	str r0, [sp, #4]
	ldrh r0, [r4, #0x3c]
	sub r0, #1
	mov r1, sb
	strh r0, [r1, #8]
	ldrh r0, [r4, #0x3e]
	sub r0, #1
	strh r0, [r1, #0xa]
	mov r0, sp
	mov r1, #0
	bl sub_8017084
	cmp r0, #0
	beq _08018A8E
	mov r0, #1
	orr r5, r0
_08018A8E:
	mov r0, sp
	mov r1, #0
	bl sub_80170A4
	cmp r0, #0
	beq _08018AA2
	mov r0, #2
	orr r5, r0
	lsl r0, r5, #0x18
	lsr r5, r0, #0x18
_08018AA2:
	mov r0, sp
	mov r1, #0
	bl sub_80170C4
	cmp r0, #0
	beq _08018AB6
	mov r0, #4
	orr r5, r0
	lsl r0, r5, #0x18
	lsr r5, r0, #0x18
_08018AB6:
	mov r0, sp
	mov r1, #0
	bl sub_80170E4
	cmp r0, #0
	beq _08018ACA
	mov r0, #8
	orr r5, r0
	lsl r0, r5, #0x18
	lsr r5, r0, #0x18
_08018ACA:
	cmp r5, #0
	beq _08018AF0
	ldr r2, [r6]
	ldr r0, _08018AEC
	ldr r1, [r0, #0x50]
	ldr r0, [r0, #0x4c]
	str r0, [r2, #4]
	str r1, [r2, #8]
	bl sub_08018F4C
	ldr r0, [r6]
	bl clearObjForces
	b _08018B22
	.align 2, 0
_08018AE8: .4byte 0x03004750
_08018AEC: .4byte 0x03004810
_08018AF0:
	ldrh r1, [r6, #0x1a]
	cmp r1, #0x62
	bls _08018AFE
	mov r0, #2
	and r0, r1
	add r0, #0x5a
	strh r0, [r6, #0x1a]
_08018AFE:
	mov r0, #0xf6
	bl playSong
	b _08018B22
_08018B06:
	bl sub_8018FB8
	b _08018B22
_08018B0C:
	cmp r6, #0
	bne _08018B12
	b _08018A00
_08018B12:
	mov r2, r8
	ldrb r0, [r2]
	cmp r0, #0
	bne _08018B22
	bl sub_08019234
	bl sub_8018FB8
_08018B22:
	add sp, #0x1c
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0




thumb_func_global sub_8018B34
sub_8018B34: @ 0x08018B34
	push {r4, r5, lr}
	sub sp, #8
	ldr r1, _08018B80
	mov r0, sp
	mov r2, #6
	bl memcpy
	mov r4, #0
	ldr r0, _08018B84
	mov ip, r0
	ldr r0, [r0, #0x10]
	asr r0, r0, #8
	lsl r0, r0, #0x18
	mov r1, #0xa0
	lsl r1, r1, #0x13
	add r0, r0, r1
	lsr r5, r0, #0x1c
	mov r0, ip
	add r0, #0x54
	ldrb r0, [r0]
	cmp r0, #0
	bne _08018BC8
	ldr r1, _08018B88
	add r0, r1, #0
	add r0, #0x4a
	ldrb r3, [r0]
	mov r0, #8
	and r0, r3
	add r2, r1, #0
	cmp r0, #0
	beq _08018B8C
	ldrh r0, [r2, #0x16]
	mov r4, #0x10
	ldrh r3, [r2, #0x10]
	cmp r0, r3
	bne _08018BE2
	mov r4, #0x20
	b _08018BE2
	.align 2, 0
_08018B80: .4byte dword_813CF94
_08018B84: .4byte 0x03004810
_08018B88: .4byte 0x03004750
_08018B8C:
	mov r0, #0x10
	and r0, r3
	cmp r0, #0
	beq _08018B98
	mov r4, #0x70
	b _08018BE2
_08018B98:
	mov r0, ip
	add r0, #0x57
	ldrb r0, [r0]
	cmp r0, #0
	beq _08018BE2
	mov r0, ip
	add r0, #0x29
	ldrb r0, [r0]
	cmp r0, #1
	bne _08018BB8
	ldrh r0, [r2, #0x10]
	mov r4, #0x50
	cmp r0, #0
	bne _08018BE2
	mov r4, #0x60
	b _08018BE2
_08018BB8:
	cmp r0, #2
	bne _08018BE2
	ldrh r0, [r2, #0x10]
	mov r4, #0x60
	cmp r0, #0
	bne _08018BE2
	mov r4, #0x50
	b _08018BE2
_08018BC8:
	ldr r2, _08018C10
	mov r0, ip
	add r0, #0x55
	ldrb r0, [r0]
	lsl r0, r0, #1
	mov r3, sp
	add r1, r3, r0
	ldrh r0, [r2, #0x10]
	mov r4, #0x30
	ldrh r1, [r1]
	cmp r0, r1
	bne _08018BE2
	mov r4, #0x40
_08018BE2:
	ldr r1, _08018C14
	add r0, r1, #0
	add r0, #0x29
	ldrb r0, [r0]
	cmp r0, #0
	bne _08018C18
	ldr r0, [r1, #0x10]
	asr r0, r0, #8
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _08018C18
	add r0, r2, #0
	add r0, #0x4a
	ldrb r1, [r0]
	mov r0, #0x18
	and r0, r1
	cmp r0, #0
	bne _08018C18
	mov r0, #0xc7
	bl setPlayerSprite
	b _08018C54
	.align 2, 0
_08018C10: .4byte 0x03004750
_08018C14: .4byte 0x03004810
_08018C18:
	ldrh r0, [r2, #0x10]
	cmp r0, #0
	bne _08018C38
	ldr r1, _08018C34
	lsl r0, r5, #1
	add r0, r0, r1
	ldrh r0, [r0]
	add r0, r4, r0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	bl setPlayerSprite
	b _08018C4A
	.align 2, 0
_08018C34: .4byte byte_813CE1C
_08018C38:
	ldr r1, _08018C60
	lsl r0, r5, #1
	add r0, r0, r1
	ldrh r0, [r0]
	add r0, r4, r0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	bl setPlayerSprite
_08018C4A:
	cmp r4, #0x70
	bne _08018C54
	ldr r1, _08018C64
	mov r0, #0x28
	strh r0, [r1, #6]
_08018C54:
	bl sub_08018C68
	add sp, #8
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_08018C60: .4byte a_0123456789_0
_08018C64: .4byte 0x03004750

thumb_func_global sub_08018C68
sub_08018C68: @ 0x08018C68
	ldr r0, _08018C88
	ldr r0, [r0, #0x10]
	asr r0, r0, #8
	lsl r0, r0, #0x18
	mov r1, #0xa0
	lsl r1, r1, #0x13
	add r0, r0, r1
	lsr r3, r0, #0x1c
	ldr r2, _08018C8C
	ldrh r0, [r2, #0x10]
	cmp r0, #0
	bne _08018C94
	add r2, #0xb8
	ldr r1, _08018C90
	b _08018C98
	.align 2, 0
_08018C88: .4byte 0x03004810
_08018C8C: .4byte 0x03004750
_08018C90: .4byte dword_813CD1C
_08018C94:
	add r2, #0xb8
	ldr r1, _08018CA8
_08018C98:
	lsl r0, r3, #3
	add r0, r0, r1
	ldr r1, [r0, #4]
	ldr r0, [r0]
	str r0, [r2]
	str r1, [r2, #4]
	bx lr
	.align 2, 0
_08018CA8: .4byte dword_813CD9C

thumb_func_global sub_08018CAC
sub_08018CAC: @ 0x08018CAC
	ldr r0, _08018CD4
	ldr r0, [r0, #0x10]
	asr r0, r0, #8
	lsl r0, r0, #0x18
	mov r1, #0xa0
	lsl r1, r1, #0x13
	add r0, r0, r1
	lsr r2, r0, #0x1c
	add r3, r2, #0
	ldr r0, _08018CD8
	ldrh r1, [r0, #0x16]
	ldrh r0, [r0, #0x10]
	cmp r1, r0
	bne _08018CF0
	cmp r0, #0
	bne _08018CE0
	ldr r1, _08018CDC
	lsl r0, r2, #2
	b _08018D08
	.align 2, 0
_08018CD4: .4byte 0x03004810
_08018CD8: .4byte 0x03004750
_08018CDC: .4byte unk_813ce5c
_08018CE0:
	ldr r0, _08018CEC
	lsl r1, r2, #2
	add r1, r1, r0
	mov r2, #0
	ldrsh r0, [r1, r2]
	b _08018D12
	.align 2, 0
_08018CEC: .4byte unk_813ce5c
_08018CF0:
	cmp r0, #0
	beq _08018D04
	ldr r0, _08018D00
	lsl r1, r2, #2
	add r1, r1, r0
	mov r2, #0
	ldrsh r0, [r1, r2]
	b _08018D12
	.align 2, 0
_08018D00: .4byte dword_813CE9C
_08018D04:
	ldr r1, _08018D14
	lsl r0, r3, #2
_08018D08:
	add r0, r0, r1
	ldrh r0, [r0]
	neg r0, r0
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
_08018D12:
	bx lr
	.align 2, 0
_08018D14: .4byte dword_813CE9C

thumb_func_global sub_08018D18
sub_08018D18: @ 0x08018D18
	ldr r0, _08018D38
	ldr r0, [r0, #0x10]
	asr r0, r0, #8
	lsl r0, r0, #0x18
	mov r1, #0xa0
	lsl r1, r1, #0x13
	add r0, r0, r1
	lsr r2, r0, #0x1c
	ldr r1, _08018D3C
	ldrh r0, [r1, #0x16]
	ldrh r1, [r1, #0x10]
	cmp r0, r1
	beq _08018D44
	ldr r0, _08018D40
	b _08018D46
	.align 2, 0
_08018D38: .4byte 0x03004810
_08018D3C: .4byte 0x03004750
_08018D40: .4byte dword_813CE9C
_08018D44:
	ldr r0, _08018D50
_08018D46:
	lsl r1, r2, #2
	add r1, r1, r0
	mov r2, #2
	ldrsh r0, [r1, r2]
	bx lr
	.align 2, 0
_08018D50: .4byte unk_813ce5c


thumb_func_global sub_8018D54
sub_8018D54: @ 0x08018D54
	push {lr}
	ldr r1, [r0, #4]
	ldr r2, [r0, #8]
	str r1, [r0, #0xc]
	str r2, [r0, #0x10]
	bl sub_801E104
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global sub_08018D68
sub_08018D68: @ 0x08018D68
	push {lr}
	ldr r2, _08018D80
	add r1, r2, #0
	add r1, #0xb0
	mov r0, #0
	strh r0, [r1]
	ldr r0, [r2]
	mov r1, #0
	bl setObjPhysicsCallback
	pop {r0}
	bx r0
	.align 2, 0
_08018D80: .4byte 0x03004750

thumb_func_global sub_08018D84
sub_08018D84: @ 0x08018D84
	push {lr}
	ldr r2, _08018D9C
	add r1, r2, #0
	add r1, #0xb0
	mov r0, #1
	strh r0, [r1]
	ldr r0, [r2]
	ldr r1, _08018DA0
	bl setObjPhysicsCallback
	pop {r0}
	bx r0
	.align 2, 0
_08018D9C: .4byte 0x03004750
_08018DA0: .4byte (sub_80189B4+1)

	
	thumb_func_global sub_8018DA4
sub_8018DA4: @ 0x08018DA4
	push {r4, lr}
	sub sp, #8
	ldr r4, _08018DFC
	add r1, r4, #0
	add r1, #0xb0
	mov r0, #2
	strh r0, [r1]
	ldr r0, [r4]
	ldr r1, _08018E00
	bl setObjPhysicsCallback
	mov r0, #0xa
	bl sub_80190F4
	bl sub_8018B34
	mov r0, sp
	bl sub_801E35C
	ldr r2, [r4]
	ldr r0, [sp]
	ldr r1, [sp, #4]
	str r0, [r2, #4]
	str r1, [r2, #8]
	ldr r2, [r4]
	add r1, r4, #0
	add r1, #0xb8
	ldr r0, [r2, #4]
	ldr r1, [r1]
	sub r0, r0, r1
	str r0, [r2, #4]
	add r1, r4, #0
	add r1, #0xbc
	ldr r0, [r2, #8]
	ldr r1, [r1]
	sub r0, r0, r1
	str r0, [r2, #8]
	ldrh r0, [r4, #0x10]
	strh r0, [r4, #0x16]
	add sp, #8
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_08018DFC: .4byte 0x03004750
_08018E00: .4byte (sub_8018D54+1)


thumb_func_global grapplingHookDeath
grapplingHookDeath: @ 0x08018E04
	push {r4, r5, r6, r7, lr}
	sub sp, #0xc
	mov r6, #0
	ldr r0, _08018F08
	ldr r2, [r0]
	mov r0, #0x38
	ldrsh r1, [r2, r0]
	lsl r1, r1, #8
	ldr r0, [r2, #4]
	add r0, r0, r1
	str r0, [sp]
	mov r0, #0x3a
	ldrsh r1, [r2, r0]
	lsl r1, r1, #8
	ldr r0, [r2, #8]
	add r0, r0, r1
	str r0, [sp, #4]
	mov r1, sp
	ldrh r0, [r2, #0x3c]
	sub r0, #1
	strh r0, [r1, #8]
	ldrh r0, [r2, #0x3e]
	sub r0, #1
	strh r0, [r1, #0xa]
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #0x1f
	bgt _08018E64
	mov r4, sp
_08018E3E:
	mov r0, sp
	mov r1, #0
	bl sub_8017084
	neg r6, r0
	cmp r6, #0
	bne _08018F0C
	ldr r0, [sp, #4]
	mov r1, #0x80
	lsl r1, r1, #4
	add r0, r0, r1
	str r0, [sp, #4]
	ldrh r0, [r4, #0xa]
	add r0, #8
	strh r0, [r4, #0xa]
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #0x1f
	ble _08018E3E
_08018E64:
	cmp r6, #0
	bne _08018EA4
	ldr r4, _08018F08
	ldr r0, [r4]
	ldr r0, [r0, #8]
	mov r2, #0xf8
	lsl r2, r2, #5
	add r0, r0, r2
	str r0, [sp, #4]
	mov r1, sp
	mov r0, #0x1f
	strh r0, [r1, #0xa]
	mov r0, sp
	mov r1, #0
	bl sub_8017084
	neg r6, r0
	cmp r6, #0
	beq _08018EA4
	ldr r1, [r4]
	add r1, #0x51
	ldrb r2, [r1]
	mov r0, #1
	orr r0, r2
	strb r0, [r1]
	mov r0, sp
	mov r2, #0xa
	ldrsh r1, [r0, r2]
	mov r0, #0x20
	sub r0, r0, r1
	lsl r0, r0, #8
	add r6, r6, r0
_08018EA4:
	ldr r4, _08018F08
	add r0, r4, #0
	add r0, #0xb0
	mov r7, #0
	mov r1, #3
	strh r1, [r0]
	ldr r0, [r4]
	mov r1, #0
	bl setObjPhysicsCallback
	ldr r0, [r4]
	add r1, r0, #0
	add r1, #0x4d
	ldrb r5, [r1]
	cmp r5, #1
	bne _08018F30
	bl clearObjForces
	bl sub_0801E36C
	mov r0, #6
	bl sub_80190F4
	mov r0, #0xf
	bl setPlayerSprite
	mov r0, #0xa
	bl set_player_18
	mov r0, #0x2c
	mov r1, #0x10
	bl sub_80191B0
	add r0, r4, #0
	add r0, #0xb2
	strh r5, [r0]
	strh r7, [r4, #0x1e]
	sub r0, #0x12
	strh r7, [r0]
	ldr r1, [r4]
	add r0, r1, #0
	add r0, #0x51
	ldrb r0, [r0]
	and r5, r0
	cmp r5, #0
	beq _08018F3C
	ldr r0, [r1, #8]
	sub r0, r0, r6
	str r0, [r1, #8]
	b _08018F3C
	.align 2, 0
_08018F08: .4byte 0x03004750
_08018F0C:
	ldr r0, _08018F2C
	ldr r1, [r0]
	add r1, #0x51
	ldrb r2, [r1]
	mov r0, #1
	orr r0, r2
	strb r0, [r1]
	mov r0, sp
	mov r2, #0xa
	ldrsh r1, [r0, r2]
	mov r0, #0x20
	sub r0, r0, r1
	lsl r0, r0, #8
	add r6, r6, r0
	b _08018E64
	.align 2, 0
_08018F2C: .4byte 0x03004750
_08018F30:
	bl clearObjForces
	mov r0, #5
	mov r1, #0xc
	bl sub_0801C2D4
_08018F3C:
	mov r0, #0xf5
	bl playSong
	add sp, #0xc
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global sub_08018F4C
sub_08018F4C: @ 0x08018F4C
	push {r4, r5, r6, lr}
	ldr r4, _08018FA0
	add r1, r4, #0
	add r1, #0xb0
	mov r6, #0
	mov r0, #3
	strh r0, [r1]
	ldr r0, [r4]
	mov r1, #0
	bl setObjPhysicsCallback
	ldr r0, [r4]
	bl clearObjForces
	ldr r0, [r4]
	add r0, #0x4d
	ldrb r5, [r0]
	cmp r5, #1
	bne _08018FA4
	bl sub_0801E36C
	mov r0, #6
	bl sub_80190F4
	mov r0, #0xf
	bl setPlayerSprite
	mov r0, #0xa
	bl set_player_18
	mov r0, #0x2c
	mov r1, #0x10
	bl sub_80191B0
	add r0, r4, #0
	add r0, #0xb2
	strh r5, [r0]
	strh r6, [r4, #0x1e]
	sub r0, #0x12
	strh r6, [r0]
	b _08018FAC
	.align 2, 0
_08018FA0: .4byte 0x03004750
_08018FA4:
	mov r0, #5
	mov r1, #0xc
	bl sub_0801C2D4
_08018FAC:
	mov r0, #0xf5
	bl playSong
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	
	
	
thumb_func_global sub_8018FB8
sub_8018FB8: @ 0x08018FB8
	push {r4, r5, lr}
	ldr r4, _08018FEC
	add r1, r4, #0
	add r1, #0xb0
	mov r5, #0
	mov r0, #3
	strh r0, [r1]
	ldr r0, [r4]
	mov r1, #0
	bl setObjPhysicsCallback
	ldr r0, [r4]
	bl clearObjForces
	mov r0, #0
	bl sub_80190F4
	mov r0, #0
	bl setPlayerSprite
	add r4, #0xb2
	strh r5, [r4]
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_08018FEC: .4byte 0x03004750


thumb_func_global sub_08018FF0
sub_08018FF0: @ 0x08018FF0
	push {r4, r5, r6, lr}
	ldr r0, _08019014
	ldrh r1, [r0, #0x1a]
	add r6, r0, #0
	cmp r1, #0
	beq _08019052
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _08019018
	ldr r0, [r6]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	b _08019026
	.align 2, 0
_08019014: .4byte 0x03004750
_08019018:
	ldr r0, [r6]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
_08019026:
	strb r0, [r1]
	ldrh r0, [r6, #0x1a]
	sub r0, #1
	strh r0, [r6, #0x1a]
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _08019052
	ldr r0, [r6]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	add r2, r6, #0
	add r2, #0x22
	ldrb r1, [r2]
	mov r0, #9
	neg r0, r0
	and r0, r1
	strb r0, [r2]
_08019052:
	add r5, r6, #0
	ldrh r0, [r5, #6]
	ldrh r1, [r5, #0xa]
	cmp r0, r1
	beq _0801907A
	ldr r0, [r5]
	ldr r0, [r0]
	ldr r4, _080190BC
	ldrh r3, [r5, #6]
	lsl r3, r3, #3
	add r1, r3, r4
	ldr r1, [r1]
	ldrh r2, [r0, #0xc]
	add r4, #4
	add r3, r3, r4
	ldr r3, [r3]
	bl setSpriteAnimation
	ldrh r0, [r5, #6]
	strh r0, [r5, #0xa]
_0801907A:
	ldr r0, [r5]
	ldr r2, [r0]
	mov r3, #0
	ldrh r0, [r5, #0x10]
	cmp r0, #0
	bne _08019088
	mov r3, #1
_08019088:
	add r2, #0x24
	lsl r3, r3, #1
	ldrb r1, [r2]
	mov r0, #3
	neg r0, r0
	and r0, r1
	orr r0, r3
	strb r0, [r2]
	ldr r0, [r6]
	ldr r0, [r0]
	bl updateSpriteAnimation
	cmp r0, #0
	beq _080190B6
	add r0, r6, #0
	add r0, #0x23
	ldrb r0, [r0]
	lsr r0, r0, #7
	cmp r0, #0
	beq _080190B6
	ldrh r0, [r6, #0xc]
	bl setPlayerSprite
_080190B6:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_080190BC: .4byte playerAnimations

thumb_func_global set_player_18
set_player_18: @ 0x080190C0
	ldr r1, _080190C8
	strh r0, [r1, #0x18]
	bx lr
	.align 2, 0
_080190C8: .4byte 0x03004750

thumb_func_global dec_player_18
dec_player_18: @ 0x080190CC
	ldr r1, _080190DC
	ldrh r0, [r1, #0x18]
	sub r0, #1
	strh r0, [r1, #0x18]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	bx lr
	.align 2, 0
_080190DC: .4byte 0x03004750

thumb_func_global inc_player_18
inc_player_18: @ 0x080190E0
	ldr r1, _080190F0
	ldrh r0, [r1, #0x18]
	add r0, #1
	strh r0, [r1, #0x18]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	bx lr
	.align 2, 0
_080190F0: .4byte 0x03004750

thumb_func_global sub_80190F4
sub_80190F4: @ 0x080190F4
	push {r4, r5, r6, lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	add r5, r0, #0
	ldr r6, _08019130
	ldrh r0, [r6, #4]
	cmp r0, r5
	beq _08019128
	strh r0, [r6, #8]
	ldr r4, _08019134
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #3
	bne _08019118
	ldr r0, [r6]
	ldr r1, _08019138
	bl setObjCollisionCallback
_08019118:
	strh r5, [r6, #4]
	and r4, r5
	cmp r4, #3
	bne _08019128
	ldr r0, [r6]
	ldr r1, _08019138
	bl setObjCollisionCallback
_08019128:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_08019130: .4byte 0x03004750
_08019134: .4byte 0x0000FFFF
_08019138: .4byte (sub_801B8FC+1)

thumb_func_global setPlayerSprite
setPlayerSprite: @ 0x0801913C
	push {r4, r5, lr}
	sub sp, #4
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	ldr r2, _08019158
	add r0, r2, #0
	add r0, #0x24
	ldrb r4, [r0]
	lsl r0, r4, #0x1f
	cmp r0, #0
	beq _0801915C
	strh r1, [r2, #0xe]
	b _080191A4
	.align 2, 0
_08019158: .4byte 0x03004750
_0801915C:
	strh r1, [r2, #6]
	add r3, r2, #0
	add r3, #0x23
	ldrb r0, [r3]
	mov r1, #0x7f
	and r1, r0
	strb r1, [r3]
	lsl r0, r4, #0x1c
	cmp r0, #0
	bge _08019184
	ldr r0, [r2]
	mov r1, #7
	neg r1, r1
	mov r2, #0x10
	str r2, [sp]
	mov r2, #0xf
	mov r3, #0xe
	bl setObjSize
	b _080191A4
_08019184:
	ldr r0, [r2]
	ldr r1, _080191AC
	ldrh r4, [r2, #6]
	lsl r4, r4, #3
	add r4, r4, r1
	mov r2, #0
	ldrsh r1, [r4, r2]
	mov r3, #2
	ldrsh r2, [r4, r3]
	mov r5, #4
	ldrsh r3, [r4, r5]
	mov r5, #6
	ldrsh r4, [r4, r5]
	str r4, [sp]
	bl setObjSize
_080191A4:
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_080191AC: .4byte dword_813C68C

thumb_func_global sub_80191B0
sub_80191B0: @ 0x080191B0
	push {r4, lr}
	add r4, r1, #0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	bl setPlayerSprite
	ldr r1, _080191D4
	strh r4, [r1, #0xc]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x80
	orr r0, r2
	strb r0, [r1]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_080191D4: .4byte 0x03004750

thumb_func_global sub_080191D8
sub_080191D8: @ 0x080191D8
	push {r4, lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r4, _08019204
	ldrh r1, [r4, #6]
	strh r1, [r4, #0xe]
	add r4, #0x24
	ldrb r2, [r4]
	mov r1, #2
	neg r1, r1
	and r1, r2
	strb r1, [r4]
	bl setPlayerSprite
	ldrb r0, [r4]
	mov r1, #1
	orr r0, r1
	strb r0, [r4]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_08019204: .4byte 0x03004750

thumb_func_global sub_08019208
sub_08019208: @ 0x08019208
	push {r4, lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r4, _08019230
	add r4, #0x24
	ldrb r2, [r4]
	mov r1, #2
	neg r1, r1
	and r1, r2
	strb r1, [r4]
	bl setPlayerSprite
	ldrb r0, [r4]
	mov r1, #1
	orr r0, r1
	strb r0, [r4]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_08019230: .4byte 0x03004750

thumb_func_global sub_08019234
sub_08019234: @ 0x08019234
	push {lr}
	ldr r3, _08019258
	add r2, r3, #0
	add r2, #0x24
	ldrb r1, [r2]
	lsl r0, r1, #0x1f
	cmp r0, #0
	beq _08019252
	mov r0, #2
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	ldrh r0, [r3, #0xe]
	bl setPlayerSprite
_08019252:
	pop {r0}
	bx r0
	.align 2, 0
_08019258: .4byte 0x03004750

thumb_func_global sub_0801925C
sub_0801925C: @ 0x0801925C
	ldr r1, _0801929C
	ldrh r2, [r1, #0x1a]
	cmp r2, #0x62
	bls _0801929A
	ldr r0, [r1]
	add r0, #0x4d
	ldrb r3, [r0]
	cmp r3, #0
	bne _0801929A
	mov r0, #2
	and r0, r2
	add r0, #0x5a
	strh r0, [r1, #0x1a]
	mov r0, #0x23
	add r0, r0, r1
	mov ip, r0
	ldrb r2, [r0]
	lsl r0, r2, #0x1d
	cmp r0, #0
	bge _0801928E
	ldrh r0, [r1, #6]
	cmp r0, #4
	beq _0801929A
	cmp r0, #0x14
	beq _0801929A
_0801928E:
	strh r3, [r1, #0x1c]
	mov r0, #9
	neg r0, r0
	and r0, r2
	mov r1, ip
	strb r0, [r1]
_0801929A:
	bx lr
	.align 2, 0
_0801929C: .4byte 0x03004750

thumb_func_global sub_080192A0
sub_080192A0: @ 0x080192A0
	push {r4, r5, r6, r7, lr}
	ldr r4, _080192BC
	ldrh r0, [r4, #0x1c]
	cmp r0, #0
	bne _08019386
	add r0, r4, #0
	add r0, #0xb0
	ldrh r0, [r0]
	cmp r0, #0
	beq _080192C0
	cmp r0, #2
	beq _08019354
	b _08019386
	.align 2, 0
_080192BC: .4byte 0x03004750
_080192C0:
	add r0, r4, #0
	add r0, #0xb2
	ldrh r0, [r0]
	cmp r0, #0
	beq _08019386
	bl getPlayerHealth
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08019386
	bl sub_0801C1B0
	cmp r0, #0
	beq _080192EE
	mov r0, #7
	bl sub_80190F4
	mov r0, #0x10
	bl setPlayerSprite
	bl activateKillSpecial
	b _08019386
_080192EE:
	bl canActivateInvulnSpecial
	add r6, r0, #0
	cmp r6, #0
	bne _0801935C
	add r0, r4, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r7, #1
	add r0, r7, #0
	and r0, r1
	cmp r0, #0
	beq _08019386
	bl sub_0801D2DC
	add r0, r4, #0
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #0x20
	and r0, r1
	cmp r0, #0
	beq _08019322
	mov r5, #0x60
	mov r0, #0x2a
	strh r6, [r4, #0x10]
	b _08019336
_08019322:
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	beq _08019332
	mov r5, #0x20
	mov r0, #0x2a
	strh r7, [r4, #0x10]
	b _08019336
_08019332:
	mov r5, #0x40
	mov r0, #0x2b
_08019336:
	bl sub_080191D8
	mov r1, #0x90
	lsl r1, r1, #5
	mov r2, #0xa0
	lsl r2, r2, #5
	add r0, r5, #0
	bl sub_0801D3A8
	bl sub_08018D84
	mov r0, #0xf4
	bl playSong
	b _08019386
_08019354:
	bl canActivateInvulnSpecial
	cmp r0, #0
	beq _08019362
_0801935C:
	bl activateInvulnSpecial
	b _08019386
_08019362:
	add r0, r4, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _08019386
	bl sub_0801A93C
	bl grapplingHookDeath
	bl sub_0801394C
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08019386
	bl removeCameraPlayerControl
_08019386:
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0

thumb_func_global sub_0801938C
sub_0801938C: @ 0x0801938C
	push {r4, lr}
	ldr r0, _080193A0
	add r4, r0, #0
	add r4, #0xb0
	ldrh r0, [r4]
	cmp r0, #1
	beq _080193A4
	cmp r0, #3
	beq _080193BA
	b _080193CE
	.align 2, 0
_080193A0: .4byte 0x03004750
_080193A4:
	bl sub_0801D3D4
	cmp r0, #0
	beq _080193CE
	bl sub_0801D2DC
	bl sub_08019234
	bl sub_08018D68
	b _080193CE
_080193BA:
	mov r0, #0x80
	lsl r0, r0, #5
	bl sub_0801D55C
	cmp r0, #0
	beq _080193CE
	bl sub_0801D2DC
	mov r0, #0
	strh r0, [r4]
_080193CE:
	pop {r4}
	pop {r0}
	bx r0

thumb_func_global sub_080193D4
sub_080193D4: @ 0x080193D4
	push {lr}
	ldr r0, _08019408
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	cmp r0, #0
	blt _08019402
	bl sub_0801940C
	cmp r0, #0
	bne _08019402
	bl sub_0801BC90
	cmp r0, #0
	bne _08019402
	bl sub_08019590
	bl sub_08019738
	bl sub_080197B4
	bl sub_08019970
_08019402:
	pop {r0}
	bx r0
	.align 2, 0
_08019408: .4byte 0x03004750

thumb_func_global sub_0801940C
sub_0801940C: @ 0x0801940C
	push {r4, r5, r6, r7, lr}
	sub sp, #0x10
	ldr r4, _08019504
	ldrh r5, [r4, #0x10]
	mov r7, #0
	mov r6, #0x85
	lsl r6, r6, #1
	bl isUsingInvulnSpecial
	cmp r0, #0
	beq _08019424
	b _08019550
_08019424:
	add r0, r4, #0
	add r0, #0x22
	ldrb r1, [r0]
	mov r0, #0x18
	and r0, r1
	cmp r0, #0
	beq _08019434
	b _08019550
_08019434:
	ldrh r0, [r4, #4]
	cmp r0, #0
	bge _0801943C
	b _08019550
_0801943C:
	cmp r0, #0xb
	ble _08019446
	cmp r0, #0x1a
	beq _08019446
	b _08019550
_08019446:
	ldr r2, [r4]
	mov r0, #0x38
	ldrsh r1, [r2, r0]
	lsl r1, r1, #8
	ldr r0, [r2, #4]
	add r0, r0, r1
	str r0, [sp]
	mov r0, #0x3a
	ldrsh r1, [r2, r0]
	lsl r1, r1, #8
	ldr r0, [r2, #8]
	add r0, r0, r1
	str r0, [sp, #4]
	ldr r0, [r2, #0x3c]
	str r0, [sp, #8]
	mov r1, sp
	mov r0, sp
	ldrh r0, [r0, #8]
	sub r0, #1
	strh r0, [r1, #8]
	mov r0, sp
	ldrh r0, [r0, #0xa]
	sub r0, #1
	strh r0, [r1, #0xa]
	ldr r0, [sp]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	ldr r1, [sp, #4]
	lsl r1, r1, #8
	lsr r1, r1, #0x10
	mov r2, sp
	ldrh r2, [r2, #8]
	add r4, sp, #0xc
	add r3, r4, #0
	bl sub_08012CC0
	add r1, r0, #0
	cmp r1, #0
	bne _080194F8
	ldr r0, [sp]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	ldr r1, [sp, #4]
	asr r1, r1, #8
	mov r2, sp
	ldrh r2, [r2, #0xa]
	sub r1, r1, r2
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r2, sp
	ldrh r2, [r2, #8]
	add r3, r4, #0
	bl sub_08012CC0
	add r1, r0, #0
	cmp r1, #0
	bne _080194F8
	ldr r0, [sp]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	ldr r1, [sp, #4]
	lsl r1, r1, #8
	lsr r1, r1, #0x10
	mov r2, sp
	ldrh r2, [r2, #0xa]
	add r3, r4, #0
	bl sub_08012D84
	add r1, r0, #0
	cmp r1, #0
	bne _080194F8
	ldr r0, [sp]
	mov r1, sp
	asr r0, r0, #8
	ldrh r1, [r1, #8]
	add r0, r0, r1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r1, [sp, #4]
	lsl r1, r1, #8
	lsr r1, r1, #0x10
	mov r2, sp
	ldrh r2, [r2, #0xa]
	add r3, r4, #0
	bl sub_08012D84
	add r1, r0, #0
	cmp r1, #0
	beq _08019550
_080194F8:
	ldrb r0, [r4]
	cmp r0, #1
	bne _08019508
	mov r5, #1
	b _0801950E
	.align 2, 0
_08019504: .4byte 0x03004750
_08019508:
	cmp r0, #2
	bne _0801950E
	mov r5, #0
_0801950E:
	ldr r2, _08019548
	ldrh r0, [r2, #4]
	cmp r0, #9
	bne _08019518
	ldrh r5, [r2, #0x10]
_08019518:
	ldrb r3, [r4]
	sub r0, r3, #3
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #1
	bhi _08019528
	mov r7, #1
	ldr r6, _0801954C
_08019528:
	cmp r3, #5
	bne _08019532
	mov r7, #2
	mov r6, #0x86
	lsl r6, r6, #1
_08019532:
	neg r0,r1
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	add r1, r5, #0
	add r2, r7, #0
	add r3, r6, #0
	bl sub_0801BD80
	mov r0, #1
	b _08019586
	.align 2, 0
_08019548: .4byte 0x03004750
_0801954C: .4byte 0x0000010B
_08019550:
	bl getCameraField2C_bit0
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #1
	bne _08019584
	bl sub_0801CC28
	cmp r0, #0
	beq _08019584
	ldr r2, _08019580
	add r0, r2, #0
	add r0, #0x22
	ldrb r1, [r0]
	mov r0, #6
	and r0, r1
	cmp r0, #0
	bne _08019584
	ldrh r0, [r2, #0x10]
	bl sub_0801C064
	mov r0, #1
	b _08019586
	.align 2, 0
_08019580: .4byte 0x03004750
_08019584:
	mov r0, #0
_08019586:
	add sp, #0x10
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global sub_08019590
sub_08019590: @ 0x08019590
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x18
	add r4, sp, #0x10
	ldr r1, _08019624
	add r0, r4, #0
	mov r2, #4
	bl memcpy
	ldr r2, _08019628
	add r0, r2, #0
	add r0, #0x22
	ldrb r1, [r0]
	mov r0, #6
	and r0, r1
	cmp r0, #0
	beq _080195BA
	b _08019724
_080195BA:
	ldr r0, [r2]
	add r0, #0x4d
	ldrb r0, [r0]
	cmp r0, #1
	beq _080195C6
	b _08019724
_080195C6:
	ldrh r0, [r2, #4]
	cmp r0, #8
	bne _080195CE
	b _08019724
_080195CE:
	cmp r0, #0x1b
	bne _080195D4
	b _08019724
_080195D4:
	add r0, r2, #0
	add r0, #0xb0
	ldrh r0, [r0]
	cmp r0, #0
	beq _080195E0
	b _08019724
_080195E0:
	add r1, r2, #0
	add r1, #0x9c
	ldrh r0, [r2, #0x10]
	lsl r0, r0, #1
	add r0, r4, r0
	ldrh r1, [r1]
	ldrh r0, [r0]
	and r0, r1
	cmp r0, #0
	bne _080195F6
	b _08019724
_080195F6:
	mov r0, #0
	mov sl, r0
	mov r8, r2
	mov r4, r8
	add r6, sp, #4
	mov sb, r0
	mov r5, sp
_08019604:
	mov r1, r8
	ldr r2, [r1]
	mov r3, #0x38
	ldrsh r0, [r2, r3]
	lsl r0, r0, #8
	ldr r1, [r2, #4]
	add r1, r1, r0
	mov r3, r8
	ldrh r0, [r3, #0x10]
	cmp r0, #0
	beq _0801962C
	mov r3, #0x3c
	ldrsh r0, [r2, r3]
	lsl r0, r0, #8
	b _0801962E
	.align 2, 0
_08019624: .4byte dword_813CF6C
_08019628: .4byte 0x03004750
_0801962C:
	ldr r0, _080196C4
_0801962E:
	add r1, r1, r0
	str r1, [r5]
	add r3, r4, #0
	ldr r1, [r3]
	mov r0, #0x3a
	ldrsh r2, [r1, r0]
	mov r0, sb
	add r0, #4
	sub r2, r2, r0
	lsl r2, r2, #8
	ldr r1, [r1, #8]
	add r1, r1, r2
	str r1, [r6]
	ldr r0, [r5]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	lsl r1, r1, #8
	lsr r1, r1, #0x10
	str r3, [sp, #0x14]
	bl getCollision
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	lsr r7, r0, #0x14
	ldr r3, [sp, #0x14]
	cmp r7, #1
	bne _08019710
	mov r0, #0xf
	and r0, r1
	sub r0, #1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #1
	bhi _08019710
	ldrh r1, [r3, #0x1a]
	cmp r1, #0x62
	bls _08019680
	mov r0, #2
	and r0, r1
	add r0, #0x5a
	strh r0, [r3, #0x1a]
_08019680:
	bl sub_0801A93C
	ldr r0, [r4]
	bl clearObjForces
	mov r0, #8
	bl sub_80190F4
	mov r0, #0x12
	bl setPlayerSprite
	ldr r0, [r4]
	add r0, #0x4e
	strb r7, [r0]
	ldr r2, [r4]
	ldr r0, [r2, #4]
	ldr r1, [r2, #8]
	str r0, [r4, #0x34]
	str r1, [r4, #0x38]
	ldr r0, [r6]
	ldr r1, _080196C8
	bic r0, r1
	add r1, #1
	add r0, r0, r1
	str r0, [r6]
	ldrh r0, [r4, #0x10]
	cmp r0, #0
	bne _080196CC
	ldr r0, [r5]
	mov r3, #0x80
	lsl r3, r3, #2
	add r0, r0, r3
	b _080196D2
	.align 2, 0
_080196C4: .4byte 0xFFFFFF00
_080196C8: .4byte 0x000007FF
_080196CC:
	ldr r0, [r5]
	ldr r1, _08019704
	add r0, r0, r1
_080196D2:
	str r0, [r2, #4]
	mov r2, r8
	ldr r1, [r2]
	ldr r0, [r6]
	ldr r3, _08019708
	add r0, r0, r3
	str r0, [r1, #8]
	mov r1, r8
	add r1, #0xb2
	mov r0, #0
	strh r0, [r1]
	ldr r0, [r5]
	ldr r1, [r5, #4]
	str r0, [r2, #0x2c]
	str r1, [r2, #0x30]
	ldr r0, [r6]
	ldr r3, _0801970C
	add r0, r0, r3
	str r0, [r2, #0x38]
	mov r0, #0xff
	bl playSong
	mov r0, #1
	b _08019726
	.align 2, 0
_08019704: .4byte 0xFFFFFE00
_08019708: .4byte 0xFFFFD500
_0801970C: .4byte 0xFFFFDC00
_08019710:
	add r6, #8
	mov r0, #8
	add sb, r0
	add r5, #8
	mov r1, #1
	add sl, r1
	mov r2, sl
	cmp r2, #1
	bgt _08019724
	b _08019604
_08019724:
	mov r0, #0
_08019726:
	add sp, #0x18
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global sub_08019738
sub_08019738: @ 0x08019738
	push {lr}
	ldr r0, _08019750
	ldrh r1, [r0, #4]
	add r2, r0, #0
	cmp r1, #0x12
	bhi _080197AC
	lsl r0, r1, #2
	ldr r1, _08019754
	add r0, r0, r1
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
_08019750: .4byte 0x03004750
_08019754: .4byte _08019758
_08019758: @ jump table
	.4byte _080197A4 @ case 0
	.4byte _080197A4 @ case 1
	.4byte _080197A4 @ case 2
	.4byte _080197A4 @ case 3
	.4byte _080197A4 @ case 4
	.4byte _080197AC @ case 5
	.4byte _080197AC @ case 6
	.4byte _080197AC @ case 7
	.4byte _080197AC @ case 8
	.4byte _080197AC @ case 9
	.4byte _080197AC @ case 10
	.4byte _080197A4 @ case 11
	.4byte _080197AC @ case 12
	.4byte _080197AC @ case 13
	.4byte _080197A4 @ case 14
	.4byte _080197AC @ case 15
	.4byte _080197AC @ case 16
	.4byte _080197A4 @ case 17
	.4byte _080197A4 @ case 18
_080197A4:
	ldr r0, [r2]
	bl tryApplyConveyorMovement
	b _080197AE
_080197AC:
	mov r0, #0
_080197AE:
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global sub_080197B4
sub_080197B4: @ 0x080197B4
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	sub sp, #0x10
	ldr r1, _08019874
	mov r0, sp
	mov r2, #8
	bl memcpy
	add r0, sp, #8
	mov r8, r0
	ldr r1, _08019878
	mov r2, #8
	bl memcpy
	ldr r4, _0801987C
	mov r1, #0x22
	add r1, r1, r4
	mov sb, r1
	ldrb r0, [r1]
	lsr r0, r0, #7
	cmp r0, #0
	beq _080197E6
	b _080198E6
_080197E6:
	ldrh r0, [r4, #4]
	cmp r0, #0
	bne _080198E6
	ldr r1, [r4]
	ldr r0, [r1, #4]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	ldr r1, [r1, #8]
	asr r1, r1, #8
	sub r1, #1
	lsl r6, r0, #0x10
	lsr r0, r6, #0x10
	lsl r5, r1, #0x10
	lsr r7, r5, #0x10
	add r1, r7, #0
	bl sub_08012970
	cmp r0, #0
	bne _080198E6
	asr r6, r6, #0x10
	asr r5, r5, #0x10
	add r0, r6, #0
	add r1, r5, #0
	bl sub_080198F8
	cmp r0, #0
	bne _080198E6
	ldr r0, [r4]
	ldrh r0, [r0, #0x38]
	add r0, r6, r0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	add r1, r7, #0
	bl sub_080129B8
	cmp r0, #0
	bne _08019844
	ldr r0, [r4]
	ldrh r0, [r0, #0x38]
	add r0, r6, r0
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	add r1, r5, #0
	bl sub_080198F8
	cmp r0, #0
	beq _08019880
_08019844:
	add r0, r4, #0
	add r0, #0x20
	ldrb r1, [r0]
	add r2, r1, #1
	strb r2, [r0]
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	cmp r1, #0x3c
	bls _080198E6
	mov r2, sb
	ldrb r0, [r2]
	mov r1, #0x80
	orr r0, r1
	strb r0, [r2]
	ldrh r1, [r4, #0x10]
	lsl r1, r1, #1
	mov r2, sp
	add r0, r2, r1
	ldrh r0, [r0]
	add r1, r8
	ldrh r1, [r1]
	bl sub_80191B0
	b _080198E6
	.align 2, 0
_08019874: .4byte unk_813cf9a
_08019878: .4byte unk_813cfa2
_0801987C: .4byte 0x03004750
_08019880:
	ldr r1, [r4]
	ldrh r0, [r1, #0x38]
	add r0, r0, r6
	ldrh r1, [r1, #0x3c]
	add r0, r0, r1
	sub r0, #1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	add r1, r7, #0
	bl sub_080129B8
	cmp r0, #0
	bne _080198B4
	ldr r1, [r4]
	ldrh r0, [r1, #0x38]
	add r0, r0, r6
	ldrh r1, [r1, #0x3c]
	add r0, r0, r1
	sub r0, #1
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	add r1, r5, #0
	bl sub_080198F8
	cmp r0, #0
	beq _080198E6
_080198B4:
	add r0, r4, #0
	add r0, #0x20
	ldrb r1, [r0]
	add r2, r1, #1
	strb r2, [r0]
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	cmp r1, #0x3c
	bls _080198E6
	mov r1, sb
	ldrb r0, [r1]
	mov r1, #0x80
	orr r0, r1
	mov r2, sb
	strb r0, [r2]
	ldrh r1, [r4, #0x10]
	lsl r1, r1, #1
	add r1, #4
	mov r2, sp
	add r0, r2, r1
	ldrh r0, [r0]
	add r1, r8
	ldrh r1, [r1]
	bl sub_80191B0
_080198E6:
	mov r0, #0
	add sp, #0x10
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global sub_080198F8
sub_080198F8: @ 0x080198F8
	push {r4, r5, r6, r7, lr}
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	lsl r1, r1, #0x10
	lsr r7, r1, #0x10
	ldr r0, _08019958
	ldr r5, [r0, #4]
	add r0, #0x74
	cmp r5, r0
	beq _08019964
	lsl r0, r2, #0x10
	asr r6, r0, #0x10
_08019910:
	ldr r4, [r5, #0x3c]
	mov r1, #0x38
	ldrsh r0, [r4, r1]
	lsl r0, r0, #8
	ldr r3, [r4, #4]
	add r3, r3, r0
	lsl r3, r3, #8
	mov r1, #0x3a
	ldrsh r0, [r4, r1]
	lsl r0, r0, #8
	ldr r2, [r4, #8]
	add r2, r2, r0
	lsl r2, r2, #8
	ldrh r1, [r4, #0x3c]
	asr r3, r3, #0x10
	add r1, r3, r1
	ldrh r0, [r4, #0x3e]
	asr r2, r2, #0x10
	sub r0, r2, r0
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	cmp r6, r1
	bgt _0801995C
	lsl r0, r7, #0x10
	lsl r1, r4, #0x10
	asr r4, r0, #0x10
	cmp r0, r1
	blt _0801995C
	cmp r6, r3
	blt _0801995C
	cmp r4, r2
	bgt _0801995C
	mov r0, #1
	b _08019966
	.align 2, 0
_08019958: .4byte 0x03004880
_0801995C:
	ldr r5, [r5, #4]
	ldr r0, _0801996C
	cmp r5, r0
	bne _08019910
_08019964:
	mov r0, #0
_08019966:
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
_0801996C: .4byte 0x030048F4

thumb_func_global sub_08019970
sub_08019970: @ 0x08019970
	push {r4, lr}
	ldr r4, _080199B4
	ldr r1, [r4]
	ldr r0, [r1, #4]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	ldr r1, [r1, #8]
	lsl r1, r1, #8
	lsr r1, r1, #0x10
	bl getCollision
	lsl r1, r0, #0x10
	mov r0, #0xf0
	lsl r0, r0, #0xc
	and r0, r1
	lsr r0, r0, #0x10
	cmp r0, #1
	bne _080199AA
	lsr r0, r1, #0x14
	cmp r0, #0
	bne _080199AA
	ldr r0, [r4]
	ldr r0, [r0, #0x1c]
	cmp r0, #0
	bge _080199AA
	mov r0, #0x89
	lsl r0, r0, #1
	bl playSong
_080199AA:
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
_080199B4: .4byte 0x03004750

thumb_func_global sub_080199B8
sub_080199B8: @ 0x080199B8
	push {lr}
	ldr r0, _080199E8
	ldr r1, [r0]
	ldr r0, [r1, #4]
	lsl r0, r0, #8
	ldr r1, [r1, #8]
	asr r1, r1, #8
	sub r1, #1
	lsr r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl getCollision
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	lsr r0, r0, #0x14
	cmp r0, #1
	bne _080199EC
	mov r0, #0xf
	and r0, r1
	cmp r0, #5
	bne _080199EC
	mov r0, #1
	b _080199EE
	.align 2, 0
_080199E8: .4byte 0x03004750
_080199EC:
	mov r0, #0
_080199EE:
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global sub_080199F4
sub_080199F4: @ 0x080199F4
	push {r4, r5, r6, r7, lr}
	add r6, r0, #0
	mov r5, #0
	ldr r7, _08019A18
	ldr r4, _08019A1C
	ldrh r0, [r4, #4]
	cmp r0, #9
	beq _08019A66
	cmp r1, #1
	bne _08019A38
	ldr r0, [r6, #0x3c]
	ldr r0, [r0, #0x18]
	cmp r0, #0
	beq _08019A20
	cmp r0, #0
	blt _08019A2E
	b _08019A5A
	.align 2, 0
_08019A18: .4byte 0x00000109
_08019A1C: .4byte 0x03004750
_08019A20:
	add r0, r6, #0
	add r0, #0x67
	ldrb r0, [r0]
	mov r2, #0xf
	and r2, r0
	cmp r2, #0
	bne _08019A32
_08019A2E:
	strh r1, [r4, #0x10]
	b _08019A66
_08019A32:
	cmp r2, #1
	beq _08019A5A
	b _08019A5E
_08019A38:
	ldr r2, [r6, #0x3c]
	ldr r0, [r4]
	ldr r1, [r2, #4]
	ldr r0, [r0, #4]
	cmp r1, r0
	ble _08019A48
	mov r0, #1
	b _08019A64
_08019A48:
	cmp r1, r0
	blt _08019A5A
	ldr r0, [r2, #0x18]
	cmp r0, #0
	beq _08019A5E
	cmp r0, #0
	bge _08019A5A
	mov r0, #1
	b _08019A64
_08019A5A:
	strh r5, [r4, #0x10]
	b _08019A66
_08019A5E:
	mov r0, #2
	bl random
_08019A64:
	strh r0, [r4, #0x10]
_08019A66:
	add r0, r6, #0
	add r0, #0x5a
	ldrh r0, [r0]
	sub r0, #0x4c
	cmp r0, #0x40
	bls _08019A74
	b _08019B94
_08019A74:
	lsl r0, r0, #2
	ldr r1, _08019A80
	add r0, r0, r1
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
_08019A80: .4byte _08019A84
_08019A84: @ jump table
	.4byte _08019B8C @ case 0
	.4byte _08019B88 @ case 1
	.4byte _08019B88 @ case 2
	.4byte _08019B8C @ case 3
	.4byte _08019B8C @ case 4
	.4byte _08019B8C @ case 5
	.4byte _08019B94 @ case 6
	.4byte _08019B88 @ case 7
	.4byte _08019B88 @ case 8
	.4byte _08019B94 @ case 9
	.4byte _08019B94 @ case 10
	.4byte _08019B94 @ case 11
	.4byte _08019B94 @ case 12
	.4byte _08019B94 @ case 13
	.4byte _08019B94 @ case 14
	.4byte _08019B8C @ case 15
	.4byte _08019B94 @ case 16
	.4byte _08019B94 @ case 17
	.4byte _08019B94 @ case 18
	.4byte _08019B94 @ case 19
	.4byte _08019B88 @ case 20
	.4byte _08019B88 @ case 21
	.4byte _08019B8C @ case 22
	.4byte _08019B8C @ case 23
	.4byte _08019B8C @ case 24
	.4byte _08019B94 @ case 25
	.4byte _08019B94 @ case 26
	.4byte _08019B94 @ case 27
	.4byte _08019B8C @ case 28
	.4byte _08019B8C @ case 29
	.4byte _08019B8C @ case 30
	.4byte _08019B8C @ case 31
	.4byte _08019B94 @ case 32
	.4byte _08019B88 @ case 33
	.4byte _08019B94 @ case 34
	.4byte _08019B94 @ case 35
	.4byte _08019B94 @ case 36
	.4byte _08019B94 @ case 37
	.4byte _08019B94 @ case 38
	.4byte _08019B94 @ case 39
	.4byte _08019B94 @ case 40
	.4byte _08019B94 @ case 41
	.4byte _08019B94 @ case 42
	.4byte _08019B94 @ case 43
	.4byte _08019B94 @ case 44
	.4byte _08019B94 @ case 45
	.4byte _08019B94 @ case 46
	.4byte _08019B94 @ case 47
	.4byte _08019B94 @ case 48
	.4byte _08019B94 @ case 49
	.4byte _08019B94 @ case 50
	.4byte _08019B94 @ case 51
	.4byte _08019B94 @ case 52
	.4byte _08019B94 @ case 53
	.4byte _08019B94 @ case 54
	.4byte _08019B94 @ case 55
	.4byte _08019B94 @ case 56
	.4byte _08019B94 @ case 57
	.4byte _08019B94 @ case 58
	.4byte _08019B94 @ case 59
	.4byte _08019B94 @ case 60
	.4byte _08019B94 @ case 61
	.4byte _08019B8C @ case 62
	.4byte _08019B8C @ case 63
	.4byte _08019B8C @ case 64
_08019B88:
	mov r5, #1
	b _08019B98
_08019B8C:
	mov r5, #2
	mov r7, #0x86
	lsl r7, r7, #1
	b _08019B98
_08019B94:
	mov r5, #0
	ldr r7, _08019BC0
_08019B98:
	ldr r3, _08019BC4
	ldr r2, _08019BC8
	ldr r0, [r6, #8]
	ldrh r1, [r0, #0x1c]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r2, [r2, #0x11]
	add r0, r0, r2
	add r0, r0, r3
	ldrb r0, [r0]
	neg r0, r0
	ldr r1, _08019BCC
	ldrh r1, [r1, #0x10]
	add r2, r5, #0
	add r3, r7, #0
	bl sub_0801BD80
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_08019BC0: .4byte 0x00000109
_08019BC4: .4byte byte_81428D4
_08019BC8: .4byte 0x03003D40
_08019BCC: .4byte 0x03004750

thumb_func_global sub_08019BD0
sub_08019BD0: @ 0x08019BD0
	push {r4, r5, r6, lr}
	bl isUsingInvulnSpecial
	cmp r0, #0
	bne _08019C9C
	ldr r5, _08019C38
	add r6, r5, #0
	add r6, #0x22
	ldrb r1, [r6]
	mov r0, #0x18
	and r0, r1
	cmp r0, #0
	bne _08019C9C
	ldr r0, [r5]
	bl sub_0801F34C
	add r4, r0, #0
	cmp r4, #0
	beq _08019C44
	add r0, #0x5a
	ldrh r1, [r0]
	cmp r1, #0x65
	bne _08019C0C
	ldrb r0, [r6]
	lsl r0, r0, #0x1a
	cmp r0, #0
	blt _08019C44
	ldrh r0, [r5, #4]
	cmp r0, #3
	beq _08019C44
_08019C0C:
	cmp r1, #0x53
	bne _08019C18
	ldrb r0, [r6]
	lsl r0, r0, #0x1a
	cmp r0, #0
	blt _08019C44
_08019C18:
	ldr r3, _08019C3C
	ldr r2, _08019C40
	ldr r0, [r4, #8]
	ldrh r1, [r0, #0x1c]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r2, [r2, #0x11]
	add r0, r0, r2
	add r0, r0, r3
	ldrb r0, [r0]
	cmp r0, #0
	beq _08019C44
	add r0, r4, #0
	mov r1, #1
	b _08019C88
	.align 2, 0
_08019C38: .4byte 0x03004750
_08019C3C: .4byte byte_81428D4
_08019C40: .4byte 0x03003D40
_08019C44:
	ldr r5, _08019C90
	ldr r0, [r5]
	bl sub_0801F300
	add r4, r0, #0
	cmp r4, #0
	beq _08019C9C
	add r0, #0x5a
	ldrh r0, [r0]
	cmp r0, #0x8f
	beq _08019C6C
	ldrh r0, [r5, #4]
	cmp r0, #3
	beq _08019C9C
	add r0, r5, #0
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x1a
	cmp r0, #0
	blt _08019C9C
_08019C6C:
	ldr r3, _08019C94
	ldr r2, _08019C98
	ldr r0, [r4, #8]
	ldrh r1, [r0, #0x1c]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r2, [r2, #0x11]
	add r0, r0, r2
	add r0, r0, r3
	ldrb r0, [r0]
	cmp r0, #0
	beq _08019C9C
	add r0, r4, #0
	mov r1, #0
_08019C88:
	bl sub_080199F4
	mov r0, #1
	b _08019C9E
	.align 2, 0
_08019C90: .4byte 0x03004750
_08019C94: .4byte byte_81428D4
_08019C98: .4byte 0x03003D40
_08019C9C:
	mov r0, #0
_08019C9E:
	pop {r4, r5, r6}
	pop {r1}
	bx r1

thumb_func_global sub_08019CA4
sub_08019CA4: @ 0x08019CA4
	push {r4, r5, r6, lr}
	mov r6, #0
	bl getPlayerHealth
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _08019CBE
	ldr r0, _08019D30
	add r0, #0x22
	ldrb r1, [r0]
	mov r2, #0x10
	orr r1, r2
	strb r1, [r0]
_08019CBE:
	ldr r4, _08019D30
	ldrh r0, [r4, #4]
	cmp r0, #0xc
	beq _08019D94
	cmp r0, #9
	beq _08019D94
	add r5, r4, #0
	add r5, #0x22
	ldrb r1, [r5]
	mov r0, #0x42
	and r0, r1
	cmp r0, #0
	bne _08019D94
	add r0, r4, #0
	add r0, #0x4a
	ldrb r1, [r0]
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	bne _08019D94
	ldr r0, [r4]
	add r0, #0x4d
	ldrb r0, [r0]
	cmp r0, #0
	bne _08019D94
	bl getPlayerHealth
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _08019D94
	bl sub_0801A93C
	mov r0, #0xe
	bl sub_80190F4
	bl sub_08032218
	cmp r0, #0
	bne _08019D50
	ldrb r0, [r5]
	lsl r0, r0, #0x1f
	cmp r0, #0
	beq _08019D34
	add r0, r4, #0
	add r0, #0x23
	ldrb r1, [r0]
	lsl r1, r1, #0x1d
	lsr r0, r1, #0x1f
	add r0, #0xb8
	lsr r1, r1, #0x1f
	add r1, #0xb8
	bl sub_80191B0
	mov r0, #0xc
	bl set_player_18
	b _08019D58
	.align 2, 0
_08019D30: .4byte 0x03004750
_08019D34:
	add r0, r4, #0
	add r0, #0x23
	ldrb r1, [r0]
	lsl r1, r1, #0x1d
	lsr r0, r1, #0x1f
	add r0, #0xb6
	lsr r1, r1, #0x1f
	add r1, #0xb6
	bl sub_80191B0
	mov r0, #0x12
	bl set_player_18
	b _08019D58
_08019D50:
	mov r0, #0xc8
	mov r1, #0xc8
	bl sub_80191B0
_08019D58:
	ldr r4, _08019D9C
	ldr r0, [r4]
	bl clearObjForces
	add r1, r4, #0
	add r1, #0xb2
	mov r0, #0
	strh r0, [r1]
	strh r0, [r4, #0x1a]
	ldr r0, [r4]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	mov r0, #0
	mov r1, #8
	bl fadeoutSong
	ldr r0, _08019DA0
	bl playSong
	add r4, #0x22
	ldrb r0, [r4]
	mov r1, #2
	orr r0, r1
	strb r0, [r4]
	mov r6, #1
_08019D94:
	add r0, r6, #0
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
_08019D9C: .4byte 0x03004750
_08019DA0: .4byte 0x0000010D

thumb_func_global sub_08019DA4
sub_08019DA4: @ 0x08019DA4
	push {r4, lr}
	sub sp, #0x54
	add r1, r0, #0
	mov r0, sp
	mov r2, #0x54
	bl memcpy
	mov r1, sp
	mov r0, sp
	ldrh r0, [r0, #0x38]
	sub r0, #8
	strh r0, [r1, #0x38]
	mov r0, sp
	ldrh r0, [r0, #0x3c]
	add r0, #0x10
	strh r0, [r1, #0x3c]
	ldr r0, _08019E00
	ldr r4, [r0, #4]
	add r0, #0x74
	cmp r4, r0
	beq _08019E0C
_08019DCE:
	add r0, r4, #0
	add r0, #0x5c
	ldrh r0, [r0]
	cmp r0, #1
	bne _08019E04
	add r0, r4, #0
	add r0, #0x66
	ldrb r0, [r0]
	cmp r0, #0
	beq _08019E04
	add r0, r4, #0
	add r0, #0x44
	ldrb r0, [r0]
	lsl r0, r0, #0x1a
	cmp r0, #0
	blt _08019E04
	ldr r1, [r4, #0x3c]
	mov r0, sp
	bl collide
	cmp r0, #0
	beq _08019E04
	add r0, r4, #0
	b _08019E0E
	.align 2, 0
_08019E00: .4byte 0x03004EE0
_08019E04:
	ldr r4, [r4, #4]
	ldr r0, _08019E18
	cmp r4, r0
	bne _08019DCE
_08019E0C:
	mov r0, #0
_08019E0E:
	add sp, #0x54
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
_08019E18: .4byte 0x03004F54

thumb_func_global sub_08019E1C
sub_08019E1C: @ 0x08019E1C
	push {r4, lr}
	sub sp, #0x54
	add r1, r0, #0
	mov r0, sp
	mov r2, #0x54
	bl memcpy
	mov r1, sp
	mov r0, sp
	ldrh r0, [r0, #0x38]
	sub r0, #8
	strh r0, [r1, #0x38]
	mov r0, sp
	ldrh r0, [r0, #0x3c]
	add r0, #0x10
	strh r0, [r1, #0x3c]
	ldr r0, _08019E6C
	ldr r4, [r0, #4]
	add r0, #0x74
	cmp r4, r0
	beq _08019E78
_08019E46:
	add r0, r4, #0
	add r0, #0x66
	ldrb r0, [r0]
	cmp r0, #0
	beq _08019E70
	add r0, r4, #0
	add r0, #0x5a
	ldrh r0, [r0]
	cmp r0, #0x65
	bne _08019E70
	ldr r1, [r4, #0x3c]
	mov r0, sp
	bl collide
	cmp r0, #0
	beq _08019E70
	add r0, r4, #0
	b _08019E7A
	.align 2, 0
_08019E6C: .4byte 0x03005A40
_08019E70:
	ldr r4, [r4, #4]
	ldr r0, _08019E84
	cmp r4, r0
	bne _08019E46
_08019E78:
	mov r0, #0
_08019E7A:
	add sp, #0x54
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
_08019E84: .4byte 0x03005AB4

thumb_func_global sub_08019E88
sub_08019E88: @ 0x08019E88
	push {r4, r5, r6, lr}
	add r6, r0, #0
	ldr r0, _08019ECC
	ldr r2, [r0]
	ldr r1, [r2, #4]
	asr r1, r1, #8
	ldrh r0, [r2, #0x38]
	add r1, r1, r0
	ldrh r0, [r2, #0x3c]
	lsl r4, r1, #0x10
	asr r5, r4, #0x10
	add r0, r5, r0
	sub r0, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	ldr r0, [r2, #8]
	asr r0, r0, #8
	sub r0, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	add r2, r1, #0
	cmp r6, #0
	ble _08019ED0
	lsl r5, r3, #0x10
	lsr r0, r5, #0x10
	lsl r4, r1, #0x10
	lsr r1, r4, #0x10
	bl sub_08012970
	cmp r0, #0
	bne _08019EEE
	asr r0, r5, #0x10
	asr r1, r4, #0x10
	b _08019EE6
	.align 2, 0
_08019ECC: .4byte 0x03004750
_08019ED0:
	cmp r6, #0
	bge _08019F0A
	lsr r0, r4, #0x10
	lsl r4, r2, #0x10
	lsr r1, r4, #0x10
	bl sub_08012970
	cmp r0, #0
	bne _08019EEE
	asr r1, r4, #0x10
	add r0, r5, #0
_08019EE6:
	bl sub_080198F8
	cmp r0, #0
	beq _08019F0A
_08019EEE:
	ldr r4, _08019F10
	ldr r0, [r4]
	bl sub_08019DA4
	cmp r0, #0
	beq _08019F20
	cmp r6, #0
	ble _08019F14
	ldr r0, [r0, #0x3c]
	ldr r1, [r4]
	ldr r2, [r0, #4]
	ldr r0, [r1, #4]
	cmp r2, r0
	blt _08019F20
_08019F0A:
	mov r0, #0
	b _08019F52
	.align 2, 0
_08019F10: .4byte 0x03004750
_08019F14:
	ldr r0, [r0, #0x3c]
	ldr r1, [r4]
	ldr r2, [r0, #4]
	ldr r0, [r1, #4]
	cmp r2, r0
	ble _08019F0A
_08019F20:
	ldr r4, _08019F40
	ldr r0, [r4]
	bl sub_08019E1C
	cmp r0, #0
	beq _08019F50
	cmp r6, #0
	ble _08019F44
	ldr r0, [r0, #0x3c]
	ldr r1, [r4]
	ldr r2, [r0, #4]
	ldr r0, [r1, #4]
	cmp r2, r0
	blt _08019F50
	b _08019F0A
	.align 2, 0
_08019F40: .4byte 0x03004750
_08019F44:
	ldr r0, [r0, #0x3c]
	ldr r1, [r4]
	ldr r2, [r0, #4]
	ldr r0, [r1, #4]
	cmp r2, r0
	ble _08019F0A
_08019F50:
	mov r0, #1
_08019F52:
	pop {r4, r5, r6}
	pop {r1}
	bx r1

thumb_func_global sub_08019F58
sub_08019F58: @ 0x08019F58
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x10
	lsl r0, r0, #0x18
	lsr r7, r0, #0x18
	ldr r1, _0801A048
	add r0, sp, #8
	mov r2, #4
	bl memcpy
	add r4, sp, #0xc
	ldr r1, _0801A04C
	add r0, r4, #0
	mov r2, #4
	bl memcpy
	ldr r1, _0801A050
	add r3, r1, #0
	add r3, #0x4a
	ldrb r2, [r3]
	mov r0, #0x10
	and r0, r2
	mov sl, r4
	cmp r0, #0
	bne _08019F92
	b _0801A5D0
_08019F92:
	add r0, r1, #0
	add r0, #0x47
	strb r7, [r0]
	cmp r7, #3
	beq _08019FB0
	add r0, #0x53
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #1
	and r0, r1
	cmp r0, #0
	beq _08019FB0
	mov r0, #2
	orr r0, r2
	strb r0, [r3]
_08019FB0:
	cmp r7, #0
	beq _08019FB8
	cmp r7, #2
	bne _0801A012
_08019FB8:
	ldr r5, _0801A050
	ldr r0, [r5]
	ldr r0, [r0]
	mov r1, #0x20
	ldrsh r0, [r0, r1]
	cmp r0, #1
	bne _0801A012
	ldr r2, _0801A054
	add r0, r5, #0
	add r0, #0x44
	ldrb r1, [r0]
	lsl r1, r1, #2
	add r1, r1, r2
	sub r0, #0x21
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1f
	ldr r1, [r1]
	lsl r0, r0, #2
	add r0, r0, r1
	ldrh r2, [r5, #0x10]
	ldr r0, [r0]
	lsl r2, r2, #2
	add r2, r2, r0
	add r3, r5, #0
	add r3, #0x48
	ldrb r0, [r3]
	add r1, r0, #1
	strb r1, [r3]
	lsl r0, r0, #0x18
	ldr r1, [r2]
	lsr r0, r0, #0x17
	add r0, r0, r1
	mov r2, #0
	ldrsh r0, [r0, r2]
	lsl r4, r0, #8
	add r0, r4, #0
	bl sub_08019E88
	cmp r0, #0
	beq _0801A012
	ldr r0, [r5]
	add r1, r4, #0
	bl setObjVelocity_X
_0801A012:
	cmp r7, #1
	bne _0801A018
	b _0801A124
_0801A018:
	cmp r7, #3
	beq _0801A01E
	b _0801A374
_0801A01E:
	ldr r4, _0801A050
	ldr r0, [r4]
	ldr r1, [r0, #0x1c]
	add r1, #0x10
	str r1, [r0, #0x1c]
	add r1, r4, #0
	add r1, #0x45
	ldrb r0, [r1]
	sub r0, #1
	mov r6, #0
	strb r0, [r1]
	lsl r0, r0, #0x18
	asr r5, r0, #0x18
	cmp r5, #6
	beq _0801A08A
	cmp r5, #6
	bgt _0801A058
	cmp r5, #0
	beq _0801A09C
	b _0801A5BE
	.align 2, 0
_0801A048: .4byte dword_813CF80
_0801A04C: .4byte unk_813D1A2
_0801A050: .4byte 0x03004750
_0801A054: .4byte off_813D108
_0801A058:
	cmp r5, #0x10
	beq _0801A05E
	b _0801A5BE
_0801A05E:
	str r6, [sp]
	str r6, [sp, #4]
	add r0, r4, #0
	mov r1, #0x43
	mov r2, #0
	mov r3, #0xa
	bl createPlayerAttackObj
	str r0, [r4, #0x40]
	cmp r0, #0
	bne _0801A076
	b _0801A48A
_0801A076:
	add r1, r0, #0
	add r1, #0x68
	mov r0, #2
	strb r0, [r1]
	ldr r3, [r4, #0x40]
	ldr r2, [r3, #0x44]
	lsl r1, r2, #5
	lsr r1, r1, #0x10
	mov r0, #4
	b _0801A47E
_0801A08A:
	ldr r0, [r4, #0x40]
	cmp r0, #0
	bne _0801A092
	b _0801A5BE
_0801A092:
	bl destroyObject
	str r6, [r4, #0x40]
	add r2, r4, #0
	b _0801A262
_0801A09C:
	add r2, r4, #0
	add r2, #0x44
	ldrb r6, [r2]
	mov r0, #0x22
	add r0, r0, r4
	mov ip, r0
	ldrb r1, [r0]
	mov r3, #0x21
	neg r3, r3
	add r0, r3, #0
	and r0, r1
	mov r1, ip
	strb r0, [r1]
	mov r0, #0x4a
	add r0, r0, r4
	mov ip, r0
	ldrb r0, [r0]
	mov r1, #1
	orr r0, r1
	mov r1, #0x46
	add r1, r1, r4
	mov r8, r1
	strb r5, [r1]
	strb r5, [r2]
	mov r1, #3
	neg r1, r1
	and r0, r1
	sub r1, #2
	and r0, r1
	sub r1, #4
	and r0, r1
	sub r1, #8
	and r0, r1
	and r0, r3
	mov r2, ip
	strb r0, [r2]
	ldr r0, [r4, #0x40]
	cmp r0, #0
	beq _0801A0F0
	bl destroyObject
	str r5, [r4, #0x40]
_0801A0F0:
	bl sub_08019234
	ldr r0, _0801A11C
	lsl r1, r6, #2
	add r1, r1, r0
	add r0, r4, #0
	add r0, #0x23
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1f
	ldr r1, [r1]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldrh r0, [r0, #6]
	ldr r1, _0801A120
	ldrh r1, [r1, #6]
	bl sub_80191B0
	mov r4, r8
	strb r7, [r4]
	b _0801A5BE
	.align 2, 0
_0801A11C: .4byte off_813D188
_0801A120: .4byte dword_813D190
_0801A124:
	ldr r1, _0801A144
	add r0, r1, #0
	add r0, #0x45
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	add r5, r1, #0
	cmp r0, #0xd
	ble _0801A148
	add r2, r5, #0
	add r2, #0x4a
	ldrb r1, [r2]
	mov r0, #3
	neg r0, r0
	and r0, r1
	b _0801A162
	.align 2, 0
_0801A144: .4byte 0x03004750
_0801A148:
	add r0, r5, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #1
	and r0, r1
	cmp r0, #0
	beq _0801A164
	add r2, r5, #0
	add r2, #0x4a
	ldrb r0, [r2]
	mov r1, #2
	orr r0, r1
_0801A162:
	strb r0, [r2]
_0801A164:
	add r1, r5, #0
	add r1, #0x45
	ldrb r0, [r1]
	sub r0, #1
	mov r6, #0
	strb r0, [r1]
	lsl r0, r0, #0x18
	asr r4, r0, #0x18
	cmp r4, #8
	beq _0801A260
	cmp r4, #8
	bgt _0801A182
	cmp r4, #0
	beq _0801A270
	b _0801A27C
_0801A182:
	cmp r4, #0x11
	bne _0801A27C
	ldr r0, _0801A244
	mov r1, #0x23
	add r1, r1, r5
	mov r8, r1
	ldrb r2, [r1]
	lsl r2, r2, #0x19
	lsr r2, r2, #0x1f
	lsl r2, r2, #2
	add r0, r2, r0
	ldr r0, [r0]
	lsl r3, r7, #1
	add r0, r3, r0
	ldrh r1, [r0]
	ldr r0, _0801A248
	add r2, r2, r0
	ldrh r0, [r5, #0x10]
	ldr r2, [r2]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r0, [r0]
	add r0, r3, r0
	mov r4, #0
	ldrsh r2, [r0, r4]
	ldr r0, _0801A24C
	add r3, r3, r0
	mov r0, #0
	ldrsh r3, [r3, r0]
	str r6, [sp]
	str r6, [sp, #4]
	add r0, r5, #0
	bl createPlayerAttackObj
	str r0, [r5, #0x40]
	cmp r0, #0
	bne _0801A1CE
	b _0801A48A
_0801A1CE:
	add r0, #0x68
	mov r1, #1
	strb r1, [r0]
	add r0, r5, #0
	add r0, #0x44
	ldrb r4, [r0]
	cmp r4, #1
	bne _0801A234
	ldr r0, [r5, #0x40]
	add r0, #0x68
	mov r7, #2
	strb r7, [r0]
	ldr r0, [r5, #0x40]
	ldr r2, [r0, #0x3c]
	ldr r0, _0801A250
	ldr r1, [r0, #4]
	ldr r0, [r0]
	str r0, [r2, #0x38]
	str r1, [r2, #0x3c]
	ldr r3, [r5, #0x40]
	ldr r2, [r3, #0x44]
	lsl r1, r2, #5
	lsr r1, r1, #0x10
	ldr r6, _0801A254
	orr r1, r4
	lsl r1, r1, #0xb
	ldr r4, _0801A258
	add r0, r4, #0
	and r0, r2
	orr r0, r1
	str r0, [r3, #0x44]
	mov r1, r8
	ldrb r0, [r1]
	lsl r0, r0, #0x19
	cmp r0, #0
	bge _0801A234
	ldr r0, [r5, #0x40]
	add r0, #0x68
	mov r1, #3
	strb r1, [r0]
	ldr r3, [r5, #0x40]
	ldr r2, [r3, #0x44]
	lsl r0, r2, #5
	lsr r0, r0, #0x10
	orr r0, r7
	and r0, r6
	lsl r0, r0, #0xb
	add r1, r4, #0
	and r1, r2
	orr r1, r0
	str r1, [r3, #0x44]
_0801A234:
	ldr r0, _0801A25C
	ldr r3, [r0, #0x40]
	ldr r2, [r3, #0x44]
	lsl r1, r2, #5
	lsr r1, r1, #0x10
	mov r0, #8
	b _0801A47E
	.align 2, 0
_0801A244: .4byte off_813D124
_0801A248: .4byte off_813D004
_0801A24C: .4byte dword_813CFBC
_0801A250: .4byte dword_813CFB4
_0801A254: .4byte 0x0000FFFF
_0801A258: .4byte 0xF80007FF
_0801A25C: .4byte 0x03004750
_0801A260:
	add r2, r5, #0
_0801A262:
	add r2, #0x22
	ldrb r1, [r2]
	mov r0, #0x21
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	b _0801A5BE
_0801A270:
	ldr r0, [r5, #0x40]
	cmp r0, #0
	beq _0801A27C
	bl destroyObject
	str r4, [r5, #0x40]
_0801A27C:
	ldr r1, _0801A2F4
	mov r2, #0x44
	add r2, r2, r1
	mov r8, r2
	ldrb r6, [r2]
	mov r4, #0x45
	add r4, r4, r1
	mov sb, r4
	mov r0, #0
	ldrsb r0, [r4, r0]
	add r5, r1, #0
	cmp r0, #0
	ble _0801A298
	b _0801A5BE
_0801A298:
	cmp r6, #0
	bne _0801A300
	add r2, r5, #0
	add r2, #0x4a
	ldrb r1, [r2]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _0801A300
	add r1, r5, #0
	add r1, #0x9c
	ldrh r0, [r5, #0x10]
	lsl r0, r0, #1
	add r0, sp
	add r0, #8
	ldrh r1, [r1]
	ldrh r0, [r0]
	and r0, r1
	add r4, r5, #0
	add r4, #0x23
	cmp r0, #0
	beq _0801A2CC
	ldrb r0, [r4]
	mov r1, #0x40
	orr r0, r1
	strb r0, [r4]
_0801A2CC:
	ldrb r1, [r2]
	mov r0, #3
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	ldr r1, _0801A2F8
	ldrb r0, [r4]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1f
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r1, [r0]
	lsl r0, r7, #1
	add r0, r0, r1
	ldrh r0, [r0]
	bl sub_08019208
	ldr r1, _0801A2FC
	b _0801A518
	.align 2, 0
_0801A2F4: .4byte 0x03004750
_0801A2F8: .4byte off_813D140
_0801A2FC: .4byte dword_813D14C
_0801A300:
	cmp r6, #1
	beq _0801A316
	add r0, r5, #0
	add r0, #0x45
	mov r1, #0
	ldrsb r1, [r0, r1]
	mov r0, #4
	neg r0, r0
	cmp r1, r0
	ble _0801A316
	b _0801A5BE
_0801A316:
	add r0, r5, #0
	add r0, #0x23
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	lsr r4, r0, #0x1f
	cmp r4, #0
	beq _0801A32E
	ldrh r0, [r5, #0x10]
	lsl r0, r0, #1
	add r0, sl
	ldrh r0, [r0]
	strh r0, [r5, #0x10]
_0801A32E:
	bl sub_0801A93C
	ldr r1, _0801A368
	lsl r0, r6, #2
	add r0, r0, r1
	ldr r1, [r0]
	lsl r0, r4, #2
	add r0, r0, r1
	ldr r0, [r0]
	lsl r1, r7, #1
	add r0, r1, r0
	ldrh r0, [r0]
	ldr r2, _0801A36C
	add r1, r1, r2
	ldrh r1, [r1]
	bl sub_80191B0
	add r3, r5, #0
	add r3, #0x46
	mov r0, #0
	strb r0, [r3]
	add r2, r5, #0
	add r2, #0x22
	ldrb r1, [r2]
	sub r0, #0x21
	and r0, r1
	strb r0, [r2]
	ldr r1, _0801A370
	b _0801A5B6
	.align 2, 0
_0801A368: .4byte off_813D188
_0801A36C: .4byte dword_813D190
_0801A370: .4byte unk_813d19e
_0801A374:
	ldr r1, _0801A394
	add r0, r1, #0
	add r0, #0x45
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	add r5, r1, #0
	cmp r0, #0xd
	ble _0801A398
	add r2, r5, #0
	add r2, #0x4a
	ldrb r1, [r2]
	mov r0, #3
	neg r0, r0
	and r0, r1
	b _0801A3B2
	.align 2, 0
_0801A394: .4byte 0x03004750
_0801A398:
	add r0, r5, #0
	add r0, #0x9a
	ldrh r1, [r0]
	mov r0, #0x80
	lsl r0, r0, #1
	and r0, r1
	cmp r0, #0
	beq _0801A3B4
	add r2, r5, #0
	add r2, #0x4a
	ldrb r0, [r2]
	mov r1, #2
	orr r0, r1
_0801A3B2:
	strb r0, [r2]
_0801A3B4:
	add r1, r5, #0
	add r1, #0x45
	ldrb r0, [r1]
	sub r0, #1
	mov r6, #0
	strb r0, [r1]
	lsl r0, r0, #0x18
	asr r4, r0, #0x18
	cmp r4, #0
	beq _0801A498
	cmp r4, #0xe
	bne _0801A4A4
	ldr r0, _0801A458
	mov r4, #0x23
	add r4, r4, r5
	mov r8, r4
	ldrb r2, [r4]
	lsl r2, r2, #0x19
	lsr r2, r2, #0x1f
	lsl r2, r2, #2
	add r0, r2, r0
	ldr r0, [r0]
	lsl r3, r7, #1
	add r0, r3, r0
	ldrh r1, [r0]
	ldr r0, _0801A45C
	add r2, r2, r0
	ldrh r0, [r5, #0x10]
	ldr r2, [r2]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r0, [r0]
	add r0, r3, r0
	mov r4, #0
	ldrsh r2, [r0, r4]
	ldr r0, _0801A460
	add r3, r3, r0
	mov r0, #0
	ldrsh r3, [r3, r0]
	str r6, [sp]
	str r6, [sp, #4]
	add r0, r5, #0
	bl createPlayerAttackObj
	str r0, [r5, #0x40]
	cmp r0, #0
	beq _0801A48A
	add r0, #0x68
	mov r1, #1
	strb r1, [r0]
	add r0, r5, #0
	add r0, #0x44
	ldrb r4, [r0]
	cmp r4, #1
	bne _0801A48A
	ldr r0, [r5, #0x40]
	ldr r2, [r0, #0x3c]
	ldr r0, _0801A464
	ldr r1, [r0, #4]
	ldr r0, [r0]
	str r0, [r2, #0x38]
	str r1, [r2, #0x3c]
	mov r1, r8
	ldrb r0, [r1]
	lsl r0, r0, #0x19
	cmp r0, #0
	blt _0801A46C
	ldr r0, [r5, #0x40]
	add r0, #0x68
	mov r1, #2
	strb r1, [r0]
	ldr r3, [r5, #0x40]
	ldr r2, [r3, #0x44]
	lsl r0, r2, #5
	lsr r0, r0, #0x10
	orr r0, r4
	lsl r0, r0, #0xb
	ldr r1, _0801A468
	and r1, r2
	orr r1, r0
	str r1, [r3, #0x44]
	b _0801A48A
	.align 2, 0
_0801A458: .4byte off_813D124
_0801A45C: .4byte off_813D004
_0801A460: .4byte dword_813CFBC
_0801A464: .4byte dword_813CFAC
_0801A468: .4byte 0xF80007FF
_0801A46C:
	ldr r0, [r5, #0x40]
	add r0, #0x68
	mov r1, #3
	strb r1, [r0]
	ldr r3, [r5, #0x40]
	ldr r2, [r3, #0x44]
	lsl r1, r2, #5
	lsr r1, r1, #0x10
	mov r0, #2
_0801A47E:
	orr r1, r0
	lsl r1, r1, #0xb
	ldr r0, _0801A494
	and r0, r2
	orr r0, r1
	str r0, [r3, #0x44]
_0801A48A:
	mov r0, #0xf2
	bl playSong
	b _0801A5BE
	.align 2, 0
_0801A494: .4byte 0xF80007FF
_0801A498:
	ldr r0, [r5, #0x40]
	cmp r0, #0
	beq _0801A4A4
	bl destroyObject
	str r4, [r5, #0x40]
_0801A4A4:
	ldr r1, _0801A544
	mov r2, #0x44
	add r2, r2, r1
	mov r8, r2
	ldrb r6, [r2]
	mov r4, #0x45
	add r4, r4, r1
	mov sb, r4
	mov r0, #0
	ldrsb r0, [r4, r0]
	add r5, r1, #0
	cmp r0, #0
	ble _0801A4C0
	b _0801A5BE
_0801A4C0:
	cmp r6, #0
	bne _0801A550
	add r2, r5, #0
	add r2, #0x4a
	ldrb r1, [r2]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _0801A550
	add r1, r5, #0
	add r1, #0x9c
	ldrh r0, [r5, #0x10]
	lsl r0, r0, #1
	add r0, sp
	add r0, #8
	ldrh r1, [r1]
	ldrh r0, [r0]
	and r0, r1
	add r4, r5, #0
	add r4, #0x23
	cmp r0, #0
	beq _0801A4F4
	ldrb r0, [r4]
	mov r1, #0x40
	orr r0, r1
	strb r0, [r4]
_0801A4F4:
	ldrb r1, [r2]
	mov r0, #3
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	ldr r1, _0801A548
	ldrb r0, [r4]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1f
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r1, [r0]
	lsl r0, r7, #1
	add r0, r0, r1
	ldrh r0, [r0]
	bl sub_08019208
	ldr r1, _0801A54C
_0801A518:
	ldrb r0, [r4]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1f
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	mov r1, sb
	strb r0, [r1]
	mov r2, r8
	ldrb r0, [r2]
	add r0, #1
	strb r0, [r2]
	add r0, r5, #0
	add r0, #0x48
	strb r6, [r0]
	add r2, r5, #0
	add r2, #0x22
	ldrb r0, [r2]
	mov r1, #0x20
	orr r0, r1
	strb r0, [r2]
	b _0801A5BE
	.align 2, 0
_0801A544: .4byte 0x03004750
_0801A548: .4byte off_813D140
_0801A54C: .4byte dword_813D148
_0801A550:
	cmp r6, #1
	beq _0801A564
	add r0, r5, #0
	add r0, #0x45
	mov r1, #0
	ldrsb r1, [r0, r1]
	mov r0, #4
	neg r0, r0
	cmp r1, r0
	bgt _0801A5BE
_0801A564:
	add r1, r5, #0
	add r0, r1, #0
	add r0, #0x23
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	lsr r4, r0, #0x1f
	cmp r4, #0
	beq _0801A57E
	ldrh r0, [r1, #0x10]
	lsl r0, r0, #1
	add r0, sl
	ldrh r0, [r0]
	strh r0, [r1, #0x10]
_0801A57E:
	bl sub_0801A93C
	ldr r1, _0801A5C4
	lsl r0, r6, #2
	add r0, r0, r1
	ldr r1, [r0]
	lsl r0, r4, #2
	add r0, r0, r1
	ldr r0, [r0]
	lsl r1, r7, #1
	add r0, r1, r0
	ldrh r0, [r0]
	ldr r2, _0801A5C8
	add r1, r1, r2
	ldrh r1, [r1]
	bl sub_80191B0
	add r3, r5, #0
	add r3, #0x46
	mov r0, #0
	strb r0, [r3]
	add r2, r5, #0
	add r2, #0x22
	ldrb r1, [r2]
	sub r0, #0x21
	and r0, r1
	strb r0, [r2]
	ldr r1, _0801A5CC
_0801A5B6:
	lsl r0, r6, #1
	add r0, r0, r1
	ldrh r0, [r0]
	strb r0, [r3]
_0801A5BE:
	mov r0, #1
	b _0801A5D2
	.align 2, 0
_0801A5C4: .4byte off_813D188
_0801A5C8: .4byte dword_813D190
_0801A5CC: .4byte unk_813d19a
_0801A5D0:
	mov r0, #0
_0801A5D2:
	add sp, #0x10
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global handlePowerupShooting
handlePowerupShooting: @ 0x0801A5E4
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0xc
	lsl r0, r0, #0x18
	lsr r7, r0, #0x18
	ldr r1, _0801A62C
	add r4, r1, #0
	add r4, #0x4a
	ldrb r2, [r4]
	mov r0, #8
	and r0, r2
	add r5, r1, #0
	cmp r0, #0
	bne _0801A608
	b _0801A880
_0801A608:
	add r0, r5, #0
	add r0, #0x47
	strb r7, [r0]
	add r0, #0x53
	ldrh r1, [r0]
	mov r3, #2
	add r0, r3, #0
	and r0, r1
	cmp r0, #0
	beq _0801A636
	add r1, r2, #0
	add r0, r3, #0
	and r0, r1
	cmp r0, #0
	bne _0801A630
	mov r0, #2
	orr r0, r1
	b _0801A634
	.align 2, 0
_0801A62C: .4byte 0x03004750
_0801A630:
	mov r0, #4
	orr r0, r2
_0801A634:
	strb r0, [r4]
_0801A636:
	add r1, r5, #0
	add r1, #0x45
	ldrb r0, [r1]
	sub r0, #1
	strb r0, [r1]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	cmp r0, #0
	bne _0801A64A
	b _0801A804
_0801A64A:
	cmp r0, #8
	beq _0801A650
	b _0801A86C
_0801A650:
	cmp r7, #4
	beq _0801A684
	ldrh r0, [r5, #0x10]
	strh r0, [r5, #0x16]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sl, r0
	ldr r4, _0801A67C
	bl getPowerUpLevel
	ldrh r1, [r5, #0x10]
	lsl r1, r1, #1
	lsl r0, r0, #0x18
	lsr r0, r0, #0x16
	add r1, r1, r0
	add r1, r1, r4
	ldrh r6, [r1]
	ldr r1, _0801A680
	lsl r0, r7, #1
	add r0, r0, r1
	ldrh r0, [r0]
	b _0801A6A6
	.align 2, 0
_0801A67C: .4byte dword_813D1B0
_0801A680: .4byte unk_813d1a6
_0801A684:
	ldrh r5, [r5, #0x16]
	mov sl, r5
	bl getPowerUpLevel
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0801A69C
	bl sub_08018CAC
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	b _0801A69E
_0801A69C:
	mov r6, #0
_0801A69E:
	bl sub_08018D18
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
_0801A6A6:
	mov sb, r0
	ldr r4, _0801A6D0
	bl getPowerUpLevel
	lsl r0, r0, #0x18
	lsr r0, r0, #0x17
	add r0, r0, r4
	ldrh r0, [r0]
	mov r8, r0
	str r0, [sp, #8]
	bl getPowerUpLevel
	lsl r0, r0, #0x18
	lsr r5, r0, #0x18
	cmp r5, #1
	beq _0801A720
	cmp r5, #1
	bgt _0801A6D4
	cmp r5, #0
	beq _0801A6DE
	b _0801A7C0
	.align 2, 0
_0801A6D0: .4byte powerupObjTable
_0801A6D4:
	cmp r5, #2
	bne _0801A7C0
	ldr r0, _0801A718
	add r0, #0x44
	strb r5, [r0]
_0801A6DE:
	ldr r4, _0801A71C
	bl getPowerUpLevel
	mov r2, sl
	lsl r1, r2, #2
	lsl r0, r0, #0x18
	lsr r0, r0, #0x15
	add r1, r1, r0
	add r1, r1, r4
	ldr r0, _0801A718
	lsl r2, r6, #0x10
	asr r2, r2, #0x10
	mov r4, sb
	lsl r3, r4, #0x10
	asr r3, r3, #0x10
	ldr r1, [r1]
	str r1, [sp]
	mov r1, #0
	str r1, [sp, #4]
	ldr r1, [sp, #8]
	bl createPlayerAttackObj
	cmp r0, #0
	beq _0801A7C0
	add r1, r0, #0
	add r1, #0x68
	mov r0, #1
	strb r0, [r1]
	b _0801A7C0
	.align 2, 0
_0801A718: .4byte 0x03004750
_0801A71C: .4byte dword_813D1BC
_0801A720:
	ldr r1, _0801A7D8
	add r1, #0x44
	mov r4, #0
	mov r0, #2
	strb r0, [r1]
	bl getPowerUpLevel
	mov r1, sl
	lsl r7, r1, #2
	lsl r0, r0, #0x18
	lsr r0, r0, #0x15
	add r0, r7, r0
	ldr r2, _0801A7DC
	add r0, r0, r2
	lsl r1, r6, #0x10
	asr r1, r1, #0x10
	mov sl, r1
	mov r2, sb
	lsl r1, r2, #0x10
	asr r6, r1, #0x10
	ldr r0, [r0]
	str r0, [sp]
	str r4, [sp, #4]
	ldr r0, _0801A7D8
	mov r1, r8
	mov r2, sl
	add r3, r6, #0
	bl createPlayerAttackObj
	cmp r0, #0
	beq _0801A7C0
	add r0, #0x68
	strb r5, [r0]
	bl getPowerUpLevel
	lsl r0, r0, #0x18
	lsr r0, r0, #0x15
	add r0, r7, r0
	ldr r4, _0801A7DC
	add r0, r0, r4
	add r3, r6, #2
	lsl r3, r3, #0x10
	asr r3, r3, #0x10
	ldr r0, [r0]
	str r0, [sp]
	mov r0, #0xc8
	str r0, [sp, #4]
	ldr r0, _0801A7D8
	mov r1, r8
	mov r2, sl
	bl createPlayerAttackObj
	cmp r0, #0
	beq _0801A790
	add r0, #0x68
	strb r5, [r0]
_0801A790:
	bl getPowerUpLevel
	lsl r0, r0, #0x18
	lsr r0, r0, #0x15
	add r0, r7, r0
	ldr r1, _0801A7DC
	add r0, r0, r1
	sub r3, r6, #2
	lsl r3, r3, #0x10
	asr r3, r3, #0x10
	ldr r0, [r0]
	str r0, [sp]
	mov r0, #0xc8
	neg r0, r0
	str r0, [sp, #4]
	ldr r0, _0801A7D8
	ldr r1, [sp, #8]
	mov r2, sl
	bl createPlayerAttackObj
	cmp r0, #0
	beq _0801A7C0
	add r0, #0x68
	strb r5, [r0]
_0801A7C0:
	bl getPowerUpLevel
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #1
	beq _0801A7EE
	cmp r0, #1
	bgt _0801A7E0
	cmp r0, #0
	beq _0801A7E6
	b _0801A86C
	.align 2, 0
_0801A7D8: .4byte 0x03004750
_0801A7DC: .4byte dword_813D1BC
_0801A7E0:
	cmp r0, #2
	beq _0801A7F8
	b _0801A86C
_0801A7E6:
	mov r0, #0xf0
	bl playSong
	b _0801A86C
_0801A7EE:
	mov r0, #0x8a
	lsl r0, r0, #1
	bl playSong
	b _0801A86C
_0801A7F8:
	ldr r0, _0801A800
	bl playSong
	b _0801A86C
	.align 2, 0
_0801A800: .4byte 0x00000115
_0801A804:
	add r0, r5, #0
	add r0, #0x44
	ldrb r2, [r0]
	cmp r2, #0
	bne _0801A81A
	add r0, #6
	ldrb r1, [r0]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	bne _0801A82C
_0801A81A:
	cmp r2, #1
	bne _0801A83C
	add r0, r5, #0
	add r0, #0x4a
	ldrb r1, [r0]
	mov r0, #4
	and r0, r1
	cmp r0, #0
	beq _0801A83C
_0801A82C:
	add r1, r5, #0
	add r1, #0x45
	mov r0, #9
	strb r0, [r1]
	sub r1, #1
	ldrb r0, [r1]
	add r0, #1
	b _0801A86A
_0801A83C:
	bl sub_0801A93C
	cmp r7, #4
	beq _0801A856
	ldr r0, _0801A870
	lsl r1, r7, #1
	add r0, r1, r0
	ldrh r0, [r0]
	ldr r2, _0801A874
	add r1, r1, r2
	ldrh r1, [r1]
	bl sub_80191B0
_0801A856:
	bl getPowerUpLevel
	ldr r1, _0801A878
	ldr r2, _0801A87C
	lsl r0, r0, #0x18
	lsr r0, r0, #0x17
	add r0, r0, r2
	ldrb r0, [r0]
	add r0, #8
	add r1, #0x46
_0801A86A:
	strb r0, [r1]
_0801A86C:
	mov r0, #1
	b _0801A882
	.align 2, 0
_0801A870: .4byte word_813D1DA
_0801A874: .4byte dword_813D1E4
_0801A878: .4byte 0x03004750
_0801A87C: .4byte unk_813d1ee
_0801A880:
	mov r0, #0
_0801A882:
	add sp, #0xc
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global sub_0801A894
sub_0801A894: @ 0x0801A894
	push {r4, lr}
	lsl r0, r0, #0x18
	lsr r2, r0, #0x18
	add r4, r2, #0
	ldr r1, _0801A8C8
	ldrh r0, [r1, #0x1c]
	cmp r0, #0
	bne _0801A8C2
	add r0, r1, #0
	add r0, #0x46
	ldrb r0, [r0]
	cmp r0, #0
	bne _0801A8CC
	add r0, r2, #0
	bl sub_08019F58
	cmp r0, #0
	bne _0801A8CC
	add r0, r4, #0
	bl handlePowerupShooting
	cmp r0, #0
	bne _0801A8CC
_0801A8C2:
	mov r0, #0
	b _0801A8CE
	.align 2, 0
_0801A8C8: .4byte 0x03004750
_0801A8CC:
	mov r0, #1
_0801A8CE:
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global sub_0801A8D4
sub_0801A8D4: @ 0x0801A8D4
	push {r4, lr}
	ldr r0, _0801A930
	mov ip, r0
	mov r2, ip
	add r2, #0x46
	ldrb r1, [r2]
	cmp r1, #0
	beq _0801A934
	mov r4, ip
	add r4, #0x4a
	ldrb r0, [r4]
	mov r3, #2
	neg r3, r3
	and r3, r0
	strb r3, [r4]
	sub r0, r1, #1
	strb r0, [r2]
	lsl r0, r0, #0x18
	lsr r2, r0, #0x18
	cmp r2, #0
	bne _0801A92C
	mov r1, #1
	add r0, r3, #0
	orr r0, r1
	mov r1, ip
	add r1, #0x44
	strb r2, [r1]
	mov r1, #3
	neg r1, r1
	and r0, r1
	sub r1, #6
	and r0, r1
	sub r1, #8
	and r0, r1
	strb r0, [r4]
	mov r0, ip
	add r0, #0x47
	ldrb r0, [r0]
	cmp r0, #3
	bne _0801A92C
	mov r1, ip
	add r1, #0xb2
	mov r0, #1
	strh r0, [r1]
_0801A92C:
	mov r0, #1
	b _0801A936
	.align 2, 0
_0801A930: .4byte 0x03004750
_0801A934:
	mov r0, #0
_0801A936:
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global sub_0801A93C
sub_0801A93C: @ 0x0801A93C
	push {r4, r5, r6, lr}
	ldr r4, _0801A9A4
	add r2, r4, #0
	add r2, #0x22
	ldrb r1, [r2]
	mov r3, #0x21
	neg r3, r3
	add r0, r3, #0
	and r0, r1
	strb r0, [r2]
	add r6, r4, #0
	add r6, #0x4a
	ldrb r1, [r6]
	mov r0, #1
	orr r1, r0
	add r0, r4, #0
	add r0, #0x46
	mov r5, #0
	strb r5, [r0]
	sub r0, #2
	strb r5, [r0]
	mov r0, #3
	neg r0, r0
	and r1, r0
	sub r0, #2
	and r1, r0
	sub r0, #4
	and r1, r0
	sub r0, #8
	and r1, r0
	mov r0, #0x23
	add r0, r0, r4
	mov ip, r0
	ldrb r2, [r0]
	mov r0, #0x41
	neg r0, r0
	and r0, r2
	mov r2, ip
	strb r0, [r2]
	and r1, r3
	strb r1, [r6]
	ldr r0, [r4, #0x40]
	cmp r0, #0
	beq _0801A99A
	bl destroyObject
	str r5, [r4, #0x40]
_0801A99A:
	bl sub_08019234
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_0801A9A4: .4byte 0x03004750

thumb_func_global activateKillSpecial
activateKillSpecial: @ 0x0801A9A8
	push {r4, r5, r6, lr}
	ldr r5, _0801AA38
	add r6, r5, #0
	add r6, #0x22
	ldrb r0, [r6]
	mov r1, #0x40
	orr r0, r1
	strb r0, [r6]
	mov r0, #0x50
	neg r0, r0
	bl ModifySpecialCharge
	bl sub_0801A93C
	bl sub_08018D68
	bl sub_0801D2DC
	add r0, r5, #0
	add r0, #0xb2
	mov r3, #0
	mov r4, #0
	strh r4, [r0]
	ldrh r1, [r5, #4]
	sub r0, #0x56
	strh r1, [r0]
	ldrh r0, [r5, #6]
	add r1, r5, #0
	add r1, #0x5e
	strh r0, [r1]
	ldrh r1, [r5, #0x10]
	add r0, r5, #0
	add r0, #0x60
	strh r1, [r0]
	ldr r2, [r5]
	ldr r0, [r2, #4]
	ldr r1, [r2, #8]
	str r0, [r5, #0x4c]
	str r1, [r5, #0x50]
	add r0, r5, #0
	add r0, #0x64
	strb r3, [r0]
	add r2, #0x52
	ldrb r0, [r2]
	mov r1, #1
	orr r0, r1
	strb r0, [r2]
	ldr r0, [r5]
	add r0, #0x4e
	mov r1, #1
	strb r1, [r0]
	ldrb r0, [r6]
	mov r1, #0x10
	orr r0, r1
	strb r0, [r6]
	ldr r0, [r5]
	bl clearObjForces
	add r0, r5, #0
	add r0, #0xac
	str r4, [r0]
	mov r0, #0x13
	bl sub_80190F4
	ldr r0, [r5]
	add r0, #0x4d
	ldrb r0, [r0]
	cmp r0, #1
	bne _0801AA3C
	mov r4, #0xbc
	b _0801AA4A
	.align 2, 0
_0801AA38: .4byte 0x03004750
_0801AA3C:
	ldrb r0, [r6]
	lsl r0, r0, #0x1f
	mov r1, #0xba
	cmp r0, #0
	beq _0801AA48
	mov r1, #0xbb
_0801AA48:
	add r4, r1, #0
_0801AA4A:
	add r0, r4, #0
	add r1, r4, #0
	bl sub_80191B0
	ldr r5, _0801AAB4
	add r0, r4, #4
	add r1, r5, #0
	add r1, #0x62
	strh r0, [r1]
	ldr r0, [r5]
	mov r4, #0x80
	lsl r4, r4, #6
	add r1, r4, #0
	bl setObjMaxVelocityX
	ldr r0, [r5]
	add r1, r4, #0
	bl setObjMaxVelocityY
	ldrh r0, [r5, #0x1a]
	cmp r0, #0
	beq _0801AA98
	ldr r0, [r5]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	add r2, r5, #0
	add r2, #0x22
	ldrb r1, [r2]
	mov r0, #9
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	mov r0, #0
	strh r0, [r5, #0x1a]
_0801AA98:
	mov r0, #1
	bl setPlayerPaletteMaybe
	bl sub_08013888
	bl sub_0801BAB0
	ldr r0, _0801AAB8
	bl playSong
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_0801AAB4: .4byte 0x03004750
_0801AAB8: .4byte 0x00000101

thumb_func_global sub_0801AABC
sub_0801AABC: @ 0x0801AABC
	push {r4, lr}
	sub sp, #8
	ldr r4, _0801AB04
	mov r0, #0
	str r0, [sp]
	str r0, [sp, #4]
	add r0, r4, #0
	mov r1, #0x57
	mov r2, #0
	mov r3, #0
	bl createPlayerAttackObj
	str r0, [r4, #0x40]
	cmp r0, #0
	beq _0801AAE2
	add r1, r0, #0
	add r1, #0x68
	mov r0, #3
	strb r0, [r1]
_0801AAE2:
	mov r0, #0x14
	bl sub_80190F4
	mov r0, #0xbd
	bl setPlayerSprite
	bl sub_0801AB08
	bl sub_0801AD5C
	bl sub_0801ADA8
	add sp, #8
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801AB04: .4byte 0x03004750

thumb_func_global sub_0801AB08
sub_0801AB08: @ 0x0801AB08
	push {r4, r5, lr}
	sub sp, #0x10
	ldr r4, _0801AB48
	add r0, r4, #0
	add r0, #0x67
	mov r5, #0
	strb r5, [r0]
	add r1, r4, #0
	add r1, #0x68
	mov r0, #0xff
	strb r0, [r1]
	ldr r0, [r4]
	add r0, #0x14
	bl sub_0801AE00
	add r1, r0, #0
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	mov r0, sp
	bl sub_0801B0E0
	ldr r0, [sp]
	ldr r1, [sp, #4]
	str r0, [sp, #8]
	str r1, [sp, #0xc]
	ldr r0, [r4]
	ldr r1, [r0, #4]
	ldr r0, [sp, #8]
	cmp r1, r0
	ble _0801AB4C
	strh r5, [r4, #0x10]
	b _0801AB62
	.align 2, 0
_0801AB48: .4byte 0x03004750
_0801AB4C:
	cmp r1, r0
	bge _0801AB56
	mov r0, #1
	strh r0, [r4, #0x10]
	b _0801AB62
_0801AB56:
	mov r1, #0
	ldrh r0, [r4, #0x10]
	cmp r0, #0
	bne _0801AB60
	mov r1, #1
_0801AB60:
	strh r1, [r4, #0x10]
_0801AB62:
	add r0, sp, #8
	bl sub_0801AC94
	add sp, #0x10
	pop {r4, r5}
	pop {r0}
	bx r0

thumb_func_global sub_0801AB70
sub_0801AB70: @ 0x0801AB70
	push {r4, r5, r6, lr}
	ldr r4, _0801ABCC
	ldr r0, [r4]
	bl clearObjForces
	mov r0, #0x15
	bl sub_80190F4
	add r0, r4, #0
	add r0, #0x62
	ldrh r1, [r0]
	add r0, r1, #0
	bl sub_80191B0
	add r0, r4, #0
	add r0, #0x60
	ldrh r0, [r0]
	strh r0, [r4, #0x10]
	ldr r2, [r4]
	ldr r0, [r4, #0x4c]
	ldr r1, [r4, #0x50]
	str r0, [r2, #4]
	str r1, [r2, #8]
	mov r6, #0
	mov r5, #0x10
	add r3, r4, #0
	add r3, #0x6c
	mov r2, #7
_0801ABA8:
	ldm r3!, {r0}
	ldr r1, [r0]
	str r6, [r1, #8]
	add r1, #0x23
	ldrb r0, [r1]
	orr r0, r5
	strb r0, [r1]
	sub r2, #1
	cmp r2, #0
	bge _0801ABA8
	add r1, r4, #0
	add r1, #0x99
	mov r0, #9
	strb r0, [r1]
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_0801ABCC: .4byte 0x03004750

thumb_func_global sub_0801ABD0
sub_0801ABD0: @ 0x0801ABD0
	push {r4, r5, lr}
	ldr r4, _0801AC7C
	ldr r0, [r4]
	bl clearObjForces
	mov r0, #0
	bl set_player_18
	add r0, r4, #0
	add r0, #0x5c
	ldrh r0, [r0]
	bl sub_80190F4
	add r0, r4, #0
	add r0, #0x5e
	ldrh r0, [r0]
	bl setPlayerSprite
	add r0, r4, #0
	add r0, #0x60
	ldrh r0, [r0]
	mov r3, #0
	strh r0, [r4, #0x10]
	add r5, r4, #0
	add r5, #0x22
	ldrb r1, [r5]
	mov r0, #0x11
	neg r0, r0
	and r0, r1
	strb r0, [r5]
	ldr r1, [r4]
	add r1, #0x52
	ldrb r2, [r1]
	mov r0, #2
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	ldr r2, [r4]
	ldr r0, [r4, #0x4c]
	ldr r1, [r4, #0x50]
	str r0, [r2, #4]
	str r1, [r2, #8]
	ldr r0, [r4]
	add r0, #0x4e
	strb r3, [r0]
	bl sub_08013898
	ldr r0, [r4, #0x40]
	cmp r0, #0
	beq _0801AC3C
	bl destroyObject
	mov r0, #0
	str r0, [r4, #0x40]
_0801AC3C:
	ldr r0, [r4]
	mov r1, #0xe0
	lsl r1, r1, #3
	bl setObjMaxVelocityX
	ldr r0, [r4]
	mov r1, #0xc0
	lsl r1, r1, #3
	bl setObjMaxVelocityY
	mov r0, #0
	bl setPlayerPaletteMaybe
	bl sub_0801BB84
	ldrb r1, [r5]
	mov r0, #0x41
	neg r0, r0
	and r0, r1
	strb r0, [r5]
	ldr r0, [r4]
	add r0, #0x4d
	ldrb r3, [r0]
	cmp r3, #1
	bne _0801AC74
	add r0, r4, #0
	add r0, #0xb2
	strh r3, [r0]
_0801AC74:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_0801AC7C: .4byte 0x03004750

thumb_func_global sub_0801AC80
sub_0801AC80: @ 0x0801AC80
	push {lr}
	bl sub_801D964
	asr r0, r0, #8
	add r0, #0xc0
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global sub_0801AC94
sub_0801AC94: @ 0x0801AC94
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	add r5, r0, #0
	ldr r7, _0801AD54
	ldr r0, [r7]
	add r0, #4
	add r1, r5, #0
	bl sub_0801AC80
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	mov sb, r0
	ldr r2, [r7]
	ldr r1, [r2, #4]
	ldr r0, [r5]
	sub r0, r1, r0
	cmp r0, #0
	bge _0801ACBE
	neg r0, r0
_0801ACBE:
	lsl r0, r0, #8
	lsr r3, r0, #0x10
	ldr r1, [r2, #8]
	ldr r0, [r5, #4]
	sub r1, r1, r0
	cmp r1, #0
	bge _0801ACCE
	neg r1, r1
_0801ACCE:
	lsl r1, r1, #8
	lsl r0, r3, #0x10
	asr r0, r0, #0x10
	add r2, r0, #0
	mul r2, r0, r2
	add r0, r2, #0
	asr r1, r1, #0x10
	add r2, r1, #0
	mul r2, r1, r2
	add r1, r2, #0
	add r0, r0, r1
	bl Bios_Sqrt
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	ldr r0, [r5]
	ldr r1, [r5, #4]
	str r0, [r7, #0x54]
	str r1, [r7, #0x58]
	ldr r5, [r7]
	ldr r0, _0801AD58
	mov r8, r0
	mov r0, sb
	add r0, #0x40
	lsl r0, r0, #1
	add r0, r8
	mov r1, #0
	ldrsh r0, [r0, r1]
	mov r6, #0x80
	lsl r6, r6, #6
	add r1, r6, #0
	bl multiply_fixed8
	add r1, r0, #0
	add r0, r5, #0
	bl setObjVelocity_X
	ldr r5, [r7]
	mov r2, sb
	lsl r0, r2, #1
	add r0, r8
	mov r1, #0
	ldrsh r0, [r0, r1]
	add r1, r6, #0
	bl multiply_fixed8
	add r1, r0, #0
	add r0, r5, #0
	bl setObjVelocity_Y
	lsl r4, r4, #0x10
	asr r0, r4, #0x10
	cmp r0, #0
	bge _0801AD3E
	add r0, #0x1f
_0801AD3E:
	lsl r0, r0, #0xb
	lsr r0, r0, #0x10
	bl set_player_18
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_0801AD54: .4byte 0x03004750
_0801AD58: .4byte dword_80382C8

thumb_func_global sub_0801AD5C
sub_0801AD5C: @ 0x0801AD5C
	push {r4, r5, lr}
	sub sp, #8
	ldr r4, _0801ADA4
	ldr r0, [r4]
	ldr r1, [r0, #8]
	ldr r0, [r0, #4]
	str r0, [sp]
	str r1, [sp, #4]
	mov r0, sp
	bl sub_0801B26C
	ldr r0, [r4]
	ldr r0, [r0, #0x18]
	mov r5, #0x80
	lsl r5, r5, #2
	add r1, r5, #0
	bl sub_800F2A0
	ldr r1, [sp]
	add r1, r1, r0
	str r1, [sp]
	ldr r0, [r4]
	ldr r0, [r0, #0x1c]
	add r1, r5, #0
	bl sub_800F2A0
	ldr r1, [sp, #4]
	add r1, r1, r0
	str r1, [sp, #4]
	mov r0, sp
	bl sub_0801B26C
	add sp, #8
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_0801ADA4: .4byte 0x03004750

thumb_func_global sub_0801ADA8
sub_0801ADA8: @ 0x0801ADA8
	push {r4, r5, r6, lr}
	ldr r2, _0801ADFC
	add r5, r2, #0
	add r5, #0x68
	mov r1, #0
	ldrsb r1, [r5, r1]
	mov r0, #1
	neg r0, r0
	cmp r1, r0
	beq _0801ADF6
	mov r3, #0
	mov ip, r2
	ldr r0, [r2]
	ldr r4, [r0]
	add r6, r5, #0
	mov r5, ip
	add r5, #0x67
_0801ADCA:
	mov r0, #0
	ldrsb r0, [r6, r0]
	ldrb r1, [r5]
	add r0, r0, r1
	sub r1, r3, #1
	add r0, r0, r1
	mov r1, #7
	and r0, r1
	lsl r0, r0, #2
	mov r1, ip
	add r1, #0x6c
	add r0, r0, r1
	ldr r0, [r0]
	ldr r2, [r0]
	lsl r1, r3, #4
	ldr r0, [r4]
	add r0, r0, r1
	add r0, #0x10
	str r0, [r2]
	add r3, #1
	cmp r3, #7
	ble _0801ADCA
_0801ADF6:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_0801ADFC: .4byte 0x03004750

thumb_func_global sub_0801AE00
sub_0801AE00: @ 0x0801AE00
	add r1, r0, #0
	mov r2, #0
	mov r3, #0
	ldrsh r0, [r1, r3]
	cmp r0, #0x78
	ble _0801AE0E
	mov r2, #1
_0801AE0E:
	mov r3, #2
	ldrsh r0, [r1, r3]
	cmp r0, #0x50
	ble _0801AE1A
	mov r0, #2
	orr r2, r0
_0801AE1A:
	add r0, r2, #0
	bx lr
	.align 2, 0

thumb_func_global sub_0801AE20
sub_0801AE20: @ 0x0801AE20
	push {r4, lr}
	lsl r0, r0, #0x18
	lsr r4, r0, #0x18
	ldr r0, _0801AE44
	add r0, #0x65
	mov r1, #0
	ldrsb r1, [r0, r1]
	mov r0, #1
	neg r0, r0
	cmp r1, r0
	bne _0801AE48
	mov r0, #3
	bl random
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	b _0801AE60
	.align 2, 0
_0801AE44: .4byte 0x03004750
_0801AE48:
	mov r0, #2
	bl random
	ldr r2, _0801AE68
	lsl r1, r4, #0x18
	asr r1, r1, #0x16
	add r1, r1, r2
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r1, [r1]
	add r1, r1, r0
	ldrb r0, [r1]
_0801AE60:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
_0801AE68: .4byte dword_813D26C

thumb_func_global sub_0801AE6C
sub_0801AE6C: @ 0x0801AE6C
	push {lr}
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #0xa0
	ble _0801AE7A
	mov r1, #0xa0
_0801AE7A:
	lsl r0, r1, #0x10
	asr r0, r0, #0x10
	mov r1, #0x35
	bl Bios_Div
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	pop {r1}
	bx r1

thumb_func_global sub_0801AE8C
sub_0801AE8C: @ 0x0801AE8C
	ldr r1, _0801AE9C
	mov r0, #0
	strb r0, [r1]
	ldr r1, _0801AEA0
	ldr r0, _0801AEA4
	ldr r0, [r0, #4]
	str r0, [r1]
	bx lr
	.align 2, 0
_0801AE9C: .4byte 0x03001D71
_0801AEA0: .4byte 0x03001D74
_0801AEA4: .4byte 0x03005A40

thumb_func_global sub_0801AEA8
sub_0801AEA8: @ 0x0801AEA8
	push {r4, lr}
_0801AEAA:
	mov r1, #0
	ldr r0, _0801AEE0
	ldrb r0, [r0]
	cmp r0, #1
	beq _0801AF10
	cmp r0, #1
	bgt _0801AF2C
	cmp r0, #0
	bne _0801AF2C
	ldr r2, _0801AEE4
	ldr r3, _0801AEE8
_0801AEC0:
	ldr r0, [r2]
	cmp r0, r3
	beq _0801AEF8
	add r1, r0, #0
	ldr r0, [r1, #4]
	str r0, [r2]
	add r0, r1, #0
	add r0, #0x5a
	ldrh r0, [r0]
	sub r0, #0x8b
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #1
	bls _0801AEC0
	b _0801AF2C
	.align 2, 0
_0801AEE0: .4byte 0x03001D71
_0801AEE4: .4byte 0x03001D74
_0801AEE8: .4byte 0x03005AB4
_0801AEEC:
	ldr r1, _0801AEF4
	mov r0, #2
	strb r0, [r1]
	b _0801AEAA
	.align 2, 0
_0801AEF4: .4byte 0x03001D71
_0801AEF8:
	ldr r1, _0801AF08
	mov r0, #1
	strb r0, [r1]
	ldr r0, _0801AF0C
	ldr r0, [r0, #4]
	str r0, [r2]
	b _0801AEAA
	.align 2, 0
_0801AF08: .4byte 0x03001D71
_0801AF0C: .4byte 0x03004EE0
_0801AF10:
	ldr r2, _0801AF34
	add r4, r2, #0
	ldr r3, _0801AF38
_0801AF16:
	ldr r0, [r4]
	cmp r0, r3
	beq _0801AEEC
	add r1, r0, #0
	ldr r0, [r1, #4]
	str r0, [r2]
	add r0, r1, #0
	add r0, #0x5a
	ldrh r0, [r0]
	cmp r0, #0x3d
	beq _0801AF16
_0801AF2C:
	add r0, r1, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
_0801AF34: .4byte 0x03001D74
_0801AF38: .4byte 0x03004F54

thumb_func_global sub_0801AF3C
sub_0801AF3C: @ 0x0801AF3C
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	mov sb, r0
	mov r7, #0
	mov r0, #0
	mov r8, r0
	bl sub_0801AE8C
	b _0801AFD6
_0801AF56:
	ldr r2, [r4, #0x3c]
	ldrh r0, [r2, #0x14]
	cmp r0, #0xf9
	bhi _0801AFD6
	ldrh r0, [r2, #0x16]
	cmp r0, #0xc2
	bhi _0801AFD6
	add r0, r4, #0
	add r0, #0x44
	ldrb r0, [r0]
	lsl r0, r0, #0x1a
	cmp r0, #0
	blt _0801AFD6
	ldr r0, [r4, #0x48]
	add r0, #0x5a
	ldrh r0, [r0]
	cmp r0, #0x57
	beq _0801AFD6
	cmp r7, #0
	bne _0801AFA0
	add r7, r4, #0
	ldr r1, _0801AF9C
	add r0, r7, #0
	add r0, #0x5c
	ldrh r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r8, r0
	add r0, r2, #0
	add r0, #0x14
	bl sub_0801AE00
	b _0801AFD6
	.align 2, 0
_0801AF9C: .4byte dword_813D204
_0801AFA0:
	ldr r6, _0801AFF0
	add r5, r4, #0
	add r5, #0x5c
	ldrh r0, [r5]
	lsl r0, r0, #2
	add r0, r0, r6
	ldr r0, [r0]
	cmp r8, r0
	bgt _0801AFD6
	add r0, r2, #0
	add r0, #0x14
	bl sub_0801AE00
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp sb, r0
	beq _0801AFD6
	add r7, r4, #0
	ldrh r0, [r5]
	lsl r0, r0, #2
	add r0, r0, r6
	ldr r0, [r0]
	mov r8, r0
	ldr r0, [r7, #0x3c]
	add r0, #0x14
	bl sub_0801AE00
_0801AFD6:
	bl sub_0801AEA8
	add r4, r0, #0
	cmp r4, #0
	bne _0801AF56
	add r0, r7, #0
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
_0801AFF0: .4byte dword_813D204

thumb_func_global sub_0801AFF4
sub_0801AFF4: @ 0x0801AFF4
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	mov sb, r0
	mov r6, #0
	mov r8, r6
	bl sub_0801AE8C
	b _0801B0C2
_0801B00C:
	ldr r1, [r4, #0x3c]
	ldrh r0, [r1, #0x14]
	add r3, r1, #0
	cmp r0, #0xf9
	bhi _0801B0C2
	ldrh r0, [r1, #0x16]
	cmp r0, #0xc2
	bhi _0801B0C2
	add r0, r4, #0
	add r0, #0x44
	ldrb r0, [r0]
	lsl r0, r0, #0x1a
	cmp r0, #0
	blt _0801B0C2
	add r0, r4, #0
	add r0, #0x5a
	ldrh r1, [r0]
	add r2, r0, #0
	cmp r1, #0x8d
	beq _0801B0C2
	cmp r1, #0x3d
	beq _0801B0C2
	ldr r0, [r4, #0x48]
	add r0, #0x5a
	ldrh r0, [r0]
	cmp r0, #0x57
	bne _0801B058
	cmp r1, #0x38
	bls _0801B0C2
	ldr r0, _0801B078
	ldr r0, [r0, #0x40]
	ldr r0, [r0, #0x48]
	cmp r0, #0
	beq _0801B058
	add r0, #0x5a
	ldrh r0, [r0]
	cmp r0, #0x38
	bhi _0801B0C2
_0801B058:
	cmp r6, #0
	bne _0801B080
	add r6, r4, #0
	ldr r1, _0801B07C
	add r0, r6, #0
	add r0, #0x5c
	ldrh r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r8, r0
	ldr r0, [r6, #0x3c]
	add r0, #0x14
	bl sub_0801AE00
	b _0801B0C2
	.align 2, 0
_0801B078: .4byte 0x03004750
_0801B07C: .4byte dword_813D204
_0801B080:
	ldr r7, _0801B0DC
	add r5, r4, #0
	add r5, #0x5c
	ldrh r0, [r5]
	lsl r0, r0, #2
	add r0, r0, r7
	ldr r0, [r0]
	cmp r8, r0
	bgt _0801B0C2
	add r1, r6, #0
	add r1, #0x5a
	ldrh r0, [r2]
	ldrh r1, [r1]
	cmp r0, r1
	bhs _0801B0C2
	add r0, r3, #0
	add r0, #0x14
	bl sub_0801AE00
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp sb, r0
	beq _0801B0C2
	add r6, r4, #0
	ldrh r0, [r5]
	lsl r0, r0, #2
	add r0, r0, r7
	ldr r0, [r0]
	mov r8, r0
	ldr r0, [r6, #0x3c]
	add r0, #0x14
	bl sub_0801AE00
_0801B0C2:
	bl sub_0801AEA8
	add r4, r0, #0
	cmp r4, #0
	bne _0801B00C
	add r0, r6, #0
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
_0801B0DC: .4byte dword_813D204

thumb_func_global sub_0801B0E0
sub_0801B0E0: @ 0x0801B0E0
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #8
	mov sl, r0
	lsl r1, r1, #0x18
	lsr r7, r1, #0x18
	ldr r1, _0801B104
	add r0, r1, #0
	add r0, #0x64
	ldrb r0, [r0]
	cmp r0, #6
	bne _0801B108
	ldr r5, [r1, #0x4c]
	ldr r6, [r1, #0x50]
	b _0801B1A2
	.align 2, 0
_0801B104: .4byte 0x03004750
_0801B108:
	ldr r0, _0801B124
	ldrb r2, [r0, #2]
	ldr r1, _0801B128
	ldrb r0, [r0, #1]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	sub r0, #1
	cmp r2, r0
	bne _0801B12C
	add r0, r7, #0
	bl sub_0801AFF4
	b _0801B132
	.align 2, 0
_0801B124: .4byte 0x03003D40
_0801B128: .4byte stageCount
_0801B12C:
	add r0, r7, #0
	bl sub_0801AF3C
_0801B132:
	cmp r0, #0
	beq _0801B13E
	ldr r0, [r0, #0x3c]
	ldr r5, [r0, #4]
	ldr r6, [r0, #8]
	b _0801B1A2
_0801B13E:
	ldr r4, _0801B174
	mov r0, #0x65
	add r0, r0, r4
	mov r8, r0
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	bl sub_0801AE20
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	mov sb, r0
	ldr r0, [r4]
	mov r1, #0x16
	ldrsh r0, [r0, r1]
	bl sub_0801AE6C
	mov r1, r8
	strb r0, [r1]
	mov r0, #1
	and r0, r7
	cmp r0, #0
	beq _0801B17C
	ldr r0, _0801B178
	ldrh r0, [r0]
	add r0, #0x10
	b _0801B182
	.align 2, 0
_0801B174: .4byte 0x03004750
_0801B178: .4byte 0x03004720
_0801B17C:
	ldr r0, _0801B1E4
	ldrh r0, [r0]
	add r0, #0xe0
_0801B182:
	lsl r0, r0, #8
	add r5, r0, #0
	mov r0, #0x14
	bl random
	ldr r1, _0801B1E4
	ldrh r2, [r1, #2]
	ldr r1, _0801B1E8
	add r1, sb
	ldrb r1, [r1]
	sub r2, r2, r1
	sub r2, #0xa
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	add r2, r2, r0
	lsl r6, r2, #8
_0801B1A2:
	ldr r4, _0801B1EC
	ldr r0, [r4, #0x40]
	cmp r0, #0
	beq _0801B1AE
	bl destroyObject
_0801B1AE:
	mov r0, #0
	str r0, [sp]
	str r0, [sp, #4]
	add r0, r4, #0
	mov r1, #0x57
	mov r2, #0
	mov r3, #0
	bl createPlayerAttackObj
	str r0, [r4, #0x40]
	cmp r0, #0
	beq _0801B1CE
	add r1, r0, #0
	add r1, #0x68
	mov r0, #3
	strb r0, [r1]
_0801B1CE:
	mov r0, sl
	str r5, [r0]
	str r6, [r0, #4]
	add sp, #8
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r2}
	bx r2
	.align 2, 0
_0801B1E4: .4byte 0x03004720
_0801B1E8: .4byte dword_813D260
_0801B1EC: .4byte 0x03004750

thumb_func_global createPlayerAttackObjs
createPlayerAttackObjs: @ 0x0801B1F0
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #4
	ldr r6, _0801B268
	mov r5, #1
	mov r0, #0x6c
	add r0, r0, r6
	mov r8, r0
	mov r4, #7
_0801B204:
	ldr r0, [r6]
	ldr r0, [r0]
	ldrh r2, [r0, #0xc]
	mov r0, #0x80
	str r0, [sp]
	mov r0, #0
	mov r1, #0
	mov r3, #1
	bl allocSprite
	bl initPlayerInit
	add r2, r0, #0
	add r2, #0x4e
	mov r1, #1
	strb r1, [r2]
	add r2, #4
	ldrb r1, [r2]
	orr r1, r5
	strb r1, [r2]
	ldr r3, [r0]
	add r3, #0x23
	ldrb r1, [r3]
	mov r2, #0x10
	orr r1, r2
	strb r1, [r3]
	ldr r3, [r0]
	add r3, #0x25
	ldrb r1, [r3]
	mov r7, #4
	neg r7, r7
	add r2, r7, #0
	and r1, r2
	orr r1, r5
	strb r1, [r3]
	mov r1, r8
	add r1, #4
	mov r8, r1
	sub r1, #4
	stm r1!, {r0}
	sub r4, #1
	cmp r4, #0
	bge _0801B204
	add sp, #4
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_0801B268: .4byte 0x03004750

thumb_func_global sub_0801B26C
sub_0801B26C: @ 0x0801B26C
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	add r4, r0, #0
	ldr r5, _0801B2F8
	add r7, r5, #0
	add r7, #0x67
	ldrb r0, [r7]
	cmp r0, #8
	bne _0801B286
	mov r0, #1
	bl sub_0801B300
_0801B286:
	mov r0, #0x68
	add r0, r0, r5
	mov r8, r0
	mov r1, #0
	ldrsb r1, [r0, r1]
	mov r0, #1
	neg r0, r0
	mov r6, #0
	cmp r1, r0
	beq _0801B29E
	mov r1, r8
	ldrb r6, [r1]
_0801B29E:
	lsl r0, r6, #0x18
	asr r0, r0, #0x18
	ldrb r1, [r7]
	add r0, r0, r1
	mov r1, #7
	and r0, r1
	lsl r0, r0, #2
	add r1, r5, #0
	add r1, #0x6c
	add r0, r0, r1
	ldr r3, [r0]
	ldr r0, [r4]
	ldr r1, [r4, #4]
	str r0, [r3, #4]
	str r1, [r3, #8]
	ldr r1, [r3]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	ldr r0, [r3]
	ldr r4, _0801B2FC
	ldrh r3, [r5, #6]
	lsl r3, r3, #3
	add r1, r3, r4
	ldr r1, [r1]
	ldrh r2, [r0, #0xc]
	add r4, #4
	add r3, r3, r4
	ldr r3, [r3]
	bl setSpriteAnimation
	ldrb r0, [r7]
	add r0, #1
	strb r0, [r7]
	mov r0, r8
	strb r6, [r0]
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_0801B2F8: .4byte 0x03004750
_0801B2FC: .4byte playerAnimations

thumb_func_global sub_0801B300
sub_0801B300: @ 0x0801B300
	push {r4, r5, r6, r7, lr}
	lsl r0, r0, #0x18
	lsr r5, r0, #0x18
	ldr r1, _0801B31C
	add r2, r1, #0
	add r2, #0x68
	mov r0, #0
	ldrsb r0, [r2, r0]
	mov ip, r1
	cmp r0, #0
	bge _0801B32C
	mov r0, #1
	b _0801B372
	.align 2, 0
_0801B31C: .4byte 0x03004750
_0801B320:
	mov r1, ip
	add r1, #0x68
	ldrb r0, [r1]
	orr r0, r7
	strb r0, [r1]
	b _0801B370
_0801B32C:
	mov r3, #0
	cmp r3, r5
	bge _0801B370
	mov r6, ip
	add r6, #0x67
	mov r7, #0xff
	add r4, r2, #0
_0801B33A:
	ldrb r0, [r6]
	sub r0, #1
	strb r0, [r6]
	and r0, r7
	cmp r0, #0xff
	beq _0801B320
	mov r0, #0
	ldrsb r0, [r4, r0]
	lsl r0, r0, #2
	mov r1, ip
	add r1, #0x6c
	add r0, r0, r1
	ldr r0, [r0]
	ldr r2, [r0]
	add r2, #0x23
	ldrb r0, [r2]
	mov r1, #0x10
	orr r0, r1
	strb r0, [r2]
	ldrb r0, [r4]
	add r0, #1
	mov r1, #7
	and r0, r1
	strb r0, [r4]
	add r3, #1
	cmp r3, r5
	blt _0801B33A
_0801B370:
	mov r0, #0
_0801B372:
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1

thumb_func_global sub_0801B378
sub_0801B378: @ 0x0801B378
	push {r4, r5, lr}
	mov r4, #0
	ldr r0, _0801B3D0
	mov ip, r0
	mov r5, #3
	neg r5, r5
_0801B384:
	lsl r1, r4, #2
	mov r0, ip
	add r0, #0x6c
	add r1, r1, r0
	ldr r0, [r1]
	ldr r2, [r0]
	mov r3, ip
	ldr r0, [r3]
	ldr r0, [r0]
	ldrh r0, [r0, #0x20]
	strh r0, [r2, #0x20]
	ldr r0, [r1]
	ldr r2, [r0]
	ldr r0, [r3]
	ldr r0, [r0]
	ldrh r0, [r0, #0xe]
	strh r0, [r2, #0xe]
	ldr r0, [r1]
	ldr r3, [r0]
	mov r1, ip
	ldr r0, [r1]
	ldr r0, [r0]
	add r0, #0x24
	ldrb r0, [r0]
	add r3, #0x24
	mov r1, #2
	and r1, r0
	ldrb r2, [r3]
	add r0, r5, #0
	and r0, r2
	orr r0, r1
	strb r0, [r3]
	add r4, #1
	cmp r4, #7
	ble _0801B384
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_0801B3D0: .4byte 0x03004750

thumb_func_global sub_0801B3D4
sub_0801B3D4: @ 0x0801B3D4
	push {r4, r5, lr}
	ldr r4, _0801B404
	add r0, r4, #0
	add r0, #0x23
	ldrb r0, [r0]
	lsl r0, r0, #0x1e
	lsr r5, r0, #0x1f
	cmp r5, #0
	bne _0801B3F0
	bl getPlayerHealth
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _0801B408
_0801B3F0:
	add r1, r4, #0
	add r1, #0x9a
	mov r0, #0
	strh r0, [r1]
	add r1, #2
	strh r0, [r1]
	add r1, #2
	strh r0, [r1]
	b _0801B45E
	.align 2, 0
_0801B404: .4byte 0x03004750
_0801B408:
	ldrh r1, [r4, #0x1c]
	cmp r1, #0
	beq _0801B446
	add r0, r4, #0
	add r0, #0x9a
	strh r5, [r0]
	add r0, #2
	strh r5, [r0]
	add r0, #2
	strh r5, [r0]
	sub r0, r1, #1
	strh r0, [r4, #0x1c]
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _0801B45E
	bl _3005773_clearBits3_5
	ldr r0, [r4]
	add r0, #0x4d
	ldrb r0, [r0]
	cmp r0, #1
	bne _0801B45E
	bl getPlayerHealth
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0801B45E
	mov r0, #0x10
	bl setPlayerSprite
	b _0801B45E
_0801B446:
	ldr r1, _0801B464
	ldrh r0, [r1, #0xc]
	add r2, r4, #0
	add r2, #0x9a
	strh r0, [r2]
	ldrh r2, [r1, #0xe]
	add r0, r4, #0
	add r0, #0x9c
	strh r2, [r0]
	ldrh r1, [r1, #0x14]
	add r0, #2
	strh r1, [r0]
_0801B45E:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_0801B464: .4byte 0x03002080

thumb_func_global sub_0801B468
sub_0801B468: @ 0x0801B468
	push {r4, r5, r6, lr}
	ldr r0, _0801B494
	mov r6, #4
	neg r6, r6
	add r5, r0, #0
	add r5, #0x6c
	mov r4, #1
	mov r3, #7
_0801B478:
	ldm r5!, {r0}
	ldr r1, [r0]
	add r1, #0x25
	ldrb r2, [r1]
	add r0, r6, #0
	and r0, r2
	orr r0, r4
	strb r0, [r1]
	sub r3, #1
	cmp r3, #0
	bge _0801B478
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_0801B494: .4byte 0x03004750

thumb_func_global sub_0801B498
sub_0801B498: @ 0x0801B498
	push {r4, r5, lr}
	ldr r0, _0801B4C0
	mov r5, #4
	neg r5, r5
	add r4, r0, #0
	add r4, #0x6c
	mov r3, #7
_0801B4A6:
	ldm r4!, {r0}
	ldr r1, [r0]
	add r1, #0x25
	ldrb r2, [r1]
	add r0, r5, #0
	and r0, r2
	strb r0, [r1]
	sub r3, #1
	cmp r3, #0
	bge _0801B4A6
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_0801B4C0: .4byte 0x03004750

thumb_func_global sub_0801B4C4
sub_0801B4C4: @ 0x0801B4C4
	push {r4, r5, r6, lr}
	ldr r4, _0801B560
	add r5, r4, #0
	add r5, #0x22
	ldrb r0, [r5]
	mov r6, #0x46
	and r6, r0
	cmp r6, #0
	bne _0801B558
	bl getPlayerHealth
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0801B558
	ldr r0, _0801B564
	ldrb r0, [r0, #0x10]
	cmp r0, #0
	beq _0801B558
	bl sub_08018D68
	bl sub_0801D2DC
	add r0, r4, #0
	add r0, #0xb2
	strh r6, [r0]
	bl sub_0801A93C
	mov r0, #0
	bl setPlayerPaletteMaybe
	ldr r0, [r4]
	add r0, #0x4e
	mov r1, #1
	strb r1, [r0]
	ldr r1, [r4]
	add r1, #0x52
	ldrb r2, [r1]
	mov r0, #2
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	ldrb r0, [r5]
	mov r1, #0x10
	orr r0, r1
	strb r0, [r5]
	mov r0, #0x16
	bl sub_80190F4
	mov r0, #0xc1
	mov r1, #0xc1
	bl sub_80191B0
	bl removeCameraPlayerControl
	ldr r0, _0801B568
	strh r6, [r0, #8]
	ldrb r0, [r5]
	mov r1, #4
	orr r0, r1
	strb r0, [r5]
	strh r6, [r4, #0x1a]
	ldr r0, [r4]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	ldrb r1, [r5]
	mov r0, #9
	neg r0, r0
	and r0, r1
	strb r0, [r5]
_0801B558:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_0801B560: .4byte 0x03004750
_0801B564: .4byte 0x03003D40
_0801B568: .4byte 0x03004720

thumb_func_global nullsub_6
nullsub_6: @ 0x0801B56C
	bx lr
	.align 2, 0
	
	
	
thumb_func_global sub_801B570
sub_801B570: @ 0x0801B570
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x28
	mov sb, r0
	add r7, r1, #0
	mov r0, #1
	str r0, [sp, #0x24]
	add r4, sp, #0x10
	mov r2, sb
	mov r3, #0x38
	ldrsh r1, [r2, r3]
	lsl r1, r1, #8
	ldr r0, [r2, #4]
	add r0, r0, r1
	str r0, [sp, #0x10]
	mov r0, #0x3a
	ldrsh r1, [r2, r0]
	lsl r1, r1, #8
	ldr r0, [r2, #8]
	add r0, r0, r1
	str r0, [r4, #4]
	ldrh r0, [r2, #0x3c]
	sub r0, #1
	strh r0, [r4, #8]
	ldrh r0, [r2, #0x3e]
	sub r0, #1
	strh r0, [r4, #0xa]
	add r0, r7, #0
	bl getVelocityDirection
	lsl r0, r0, #0x18
	lsr r5, r0, #0x18
	add r0, r5, #0
	ldr r1, [sp, #0x24]
	and r0, r1
	mov sl, r4
	cmp r0, #0
	beq _0801B5EE
	ldr r1, [r7, #4]
	mov r0, sl
	bl sub_8017084
	add r2, r0, #0
	mov r4, sb
	add r4, #0x51
	cmp r2, #0
	beq _0801B5E2
	ldr r0, [r7, #4]
	add r0, r0, r2
	str r0, [r7, #4]
	ldrb r1, [r4]
	mov r0, #1
	orr r0, r1
	strb r0, [r4]
_0801B5E2:
	mov r0, sb
	add r0, #0x4d
	add r2, sp, #0x24
	ldrb r2, [r2]
	strb r2, [r0]
	b _0801B62A
_0801B5EE:
	mov r0, #2
	and r0, r5
	mov r4, sb
	add r4, #0x51
	cmp r0, #0
	beq _0801B62A
	ldr r1, [r7, #4]
	mov r0, sl
	bl sub_80170A4
	add r2, r0, #0
	cmp r2, #0
	beq _0801B620
	ldr r0, [r7, #4]
	add r0, r0, r2
	str r0, [r7, #4]
	ldrb r1, [r4]
	mov r0, #2
	orr r0, r1
	strb r0, [r4]
	mov r1, sb
	add r1, #0x4d
	mov r0, #0
	strb r0, [r1]
	b _0801B62A
_0801B620:
	mov r0, sb
	add r0, #0x4d
	add r3, sp, #0x24
	ldrb r3, [r3]
	strb r3, [r0]
_0801B62A:
	mov r1, sl
	ldr r0, [r1, #4]
	ldr r1, [r7, #4]
	add r0, r0, r1
	mov r2, sl
	str r0, [r2, #4]
	mov r0, #4
	and r0, r5
	cmp r0, #0
	beq _0801B658
	ldr r1, [r7]
	mov r0, sl
	bl sub_80170C4
	add r2, r0, #0
	cmp r2, #0
	beq _0801B67C
	ldr r0, [r7]
	add r0, r0, r2
	str r0, [r7]
	ldrb r1, [r4]
	mov r0, #4
	b _0801B678
_0801B658:
	mov r0, #8
	and r5, r0
	cmp r5, #0
	beq _0801B67C
	ldr r1, [r7]
	mov r0, sl
	bl sub_80170E4
	add r2, r0, #0
	cmp r2, #0
	beq _0801B67C
	ldr r0, [r7]
	add r0, r0, r2
	str r0, [r7]
	ldrb r1, [r4]
	mov r0, #8
_0801B678:
	orr r0, r1
	strb r0, [r4]
_0801B67C:
	ldr r0, [sp, #0x10]
	ldr r1, [r7]
	add r0, r0, r1
	str r0, [sp, #0x10]
	mov r0, sl
	mov r1, sp
	bl sub_801F7A4
	mov ip, r0
	cmp r0, #0
	bne _0801B694
	b _0801B85C
_0801B694:
	mov r0, #0
	str r0, [sp, #0x1c]
	add r1, sp, #0x1c
	str r0, [r1, #4]
	mov r5, #0
	mov r2, #0xc8
	lsl r2, r2, #7
	ldr r0, [sp]
	mov r8, r1
	cmp r0, #0
	beq _0801B6C0
	add r1, r0, #0
	cmp r0, #0
	bge _0801B6B2
	neg r1, r0
_0801B6B2:
	cmp r2, r1
	ble _0801B6C0
	add r2, r0, #0
	cmp r2, #0
	bge _0801B6BE
	neg r2, r2
_0801B6BE:
	mov r5, #1
_0801B6C0:
	ldr r6, [sp, #4]
	cmp r6, #0
	beq _0801B6DC
	add r0, r6, #0
	cmp r6, #0
	bge _0801B6CE
	neg r0, r6
_0801B6CE:
	cmp r2, r0
	ble _0801B6DC
	add r2, r6, #0
	cmp r2, #0
	bge _0801B6DA
	neg r2, r2
_0801B6DA:
	mov r5, #2
_0801B6DC:
	ldr r3, [sp, #8]
	cmp r3, #0
	beq _0801B6F8
	add r0, r3, #0
	cmp r3, #0
	bge _0801B6EA
	neg r0, r3
_0801B6EA:
	cmp r2, r0
	ble _0801B6F8
	add r2, r3, #0
	cmp r2, #0
	bge _0801B6F6
	neg r2, r2
_0801B6F6:
	mov r5, #4
_0801B6F8:
	ldr r1, [sp, #0xc]
	cmp r1, #0
	beq _0801B70C
	add r0, r1, #0
	cmp r1, #0
	bge _0801B706
	neg r0, r1
_0801B706:
	cmp r2, r0
	ble _0801B70C
	mov r5, #8
_0801B70C:
	mov r2, #1
	and r2, r5
	cmp r2, #0
	beq _0801B732
	ldr r0, [sp]
	mov r3, r8
	str r0, [r3, #4]
	ldrb r1, [r4]
	mov r0, #1
	orr r0, r1
	strb r0, [r4]
	mov r0, ip
	ldr r1, [r0, #0x24]
	cmp r1, #0
	bge _0801B7A0
	ldr r0, [r3, #4]
	add r0, r1, r0
	str r0, [r3, #4]
	b _0801B7A0
_0801B732:
	mov r0, #2
	and r0, r5
	cmp r0, #0
	beq _0801B766
	mov r1, r8
	str r6, [r1, #4]
	ldrb r1, [r4]
	mov r0, #2
	orr r0, r1
	strb r0, [r4]
	mov r0, sb
	add r0, #0x4d
	strb r2, [r0]
	mov r2, ip
	ldr r0, [r2, #0x20]
	ldr r1, [sp, #0x1c]
	add r0, r0, r1
	str r0, [sp, #0x1c]
	ldr r0, [r2, #0x24]
	mov r3, r8
	ldr r1, [r3, #4]
	add r0, r0, r1
	str r0, [r3, #4]
	mov r0, #2
	str r0, [sp, #0x24]
	b _0801B7A0
_0801B766:
	mov r0, #4
	and r0, r5
	cmp r0, #0
	beq _0801B782
	str r3, [sp, #0x1c]
	ldrb r1, [r4]
	mov r0, #4
	orr r0, r1
	strb r0, [r4]
	mov r2, ip
	ldr r1, [r2, #0x20]
	cmp r1, #0
	ble _0801B7A0
	b _0801B79A
_0801B782:
	mov r0, #8
	and r5, r0
	cmp r5, #0
	beq _0801B7A0
	str r1, [sp, #0x1c]
	ldrb r1, [r4]
	orr r0, r1
	strb r0, [r4]
	mov r3, ip
	ldr r1, [r3, #0x20]
	cmp r1, #0
	bge _0801B7A0
_0801B79A:
	ldr r0, [sp, #0x1c]
	add r0, r1, r0
	str r0, [sp, #0x1c]
_0801B7A0:
	mov r0, r8
	bl getVelocityDirection
	lsl r0, r0, #0x18
	lsr r5, r0, #0x18
	mov r0, #1
	and r0, r5
	cmp r0, #0
	beq _0801B7D0
	mov r0, r8
	ldr r1, [r0, #4]
	mov r0, sl
	bl sub_8017084
	add r2, r0, #0
	cmp r2, #0
	beq _0801B7F8
	mov r1, r8
	ldr r0, [r1, #4]
	add r0, r0, r2
	str r0, [r1, #4]
	ldrb r1, [r4]
	mov r0, #1
	b _0801B7F4
_0801B7D0:
	mov r0, #2
	and r0, r5
	cmp r0, #0
	beq _0801B7F8
	mov r2, r8
	ldr r1, [r2, #4]
	mov r0, sl
	bl sub_80170A4
	add r2, r0, #0
	cmp r2, #0
	beq _0801B7F8
	mov r3, r8
	ldr r0, [r3, #4]
	add r0, r0, r2
	str r0, [r3, #4]
	ldrb r1, [r4]
	mov r0, #2
_0801B7F4:
	orr r0, r1
	strb r0, [r4]
_0801B7F8:
	mov r2, sl
	ldr r0, [r2, #4]
	mov r3, r8
	ldr r1, [r3, #4]
	add r0, r0, r1
	str r0, [r2, #4]
	mov r0, #4
	and r0, r5
	cmp r0, #0
	beq _0801B826
	ldr r1, [sp, #0x1c]
	add r0, r2, #0
	bl sub_80170C4
	add r2, r0, #0
	cmp r2, #0
	beq _0801B84A
	ldr r0, [sp, #0x1c]
	add r0, r0, r2
	str r0, [sp, #0x1c]
	ldrb r1, [r4]
	mov r0, #4
	b _0801B846
_0801B826:
	mov r0, #8
	and r5, r0
	cmp r5, #0
	beq _0801B84A
	ldr r1, [sp, #0x1c]
	mov r0, sl
	bl sub_80170E4
	add r2, r0, #0
	cmp r2, #0
	beq _0801B84A
	ldr r0, [sp, #0x1c]
	add r0, r0, r2
	str r0, [sp, #0x1c]
	ldrb r1, [r4]
	mov r0, #8
_0801B846:
	orr r0, r1
	strb r0, [r4]
_0801B84A:
	ldr r0, [r7]
	ldr r1, [sp, #0x1c]
	add r0, r0, r1
	str r0, [r7]
	ldr r0, [r7, #4]
	mov r2, r8
	ldr r1, [r2, #4]
	add r0, r0, r1
	str r0, [r7, #4]
_0801B85C:
	mov r0, sb
	add r0, #0x4f
	ldrb r1, [r0]
	ldrb r2, [r4]
	orr r1, r2
	strb r1, [r0]
	ldr r0, [sp, #0x24]
	add sp, #0x28
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
	thumb_func_global sub_801B87C
sub_801B87C: @ 0x0801B87C
	push {r4, lr}
	add r4, r1, #0
	ldr r0, _0801B8AC
	ldr r1, [r0]
	ldr r0, [r1, #4]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	ldr r1, [r1, #8]
	lsl r1, r1, #8
	lsr r1, r1, #0x10
	bl getCollision
	lsl r1, r0, #0x10
	mov r0, #0xf0
	lsl r0, r0, #0xc
	and r0, r1
	lsr r0, r0, #0x10
	cmp r0, #1
	bne _0801B8B0
	lsr r0, r1, #0x14
	cmp r0, #0
	bne _0801B8B0
	str r0, [r4, #4]
	b _0801B8B6
	.align 2, 0
_0801B8AC: .4byte 0x03004750
_0801B8B0:
	mov r0, #0x30
	bl set_player_18
_0801B8B6:
	mov r0, #1
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
	thumb_func_global sub_801B8C0
sub_801B8C0: @ 0x0801B8C0
	add r2, r0, #0
	ldr r1, [r2]
	add r0, r1, #0
	cmp r1, #0
	bge _0801B8CC
	neg r0, r1
_0801B8CC:
	cmp r0, #0xff
	bgt _0801B8D4
	mov r0, #0
	b _0801B8F0
_0801B8D4:
	add r0, r1, #0
	cmp r0, #0
	ble _0801B8E8
	mov r1, #0x80
	lsl r1, r1, #1
	ldr r3, _0801B8E4
	b _0801B8EE
	.align 2, 0
_0801B8E4: .4byte 0xFFFFFF00
_0801B8E8:
	ldr r1, _0801B8F8
	mov r3, #0x80
	lsl r3, r3, #1
_0801B8EE:
	add r0, r0, r3
_0801B8F0:
	str r0, [r2]
	add r0, r1, #0
	bx lr
	.align 2, 0
_0801B8F8: .4byte 0xFFFFFF00
	thumb_func_global sub_801B8FC
sub_801B8FC: @ 0x0801B8FC
	push {r4, r5, r6, r7, lr}
	sub sp, #0x10
	add r4, r0, #0
	ldr r2, [r1]
	ldr r3, [r1, #4]
	str r2, [sp]
	str r3, [sp, #4]
	str r2, [sp, #8]
	str r3, [sp, #0xc]
	mov r0, #0
	str r0, [r1, #4]
	str r0, [r1]
	mov r6, sp
	add r5, sp, #8
	mov r7, #0
_0801B91A:
	mov r0, sp
	bl sub_801B8C0
	str r0, [sp, #8]
	ldr r0, [r6, #4]
	str r0, [r5, #4]
	add r0, r4, #0
	add r1, r5, #0
	bl sub_801B570
	cmp r0, #2
	bne _0801B934
	str r7, [r6, #4]
_0801B934:
	ldr r0, [r4, #4]
	ldr r1, [sp, #8]
	add r0, r0, r1
	str r0, [r4, #4]
	ldr r0, [r4, #8]
	ldr r1, [r5, #4]
	add r0, r0, r1
	str r0, [r4, #8]
	ldr r0, _0801B958
	ldrh r0, [r0, #4]
	cmp r0, #3
	bne _0801B95C
	add r0, r4, #0
	add r0, #0x4d
	ldrb r0, [r0]
	cmp r0, #1
	bne _0801B95E
	b _0801B964
	.align 2, 0
_0801B958: .4byte 0x03004750
_0801B95C:
	str r7, [r6, #4]
_0801B95E:
	ldr r0, [sp, #8]
	cmp r0, #0
	bne _0801B91A
_0801B964:
	mov r0, #1
	add sp, #0x10
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	

thumb_func_global sub_0801B970
sub_0801B970: @ 0x0801B970
	ldr r2, _0801B998
	add r0, r2, #0
	add r0, #0x24
	ldrb r0, [r0]
	lsl r0, r0, #0x1d
	cmp r0, #0
	blt _0801B9AC
	ldr r0, _0801B99C
	ldrb r0, [r0]
	cmp r0, #0
	beq _0801B9C0
	ldr r1, _0801B9A0
	add r0, r2, #0
	add r0, #0x8c
	ldr r0, [r0]
	str r0, [r1]
	ldr r0, _0801B9A4
	str r0, [r1, #4]
	ldr r0, _0801B9A8
	b _0801B9BC
	.align 2, 0
_0801B998: .4byte 0x03004750
_0801B99C: .4byte 0x03001D70
_0801B9A0: .4byte 0x040000B0
_0801B9A4: .4byte 0x04000010
_0801B9A8: .4byte 0xA2600002
_0801B9AC:
	ldr r1, _0801B9C4
	add r0, r2, #0
	add r0, #0x8c
	ldr r0, [r0]
	str r0, [r1]
	ldr r0, _0801B9C8
	str r0, [r1, #4]
	ldr r0, _0801B9CC
_0801B9BC:
	str r0, [r1, #8]
	ldr r0, [r1, #8]
_0801B9C0:
	bx lr
	.align 2, 0
_0801B9C4: .4byte 0x040000B0
_0801B9C8: .4byte 0x04000010
_0801B9CC: .4byte 0xA2600003

thumb_func_global sub_0801B9D0
sub_0801B9D0: @ 0x0801B9D0
	push {r4, r5, r6, lr}
	ldr r0, _0801BA94
	ldrh r5, [r0]
	ldr r1, _0801BA98
	add r0, r1, #0
	add r0, #0x24
	ldrb r2, [r0]
	lsl r0, r2, #0x1d
	mov ip, r1
	cmp r0, #0
	blt _0801BA8C
	lsl r0, r2, #0x1e
	cmp r0, #0
	bge _0801BA8C
	mov r4, ip
	add r4, #0x98
	ldrb r0, [r4]
	add r3, r0, #0
	cmp r3, #0x1f
	bhi _0801BA08
	ldr r2, _0801BA9C
	ldr r1, _0801BAA0
	add r0, #1
	strb r0, [r4]
	lsl r0, r3, #1
	add r0, r0, r1
	ldrh r0, [r0]
	strh r0, [r2]
_0801BA08:
	mov r1, ip
	ldrh r0, [r1, #4]
	cmp r0, #0x15
	bne _0801BA2C
	mov r3, ip
	add r3, #0x99
	ldrb r0, [r3]
	cmp r0, #0
	beq _0801BA1E
	sub r0, #1
	strb r0, [r3]
_0801BA1E:
	ldr r2, _0801BA9C
	ldr r1, _0801BAA4
	ldrb r0, [r3]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	strh r0, [r2]
_0801BA2C:
	mov r0, ip
	add r0, #0x90
	ldr r4, [r0]
	mov r2, #0
	ldr r3, _0801BAA8
_0801BA36:
	add r1, r2, r5
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #0x18
	lsl r1, r2, #2
	add r1, r1, r2
	add r1, r1, r5
	lsl r1, r1, #0x18
	lsr r0, r0, #0x17
	add r0, #0x80
	add r0, r0, r3
	mov r6, #0
	ldrsh r0, [r0, r6]
	asr r0, r0, #7
	add r0, #8
	strh r0, [r4]
	add r4, #2
	lsr r1, r1, #0x17
	add r1, r1, r3
	mov r6, #0
	ldrsh r0, [r1, r6]
	asr r0, r0, #6
	add r0, #0x30
	strh r0, [r4]
	add r4, #2
	add r0, r2, #1
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	cmp r2, #0x9f
	bls _0801BA36
	ldr r3, _0801BAAC
	mov r0, #0
	strh r0, [r3]
	mov r2, ip
	add r2, #0x90
	ldr r4, [r2]
	mov r1, ip
	add r1, #0x8c
	ldr r0, [r1]
	str r0, [r2]
	str r4, [r1]
	mov r0, #1
	strh r0, [r3]
_0801BA8C:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_0801BA94: .4byte 0x03002080
_0801BA98: .4byte 0x03004750
_0801BA9C: .4byte 0x03003D94
_0801BAA0: .4byte unk_813d28a
_0801BAA4: .4byte dword_813D278
_0801BAA8: .4byte dword_80382C8
_0801BAAC: .4byte 0x04000208

thumb_func_global sub_0801BAB0
sub_0801BAB0: @ 0x0801BAB0
	push {r4, r5, r6, r7, lr}
	sub sp, #4
	ldr r4, _0801BB48
	add r6, r4, #0
	add r6, #0x24
	ldrb r1, [r6]
	lsl r0, r1, #0x1d
	cmp r0, #0
	blt _0801BB78
	lsl r0, r1, #0x1e
	lsr r5, r0, #0x1f
	cmp r5, #0
	bne _0801BB7C
	bl setBG0_killSpecial
	ldr r2, _0801BB4C
	ldrh r1, [r2]
	add r0, r4, #0
	add r0, #0x94
	mov r3, #0
	strh r1, [r0]
	ldr r1, _0801BB50
	ldrh r0, [r1]
	mov r7, #0x96
	strh r0, [r7, r4]
	ldr r7, _0801BB54
	add r0, r7, #0
	strh r0, [r2]
	mov r2, #0x80
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	add r0, r4, #0
	add r0, #0x98
	strb r3, [r0]
	add r0, #1
	strb r3, [r0]
	ldr r1, _0801BB58
	ldrh r2, [r1, #0xa]
	ldr r0, _0801BB5C
	and r0, r2
	strh r0, [r1, #0xa]
	ldrh r2, [r1, #0xa]
	ldr r0, _0801BB60
	and r0, r2
	strh r0, [r1, #0xa]
	ldrh r0, [r1, #0xa]
	str r5, [sp]
	ldr r0, _0801BB64
	mov r7, sp
	str r7, [r0]
	ldr r3, _0801BB68
	str r3, [r0, #4]
	ldr r1, _0801BB6C
	str r1, [r0, #8]
	ldr r2, [r0, #8]
	str r5, [sp]
	str r7, [r0]
	ldr r2, _0801BB70
	str r2, [r0, #4]
	str r1, [r0, #8]
	ldr r0, [r0, #8]
	add r0, r4, #0
	add r0, #0x8c
	str r3, [r0]
	add r0, #4
	str r2, [r0]
	ldrb r0, [r6]
	mov r1, #2
	orr r0, r1
	strb r0, [r6]
	ldr r1, _0801BB74
	mov r0, #1
	strb r0, [r1]
	b _0801BB7C
	.align 2, 0
_0801BB48: .4byte 0x03004750
_0801BB4C: .4byte 0x03003D74
_0801BB50: .4byte 0x03003D94
_0801BB54: .4byte 0x00003F41
_0801BB58: .4byte 0x040000B0
_0801BB5C: .4byte 0x0000C5FF
_0801BB60: .4byte 0x00007FFF
_0801BB64: .4byte 0x040000D4
_0801BB68: .4byte 0x0200A210
_0801BB6C: .4byte 0x850000A0
_0801BB70: .4byte 0x02020160
_0801BB74: .4byte 0x03001D70
_0801BB78:
	bl setBG0_killSpecial
_0801BB7C:
	add sp, #4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0

thumb_func_global sub_0801BB84
sub_0801BB84: @ 0x0801BB84
	push {r4, r5, r6, lr}
	ldr r3, _0801BBE0
	add r6, r3, #0
	add r6, #0x24
	ldrb r5, [r6]
	lsl r0, r5, #0x1d
	lsr r4, r0, #0x1f
	cmp r4, #0
	bne _0801BBFC
	lsl r0, r5, #0x1e
	cmp r0, #0
	bge _0801BC00
	ldr r0, _0801BBE4
	strb r4, [r0]
	ldr r1, _0801BBE8
	ldrh r2, [r1, #0xa]
	ldr r0, _0801BBEC
	and r0, r2
	strh r0, [r1, #0xa]
	ldrh r2, [r1, #0xa]
	ldr r0, _0801BBF0
	and r0, r2
	strh r0, [r1, #0xa]
	ldrh r0, [r1, #0xa]
	ldr r1, _0801BBF4
	add r0, r3, #0
	add r0, #0x94
	ldrh r0, [r0]
	strh r0, [r1]
	ldr r1, _0801BBF8
	add r0, r3, #0
	add r0, #0x96
	ldrh r0, [r0]
	strh r0, [r1]
	add r0, r3, #0
	add r0, #0x98
	strb r4, [r0]
	add r0, #1
	strb r4, [r0]
	mov r0, #3
	neg r0, r0
	and r0, r5
	strb r0, [r6]
	bl setBG0_toConst
	b _0801BC00
	.align 2, 0
_0801BBE0: .4byte 0x03004750
_0801BBE4: .4byte 0x03001D70
_0801BBE8: .4byte 0x040000B0
_0801BBEC: .4byte 0x0000C5FF
_0801BBF0: .4byte 0x00007FFF
_0801BBF4: .4byte 0x03003D74
_0801BBF8: .4byte 0x03003D94
_0801BBFC:
	bl setBG0_toConst
_0801BC00:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global sub_0801BC08
sub_0801BC08: @ 0x0801BC08
	push {lr}
	ldr r0, _0801BC24
	ldrh r0, [r0, #4]
	cmp r0, #0x15
	bgt _0801BC1E
	cmp r0, #0x13
	blt _0801BC1E
	mov r0, #0x80
	mov r1, #0x80
	bl sub_08016A84
_0801BC1E:
	pop {r0}
	bx r0
	.align 2, 0
_0801BC24: .4byte 0x03004750

thumb_func_global sub_0801BC28
sub_0801BC28: @ 0x0801BC28
	push {r4, r5, r6, lr}
	sub sp, #4
	ldr r3, _0801BC84
	add r6, r3, #0
	add r6, #0x24
	ldrb r5, [r6]
	lsl r0, r5, #0x1c
	cmp r0, #0
	bge _0801BC7C
	ldr r4, [r3]
	ldr r0, [r3, #0x3c]
	ldr r1, [r4, #8]
	sub r0, r0, r1
	ldr r1, _0801BC88
	add r2, r4, #0
	cmp r0, r1
	bgt _0801BC54
	add r0, r2, #0
	add r0, #0x4d
	ldrb r0, [r0]
	cmp r0, #0
	bne _0801BC7C
_0801BC54:
	mov r0, #9
	neg r0, r0
	and r0, r5
	strb r0, [r6]
	ldr r1, _0801BC8C
	ldrh r0, [r3, #6]
	lsl r0, r0, #3
	add r0, r0, r1
	mov r2, #0
	ldrsh r1, [r0, r2]
	mov r3, #2
	ldrsh r2, [r0, r3]
	mov r5, #4
	ldrsh r3, [r0, r5]
	mov r5, #6
	ldrsh r0, [r0, r5]
	str r0, [sp]
	add r0, r4, #0
	bl setObjSize
_0801BC7C:
	add sp, #4
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_0801BC84: .4byte 0x03004750
_0801BC88: .4byte 0x00000FFF
_0801BC8C: .4byte dword_813C68C

thumb_func_global sub_0801BC90
sub_0801BC90: @ 0x0801BC90
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	bl isUsingInvulnSpecial
	cmp r0, #0
	bne _0801BD6E
	ldr r0, _0801BD50
	ldrb r0, [r0, #1]
	cmp r0, #3
	bls _0801BD6E
	ldr r2, _0801BD54
	add r0, r2, #0
	add r0, #0x22
	ldrb r1, [r0]
	mov r0, #0x18
	and r0, r1
	cmp r0, #0
	bne _0801BD6E
	ldr r2, [r2]
	mov r0, #0x38
	ldrsh r1, [r2, r0]
	lsl r1, r1, #8
	ldr r0, [r2, #4]
	add r0, r0, r1
	lsl r0, r0, #8
	lsr r7, r0, #0x10
	mov r6, #0x3a
	ldrsh r1, [r2, r6]
	lsl r1, r1, #8
	ldr r0, [r2, #8]
	add r0, r0, r1
	lsl r0, r0, #8
	lsr r5, r0, #0x10
	ldrh r0, [r2, #0x3c]
	add r0, r7, r0
	sub r0, #1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r8, r0
	ldrh r0, [r2, #0x3e]
	sub r0, r5, r0
	add r0, #1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sl, r0
	mov r4, #0
	ldr r0, _0801BD58
	mov sb, r0
_0801BCF6:
	lsl r0, r4, #0x10
	lsr r0, r0, #0x10
	bl sub_080152F8
	add r1, r0, #0
	cmp r1, #0
	beq _0801BD68
	ldrh r0, [r1]
	cmp r0, sb
	beq _0801BD68
	ldr r2, _0801BD50
	ldr r3, _0801BD58
_0801BD0E:
	ldrh r6, [r1, #4]
	cmp r7, r6
	bhi _0801BD60
	cmp r8, r0
	blo _0801BD60
	ldrh r0, [r1, #2]
	cmp r5, r0
	blo _0801BD60
	ldrh r6, [r1, #6]
	cmp sl, r6
	bhi _0801BD60
	ldrb r0, [r2, #0x11]
	lsl r0, r0, #1
	add r1, #8
	add r1, r1, r0
	mov r0, #0
	ldrsh r4, [r1, r0]
	ldr r0, _0801BD54
	ldrh r1, [r0, #0x10]
	ldrb r0, [r2, #2]
	mov r2, #1
	cmp r0, #2
	bne _0801BD3E
	mov r2, #2
_0801BD3E:
	ldr r3, _0801BD5C
	cmp r0, #2
	bne _0801BD46
	add r3, #1
_0801BD46:
	add r0, r4, #0
	bl sub_0801BD80
	mov r0, #1
	b _0801BD70
	.align 2, 0
_0801BD50: .4byte 0x03003D40
_0801BD54: .4byte 0x03004750
_0801BD58: .4byte 0x0000FFFF
_0801BD5C: .4byte 0x0000010B
_0801BD60:
	add r1, #0x10
	ldrh r0, [r1]
	cmp r0, r3
	bne _0801BD0E
_0801BD68:
	add r4, #1
	cmp r4, #4
	ble _0801BCF6
_0801BD6E:
	mov r0, #0
_0801BD70:
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global sub_0801BD80
sub_0801BD80: @ 0x0801BD80
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x30
	str r3, [sp, #0x28]
	lsl r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov sl, r1
	lsl r2, r2, #0x10
	lsr r6, r2, #0x10
	ldr r5, _0801BDF4
	ldrh r1, [r5, #4]
	mov sb, r1
	add r2, sp, #4
	ldr r1, _0801BDF8
	ldm r1!, {r3, r4, r7}
	stm r2!, {r3, r4, r7}
	mov r1, sp
	add r1, #0x10
	str r1, [sp, #0x2c]
	add r2, r1, #0
	ldr r1, _0801BDFC
	ldm r1!, {r3, r4, r7}
	stm r2!, {r3, r4, r7}
	ldm r1!, {r3, r4, r7}
	stm r2!, {r3, r4, r7}
	lsr r1, r0, #0x10
	mov r8, r1
	asr r0, r0, #0x10
	bl modifyPlayerHealth
	add r7, r5, #0
	add r7, #0x23
	ldrb r1, [r7]
	mov r0, #5
	neg r0, r0
	and r0, r1
	mov r1, #0x11
	neg r1, r1
	and r0, r1
	strb r0, [r7]
	add r1, r5, #0
	add r1, #0xa0
	mov r0, #0
	strh r0, [r1]
	bl _3005773_clearBits3_5
	cmp r6, #0
	beq _0801BE2A
	cmp r6, #1
	beq _0801BE00
	cmp r6, #2
	beq _0801BE18
	b _0801BE2A
	.align 2, 0
_0801BDF4: .4byte 0x03004750
_0801BDF8: .4byte knockback_1
_0801BDFC: .4byte knockback_2
_0801BE00:
	mov r0, #3
	bl setPlayerPaletteMaybe
	add r1, r5, #0
	add r1, #0xa8
	mov r0, #0x18
	strh r0, [r1]
	ldrb r0, [r7]
	mov r1, #0x10
	orr r0, r1
	mov r1, #0x20
	b _0801BE26
_0801BE18:
	mov r0, #2
	bl setPlayerPaletteMaybe
	ldrb r0, [r7]
	mov r1, #4
	orr r0, r1
	mov r1, #8
_0801BE26:
	orr r0, r1
	strb r0, [r7]
_0801BE2A:
	ldr r4, _0801BE90
	ldr r0, [r4]
	bl clearObjForces
	add r0, r4, #0
	add r0, #0xac
	mov r5, #0
	str r5, [r0]
	ldrh r0, [r4, #4]
	cmp r0, #8
	bne _0801BE4A
	ldr r2, [r4]
	ldr r0, [r4, #0x34]
	ldr r1, [r4, #0x38]
	str r0, [r2, #4]
	str r1, [r2, #8]
_0801BE4A:
	add r0, r4, #0
	add r0, #0xb0
	ldrh r0, [r0]
	cmp r0, #0
	beq _0801BE58
	bl grapplingHookDeath
_0801BE58:
	add r0, r4, #0
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x1f
	cmp r0, #0
	beq _0801BECC
	bl sub_0801C638
	cmp r0, #0
	beq _0801BECC
	mov r2, sb
	cmp r2, #9
	beq _0801BE94
	mov r0, #1
	bl sub_80190F4
	add r0, r4, #0
	add r0, #0x23
	ldrb r0, [r0]
	lsl r0, r0, #0x1d
	lsr r0, r0, #0x1f
	add r0, #2
	bl setPlayerSprite
	ldr r0, [r4]
	add r0, #0x4e
	strb r5, [r0]
	b _0801BFE4
	.align 2, 0
_0801BE90: .4byte 0x03004750
_0801BE94:
	ldr r0, [r4]
	ldr r2, [r0]
	ldr r0, _0801BEC8
	add r1, r0, #0
	add r1, #0xa0
	ldr r1, [r1]
	str r1, [r2, #4]
	add r0, #0xa4
	ldr r0, [r0]
	str r0, [r2, #8]
	add r3, r2, #0
	add r3, #0x23
	ldrb r0, [r3]
	mov r1, #0x80
	orr r0, r1
	strb r0, [r3]
	add r2, #0x24
	ldrb r0, [r2]
	mov r1, #1
	orr r0, r1
	strb r0, [r2]
	mov r0, #0x14
	strh r0, [r4, #0xa]
	strh r0, [r4, #6]
	b _0801BFE4
	.align 2, 0
_0801BEC8: .4byte playerAnimations
_0801BECC:
	mov r3, r8
	lsl r0, r3, #0x10
	asr r1, r0, #0x10
	mov r0, #5
	neg r0, r0
	cmp r1, r0
	blt _0801BEDE
	mov r6, #0
	b _0801BEEA
_0801BEDE:
	mov r0, #0xa
	neg r0, r0
	mov r6, #2
	cmp r1, r0
	blt _0801BEEA
	mov r6, #1
_0801BEEA:
	ldr r5, _0801BF80
	add r2, r5, #0
	add r2, #0x24
	ldrb r1, [r2]
	lsl r0, r1, #0x1c
	cmp r0, #0
	bge _0801BF28
	mov r0, #9
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	ldr r0, [r5]
	ldr r1, [r5, #0x3c]
	ldr r4, _0801BF84
	add r1, r1, r4
	str r1, [r0, #8]
	ldr r1, _0801BF88
	ldrh r4, [r5, #6]
	lsl r4, r4, #3
	add r4, r4, r1
	mov r7, #0
	ldrsh r1, [r4, r7]
	mov r3, #2
	ldrsh r2, [r4, r3]
	mov r7, #4
	ldrsh r3, [r4, r7]
	mov r7, #6
	ldrsh r4, [r4, r7]
	str r4, [sp]
	bl setObjSize
_0801BF28:
	mov r0, #7
	bl sub_80190F4
	ldr r0, [r5]
	lsl r1, r6, #2
	add r1, sp
	add r1, #4
	ldr r1, [r1]
	bl setObjVelocity_Y
	add r2, r5, #0
	add r2, #0xac
	mov r1, sl
	lsl r0, r1, #2
	lsl r1, r6, #3
	add r0, r0, r1
	add r0, sp
	add r0, #0x10
	ldr r0, [r0]
	str r0, [r2]
	mov r0, #0
	strh r0, [r5, #0x1e]
	add r1, r5, #0
	add r1, #0xb2
	mov r0, #1
	strh r0, [r1]
	ldr r0, [r5]
	add r0, #0x4d
	ldrb r0, [r0]
	cmp r0, #1
	bne _0801BF9E
	mov r2, sb
	cmp r2, #0xa
	beq _0801BF8C
	add r0, r5, #0
	add r0, #0x23
	ldrb r0, [r0]
	lsl r0, r0, #0x1d
	lsr r0, r0, #0x1f
	add r0, #0xb2
	bl setPlayerSprite
	b _0801BFCC
	.align 2, 0
_0801BF80: .4byte 0x03004750
_0801BF84: .4byte 0xFFFFF000
_0801BF88: .4byte dword_813C68C
_0801BF8C:
	add r0, r5, #0
	add r0, #0x23
	ldrb r0, [r0]
	lsl r0, r0, #0x1d
	lsr r0, r0, #0x1f
	add r0, #0xb4
	bl setPlayerSprite
	b _0801BFCC
_0801BF9E:
	add r0, r5, #0
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x1f
	cmp r0, #0
	beq _0801BFBC
	add r0, r5, #0
	add r0, #0x23
	ldrb r0, [r0]
	lsl r0, r0, #0x1d
	lsr r0, r0, #0x1f
	add r0, #0xb0
	bl setPlayerSprite
	b _0801BFCC
_0801BFBC:
	add r0, r5, #0
	add r0, #0x23
	ldrb r0, [r0]
	lsl r0, r0, #0x1d
	lsr r0, r0, #0x1f
	add r0, #0xae
	bl setPlayerSprite
_0801BFCC:
	ldr r2, _0801C020
	add r3, r2, #0
	add r3, #0x22
	ldrb r1, [r3]
	mov r0, #2
	neg r0, r0
	and r0, r1
	strb r0, [r3]
	ldr r0, [r2]
	add r0, #0x4e
	mov r1, #0
	strb r1, [r0]
_0801BFE4:
	bl sub_0801A93C
	ldr r2, _0801C020
	add r3, r2, #0
	add r3, #0x22
	ldrb r0, [r3]
	mov r1, #8
	orr r0, r1
	strb r0, [r3]
	ldr r0, _0801C024
	strh r0, [r2, #0x1a]
	mov r0, #0x14
	strh r0, [r2, #0x1c]
	bl removeCameraPlayerControl
	bl lowerPowerupLevelIfNeeded
	ldr r3, [sp, #0x28]
	lsl r0, r3, #0x10
	lsr r0, r0, #0x10
	bl playSong
	add sp, #0x30
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_0801C020: .4byte 0x03004750
_0801C024: .4byte 0x0000FFFF



thumb_func_global sub_801C028
sub_801C028: @ 0x0801C028
	push {r4, lr}
	ldr r4, _0801C040
	ldr r0, [r4]
	ldr r1, [r0, #8]
	mov r0, #0xa0
	lsl r0, r0, #9
	cmp r1, r0
	ble _0801C044
	mov r0, #0x14
	strh r0, [r4, #0x1c]
	b _0801C05E
	.align 2, 0
_0801C040: .4byte 0x03004750
_0801C044:
	mov r0, #0
	strh r0, [r4, #0x1c]
	bl _3005773_clearBits3_5
	ldr r1, [r4]
	add r1, #0x52
	ldrb r2, [r1]
	mov r0, #2
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	bl endCurrentTask
_0801C05E:
	pop {r4}
	pop {r0}
	bx r0




thumb_func_global sub_0801C064
sub_0801C064: @ 0x0801C064
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x28
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r8, r0
	mov r0, #1
	mov sb, r0
	ldr r5, _0801C0F0
	ldrh r6, [r5, #4]
	add r1, sp, #4
	ldr r0, _0801C0F4
	ldm r0!, {r2, r3, r4}
	stm r1!, {r2, r3, r4}
	add r4, sp, #0x10
	add r1, r4, #0
	ldr r0, _0801C0F8
	ldm r0!, {r2, r3, r7}
	stm r1!, {r2, r3, r7}
	ldm r0!, {r2, r3, r7}
	stm r1!, {r2, r3, r7}
	ldr r0, [r5]
	bl clearObjForces
	add r0, r5, #0
	add r0, #0xac
	mov r7, #0
	str r7, [r0]
	ldrh r0, [r5, #4]
	mov sl, r4
	cmp r0, #8
	bne _0801C0B4
	ldr r2, [r5]
	ldr r0, [r5, #0x34]
	ldr r1, [r5, #0x38]
	str r0, [r2, #4]
	str r1, [r2, #8]
_0801C0B4:
	add r0, r5, #0
	add r0, #0xb0
	ldrh r0, [r0]
	cmp r0, #0
	beq _0801C0C2
	bl grapplingHookDeath
_0801C0C2:
	add r0, r5, #0
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x1f
	cmp r0, #0
	beq _0801C0FC
	bl sub_0801C638
	cmp r0, #0
	beq _0801C0FC
	cmp r6, #9
	beq _0801C18C
	mov r0, #1
	bl sub_80190F4
	mov r0, #2
	bl setPlayerSprite
	ldr r0, [r5]
	add r0, #0x4e
	strb r7, [r0]
	b _0801C18C
	.align 2, 0
_0801C0F0: .4byte 0x03004750
_0801C0F4: .4byte dword_813D2F0
_0801C0F8: .4byte dword_813D2FC
_0801C0FC:
	ldr r5, _0801C1A4
	add r2, r5, #0
	add r2, #0x24
	ldrb r1, [r2]
	lsl r0, r1, #0x1c
	cmp r0, #0
	bge _0801C13A
	mov r0, #9
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	ldr r0, [r5]
	ldr r1, [r5, #0x3c]
	ldr r4, _0801C1A8
	add r1, r1, r4
	str r1, [r0, #8]
	ldr r1, _0801C1AC
	ldrh r4, [r5, #6]
	lsl r4, r4, #3
	add r4, r4, r1
	mov r6, #0
	ldrsh r1, [r4, r6]
	mov r7, #2
	ldrsh r2, [r4, r7]
	mov r6, #4
	ldrsh r3, [r4, r6]
	mov r7, #6
	ldrsh r4, [r4, r7]
	str r4, [sp]
	bl setObjSize
_0801C13A:
	mov r0, #7
	bl sub_80190F4
	ldr r0, [r5]
	mov r2, sb
	lsl r1, r2, #2
	add r1, sp
	add r1, #4
	ldr r1, [r1]
	bl setObjVelocity_Y
	add r2, r5, #0
	add r2, #0xac
	mov r3, r8
	lsl r0, r3, #2
	mov r4, sb
	lsl r1, r4, #3
	add r0, r0, r1
	add r0, sl
	ldr r0, [r0]
	str r0, [r2]
	mov r4, #0
	mov r0, #0
	strh r0, [r5, #0x1e]
	add r1, r5, #0
	add r1, #0xb2
	mov r0, #1
	strh r0, [r1]
	mov r0, #0x10
	bl setPlayerSprite
	add r2, r5, #0
	add r2, #0x22
	ldrb r1, [r2]
	mov r0, #2
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	ldr r0, [r5]
	add r0, #0x4e
	strb r4, [r0]
_0801C18C:
	bl sub_0801A93C
	bl removeCameraPlayerControl
	add sp, #0x28
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_0801C1A4: .4byte 0x03004750
_0801C1A8: .4byte 0xFFFFF000
_0801C1AC: .4byte dword_813C68C

thumb_func_global sub_0801C1B0
sub_0801C1B0: @ 0x0801C1B0
	push {r4, r5, lr}
	mov r5, #0
	ldr r4, _0801C238
	add r0, r4, #0
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	cmp r0, #0
	blt _0801C230
	add r0, r4, #0
	add r0, #0xa8
	ldrh r0, [r0]
	cmp r0, #0
	bne _0801C230
	bl isUsingInvulnSpecial
	cmp r0, #0
	bne _0801C230
	add r0, r4, #0
	add r0, #0x23
	ldrb r0, [r0]
	lsl r0, r0, #0x1e
	cmp r0, #0
	blt _0801C230
	ldrh r0, [r4, #0x1c]
	cmp r0, #0
	bne _0801C230
	bl getPlayerSpecialBar
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0x4f
	bls _0801C230
	bl getPlayerHealth
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0801C230
	add r0, r4, #0
	add r0, #0x9a
	ldrh r2, [r0]
	mov r3, #1
	add r0, r3, #0
	and r0, r2
	cmp r0, #0
	bne _0801C218
	ldr r0, _0801C23C
	ldrh r1, [r0, #0xc]
	add r0, r3, #0
	and r0, r1
	cmp r0, #0
	beq _0801C230
_0801C218:
	mov r3, #2
	add r0, r3, #0
	and r0, r2
	cmp r0, #0
	bne _0801C22E
	ldr r0, _0801C23C
	ldrh r1, [r0, #0xc]
	add r0, r3, #0
	and r0, r1
	cmp r0, #0
	beq _0801C230
_0801C22E:
	mov r5, #1
_0801C230:
	add r0, r5, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
_0801C238: .4byte 0x03004750
_0801C23C: .4byte 0x03002080

thumb_func_global sub_0801C240
sub_0801C240: @ 0x0801C240
	push {r4, r5, r6, lr}
	add r6, r0, #0
	ldr r4, _0801C2D0
	add r5, r4, #0
	add r5, #0x22
	ldrb r0, [r5]
	lsl r0, r0, #0x1f
	cmp r0, #0
	beq _0801C272
	add r2, r4, #0
	add r2, #0x24
	ldrb r0, [r2]
	mov r1, #8
	orr r0, r1
	strb r0, [r2]
	ldr r0, [r4]
	ldr r0, [r0, #8]
	lsl r0, r0, #8
	asr r0, r0, #0x10
	add r0, #8
	mov r1, #8
	neg r1, r1
	and r0, r1
	lsl r0, r0, #8
	str r0, [r4, #0x3c]
_0801C272:
	mov r0, #7
	bl sub_80190F4
	mov r0, #0x10
	bl setPlayerSprite
	bl sub_0801A93C
	add r1, r4, #0
	add r1, #0xb2
	mov r3, #0
	mov r2, #0
	mov r0, #1
	strh r0, [r1]
	ldrb r0, [r5]
	mov r1, #2
	neg r1, r1
	and r1, r0
	add r0, r4, #0
	add r0, #0xa0
	strh r2, [r0]
	strh r2, [r4, #0x1e]
	mov r0, #0x7f
	and r1, r0
	strb r1, [r5]
	add r0, r4, #0
	add r0, #0x20
	strb r3, [r0]
	add r0, #0x8c
	str r2, [r0]
	ldr r0, [r4]
	bl clearObjForces
	ldr r0, [r4]
	add r1, r6, #0
	bl setObjVelocity_X
	bl sub_0801394C
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0801C2CA
	bl removeCameraPlayerControl
_0801C2CA:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_0801C2D0: .4byte 0x03004750

thumb_func_global sub_0801C2D4
sub_0801C2D4: @ 0x0801C2D4
	push {r4, r5, lr}
	add r4, r0, #0
	add r5, r1, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	bl sub_0801A93C
	mov r0, #0xb
	bl sub_80190F4
	add r0, r4, #0
	mov r1, #2
	bl sub_80191B0
	add r0, r5, #0
	bl set_player_18
	ldr r2, _0801C324
	add r3, r2, #0
	add r3, #0x22
	ldrb r0, [r3]
	mov r1, #1
	orr r0, r1
	strb r0, [r3]
	add r2, #0xb2
	mov r0, #0
	strh r0, [r2]
	bl sub_0801394C
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0801C31C
	bl removeCameraPlayerControl
_0801C31C:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_0801C324: .4byte 0x03004750

thumb_func_global sub_0801C328
sub_0801C328: @ 0x0801C328
	ldr r1, _0801C340
	add r3, r1, #0
	add r3, #0x22
	ldrb r2, [r3]
	mov r0, #0x7f
	and r0, r2
	strb r0, [r3]
	add r1, #0x20
	mov r0, #0
	strb r0, [r1]
	bx lr
	.align 2, 0
_0801C340: .4byte 0x03004750

thumb_func_global sub_0801C344
sub_0801C344: @ 0x0801C344
	ldr r2, _0801C358
	add r0, r2, #0
	add r0, #0x9c
	ldrh r1, [r0]
	mov r0, #0x20
	and r0, r1
	cmp r0, #0
	beq _0801C35C
	mov r0, #0
	b _0801C366
	.align 2, 0
_0801C358: .4byte 0x03004750
_0801C35C:
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	beq _0801C368
	mov r0, #1
_0801C366:
	strh r0, [r2, #0x10]
_0801C368:
	bx lr
	.align 2, 0

thumb_func_global sub_0801C36C
sub_0801C36C: @ 0x0801C36C
	push {r4, lr}
	add r3, r0, #0
	ldr r1, _0801C3B0
	add r0, r1, #0
	add r0, #0x9c
	ldrh r2, [r0]
	mov r0, #0x20
	and r0, r2
	cmp r0, #0
	beq _0801C3B8
	cmp r3, #0
	bne _0801C390
	add r0, r1, #0
	add r0, #0xb0
	ldrh r0, [r0]
	cmp r0, #0
	bne _0801C390
	strh r3, [r1, #0x10]
_0801C390:
	ldr r1, _0801C3B0
	ldr r0, [r1]
	add r4, r1, #0
	add r4, #0xac
	ldr r1, [r4]
	ldr r2, _0801C3B4
	add r1, r1, r2
	bl setObjVelocity_X
	ldr r0, [r4]
	cmp r0, #0x10
	ble _0801C406
	sub r0, #0x10
	str r0, [r4]
	b _0801C406
	.align 2, 0
_0801C3B0: .4byte 0x03004750
_0801C3B4: .4byte 0xFFFFFE40
_0801C3B8:
	mov r0, #0x10
	and r0, r2
	cmp r0, #0
	beq _0801C3FC
	cmp r3, #0
	bne _0801C3D2
	add r0, r1, #0
	add r0, #0xb0
	ldrh r0, [r0]
	cmp r0, #0
	bne _0801C3D2
	mov r0, #1
	strh r0, [r1, #0x10]
_0801C3D2:
	ldr r1, _0801C3F8
	ldr r0, [r1]
	add r4, r1, #0
	add r4, #0xac
	ldr r1, [r4]
	mov r2, #0xe0
	lsl r2, r2, #1
	add r1, r1, r2
	bl setObjVelocity_X
	ldr r1, [r4]
	mov r0, #0x10
	neg r0, r0
	cmp r1, r0
	bge _0801C406
	add r0, r1, #0
	add r0, #0x10
	str r0, [r4]
	b _0801C406
	.align 2, 0
_0801C3F8: .4byte 0x03004750
_0801C3FC:
	ldr r0, [r1]
	add r1, #0xac
	ldr r1, [r1]
	bl setObjVelocity_X
_0801C406:
	ldr r4, _0801C448
	add r1, r4, #0
	add r1, #0xac
	ldr r0, [r1]
	cmp r0, #0
	ble _0801C416
	sub r0, #1
	str r0, [r1]
_0801C416:
	ldr r0, [r1]
	cmp r0, #0
	bge _0801C420
	add r0, #1
	str r0, [r1]
_0801C420:
	ldr r2, [r4]
	ldr r0, [r2, #0x18]
	ldr r1, _0801C44C
	cmp r0, r1
	bge _0801C430
	add r0, r2, #0
	bl setObjVelocity_X
_0801C430:
	ldr r2, [r4]
	ldr r0, [r2, #0x18]
	mov r1, #0xe0
	lsl r1, r1, #1
	cmp r0, r1
	ble _0801C442
	add r0, r2, #0
	bl setObjVelocity_X
_0801C442:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801C448: .4byte 0x03004750
_0801C44C: .4byte 0xFFFFFE40

thumb_func_global sub_0801C450
sub_0801C450: @ 0x0801C450
	push {r4, r5, r6, lr}
	add r5, r0, #0
	ldr r6, _0801C4C0
	add r2, r6, #0
	add r2, #0x22
	ldrb r1, [r2]
	mov r0, #2
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	mov r4, #0
	strh r4, [r6, #0x1e]
	mov r0, #0
	bl set_player_18
	ldr r0, [r6]
	mov r1, #0xa5
	lsl r1, r1, #3
	bl setObjVelocity_Y
	ldr r0, [r6]
	mov r1, #0
	bl setObjAccelerationY
	ldr r0, [r6]
	add r1, r5, #0
	bl setObjVelocity_X
	mov r0, #5
	bl sub_80190F4
	mov r0, #0xe
	bl setPlayerSprite
	add r1, r6, #0
	add r1, #0xb2
	mov r0, #1
	strh r0, [r1]
	add r2, r6, #0
	add r2, #0x4a
	ldrb r1, [r2]
	sub r0, #0x22
	and r0, r1
	strb r0, [r2]
	add r0, r6, #0
	add r0, #0xac
	str r4, [r0]
	sub r0, #0xc
	strh r4, [r0]
	mov r0, #0xfc
	bl playSong
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_0801C4C0: .4byte 0x03004750

thumb_func_global sub_0801C4C4
sub_0801C4C4: @ 0x0801C4C4
	push {r4, r5, lr}
	ldr r4, _0801C514
	add r5, r4, #0
	add r5, #0x22
	ldrb r1, [r5]
	mov r0, #2
	neg r0, r0
	and r0, r1
	strb r0, [r5]
	mov r0, #0
	bl sub_80190F4
	add r2, r4, #0
	add r2, #0x4a
	ldrb r0, [r2]
	mov r1, #0x10
	orr r0, r1
	strb r0, [r2]
	mov r0, #0x1c
	bl sub_080191D8
	add r2, r4, #0
	add r2, #0x45
	mov r1, #0
	mov r0, #0x18
	strb r0, [r2]
	add r0, r4, #0
	add r0, #0x48
	strb r1, [r0]
	mov r0, #0
	bl setPlayerSprite
	ldrb r0, [r5]
	mov r1, #0x20
	orr r0, r1
	strb r0, [r5]
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_0801C514: .4byte 0x03004750

thumb_func_global sub_0801C518
sub_0801C518: @ 0x0801C518
	push {r4, lr}
	ldr r4, _0801C558
	add r2, r4, #0
	add r2, #0x22
	ldrb r1, [r2]
	mov r0, #2
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	mov r0, #0
	bl sub_80190F4
	add r2, r4, #0
	add r2, #0x4a
	ldrb r0, [r2]
	mov r1, #8
	orr r0, r1
	strb r0, [r2]
	mov r0, #0x16
	bl sub_080191D8
	add r1, r4, #0
	add r1, #0x45
	mov r0, #0xf
	strb r0, [r1]
	mov r0, #0
	bl setPlayerSprite
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801C558: .4byte 0x03004750

thumb_func_global sub_0801C55C
sub_0801C55C: @ 0x0801C55C
	push {r4, lr}
	mov r0, #1
	bl sub_80190F4
	ldr r4, _0801C58C
	add r2, r4, #0
	add r2, #0x4a
	ldrb r0, [r2]
	mov r1, #0x10
	orr r0, r1
	strb r0, [r2]
	mov r0, #0x22
	bl sub_080191D8
	add r4, #0x45
	mov r0, #0x19
	strb r0, [r4]
	mov r0, #2
	bl setPlayerSprite
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801C58C: .4byte 0x03004750

thumb_func_global sub_0801C590
sub_0801C590: @ 0x0801C590
	push {r4, lr}
	ldr r4, _0801C5C0
	add r2, r4, #0
	add r2, #0x4a
	ldrb r0, [r2]
	mov r1, #8
	orr r0, r1
	strb r0, [r2]
	mov r0, #1
	bl sub_80190F4
	mov r0, #0x18
	bl sub_080191D8
	add r4, #0x45
	mov r0, #0x12
	strb r0, [r4]
	mov r0, #2
	bl setPlayerSprite
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801C5C0: .4byte 0x03004750

thumb_func_global sub_0801C5C4
sub_0801C5C4: @ 0x0801C5C4
	push {r4, r5, lr}
	ldr r4, _0801C604
	add r1, r4, #0
	add r1, #0xb2
	mov r0, #0
	strh r0, [r1]
	add r5, r4, #0
	add r5, #0x4a
	ldrb r0, [r5]
	mov r1, #0x10
	orr r0, r1
	strb r0, [r5]
	mov r0, #0x28
	bl sub_080191D8
	add r1, r4, #0
	add r1, #0x45
	mov r0, #0x14
	strb r0, [r1]
	mov r0, #0x1f
	strh r0, [r4, #0x1e]
	ldrb r0, [r5]
	mov r2, #0x20
	orr r0, r2
	strb r0, [r5]
	sub r1, #0x23
	ldrb r0, [r1]
	orr r0, r2
	strb r0, [r1]
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_0801C604: .4byte 0x03004750

thumb_func_global sub_0801C608
sub_0801C608: @ 0x0801C608
	push {r4, lr}
	ldr r4, _0801C634
	add r1, r4, #0
	add r1, #0xb2
	mov r0, #0
	strh r0, [r1]
	add r2, r4, #0
	add r2, #0x4a
	ldrb r0, [r2]
	mov r1, #8
	orr r0, r1
	strb r0, [r2]
	mov r0, #0x1a
	bl sub_080191D8
	add r1, r4, #0
	add r1, #0x45
	mov r0, #0xf
	strb r0, [r1]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801C634: .4byte 0x03004750

thumb_func_global sub_0801C638
sub_0801C638: @ 0x0801C638
	push {r4, r5, lr}
	sub sp, #0xc
	ldr r5, _0801C6A4
	ldr r1, [r5]
	ldr r0, [r1, #4]
	ldr r2, _0801C6A8
	add r0, r0, r2
	str r0, [sp]
	ldr r0, [r1, #8]
	mov r1, #0x80
	lsl r1, r1, #5
	add r0, r0, r1
	str r0, [sp, #4]
	mov r1, sp
	mov r0, #0xd
	strh r0, [r1, #8]
	mov r0, #0x10
	strh r0, [r1, #0xa]
	ldr r0, [sp]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	ldr r1, [sp, #4]
	lsl r1, r1, #8
	lsr r1, r1, #0x10
	mov r2, sp
	ldrh r2, [r2, #8]
	bl sub_08012A08
	add r4, r0, #0
	ldr r1, [sp, #4]
	mov r2, #0xf0
	lsl r2, r2, #4
	add r1, r1, r2
	str r1, [sp, #4]
	ldr r0, [sp]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	lsl r1, r1, #8
	lsr r1, r1, #0x10
	mov r2, sp
	ldrh r2, [r2, #8]
	bl sub_08012A08
	orr r4, r0
	ldr r0, [r5]
	bl sub_0801F898
	orr r4, r0
	add r0, r4, #0
	add sp, #0xc
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
_0801C6A4: .4byte 0x03004750
_0801C6A8: .4byte 0xFFFFF900

thumb_func_global sub_0801C6AC
sub_0801C6AC: @ 0x0801C6AC
	push {r4, lr}
	ldr r4, _0801C6E0
	ldr r0, [r4]
	bl clearObjForces
	add r0, r4, #0
	add r0, #0x4a
	ldrb r1, [r0]
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	beq _0801C6E4
	add r0, r4, #0
	add r0, #0x45
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	cmp r0, #5
	ble _0801C6E4
	mov r0, #0x1a
	bl sub_80190F4
	mov r0, #0xfd
	bl playSong
	b _0801C72C
	.align 2, 0
_0801C6E0: .4byte 0x03004750
_0801C6E4:
	ldr r4, _0801C708
	ldrh r0, [r4, #0x1e]
	cmp r0, #0x1d
	bls _0801C70C
	bl sub_0801A93C
	mov r0, #5
	mov r1, #0xc
	bl sub_0801C2D4
	add r1, r4, #0
	add r1, #0x21
	mov r0, #3
	strb r0, [r1]
	mov r0, #0xfe
	bl playSong
	b _0801C72C
	.align 2, 0
_0801C708: .4byte 0x03004750
_0801C70C:
	bl sub_0801A93C
	mov r0, #0
	bl sub_80190F4
	mov r0, #0x11
	mov r1, #0
	bl sub_80191B0
	add r1, r4, #0
	add r1, #0xb2
	mov r0, #0
	strh r0, [r1]
	mov r0, #0xfd
	bl playSong
_0801C72C:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global _3005773_clearBits3_5
_3005773_clearBits3_5: @ 0x0801C734
	ldr r2, _0801C74C
	add r2, #0x23
	ldrb r1, [r2]
	mov r0, #0x21
	neg r0, r0
	and r0, r1
	mov r1, #9
	neg r1, r1
	and r0, r1
	strb r0, [r2]
	bx lr
	.align 2, 0
_0801C74C: .4byte 0x03004750

thumb_func_global sub_0801C750
sub_0801C750: @ 0x0801C750
	push {r4, r5, lr}
	ldr r5, _0801C78C
	add r4, r5, #0
	add r4, #0x23
	ldrb r1, [r4]
	mov r0, #0x14
	and r0, r1
	cmp r0, #0
	beq _0801C786
	bl _3005773_clearBits3_5
	ldrb r1, [r4]
	mov r0, #0x11
	neg r0, r0
	and r0, r1
	mov r1, #0x21
	neg r1, r1
	and r0, r1
	add r2, r5, #0
	add r2, #0xa8
	mov r1, #0
	strh r1, [r2]
	sub r1, #5
	and r0, r1
	sub r1, #4
	and r0, r1
	strb r0, [r4]
_0801C786:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_0801C78C: .4byte 0x03004750

thumb_func_global sub_0801C790
sub_0801C790: @ 0x0801C790
	push {r4, lr}
	bl getPlayerHealth
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0801C7F2
	ldr r3, _0801C7F8
	mov r0, #0x23
	add r0, r0, r3
	mov ip, r0
	ldrb r0, [r0]
	lsl r0, r0, #0x1b
	cmp r0, #0
	bge _0801C7F2
	ldr r0, [r3]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r4, #0x11
	neg r4, r4
	add r0, r4, #0
	and r0, r2
	strb r0, [r1]
	add r2, r3, #0
	add r2, #0xa8
	ldrh r0, [r2]
	sub r0, #1
	strh r0, [r2]
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0801C7D8
	mov r1, ip
	ldrb r0, [r1]
	lsl r0, r0, #0x1a
	cmp r0, #0
	blt _0801C7F2
_0801C7D8:
	mov r0, ip
	ldrb r1, [r0]
	add r0, r4, #0
	and r0, r1
	mov r1, #0x21
	neg r1, r1
	and r0, r1
	mov r1, ip
	strb r0, [r1]
	mov r0, #0
	strh r0, [r2]
	bl setPlayerPaletteMaybe
_0801C7F2:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801C7F8: .4byte 0x03004750

thumb_func_global sub_0801C7FC
sub_0801C7FC: @ 0x0801C7FC
	push {r4, r5, lr}
	ldr r0, _0801C81C
	ldrh r4, [r0, #6]
	add r5, r0, #0
	cmp r4, #0xb3
	beq _0801C844
	cmp r4, #0xb3
	bgt _0801C82A
	cmp r4, #0x14
	beq _0801C844
	cmp r4, #0x14
	bgt _0801C820
	cmp r4, #4
	beq _0801C844
	b _0801C876
	.align 2, 0
_0801C81C: .4byte 0x03004750
_0801C820:
	cmp r4, #0xaf
	beq _0801C844
	cmp r4, #0xb1
	beq _0801C844
	b _0801C876
_0801C82A:
	cmp r4, #0xb9
	beq _0801C844
	cmp r4, #0xb9
	bgt _0801C83C
	cmp r4, #0xb5
	beq _0801C844
	cmp r4, #0xb7
	beq _0801C844
	b _0801C876
_0801C83C:
	cmp r4, #0xca
	beq _0801C844
	cmp r4, #0xcc
	bne _0801C876
_0801C844:
	ldr r0, [r5]
	ldr r3, [r0]
	ldr r2, _0801C87C
	sub r4, #1
	lsl r1, r4, #3
	add r0, r1, r2
	ldr r0, [r0]
	str r0, [r3, #4]
	add r2, #4
	add r1, r1, r2
	ldr r0, [r1]
	str r0, [r3, #8]
	add r2, r3, #0
	add r2, #0x23
	ldrb r0, [r2]
	mov r1, #0x80
	orr r0, r1
	strb r0, [r2]
	add r3, #0x24
	ldrb r0, [r3]
	mov r1, #1
	orr r0, r1
	strb r0, [r3]
	strh r4, [r5, #0xa]
	strh r4, [r5, #6]
_0801C876:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_0801C87C: .4byte playerAnimations

thumb_func_global sub_0801C880
sub_0801C880: @ 0x0801C880
	push {r4, lr}
	bl getPlayerHealth
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _0801C8BC
	ldr r0, _0801C8B8
	add r4, r0, #0
	add r4, #0x23
	ldrb r0, [r4]
	lsl r0, r0, #0x1f
	cmp r0, #0
	beq _0801C8FC
	bl sub_0801C7FC
	ldrb r0, [r4]
	mov r1, #5
	neg r1, r1
	and r1, r0
	mov r0, #9
	neg r0, r0
	and r1, r0
	strb r1, [r4]
	mov r0, #0
	bl setPlayerPaletteMaybe
	b _0801C8FC
	.align 2, 0
_0801C8B8: .4byte 0x03004750
_0801C8BC:
	ldr r1, _0801C904
	add r4, r1, #0
	add r4, #0x23
	ldrb r0, [r4]
	lsl r0, r0, #0x1d
	cmp r0, #0
	bge _0801C8FC
	ldr r0, [r1]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	ldrb r0, [r4]
	lsl r0, r0, #0x1c
	cmp r0, #0
	blt _0801C8FC
	bl sub_0801C7FC
	ldrb r0, [r4]
	mov r1, #5
	neg r1, r1
	and r1, r0
	mov r0, #9
	neg r0, r0
	and r1, r0
	strb r1, [r4]
	mov r0, #0
	bl setPlayerPaletteMaybe
_0801C8FC:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801C904: .4byte 0x03004750

thumb_func_global sub_0801C908
sub_0801C908: @ 0x0801C908
	push {lr}
	ldr r0, _0801C930
	ldr r1, [r0]
	ldr r0, [r1, #4]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	ldr r1, [r1, #8]
	lsl r1, r1, #8
	lsr r1, r1, #0x10
	bl getCollision
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	bl sub_080328AC
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	pop {r1}
	bx r1
	.align 2, 0
_0801C930: .4byte 0x03004750




thumb_func_global sub_801C934
sub_801C934: @ 0x0801C934
	push {r4, r5, r6, r7, lr}
	ldr r0, _0801C98C
	ldrh r5, [r0]
	ldr r1, _0801C990
	add r0, r1, #0
	add r0, #0x90
	ldr r4, [r0]
	mov r2, #0
	mov ip, r1
	mov r7, ip
	add r7, #0x22
	ldr r6, _0801C994
_0801C94C:
	add r1, r2, r5
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #0x18
	lsr r1, r0, #0x18
	lsl r0, r2, #2
	add r0, r0, r2
	add r0, r0, r5
	lsl r0, r0, #0x18
	lsr r3, r0, #0x18
	ldrb r0, [r7]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1f
	cmp r0, #0
	beq _0801C998
	add r0, r1, #0
	add r0, #0x40
	lsl r0, r0, #1
	add r0, r0, r6
	mov r1, #0
	ldrsh r0, [r0, r1]
	asr r0, r0, #7
	add r0, #8
	strh r0, [r4]
	add r4, #2
	lsl r0, r3, #1
	add r0, r0, r6
	mov r1, #0
	ldrsh r0, [r0, r1]
	asr r0, r0, #6
	add r0, #0x30
	b _0801C99C
	.align 2, 0
_0801C98C: .4byte 0x03002080
_0801C990: .4byte 0x03004750
_0801C994: .4byte dword_80382C8
_0801C998:
	strh r0, [r4]
	add r4, #2
_0801C99C:
	strh r0, [r4]
	add r4, #2
	lsl r0, r2, #3
	sub r0, r0, r2
	add r0, r0, r5
	lsl r0, r0, #0x18
	lsr r0, r0, #0x17
	add r0, #0x80
	add r0, r0, r6
	mov r1, #0
	ldrsh r0, [r0, r1]
	asr r0, r0, #7
	strh r0, [r4]
	add r4, #2
	add r0, r2, #1
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	cmp r2, #0x9f
	bls _0801C94C
	ldr r3, _0801C9E4
	mov r0, #0
	strh r0, [r3]
	mov r2, ip
	add r2, #0x90
	ldr r4, [r2]
	mov r1, ip
	add r1, #0x8c
	ldr r0, [r1]
	str r0, [r2]
	str r4, [r1]
	mov r0, #1
	strh r0, [r3]
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_0801C9E4: .4byte 0x04000208




thumb_func_global sub_0801C9E8
sub_0801C9E8: @ 0x0801C9E8
	push {r4, r5, lr}
	sub sp, #4
	ldr r0, _0801CA84
	ldr r0, [r0]
	ldr r1, _0801CA88
	and r0, r1
	mov r1, #0x81
	lsl r1, r1, #0xa
	cmp r0, r1
	bne _0801CA7A
	ldr r5, _0801CA8C
	ldr r2, _0801CA90
	ldrh r1, [r2]
	add r0, r5, #0
	add r0, #0x94
	mov r4, #0
	strh r1, [r0]
	ldr r1, _0801CA94
	ldrh r0, [r1]
	add r3, r5, #0
	add r3, #0x96
	strh r0, [r3]
	ldr r3, _0801CA98
	add r0, r3, #0
	strh r0, [r2]
	ldr r2, _0801CA9C
	add r0, r2, #0
	strh r0, [r1]
	ldr r1, _0801CAA0
	ldrh r2, [r1, #0xa]
	ldr r0, _0801CAA4
	and r0, r2
	strh r0, [r1, #0xa]
	ldrh r2, [r1, #0xa]
	ldr r0, _0801CAA8
	and r0, r2
	strh r0, [r1, #0xa]
	ldrh r0, [r1, #0xa]
	str r4, [sp]
	ldr r0, _0801CAAC
	mov r3, sp
	str r3, [r0]
	ldr r3, _0801CAB0
	str r3, [r0, #4]
	ldr r1, _0801CAB4
	str r1, [r0, #8]
	ldr r2, [r0, #8]
	str r4, [sp]
	mov r2, sp
	str r2, [r0]
	ldr r2, _0801CAB8
	str r2, [r0, #4]
	str r1, [r0, #8]
	ldr r0, [r0, #8]
	add r0, r5, #0
	add r0, #0x8c
	str r3, [r0]
	add r0, #4
	str r2, [r0]
	add r2, r5, #0
	add r2, #0x24
	ldrb r0, [r2]
	mov r1, #4
	orr r0, r1
	strb r0, [r2]
	ldr r1, _0801CABC
	mov r0, #1
	strb r0, [r1]
	ldr r0, _0801CAC0
	mov r1, #3
	bl createTask
	str r0, [r5, #0x28]
_0801CA7A:
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_0801CA84: .4byte 0x03003D40
_0801CA88: .4byte 0x00FFFF00
_0801CA8C: .4byte 0x03004750
_0801CA90: .4byte 0x03003D74
_0801CA94: .4byte 0x03003D94
_0801CA98: .4byte 0x00003F43
_0801CA9C: .4byte 0x0000080E
_0801CAA0: .4byte 0x040000B0
_0801CAA4: .4byte 0x0000C5FF
_0801CAA8: .4byte 0x00007FFF
_0801CAAC: .4byte 0x040000D4
_0801CAB0: .4byte 0x0200A210
_0801CAB4: .4byte 0x850000F0
_0801CAB8: .4byte 0x02020160
_0801CABC: .4byte 0x03001D70
_0801CAC0: .4byte (sub_801C934+1)

thumb_func_global sub_801CAC4
sub_801CAC4: @ 0x0801CAC4
	push {lr}
	ldr r0, _0801CB20
	ldr r0, [r0]
	ldr r1, _0801CB24
	and r0, r1
	mov r1, #0x81
	lsl r1, r1, #0xa
	cmp r0, r1
	bne _0801CB1A
	ldr r1, _0801CB28
	mov r0, #0
	strb r0, [r1]
	ldr r3, _0801CB2C
	add r2, r3, #0
	add r2, #0x24
	ldrb r1, [r2]
	mov r0, #5
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	ldr r1, _0801CB30
	ldrh r2, [r1, #0xa]
	ldr r0, _0801CB34
	and r0, r2
	strh r0, [r1, #0xa]
	ldrh r2, [r1, #0xa]
	ldr r0, _0801CB38
	and r0, r2
	strh r0, [r1, #0xa]
	ldrh r0, [r1, #0xa]
	ldr r1, _0801CB3C
	add r0, r3, #0
	add r0, #0x94
	ldrh r0, [r0]
	strh r0, [r1]
	ldr r1, _0801CB40
	add r0, r3, #0
	add r0, #0x96
	ldrh r0, [r0]
	strh r0, [r1]
	ldr r0, [r3, #0x28]
	bl endTask
_0801CB1A:
	pop {r0}
	bx r0
	.align 2, 0
_0801CB20: .4byte 0x03003D40
_0801CB24: .4byte 0x00FFFF00
_0801CB28: .4byte 0x03001D70
_0801CB2C: .4byte 0x03004750
_0801CB30: .4byte 0x040000B0
_0801CB34: .4byte 0x0000C5FF
_0801CB38: .4byte 0x00007FFF
_0801CB3C: .4byte 0x03003D74
_0801CB40: .4byte 0x03003D94

thumb_func_global sub_0801CB44
sub_0801CB44: @ 0x0801CB44
	ldr r1, _0801CB58
	add r2, r1, #0
	add r2, #0x21
	ldrb r1, [r2]
	cmp r1, #0
	beq _0801CB54
	sub r1, #1
	strb r1, [r2]
_0801CB54:
	bx lr
	.align 2, 0
_0801CB58: .4byte 0x03004750
thumb_func_global sub_801CB5C



sub_801CB5C: @ 0x0801CB5C
	ldr r0, _0801CB64
	add r0, #0x21
	ldrb r0, [r0]
	bx lr
	.align 2, 0
_0801CB64: .4byte 0x03004750



thumb_func_global player_launchSiteTimerDeath
player_launchSiteTimerDeath: @ 0x0801CB68
	push {lr}
	ldr r0, _0801CB98
	add r0, #0x22
	ldrb r1, [r0]
	mov r0, #6
	and r0, r1
	cmp r0, #0
	bne _0801CB94
	bl sub_08032F5C
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0801CB94
	bl getPlayerHealth
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0801CB94
	mov r0, #0x50
	neg r0, r0
	bl dealHostageDamage
_0801CB94:
	pop {r0}
	bx r0
	.align 2, 0
_0801CB98: .4byte 0x03004750

thumb_func_global sub_801CB9C
sub_801CB9C: @ 0x0801CB9C
	push {lr}
	ldr r1, _0801CBD8
	mov r0, #0
	strb r0, [r1]
	ldr r3, _0801CBDC
	add r2, r3, #0
	add r2, #0x24
	ldrb r1, [r2]
	mov r0, #5
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	ldr r1, _0801CBE0
	ldrh r2, [r1, #0xa]
	ldr r0, _0801CBE4
	and r0, r2
	strh r0, [r1, #0xa]
	ldrh r2, [r1, #0xa]
	ldr r0, _0801CBE8
	and r0, r2
	strh r0, [r1, #0xa]
	ldrh r0, [r1, #0xa]
	ldr r0, [r3, #0x28]
	cmp r0, #0
	beq _0801CBD2
	bl endTask
_0801CBD2:
	pop {r0}
	bx r0
	.align 2, 0
_0801CBD8: .4byte 0x03001D70
_0801CBDC: .4byte 0x03004750
_0801CBE0: .4byte 0x040000B0
_0801CBE4: .4byte 0x0000C5FF
_0801CBE8: .4byte 0x00007FFF

thumb_func_global sub_0801CBEC
sub_0801CBEC: @ 0x0801CBEC
	push {lr}
	ldr r0, _0801CC18
	ldr r1, [r0]
	ldr r0, _0801CC1C
	and r1, r0
	mov r0, #0x81
	lsl r0, r0, #0xa
	cmp r1, r0
	beq _0801CC0C
	ldr r0, _0801CC20
	cmp r1, r0
	bne _0801CC14
	ldr r0, _0801CC24
	ldrh r0, [r0, #6]
	cmp r0, #0xc8
	beq _0801CC14
_0801CC0C:
	mov r0, #0xbc
	lsl r0, r0, #1
	bl playSong
_0801CC14:
	pop {r0}
	bx r0
	.align 2, 0
_0801CC18: .4byte 0x03003D40
_0801CC1C: .4byte 0x00FFFF00
_0801CC20: .4byte 0x00030200
_0801CC24: .4byte 0x03004750

thumb_func_global sub_0801CC28
sub_0801CC28: @ 0x0801CC28
	ldr r0, _0801CC3C
	ldrh r0, [r0, #4]
	cmp r0, #0x1a
	bhi _0801CCB4
	lsl r0, r0, #2
	ldr r1, _0801CC40
	add r0, r0, r1
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
_0801CC3C: .4byte 0x03004750
_0801CC40: .4byte _0801CC44
_0801CC44: @ jump table
	.4byte _0801CCB0 @ case 0
	.4byte _0801CCB0 @ case 1
	.4byte _0801CCB0 @ case 2
	.4byte _0801CCB0 @ case 3
	.4byte _0801CCB0 @ case 4
	.4byte _0801CCB4 @ case 5
	.4byte _0801CCB4 @ case 6
	.4byte _0801CCB4 @ case 7
	.4byte _0801CCB0 @ case 8
	.4byte _0801CCB0 @ case 9
	.4byte _0801CCB0 @ case 10
	.4byte _0801CCB0 @ case 11
	.4byte _0801CCB4 @ case 12
	.4byte _0801CCB4 @ case 13
	.4byte _0801CCB4 @ case 14
	.4byte _0801CCB4 @ case 15
	.4byte _0801CCB4 @ case 16
	.4byte _0801CCB4 @ case 17
	.4byte _0801CCB4 @ case 18
	.4byte _0801CCB4 @ case 19
	.4byte _0801CCB4 @ case 20
	.4byte _0801CCB4 @ case 21
	.4byte _0801CCB4 @ case 22
	.4byte _0801CCB4 @ case 23
	.4byte _0801CCB4 @ case 24
	.4byte _0801CCB4 @ case 25
	.4byte _0801CCB0 @ case 26
_0801CCB0:
	mov r0, #1
	b _0801CCB6
_0801CCB4:
	mov r0, #0
_0801CCB6:
	bx lr
	
	
	
	
	thumb_func_global sub_801CCB8
sub_801CCB8: @ 0x0801CCB8
	push {r4, r5, r6, lr}
	ldr r5, _0801CD2C
	ldr r0, [r5]
	ldr r1, _0801CD30
	ldr r4, _0801CD34
	ldrb r2, [r4, #1]
	lsl r2, r2, #2
	add r2, r2, r1
	mov r3, #0
	ldrsh r1, [r2, r3]
	mov r3, #2
	ldrsh r2, [r2, r3]
	bl cameraRelativeXY
	ldr r0, [r4]
	ldr r1, _0801CD38
	and r0, r1
	mov r1, #0x81
	lsl r1, r1, #0xa
	cmp r0, r1
	bne _0801CD96
	ldr r0, _0801CD3C
	ldr r1, [r0, #4]
	add r0, #0x74
	cmp r1, r0
	beq _0801CD96
	add r6, r5, #0
_0801CCEE:
	add r0, r1, #0
	add r0, #0x5a
	ldrh r0, [r0]
	cmp r0, #0x3b
	bne _0801CD8E
	ldr r0, [r1, #0x3c]
	ldr r5, [r0, #4]
	ldr r4, [r0, #8]
	ldr r0, _0801CD40
	cmp r5, r0
	bgt _0801CD44
	mov r1, #0x80
	lsl r1, r1, #4
	add r0, r5, r1
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	asr r4, r4, #8
	sub r1, r4, #1
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl getCollision
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	bl isCollisionSolid
	lsl r0, r0, #0x10
	add r2, r4, #0
	cmp r0, #0
	bne _0801CD74
	b _0801CD68
	.align 2, 0
_0801CD2C: .4byte 0x03004750
_0801CD30: .4byte word_813CEDC
_0801CD34: .4byte 0x03003D40
_0801CD38: .4byte 0x00FFFF00
_0801CD3C: .4byte 0x03004EE0
_0801CD40: .4byte 0x00007FFF
_0801CD44:
	ldr r3, _0801CD70
	add r0, r5, r3
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	asr r4, r4, #8
	sub r1, r4, #1
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl getCollision
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	bl isCollisionSolid
	lsl r0, r0, #0x10
	add r2, r4, #0
	cmp r0, #0
	beq _0801CD74
_0801CD68:
	ldr r1, _0801CD70
	mov r4, #1
	b _0801CD7A
	.align 2, 0
_0801CD70: .4byte 0xFFFFF800
_0801CD74:
	mov r1, #0x80
	lsl r1, r1, #4
	mov r4, #0
_0801CD7A:
	ldr r0, [r6]
	add r1, r5, r1
	lsl r1, r1, #8
	asr r1, r1, #0x10
	lsl r2, r2, #0x10
	asr r2, r2, #0x10
	bl cameraRelativeXY
	strh r4, [r6, #0x10]
	b _0801CD96
_0801CD8E:
	ldr r1, [r1, #4]
	ldr r0, _0801CD9C
	cmp r1, r0
	bne _0801CCEE
_0801CD96:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_0801CD9C: .4byte 0x03004F54
	
	
	

thumb_func_global activateInvulnSpecial
activateInvulnSpecial: @ 0x0801CDA0
	push {r4, lr}
	ldr r4, _0801CDDC
	add r0, r4, #0
	add r0, #0xa2
	mov r1, #0
	strb r1, [r0]
	add r0, #2
	strh r1, [r0]
	bl EnableInvulnSpecial
	mov r0, #4
	bl setPlayerPaletteMaybe
	ldr r0, _0801CDE0
	bl playSong
	ldr r3, _0801CDE4
	ldrh r2, [r3, #0xc]
	ldr r1, _0801CDE8
	add r0, r1, #0
	and r0, r2
	strh r0, [r3, #0xc]
	add r2, r4, #0
	add r2, #0x9a
	ldrh r0, [r2]
	and r1, r0
	strh r1, [r2]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801CDDC: .4byte 0x03004750
_0801CDE0: .4byte 0x00000101
_0801CDE4: .4byte 0x03002080
_0801CDE8: .4byte 0x0000FFFC

thumb_func_global invulnSpecial_handleCounter_pal
invulnSpecial_handleCounter_pal: @ 0x0801CDEC
	push {r4, r5, lr}
	bl isUsingInvulnSpecial
	cmp r0, #0
	beq _0801CE80
	bl getPlayerSpecial
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0801CE10
	ldr r1, _0801CE2C
	add r0, r1, #0
	add r0, #0x22
	ldrb r0, [r0]
	mov r2, #6
	and r2, r0
	cmp r2, #0
	beq _0801CE30
_0801CE10:
	ldr r4, _0801CE2C
	add r0, r4, #0
	add r0, #0xa2
	mov r5, #0
	strb r5, [r0]
	bl disableInvulnSpecial
	add r4, #0xa4
	strh r5, [r4]
	mov r0, #0
	bl setPlayerPaletteMaybe
	b _0801CE80
	.align 2, 0
_0801CE2C: .4byte 0x03004750
_0801CE30:
	add r3, r1, #0
	add r3, #0xa2
	ldrb r0, [r3]
	cmp r0, #0xc
	bls _0801CE6E
	add r1, #0xa4
	ldrh r0, [r1]
	add r0, #1
	strh r0, [r1]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #5
	bls _0801CE80
	strh r2, [r1]
	bl getPlayerSpecial
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0x50
	bne _0801CE64
	bl getPlayerSpecialBar
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0x4f
	bls _0801CE80
_0801CE64:
	mov r0, #1
	neg r0, r0
	bl ModifySpecialCharge
	b _0801CE80
_0801CE6E:
	add r0, #1
	strb r0, [r3]
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0xd
	bne _0801CE80
	mov r0, #5
	bl setPlayerPaletteMaybe
_0801CE80:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global canActivateInvulnSpecial
canActivateInvulnSpecial: @ 0x0801CE88
	push {r4, r5, lr}
	mov r5, #0
	ldr r4, _0801CF14
	add r0, r4, #0
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	cmp r0, #0
	blt _0801CF0C
	add r0, r4, #0
	add r0, #0xa8
	ldrh r0, [r0]
	cmp r0, #0
	bne _0801CF0C
	add r0, r4, #0
	add r0, #0x23
	ldrb r0, [r0]
	lsl r0, r0, #0x1e
	cmp r0, #0
	blt _0801CF0C
	bl getPlayerSpecialBar
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0x4f
	bhi _0801CF0C
	bl getPlayerHealth
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0801CF0C
	add r0, r4, #0
	add r0, #0x9a
	ldrh r2, [r0]
	mov r3, #1
	add r0, r3, #0
	and r0, r2
	cmp r0, #0
	bne _0801CEE2
	ldr r0, _0801CF18
	ldrh r1, [r0, #0xc]
	add r0, r3, #0
	and r0, r1
	cmp r0, #0
	beq _0801CF0C
_0801CEE2:
	mov r3, #2
	add r0, r3, #0
	and r0, r2
	cmp r0, #0
	bne _0801CEF8
	ldr r0, _0801CF18
	ldrh r1, [r0, #0xc]
	add r0, r3, #0
	and r0, r1
	cmp r0, #0
	beq _0801CF0C
_0801CEF8:
	bl isUsingInvulnSpecial
	cmp r0, #0
	bne _0801CF0C
	bl getPlayerSpecialBar
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0801CF0C
	mov r5, #1
_0801CF0C:
	add r0, r5, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
_0801CF14: .4byte 0x03004750
_0801CF18: .4byte 0x03002080

thumb_func_global getCurrentPowerUpPal
getCurrentPowerUpPal: @ 0x0801CF1C
	push {lr}
	bl getPowerUpLevel
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #1
	beq _0801CF34
	cmp r0, #1
	ble _0801CF44
	cmp r0, #2
	beq _0801CF3C
	b _0801CF44
_0801CF34:
	ldr r0, _0801CF38
	b _0801CF46
	.align 2, 0
_0801CF38: .4byte playerLvl2Pal
_0801CF3C:
	ldr r0, _0801CF40
	b _0801CF46
	.align 2, 0
_0801CF40: .4byte playerLvl3Pal
_0801CF44:
	ldr r0, _0801CF4C
_0801CF46:
	pop {r1}
	bx r1
	.align 2, 0
_0801CF4C: .4byte playerLvl1Pal

thumb_func_global setPlayerPaletteMaybe
setPlayerPaletteMaybe: @ 0x0801CF50
	push {r4, r5, lr}
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #7
	bls _0801CF5C
	b _0801D1EC
_0801CF5C:
	lsl r0, r0, #2
	ldr r1, _0801CF68
	add r0, r0, r1
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
_0801CF68: .4byte _0801CF6C
_0801CF6C: @ jump table
	.4byte _0801CF8C @ case 0
	.4byte _0801D020 @ case 1
	.4byte _0801D04C @ case 2
	.4byte _0801D09C @ case 3
	.4byte _0801D0C0 @ case 4
	.4byte _0801D0E4 @ case 5
	.4byte _0801D128 @ case 6
	.4byte _0801D180 @ case 7
_0801CF8C:
	ldr r5, _0801CFA4
	add r0, r5, #0
	add r0, #0x23
	ldrb r1, [r0]
	mov r0, #0x14
	and r0, r1
	cmp r0, #0
	beq _0801CFA8
	bl _3005773_clearBits3_5
	b _0801D1EC
	.align 2, 0
_0801CFA4: .4byte 0x03004750
_0801CFA8:
	add r4, r5, #0
	add r4, #0xa6
	ldrh r0, [r4]
	cmp r0, #0xff
	beq _0801CFBA
	bl freePalFadeTaskAtIndex
	mov r0, #0xff
	strh r0, [r4]
_0801CFBA:
	ldrh r0, [r5, #4]
	cmp r0, #0x19
	beq _0801CFC4
	cmp r0, #0xc
	bne _0801CFF0
_0801CFC4:
	ldr r4, _0801CFE4
	bl getCurrentPowerUpPal
	str r0, [r4]
	ldr r0, [r5]
	ldr r0, [r0]
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	lsl r0, r0, #5
	ldr r1, _0801CFE8
	add r0, r0, r1
	str r0, [r4, #4]
	ldr r0, _0801CFEC
	b _0801D00E
	.align 2, 0
_0801CFE4: .4byte 0x040000D4
_0801CFE8: .4byte 0x05000200
_0801CFEC: .4byte 0x80000010
_0801CFF0:
	ldr r4, _0801D014
	bl getCurrentPowerUpPal
	str r0, [r4]
	ldr r0, [r5]
	ldr r0, [r0]
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	lsl r0, r0, #5
	ldr r2, _0801D018
	add r0, r0, r2
	str r0, [r4, #4]
	ldr r0, _0801D01C
_0801D00E:
	str r0, [r4, #8]
	ldr r0, [r4, #8]
	b _0801D1EC
	.align 2, 0
_0801D014: .4byte 0x040000D4
_0801D018: .4byte 0x05000200
_0801D01C: .4byte 0x80000020
_0801D020:
	bl sub_0801C750
	ldr r5, _0801D040
	add r4, r5, #0
	add r4, #0xa6
	ldrh r0, [r4]
	cmp r0, #0xff
	beq _0801D038
	bl freePalFadeTaskAtIndex
	mov r0, #0xff
	strh r0, [r4]
_0801D038:
	ldr r1, _0801D044
	ldr r0, _0801D048
	b _0801D068
	.align 2, 0
_0801D040: .4byte 0x03004750
_0801D044: .4byte 0x040000D4
_0801D048: .4byte dword_813B1D0
_0801D04C:
	bl sub_0801C750
	ldr r5, _0801D088
	add r4, r5, #0
	add r4, #0xa6
	ldrh r0, [r4]
	cmp r0, #0xff
	beq _0801D064
	bl freePalFadeTaskAtIndex
	mov r0, #0xff
	strh r0, [r4]
_0801D064:
	ldr r1, _0801D08C
	ldr r0, _0801D090
_0801D068:
	str r0, [r1]
	ldr r0, [r5]
	ldr r0, [r0]
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	lsl r0, r0, #5
	ldr r2, _0801D094
	add r0, r0, r2
	str r0, [r1, #4]
	ldr r0, _0801D098
	str r0, [r1, #8]
	ldr r0, [r1, #8]
	b _0801D1EC
	.align 2, 0
_0801D088: .4byte 0x03004750
_0801D08C: .4byte 0x040000D4
_0801D090: .4byte dword_813B1F0
_0801D094: .4byte 0x05000200
_0801D098: .4byte 0x80000010
_0801D09C:
	bl sub_0801C750
	ldr r5, _0801D0B8
	add r4, r5, #0
	add r4, #0xa6
	ldrh r0, [r4]
	cmp r0, #0xff
	beq _0801D0B4
	bl freePalFadeTaskAtIndex
	mov r0, #0xff
	strh r0, [r4]
_0801D0B4:
	ldr r0, _0801D0BC
	b _0801D0FE
	.align 2, 0
_0801D0B8: .4byte 0x03004750
_0801D0BC: .4byte off_813B108
_0801D0C0:
	bl sub_0801C750
	ldr r5, _0801D0DC
	add r4, r5, #0
	add r4, #0xa6
	ldrh r0, [r4]
	cmp r0, #0xff
	beq _0801D0D8
	bl freePalFadeTaskAtIndex
	mov r0, #0xff
	strh r0, [r4]
_0801D0D8:
	ldr r0, _0801D0E0
	b _0801D0FE
	.align 2, 0
_0801D0DC: .4byte 0x03004750
_0801D0E0: .4byte off_813BF7C
_0801D0E4:
	bl sub_0801C750
	ldr r5, _0801D11C
	add r4, r5, #0
	add r4, #0xa6
	ldrh r0, [r4]
	cmp r0, #0xff
	beq _0801D0FC
	bl freePalFadeTaskAtIndex
	mov r0, #0xff
	strh r0, [r4]
_0801D0FC:
	ldr r0, _0801D120
_0801D0FE:
	ldr r1, [r5]
	ldr r1, [r1]
	add r1, #0x22
	ldrb r1, [r1]
	lsl r1, r1, #0x18
	asr r1, r1, #0x18
	lsl r1, r1, #5
	ldr r2, _0801D124
	add r1, r1, r2
	mov r2, #0
	bl startPalAnimation
	strh r0, [r4]
	b _0801D1EC
	.align 2, 0
_0801D11C: .4byte 0x03004750
_0801D120: .4byte off_813BD50
_0801D124: .4byte 0x05000200
_0801D128:
	ldr r4, _0801D170
	add r0, r4, #0
	add r0, #0x23
	ldrb r1, [r0]
	mov r0, #0x14
	and r0, r1
	cmp r0, #0
	bne _0801D1EC
	bl isUsingInvulnSpecial
	cmp r0, #0
	bne _0801D1EC
	add r0, r4, #0
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	cmp r0, #0
	blt _0801D1EC
	add r5, r4, #0
	add r5, #0xa6
	ldrh r0, [r5]
	cmp r0, #0xff
	beq _0801D15E
	bl freePalFadeTaskAtIndex
	mov r0, #0xff
	strh r0, [r5]
_0801D15E:
	bl getPowerUpLevel
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #1
	bne _0801D178
	ldr r0, _0801D174
	b _0801D1D2
	.align 2, 0
_0801D170: .4byte 0x03004750
_0801D174: .4byte off_8103628
_0801D178:
	ldr r0, _0801D17C
	b _0801D1D2
	.align 2, 0
_0801D17C: .4byte off_810393C
_0801D180:
	ldr r4, _0801D1C8
	add r0, r4, #0
	add r0, #0x23
	ldrb r1, [r0]
	mov r0, #0x14
	and r0, r1
	cmp r0, #0
	bne _0801D1EC
	bl isUsingInvulnSpecial
	cmp r0, #0
	bne _0801D1EC
	add r0, r4, #0
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	cmp r0, #0
	blt _0801D1EC
	add r5, r4, #0
	add r5, #0xa6
	ldrh r0, [r5]
	cmp r0, #0xff
	beq _0801D1B6
	bl freePalFadeTaskAtIndex
	mov r0, #0xff
	strh r0, [r5]
_0801D1B6:
	bl getPowerUpLevel
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #1
	bne _0801D1D0
	ldr r0, _0801D1CC
	b _0801D1D2
	.align 2, 0
_0801D1C8: .4byte 0x03004750
_0801D1CC: .4byte playerlvl3_to2_palfade
_0801D1D0:
	ldr r0, _0801D1F4
_0801D1D2:
	ldr r1, [r4]
	ldr r1, [r1]
	add r1, #0x22
	ldrb r1, [r1]
	lsl r1, r1, #0x18
	asr r1, r1, #0x18
	lsl r1, r1, #5
	ldr r2, _0801D1F8
	add r1, r1, r2
	mov r2, #0
	bl startPalAnimation
	strh r0, [r5]
_0801D1EC:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_0801D1F4: .4byte playerlvl2_to1_palfade
_0801D1F8: .4byte 0x05000200

thumb_func_global initGrapplingHook
initGrapplingHook: @ 0x0801D1FC
	push {r4, r5, r6, lr}
	sub sp, #0x14
	add r6, r0, #0
	lsl r6, r6, #0x10
	lsr r6, r6, #0x10
	mov r0, #2
	bl allocateSpriteTiles
	add r5, r0, #0
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	lsl r6, r6, #0x10
	mov r0, #0x80
	lsl r0, r0, #9
	add r6, r6, r0
	lsr r6, r6, #0x10
	add r1, sp, #0x10
	mov r0, #0
	strh r0, [r1]
	ldr r1, _0801D260
	add r2, sp, #0x10
	str r2, [r1]
	ldr r4, _0801D264
	str r4, [r1, #4]
	ldr r0, _0801D268
	str r0, [r1, #8]
	ldr r0, [r1, #8]
	ldr r0, _0801D26C
	lsl r1, r5, #5
	ldr r2, _0801D270
	add r1, r1, r2
	mov r2, #0x20
	bl Bios_memcpy
	mov r0, #0
	strh r5, [r4, #0x30]
	add r5, #1
	strh r5, [r4, #0x32]
	strh r6, [r4, #0x34]
	str r0, [r4, #0x10]
	str r0, [r4, #0x18]
	str r0, [r4, #0x1c]
	mov r5, #0
	mov r6, #0
_0801D254:
	lsl r0, r5, #0x18
	lsr r1, r0, #0x18
	cmp r5, #0
	bne _0801D274
	ldrh r3, [r4, #0x32]
	b _0801D276
	.align 2, 0
_0801D260: .4byte 0x040000D4
_0801D264: .4byte 0x03004810
_0801D268: .4byte 0x8100002C
_0801D26C: .4byte grapplingHookGfx
_0801D270: .4byte 0x06010000
_0801D274:
	ldrh r3, [r4, #0x30]
_0801D276:
	ldr r0, _0801D2A8
	ldrh r0, [r0, #0x34]
	str r0, [sp]
	mov r0, #0xa0
	lsl r0, r0, #2
	str r0, [sp, #4]
	str r6, [sp, #8]
	str r6, [sp, #0xc]
	add r0, r1, #0
	mov r1, #0
	mov r2, #0
	bl createSimpleSprite
	ldr r1, _0801D2AC
	lsl r0, r5, #4
	add r0, r0, r1
	strb r6, [r0, #0xc]
	add r5, #1
	cmp r5, #0x17
	ble _0801D254
	add sp, #0x14
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_0801D2A8: .4byte 0x03004810
_0801D2AC: .4byte 0x030037D0




thumb_func_global unused_freeGrapplingHook
unused_freeGrapplingHook: @ 0x0801D2B0
	push {r4, r5, lr}
	ldr r0, _0801D2D4
	ldrh r0, [r0, #0x30]
	mov r1, #2
	bl freeSpriteTiles
	ldr r5, _0801D2D8
	mov r4, #0x17
_0801D2C0:
	add r0, r5, #0
	bl freeSimpleSpriteHeader
	add r5, #0x10
	sub r4, #1
	cmp r4, #0
	bge _0801D2C0
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_0801D2D4: .4byte 0x03004810
_0801D2D8: .4byte 0x030037D0



thumb_func_global sub_0801D2DC
sub_0801D2DC: @ 0x0801D2DC
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	ldr r3, _0801D324
	mov r0, #0
	str r0, [r3, #0x10]
	str r0, [r3, #0x18]
	str r0, [r3, #0x1c]
	str r0, [r3, #0x20]
	add r1, r3, #0
	add r1, #0x44
	mov r2, #0
	strh r0, [r1]
	add r1, #2
	strh r0, [r1]
	add r0, r3, #0
	add r0, #0x56
	strb r2, [r0]
	add r0, #1
	strb r2, [r0]
	ldr r4, _0801D328
	mov ip, r3
	ldr r0, _0801D32C
	mov r8, r0
	add r3, r4, #0
	mov r0, #0xb8
	lsl r0, r0, #1
	add r7, r3, r0
	ldr r0, _0801D330
	add r6, r0, #0
	mov r5, #0
_0801D31A:
	cmp r3, r4
	bne _0801D334
	mov r0, ip
	ldrh r2, [r0, #0x32]
	b _0801D338
	.align 2, 0
_0801D324: .4byte 0x03004810
_0801D328: .4byte 0x030037D0
_0801D32C: .4byte 0xFFFFFC00
_0801D330: .4byte 0x000003FF
_0801D334:
	mov r0, ip
	ldrh r2, [r0, #0x30]
_0801D338:
	and r2, r6
	ldrh r1, [r3, #4]
	mov r0, r8
	and r0, r1
	orr r0, r2
	strh r0, [r3, #4]
	strb r5, [r3, #0xc]
	add r3, #0x10
	cmp r3, r7
	ble _0801D31A
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
.align 2, 0




	thumb_func_global sub_801D358
sub_801D358: @ 0x0801D358
	push {r4, r5, r6, lr}
	add r5, r0, #0
	add r6, r1, #0
	bl sub_801D964
	ldr r4, _0801D388
	str r0, [r4, #0x10]
	add r0, r5, #0
	add r1, r6, #0
	bl sub_801D9E0
	str r0, [r4, #0x20]
	ldr r0, [r5]
	ldr r1, [r5, #4]
	str r0, [r4]
	str r1, [r4, #4]
	ldr r0, [r6]
	ldr r1, [r6, #4]
	str r0, [r4, #8]
	str r1, [r4, #0xc]
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_0801D388: .4byte 0x03004810
	thumb_func_global sub_801D38C
sub_801D38C: @ 0x0801D38C
	push {lr}
	sub sp, #8
	str r0, [sp]
	str r1, [sp, #4]
	ldr r0, _0801D3A4
	mov r1, sp
	bl sub_801D358
	add sp, #8
	pop {r0}
	bx r0
	.align 2, 0
_0801D3A4: .4byte 0x03004810





thumb_func_global sub_0801D3A8
sub_0801D3A8: @ 0x0801D3A8
	push {r4, r5, r6, lr}
	add r4, r0, #0
	add r5, r1, #0
	add r6, r2, #0
	lsl r4, r4, #0x18
	lsr r4, r4, #0x18
	bl sub_0801D2DC
	ldr r1, _0801D3D0
	sub r4, #0x40
	lsl r4, r4, #8
	str r4, [r1, #0x10]
	str r5, [r1, #0x38]
	str r6, [r1, #0x3c]
	add r1, #0x40
	mov r0, #0
	strh r0, [r1]
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_0801D3D0: .4byte 0x03004810

thumb_func_global sub_0801D3D4
sub_0801D3D4: @ 0x0801D3D4
	push {r4, lr}
	mov r4, #0
	ldr r2, _0801D404
	add r3, r2, #0
	add r3, #0x40
	ldrh r0, [r3]
	cmp r0, #0
	bne _0801D408
	ldr r0, [r2, #0x20]
	ldr r1, [r2, #0x38]
	add r0, r0, r1
	str r0, [r2, #0x20]
	mov r1, #0xc0
	lsl r1, r1, #7
	cmp r0, r1
	ble _0801D436
	str r1, [r2, #0x20]
	mov r0, #0
	mov r1, #1
	strh r1, [r3]
	add r1, r2, #0
	add r1, #0x42
	strb r0, [r1]
	b _0801D436
	.align 2, 0
_0801D404: .4byte 0x03004810
_0801D408:
	cmp r0, #1
	bne _0801D424
	add r0, r2, #0
	add r0, #0x42
	ldrb r1, [r0]
	add r2, r1, #1
	strb r2, [r0]
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	cmp r1, #0xa
	bls _0801D436
	mov r0, #2
	strh r0, [r3]
	b _0801D436
_0801D424:
	ldr r0, [r2, #0x20]
	ldr r1, [r2, #0x3c]
	sub r0, r0, r1
	str r0, [r2, #0x20]
	cmp r0, #0
	bgt _0801D436
	str r4, [r2, #0x20]
	strh r4, [r3]
	mov r4, #1
_0801D436:
	add r0, r4, #0
	pop {r4}
	pop {r1}
	bx r1



.align 2, 0
	thumb_func_global sub_801D440
sub_801D440: @ 0x0801D440
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #0x10
	ldr r4, _0801D4D8
	str r0, [r4, #8]
	str r1, [r4, #0xc]
	add r1, r4, #0
	add r1, #8
	ldr r2, [r4, #0x10]
	ldr r3, [r4, #0x20]
	mov r0, sp
	bl sub_801DA34
	ldr r0, [sp]
	ldr r1, [sp, #4]
	str r0, [r4]
	str r1, [r4, #4]
	ldr r0, [r4, #0x20]
	lsl r0, r0, #8
	asr r0, r0, #0x10
	cmp r0, #0
	bge _0801D470
	add r0, #3
_0801D470:
	asr r2, r0, #2
	ldr r0, [r4, #0x10]
	asr r0, r0, #8
	lsl r0, r0, #0x18
	lsr r6, r0, #0x19
	cmp r2, #3
	ble _0801D54E
	sub r5, r2, #4
	cmp r5, #0
	blt _0801D54E
	mov r8, r4
	add r4, sp, #8
	ldr r7, _0801D4DC
_0801D48A:
	ldr r1, _0801D4E0
	lsl r2, r5, #2
	lsl r0, r6, #7
	add r2, r2, r0
	add r2, r2, r1
	mov r0, #0
	ldrsh r1, [r2, r0]
	lsl r1, r1, #8
	mov r3, r8
	ldr r0, [r3]
	add r0, r0, r1
	str r0, [sp, #8]
	mov r1, #2
	ldrsh r0, [r2, r1]
	lsl r0, r0, #8
	ldr r1, [r3, #4]
	sub r1, r1, r0
	str r1, [r4, #4]
	ldr r0, [sp, #8]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	lsl r1, r1, #8
	lsr r1, r1, #0x10
	bl getCollision
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	bl collisionUnk
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0801D548
	cmp r6, #0x10
	beq _0801D4F6
	cmp r6, #0x10
	bgt _0801D4E4
	cmp r6, #0
	beq _0801D4EA
	b _0801D524
	.align 2, 0
_0801D4D8: .4byte 0x03004810
_0801D4DC: .4byte 0xFFFFF800
_0801D4E0: .4byte dword_813D314
_0801D4E4:
	cmp r6, #0x70
	beq _0801D510
	b _0801D524
_0801D4EA:
	ldr r0, [r4, #4]
	and r0, r7
	mov r2, #0x80
	lsl r2, r2, #3
	add r0, r0, r2
	b _0801D522
_0801D4F6:
	ldr r0, [sp, #8]
	and r0, r7
	ldr r3, _0801D50C
	add r0, r0, r3
	str r0, [sp, #8]
	ldr r0, [r4, #4]
	and r0, r7
	mov r1, #0x80
	lsl r1, r1, #3
	b _0801D520
	.align 2, 0
_0801D50C: .4byte 0x000003FF
_0801D510:
	ldr r0, [sp, #8]
	and r0, r7
	mov r1, #0x80
	lsl r1, r1, #3
	add r0, r0, r1
	str r0, [sp, #8]
	ldr r0, [r4, #4]
	and r0, r7
_0801D520:
	add r0, r0, r1
_0801D522:
	str r0, [r4, #4]
_0801D524:
	add r0, r4, #0
	ldr r1, _0801D540
	bl sub_801D358
	ldr r0, _0801D544
	ldr r0, [r0]
	ldr r1, [r0, #8]
	ldr r0, [r0, #4]
	mov r2, r8
	str r0, [r2, #0x4c]
	str r1, [r2, #0x50]
	mov r0, #1
	b _0801D550
	.align 2, 0
_0801D540: .4byte 0x03004818
_0801D544: .4byte 0x03004750
_0801D548:
	sub r5, #1
	cmp r5, #0
	bge _0801D48A
_0801D54E:
	mov r0, #0
_0801D550:
	add sp, #0x10
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	




thumb_func_global sub_0801D55C
sub_0801D55C: @ 0x0801D55C
	push {r4, r5, r6, r7, lr}
	mov r7, #0
	lsl r0, r0, #8
	asr r0, r0, #0x10
	cmp r0, #0
	bge _0801D56A
	add r0, #3
_0801D56A:
	asr r3, r0, #2
	ldr r0, _0801D5D8
	add r1, r0, #0
	add r1, #0x46
	ldrh r1, [r1]
	add r4, r0, #0
	add r4, #0x44
	ldrh r2, [r4]
	sub r1, r1, r2
	add r1, #1
	mov ip, r0
	cmp r3, r1
	blt _0801D588
	add r3, r1, #0
	mov r7, #1
_0801D588:
	cmp r3, #0
	bne _0801D58E
	mov r3, #1
_0801D58E:
	cmp r3, #0
	ble _0801D5AC
	ldr r6, _0801D5DC
	mov r5, #0
	add r2, r3, #0
_0801D598:
	ldrh r0, [r4]
	add r1, r0, #1
	strh r1, [r4]
	lsl r0, r0, #0x10
	lsr r0, r0, #0xc
	add r0, r0, r6
	strb r5, [r0, #0xc]
	sub r2, #1
	cmp r2, #0
	bne _0801D598
_0801D5AC:
	cmp r7, #0
	bne _0801D5D0
	ldr r2, _0801D5DC
	mov r0, ip
	add r0, #0x44
	ldrh r1, [r0]
	lsl r1, r1, #4
	add r1, r1, r2
	mov r0, ip
	ldrh r2, [r0, #0x32]
	ldr r3, _0801D5E0
	add r0, r3, #0
	and r2, r0
	ldrh r3, [r1, #4]
	ldr r0, _0801D5E4
	and r0, r3
	orr r0, r2
	strh r0, [r1, #4]
_0801D5D0:
	add r0, r7, #0
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
_0801D5D8: .4byte 0x03004810
_0801D5DC: .4byte 0x030037D0
_0801D5E0: .4byte 0x000003FF
_0801D5E4: .4byte 0xFFFFFC00

thumb_func_global grapple_leftHeld
grapple_leftHeld: @ 0x0801D5E8
	push {r4, lr}
	ldr r1, _0801D67C
	add r0, r1, #0
	add r0, #0x29
	ldrb r0, [r0]
	add r4, r1, #0
	cmp r0, #1
	beq _0801D620
	ldr r2, [r4, #0x18]
	cmp r2, #0
	bge _0801D600
	neg r2, r2
_0801D600:
	ldr r1, _0801D680
	ldr r0, [r4, #0x20]
	asr r0, r0, #0xc
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r1, [r0]
	cmp r2, r1
	bge _0801D620
	ldr r0, [r4, #0x10]
	add r0, #0x80
	asr r0, r0, #8
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0801D620
	neg r0,r1
	str r0, [r4, #0x18]
_0801D620:
	ldr r0, [r4, #0x1c]
	sub r0, #0x1f
	str r0, [r4, #0x1c]
	add r0, r4, #0
	add r0, #0x57
	ldrb r0, [r0]
	cmp r0, #0
	beq _0801D674
	add r0, r4, #0
	add r0, #0x29
	ldrb r0, [r0]
	cmp r0, #2
	beq _0801D674
	mov r0, #0
	strh r0, [r4, #0x2e]
	ldr r1, _0801D680
	ldr r0, [r4, #0x20]
	asr r0, r0, #0xc
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0xb0
	lsl r1, r1, #1
	bl multiply_fixed8
	neg r0, r0
	str r0, [r4, #0x18]
	mov r1, #0x80
	lsl r1, r1, #5
	bl sub_800F2A0
	neg r0, r0
	str r0, [r4, #0x1c]
	add r1, r4, #0
	add r1, #0x55
	mov r0, #2
	strb r0, [r1]
	sub r1, #1
	mov r0, #0x14
	strb r0, [r1]
	bl sub_8018B34
_0801D674:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801D67C: .4byte 0x03004810
_0801D680: .4byte dword_8142380

thumb_func_global sub_0801D684
sub_0801D684: @ 0x0801D684
	push {r4, lr}
	ldr r1, _0801D714
	add r0, r1, #0
	add r0, #0x29
	ldrb r0, [r0]
	add r4, r1, #0
	cmp r0, #2
	beq _0801D6BA
	ldr r2, [r4, #0x18]
	cmp r2, #0
	bge _0801D69C
	neg r2, r2
_0801D69C:
	ldr r1, _0801D718
	ldr r0, [r4, #0x20]
	asr r0, r0, #0xc
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r1, [r0]
	cmp r2, r1
	bge _0801D6BA
	ldr r0, [r4, #0x10]
	add r0, #0x80
	asr r0, r0, #8
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0801D6BA
	str r1, [r4, #0x18]
_0801D6BA:
	ldr r0, [r4, #0x1c]
	add r0, #0x1f
	str r0, [r4, #0x1c]
	add r0, r4, #0
	add r0, #0x57
	ldrb r0, [r0]
	cmp r0, #0
	beq _0801D70C
	add r0, r4, #0
	add r0, #0x29
	ldrb r0, [r0]
	cmp r0, #1
	beq _0801D70C
	mov r0, #0
	strh r0, [r4, #0x2e]
	ldr r1, _0801D718
	ldr r0, [r4, #0x20]
	asr r0, r0, #0xc
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0xb0
	lsl r1, r1, #1
	bl multiply_fixed8
	str r0, [r4, #0x18]
	mov r1, #0x80
	lsl r1, r1, #5
	bl sub_800F2A0
	neg r0, r0
	str r0, [r4, #0x1c]
	add r1, r4, #0
	add r1, #0x55
	mov r0, #1
	strb r0, [r1]
	sub r1, #1
	mov r0, #0x28
	strb r0, [r1]
	bl sub_8018B34
_0801D70C:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801D714: .4byte 0x03004810
_0801D718: .4byte dword_8142380

thumb_func_global sub_801D71C
sub_801D71C: @ 0x0801D71C
	push {r4, lr}
	add r2, r0, #0
	ldr r4, _0801D73C
	ldr r3, [r4, #0x20]
	sub r1, r3, r2
	ldr r0, _0801D740
	cmp r1, r0
	bgt _0801D730
	ldr r0, _0801D744
	add r2, r3, r0
_0801D730:
	neg r0, r2
	str r0, [r4, #0x24]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801D73C: .4byte 0x03004810
_0801D740: .4byte 0x000007FF
_0801D744: .4byte 0xFFFFF800

thumb_func_global sub_801D748
sub_801D748: @ 0x0801D748
	push {r4, lr}
	add r1, r0, #0
	ldr r4, _0801D764
	ldr r2, [r4, #0x20]
	add r0, r2, r1
	mov r3, #0xc0
	lsl r3, r3, #7
	cmp r0, r3
	ble _0801D75C
	sub r1, r3, r2
_0801D75C:
	str r1, [r4, #0x24]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801D764: .4byte 0x03004810



thumb_func_global sub_801D768
sub_801D768: @ 0x0801D768
	ldr r0, _0801D77C
	add r1, r0, #0
	add r1, #0x56
	ldrb r0, [r1]
	cmp r0, #0
	bne locret_801D778
	mov r0, #0x30
	strb r0, [r1]
locret_801D778:
	bx lr
	.align 2, 0
_0801D77C: .4byte 0x03004810
	thumb_func_global sub_801D780
sub_801D780: @ 0x0801D780
	push {r4, lr}
	ldr r1, _0801D7A8
	add r4, r1, #0
	add r4, #0x56
	ldrb r0, [r4]
	cmp r0, #0
	beq _0801D7B0
	ldr r1, [r1, #0x20]
	mov r0, #0x80
	lsl r0, r0, #4
	cmp r1, r0
	ble _0801D7AC
	mov r0, #0xa0
	lsl r0, r0, #3
	bl sub_801D71C
	ldrb r0, [r4]
	sub r0, #1
	b _0801D7AE
	.align 2, 0
_0801D7A8: .4byte 0x03004810
_0801D7AC:
	mov r0, #0
_0801D7AE:
	strb r0, [r4]
_0801D7B0:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0



thumb_func_global sub_0801D7B8
sub_0801D7B8: @ 0x0801D7B8
	push {r4, lr}
	sub sp, #0xc
	mov r1, sp
	ldr r0, _0801D7D8
	ldm r0!, {r2, r3, r4}
	stm r1!, {r2, r3, r4}
	ldr r1, _0801D7DC
	ldr r0, [r1, #0x10]
	asr r0, r0, #8
	lsl r0, r0, #0x18
	lsr r3, r0, #0x18
	add r4, r1, #0
	cmp r3, #0
	bne _0801D7E0
	str r3, [r4, #0x1c]
	b _0801D7F0
	.align 2, 0
_0801D7D8: .4byte dword_814239C
_0801D7DC: .4byte 0x03004810
_0801D7E0:
	lsl r0, r3, #0x18
	cmp r0, #0
	blt _0801D7EC
	mov r0, #0x30
	neg r0, r0
	b _0801D7EE
_0801D7EC:
	mov r0, #0x30
_0801D7EE:
	str r0, [r4, #0x1c]
_0801D7F0:
	add r0, r4, #0
	add r0, #0x29
	ldrb r0, [r0]
	lsl r0, r0, #2
	mov r2, sp
	add r1, r2, r0
	ldr r0, [r4, #0x1c]
	ldr r1, [r1]
	add r0, r0, r1
	str r0, [r4, #0x1c]
	cmp r3, #0
	bne _0801D826
	ldr r2, [r4, #0x18]
	cmp r2, #0
	bge _0801D810
	neg r2, r2
_0801D810:
	ldr r0, _0801D83C
	ldr r1, [r4, #0x20]
	asr r1, r1, #0xc
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r0, [r1]
	cmp r2, r0
	bge _0801D826
	str r3, [r4, #0x1c]
	str r3, [r4, #0x18]
	str r3, [r4, #0x10]
_0801D826:
	add r1, r4, #0
	add r1, #0x54
	ldrb r0, [r1]
	cmp r0, #0
	beq _0801D834
	sub r0, #1
	strb r0, [r1]
_0801D834:
	add sp, #0xc
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801D83C: .4byte dword_8142364
	
	
	
	
	thumb_func_global sub_801D840
sub_801D840: @ 0x0801D840
	push {r4, lr}
	ldr r4, _0801D87C
	ldr r0, [r4, #0x10]
	asr r0, r0, #8
	lsl r0, r0, #0x18
	lsr r1, r0, #0x18
	ldr r0, [r4, #0x18]
	cmp r0, #0x7f
	bgt _0801D876
	add r0, r1, #0
	sub r0, #0x71
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0x1e
	bhi _0801D876
	ldr r1, [r4, #0x20]
	mov r0, #0x81
	lsl r0, r0, #4
	cmp r1, r0
	ble _0801D876
	mov r0, #0xa0
	lsl r0, r0, #3
	bl sub_801D71C
	mov r0, #0
	str r0, [r4, #0x1c]
	str r0, [r4, #0x18]
_0801D876:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801D87C: .4byte 0x03004810
	thumb_func_global sub_801D880
sub_801D880: @ 0x0801D880
	push {r4, r5, lr}
	ldr r2, _0801D8C4
	ldr r1, [r2, #0x10]
	asr r1, r1, #8
	lsl r1, r1, #0x18
	lsr r4, r1, #0x18
	add r1, r2, #0
	add r1, #0x29
	ldrb r1, [r1]
	add r3, r2, #0
	add r3, #0x2a
	mov r5, #0
	strb r1, [r3]
	ldr r1, _0801D8C8
	cmp r0, r1
	bge _0801D8A2
	str r5, [r2, #0x1c]
_0801D8A2:
	add r0, r4, #0
	sub r0, #0x61
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0x3e
	bhi _0801D8D4
	ldr r1, [r2, #0x18]
	add r3, r1, #0
	cmp r1, #0
	bge _0801D8B8
	neg r3, r1
_0801D8B8:
	ldr r0, _0801D8CC
	cmp r3, r0
	bgt _0801D8D0
	ldr r0, [r2, #0x1c]
	add r0, r1, r0
	b _0801D8DA
	.align 2, 0
_0801D8C4: .4byte 0x03004810
_0801D8C8: .4byte 0xFFFFFC00
_0801D8CC: .4byte 0x000001FF
_0801D8D0:
	str r5, [r2, #0x1c]
	b _0801D8DC
_0801D8D4:
	ldr r0, [r2, #0x18]
	ldr r1, [r2, #0x1c]
	add r0, r0, r1
_0801D8DA:
	str r0, [r2, #0x18]
_0801D8DC:
	ldr r1, _0801D8F4
	ldr r0, [r1, #0x18]
	cmp r0, #0
	bge _0801D8FC
	ldr r2, _0801D8F8
	cmp r0, r2
	bge _0801D8EC
	str r2, [r1, #0x18]
_0801D8EC:
	add r1, #0x29
	mov r0, #2
	strb r0, [r1]
	b _0801D91C
	.align 2, 0
_0801D8F4: .4byte 0x03004810
_0801D8F8: .4byte 0xFFFFF900
_0801D8FC:
	cmp r0, #0
	ble _0801D912
	mov r2, #0xe0
	lsl r2, r2, #3
	cmp r0, r2
	ble _0801D90A
	str r2, [r1, #0x18]
_0801D90A:
	add r1, #0x29
	mov r0, #1
	strb r0, [r1]
	b _0801D91C
_0801D912:
	add r1, #0x29
	mov r0, #0
	strb r0, [r1]
	bl sub_8018B34
_0801D91C:
	ldr r1, _0801D954
	add r3, r1, #0
	add r3, #0x54
	ldrb r0, [r3]
	cmp r0, #0
	beq _0801D942
	add r0, r1, #0
	add r0, #0x55
	add r2, r1, #0
	add r2, #0x29
	ldrb r0, [r0]
	ldrb r2, [r2]
	cmp r0, r2
	beq _0801D93C
	mov r0, #0
	strb r0, [r3]
_0801D93C:
	ldrb r0, [r3]
	cmp r0, #0
	bne _0801D958
_0801D942:
	ldrh r0, [r1, #0x2e]
	cmp r0, #1
	bls _0801D958
	add r1, #0x57
	mov r0, #1
	strb r0, [r1]
	bl sub_8018B34
	b _0801D95E
	.align 2, 0
_0801D954: .4byte 0x03004810
_0801D958:
	add r1, #0x57
	mov r0, #0
	strb r0, [r1]
_0801D95E:
	pop {r4, r5}
	pop {r0}
	bx r0
	
	
	
	

thumb_func_global sub_801D964
sub_801D964: @ 0x0801D964
	push {r4, lr}
	mov r4, #3
	ldr r3, [r1]
	ldr r2, [r0]
	sub r3, r3, r2
	ldr r1, [r1, #4]
	ldr r0, [r0, #4]
	sub r2, r1, r0
	cmp r3, #0
	blt _0801D988
	cmp r2, #0
	bge _0801D980
	mov r4, #0
	b _0801D98E
_0801D980:
	cmp r3, #0
	ble _0801D988
	mov r4, #1
	b _0801D98E
_0801D988:
	cmp r2, #0
	ble _0801D98E
	mov r4, #2
_0801D98E:
	mov r0, #1
	and r0, r4
	cmp r0, #0
	beq _0801D9A8
	add r1, r3, #0
	cmp r1, #0
	bge _0801D99E
	neg r1, r1
_0801D99E:
	add r3, r2, #0
	cmp r3, #0
	bge _0801D9B6
	neg r3, r3
	b _0801D9B6
_0801D9A8:
	cmp r3, #0
	bge _0801D9AE
	neg r3, r3
_0801D9AE:
	add r1, r2, #0
	cmp r1, #0
	bge _0801D9B6
	neg r1, r1
_0801D9B6:
	ldr r0, _0801D9BC
	lsl r2, r4, #6
	b _0801D9C4
	.align 2, 0
_0801D9BC: .4byte off_8142314
_0801D9C0:
	lsr r3, r3, #1
	lsr r1, r1, #1
_0801D9C4:
	cmp r3, #0x1f
	bhi _0801D9C0
	cmp r1, #0x1f
	bhi _0801D9C0
	ldr r0, [r0]
	lsl r1, r1, #5
	add r1, r1, r3
	add r0, r0, r1
	ldrb r0, [r0]
	add r0, r0, r2
	lsl r0, r0, #8
	pop {r4}
	pop {r1}
	bx r1
	




thumb_func_global sub_801D9E0
sub_801D9E0: @ 0x0801D9E0
	push {r4, lr}
	add r4, r0, #0
	add r3, r1, #0
	ldr r1, [r3]
	ldr r0, [r4]
	sub r2, r1, r0
	cmp r2, #0
	bge _0801D9F2
	neg r2, r2
_0801D9F2:
	ldr r1, [r3, #4]
	ldr r0, [r4, #4]
	sub r1, r1, r0
	cmp r1, #0
	bge _0801D9FE
	neg r1, r1
_0801D9FE:
	add r0, r2, #0
	mul r0, r2, r0
	add r2, r1, #0
	mul r2, r1, r2
	add r1, r2, #0
	add r0, r0, r1
	bl Bios_Sqrt
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	mov r0, #0xc0
	lsl r0, r0, #7
	cmp r1, r0
	ble _0801DA1C
	add r1, r0, #0
_0801DA1C:
	ldr r0, _0801DA30
	cmp r1, r0
	bgt _0801DA26
	mov r1, #0x80
	lsl r1, r1, #4
_0801DA26:
	add r0, r1, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
_0801DA30: .4byte 0x000007FF
	thumb_func_global sub_801DA34
sub_801DA34: @ 0x0801DA34
	push {r4, r5, r6, r7, lr}
	add r7, r0, #0
	add r6, r1, #0
	asr r2, r2, #8
	lsl r2, r2, #0x18
	lsr r1, r2, #0x19
	lsl r3, r3, #8
	asr r0, r3, #0x10
	cmp r0, #0
	bge _0801DA4A
	add r0, #3
_0801DA4A:
	asr r0, r0, #2
	ldr r2, _0801DA78
	sub r0, #1
	lsl r0, r0, #2
	lsl r1, r1, #7
	add r0, r0, r1
	add r0, r0, r2
	mov r2, #0
	ldrsh r1, [r0, r2]
	lsl r1, r1, #8
	ldr r2, [r6]
	sub r4, r2, r1
	mov r1, #2
	ldrsh r0, [r0, r1]
	lsl r0, r0, #8
	ldr r1, [r6, #4]
	add r5, r1, r0
	str r4, [r7]
	str r5, [r7, #4]
	add r0, r7, #0
	pop {r4, r5, r6, r7}
	pop {r2}
	bx r2
	.align 2, 0
_0801DA78: .4byte dword_813D314
	thumb_func_global sub_801DA7C
sub_801DA7C: @ 0x0801DA7C
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x34
	add r7, r0, #0
	ldr r0, [r7, #0x18]
	ldr r1, [r7, #0x1c]
	str r0, [sp, #0x20]
	str r1, [sp, #0x24]
	add r1, sp, #0x1c
	mov r0, #0
	strh r0, [r1]
	mov r2, #0x80
	lsl r2, r2, #4
	ldr r0, [sp, #0x20]
	add r3, sp, #0x20
	mov r8, r3
	cmp r0, r2
	bgt _0801DAAC
	ldr r0, [r3, #4]
	cmp r0, r2
	ble _0801DAB0
_0801DAAC:
	mov r0, #1
	strh r0, [r1]
_0801DAB0:
	add r6, r7, #0
	add r6, #0x4f
	mov r4, #0
	strb r4, [r6]
	add r5, r7, #0
	add r5, #0x51
	strb r4, [r5]
	add r0, r7, #0
	add r0, #0x18
	bl getVelocityDirection
	add r1, r7, #0
	add r1, #0x4c
	strb r0, [r1]
	add r2, sp, #0x10
	mov r0, #0x38
	ldrsh r1, [r7, r0]
	lsl r1, r1, #8
	ldr r0, [r7, #4]
	add r0, r0, r1
	str r0, [sp, #0x10]
	mov r3, #0x3a
	ldrsh r1, [r7, r3]
	lsl r1, r1, #8
	ldr r0, [r7, #8]
	add r0, r0, r1
	str r0, [r2, #4]
	ldrh r0, [r7, #0x3c]
	sub r0, #1
	strh r0, [r2, #8]
	ldrh r0, [r7, #0x3e]
	sub r0, #1
	strh r0, [r2, #0xa]
	str r4, [r7, #0x1c]
	str r4, [r7, #0x18]
	str r6, [sp, #0x30]
	mov sb, r5
	mov sl, r2
_0801DAFC:
	mov r0, r8
	bl sub_8017040
	str r0, [sp, #0x28]
	mov r0, r8
	add r0, #4
	bl sub_8017040
	add r4, r0, #0
	str r4, [sp, #0x2c]
	add r0, sp, #0x10
	add r1, r4, #0
	bl sub_8017084
	add r6, r0, #0
	cmp r6, #0
	beq _0801DB26
	ldrb r0, [r5]
	mov r1, #1
	orr r0, r1
	strb r0, [r5]
_0801DB26:
	add r0, sp, #0x10
	add r1, r4, #0
	bl sub_80170A4
	add r2, r0, #0
	cmp r2, #0
	beq _0801DB4E
	ldrb r1, [r5]
	mov r0, #2
	orr r1, r0
	mov r0, #0
	orr r1, r0
	strb r1, [r5]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	bne _0801DB4E
	str r0, [sp, #0x20]
	mov r1, #0
	str r1, [sp, #0x28]
_0801DB4E:
	add r0, r6, r2
	ldr r2, [sp, #0x2c]
	add r0, r2, r0
	str r0, [sp, #0x2c]
	mov r4, sl
	ldr r0, [r4, #4]
	ldr r3, [sp, #0x2c]
	add r0, r0, r3
	str r0, [r4, #4]
	add r0, r4, #0
	ldr r1, [sp, #0x28]
	bl sub_80170C4
	add r6, r0, #0
	cmp r6, #0
	beq _0801DB76
	ldrb r0, [r5]
	mov r1, #4
	orr r0, r1
	strb r0, [r5]
_0801DB76:
	add r0, r4, #0
	ldr r1, [sp, #0x28]
	bl sub_80170E4
	add r2, r0, #0
	cmp r2, #0
	beq _0801DB8C
	ldrb r0, [r5]
	mov r1, #8
	orr r0, r1
	strb r0, [r5]
_0801DB8C:
	add r0, r6, r2
	ldr r1, [sp, #0x28]
	add r0, r1, r0
	str r0, [sp, #0x28]
	ldr r0, [sp, #0x10]
	ldr r2, [sp, #0x28]
	add r0, r0, r2
	str r0, [sp, #0x10]
	mov r0, sl
	mov r1, sp
	bl sub_801F7A4
	add r3, r0, #0
	cmp r3, #0
	beq _0801DC0A
	ldr r2, [sp]
	cmp r2, #0
	beq _0801DBBC
	ldrb r0, [r5]
	mov r1, #1
	orr r0, r1
	strb r0, [r5]
	mov r0, r8
	str r2, [r0, #4]
_0801DBBC:
	ldr r2, [sp, #4]
	cmp r2, #0
	beq _0801DBEA
	ldrb r0, [r5]
	mov r1, #2
	orr r0, r1
	strb r0, [r5]
	mov r1, r8
	str r2, [r1, #4]
	ldr r0, [sp, #4]
	cmp r0, #0
	beq _0801DBEA
	ldr r0, [sp]
	cmp r0, #0
	bne _0801DBEA
	ldr r0, [r3, #0x24]
	add r0, r2, r0
	str r0, [r1, #4]
	ldr r0, [sp, #0x20]
	ldr r1, [r3, #0x20]
	add r0, r0, r1
	str r0, [sp, #0x20]
	b _0801DC0A
_0801DBEA:
	ldr r2, [sp, #8]
	cmp r2, #0
	beq _0801DBFA
	ldrb r0, [r5]
	mov r1, #4
	orr r0, r1
	strb r0, [r5]
	str r2, [sp, #0x20]
_0801DBFA:
	ldr r2, [sp, #0xc]
	cmp r2, #0
	beq _0801DC0A
	ldrb r0, [r5]
	mov r1, #8
	orr r0, r1
	strb r0, [r5]
	str r2, [sp, #0x20]
_0801DC0A:
	ldr r0, [r7, #0x18]
	ldr r2, [sp, #0x28]
	add r0, r0, r2
	str r0, [r7, #0x18]
	ldr r0, [r7, #0x1c]
	ldr r1, [sp, #0x2c]
	add r0, r0, r1
	str r0, [r7, #0x1c]
	ldr r0, [sp, #0x20]
	cmp r0, #0
	bne _0801DC28
	mov r2, r8
	ldr r0, [r2, #4]
	cmp r0, #0
	beq _0801DC32
_0801DC28:
	mov r1, sb
	ldrb r0, [r1]
	cmp r0, #0
	bne _0801DC32
	b _0801DAFC
_0801DC32:
	ldr r2, [sp, #0x30]
	ldrb r0, [r2]
	mov r2, sb
	ldrb r1, [r2]
	orr r0, r1
	ldr r1, [sp, #0x30]
	strb r0, [r1]
	ldr r0, [r7, #4]
	ldr r1, [r7, #0x18]
	add r0, r0, r1
	str r0, [r7, #4]
	ldr r0, [r7, #8]
	ldr r1, [r7, #0x1c]
	add r0, r0, r1
	str r0, [r7, #8]
	add r0, r3, #0
	add sp, #0x34
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
	thumb_func_global sub_801DC64
sub_801DC64: @ 0x0801DC64
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	ldr r1, _0801DCB4
	ldr r0, [r1, #0x20]
	mov r8, r0
	ldr r4, [r1, #0x24]
	cmp r4, #0
	bne _0801DC78
	b _0801DD96
_0801DC78:
	mov r0, #0
	str r0, [r1, #0x24]
	add r7, r1, #0
	ldr r5, _0801DCB8
_0801DC80:
	ldr r0, _0801DCBC
	add r1, r7, #0
	mov r2, #0x58
	bl memcpy
	ldr r1, [r5]
	ldr r0, _0801DCC0
	mov r2, #0x54
	bl memcpy
	cmp r4, #0
	ble _0801DCC4
	add r1, r4, #0
	mov r2, #0x80
	lsl r2, r2, #1
	cmp r4, r2
	ble _0801DCA4
	add r1, r2, #0
_0801DCA4:
	ldr r0, [r7, #0x20]
	add r0, r0, r1
	str r0, [r7, #0x20]
	add r0, r4, #0
	cmp r4, r2
	ble _0801DCDC
	b _0801DCDA
	.align 2, 0
_0801DCB4: .4byte 0x03004810
_0801DCB8: .4byte 0x03004750
_0801DCBC: .4byte 0x03001D78
_0801DCC0: .4byte 0x03001DD0
_0801DCC4:
	add r1, r4, #0
	ldr r2, _0801DD2C
	cmp r4, r2
	bge _0801DCCE
	add r1, r2, #0
_0801DCCE:
	ldr r0, [r7, #0x20]
	add r0, r0, r1
	str r0, [r7, #0x20]
	add r0, r4, #0
	cmp r4, r2
	bge _0801DCDC
_0801DCDA:
	add r0, r2, #0
_0801DCDC:
	sub r4, r4, r0
	bl sub_801E310
	bl sub_8018B34
	ldr r0, [r5]
	ldr r6, _0801DD30
	add r1, r5, #0
	add r1, #0xb8
	ldr r2, [r6, #8]
	ldr r1, [r1]
	sub r2, r2, r1
	ldr r1, [r0, #4]
	sub r2, r2, r1
	str r2, [r0, #0x18]
	add r1, r5, #0
	add r1, #0xbc
	ldr r2, [r6, #0xc]
	ldr r1, [r1]
	sub r2, r2, r1
	ldr r1, [r0, #8]
	sub r2, r2, r1
	str r2, [r0, #0x1c]
	bl sub_801DA7C
	ldr r1, [r5]
	add r0, r1, #0
	add r0, #0x51
	ldrb r2, [r0]
	cmp r2, #0
	beq _0801DD68
	mov r0, #3
	and r0, r2
	cmp r0, #2
	bne _0801DD34
	add r1, #0x4d
	mov r0, #0
	strb r0, [r1]
	mov r0, #1
	b _0801DD98
	.align 2, 0
_0801DD2C: .4byte 0xFFFFFF00
_0801DD30: .4byte 0x03004810
_0801DD34:
	add r0, r6, #0
	ldr r1, _0801DD60
	mov r2, #0x58
	bl memcpy
	ldr r0, _0801DD64
	add r0, #0x4e
	ldrb r0, [r0]
	cmp r0, #1
	bne _0801DD50
	mov r4, #1
_0801DD4A:
	add r4, #1
	cmp r4, #0
	bne _0801DD4A
_0801DD50:
	ldr r0, [r5]
	ldr r1, _0801DD64
	mov r2, #0x54
	bl memcpy
	bl sub_8018B34
	b _0801DD6C
	.align 2, 0
_0801DD60: .4byte 0x03001D78
_0801DD64: .4byte 0x03001DD0
_0801DD68:
	cmp r4, #0
	bne _0801DC80
_0801DD6C:
	ldr r5, _0801DDA4
	add r1, r5, #0
	add r1, #0x49
	ldrb r0, [r1]
	cmp r0, #0
	bne _0801DD82
	mov r0, #8
	strb r0, [r1]
	mov r0, #0xf7
	bl playSong
_0801DD82:
	ldr r4, [r5, #0x18]
	ldr r1, [r5, #0x20]
	mov r0, r8
	bl sub_800F2A0
	add r1, r0, #0
	add r0, r4, #0
	bl multiply_fixed8
	str r0, [r5, #0x18]
_0801DD96:
	mov r0, #0
_0801DD98:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
_0801DDA4: .4byte 0x03004810
	thumb_func_global sub_801DDA8
sub_801DDA8: @ 0x0801DDA8
	push {r4, lr}
	lsl r0, r0, #0x18
	lsr r3, r0, #0x18
	ldr r0, _0801DDC8
	add r2, r0, #0
	add r2, #0x29
	ldrb r2, [r2]
	add r4, r0, #0
	cmp r2, #1
	beq _0801DDE2
	cmp r2, #2
	bne _0801DDCC
	mov r0, #0x80
	lsl r0, r0, #1
	b _0801DDE4
	.align 2, 0
_0801DDC8: .4byte 0x03004810
_0801DDCC:
	mov r0, #4
	and r0, r3
	cmp r0, #0
	beq _0801DDDA
	mov r0, #0x80
	lsl r0, r0, #1
	b _0801DDE4
_0801DDDA:
	mov r0, #8
	and r0, r3
	cmp r0, #0
	beq _0801DDE6
_0801DDE2:
	ldr r0, _0801DDFC
_0801DDE4:
	str r0, [r1]
_0801DDE6:
	ldr r0, [r4, #0x10]
	asr r0, r0, #8
	lsl r0, r0, #0x18
	lsr r0, r0, #0x1d
	cmp r0, #7
	bhi def_801DDFA
	lsl r0, r0, #2
	ldr r1, _0801DE00
	add r0, r0, r1
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
_0801DDFC: .4byte 0xFFFFFF00
_0801DE00: .4byte _0801DE04
_0801DE04: @ jump table
	.4byte _0801DE24 @ case 0
	.4byte _0801DE38 @ case 1
	.4byte _0801DE44 @ case 2
	.4byte _0801DE44 @ case 3
	.4byte _0801DE50 @ case 4
	.4byte _0801DE50 @ case 5
	.4byte _0801DE5C @ case 6
	.4byte _0801DE8C @ case 7
_0801DE24:
	mov r0, #0xc
	and r0, r3
	cmp r0, #0
	beq def_801DDFA
	ldr r0, [r4, #0x18]
	mov r1, #0x80
	lsl r1, r1, #1
	cmp r0, r1
	bgt _0801DEAA
	b _0801DEA4
_0801DE38:
	mov r0, #5
	and r0, r3
	cmp r0, #0
	bne _0801DE64
	mov r0, #8
	b _0801DE6E
_0801DE44:
	mov r0, #4
	and r0, r3
	cmp r0, #0
	bne _0801DE64
	mov r0, #9
	b _0801DE6E
_0801DE50:
	mov r0, #8
	and r0, r3
	cmp r0, #0
	bne _0801DE64
	mov r0, #5
	b _0801DE6E
_0801DE5C:
	mov r0, #9
	and r0, r3
	cmp r0, #0
	beq _0801DE6C
_0801DE64:
	mov r0, #0x40
	bl sub_801D748
	b def_801DDFA
_0801DE6C:
	mov r0, #4
_0801DE6E:
	and r0, r3
	cmp r0, #0
	beq def_801DDFA
	mov r0, #0x80
	lsl r0, r0, #3
	bl sub_801D71C
	ldr r0, _0801DE88
	add r0, #0xb4
	mov r1, #0xa
	strb r1, [r0]
	b def_801DDFA
	.align 2, 0
_0801DE88: .4byte 0x03004750
_0801DE8C:
	mov r0, #0xc
	and r0, r3
	cmp r0, #0
	beq def_801DDFA
	add r2, r4, #0
	ldr r0, [r2, #0x18]
	mov r1, #0x80
	lsl r1, r1, #1
	cmp r0, r1
	ble _0801DEA4
	str r1, [r2, #0x18]
	b def_801DDFA
_0801DEA4:
	ldr r1, _0801DEB4
	cmp r0, r1
	bge def_801DDFA
_0801DEAA:
	str r1, [r4, #0x18]
def_801DDFA:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801DEB4: .4byte 0xFFFFFF00
	thumb_func_global sub_801DEB8
sub_801DEB8: @ 0x0801DEB8
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x1c
	mov sl, r0
	str r1, [sp, #0x14]
	add r0, r2, #0
	mov r1, #0
	mov r8, r1
	mov r4, r8
	ldr r2, [sp, #0x14]
	strb r4, [r2]
	mov r1, sl
	strb r4, [r1]
	bl sub_801D880
	ldr r0, _0801DEEC
	ldr r1, [r0, #0x18]
	str r1, [sp, #0x10]
	ldr r2, [r0, #0x10]
	str r2, [sp, #0x18]
	mov sb, r0
	ldr r6, _0801DEF0
	b _0801DEFC
	.align 2, 0
_0801DEEC: .4byte 0x03004810
_0801DEF0: .4byte 0x03004750
_0801DEF4:
	ldr r0, [sp, #0x10]
	cmp r0, #0
	bne _0801DEFC
	b _0801E054
_0801DEFC:
	ldr r0, _0801DF34
	mov r1, sb
	mov r2, #0x58
	bl memcpy
	ldr r1, [r6]
	ldr r0, _0801DF38
	mov r2, #0x54
	bl memcpy
	ldr r2, [sp, #0x10]
	cmp r2, #0
	blt _0801DF3C
	add r1, r2, #0
	mov r3, #0x80
	lsl r3, r3, #1
	cmp r2, r3
	ble _0801DF22
	add r1, r3, #0
_0801DF22:
	mov r4, sb
	ldr r0, [r4, #0x10]
	add r0, r0, r1
	str r0, [r4, #0x10]
	add r0, r2, #0
	add r1, r0, #0
	cmp r1, r3
	ble _0801DF58
	b _0801DF56
	.align 2, 0
_0801DF34: .4byte 0x03001D78
_0801DF38: .4byte 0x03001DD0
_0801DF3C:
	add r1, r2, #0
	ldr r3, _0801E01C
	cmp r1, r3
	bge _0801DF46
	add r1, r3, #0
_0801DF46:
	mov r4, sb
	ldr r0, [r4, #0x10]
	add r0, r0, r1
	str r0, [r4, #0x10]
	add r0, r2, #0
	add r1, r0, #0
	cmp r0, r3
	bge _0801DF58
_0801DF56:
	add r0, r3, #0
_0801DF58:
	sub r0, r1, r0
	str r0, [sp, #0x10]
	bl sub_801E310
	bl sub_8018B34
	ldr r0, [r6]
	ldr r5, _0801E020
	add r1, r6, #0
	add r1, #0xb8
	ldr r2, [r5, #8]
	ldr r1, [r1]
	sub r2, r2, r1
	ldr r1, [r0, #4]
	sub r2, r2, r1
	str r2, [r0, #0x18]
	add r1, r6, #0
	add r1, #0xbc
	ldr r2, [r5, #0xc]
	ldr r1, [r1]
	sub r2, r2, r1
	ldr r1, [r0, #8]
	sub r2, r2, r1
	str r2, [r0, #0x1c]
	bl sub_801DA7C
	add r7, r0, #0
	ldr r2, [r6]
	add r0, r2, #0
	add r0, #0x51
	ldrb r4, [r0]
	cmp r4, #0
	beq _0801DEF4
	mov r0, sl
	ldrb r1, [r0]
	add r0, r4, #0
	orr r0, r1
	mov r1, sl
	strb r0, [r1]
	mov r1, #3
	add r0, r4, #0
	and r0, r1
	cmp r0, #2
	bne _0801DFB2
	b _0801E0A8
_0801DFB2:
	mov r0, #1
	ldr r2, [sp, #0x14]
	strb r0, [r2]
	add r0, r5, #0
	ldr r1, _0801E024
	mov r2, #0x58
	bl memcpy
	ldr r0, _0801E028
	add r0, #0x4e
	ldrb r0, [r0]
	cmp r0, #1
	bne _0801DFD8
	mov r4, #1
_0801DFCE:
	add r0, r4, #1
	lsl r0, r0, #0x18
	lsr r4, r0, #0x18
	cmp r4, #0
	bne _0801DFCE
_0801DFD8:
	ldr r5, _0801E02C
	ldr r0, [r5]
	ldr r1, _0801E028
	mov r2, #0x54
	bl memcpy
	bl sub_8018B34
	mov r0, r8
	cmp r0, #0
	bne _0801E04C
	mov r8, r4
	mov r0, r8
	add r1, sp, #0x10
	bl sub_801DDA8
	cmp r7, #0
	bne _0801DFFE
	b _0801DEF4
_0801DFFE:
	ldr r1, _0801E030
	ldrb r0, [r1]
	cmp r0, #1
	beq _0801E040
	cmp r0, #2
	beq _0801E014
	mov r0, #4
	mov r2, r8
	and r0, r2
	cmp r0, #0
	beq _0801E034
_0801E014:
	mov r0, #0x80
	lsl r0, r0, #2
	str r0, [sp, #0x10]
	b _0801DEF4
	.align 2, 0
_0801E01C: .4byte 0xFFFFFF00
_0801E020: .4byte 0x03004810
_0801E024: .4byte 0x03001D78
_0801E028: .4byte 0x03001DD0
_0801E02C: .4byte 0x03004750
_0801E030: .4byte 0x03004839
_0801E034:
	mov r0, #8
	mov r4, r8
	and r0, r4
	cmp r0, #0
	bne _0801E040
	b _0801DEF4
_0801E040:
	ldr r0, _0801E048
	str r0, [sp, #0x10]
	b _0801DEF4
	.align 2, 0
_0801E048: .4byte 0xFFFFFE00
_0801E04C:
	ldr r0, [r5]
	add r0, #0x51
	mov r1, r8
	strb r1, [r0]
_0801E054:
	ldr r1, _0801E0A0
	mov r0, sp
	mov r2, #0xe
	bl memcpy
	ldr r1, _0801E0A4
	add r0, r1, #0
	add r0, #0x29
	ldrb r0, [r0]
	cmp r0, #1
	bne _0801E0B4
	ldr r2, [sp, #0x18]
	asr r0, r2, #8
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0xf8
	bls _0801E0F2
	ldr r0, [r1, #0x10]
	asr r0, r0, #8
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #7
	bhi _0801E0F2
	ldr r0, [r1, #0x18]
	asr r0, r0, #8
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0
	beq _0801E0F2
	cmp r0, #6
	bls _0801E094
	mov r0, #6
_0801E094:
	lsl r0, r0, #1
	add r0, sp
	ldrh r0, [r0]
	bl playSong
	b _0801E0F2
	.align 2, 0
_0801E0A0: .4byte dword_81423A8
_0801E0A4: .4byte 0x03004810
_0801E0A8:
	add r1, r2, #0
	add r1, #0x4d
	mov r0, #0
	strb r0, [r1]
	mov r0, #1
	b _0801E0F4
_0801E0B4:
	cmp r0, #2
	bne _0801E0F2
	ldr r4, [sp, #0x18]
	asr r0, r4, #8
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #7
	bhi _0801E0F2
	ldr r0, [r1, #0x10]
	asr r0, r0, #8
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0xf8
	bls _0801E0F2
	ldr r1, [r1, #0x18]
	asr r0, r1, #8
	lsl r0, r0, #0x18
	cmp r0, #0
	beq _0801E0F2
	neg r0, r1
	asr r0, r0, #8
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #6
	bls _0801E0E8
	mov r0, #6
_0801E0E8:
	lsl r0, r0, #1
	add r0, sp
	ldrh r0, [r0]
	bl playSong
_0801E0F2:
	mov r0, #0
_0801E0F4:
	add sp, #0x1c
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	thumb_func_global sub_801E104
sub_801E104: @ 0x0801E104
	push {r4, r5, r6, lr}
	sub sp, #4
	ldr r4, _0801E190
	add r1, r4, #0
	add r1, #0x49
	ldrb r0, [r1]
	cmp r0, #0
	beq _0801E118
	sub r0, #1
	strb r0, [r1]
_0801E118:
	ldr r5, [r4, #0x24]
	bl sub_801DC64
	cmp r0, #0
	beq _0801E124
	b _0801E22C
_0801E124:
	ldr r0, [r4, #0x10]
	str r0, [r4, #0x14]
	mov r6, sp
	add r6, #1
	mov r0, sp
	add r1, r6, #0
	add r2, r5, #0
	bl sub_801DEB8
	cmp r0, #0
	bne _0801E22C
	ldr r1, [r4, #0x10]
	asr r1, r1, #8
	lsl r1, r1, #0x18
	ldr r0, [r4, #0x14]
	asr r0, r0, #8
	lsl r0, r0, #0x18
	lsr r2, r1, #0x18
	cmp r1, r0
	bne _0801E224
	ldrb r0, [r6]
	cmp r0, #0
	beq _0801E224
	add r0, r2, #0
	sub r0, #0x41
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0x7e
	bhi _0801E1C8
	ldr r0, _0801E194
	ldrh r1, [r0, #0xe]
	mov r0, #0xf8
	lsl r0, r0, #1
	and r0, r1
	cmp r0, #0
	bne _0801E22C
	ldrh r0, [r4, #0x2c]
	add r0, #1
	strh r0, [r4, #0x2c]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0xc
	bls _0801E22C
	add r0, r4, #0
	add r0, #0x29
	ldrb r0, [r0]
	cmp r0, #1
	beq _0801E19E
	cmp r0, #1
	bgt _0801E198
	cmp r0, #0
	beq _0801E216
	b _0801E22C
	.align 2, 0
_0801E190: .4byte 0x03004810
_0801E194: .4byte 0x03002080
_0801E198:
	cmp r0, #2
	beq _0801E1B4
	b _0801E22C
_0801E19E:
	add r0, r2, #0
	add r0, #0x7f
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0x1e
	bhi _0801E216
	ldr r0, _0801E1B0
	str r0, [r4, #0x18]
	b _0801E22C
	.align 2, 0
_0801E1B0: .4byte 0xFFFFFD00
_0801E1B4:
	add r0, r2, #0
	sub r0, #0x61
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0x1e
	bhi _0801E216
	mov r0, #0xc0
	lsl r0, r0, #3
	str r0, [r4, #0x18]
	b _0801E22C
_0801E1C8:
	add r0, r2, #0
	sub r0, #0x30
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0xa0
	bls _0801E224
	ldrh r2, [r4, #0x2e]
	cmp r2, #1
	bhi _0801E1EC
	mov r0, sp
	ldrb r1, [r0]
	mov r0, #0xc
	and r0, r1
	cmp r0, #0
	beq _0801E1EC
	add r0, r2, #1
	strh r0, [r4, #0x2e]
	b _0801E22C
_0801E1EC:
	ldr r0, _0801E21C
	ldrh r1, [r0, #0xe]
	mov r0, #0xf8
	lsl r0, r0, #1
	and r0, r1
	cmp r0, #0
	bne _0801E22C
	mov r0, sp
	ldrb r1, [r0]
	mov r0, #0xc
	and r0, r1
	cmp r0, #0
	bne _0801E22C
	ldr r1, _0801E220
	ldrh r0, [r1, #0x2c]
	add r0, #1
	strh r0, [r1, #0x2c]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0x3c
	bls _0801E22C
_0801E216:
	bl grapplingHookDeath
	b _0801E22C
	.align 2, 0
_0801E21C: .4byte 0x03002080
_0801E220: .4byte 0x03004810
_0801E224:
	ldr r1, _0801E234
	mov r0, #0
	strh r0, [r1, #0x2c]
	strh r0, [r1, #0x2e]
_0801E22C:
	add sp, #4
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_0801E234: .4byte 0x03004810
	thumb_func_global sub_801E238
sub_801E238: @ 0x0801E238
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #4
	ldr r1, _0801E2FC
	mov r2, #0
	mov r3, #0xb8
	lsl r3, r3, #1
	add r0, r1, r3
_0801E24E:
	strb r2, [r0, #0xc]
	sub r0, #0x10
	cmp r0, r1
	bge _0801E24E
	ldr r4, _0801E300
	mov r5, sp
	add r0, r4, #0
	mov r1, sp
	bl copyModifiedByCamera
	ldr r0, [r4, #0x20]
	lsl r0, r0, #8
	asr r0, r0, #0x10
	cmp r0, #0
	bge _0801E26E
	add r0, #3
_0801E26E:
	asr r7, r0, #2
	ldr r0, [r4, #0x10]
	asr r0, r0, #8
	lsl r0, r0, #0x18
	lsr r0, r0, #0x19
	mov sb, r0
	mov r0, sp
	mov r1, #0
	ldrsh r0, [r0, r1]
	sub r0, #3
	mov r8, r0
	mov r2, #2
	ldrsh r0, [r5, r2]
	sub r0, #3
	mov ip, r0
	mov r6, #0
	cmp r6, r7
	bge _0801E2E2
	ldr r3, _0801E304
	mov sl, r3
	ldr r5, _0801E2FC
_0801E298:
	lsl r3, r6, #2
	mov r1, sb
	lsl r0, r1, #7
	add r3, r3, r0
	add r3, sl
	mov r2, #0
	ldrsh r4, [r3, r2]
	add r4, r8
	ldr r1, _0801E308
	add r0, r1, #0
	add r1, r4, #0
	and r1, r0
	ldrh r2, [r5, #2]
	ldr r0, _0801E30C
	and r0, r2
	orr r0, r1
	strh r0, [r5, #2]
	mov r2, #2
	ldrsh r0, [r3, r2]
	mov r3, ip
	add r1, r3, r0
	strb r1, [r5]
	add r4, #0xf
	mov r0, #0x87
	lsl r0, r0, #1
	cmp r4, r0
	bhi _0801E2DA
	add r0, r1, #0
	add r0, #0xf
	cmp r0, #0xbe
	bhi _0801E2DA
	mov r0, #1
	strb r0, [r5, #0xc]
_0801E2DA:
	add r5, #0x10
	add r6, #1
	cmp r6, r7
	blt _0801E298
_0801E2E2:
	sub r1, r6, #1
	ldr r0, _0801E300
	add r0, #0x46
	strh r1, [r0]
	add sp, #4
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_0801E2FC: .4byte 0x030037D0
_0801E300: .4byte 0x03004810
_0801E304: .4byte dword_813D314
_0801E308: .4byte 0x000001FF
_0801E30C: .4byte 0xFFFFFE00
	thumb_func_global sub_801E310
sub_801E310: @ 0x0801E310
	push {r4, r5, lr}
	ldr r4, _0801E354
	ldr r2, [r4, #0x10]
	asr r2, r2, #8
	add r2, #0xc0
	lsl r2, r2, #0x18
	lsr r2, r2, #0x18
	ldr r3, _0801E358
	add r0, r2, #0
	add r0, #0x40
	lsl r0, r0, #1
	add r0, r0, r3
	mov r5, #0
	ldrsh r1, [r0, r5]
	lsl r2, r2, #1
	add r2, r2, r3
	mov r0, #0
	ldrsh r5, [r2, r0]
	ldr r0, [r4, #0x20]
	bl multiply_fixed8
	ldr r1, [r4]
	add r1, r1, r0
	str r1, [r4, #8]
	ldr r0, [r4, #0x20]
	add r1, r5, #0
	bl multiply_fixed8
	ldr r1, [r4, #4]
	add r1, r1, r0
	str r1, [r4, #0xc]
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_0801E354: .4byte 0x03004810
_0801E358: .4byte dword_80382C8




	thumb_func_global sub_801E35C
sub_801E35C: @ 0x0801E35C
	ldr r1, _0801E368
	ldr r2, [r1, #0xc]
	ldr r1, [r1, #8]
	str r1, [r0]
	str r2, [r0, #4]
	bx lr
	.align 2, 0
_0801E368: .4byte 0x03004810






thumb_func_global sub_0801E36C
sub_0801E36C: @ 0x0801E36C
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	ldr r4, _0801E3FC
	ldr r0, [r4, #0x20]
	mov r8, r0
	ldr r0, [r4, #0x18]
	mov r1, #0x80
	lsl r1, r1, #1
	bl multiply_fixed8
	add r6, r0, #0
	ldr r1, [r4, #0x10]
	asr r1, r1, #8
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	ldr r2, _0801E400
	add r0, r1, #0
	add r0, #0x40
	lsl r0, r0, #1
	add r0, r0, r2
	mov r3, #0
	ldrsh r0, [r0, r3]
	lsl r1, r1, #1
	add r1, r1, r2
	mov r2, #0
	ldrsh r5, [r1, r2]
	asr r1, r6, #1
	bl multiply_fixed8
	add r4, r0, #0
	add r0, r5, #0
	add r1, r6, #0
	bl multiply_fixed8
	mov r1, #0xb0
	bl multiply_fixed8
	add r5, r0, #0
	mov r3, r8
	asr r3, r3, #0xd
	mov r8, r3
	ldr r6, _0801E404
	ldr r0, [r6]
	bl clearObjForces
	add r1, r6, #0
	add r1, #0xac
	mov r0, r8
	asr r7, r0, #2
	add r0, r7, #0
	mul r0, r4, r0
	add r4, r4, r0
	str r4, [r1]
	mov r1, #0xc0
	lsl r1, r1, #1
	add r0, r5, #0
	bl multiply_fixed8
	add r5, r0, #0
	cmp r5, #0
	bge _0801E408
	ldr r0, [r6]
	add r1, r7, #0
	mul r1, r5, r1
	mov r2, #0x8c
	lsl r2, r2, #1
	add r1, r1, r2
	add r1, r5, r1
	bl setObjVelocity_Y
	b _0801E41C
	.align 2, 0
_0801E3FC: .4byte 0x03004810
_0801E400: .4byte dword_80382C8
_0801E404: .4byte 0x03004750
_0801E408:
	ldr r0, [r6]
	mov r3, r8
	asr r1, r3, #1
	mul r1, r5, r1
	mov r2, #0x8c
	lsl r2, r2, #1
	add r1, r1, r2
	add r1, r5, r1
	bl setObjVelocity_Y
_0801E41C:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0





.align 2, 0
	thumb_func_global sub_801E428
sub_801E428: @ 0x0801E428
	push {r4, r5, r6, lr}
	sub sp, #0x1c
	ldr r5, _0801E470
	ldr r0, [r5, #0x20]
	asr r2, r0, #0xc
	mov r1, sp
	ldr r0, _0801E474
	ldm r0!, {r3, r4, r6}
	stm r1!, {r3, r4, r6}
	ldm r0!, {r3, r4, r6}
	stm r1!, {r3, r4, r6}
	ldr r0, [r0]
	str r0, [r1]
	ldr r0, [r5, #0x10]
	asr r0, r0, #8
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0801E480
	ldr r0, _0801E478
	add r0, #0x80
	mov r1, #0
	ldrsh r4, [r0, r1]
	lsl r0, r2, #2
	add r0, sp
	ldr r0, [r0]
	ldr r1, _0801E47C
	ldr r1, [r1]
	ldr r1, [r1, #0x18]
	bl multiply_fixed8
	add r1, r0, #0
	add r0, r4, #0
	bl multiply_fixed8
	b _0801E482
	.align 2, 0
_0801E470: .4byte 0x03004810
_0801E474: .4byte dword_81423B8
_0801E478: .4byte dword_80382C8
_0801E47C: .4byte 0x03004750
_0801E480:
	mov r0, #0
_0801E482:
	str r0, [r5, #0x18]
	add sp, #0x1c
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	thumb_func_global sub_801E48C
sub_801E48C: @ 0x0801E48C
	push {r4, lr}
	sub sp, #4
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r1, _0801E4D0
	mov r0, sp
	str r0, [r1]
	ldr r0, _0801E4D4
	str r0, [r1, #4]
	ldr r0, _0801E4D8
	str r0, [r1, #8]
	ldr r0, [r1, #8]
	ldr r4, _0801E4DC
	mov r0, #0
	strh r0, [r4, #2]
	mov r0, #1
	strh r0, [r4]
	ldr r0, _0801E4E0
	ldr r0, [r0]
	strh r0, [r4, #4]
	mov r0, #0
	bl srand
	ldr r0, _0801E4E4
	mov r1, #1
	bl createTask
	str r0, [r4, #0xc]
	add sp, #4
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801E4D0: .4byte 0x040000D4
_0801E4D4: .4byte 0x020021C0
_0801E4D8: .4byte 0x81003840
_0801E4DC: .4byte 0x03004870
_0801E4E0: .4byte 0x03002080
_0801E4E4: .4byte (sub_801E57C+1)
	thumb_func_global sub_801E4E8
sub_801E4E8: @ 0x0801E4E8
	push {r4, lr}
	ldr r4, _0801E50C
	mov r0, #0
	strh r0, [r4, #2]
	mov r0, #2
	strh r0, [r4]
	mov r0, #0
	bl srand
	ldr r0, _0801E510
	mov r1, #1
	bl createTask
	str r0, [r4, #0xc]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801E50C: .4byte 0x03004870
_0801E510: .4byte (sub_801E5C8+1)
	thumb_func_global sub_801E514
sub_801E514: @ 0x0801E514
	push {lr}
	ldr r0, _0801E528
	mov r1, #3
	strh r1, [r0]
	ldr r0, [r0, #0xc]
	bl endTask
	pop {r0}
	bx r0
	.align 2, 0
_0801E528: .4byte 0x03004870
	thumb_func_global sub_801E52C
sub_801E52C: @ 0x0801E52C
	push {r4, r5, lr}
	ldr r4, _0801E570
	ldrh r0, [r4, #2]
	cmp r0, #0
	beq _0801E568
	ldr r3, _0801E574
	add r2, r0, #0
	sub r0, r2, #1
	lsl r0, r0, #2
	add r0, r0, r3
	ldrh r1, [r0]
	mov r5, #3
	add r0, r5, #0
	and r0, r1
	cmp r0, #3
	beq _0801E568
	cmp r2, #1
	beq _0801E55E
	sub r0, r2, #2
	lsl r0, r0, #2
	add r0, r0, r3
	ldrh r2, [r0]
	ldr r1, _0801E578
	and r1, r2
	strh r1, [r0]
_0801E55E:
	ldrh r0, [r4, #2]
	sub r0, #1
	lsl r0, r0, #2
	add r0, r0, r3
	strh r5, [r0]
_0801E568:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_0801E570: .4byte 0x03004870
_0801E574: .4byte 0x020021C0
_0801E578: .4byte 0x0000FFFC
	thumb_func_global sub_801E57C
sub_801E57C: @ 0x0801E57C
	push {r4, lr}
	ldr r2, _0801E5B8
	ldr r3, _0801E5BC
	ldrh r0, [r3, #2]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r4, _0801E5C0
	ldrh r1, [r4, #0xc]
	strh r1, [r0]
	ldrh r0, [r3, #2]
	lsl r0, r0, #2
	add r2, #2
	add r0, r0, r2
	ldrh r1, [r4, #0xe]
	strh r1, [r0]
	ldrh r0, [r3, #2]
	add r0, #1
	strh r0, [r3, #2]
	lsl r0, r0, #0x10
	mov r1, #0x96
	lsl r1, r1, #0x11
	cmp r0, r1
	bls _0801E5B0
	ldr r0, _0801E5C4
	bl setCurrentTaskFunc
_0801E5B0:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801E5B8: .4byte 0x020021C0
_0801E5BC: .4byte 0x03004870
_0801E5C0: .4byte 0x03002080
_0801E5C4: .4byte (sub_801E61C+1)
	thumb_func_global sub_801E5C8
sub_801E5C8: @ 0x0801E5C8
	push {lr}
	ldr r2, _0801E60C
	ldr r1, _0801E610
	ldrh r0, [r2, #2]
	lsl r0, r0, #2
	add r0, r0, r1
	ldrh r0, [r0]
	strh r0, [r2, #6]
	ldrh r0, [r2, #2]
	lsl r0, r0, #2
	add r1, #2
	add r0, r0, r1
	ldrh r0, [r0]
	strh r0, [r2, #8]
	ldrh r0, [r2, #2]
	add r0, #1
	strh r0, [r2, #2]
	lsl r0, r0, #0x10
	mov r1, #0x96
	lsl r1, r1, #0x11
	cmp r0, r1
	bls _0801E608
	ldr r0, _0801E614
	bl setCurrentTaskFunc
	ldr r0, _0801E618
	mov r1, #0xfa
	lsl r1, r1, #4
	add r0, r0, r1
	ldr r1, [r0]
	mov r0, #0
	str r0, [r1, #0x14]
_0801E608:
	pop {r0}
	bx r0
	.align 2, 0
_0801E60C: .4byte 0x03004870
_0801E610: .4byte 0x020021C0
_0801E614: .4byte (sub_801E61C+1)
_0801E618: .4byte 0x0201AF00
	thumb_func_global sub_801E61C
sub_801E61C: @ 0x0801E61C
	push {lr}
	mov r0, #3
	bl sub_8016A4C
	ldr r0, _0801E64C
	mov r1, #0xfa
	lsl r1, r1, #4
	add r0, r0, r1
	ldr r1, [r0]
	ldr r0, [r1, #0x14]
	add r2, r0, #0
	add r0, #1
	str r0, [r1, #0x14]
	cmp r2, #0x1d
	bls _0801E646
	ldr r0, _0801E650
	mov r1, #3
	strh r1, [r0]
	ldr r0, _0801E654
	bl setCurrentTaskFunc
_0801E646:
	pop {r0}
	bx r0
	.align 2, 0
_0801E64C: .4byte 0x0201AF00
_0801E650: .4byte 0x03004870
_0801E654: .4byte (sub_801E658+1)
	thumb_func_global sub_801E658
sub_801E658: @ 0x0801E658
	push {lr}
	ldr r2, _0801E694
	ldrh r1, [r2, #0xc]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _0801E66C
	ldr r1, _0801E698
	mov r0, #0
	strh r0, [r1]
_0801E66C:
	ldrh r1, [r2, #0xc]
	mov r0, #3
	and r0, r1
	cmp r0, #0
	beq _0801E68A
	bl endCurrentTask
	mov r0, #2
	bl endPriorityOrHigherTask
	ldr r0, _0801E69C
	ldr r0, [r0]
	ldr r1, _0801E6A0
	bl setTaskFunc
_0801E68A:
	mov r0, #3
	bl sub_8016A4C
	pop {r0}
	bx r0
	.align 2, 0
_0801E694: .4byte 0x03002080
_0801E698: .4byte 0x03004870
_0801E69C: .4byte 0x03003E20
_0801E6A0: .4byte (cb_startLevelLoad+1)






thumb_func_global sub_0801E6A4
sub_0801E6A4: @ 0x0801E6A4
	ldr r3, _0801E6C4
	ldrb r1, [r3]
	add r2, r1, #1
	add r0, r2, #0
	asr r0, r0, #7
	lsl r0, r0, #7
	sub r0, r2, r0
	strb r0, [r3]
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0801E6BE
	mov r0, #1
	strb r0, [r3]
_0801E6BE:
	ldrb r0, [r3]
	bx lr
	.align 2, 0
_0801E6C4: .4byte 0x03001E2A

thumb_func_global sub_0801E6C8
sub_0801E6C8: @ 0x0801E6C8
	push {lr}
	bl sub_0801E6A4
	mov r1, #0x80
	orr r0, r1
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	pop {r1}
	bx r1
	.align 2, 0
	
	