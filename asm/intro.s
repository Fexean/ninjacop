.include "asm/macros.inc"

@complete

thumb_func_global clearIntroData
clearIntroData: @ 0x080268B0
	push {r4, lr}
	sub sp, #4
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r4, =0x03001E30
	ldr r2, =0x01000004
	mov r0, sp
	add r1, r4, #0
	bl Bios_memcpy
	mov r0, #3
	strb r0, [r4]
	add sp, #4
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global cb_introStart
cb_introStart: @ 0x080268DC
	push {r4, r5, r6, lr}
	sub sp, #8
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl clearSprites
	bl resetAffineTransformations
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	bl sub_080102B0
	ldr r0, =0x03002080
	ldr r0, [r0]
	bl srand
	ldr r5, =0x03001E30
	ldrb r0, [r5]
	cmp r0, #3
	bne _0802692C
	mov r0, #0
	strb r0, [r5, #3]
	bl rand
	mov r1, #3
	bl Bios_modulo
	strb r0, [r5]
	b _0802698E
	.align 2, 0
.pool
_0802692C:
	mov r4, #0
	mov r1, #0
	add r2, r5, #0
	ldrb r3, [r2, #3]
	mov r5, #1
_08026936:
	add r0, r3, #0
	asr r0, r1
	and r0, r5
	cmp r0, #0
	bne _0802694C
	lsl r0, r4, #1
	add r0, sp
	strh r1, [r0]
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
_0802694C:
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	cmp r1, #2
	bls _08026936
	cmp r4, #0
	bne _0802697A
	strb r4, [r2, #3]
	mov r1, #0
	ldrb r2, [r2]
_08026960:
	cmp r1, r2
	beq _08026970
	lsl r0, r4, #1
	add r0, sp
	strh r1, [r0]
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
_08026970:
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	cmp r1, #2
	bls _08026960
_0802697A:
	bl rand
	add r1, r4, #0
	bl Bios_modulo
	ldr r1, =0x03001E30
	lsl r0, r0, #1
	add r0, sp
	ldrh r0, [r0]
	strb r0, [r1]
_0802698E:
	ldr r4, =0x03001E30
	mov r5, #0
	strb r5, [r4, #1]
	mov r6, #1
	add r0, r6, #0
	ldrb r1, [r4]
	lsl r0, r1
	ldrb r1, [r4, #3]
	orr r0, r1
	strb r0, [r4, #3]
	strb r5, [r4, #2]
	ldr r0, =(cb_intro_switch+1)
	bl setCurrentTaskFunc
	ldr r0, =0x03003D40
	strb r6, [r0, #0x12]
	strh r5, [r4, #4]
	add sp, #8
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global cb_intro_switch
cb_intro_switch: @ 0x080269C4
	push {r4, r5, lr}
	ldr r0, =0x03002080
	ldrh r1, [r0, #0xc]
	mov r0, #8
	and r0, r1
	ldr r5, =0x03001E30
	cmp r0, #0
	beq _080269D8
	mov r0, #0xd
	strb r0, [r5, #2]
_080269D8:
	ldr r1, =introStates
	add r4, r5, #0
	ldrb r0, [r4, #2]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl call_r0
	lsl r0, r0, #0x10
	asr r1, r0, #0x10
	cmp r1, #1
	beq _08026A16
	cmp r1, #1
	bgt _08026A0C
	mov r0, #1
	neg r0, r0
	cmp r1, r0
	beq _08026A32
	b _08026A38
	.align 2, 0
.pool
_08026A0C:
	cmp r1, #2
	beq _08026A1E
	cmp r1, #3
	beq _08026A2C
	b _08026A38
_08026A16:
	ldrb r0, [r4, #2]
	add r0, #1
	strb r0, [r4, #2]
	b _08026A38
_08026A1E:
	ldr r0, =(cb_startLevelLoad_2+1)
	bl setCurrentTaskFunc
	b _08026A38
	.align 2, 0
.pool
_08026A2C:
	mov r0, #0xa
	strb r0, [r5, #2]
	b _08026A38
_08026A32:
	ldr r0, =(cb_loadTitleScreen+1)
	bl setCurrentTaskFunc
_08026A38:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global introState0_loadCity
introState0_loadCity: @ 0x08026A44
	push {r4, r5, r6, lr}
	mov r6, r8
	push {r6}
	sub sp, #4
	bl clearSprites
	bl resetAffineTransformations
	ldr r0, =dword_815209C
	mov r1, #0xa0
	lsl r1, r1, #0x13
	mov r2, #0x10
	bl Bios_memcpy
	ldr r0, =dword_81520BC
	mov r1, #0xc0
	lsl r1, r1, #0x13
	mov r2, #0x10
	bl Bios_memcpy
	ldr r0, =dword_81520DC
	ldr r1, =0x06008000
	mov r2, #0xa0
	lsl r2, r2, #2
	bl Bios_memcpy
	ldr r6, =0x03003D00
	mov r0, #0
	mov r8, r0
	mov r0, #0x80
	lsl r0, r0, #5
	strh r0, [r6]
	ldr r0, =dword_821F5E8
	ldr r2, =0x05000020
	mov r1, #1
	bl memcpy_pal
	ldr r0, =unk_821FA48
	ldr r1, =0x06000020
	bl Bios_LZ77_16_2
	ldr r0, =unk_821F628
	ldr r1, =0x06008800
	mov r5, #1
	str r5, [sp]
	mov r2, #0
	mov r3, #1
	bl LZ77_mapCpy
	ldr r0, =0x00001101
	strh r0, [r6, #2]
	ldr r4, =0x03003CD4
	ldrh r0, [r4]
	mov r2, #0x80
	lsl r2, r2, #2
	add r1, r2, #0
	orr r0, r1
	strh r0, [r4]
	ldr r0, =unk_8220C60
	ldr r1, =0x06001020
	bl Bios_LZ77_16_2
	ldr r0, =unk_8220880
	ldr r1, =0x06009000
	str r5, [sp]
	mov r2, #0
	mov r3, #0x81
	bl LZ77_mapCpy
	ldr r0, =0x00001202
	strh r0, [r6, #4]
	ldrh r0, [r4]
	mov r2, #0x80
	lsl r2, r2, #3
	add r1, r2, #0
	orr r0, r1
	strh r0, [r4]
	ldr r0, =unk_821F4B4
	ldr r1, =0x06001C20
	bl Bios_LZ77_16_2
	ldr r0, =unk_82205F8
	ldr r1, =0x06009800
	str r5, [sp]
	mov r2, #0
	mov r3, #0xe1
	bl LZ77_mapCpy
	ldr r0, =0x00001303
	strh r0, [r6, #6]
	ldrh r0, [r4]
	mov r2, #0x80
	lsl r2, r2, #4
	add r1, r2, #0
	orr r0, r1
	strh r0, [r4]
	ldr r0, =0x03001E38
	mov r1, #3
	bl allocFont
	ldr r0, =0x03001E30
	mov r1, r8
	strh r1, [r0, #4]
	mov r0, #1
	add sp, #4
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global introState1
introState1: @ 0x08026B84
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _08026B9A
	mov r0, #0
	b _08026BA2
_08026B9A:
	mov r0, #1
	bl playSong
	mov r0, #1
_08026BA2:
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global introState2
introState2: @ 0x08026BA8
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	mov r4, #0
	ldr r5, =0x03001E30
	ldr r0, =0x03002308
	mov r8, r0
	ldr r1, =dword_81C0828
	mov ip, r1
	add r6, r5, #0
	mov r7, #0x7f
_08026BBE:
	add r3, r4, #1
	lsl r2, r3, #1
	add r2, r8
	ldrh r1, [r6, #4]
	add r0, r7, #0
	and r0, r1
	lsl r0, r0, #1
	lsl r1, r4, #8
	add r0, r0, r1
	add r0, ip
	ldrh r0, [r0]
	ldrh r1, [r2]
	add r0, r0, r1
	strh r0, [r2]
	lsl r3, r3, #0x10
	lsr r4, r3, #0x10
	cmp r4, #2
	bls _08026BBE
	ldrh r0, [r5, #4]
	add r0, #1
	strh r0, [r5, #4]
	lsl r0, r0, #0x10
	ldr r1, =0x02170000
	cmp r0, r1
	bhi _08026C04
	mov r0, #0
	b _08026C2C
	.align 2, 0
.pool
_08026C04:
	mov r0, #0
	strh r0, [r5, #4]
	ldr r1, =0x03003D94
	ldr r2, =0x00000808
	add r0, r2, #0
	strh r0, [r1]
	ldr r2, =0x03003CD4
	ldrh r0, [r2]
	mov r3, #0x80
	lsl r3, r3, #1
	add r1, r3, #0
	orr r0, r1
	strh r0, [r2]
	ldr r1, =0x03003D74
	ldr r2, =0x00000E41
	add r0, r2, #0
	strh r0, [r1]
	bl loadEmergencyText
	mov r0, #1
_08026C2C:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global introState3
introState3: @ 0x08026C4C
	push {lr}
	bl blinkEmergencyString
	ldr r0, =0x03001E30
	ldrh r0, [r0, #4]
	cmp r0, #0xb9
	bhi _08026C64
	mov r0, #0
	b _08026C6C
	.align 2, 0
.pool
_08026C64:
	ldr r1, =0x03001E40
	mov r0, #0
	strh r0, [r1]
	mov r0, #1
_08026C6C:
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global introState4
introState4: @ 0x08026C74
	push {lr}
	bl blinkEmergencyString
	ldr r1, =0x03001E40
	ldrh r0, [r1]
	add r0, #1
	strh r0, [r1]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0xa
	bne _08026C90
	mov r0, #0xc8
	bl song_play_and_auto_config
_08026C90:
	mov r0, #3
	mov r1, #0x14
	mov r2, #0x78
	mov r3, #0x50
	bl TransitionToBlack
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _08026CAC
	mov r0, #0
	b _08026CC0
	.align 2, 0
.pool
_08026CAC:
	ldr r1, =0x03003D94
	mov r0, #0
	strh r0, [r1]
	bl sub_080102B0
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	mov r0, #1
_08026CC0:
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global blinkEmergencyString
blinkEmergencyString: @ 0x08026CC8
	push {r4, r5, r6, lr}
	ldr r6, =0x03003D94
	ldr r5, =unk_81C0B28
	ldr r4, =0x03001E30
	ldrh r0, [r4, #4]
	mov r1, #6
	bl Bios_modulo
	lsl r0, r0, #1
	add r0, r0, r5
	ldrh r0, [r0]
	strh r0, [r6]
	ldrh r1, [r4, #4]
	mov r0, #4
	and r0, r1
	cmp r0, #0
	beq _08026CFC
	bl showEmergencyString
	b _08026D00
	.align 2, 0
.pool
_08026CFC:
	bl hideEmergencyString
_08026D00:
	ldr r1, =0x03001E30
	ldrh r0, [r1, #4]
	add r0, #1
	strh r0, [r1, #4]
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global introState7_loadScene
introState7_loadScene: @ 0x08026D14
	push {r4, lr}
	bl gfx_related_sub_800F674
	bl sub_0801025C
	ldr r4, =0x03003D40
	mov r0, #0
	strh r0, [r4, #0x18]
	ldr r3, =introStructs
	ldr r2, =0x03001E30
	ldrb r1, [r2]
	lsl r0, r1, #2
	add r0, r0, r1
	ldrb r2, [r2, #1]
	add r0, r0, r2
	lsl r0, r0, #2
	add r0, r0, r3
	ldr r0, [r0]
	str r0, [r4, #0x14]
	mov r0, #2
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global introState8
introState8: @ 0x08026D50
	push {lr}
	bl sub_080102B0
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	bl FreeAllPalFadeTasks
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	bl stopPalFade
	mov r0, #1
	pop {r1}
	bx r1

thumb_func_global introState5_drawLetter
introState5_drawLetter: @ 0x08026D78
	push {r4, r5, r6, lr}
	sub sp, #4
	ldr r6, =0x03003CD4
	mov r1, #0x82
	lsl r1, r1, #5
	add r0, r1, #0
	strh r0, [r6]
	bl clearSprites
	bl resetAffineTransformations
	ldr r1, =introNinjaStrPalettes
	ldr r4, =0x03001E30
	ldrb r0, [r4, #1]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r2, #0xa0
	lsl r2, r2, #0x13
	mov r1, #1
	bl memcpy_pal
	ldr r1, =introNinjaTiles
	ldrb r0, [r4, #1]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0xd2
	lsl r1, r1, #6
	mov r2, #0xc0
	lsl r2, r2, #0x13
	bl Bios__memcpy_bytecount
	ldr r1, =introNinjaMaps
	ldrb r0, [r4, #1]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x06008000
	mov r5, #0
	str r5, [sp]
	mov r2, #0
	mov r3, #0
	bl tilemapCpy
	ldr r1, =0x03003D00
	mov r0, #0x80
	lsl r0, r0, #5
	strh r0, [r1]
	ldrh r0, [r6]
	mov r2, #0x80
	lsl r2, r2, #1
	add r1, r2, #0
	orr r0, r1
	strh r0, [r6]
	bl gfx_related_sub_800F674
	strh r5, [r4, #4]
	mov r0, #1
	add sp, #4
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global introState6
introState6: @ 0x08026E14
	push {lr}
	ldr r2, =0x03001E30
	ldrh r1, [r2, #4]
	add r1, #1
	strh r1, [r2, #4]
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	cmp r1, #0xf
	bls _08026E3E
	mov r0, #0
	strh r0, [r2, #4]
	ldr r2, =0x03003CD4
	ldrh r1, [r2]
	ldr r0, =0x0000FEFF
	and r0, r1
	strh r0, [r2]
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	mov r0, #1
_08026E3E:
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global introState9
introState9: @ 0x08026E50
	push {r4, lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl clearSprites
	bl resetAffineTransformations
	ldr r1, =introNinjaStrPalettes
	ldr r4, =0x03001E30
	ldrb r0, [r4, #1]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r2, #0xa0
	lsl r2, r2, #0x13
	mov r1, #1
	bl memcpy_pal
	ldr r1, =introNinjaTiles
	ldrb r0, [r4, #1]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0xd2
	lsl r1, r1, #6
	mov r2, #0xc0
	lsl r2, r2, #0x13
	bl Bios__memcpy_bytecount
	ldr r1, =0x03003D00
	mov r2, #0
	mov r0, #0x80
	lsl r0, r0, #5
	strh r0, [r1]
	strh r2, [r4, #4]
	mov r0, #1
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global introState10_loadLetterMap
introState10_loadLetterMap: @ 0x08026EB8
	push {r4, r5, lr}
	sub sp, #4
	ldr r1, =introNinjaMaps
	ldr r5, =0x03001E30
	ldrb r0, [r5, #1]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x06008000
	mov r4, #0
	str r4, [sp]
	mov r2, #0
	mov r3, #0
	bl tilemapCpy
	ldr r1, =0x03003D00
	mov r0, #0x80
	lsl r0, r0, #5
	strh r0, [r1]
	ldr r2, =0x03003CD4
	ldrh r0, [r2]
	mov r3, #0x80
	lsl r3, r3, #1
	add r1, r3, #0
	orr r0, r1
	strh r0, [r2]
	bl gfx_related_sub_800F674
	strh r4, [r5, #4]
	mov r0, #1
	add sp, #4
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global introState11
introState11: @ 0x08026F10
	push {r4, lr}
	ldr r4, =0x03001E30
	ldrh r0, [r4, #4]
	add r0, #1
	strh r0, [r4, #4]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #6
	bls _08026F44
	mov r0, #0
	strh r0, [r4, #4]
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldrb r0, [r4, #1]
	add r0, #1
	cmp r0, #4
	bgt _08026F40
	strb r0, [r4, #1]
	mov r0, #3
	b _08026F46
	.align 2, 0
.pool
_08026F40:
	mov r0, #1
	b _08026F46
_08026F44:
	mov r0, #0
_08026F46:
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global introState12
introState12: @ 0x08026F4C
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl resetAffineTransformations
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r1, =0x03003D40
	mov r0, #1
	strb r0, [r1, #0x12]
	sub r0, #2
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global introState13
introState13: @ 0x08026F78
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl resetAffineTransformations
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r1, =0x03003D40
	mov r0, #0
	strb r0, [r1, #0x12]
	sub r0, #1
	pop {r1}
	bx r1
	.align 2, 0
.pool



thumb_func_global sub_8026FA4
sub_8026FA4:
	push {lr}
	ldr r0, =0x03003E20
	ldr r0, [r0]
	ldr r1, =(cb_intro_switch+1)
	bl setTaskFunc
	mov r0, #2
	bl endPriorityOrHigherTask
	ldr r1, =0x03001E30
	ldrb r0, [r1, #1]
	add r0, #1
	cmp r0, #4
	bgt _08026FD4
	strb r0, [r1, #1]
	mov r0, #5
	b _08026FDA
	.align 2, 0
.pool
_08026FD4:
	mov r0, #0
	strb r0, [r1, #1]
	mov r0, #9
_08026FDA:
	strb r0, [r1, #2]
	pop {r0}
	bx r0



thumb_func_global getIntroSceneNum
getIntroSceneNum: @ 0x08026FE0
	ldr r0, =0x03001E30
	ldrb r0, [r0, #1]
	bx lr
	.align 2, 0
.pool



thumb_func_global loadEmergencyText
loadEmergencyText: @ 0x08026FEC
	push {r4, r5, lr}
	sub sp, #4
	ldr r1, =emergency_string
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r4, [r0]
	ldr r5, =0x03001E38
	mov r0, #0
	str r0, [sp]
	add r0, r4, #0
	add r1, r5, #0
	mov r2, #0
	mov r3, #0
	bl bufferString
	add r0, r4, #0
	mov r1, #0
	bl str_get_last_width_OamX_sum
	mov r1, #0xf0
	sub r1, r1, r0
	lsl r1, r1, #0xf
	lsr r1, r1, #0x10
	str r5, [sp]
	add r0, r4, #0
	mov r2, #0x46
	mov r3, #0
	bl setStringPos
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global showEmergencyString
showEmergencyString: @ 0x08027040
	push {lr}
	ldr r1, =emergency_string
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0
	bl showString
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global hideEmergencyString
hideEmergencyString: @ 0x08027064
	push {lr}
	ldr r1, =emergency_string
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0
	bl hideString
	pop {r0}
	bx r0
	.align 2, 0
.pool
