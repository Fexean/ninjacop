.include "asm/macros.inc"

@completely disassembled


thumb_func_global multiplyUnsignedHigh
multiplyUnsignedHigh: @ 0x0803581C
	add r2, pc, #0x0
	bx r2
	.arm
	umull r2, r3, r0, r1
	add r0, r3, #0
	bx lr


thumb_func_global SoundMain
SoundMain: @ 0x0803582C
	ldr r0, _08035898
	ldr r0, [r0]
	ldr r2, _0803589C
	ldr r3, [r0]
	cmp r2, r3
	beq _0803583A
	bx lr
_0803583A:
	add r3, #1
	str r3, [r0]
	push {r4, r5, r6, r7, lr}
	mov r1, r8
	mov r2, sb
	mov r3, sl
	mov r4, fp
	push {r0, r1, r2, r3, r4}
	sub sp, #0x18
	ldrb r1, [r0, #0xc]
	cmp r1, #0
	beq _0803585E
	ldr r2, _080358A4
	ldrb r2, [r2]
	cmp r2, #0xa0
	bhs _0803585C
	add r2, #0xe4
_0803585C:
	add r1, r1, r2
_0803585E:
	str r1, [sp, #0x14]
	ldr r3, [r0, #0x20]
	cmp r3, #0
	beq _0803586E
	ldr r0, [r0, #0x24]
	bl call_r3_2
	ldr r0, [sp, #0x18]
_0803586E:
	ldr r3, [r0, #0x28]
	bl call_r3_2
	ldr r0, [sp, #0x18]
	ldr r3, [r0, #0x10]
	mov r8, r3
	ldr r5, _080358A8
	add r5, r5, r0
	ldrb r4, [r0, #4]
	sub r7, r4, #1
	bls _0803588E
	ldrb r1, [r0, #0xb]
	sub r1, r1, r7
	mov r2, r8
	mul r2, r1, r2
	add r5, r5, r2
_0803588E:
	str r5, [sp, #8]
	ldr r6, _080358AC
	ldr r3, _080358A0
	bx r3
	.align 2, 0
_08035898: .4byte 0x03007FF0
_0803589C: .4byte 0x68736D53
_080358A0: .4byte 0x03006C39
_080358A4: .4byte 0x04000006
_080358A8: .4byte 0x00000350
_080358AC: .4byte 0x00000630

thumb_func_global SoundMainRAM
SoundMainRAM: @ 0x080358B0
	ldrb r3, [r0, #5]
	cmp r3, #0
	beq _08035910
	adr r1, loc_80358BC
	bx r1
	.align 2, 0
	.arm
loc_80358BC:
	cmp r4, #2
	addeq r7, r0, #0x350
	addne r7, r5, r8
	mov r4, r8
	
loc_80358CC:
	ldrsb r0, [r5, r6]
	ldrsb r1,[r5]
	add r0, r1
	ldrsb r1, [r7,r6]
	add r0, r1
	ldrsb r1, [r7], #1
	add r0, r1
	mul r1, r0, r3
	asr r0, r1, #9
	tst r0, #0x80
	addne r0, r0, #1
	strb r0, [r5, r6]
	strb r0, [r5],#1
	subs r4, #1
	bgt loc_80358CC
	adr r0, (loc_803593E+1)
	bx r0
	
	
	.thumb_func
	
_08035910:
	mov r0, #0
	mov r1, r8
	add r6, r6, r5
	lsr r1, r1, #3
	blo _0803591E
	stm r5!, {r0}
	stm r6!, {r0}
_0803591E:
	lsr r1, r1, #1
	blo _0803592A
	stm r5!, {r0}
	stm r6!, {r0}
	stm r5!, {r0}
	stm r6!, {r0}
_0803592A:
	stm r5!, {r0}
	stm r6!, {r0}
	stm r5!, {r0}
	stm r6!, {r0}
	stm r5!, {r0}
	stm r6!, {r0}
	stm r5!, {r0}
	stm r6!, {r0}
	sub r1, #1
	bgt _0803592A

loc_803593E:
	ldr r4, [sp, #0x18]
	ldr r0, [r4, #0x18]
	mov ip, r0
	ldrb r0, [r4, #6]
	add r4, #0x50
_08035948:
	str r0, [sp, #4]
	ldr r3, [r4, #0x24]
	ldr r0, [sp, #0x14]
	cmp r0, #0
	beq _08035968
	ldr r1, _08035964
	ldrb r1, [r1]
	cmp r1, #0xa0
	bhs _0803595C
	add r1, #0xe4
_0803595C:
	cmp r1, r0
	blo _08035968
	b _08035C3A
	.align 2, 0
_08035964: .4byte 0x04000006
_08035968:
	ldrb r6, [r4]
	mov r0, #0xc7
	tst r0, r6
	bne _08035972
	b _08035C30
_08035972:
	mov r0, #0x80
	tst r0, r6
	beq _080359A2
	mov r0, #0x40
	tst r0, r6
	bne _080359B2
	mov r6, #3
	strb r6, [r4]
	add r0, r3, #0
	add r0, #0x10
	str r0, [r4, #0x28]
	ldr r0, [r3, #0xc]
	str r0, [r4, #0x18]
	mov r5, #0
	strb r5, [r4, #9]
	str r5, [r4, #0x1c]
	ldrb r2, [r3, #3]
	mov r0, #0xc0
	tst r0, r2
	beq _080359FA
	mov r0, #0x10
	orr r6, r0
	strb r6, [r4]
	b _080359FA
_080359A2:
	ldrb r5, [r4, #9]
	mov r0, #4
	tst r0, r6
	beq _080359B8
	ldrb r0, [r4, #0xd]
	sub r0, #1
	strb r0, [r4, #0xd]
	bhi _08035A08
_080359B2:
	mov r0, #0
	strb r0, [r4]
	b _08035C30
_080359B8:
	mov r0, #0x40
	tst r0, r6
	beq _080359D8
	ldrb r0, [r4, #7]
	mul r5, r0, r5
	lsr r5, r5, #8
	ldrb r0, [r4, #0xc]
	cmp r5, r0
	bhi _08035A08
_080359CA:
	ldrb r5, [r4, #0xc]
	cmp r5, #0
	beq _080359B2
	mov r0, #4
	orr r6, r0
	strb r6, [r4]
	b _08035A08
_080359D8:
	mov r2, #3
	and r2, r6
	cmp r2, #2
	bne _080359F6
	ldrb r0, [r4, #5]
	mul r5, r0, r5
	lsr r5, r5, #8
	ldrb r0, [r4, #6]
	cmp r5, r0
	bhi _08035A08
	add r5, r0, #0
	beq _080359CA
	sub r6, #1
	strb r6, [r4]
	b _08035A08
_080359F6:
	cmp r2, #3
	bne _08035A08
_080359FA:
	ldrb r0, [r4, #4]
	add r5, r5, r0
	cmp r5, #0xff
	blo _08035A08
	mov r5, #0xff
	sub r6, #1
	strb r6, [r4]
_08035A08:
	strb r5, [r4, #9]
	ldr r0, [sp, #0x18]
	ldrb r0, [r0, #7]
	add r0, #1
	mul r0, r5, r0
	lsr r5, r0, #4
	ldrb r0, [r4, #2]
	mul r0, r5, r0
	lsr r0, r0, #8
	strb r0, [r4, #0xa]
	ldrb r0, [r4, #3]
	mul r0, r5, r0
	lsr r0, r0, #8
	strb r0, [r4, #0xb]
	mov r0, #0x10
	and r0, r6
	str r0, [sp, #0x10]
	beq _08035A3C
	add r0, r3, #0
	add r0, #0x10
	ldr r1, [r3, #8]
	add r0, r0, r1
	str r0, [sp, #0xc]
	ldr r0, [r3, #0xc]
	sub r0, r0, r1
	str r0, [sp, #0x10]
_08035A3C:
	ldr r5, [sp, #8]
	ldr r2, [r4, #0x18]
	ldr r3, [r4, #0x28]
	add r0, pc, #0x4
	bx r0

	.align 2, 0
	.arm


loc_8035A48:
					
		STR		R8, [SP]
		LDRB		R10, [R4,#0xA]
		LDRB		R11, [R4,#0xB]
		MOV		R10, R10,LSL#16
		MOV		R11, R11,LSL#16
		LDRB		R0, [R4,#1]
		TST		R0, #8
		BEQ		loc_8035B88

loc_8035A68:
		CMP		R2, #4
		BLE		loc_8035AD8
		SUBS		R2, R2,	R8
		MOVGT		LR, #0
		BGT		loc_8035A94
		MOV		LR, R8
		ADD		R2, R2,	R8
		SUB		R8, R2,	#4
		SUB		LR, LR,	R8
		ANDS		R2, R2,	#3
		MOVEQ		R2, #4

loc_8035A94:	

		LDR		R6, [R5]
		LDR		R7, [R5,#0x630]

loc_8035A9C:
		LDRSB		R0, [R3],#1
		MUL		R1, R10, R0
		BIC		R1, R1,	#0xFF0000
		ADD		R6, R1,	R6,ROR#8
		MUL		R1, R11, R0
		BIC		R1, R1,	#0xFF0000
		ADD		R7, R1,	R7,ROR#8
		ADDS		R5, R5,	#0x40000000
		BCC		loc_8035A9C
		STR		R7, [R5,#0x630]
		STR		R6, [R5],#4
		SUBS		R8, R8,	#4
		BGT		loc_8035A94
		ADDS		R8, R8,	LR
		BEQ		loc_8035C1C

loc_8035AD8:
		LDR		R6, [R5]
		LDR		R7, [R5,#0x630]

loc_8035AE0:
		LDRSB		R0, [R3],#1
		MUL		R1, R10, R0
		BIC		R1, R1,	#0xFF0000
		ADD		R6, R1,	R6,ROR#8
		MUL		R1, R11, R0
		BIC		R1, R1,	#0xFF0000
		ADD		R7, R1,	R7,ROR#8
		SUBS		R2, R2,	#1
		BEQ		loc_8035B50

loc_8035B04:
		ADDS		R5, R5,	#0x40000000
		BCC		loc_8035AE0
		STR		R7, [R5,#0x630]
		STR		R6, [R5],#4
		SUBS		R8, R8,	#4
		BGT		loc_8035A68
		B		loc_8035C1C

loc_8035B20:
		LDR		R0, [SP,#0x18]
		CMP		R0, #0
		BEQ		loc_8035B44
		LDR		R3, [SP,#0x14]
		RSB		R9, R2,	#0

loc_8035B34:
		ADDS		R2, R0,	R2
		BGT		loc_8035BEC
		SUB		R9, R9,	R0
		B		loc_8035B34

loc_8035B44:
		LDMFD		SP!, {R4,R12}
		MOV		R2, #0
		B		loc_8035B60

loc_8035B50:
		LDR		R2, [SP,#0x10]
		CMP		R2, #0
		LDRNE		R3, [SP,#0xC]
		BNE		loc_8035B04

loc_8035B60:
		STRB		R2, [R4]
		MOV		R0, R5,LSR#30
		BIC		R5, R5,	#0xC0000000
		RSB		R0, R0,	#3
		MOV		R0, R0,LSL#3
		MOV		R6, R6,ROR R0
		MOV		R7, R7,ROR R0
		STR		R7, [R5,#0x630]
		STR		R6, [R5],#4
		B		loc_8035C24

loc_8035B88:
		STMFD		SP!, {R4,R12}
		LDR		LR, [R4,#0x1C]
		LDR		R1, [R4,#0x20]
		MUL		R4, R12, R1
		LDRSB		R0, [R3]
		LDRSB		R1, [R3,#1]!
		SUB		R1, R1,	R0

loc_8035BA4:
		LDR		R6, [R5]
		LDR		R7, [R5,#0x630]

loc_8035BAC:
		MUL		R9, LR,	R1
		ADD		R9, R0,	R9,ASR#23
		MUL		R12, R10, R9
		BIC		R12, R12, #0xFF0000
		ADD		R6, R12, R6,ROR#8
		MUL		R12, R11, R9
		BIC		R12, R12, #0xFF0000
		ADD		R7, R12, R7,ROR#8
		ADD		LR, LR,	R4
		MOVS		R9, LR,LSR#23
		BEQ		loc_8035BF8
		BIC		LR, LR,	#0x3F800000
		SUBS		R2, R2,	R9
		BLE		loc_8035B20
		SUBS		R9, R9,	#1
		ADDEQ		R0, R0,	R1

loc_8035BEC:
		LDRNESB		R0, [R3,R9]!
		LDRSB		R1, [R3,#1]!
		SUB		R1, R1,	R0

loc_8035BF8:
		ADDS		R5, R5,	#0x40000000
		BCC		loc_8035BAC
		STR		R7, [R5,#0x630]
		STR		R6, [R5],#4
		SUBS		R8, R8,	#4
		BGT		loc_8035BA4
		SUB		R3, R3,	#1
		LDMFD		SP!, {R4,R12}
		STR		LR, [R4,#0x1C]

loc_8035C1C:

		STR		R2, [R4,#0x18]
		STR		R3, [R4,#0x28]

loc_8035C24:
		LDR		R8, [SP]
		ADR		R0, (_08035C30+1)
		BX		R0



	
	.thumb_func
_08035C30:
	ldr r0, [sp, #4]
	sub r0, #1
	ble _08035C3A
	add r4, #0x40
	b _08035948
_08035C3A:
	ldr r0, [sp, #0x18]
	ldr r3, _08035C50
	str r3, [r0]
	add sp, #0x1c
	pop {r0, r1, r2, r3, r4, r5, r6, r7}
	mov r8, r0
	mov sb, r1
	mov sl, r2
	mov fp, r3
	pop {r3}

thumb_func_global call_r3_2
call_r3_2: @ 0x08035C4E
	bx r3
	.align 2, 0
_08035C50: .4byte 0x68736D53
	
	
	
	thumb_func_global sub_8035C54
sub_8035C54: @ 0x08035C54
	mov ip, r4
	mov r1, #0
	mov r2, #0
	mov r3, #0
	mov r4, #0
	stm r0!, {r1, r2, r3, r4}
	stm r0!, {r1, r2, r3, r4}
	stm r0!, {r1, r2, r3, r4}
	stm r0!, {r1, r2, r3, r4}
	mov r4, ip
	bx lr
	.align 2, 0
	
	
	
	thumb_func_global sub_8035C6C
sub_8035C6C: @ 0x08035C6C
	ldr r3, [r0, #0x2c]
	cmp r3, #0
	beq locret_8035C8A
	ldr r1, [r0, #0x34]
	ldr r2, [r0, #0x30]
	cmp r2, #0
	beq _08035C7E
	str r1, [r2, #0x34]
	b _08035C80
_08035C7E:
	str r1, [r3, #0x20]
_08035C80:
	cmp r1, #0
	beq _08035C86
	str r2, [r1, #0x30]
_08035C86:
	mov r1, #0
	str r1, [r0, #0x2c]
locret_8035C8A:
	bx lr
	
	
	
thumb_func_global sub_8035C8C	
sub_8035C8C:
	push {r4, r5, lr}
	add r5, r1, #0
	ldr r4, [r5, #0x20]
	cmp r4, #0
	beq _08035CB0
_08035C96:
	ldrb r1, [r4]
	mov r0, #0xc7
	tst r0, r1
	beq _08035CA4
	mov r0, #0x40
	orr r1, r0
	strb r1, [r4]
_08035CA4:
	add r0, r4, #0
	bl sub_8035C6C
	ldr r4, [r4, #0x34]
	cmp r4, #0
	bne _08035C96
_08035CB0:
	mov r0, #0
	strb r0, [r5]
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
	
	

thumb_func_global sub_08035CBC
sub_08035CBC: @ 0x08035CBC
	mov ip, lr
	mov r1, #0x24
	ldr r2, _08035CEC
_08035CC2:
	ldr r3, [r2]
	bl sub_08035CD6
	stm r0!, {r3}
	add r2, #4
	sub r1, #1
	bgt _08035CC2
	bx ip
	.align 2, 0
	
	
sub_8035CD4:
	ldrb r3,[r2]

thumb_func_global sub_08035CD6
sub_08035CD6: @ 0x08035CD6
	push {r0}
	lsr r0, r2, #0x19
	bne _08035CE8
	ldr r0, _08035CEC
	cmp r2, r0
	blo _08035CE6
	lsr r0, r2, #0xe
	beq _08035CE8
_08035CE6:
	mov r3, #0
_08035CE8:
	pop {r0}
	bx lr
	.align 2, 0
_08035CEC: .4byte off_8337670
	
	
	
	thumb_func_global sub_8035CF0
sub_8035CF0: @ 0x08035CF0
	ldr r2, [r1, #0x40]
	
	
	thumb_func_global sub_08035CF2
sub_08035CF2: @ 0x08035CF2
	add r3, r2, #1
	str r3, [r1, #0x40]
	ldrb r3, [r2]
	b sub_08035CD6
	.align 2, 0


thumb_func_global sub_8035CFC
sub_8035CFC:
	push {lr}
	
	
	
sub_8035CFE:
	ldr r2, [r1, #0x40]
	ldrb r0, [r2, #3]
	lsl r0, r0, #8
	ldrb r3, [r2, #2]
	orr r0, r3
	lsl r0, r0, #8
	ldrb r3, [r2, #1]
	orr r0, r3
	lsl r0, r0, #8
	bl sub_8035CD4
	orr r0, r3
	str r0, [r1, #0x40]
	pop {r0}
	bx r0
	
	
	
	thumb_func_global sub_8035D1C
sub_8035D1C: @ 0x08035D1C
	ldrb r2, [r1, #2]
	cmp r2, #3
	bhs _08035D34
	lsl r2, r2, #2
	add r3, r1, r2
	ldr r2, [r1, #0x40]
	add r2, #4
	str r2, [r3, #0x44]
	ldrb r2, [r1, #2]
	add r2, #1
	strb r2, [r1, #2]
	b sub_8035CFC
_08035D34:
	b sub_8035C8C
	.align 2, 0
	
	
	
	thumb_func_global sub_8035D38
sub_8035D38: @ 0x08035D38
	ldrb r2, [r1, #2]
	cmp r2, #0
	beq locret_8035D4A
	sub r2, #1
	strb r2, [r1, #2]
	lsl r2, r2, #2
	add r3, r1, r2
	ldr r2, [r3, #0x44]
	str r2, [r1, #0x40]
locret_8035D4A:
	bx lr
	
	
	
	thumb_func_global sub_8035D4C
sub_8035D4C: @ 0x08035D4C
	push {lr}
	ldr r2, [r1, #0x40]
	ldrb r3, [r2]
	cmp r3, #0
	bne _08035D5C
	add r2, #1
	str r2, [r1, #0x40]
	b sub_8035CFE
_08035D5C:
	ldrb r3, [r1, #3]
	add r3, #1
	strb r3, [r1, #3]
	mov ip, r3
	bl sub_8035CF0
	cmp ip, r3
	bhs _08035D6E
	b sub_8035CFE
_08035D6E:
	mov r3, #0
	strb r3, [r1, #3]
	add r2, #5
	str r2, [r1, #0x40]
	pop {r0}
	bx r0
	.align 2, 0
	
	
	thumb_func_global sub_8035D7C
sub_8035D7C: @ 0x08035D7C
	mov ip, lr
	bl sub_8035CF0
	strb r3, [r1, #0x1d]
	bx ip
	.align 2, 0
	
	
	
	thumb_func_global sub_8035D88
sub_8035D88: @ 0x08035D88
	mov ip, lr
	bl sub_8035CF0
	lsl r3, r3, #1
	strh r3, [r0, #0x1c]
	ldrh r2, [r0, #0x1e]
	mul r3, r2, r3
	lsr r3, r3, #8
	strh r3, [r0, #0x20]
	bx ip
	
	
	
	thumb_func_global sub_8035D9C
sub_8035D9C: @ 0x08035D9C
	mov ip, lr
	bl sub_8035CF0
	strb r3, [r1, #0xa]
	ldrb r3, [r1]
	mov r2, #0xc
	orr r3, r2
	strb r3, [r1]
	bx ip
	.align 2, 0
	
	
	
	thumb_func_global sub_8035DB0
sub_8035DB0: @ 0x08035DB0
	mov ip, lr
	ldr r2, [r1, #0x40]
	ldrb r3, [r2]
	add r2, #1
	str r2, [r1, #0x40]
	lsl r2, r3, #1
	add r2, r2, r3
	lsl r2, r2, #2
	ldr r3, [r0, #0x30]
	add r2, r2, r3
	ldr r3, [r2]
	bl sub_08035CD6
	str r3, [r1, #0x24]
	ldr r3, [r2, #4]
	bl sub_08035CD6
	str r3, [r1, #0x28]
	ldr r3, [r2, #8]
	bl sub_08035CD6
	str r3, [r1, #0x2c]
	bx ip
	.align 2, 0
	
	
	
	thumb_func_global sub_8035DE0
sub_8035DE0: @ 0x08035DE0
	mov ip, lr
	bl sub_8035CF0
	strb r3, [r1, #0x12]
	ldrb r3, [r1]
	mov r2, #3
	orr r3, r2
	strb r3, [r1]
	bx ip
	.align 2, 0
	
	
	
	thumb_func_global sub_8035DF4
sub_8035DF4: @ 0x08035DF4
	mov ip, lr
	bl sub_8035CF0
	sub r3, #0x40
	strb r3, [r1, #0x14]
	ldrb r3, [r1]
	mov r2, #3
	orr r3, r2
	strb r3, [r1]
	bx ip
	
	
	
	thumb_func_global sub_8035E08
sub_8035E08: @ 0x08035E08
	mov ip, lr
	bl sub_8035CF0
	sub r3, #0x40
	strb r3, [r1, #0xe]
	ldrb r3, [r1]
	mov r2, #0xc
	orr r3, r2
	strb r3, [r1]
	bx ip
	
	
	
	thumb_func_global sub_8035E1C
sub_8035E1C: @ 0x08035E1C
	mov ip, lr
	bl sub_8035CF0
	strb r3, [r1, #0xf]
	ldrb r3, [r1]
	mov r2, #0xc
	orr r3, r2
	strb r3, [r1]
	bx ip
	.align 2, 0
	
	
	
	thumb_func_global sub_8035E30
sub_8035E30: @ 0x08035E30
	mov ip, lr
	bl sub_8035CF0
	strb r3, [r1, #0x1b]
	bx ip
	.align 2, 0
	
	
	
	thumb_func_global sub_8035E3C
sub_8035E3C: @ 0x08035E3C
	mov ip, lr
	bl sub_8035CF0
	ldrb r0, [r1, #0x18]
	cmp r0, r3
	beq _08035E52
	strb r3, [r1, #0x18]
	ldrb r3, [r1]
	mov r2, #0xf
	orr r3, r2
	strb r3, [r1]
_08035E52:
	bx ip
	
	
	
	thumb_func_global sub_8035E54
sub_8035E54: @ 0x08035E54
	mov ip, lr
	bl sub_8035CF0
	sub r3, #0x40
	strb r3, [r1, #0xc]
	ldrb r3, [r1]
	mov r2, #0xc
	orr r3, r2
	strb r3, [r1]
	bx ip
	
	
	
	thumb_func_global sub_8035E68
sub_8035E68: @ 0x08035E68
	mov ip, lr
	ldr r2, [r1, #0x40]
	ldrb r3, [r2]
	add r2, #1
	ldr r0, _08035E7C
	add r0, r0, r3
	bl sub_08035CF2
	strb r3, [r0]
	bx ip
	.align 2, 0
_08035E7C: .4byte 0x04000060
	
	

thumb_func_global sub_08035E80
sub_08035E80: @ 0x08035E80
	ldr r0, _0803612C
	ldr r0, [r0]
	ldr r2, _08036130
	ldr r3, [r0]
	sub r3, r3, r2
	cmp r3, #1
	bhi _08035EC0
	ldrb r1, [r0, #4]
	sub r1, #1
	strb r1, [r0, #4]
	bgt _08035EC0
	ldrb r1, [r0, #0xb]
	strb r1, [r0, #4]
	ldr r2, _08035EC4
	ldr r1, [r2, #8]
	lsl r1, r1, #7
	blo _08035EA6
	ldr r1, _08035EC8
	str r1, [r2, #8]
_08035EA6:
	ldr r1, [r2, #0x14]
	lsl r1, r1, #7
	blo _08035EB0
	ldr r1, _08035EC8
	str r1, [r2, #0x14]
_08035EB0:
	mov r1, #4
	lsl r1, r1, #8
	strh r1, [r2, #0xa]
	strh r1, [r2, #0x16]
	mov r1, #0xb6
	lsl r1, r1, #8
	strh r1, [r2, #0xa]
	strh r1, [r2, #0x16]
_08035EC0:
	bx lr
	.align 2, 0
_08035EC4: .4byte 0x040000BC
_08035EC8: .4byte 0x84400004



thumb_func_global sub_8035ECC
sub_8035ECC: @ 0x08035ECC
	ldr r2, _08036130
	ldr r3, [r0, #0x34]
	cmp r2, r3
	beq _08035ED6
	bx lr
_08035ED6:
	add r3, #1
	str r3, [r0, #0x34]
	push {r0, lr}
	ldr r3, [r0, #0x38]
	cmp r3, #0
	beq _08035EE8
	ldr r0, [r0, #0x3c]
	bl locret_8036124
_08035EE8:
	pop {r0}
	push {r4, r5, r6, r7}
	mov r4, r8
	mov r5, sb
	mov r6, sl
	mov r7, fp
	push {r4, r5, r6, r7}
	add r7, r0, #0
	ldr r0, [r7, #4]
	cmp r0, #0
	bge _08035F00
	b _08036114
_08035F00:
	ldr r0, _0803612C
	ldr r0, [r0]
	mov r8, r0
	add r0, r7, #0
	bl FadeOutBody_rev01
	ldr r0, [r7, #4]
	cmp r0, #0
	bge _08035F14
	b _08036114
_08035F14:
	ldrh r0, [r7, #0x22]
	ldrh r1, [r7, #0x20]
	add r0, r0, r1
	b _08036064
_08035F1C:
	ldrb r6, [r7, #8]
	ldr r5, [r7, #0x2c]
	mov r3, #1
	mov r4, #0
_08035F24:
	ldrb r0, [r5]
	mov r1, #0x80
	tst r1, r0
	bne _08035F2E
	b _08036040
_08035F2E:
	mov sl, r3
	orr r4, r3
	mov fp, r4
	ldr r4, [r5, #0x20]
	cmp r4, #0
	beq _08035F62
_08035F3A:
	ldrb r1, [r4]
	mov r0, #0xc7
	tst r0, r1
	beq _08035F56
	ldrb r0, [r4, #0x10]
	cmp r0, #0
	beq _08035F5C
	sub r0, #1
	strb r0, [r4, #0x10]
	bne _08035F5C
	mov r0, #0x40
	orr r1, r0
	strb r1, [r4]
	b _08035F5C
_08035F56:
	add r0, r4, #0
	bl ClearChain_rev
_08035F5C:
	ldr r4, [r4, #0x34]
	cmp r4, #0
	bne _08035F3A
_08035F62:
	ldrb r3, [r5]
	mov r0, #0x40
	tst r0, r3
	beq _08035FE0
_08035F6A:
	add r0, r5, #0
	bl Clear64byte_rev_maybe
	mov r0, #0x80
	strb r0, [r5]
	mov r0, #2
	strb r0, [r5, #0xf]
	mov r0, #0x40
	strb r0, [r5, #0x13]
	mov r0, #0x16
	strb r0, [r5, #0x19]
	mov r0, #1
	add r1, r5, #6
	strb r0, [r1, #0x1e]
	b _08035FE0
_08035F88:
	ldr r2, [r5, #0x40]
	ldrb r1, [r2]
	cmp r1, #0x80
	bhs _08035F94
	ldrb r1, [r5, #7]
	b _08035F9E
_08035F94:
	add r2, #1
	str r2, [r5, #0x40]
	cmp r1, #0xbd
	blo _08035F9E
	strb r1, [r5, #7]
_08035F9E:
	cmp r1, #0xcf
	blo _08035FB4
	mov r0, r8
	ldr r3, [r0, #0x38]
	add r0, r1, #0
	sub r0, #0xcf
	add r1, r7, #0
	add r2, r5, #0
	bl locret_8036124
	b _08035FE0
_08035FB4:
	cmp r1, #0xb0
	bls _08035FD6
	add r0, r1, #0
	sub r0, #0xb1
	strb r0, [r7, #0xa]
	mov r3, r8
	ldr r3, [r3, #0x34]
	lsl r0, r0, #2
	ldr r3, [r3, r0]
	add r0, r7, #0
	add r1, r5, #0
	bl locret_8036124
	ldrb r0, [r5]
	cmp r0, #0
	beq _0803603C
	b _08035FE0
_08035FD6:
	ldr r0, _08036128
	sub r1, #0x80
	add r1, r1, r0
	ldrb r0, [r1]
	strb r0, [r5, #1]
_08035FE0:
	ldrb r0, [r5, #1]
	cmp r0, #0
	beq _08035F88
	sub r0, #1
	strb r0, [r5, #1]
	ldrb r1, [r5, #0x19]
	cmp r1, #0
	beq _0803603C
	ldrb r0, [r5, #0x17]
	cmp r0, #0
	beq _0803603C
	ldrb r0, [r5, #0x1c]
	cmp r0, #0
	beq _08036002
	sub r0, #1
	strb r0, [r5, #0x1c]
	b _0803603C
_08036002:
	ldrb r0, [r5, #0x1a]
	add r0, r0, r1
	strb r0, [r5, #0x1a]
	add r1, r0, #0
	sub r0, #0x40
	lsl r0, r0, #0x18
	bpl _08036016
	lsl r2, r1, #0x18
	asr r2, r2, #0x18
	b _0803601A
_08036016:
	mov r0, #0x80
	sub r2, r0, r1
_0803601A:
	ldrb r0, [r5, #0x17]
	mul r0, r2, r0
	asr r2, r0, #6
	ldrb r0, [r5, #0x16]
	eor r0, r2
	lsl r0, r0, #0x18
	beq _0803603C
	strb r2, [r5, #0x16]
	ldrb r0, [r5]
	ldrb r1, [r5, #0x18]
	cmp r1, #0
	bne _08036036
	mov r1, #0xc
	b _08036038
_08036036:
	mov r1, #3
_08036038:
	orr r0, r1
	strb r0, [r5]
_0803603C:
	mov r3, sl
	mov r4, fp
_08036040:
	sub r6, #1
	ble _0803604C
	mov r0, #0x50
	add r5, r5, r0
	lsl r3, r3, #1
	b _08035F24
_0803604C:
	ldr r0, [r7, #0xc]
	add r0, #1
	str r0, [r7, #0xc]
	cmp r4, #0
	bne _0803605E
	mov r0, #0x80
	lsl r0, r0, #0x18
	str r0, [r7, #4]
	b _08036114
_0803605E:
	str r4, [r7, #4]
	ldrh r0, [r7, #0x22]
	sub r0, #0x96
_08036064:
	strh r0, [r7, #0x22]
	cmp r0, #0x96
	blo _0803606C
	b _08035F1C
_0803606C:
	ldrb r2, [r7, #8]
	ldr r5, [r7, #0x2c]
_08036070:
	ldrb r0, [r5]
	mov r1, #0x80
	tst r1, r0
	beq _0803610A
	mov r1, #0xf
	tst r1, r0
	beq _0803610A
	mov sb, r2
	add r0, r7, #0
	add r1, r5, #0
	bl TrkVolPitSet_rev01
	ldr r4, [r5, #0x20]
	cmp r4, #0
	beq _08036100
_0803608E:
	ldrb r1, [r4]
	mov r0, #0xc7
	tst r0, r1
	bne _0803609E
	add r0, r4, #0
	bl ClearChain_rev
	b _080360FA
_0803609E:
	ldrb r0, [r4, #1]
	mov r6, #7
	and r6, r0
	ldrb r3, [r5]
	mov r0, #3
	tst r0, r3
	beq _080360BC
	bl sub_8036178
	cmp r6, #0
	beq _080360BC
	ldrb r0, [r4, #0x1d]
	mov r1, #1
	orr r0, r1
	strb r0, [r4, #0x1d]
_080360BC:
	ldrb r3, [r5]
	mov r0, #0xc
	tst r0, r3
	beq _080360FA
	ldrb r1, [r4, #8]
	mov r0, #8
	ldrsb r0, [r5, r0]
	add r2, r1, r0
	bpl _080360D0
	mov r2, #0
_080360D0:
	cmp r6, #0
	beq _080360EE
	mov r0, r8
	ldr r3, [r0, #0x30]
	add r1, r2, #0
	ldrb r2, [r5, #9]
	add r0, r6, #0
	bl locret_8036124
	str r0, [r4, #0x20]
	ldrb r0, [r4, #0x1d]
	mov r1, #2
	orr r0, r1
	strb r0, [r4, #0x1d]
	b _080360FA
_080360EE:
	add r1, r2, #0
	ldrb r2, [r5, #9]
	ldr r0, [r4, #0x24]
	bl sub_8036438
	str r0, [r4, #0x20]
_080360FA:
	ldr r4, [r4, #0x34]
	cmp r4, #0
	bne _0803608E
_08036100:
	ldrb r0, [r5]
	mov r1, #0xf0
	and r0, r1
	strb r0, [r5]
	mov r2, sb
_0803610A:
	sub r2, #1
	ble _08036114
	mov r0, #0x50
	add r5, r5, r0
	bgt _08036070
_08036114:
	ldr r0, _08036130
	str r0, [r7, #0x34]
	pop {r0, r1, r2, r3, r4, r5, r6, r7}
	mov r8, r0
	mov sb, r1
	mov sl, r2
	mov fp, r3
	pop {r3}
	


thumb_func_global locret_8036124
locret_8036124: @ 0x08036124
	bx r3
	.align 2, 0

_08036128: .4byte unk_83378E4
_0803612C: .4byte 0x03007FF0
_08036130: .4byte 0x68736D53

thumb_func_global TrackStop_rev01
TrackStop_rev01: @ 0x08036134
	push {r4, r5, r6, lr}
	add r5, r1, #0
	ldrb r1, [r5]
	mov r0, #0x80
	tst r0, r1
	beq _0803616C
	ldr r4, [r5, #0x20]
	cmp r4, #0
	beq _0803616A
	mov r6, #0
_08036148:
	ldrb r0, [r4]
	cmp r0, #0
	beq _08036162
	ldrb r0, [r4, #1]
	mov r3, #7
	and r0, r3
	beq _08036160
	ldr r3, _08036174
	ldr r3, [r3]
	ldr r3, [r3, #0x2c]
	bl locret_8036124
_08036160:
	strb r6, [r4]
_08036162:
	str r6, [r4, #0x2c]
	ldr r4, [r4, #0x34]
	cmp r4, #0
	bne _08036148
_0803616A:
	str r4, [r5, #0x20]
_0803616C:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_08036174: .4byte 0x03007FF0


thumb_func_global sub_8036178
sub_8036178: @ 0x08036178
	ldrb r1, [r4, #0x12]
	mov r0, #0x14
	ldrsb r2, [r4, r0]
	mov r3, #0x80
	add r3, r3, r2
	mul r3, r1, r3
	ldrb r0, [r5, #0x10]
	mul r0, r3, r0
	asr r0, r0, #0xe
	cmp r0, #0xff
	bls _08036190
	mov r0, #0xff
_08036190:
	strb r0, [r4, #2]
	mov r3, #0x7f
	sub r3, r3, r2
	mul r3, r1, r3
	ldrb r0, [r5, #0x11]
	mul r0, r3, r0
	asr r0, r0, #0xe
	cmp r0, #0xff
	bls _080361A4
	mov r0, #0xff
_080361A4:
	strb r0, [r4, #3]
	bx lr
	
	
	
	thumb_func_global sub_80361A8
sub_80361A8: @ 0x080361A8
	push {r4, r5, r6, r7, lr}
	mov r4, r8
	mov r5, sb
	mov r6, sl
	mov r7, fp
	push {r4, r5, r6, r7}
	sub sp, #0x18
	str r1, [sp]
	add r5, r2, #0
	ldr r1, _080363A0
	ldr r1, [r1]
	str r1, [sp, #4]
	ldr r1, _080363A4
	add r0, r0, r1
	ldrb r0, [r0]
	strb r0, [r5, #4]
	ldr r3, [r5, #0x40]
	ldrb r0, [r3]
	cmp r0, #0x80
	bhs _080361EE
	strb r0, [r5, #5]
	add r3, #1
	ldrb r0, [r3]
	cmp r0, #0x80
	bhs _080361EC
	strb r0, [r5, #6]
	add r3, #1
	ldrb r0, [r3]
	cmp r0, #0x80
	bhs _080361EC
	ldrb r1, [r5, #4]
	add r1, r1, r0
	strb r1, [r5, #4]
	add r3, #1
_080361EC:
	str r3, [r5, #0x40]
_080361EE:
	mov r0, #0
	str r0, [sp, #0x14]
	add r4, r5, #0
	add r4, #0x24
	ldrb r2, [r4]
	mov r0, #0xc0
	tst r0, r2
	beq _08036240
	ldrb r3, [r5, #5]
	mov r0, #0x40
	tst r0, r2
	beq _0803620E
	ldr r1, [r5, #0x2c]
	add r1, r1, r3
	ldrb r0, [r1]
	b _08036210
_0803620E:
	add r0, r3, #0
_08036210:
	lsl r1, r0, #1
	add r1, r1, r0
	lsl r1, r1, #2
	ldr r0, [r5, #0x28]
	add r1, r1, r0
	mov sb, r1
	mov r6, sb
	ldrb r1, [r6]
	mov r0, #0xc0
	tst r0, r1
	beq _08036228
	b _0803638E
_08036228:
	mov r0, #0x80
	tst r0, r2
	beq _08036244
	ldrb r1, [r6, #3]
	mov r0, #0x80
	tst r0, r1
	beq _0803623C
	sub r1, #0xc0
	lsl r1, r1, #1
	str r1, [sp, #0x14]
_0803623C:
	ldrb r3, [r6, #1]
	b _08036244
_08036240:
	mov sb, r4
	ldrb r3, [r5, #5]
_08036244:
	str r3, [sp, #8]
	ldr r6, [sp]
	ldrb r1, [r6, #9]
	ldrb r0, [r5, #0x1d]
	add r0, r0, r1
	cmp r0, #0xff
	bls _08036254
	mov r0, #0xff
_08036254:
	str r0, [sp, #0x10]
	mov r6, sb
	ldrb r0, [r6]
	mov r6, #7
	and r6, r0
	str r6, [sp, #0xc]
	beq _08036294
	ldr r0, [sp, #4]
	ldr r4, [r0, #0x1c]
	cmp r4, #0
	bne _0803626C
	b _0803638E
_0803626C:
	sub r6, #1
	lsl r0, r6, #6
	add r4, r4, r0
	ldrb r1, [r4]
	mov r0, #0xc7
	tst r0, r1
	beq _080362E8
	mov r0, #0x40
	tst r0, r1
	bne _080362E8
	ldrb r1, [r4, #0x13]
	ldr r0, [sp, #0x10]
	cmp r1, r0
	blo _080362E8
	beq _0803628C
	b _0803638E
_0803628C:
	ldr r0, [r4, #0x2c]
	cmp r0, r5
	bhs _080362E8
	b _0803638E
_08036294:
	ldr r6, [sp, #0x10]
	add r7, r5, #0
	mov r2, #0
	mov r8, r2
	ldr r4, [sp, #4]
	ldrb r3, [r4, #6]
	add r4, #0x50
_080362A2:
	ldrb r1, [r4]
	mov r0, #0xc7
	tst r0, r1
	beq _080362E8
	mov r0, #0x40
	tst r0, r1
	beq _080362BC
	cmp r2, #0
	bne _080362C0
	add r2, #1
	ldrb r6, [r4, #0x13]
	ldr r7, [r4, #0x2c]
	b _080362DA
_080362BC:
	cmp r2, #0
	bne _080362DC
_080362C0:
	ldrb r0, [r4, #0x13]
	cmp r0, r6
	bhs _080362CC
	add r6, r0, #0
	ldr r7, [r4, #0x2c]
	b _080362DA
_080362CC:
	bhi _080362DC
	ldr r0, [r4, #0x2c]
	cmp r0, r7
	bls _080362D8
	add r7, r0, #0
	b _080362DA
_080362D8:
	blo _080362DC
_080362DA:
	mov r8, r4
_080362DC:
	add r4, #0x40
	sub r3, #1
	bgt _080362A2
	mov r4, r8
	cmp r4, #0
	beq _0803638E
_080362E8:
	add r0, r4, #0
	bl ClearChain_rev
	mov r1, #0
	str r1, [r4, #0x30]
	ldr r3, [r5, #0x20]
	str r3, [r4, #0x34]
	cmp r3, #0
	beq _080362FC
	str r4, [r3, #0x30]
_080362FC:
	str r4, [r5, #0x20]
	str r5, [r4, #0x2c]
	ldrb r0, [r5, #0x1b]
	strb r0, [r5, #0x1c]
	cmp r0, r1
	beq _0803630E
	add r1, r5, #0
	bl sub_080363E8
_0803630E:
	ldr r0, [sp]
	add r1, r5, #0
	bl TrkVolPitSet_rev01
	ldr r0, [r5, #4]
	str r0, [r4, #0x10]
	ldr r0, [sp, #0x10]
	strb r0, [r4, #0x13]
	ldr r0, [sp, #8]
	strb r0, [r4, #8]
	ldr r0, [sp, #0x14]
	strb r0, [r4, #0x14]
	mov r6, sb
	ldrb r0, [r6]
	strb r0, [r4, #1]
	ldr r7, [r6, #4]
	str r7, [r4, #0x24]
	ldr r0, [r6, #8]
	str r0, [r4, #4]
	ldrh r0, [r5, #0x1e]
	strh r0, [r4, #0xc]
	bl sub_8036178
	ldrb r1, [r4, #8]
	mov r0, #8
	ldrsb r0, [r5, r0]
	add r3, r1, r0
	bpl _08036348
	mov r3, #0
_08036348:
	ldr r6, [sp, #0xc]
	cmp r6, #0
	beq _08036376
	mov r6, sb
	ldrb r0, [r6, #2]
	strb r0, [r4, #0x1e]
	ldrb r1, [r6, #3]
	mov r0, #0x80
	tst r0, r1
	bne _08036362
	mov r0, #0x70
	tst r0, r1
	bne _08036364
_08036362:
	mov r1, #8
_08036364:
	strb r1, [r4, #0x1f]
	ldrb r2, [r5, #9]
	add r1, r3, #0
	ldr r0, [sp, #0xc]
	ldr r3, [sp, #4]
	ldr r3, [r3, #0x30]
	bl locret_8036124
	b _08036380
_08036376:
	ldrb r2, [r5, #9]
	add r1, r3, #0
	add r0, r7, #0
	bl sub_8036438
_08036380:
	str r0, [r4, #0x20]
	mov r0, #0x80
	strb r0, [r4]
	ldrb r1, [r5]
	mov r0, #0xf0
	and r0, r1
	strb r0, [r5]
_0803638E:
	add sp, #0x18
	pop {r0, r1, r2, r3, r4, r5, r6, r7}
	mov r8, r0
	mov sb, r1
	mov sl, r2
	mov fp, r3
	pop {r0}
	bx r0
	.align 2, 0
_080363A0: .4byte 0x03007FF0
_080363A4: .4byte unk_83378E4



thumb_func_global ply_endtie_rev01
ply_endtie_rev01: @ 0x080363A8
	push {r4, r5}
	ldr r2, [r1, #0x40]
	ldrb r3, [r2]
	cmp r3, #0x80
	bhs _080363BA
	strb r3, [r1, #5]
	add r2, #1
	str r2, [r1, #0x40]
	b _080363BC
_080363BA:
	ldrb r3, [r1, #5]
_080363BC:
	ldr r1, [r1, #0x20]
	cmp r1, #0
	beq _080363E4
	mov r4, #0x83
	mov r5, #0x40
_080363C6:
	ldrb r2, [r1]
	tst r2, r4
	beq _080363DE
	tst r2, r5
	bne _080363DE
	ldrb r0, [r1, #0x11]
	cmp r0, r3
	bne _080363DE
	mov r0, #0x40
	orr r2, r0
	strb r2, [r1]
	b _080363E4
_080363DE:
	ldr r1, [r1, #0x34]
	cmp r1, #0
	bne _080363C6
_080363E4:
	pop {r4, r5}
	bx lr

thumb_func_global sub_080363E8
sub_080363E8: @ 0x080363E8
	mov r2, #0
	strb r2, [r1, #0x16]
	strb r2, [r1, #0x1a]
	ldrb r2, [r1, #0x18]
	cmp r2, #0
	bne _080363F8
	mov r2, #0xc
	b _080363FA
_080363F8:
	mov r2, #3
_080363FA:
	ldrb r3, [r1]
	orr r3, r2
	strb r3, [r1]
	bx lr
	.align 2, 0

thumb_func_global sub_08036404
sub_08036404: @ 0x08036404
	ldr r2, [r1, #0x40]
	add r3, r2, #1
	str r3, [r1, #0x40]
	ldrb r3, [r2]
	bx lr
	.align 2, 0

thumb_func_global ply_lfos_rev01
ply_lfos_rev01: @ 0x08036410
	mov ip, lr
	bl sub_08036404
	strb r3, [r1, #0x19]
	cmp r3, #0
	bne _08036420
	bl sub_080363E8
_08036420:
	bx ip
	.align 2, 0

thumb_func_global ply_mod_rev01
ply_mod_rev01: @ 0x08036424
	mov r12, lr
	bl sub_08036404
	strb r3, [r1, #0x17]
	cmp r3, #0
	bne _08036434
	bl sub_080363E8
_08036434:
	bx r12
	.align 2, 0
	
	

	thumb_func_global sub_8036438
sub_8036438: @ 0x08036438
	push {r4, r5, r6, r7, lr}
	mov ip, r0
	lsl r1, r1, #0x18
	lsr r6, r1, #0x18
	lsl r7, r2, #0x18
	cmp r6, #0xb2
	bls _0803644C
	mov r6, #0xb2
	mov r7, #0xff
	lsl r7, r7, #0x18
_0803644C:
	ldr r3, _08036494
	add r0, r6, r3
	ldrb r5, [r0]
	ldr r4, _08036498
	mov r2, #0xf
	add r0, r5, #0
	and r0, r2
	lsl r0, r0, #2
	add r0, r0, r4
	lsr r1, r5, #4
	ldr r5, [r0]
	lsr r5, r1
	add r0, r6, #1
	add r0, r0, r3
	ldrb r1, [r0]
	add r0, r1, #0
	and r0, r2
	lsl r0, r0, #2
	add r0, r0, r4
	lsr r1, r1, #4
	ldr r0, [r0]
	lsr r0, r1
	mov r1, ip
	ldr r4, [r1, #4]
	sub r0, r0, r5
	add r1, r7, #0
	bl multiplyUnsignedHigh
	add r1, r0, #0
	add r1, r5, r1
	add r0, r4, #0
	bl multiplyUnsignedHigh
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
_08036494: .4byte unk_8337700
_08036498: .4byte unk_83377B4



	thumb_func_global nullsub_20
nullsub_20: @ 0x0803649C
	bx lr
	.align 2, 0
	
	

thumb_func_global MPlayContinue
MPlayContinue: @ 0x080364A0
	add r2, r0, #0
	ldr r3, [r2, #0x34]
	ldr r0, _080364B4
	cmp r3, r0
	bne _080364B2
	ldr r0, [r2, #4]
	ldr r1, _080364B8
	and r0, r1
	str r0, [r2, #4]
_080364B2:
	bx lr
	.align 2, 0
_080364B4: .4byte 0x68736D53
_080364B8: .4byte 0x7FFFFFFF

thumb_func_global MPlayFadeOut
MPlayFadeOut: @ 0x080364BC
	add r2, r0, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r3, [r2, #0x34]
	ldr r0, _080364D8
	cmp r3, r0
	bne _080364D4
	strh r1, [r2, #0x26]
	strh r1, [r2, #0x24]
	mov r0, #0x80
	lsl r0, r0, #1
	strh r0, [r2, #0x28]
_080364D4:
	bx lr
	.align 2, 0
_080364D8: .4byte 0x68736D53


thumb_func_global m4aSoundInit
m4aSoundInit: @ 0x080364DC
	push {r4, r5, r6, lr}
	ldr r0, _08036530
	mov r1, #2
	neg r1, r1
	and r0, r1
	ldr r1, _08036534
	ldr r2, _08036538
	bl Bios_memcpy
	ldr r0, _0803653C
	bl sub_080368DC
	ldr r0, _08036540
	bl MPlayExtender
	ldr r0, _08036544
	bl SoundMode_rev01
	ldr r0, _08036548
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0
	beq _0803652A
	ldr r5, _0803654C
	add r6, r0, #0
_0803650E:
	ldr r4, [r5]
	ldr r1, [r5, #4]
	ldrb r2, [r5, #8]
	add r0, r4, #0
	bl sub_08036C1C
	ldrh r0, [r5, #0xa]
	strb r0, [r4, #0xb]
	ldr r0, _08036550
	str r0, [r4, #0x18]
	add r5, #0xc
	sub r6, #1
	cmp r6, #0
	bne _0803650E
_0803652A:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_08036530: .4byte (SoundMainRAM+1)
_08036534: .4byte 0x03006C38
_08036538: .4byte 0x04000100
_0803653C: .4byte 0x02000690
_08036540: .4byte 0x020016D0
_08036544: .4byte 0x0095FA00
_08036548: .4byte 0x00000005
_0803654C: .4byte musicPlayer_1
_08036550: .4byte 0x020018D0


thumb_func_global m4aSoundMain
m4aSoundMain: @ 0x08036554
	push {lr}
	bl SoundMain
	pop {r0}
	bx r0
	.align 2, 0


thumb_func_global song_play_and_auto_config
song_play_and_auto_config: @ 0x08036560
	push {lr}
	lsl r0, r0, #0x10
	ldr r2, _08036584
	ldr r1, _08036588
	lsr r0, r0, #0xd
	add r0, r0, r1
	ldrh r3, [r0, #4]
	lsl r1, r3, #1
	add r1, r1, r3
	lsl r1, r1, #2
	add r1, r1, r2
	ldr r2, [r1]
	ldr r1, [r0]
	add r0, r2, #0
	bl MPlayStart_rev01
	pop {r0}
	bx r0
	.align 2, 0
_08036584: .4byte musicPlayer_1
_08036588: .4byte m4a_songs

thumb_func_global m4aSongNumStartOrChange
m4aSongNumStartOrChange: @ 0x0803658C
	push {lr}
	lsl r0, r0, #0x10
	ldr r2, _080365B8
	ldr r1, _080365BC
	lsr r0, r0, #0xd
	add r0, r0, r1
	ldrh r3, [r0, #4]
	lsl r1, r3, #1
	add r1, r1, r3
	lsl r1, r1, #2
	add r1, r1, r2
	ldr r1, [r1]
	ldr r3, [r1]
	ldr r2, [r0]
	cmp r3, r2
	beq _080365C0
	add r0, r1, #0
	add r1, r2, #0
	bl MPlayStart_rev01
	b _080365D4
	.align 2, 0
_080365B8: .4byte musicPlayer_1
_080365BC: .4byte m4a_songs
_080365C0:
	ldr r2, [r1, #4]
	ldrh r0, [r1, #4]
	cmp r0, #0
	beq _080365CC
	cmp r2, #0
	bge _080365D4
_080365CC:
	add r0, r1, #0
	add r1, r3, #0
	bl MPlayStart_rev01
_080365D4:
	pop {r0}
	bx r0

thumb_func_global m4aSongNumStartOrContinue
m4aSongNumStartOrContinue: @ 0x080365D8
	push {lr}
	lsl r0, r0, #0x10
	ldr r2, _08036604
	ldr r1, _08036608
	lsr r0, r0, #0xd
	add r0, r0, r1
	ldrh r3, [r0, #4]
	lsl r1, r3, #1
	add r1, r1, r3
	lsl r1, r1, #2
	add r1, r1, r2
	ldr r1, [r1]
	ldr r3, [r1]
	ldr r2, [r0]
	cmp r3, r2
	beq _0803660C
	add r0, r1, #0
	add r1, r2, #0
	bl MPlayStart_rev01
	b _08036628
	.align 2, 0
_08036604: .4byte musicPlayer_1
_08036608: .4byte m4a_songs
_0803660C:
	ldr r2, [r1, #4]
	ldrh r0, [r1, #4]
	cmp r0, #0
	bne _0803661E
	add r0, r1, #0
	add r1, r3, #0
	bl MPlayStart_rev01
	b _08036628
_0803661E:
	cmp r2, #0
	bge _08036628
	add r0, r1, #0
	bl MPlayContinue
_08036628:
	pop {r0}
	bx r0

thumb_func_global m4aSongNumStop
m4aSongNumStop: @ 0x0803662C
	push {lr}
	lsl r0, r0, #0x10
	ldr r2, _08036658
	ldr r1, _0803665C
	lsr r0, r0, #0xd
	add r0, r0, r1
	ldrh r3, [r0, #4]
	lsl r1, r3, #1
	add r1, r1, r3
	lsl r1, r1, #2
	add r1, r1, r2
	ldr r2, [r1]
	ldr r1, [r2]
	ldr r0, [r0]
	cmp r1, r0
	bne _08036652
	add r0, r2, #0
	bl MPlayStop_rev01
_08036652:
	pop {r0}
	bx r0
	.align 2, 0
_08036658: .4byte musicPlayer_1
_0803665C: .4byte m4a_songs

thumb_func_global m4aSongNumContinue
m4aSongNumContinue: @ 0x08036660
	push {lr}
	lsl r0, r0, #0x10
	ldr r2, _0803668C
	ldr r1, _08036690
	lsr r0, r0, #0xd
	add r0, r0, r1
	ldrh r3, [r0, #4]
	lsl r1, r3, #1
	add r1, r1, r3
	lsl r1, r1, #2
	add r1, r1, r2
	ldr r2, [r1]
	ldr r1, [r2]
	ldr r0, [r0]
	cmp r1, r0
	bne _08036686
	add r0, r2, #0
	bl MPlayContinue
_08036686:
	pop {r0}
	bx r0
	.align 2, 0
_0803668C: .4byte musicPlayer_1
_08036690: .4byte m4a_songs

thumb_func_global m4aMPlayAllStop
m4aMPlayAllStop: @ 0x08036694
	push {r4, r5, lr}
	ldr r0, _080366B8
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0
	beq _080366B2
	ldr r5, _080366BC
	add r4, r0, #0
_080366A4:
	ldr r0, [r5]
	bl MPlayStop_rev01
	add r5, #0xc
	sub r4, #1
	cmp r4, #0
	bne _080366A4
_080366B2:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_080366B8: .4byte 0x00000005
_080366BC: .4byte musicPlayer_1

thumb_func_global m4aMPlayContinue
m4aMPlayContinue: @ 0x080366C0
	push {lr}
	bl MPlayContinue
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global m4aMPlayAllContinue
m4aMPlayAllContinue: @ 0x080366CC
	push {r4, r5, lr}
	ldr r0, _080366F0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0
	beq _080366EA
	ldr r5, _080366F4
	add r4, r0, #0
_080366DC:
	ldr r0, [r5]
	bl MPlayContinue
	add r5, #0xc
	sub r4, #1
	cmp r4, #0
	bne _080366DC
_080366EA:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_080366F0: .4byte 0x00000005
_080366F4: .4byte musicPlayer_1

thumb_func_global m4aMPlayFadeOut
m4aMPlayFadeOut: @ 0x080366F8
	push {lr}
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl MPlayFadeOut
	pop {r0}
	bx r0
	
	
	
	.align 2, 0
	thumb_func_global sub_8036708
sub_8036708: @ 0x08036708
	add r2, r0, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r3, [r2, #0x34]
	ldr r0, _08036720
	cmp r3, r0
	bne locret_803671E
	strh r1, [r2, #0x26]
	strh r1, [r2, #0x24]
	ldr r0, _08036724
	strh r0, [r2, #0x28]
locret_803671E:
	bx lr
	.align 2, 0
_08036720: .4byte 0x68736D53
_08036724: .4byte 0x00000101



	thumb_func_global sub_8036728
sub_8036728: @ 0x08036728
	add r2, r0, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r3, [r2, #0x34]
	ldr r0, _08036748
	cmp r3, r0
	bne locret_8036746
	strh r1, [r2, #0x26]
	strh r1, [r2, #0x24]
	mov r0, #2
	strh r0, [r2, #0x28]
	ldr r0, [r2, #4]
	ldr r1, _0803674C
	and r0, r1
	str r0, [r2, #4]
locret_8036746:
	bx lr
	.align 2, 0
_08036748: .4byte 0x68736D53
_0803674C: .4byte 0x7FFFFFFF



	thumb_func_global sub_8036750
sub_8036750: @ 0x08036750
	push {r4, r5, r6, r7, lr}
	ldrb r5, [r0, #8]
	ldr r4, [r0, #0x2c]
	cmp r5, #0
	ble _08036792
	mov r7, #0x80
_0803675C:
	ldrb r1, [r4]
	add r0, r7, #0
	and r0, r1
	cmp r0, #0
	beq _0803678A
	mov r6, #0x40
	add r0, r6, #0
	and r0, r1
	cmp r0, #0
	beq _0803678A
	add r0, r4, #0
	bl Clear64byte_rev_maybe
	strb r7, [r4]
	mov r0, #2
	strb r0, [r4, #0xf]
	strb r6, [r4, #0x13]
	mov r0, #0x16
	strb r0, [r4, #0x19]
	add r1, r4, #0
	add r1, #0x24
	mov r0, #1
	strb r0, [r1]
_0803678A:
	sub r5, #1
	add r4, #0x50
	cmp r5, #0
	bgt _0803675C
_08036792:
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	
	
	
	
thumb_func_global MPlayExtender
MPlayExtender: @ 0x08036798
	push {r4, r5, r6, lr}
	sub sp, #4
	add r5, r0, #0
	ldr r1, _08036860
	mov r0, #0x8f
	strh r0, [r1]
	ldr r3, _08036864
	mov r2, #0
	strh r2, [r3]
	ldr r0, _08036868
	mov r1, #8
	strb r1, [r0]
	add r0, #6
	strb r1, [r0]
	add r0, #0x10
	strb r1, [r0]
	sub r0, #0x14
	mov r1, #0x80
	strb r1, [r0]
	add r0, #8
	strb r1, [r0]
	add r0, #0x10
	strb r1, [r0]
	sub r0, #0xd
	strb r2, [r0]
	mov r0, #0x77
	strb r0, [r3]
	ldr r0, _0803686C
	ldr r4, [r0]
	ldr r6, [r4]
	ldr r0, _08036870
	cmp r6, r0
	bne _08036858
	add r0, r6, #1
	str r0, [r4]
	ldr r1, _08036874
	ldr r0, _08036878
	str r0, [r1, #0x20]
	ldr r0, _0803687C
	str r0, [r1, #0x44]
	ldr r0, _08036880
	str r0, [r1, #0x4c]
	ldr r0, _08036884
	str r0, [r1, #0x70]
	ldr r0, _08036888
	str r0, [r1, #0x74]
	ldr r0, _0803688C
	str r0, [r1, #0x78]
	ldr r0, _08036890
	str r0, [r1, #0x7c]
	add r2, r1, #0
	add r2, #0x80
	ldr r0, _08036894
	str r0, [r2]
	add r1, #0x84
	ldr r0, _08036898
	str r0, [r1]
	str r5, [r4, #0x1c]
	ldr r0, _0803689C
	str r0, [r4, #0x28]
	ldr r0, _080368A0
	str r0, [r4, #0x2c]
	ldr r0, _080368A4
	str r0, [r4, #0x30]
	ldr r0, _080368A8
	mov r1, #0
	strb r0, [r4, #0xc]
	str r1, [sp]
	ldr r2, _080368AC
	mov r0, sp
	add r1, r5, #0
	bl Bios_memcpy
	mov r0, #1
	strb r0, [r5, #1]
	mov r0, #0x11
	strb r0, [r5, #0x1c]
	add r1, r5, #0
	add r1, #0x41
	mov r0, #2
	strb r0, [r1]
	add r1, #0x1b
	mov r0, #0x22
	strb r0, [r1]
	add r1, #0x25
	mov r0, #3
	strb r0, [r1]
	add r1, #0x1b
	mov r0, #0x44
	strb r0, [r1]
	add r1, #0x24
	mov r0, #4
	strb r0, [r1, #1]
	mov r0, #0x88
	strb r0, [r1, #0x1c]
	str r6, [r4]
_08036858:
	add sp, #4
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_08036860: .4byte 0x04000084
_08036864: .4byte 0x04000080
_08036868: .4byte 0x04000063
_0803686C: .4byte 0x03007FF0
_08036870: .4byte 0x68736D53
_08036874: .4byte 0x02001640
_08036878: .4byte (ply_memacc+1)
_0803687C: .4byte (ply_lfos_rev01+1)
_08036880: .4byte (ply_mod_rev01+1)
_08036884: .4byte (ply_xcmd+1)
_08036888: .4byte (ply_endtie_rev01+1)
_0803688C: .4byte (SampFreqSet_rev01+1)
_08036890: .4byte (TrackStop_rev01+1)
_08036894: .4byte (FadeOutBody_rev01+1)
_08036898: .4byte (TrkVolPitSet_rev01+1)
_0803689C: .4byte (CgbSound+1)
_080368A0: .4byte (CgbOscOff+1)
_080368A4: .4byte (MidiKey2CgbFr+1)
_080368A8: .4byte 0x00000000
_080368AC: .4byte 0x05000040

thumb_func_global MusicPlayerJumpTableCopy
MusicPlayerJumpTableCopy: @ 0x080368B0
	svc #0x2a
	bx lr

thumb_func_global ClearChain_rev
ClearChain_rev: @ 0x080368B4
	push {lr}
	ldr r1, _080368C4
	ldr r1, [r1]
	bl call_r1
	pop {r0}
	bx r0
	.align 2, 0
_080368C4: .4byte 0x020016C8

thumb_func_global Clear64byte_rev_maybe
Clear64byte_rev_maybe: @ 0x080368C8
	push {lr}
	ldr r1, _080368D8
	ldr r1, [r1]
	bl call_r1
	pop {r0}
	bx r0
	.align 2, 0
_080368D8: .4byte 0x020016CC

thumb_func_global sub_080368DC
sub_080368DC: @ 0x080368DC
	push {r4, r5, lr}
	sub sp, #4
	add r5, r0, #0
	mov r3, #0
	str r3, [r5]
	ldr r1, _08036994
	ldr r0, [r1]
	mov r2, #0x80
	lsl r2, r2, #0x12
	and r0, r2
	cmp r0, #0
	beq _080368F8
	ldr r0, _08036998
	str r0, [r1]
_080368F8:
	ldr r1, _0803699C
	ldr r0, [r1]
	and r0, r2
	cmp r0, #0
	beq _08036906
	ldr r0, _08036998
	str r0, [r1]
_08036906:
	ldr r0, _080369A0
	mov r2, #0x80
	lsl r2, r2, #3
	add r1, r2, #0
	strh r1, [r0]
	add r0, #0xc
	strh r1, [r0]
	ldr r1, _080369A4
	mov r0, #0x8f
	strh r0, [r1]
	sub r1, #2
	ldr r2, _080369A8
	add r0, r2, #0
	strh r0, [r1]
	ldr r2, _080369AC
	ldrb r1, [r2]
	mov r0, #0x3f
	and r0, r1
	mov r1, #0x40
	orr r0, r1
	strb r0, [r2]
	ldr r1, _080369B0
	mov r2, #0xd4
	lsl r2, r2, #2
	add r0, r5, r2
	str r0, [r1]
	add r1, #4
	ldr r0, _080369B4
	str r0, [r1]
	add r1, #8
	mov r2, #0x98
	lsl r2, r2, #4
	add r0, r5, r2
	str r0, [r1]
	add r1, #4
	ldr r0, _080369B8
	str r0, [r1]
	ldr r0, _080369BC
	str r5, [r0]
	str r3, [sp]
	ldr r2, _080369C0
	mov r0, sp
	add r1, r5, #0
	bl Bios_memcpy
	mov r0, #8
	strb r0, [r5, #6]
	mov r0, #0xf
	strb r0, [r5, #7]
	ldr r0, _080369C4
	str r0, [r5, #0x38]
	ldr r0, _080369C8
	str r0, [r5, #0x28]
	str r0, [r5, #0x2c]
	str r0, [r5, #0x30]
	str r0, [r5, #0x3c]
	ldr r4, _080369CC
	add r0, r4, #0
	bl sub_08035CBC
	str r4, [r5, #0x34]
	mov r0, #0x80
	lsl r0, r0, #0xb
	bl SampFreqSet_rev01
	ldr r0, _080369D0
	str r0, [r5]
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_08036994: .4byte 0x040000C4
_08036998: .4byte 0x84400004
_0803699C: .4byte 0x040000D0
_080369A0: .4byte 0x040000C6
_080369A4: .4byte 0x04000084
_080369A8: .4byte 0x0000A90E
_080369AC: .4byte 0x04000089
_080369B0: .4byte 0x040000BC
_080369B4: .4byte 0x040000A0
_080369B8: .4byte 0x040000A4
_080369BC: .4byte 0x03007FF0
_080369C0: .4byte 0x050003EC
_080369C4: .4byte (sub_80361A8+1)
_080369C8: .4byte (nullsub_13+1)
_080369CC: .4byte 0x02001640
_080369D0: .4byte 0x68736D53

thumb_func_global SampFreqSet_rev01
SampFreqSet_rev01: @ 0x080369D4
	push {r4, r5, r6, lr}
	add r2, r0, #0
	ldr r0, _08036A54
	ldr r4, [r0]
	mov r0, #0xf0
	lsl r0, r0, #0xc
	and r0, r2
	lsr r2, r0, #0x10
	mov r6, #0
	strb r2, [r4, #8]
	ldr r1, _08036A58
	sub r0, r2, #1
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r5, [r0]
	str r5, [r4, #0x10]
	mov r0, #0xc6
	lsl r0, r0, #3
	add r1, r5, #0
	bl divide
	strb r0, [r4, #0xb]
	ldr r0, _08036A5C
	mul r0, r5, r0
	ldr r1, _08036A60
	add r0, r0, r1
	ldr r1, _08036A64
	bl divide
	add r1, r0, #0
	str r1, [r4, #0x14]
	mov r0, #0x80
	lsl r0, r0, #0x11
	bl divide
	add r0, #1
	asr r0, r0, #1
	str r0, [r4, #0x18]
	ldr r0, _08036A68
	strh r6, [r0]
	ldr r4, _08036A6C
	ldr r0, _08036A70
	add r1, r5, #0
	bl divide
	neg r0, r0
	strh r0, [r4]
	bl sub_08036BE0
	ldr r1, _08036A74
_08036A38:
	ldrb r0, [r1]
	cmp r0, #0x9f
	beq _08036A38
	ldr r1, _08036A74
_08036A40:
	ldrb r0, [r1]
	cmp r0, #0x9f
	bne _08036A40
	ldr r1, _08036A68
	mov r0, #0x80
	strh r0, [r1]
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_08036A54: .4byte 0x03007FF0
_08036A58: .4byte unk_83377E4
_08036A5C: .4byte 0x00091D1B
_08036A60: .4byte 0x00001388
_08036A64: .4byte 0x00002710
_08036A68: .4byte 0x04000102
_08036A6C: .4byte 0x04000100
_08036A70: .4byte 0x00044940
_08036A74: .4byte 0x04000006

thumb_func_global SoundMode_rev01
SoundMode_rev01: @ 0x08036A78
	push {r4, r5, lr}
	add r3, r0, #0
	ldr r0, _08036B04
	ldr r5, [r0]
	ldr r1, [r5]
	ldr r0, _08036B08
	cmp r1, r0
	bne _08036AFE
	add r0, r1, #1
	str r0, [r5]
	mov r4, #0xff
	and r4, r3
	cmp r4, #0
	beq _08036A9A
	mov r0, #0x7f
	and r4, r0
	strb r4, [r5, #5]
_08036A9A:
	mov r4, #0xf0
	lsl r4, r4, #4
	and r4, r3
	cmp r4, #0
	beq _08036ABA
	lsr r0, r4, #8
	strb r0, [r5, #6]
	mov r4, #0xc
	add r0, r5, #0
	add r0, #0x50
	mov r1, #0
_08036AB0:
	strb r1, [r0]
	sub r4, #1
	add r0, #0x40
	cmp r4, #0
	bne _08036AB0
_08036ABA:
	mov r4, #0xf0
	lsl r4, r4, #8
	and r4, r3
	cmp r4, #0
	beq _08036AC8
	lsr r0, r4, #0xc
	strb r0, [r5, #7]
_08036AC8:
	mov r4, #0xb0
	lsl r4, r4, #0x10
	and r4, r3
	cmp r4, #0
	beq _08036AE6
	mov r0, #0xc0
	lsl r0, r0, #0xe
	and r0, r4
	lsr r4, r0, #0xe
	ldr r2, _08036B0C
	ldrb r1, [r2]
	mov r0, #0x3f
	and r0, r1
	orr r0, r4
	strb r0, [r2]
_08036AE6:
	mov r4, #0xf0
	lsl r4, r4, #0xc
	and r4, r3
	cmp r4, #0
	beq _08036AFA
	bl muteSound
	add r0, r4, #0
	bl SampFreqSet_rev01
_08036AFA:
	ldr r0, _08036B08
	str r0, [r5]
_08036AFE:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_08036B04: .4byte 0x03007FF0
_08036B08: .4byte 0x68736D53
_08036B0C: .4byte 0x04000089
	
	
	thumb_func_global sub_8036B10
sub_8036B10: @ 0x08036B10
	push {r4, r5, r6, r7, lr}
	ldr r0, _08036B5C
	ldr r6, [r0]
	ldr r1, [r6]
	ldr r0, _08036B60
	cmp r1, r0
	bne _08036B56
	add r0, r1, #1
	str r0, [r6]
	mov r5, #0xc
	add r4, r6, #0
	add r4, #0x50
	mov r0, #0
_08036B2A:
	strb r0, [r4]
	sub r5, #1
	add r4, #0x40
	cmp r5, #0
	bgt _08036B2A
	ldr r4, [r6, #0x1c]
	cmp r4, #0
	beq _08036B52
	mov r5, #1
	mov r7, #0
_08036B3E:
	lsl r0, r5, #0x18
	lsr r0, r0, #0x18
	ldr r1, [r6, #0x2c]
	bl call_r1
	strb r7, [r4]
	add r5, #1
	add r4, #0x40
	cmp r5, #4
	ble _08036B3E
_08036B52:
	ldr r0, _08036B60
	str r0, [r6]
_08036B56:
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_08036B5C: .4byte 0x03007FF0
_08036B60: .4byte 0x68736D53
	
	

thumb_func_global muteSound
muteSound: @ 0x08036B64
	push {lr}
	sub sp, #4
	ldr r0, _08036BC4
	ldr r2, [r0]
	ldr r1, [r2]
	ldr r3, _08036BC8
	add r0, r1, r3
	cmp r0, #1
	bhi _08036BBC
	add r0, r1, #0
	add r0, #0xa
	str r0, [r2]
	ldr r1, _08036BCC
	ldr r0, [r1]
	mov r3, #0x80
	lsl r3, r3, #0x12
	and r0, r3
	cmp r0, #0
	beq _08036B8E
	ldr r0, _08036BD0
	str r0, [r1]
_08036B8E:
	ldr r1, _08036BD4
	ldr r0, [r1]
	and r0, r3
	cmp r0, #0
	beq _08036B9C
	ldr r0, _08036BD0
	str r0, [r1]
_08036B9C:
	ldr r0, _08036BD8
	mov r3, #0x80
	lsl r3, r3, #3
	add r1, r3, #0
	strh r1, [r0]
	add r0, #0xc
	strh r1, [r0]
	mov r0, #0
	str r0, [sp]
	mov r0, #0xd4
	lsl r0, r0, #2
	add r1, r2, r0
	ldr r2, _08036BDC
	mov r0, sp
	bl Bios_memcpy
_08036BBC:
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
_08036BC4: .4byte 0x03007FF0
_08036BC8: .4byte 0x978C92AD
_08036BCC: .4byte 0x040000C4
_08036BD0: .4byte 0x84400004
_08036BD4: .4byte 0x040000D0
_08036BD8: .4byte 0x040000C6
_08036BDC: .4byte 0x05000318

thumb_func_global sub_08036BE0
sub_08036BE0: @ 0x08036BE0
	push {r4, lr}
	ldr r0, _08036C10
	ldr r2, [r0]
	ldr r3, [r2]
	ldr r0, _08036C14
	cmp r3, r0
	beq _08036C08
	ldr r0, _08036C18
	mov r4, #0xb6
	lsl r4, r4, #8
	add r1, r4, #0
	strh r1, [r0]
	add r0, #0xc
	strh r1, [r0]
	ldrb r0, [r2, #4]
	mov r0, #0
	strb r0, [r2, #4]
	add r0, r3, #0
	sub r0, #0xa
	str r0, [r2]
_08036C08:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_08036C10: .4byte 0x03007FF0
_08036C14: .4byte 0x68736D53
_08036C18: .4byte 0x040000C6

thumb_func_global sub_08036C1C
sub_08036C1C: @ 0x08036C1C
	push {r4, r5, r6, r7, lr}
	add r7, r0, #0
	add r6, r1, #0
	lsl r2, r2, #0x18
	lsr r4, r2, #0x18
	cmp r4, #0
	beq _08036C80
	cmp r4, #0x10
	bls _08036C30
	mov r4, #0x10
_08036C30:
	ldr r0, _08036C88
	ldr r5, [r0]
	ldr r1, [r5]
	ldr r0, _08036C8C
	cmp r1, r0
	bne _08036C80
	add r0, r1, #1
	str r0, [r5]
	add r0, r7, #0
	bl Clear64byte_rev_maybe
	str r6, [r7, #0x2c]
	strb r4, [r7, #8]
	mov r0, #0x80
	lsl r0, r0, #0x18
	str r0, [r7, #4]
	cmp r4, #0
	beq _08036C64
	mov r1, #0
_08036C56:
	strb r1, [r6]
	sub r0, r4, #1
	lsl r0, r0, #0x18
	lsr r4, r0, #0x18
	add r6, #0x50
	cmp r4, #0
	bne _08036C56
_08036C64:
	ldr r0, [r5, #0x20]
	cmp r0, #0
	beq _08036C74
	str r0, [r7, #0x38]
	ldr r0, [r5, #0x24]
	str r0, [r7, #0x3c]
	mov r0, #0
	str r0, [r5, #0x20]
_08036C74:
	str r7, [r5, #0x24]
	ldr r0, _08036C90
	str r0, [r5, #0x20]
	ldr r0, _08036C8C
	str r0, [r5]
	str r0, [r7, #0x34]
_08036C80:
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_08036C88: .4byte 0x03007FF0
_08036C8C: .4byte 0x68736D53
_08036C90: .4byte (sub_8035ECC+1)

thumb_func_global MPlayStart_rev01
MPlayStart_rev01: @ 0x08036C94
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	add r5, r0, #0
	add r7, r1, #0
	ldr r1, [r5, #0x34]
	ldr r0, _08036D74
	cmp r1, r0
	bne _08036D6A
	ldrb r0, [r5, #0xb]
	ldrb r2, [r7, #2]
	cmp r0, #0
	beq _08036CD6
	ldr r0, [r5]
	cmp r0, #0
	beq _08036CC0
	ldr r1, [r5, #0x2c]
	mov r0, #0x40
	ldrb r1, [r1]
	and r0, r1
	cmp r0, #0
	bne _08036CCC
_08036CC0:
	ldr r1, [r5, #4]
	ldrh r0, [r5, #4]
	cmp r0, #0
	beq _08036CD6
	cmp r1, #0
	blt _08036CD6
_08036CCC:
	ldrb r0, [r7, #2]
	add r2, r0, #0
	ldrb r0, [r5, #9]
	cmp r0, r2
	bhi _08036D6A
_08036CD6:
	ldr r0, [r5, #0x34]
	add r0, #1
	str r0, [r5, #0x34]
	mov r1, #0
	str r1, [r5, #4]
	str r7, [r5]
	ldr r0, [r7, #4]
	str r0, [r5, #0x30]
	strb r2, [r5, #9]
	str r1, [r5, #0xc]
	mov r0, #0x96
	strh r0, [r5, #0x1c]
	strh r0, [r5, #0x20]
	add r0, #0x6a
	strh r0, [r5, #0x1e]
	strh r1, [r5, #0x22]
	strh r1, [r5, #0x24]
	mov r6, #0
	ldr r4, [r5, #0x2c]
	ldrb r1, [r7]
	cmp r6, r1
	bge _08036D36
	ldrb r0, [r5, #8]
	cmp r6, r0
	bge _08036D56
	mov r8, r6
_08036D0A:
	add r0, r5, #0
	add r1, r4, #0
	bl TrackStop_rev01
	mov r0, #0xc0
	strb r0, [r4]
	mov r1, r8
	str r1, [r4, #0x20]
	lsl r1, r6, #2
	add r0, r7, #0
	add r0, #8
	add r0, r0, r1
	ldr r0, [r0]
	str r0, [r4, #0x40]
	add r6, #1
	add r4, #0x50
	ldrb r0, [r7]
	cmp r6, r0
	bge _08036D36
	ldrb r1, [r5, #8]
	cmp r6, r1
	blt _08036D0A
_08036D36:
	ldrb r0, [r5, #8]
	cmp r6, r0
	bge _08036D56
	mov r1, #0
	mov r8, r1
_08036D40:
	add r0, r5, #0
	add r1, r4, #0
	bl TrackStop_rev01
	mov r0, r8
	strb r0, [r4]
	add r6, #1
	add r4, #0x50
	ldrb r1, [r5, #8]
	cmp r6, r1
	blt _08036D40
_08036D56:
	mov r0, #0x80
	ldrb r1, [r7, #3]
	and r0, r1
	cmp r0, #0
	beq _08036D66
	ldrb r0, [r7, #3]
	bl SoundMode_rev01
_08036D66:
	ldr r0, _08036D74
	str r0, [r5, #0x34]
_08036D6A:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_08036D74: .4byte 0x68736D53

thumb_func_global MPlayStop_rev01
MPlayStop_rev01: @ 0x08036D78
	push {r4, r5, r6, lr}
	add r6, r0, #0
	ldr r1, [r6, #0x34]
	ldr r0, _08036DB4
	cmp r1, r0
	bne _08036DAE
	add r0, r1, #1
	str r0, [r6, #0x34]
	ldr r0, [r6, #4]
	mov r1, #0x80
	lsl r1, r1, #0x18
	orr r0, r1
	str r0, [r6, #4]
	ldrb r4, [r6, #8]
	ldr r5, [r6, #0x2c]
	cmp r4, #0
	ble _08036DAA
_08036D9A:
	add r0, r6, #0
	add r1, r5, #0
	bl TrackStop_rev01
	sub r4, #1
	add r5, #0x50
	cmp r4, #0
	bgt _08036D9A
_08036DAA:
	ldr r0, _08036DB4
	str r0, [r6, #0x34]
_08036DAE:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_08036DB4: .4byte 0x68736D53

thumb_func_global FadeOutBody_rev01
FadeOutBody_rev01: @ 0x08036DB8
	push {r4, r5, r6, r7, lr}
	add r6, r0, #0
	ldrh r1, [r6, #0x24]
	cmp r1, #0
	beq _08036E7A
	ldrh r0, [r6, #0x26]
	sub r0, #1
	strh r0, [r6, #0x26]
	ldr r3, _08036DF8
	add r2, r3, #0
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, #0
	bne _08036E7A
	strh r1, [r6, #0x26]
	ldrh r1, [r6, #0x28]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _08036DFC
	add r0, r1, #0
	add r0, #0x10
	strh r0, [r6, #0x28]
	and r0, r2
	cmp r0, #0xff
	bls _08036E4E
	mov r0, #0x80
	lsl r0, r0, #1
	strh r0, [r6, #0x28]
	strh r3, [r6, #0x24]
	b _08036E4E
	.align 2, 0
_08036DF8: .4byte 0x0000FFFF
_08036DFC:
	add r0, r1, #0
	sub r0, #0x10
	strh r0, [r6, #0x28]
	and r0, r2
	lsl r0, r0, #0x10
	cmp r0, #0
	bgt _08036E4E
	ldrb r5, [r6, #8]
	ldr r4, [r6, #0x2c]
	cmp r5, #0
	ble _08036E2E
_08036E12:
	add r0, r6, #0
	add r1, r4, #0
	bl TrackStop_rev01
	mov r0, #1
	ldrh r7, [r6, #0x28]
	and r0, r7
	cmp r0, #0
	bne _08036E26
	strb r0, [r4]
_08036E26:
	sub r5, #1
	add r4, #0x50
	cmp r5, #0
	bgt _08036E12
_08036E2E:
	mov r0, #1
	ldrh r1, [r6, #0x28]
	and r0, r1
	cmp r0, #0
	beq _08036E42
	ldr r0, [r6, #4]
	mov r1, #0x80
	lsl r1, r1, #0x18
	orr r0, r1
	b _08036E46
_08036E42:
	mov r0, #0x80
	lsl r0, r0, #0x18
_08036E46:
	str r0, [r6, #4]
	mov r0, #0
	strh r0, [r6, #0x24]
	b _08036E7A
_08036E4E:
	ldrb r5, [r6, #8]
	ldr r4, [r6, #0x2c]
	cmp r5, #0
	ble _08036E7A
	mov r3, #0x80
	mov r7, #0
	mov r2, #3
_08036E5C:
	ldrb r1, [r4]
	add r0, r3, #0
	and r0, r1
	cmp r0, #0
	beq _08036E72
	ldrh r7, [r6, #0x28]
	lsr r0, r7, #2
	strb r0, [r4, #0x13]
	add r0, r1, #0
	orr r0, r2
	strb r0, [r4]
_08036E72:
	sub r5, #1
	add r4, #0x50
	cmp r5, #0
	bgt _08036E5C
_08036E7A:
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0

thumb_func_global TrkVolPitSet_rev01
TrkVolPitSet_rev01: @ 0x08036E80
	push {r4, lr}
	add r2, r1, #0
	mov r0, #1
	ldrb r1, [r2]
	and r0, r1
	cmp r0, #0
	beq _08036EE4
	ldrb r3, [r2, #0x13]
	ldrb r1, [r2, #0x12]
	add r0, r3, #0
	mul r0, r1, r0
	lsr r3, r0, #5
	ldrb r4, [r2, #0x18]
	cmp r4, #1
	bne _08036EA8
	mov r0, #0x16
	ldrsb r0, [r2, r0]
	add r0, #0x80
	mul r0, r3, r0
	lsr r3, r0, #7
_08036EA8:
	mov r0, #0x14
	ldrsb r0, [r2, r0]
	lsl r0, r0, #1
	mov r1, #0x15
	ldrsb r1, [r2, r1]
	add r1, r0, r1
	cmp r4, #2
	bne _08036EBE
	mov r0, #0x16
	ldrsb r0, [r2, r0]
	add r1, r1, r0
_08036EBE:
	mov r0, #0x80
	neg r0, r0
	cmp r1, r0
	bge _08036ECA
	add r1, r0, #0
	b _08036ED0
_08036ECA:
	cmp r1, #0x7f
	ble _08036ED0
	mov r1, #0x7f
_08036ED0:
	add r0, r1, #0
	add r0, #0x80
	mul r0, r3, r0
	lsr r0, r0, #8
	strb r0, [r2, #0x10]
	mov r0, #0x7f
	sub r0, r0, r1
	mul r0, r3, r0
	lsr r0, r0, #8
	strb r0, [r2, #0x11]
_08036EE4:
	ldrb r1, [r2]
	mov r0, #4
	and r0, r1
	add r3, r1, #0
	cmp r0, #0
	beq _08036F28
	mov r0, #0xe
	ldrsb r0, [r2, r0]
	ldrb r1, [r2, #0xf]
	mul r0, r1, r0
	mov r1, #0xc
	ldrsb r1, [r2, r1]
	add r1, r1, r0
	lsl r1, r1, #2
	mov r0, #0xa
	ldrsb r0, [r2, r0]
	lsl r0, r0, #8
	add r1, r1, r0
	mov r0, #0xb
	ldrsb r0, [r2, r0]
	lsl r0, r0, #8
	add r1, r1, r0
	ldrb r0, [r2, #0xd]
	add r1, r0, r1
	ldrb r0, [r2, #0x18]
	cmp r0, #0
	bne _08036F22
	mov r0, #0x16
	ldrsb r0, [r2, r0]
	lsl r0, r0, #4
	add r1, r1, r0
_08036F22:
	asr r0, r1, #8
	strb r0, [r2, #8]
	strb r1, [r2, #9]
_08036F28:
	mov r0, #0xfa
	and r0, r3
	strb r0, [r2]
	pop {r4}
	pop {r0}
	bx r0

thumb_func_global MidiKey2CgbFr
MidiKey2CgbFr: @ 0x08036F34
	push {r4, r5, r6, r7, lr}
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	lsl r1, r1, #0x18
	lsr r5, r1, #0x18
	lsl r2, r2, #0x18
	lsr r2, r2, #0x18
	mov ip, r2
	cmp r0, #4
	bne _08036F6C
	cmp r5, #0x14
	bhi _08036F50
	mov r5, #0
	b _08036F5E
_08036F50:
	add r0, r5, #0
	sub r0, #0x15
	lsl r0, r0, #0x18
	lsr r5, r0, #0x18
	cmp r5, #0x3b
	bls _08036F5E
	mov r5, #0x3b
_08036F5E:
	ldr r0, _08036F68
	add r0, r5, r0
	ldrb r0, [r0]
	b _08036FCE
	.align 2, 0
_08036F68: .4byte unk_8337898
_08036F6C:
	cmp r5, #0x23
	bhi _08036F78
	mov r0, #0
	mov ip, r0
	mov r5, #0
	b _08036F8A
_08036F78:
	add r0, r5, #0
	sub r0, #0x24
	lsl r0, r0, #0x18
	lsr r5, r0, #0x18
	cmp r5, #0x82
	bls _08036F8A
	mov r5, #0x82
	mov r1, #0xff
	mov ip, r1
_08036F8A:
	ldr r3, _08036FD4
	add r0, r5, r3
	ldrb r6, [r0]
	ldr r4, _08036FD8
	mov r2, #0xf
	add r0, r6, #0
	and r0, r2
	lsl r0, r0, #1
	add r0, r0, r4
	mov r7, #0
	ldrsh r1, [r0, r7]
	asr r0, r6, #4
	add r6, r1, #0
	asr r6, r0
	add r0, r5, #1
	add r0, r0, r3
	ldrb r1, [r0]
	add r0, r1, #0
	and r0, r2
	lsl r0, r0, #1
	add r0, r0, r4
	mov r2, #0
	ldrsh r0, [r0, r2]
	asr r1, r1, #4
	asr r0, r1
	sub r0, r0, r6
	mov r7, ip
	mul r7, r0, r7
	add r0, r7, #0
	asr r0, r0, #8
	add r0, r6, r0
	mov r1, #0x80
	lsl r1, r1, #4
	add r0, r0, r1
_08036FCE:
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
_08036FD4: .4byte unk_83377FC
_08036FD8: .4byte unk_8337880

thumb_func_global CgbOscOff
CgbOscOff: @ 0x08036FDC
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	add r1, r0, #0
	cmp r0, #2
	beq _08037004
	cmp r0, #2
	bgt _08036FF0
	cmp r0, #1
	beq _08036FF6
	b _08037018
_08036FF0:
	cmp r1, #3
	beq _0803700C
	b _08037018
_08036FF6:
	ldr r1, _08037000
	mov r0, #8
	strb r0, [r1]
	add r1, #2
	b _08037020
	.align 2, 0
_08037000: .4byte 0x04000063
_08037004:
	ldr r1, _08037008
	b _0803701A
	.align 2, 0
_08037008: .4byte 0x04000069
_0803700C:
	ldr r1, _08037014
	mov r0, #0
	b _08037022
	.align 2, 0
_08037014: .4byte 0x04000070
_08037018:
	ldr r1, _08037028
_0803701A:
	mov r0, #8
	strb r0, [r1]
	add r1, #4
_08037020:
	mov r0, #0x80
_08037022:
	strb r0, [r1]
	bx lr
	.align 2, 0
_08037028: .4byte 0x04000079

thumb_func_global sub_0803702C
sub_0803702C: @ 0x0803702C
	push {r4, lr}
	add r1, r0, #0
	ldrb r0, [r1, #2]
	lsl r2, r0, #0x18
	lsr r4, r2, #0x18
	ldrb r3, [r1, #3]
	lsl r0, r3, #0x18
	lsr r3, r0, #0x18
	cmp r4, r3
	blo _0803704C
	lsr r0, r2, #0x19
	cmp r0, r3
	blo _08037058
	mov r0, #0xf
	strb r0, [r1, #0x1b]
	b _08037066
_0803704C:
	lsr r0, r0, #0x19
	cmp r0, r4
	blo _08037058
	mov r0, #0xf0
	strb r0, [r1, #0x1b]
	b _08037066
_08037058:
	mov r0, #0xff
	strb r0, [r1, #0x1b]
	ldrb r2, [r1, #3]
	ldrb r3, [r1, #2]
	add r0, r2, r3
	lsr r0, r0, #4
	b _08037076
_08037066:
	ldrb r2, [r1, #3]
	ldrb r3, [r1, #2]
	add r0, r2, r3
	lsr r0, r0, #4
	strb r0, [r1, #0xa]
	cmp r0, #0xf
	bls _08037078
	mov r0, #0xf
_08037076:
	strb r0, [r1, #0xa]
_08037078:
	ldrb r2, [r1, #6]
	ldrb r3, [r1, #0xa]
	add r0, r2, #0
	mul r0, r3, r0
	add r0, #0xf
	asr r0, r0, #4
	strb r0, [r1, #0x19]
	ldrb r0, [r1, #0x1c]
	ldrb r2, [r1, #0x1b]
	and r0, r2
	strb r0, [r1, #0x1b]
	pop {r4}
	pop {r0}
	bx r0

thumb_func_global CgbSound
CgbSound: @ 0x08037094
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x1c
	ldr r0, _080370B4
	ldr r0, [r0]
	str r0, [sp, #4]
	ldrb r0, [r0, #0xa]
	cmp r0, #0
	beq _080370B8
	sub r0, #1
	ldr r1, [sp, #4]
	strb r0, [r1, #0xa]
	b _080370BE
	.align 2, 0
_080370B4: .4byte 0x03007FF0
_080370B8:
	mov r0, #0xe
	ldr r2, [sp, #4]
	strb r0, [r2, #0xa]
_080370BE:
	mov r6, #1
	ldr r0, [sp, #4]
	ldr r4, [r0, #0x1c]
_080370C4:
	ldrb r1, [r4]
	mov r0, #0xc7
	and r0, r1
	add r2, r6, #1
	mov sl, r2
	mov r2, #0x40
	add r2, r2, r4
	mov sb, r2
	cmp r0, #0
	bne _080370DA
	b _080374C4
_080370DA:
	cmp r6, #2
	beq _0803710C
	cmp r6, #2
	bgt _080370E8
	cmp r6, #1
	beq _080370EE
	b _08037144
_080370E8:
	cmp r6, #3
	beq _08037124
	b _08037144
_080370EE:
	ldr r0, _08037100
	str r0, [sp, #8]
	ldr r7, _08037104
	ldr r2, _08037108
	str r2, [sp, #0xc]
	add r0, #4
	str r0, [sp, #0x10]
	add r2, #2
	b _08037154
	.align 2, 0
_08037100: .4byte 0x04000060
_08037104: .4byte 0x04000062
_08037108: .4byte 0x04000063
_0803710C:
	ldr r0, _08037118
	str r0, [sp, #8]
	ldr r7, _0803711C
	ldr r2, _08037120
	b _0803714C
	.align 2, 0
_08037118: .4byte 0x04000061
_0803711C: .4byte 0x04000068
_08037120: .4byte 0x04000069
_08037124:
	ldr r0, _08037138
	str r0, [sp, #8]
	ldr r7, _0803713C
	ldr r2, _08037140
	str r2, [sp, #0xc]
	add r0, #4
	str r0, [sp, #0x10]
	add r2, #2
	b _08037154
	.align 2, 0
_08037138: .4byte 0x04000070
_0803713C: .4byte 0x04000072
_08037140: .4byte 0x04000073
_08037144:
	ldr r0, _080371A4
	str r0, [sp, #8]
	ldr r7, _080371A8
	ldr r2, _080371AC
_0803714C:
	str r2, [sp, #0xc]
	add r0, #0xb
	str r0, [sp, #0x10]
	add r2, #4
_08037154:
	str r2, [sp, #0x14]
	ldr r0, [sp, #4]
	ldrb r0, [r0, #0xa]
	str r0, [sp]
	ldr r2, [sp, #0xc]
	ldrb r0, [r2]
	mov r8, r0
	add r2, r1, #0
	mov r0, #0x80
	and r0, r2
	cmp r0, #0
	beq _0803724A
	mov r3, #0x40
	add r0, r3, #0
	and r0, r2
	lsl r0, r0, #0x18
	lsr r5, r0, #0x18
	add r0, r6, #1
	mov sl, r0
	mov r1, #0x40
	add r1, r1, r4
	mov sb, r1
	cmp r5, #0
	bne _0803726E
	mov r0, #3
	strb r0, [r4]
	strb r0, [r4, #0x1d]
	add r0, r4, #0
	str r3, [sp, #0x18]
	bl sub_0803702C
	ldr r3, [sp, #0x18]
	cmp r6, #2
	beq _080371BC
	cmp r6, #2
	bgt _080371B0
	cmp r6, #1
	beq _080371B6
	b _08037210
	.align 2, 0
_080371A4: .4byte 0x04000071
_080371A8: .4byte 0x04000078
_080371AC: .4byte 0x04000079
_080371B0:
	cmp r6, #3
	beq _080371C8
	b _08037210
_080371B6:
	ldrb r0, [r4, #0x1f]
	ldr r2, [sp, #8]
	strb r0, [r2]
_080371BC:
	ldr r0, [r4, #0x24]
	lsl r0, r0, #6
	ldrb r1, [r4, #0x1e]
	add r0, r1, r0
	strb r0, [r7]
	b _0803721C
_080371C8:
	ldr r1, [r4, #0x24]
	ldr r0, [r4, #0x28]
	cmp r1, r0
	beq _080371F0
	ldr r2, [sp, #8]
	strb r3, [r2]
	ldr r1, _08037204
	ldr r2, [r4, #0x24]
	ldr r0, [r2]
	str r0, [r1]
	add r1, #4
	ldr r0, [r2, #4]
	str r0, [r1]
	add r1, #4
	ldr r0, [r2, #8]
	str r0, [r1]
	add r1, #4
	ldr r0, [r2, #0xc]
	str r0, [r1]
	str r2, [r4, #0x28]
_080371F0:
	ldr r0, [sp, #8]
	strb r5, [r0]
	ldrb r0, [r4, #0x1e]
	strb r0, [r7]
	ldrb r0, [r4, #0x1e]
	cmp r0, #0
	beq _08037208
	mov r0, #0xc0
	b _0803722A
	.align 2, 0
_08037204: .4byte 0x04000090
_08037208:
	mov r1, #0x80
	neg r1, r1
	strb r1, [r4, #0x1a]
	b _0803722C
_08037210:
	ldrb r0, [r4, #0x1e]
	strb r0, [r7]
	ldr r0, [r4, #0x24]
	lsl r0, r0, #3
	ldr r2, [sp, #0x10]
	strb r0, [r2]
_0803721C:
	ldrb r0, [r4, #4]
	add r0, #8
	mov r8, r0
	ldrb r0, [r4, #0x1e]
	cmp r0, #0
	beq _0803722A
	mov r0, #0x40
_0803722A:
	strb r0, [r4, #0x1a]
_0803722C:
	ldrb r1, [r4, #4]
	mov r2, #0
	strb r1, [r4, #0xb]
	mov r0, #0xff
	and r0, r1
	add r1, r6, #1
	mov sl, r1
	mov r1, #0x40
	add r1, r1, r4
	mov sb, r1
	cmp r0, #0
	bne _08037246
	b _08037382
_08037246:
	strb r2, [r4, #9]
	b _080373B0
_0803724A:
	mov r0, #4
	and r0, r2
	cmp r0, #0
	beq _0803727C
	ldrb r0, [r4, #0xd]
	sub r0, #1
	strb r0, [r4, #0xd]
	mov r2, #0xff
	and r0, r2
	lsl r0, r0, #0x18
	add r1, r6, #1
	mov sl, r1
	mov r2, #0x40
	add r2, r2, r4
	mov sb, r2
	cmp r0, #0
	ble _0803726E
	b _080373C2
_0803726E:
	lsl r0, r6, #0x18
	lsr r0, r0, #0x18
	bl CgbOscOff
	mov r0, #0
	strb r0, [r4]
	b _080374C0
_0803727C:
	mov r0, #0x40
	and r0, r1
	add r2, r6, #1
	mov sl, r2
	mov r2, #0x40
	add r2, r2, r4
	mov sb, r2
	cmp r0, #0
	beq _080372BC
	mov r0, #3
	and r0, r1
	cmp r0, #0
	beq _080372BC
	mov r0, #0xfc
	and r0, r1
	mov r2, #0
	strb r0, [r4]
	ldrb r1, [r4, #7]
	strb r1, [r4, #0xb]
	mov r0, #0xff
	and r0, r1
	cmp r0, #0
	beq _080372EE
	mov r0, #1
	ldrb r1, [r4, #0x1d]
	orr r0, r1
	strb r0, [r4, #0x1d]
	cmp r6, #3
	beq _080373B0
	ldrb r2, [r4, #7]
	mov r8, r2
	b _080373B0
_080372BC:
	ldrb r0, [r4, #0xb]
	cmp r0, #0
	bne _080373B0
	cmp r6, #3
	bne _080372CE
	mov r0, #1
	ldrb r1, [r4, #0x1d]
	orr r0, r1
	strb r0, [r4, #0x1d]
_080372CE:
	add r0, r4, #0
	bl sub_0803702C
	mov r0, #3
	ldrb r2, [r4]
	and r0, r2
	cmp r0, #0
	bne _08037322
	ldrb r0, [r4, #9]
	sub r0, #1
	strb r0, [r4, #9]
	mov r1, #0xff
	and r0, r1
	lsl r0, r0, #0x18
	cmp r0, #0
	bgt _0803731E
_080372EE:
	ldrb r2, [r4, #0xc]
	ldrb r1, [r4, #0xa]
	add r0, r2, #0
	mul r0, r1, r0
	add r0, #0xff
	asr r0, r0, #8
	mov r1, #0
	strb r0, [r4, #9]
	lsl r0, r0, #0x18
	cmp r0, #0
	beq _0803726E
	mov r0, #4
	ldrb r2, [r4]
	orr r0, r2
	strb r0, [r4]
	mov r0, #1
	ldrb r1, [r4, #0x1d]
	orr r0, r1
	strb r0, [r4, #0x1d]
	cmp r6, #3
	beq _080373C2
	mov r2, #8
	mov r8, r2
	b _080373C2
_0803731E:
	ldrb r0, [r4, #7]
	b _080373AE
_08037322:
	cmp r0, #1
	bne _0803732E
_08037326:
	ldrb r0, [r4, #0x19]
	strb r0, [r4, #9]
	mov r0, #7
	b _080373AE
_0803732E:
	cmp r0, #2
	bne _08037372
	ldrb r0, [r4, #9]
	sub r0, #1
	strb r0, [r4, #9]
	mov r1, #0xff
	and r0, r1
	lsl r0, r0, #0x18
	ldrb r2, [r4, #0x19]
	lsl r1, r2, #0x18
	cmp r0, r1
	bgt _0803736E
_08037346:
	ldrb r0, [r4, #6]
	cmp r0, #0
	bne _08037356
	mov r0, #0xfc
	ldrb r1, [r4]
	and r0, r1
	strb r0, [r4]
	b _080372EE
_08037356:
	ldrb r0, [r4]
	sub r0, #1
	strb r0, [r4]
	mov r0, #1
	ldrb r2, [r4, #0x1d]
	orr r0, r2
	strb r0, [r4, #0x1d]
	cmp r6, #3
	beq _08037326
	mov r0, #8
	mov r8, r0
	b _08037326
_0803736E:
	ldrb r0, [r4, #5]
	b _080373AE
_08037372:
	ldrb r0, [r4, #9]
	add r0, #1
	strb r0, [r4, #9]
	mov r1, #0xff
	and r0, r1
	ldrb r2, [r4, #0xa]
	cmp r0, r2
	blo _080373AC
_08037382:
	ldrb r0, [r4]
	sub r0, #1
	mov r2, #0
	strb r0, [r4]
	ldrb r1, [r4, #5]
	strb r1, [r4, #0xb]
	mov r0, #0xff
	and r0, r1
	cmp r0, #0
	beq _08037346
	mov r0, #1
	ldrb r1, [r4, #0x1d]
	orr r0, r1
	strb r0, [r4, #0x1d]
	ldrb r0, [r4, #0xa]
	strb r0, [r4, #9]
	cmp r6, #3
	beq _080373B0
	ldrb r2, [r4, #5]
	mov r8, r2
	b _080373B0
_080373AC:
	ldrb r0, [r4, #4]
_080373AE:
	strb r0, [r4, #0xb]
_080373B0:
	ldrb r0, [r4, #0xb]
	sub r0, #1
	strb r0, [r4, #0xb]
	ldr r0, [sp]
	cmp r0, #0
	bne _080373C2
	sub r0, #1
	str r0, [sp]
	b _080372BC
_080373C2:
	mov r0, #2
	ldrb r1, [r4, #0x1d]
	and r0, r1
	cmp r0, #0
	beq _0803743A
	cmp r6, #3
	bgt _08037402
	mov r0, #8
	ldrb r2, [r4, #1]
	and r0, r2
	cmp r0, #0
	beq _08037402
	ldr r0, _080373EC
	ldrb r0, [r0]
	cmp r0, #0x3f
	bgt _080373F4
	ldr r0, [r4, #0x20]
	add r0, #2
	ldr r1, _080373F0
	b _080373FE
	.align 2, 0
_080373EC: .4byte 0x04000089
_080373F0: .4byte 0x000007FC
_080373F4:
	cmp r0, #0x7f
	bgt _08037402
	ldr r0, [r4, #0x20]
	add r0, #1
	ldr r1, _08037410
_080373FE:
	and r0, r1
	str r0, [r4, #0x20]
_08037402:
	cmp r6, #4
	beq _08037414
	ldr r0, [r4, #0x20]
	ldr r1, [sp, #0x10]
	strb r0, [r1]
	b _08037422
	.align 2, 0
_08037410: .4byte 0x000007FE
_08037414:
	ldr r2, [sp, #0x10]
	ldrb r0, [r2]
	mov r1, #8
	and r1, r0
	ldr r0, [r4, #0x20]
	orr r0, r1
	strb r0, [r2]
_08037422:
	mov r0, #0xc0
	ldrb r1, [r4, #0x1a]
	and r0, r1
	add r1, r4, #0
	add r1, #0x21
	ldrb r1, [r1]
	add r0, r1, r0
	strb r0, [r4, #0x1a]
	mov r2, #0xff
	and r0, r2
	ldr r1, [sp, #0x14]
	strb r0, [r1]
_0803743A:
	mov r0, #1
	ldrb r2, [r4, #0x1d]
	and r0, r2
	cmp r0, #0
	beq _080374C0
	ldr r1, _08037484
	ldrb r0, [r1]
	ldrb r2, [r4, #0x1c]
	bic r0, r2
	ldrb r2, [r4, #0x1b]
	orr r0, r2
	strb r0, [r1]
	cmp r6, #3
	bne _0803748C
	ldr r0, _08037488
	ldrb r1, [r4, #9]
	add r0, r1, r0
	ldrb r0, [r0]
	ldr r2, [sp, #0xc]
	strb r0, [r2]
	mov r1, #0x80
	add r0, r1, #0
	ldrb r2, [r4, #0x1a]
	and r0, r2
	cmp r0, #0
	beq _080374C0
	ldr r0, [sp, #8]
	strb r1, [r0]
	ldrb r0, [r4, #0x1a]
	ldr r1, [sp, #0x14]
	strb r0, [r1]
	mov r0, #0x7f
	ldrb r2, [r4, #0x1a]
	and r0, r2
	strb r0, [r4, #0x1a]
	b _080374C0
	.align 2, 0
_08037484: .4byte 0x04000081
_08037488: .4byte unk_83378D4
_0803748C:
	mov r0, #0xf
	mov r1, r8
	and r1, r0
	mov r8, r1
	ldrb r2, [r4, #9]
	lsl r0, r2, #4
	add r0, r8
	ldr r1, [sp, #0xc]
	strb r0, [r1]
	mov r2, #0x80
	ldrb r0, [r4, #0x1a]
	orr r0, r2
	ldr r1, [sp, #0x14]
	strb r0, [r1]
	cmp r6, #1
	bne _080374C0
	ldr r0, [sp, #8]
	ldrb r1, [r0]
	mov r0, #8
	and r0, r1
	cmp r0, #0
	bne _080374C0
	ldrb r0, [r4, #0x1a]
	orr r0, r2
	ldr r1, [sp, #0x14]
	strb r0, [r1]
_080374C0:
	mov r0, #0
	strb r0, [r4, #0x1d]
_080374C4:
	mov r6, sl
	mov r4, sb
	cmp r6, #4
	bgt _080374CE
	b _080370C4
_080374CE:
	add sp, #0x1c
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
	
	
	thumb_func_global sub_80374E0
sub_80374E0: @ 0x080374E0
	push {r4, lr}
	add r2, r0, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r3, [r2, #0x34]
	ldr r0, _08037504
	cmp r3, r0
	bne _080374FC
	strh r1, [r2, #0x1e]
	ldrh r4, [r2, #0x1c]
	add r0, r1, #0
	mul r0, r4, r0
	asr r0, r0, #8
	strh r0, [r2, #0x20]
_080374FC:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_08037504: .4byte 0x68736D53
	
	
thumb_func_global sub_08037508
sub_08037508: @ 0x08037508
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	add r4, r0, #0
	lsl r1, r1, #0x10
	lsr r7, r1, #0x10
	lsl r6, r2, #0x10
	ldr r3, [r4, #0x34]
	ldr r0, _0803756C
	cmp r3, r0
	bne _08037560
	add r0, r3, #1
	str r0, [r4, #0x34]
	ldrb r2, [r4, #8]
	ldr r1, [r4, #0x2c]
	mov r5, #1
	cmp r2, #0
	ble _0803755C
	mov r0, #0x80
	mov r8, r0
	lsr r6, r6, #0x12
	mov r0, #3
	mov ip, r0
_08037538:
	add r0, r7, #0
	and r0, r5
	cmp r0, #0
	beq _08037552
	ldrb r3, [r1]
	mov r0, r8
	and r0, r3
	cmp r0, #0
	beq _08037552
	strb r6, [r1, #0x13]
	mov r0, ip
	orr r0, r3
	strb r0, [r1]
_08037552:
	sub r2, #1
	add r1, #0x50
	lsl r5, r5, #1
	cmp r2, #0
	bgt _08037538
_0803755C:
	ldr r0, _0803756C
	str r0, [r4, #0x34]
_08037560:
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_0803756C: .4byte 0x68736D53
	
	
	
	
	thumb_func_global sub_8037570
sub_8037570: @ 0x08037570
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	add r4, r0, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov ip, r1
	lsl r2, r2, #0x10
	lsr r6, r2, #0x10
	ldr r3, [r4, #0x34]
	ldr r0, _080375E0
	cmp r3, r0
	bne _080375D2
	add r0, r3, #1
	str r0, [r4, #0x34]
	ldrb r2, [r4, #8]
	ldr r3, [r4, #0x2c]
	mov r5, #1
	cmp r2, #0
	ble _080375CE
	mov r0, #0x80
	mov sb, r0
	lsl r0, r6, #0x10
	asr r7, r0, #0x18
	mov r0, #0xc
	mov r8, r0
_080375A8:
	mov r0, ip
	and r0, r5
	cmp r0, #0
	beq _080375C4
	ldrb r1, [r3]
	mov r0, sb
	and r0, r1
	cmp r0, #0
	beq _080375C4
	strb r7, [r3, #0xb]
	strb r6, [r3, #0xd]
	mov r0, r8
	orr r0, r1
	strb r0, [r3]
_080375C4:
	sub r2, #1
	add r3, #0x50
	lsl r5, r5, #1
	cmp r2, #0
	bgt _080375A8
_080375CE:
	ldr r0, _080375E0
	str r0, [r4, #0x34]
_080375D2:
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_080375E0: .4byte 0x68736D53
	thumb_func_global sub_80375E4
sub_80375E4: @ 0x080375E4
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	add r4, r0, #0
	lsl r1, r1, #0x10
	lsr r7, r1, #0x10
	lsl r2, r2, #0x18
	lsr r6, r2, #0x18
	ldr r3, [r4, #0x34]
	ldr r0, _08037648
	cmp r3, r0
	bne _0803763C
	add r0, r3, #1
	str r0, [r4, #0x34]
	ldrb r2, [r4, #8]
	ldr r1, [r4, #0x2c]
	mov r5, #1
	cmp r2, #0
	ble _08037638
	mov r0, #0x80
	mov r8, r0
	mov r0, #3
	mov ip, r0
_08037614:
	add r0, r7, #0
	and r0, r5
	cmp r0, #0
	beq _0803762E
	ldrb r3, [r1]
	mov r0, r8
	and r0, r3
	cmp r0, #0
	beq _0803762E
	strb r6, [r1, #0x15]
	mov r0, ip
	orr r0, r3
	strb r0, [r1]
_0803762E:
	sub r2, #1
	add r1, #0x50
	lsl r5, r5, #1
	cmp r2, #0
	bgt _08037614
_08037638:
	ldr r0, _08037648
	str r0, [r4, #0x34]
_0803763C:
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_08037648: .4byte 0x68736D53
	thumb_func_global sub_803764C
sub_803764C: @ 0x0803764C
	add r1, r0, #0
	mov r2, #0
	mov r0, #0
	strb r0, [r1, #0x1a]
	strb r0, [r1, #0x16]
	ldrb r0, [r1, #0x18]
	cmp r0, #0
	bne _08037660
	mov r0, #0xc
	b _08037662
_08037660:
	mov r0, #3
_08037662:
	ldrb r2, [r1]
	orr r0, r2
	strb r0, [r1]
	bx lr
	.align 2, 0
	thumb_func_global sub_803766C
sub_803766C: @ 0x0803766C
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	add r6, r0, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov sl, r1
	lsl r2, r2, #0x18
	lsr r2, r2, #0x18
	mov r8, r2
	ldr r1, [r6, #0x34]
	ldr r0, _080376DC
	cmp r1, r0
	bne _080376CC
	add r0, r1, #1
	str r0, [r6, #0x34]
	ldrb r5, [r6, #8]
	ldr r4, [r6, #0x2c]
	mov r7, #1
	cmp r5, #0
	ble _080376C8
	mov sb, r8
_0803769C:
	mov r0, sl
	and r0, r7
	cmp r0, #0
	beq _080376BE
	mov r0, #0x80
	ldrb r1, [r4]
	and r0, r1
	cmp r0, #0
	beq _080376BE
	mov r0, r8
	strb r0, [r4, #0x17]
	mov r1, sb
	cmp r1, #0
	bne _080376BE
	add r0, r4, #0
	bl sub_803764C
_080376BE:
	sub r5, #1
	add r4, #0x50
	lsl r7, r7, #1
	cmp r5, #0
	bgt _0803769C
_080376C8:
	ldr r0, _080376DC
	str r0, [r6, #0x34]
_080376CC:
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_080376DC: .4byte 0x68736D53
	thumb_func_global sub_80376E0
sub_80376E0: @ 0x080376E0
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	add r6, r0, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov sl, r1
	lsl r2, r2, #0x18
	lsr r2, r2, #0x18
	mov r8, r2
	ldr r1, [r6, #0x34]
	ldr r0, _08037750
	cmp r1, r0
	bne _08037740
	add r0, r1, #1
	str r0, [r6, #0x34]
	ldrb r5, [r6, #8]
	ldr r4, [r6, #0x2c]
	mov r7, #1
	cmp r5, #0
	ble _0803773C
	mov sb, r8
_08037710:
	mov r0, sl
	and r0, r7
	cmp r0, #0
	beq _08037732
	mov r0, #0x80
	ldrb r1, [r4]
	and r0, r1
	cmp r0, #0
	beq _08037732
	mov r0, r8
	strb r0, [r4, #0x19]
	mov r1, sb
	cmp r1, #0
	bne _08037732
	add r0, r4, #0
	bl sub_803764C
_08037732:
	sub r5, #1
	add r4, #0x50
	lsl r7, r7, #1
	cmp r5, #0
	bgt _08037710
_0803773C:
	ldr r0, _08037750
	str r0, [r6, #0x34]
_08037740:
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_08037750: .4byte 0x68736D53
	
	
	
	
thumb_func_global ply_memacc
ply_memacc: @ 0x08037754
	push {r4, r5, r6, lr}
	add r4, r0, #0
	add r6, r1, #0
	ldr r1, [r6, #0x40]
	ldrb r5, [r1]
	add r2, r1, #1
	str r2, [r6, #0x40]
	ldr r0, [r4, #0x18]
	ldrb r1, [r1, #1]
	add r3, r1, r0
	add r0, r2, #1
	str r0, [r6, #0x40]
	ldrb r2, [r2, #1]
	add r0, #1
	str r0, [r6, #0x40]
	cmp r5, #0x11
	bls _08037778
	b _080378A6
_08037778:
	lsl r0, r5, #2
	ldr r1, _08037784
	add r0, r0, r1
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
_08037784: .4byte _08037788
_08037788: @ jump table
	.4byte _080377D0 @ case 0
	.4byte _080377D4 @ case 1
	.4byte _080377DC @ case 2
	.4byte _080377E4 @ case 3
	.4byte _080377EE @ case 4
	.4byte _080377FC @ case 5
	.4byte _0803780A @ case 6
	.4byte _08037812 @ case 7
	.4byte _0803781A @ case 8
	.4byte _08037822 @ case 9
	.4byte _0803782A @ case 10
	.4byte _08037832 @ case 11
	.4byte _0803783A @ case 12
	.4byte _08037848 @ case 13
	.4byte _08037856 @ case 14
	.4byte _08037864 @ case 15
	.4byte _08037872 @ case 16
	.4byte _08037880 @ case 17
_080377D0:
	strb r2, [r3]
	b _080378A6
_080377D4:
	ldrb r1, [r3]
	add r0, r1, r2
	strb r0, [r3]
	b _080378A6
_080377DC:
	ldrb r1, [r3]
	sub r0, r1, r2
	strb r0, [r3]
	b _080378A6
_080377E4:
	ldr r0, [r4, #0x18]
	add r0, r0, r2
	ldrb r0, [r0]
	strb r0, [r3]
	b _080378A6
_080377EE:
	ldr r0, [r4, #0x18]
	add r0, r0, r2
	ldrb r1, [r3]
	ldrb r0, [r0]
	add r0, r1, r0
	strb r0, [r3]
	b _080378A6
_080377FC:
	ldr r0, [r4, #0x18]
	add r0, r0, r2
	ldrb r1, [r3]
	ldrb r0, [r0]
	sub r0, r1, r0
	strb r0, [r3]
	b _080378A6
_0803780A:
	ldrb r3, [r3]
	cmp r3, r2
	beq _0803788C
	b _080378A0
_08037812:
	ldrb r3, [r3]
	cmp r3, r2
	bne _0803788C
	b _080378A0
_0803781A:
	ldrb r3, [r3]
	cmp r3, r2
	bhi _0803788C
	b _080378A0
_08037822:
	ldrb r3, [r3]
	cmp r3, r2
	bhs _0803788C
	b _080378A0
_0803782A:
	ldrb r3, [r3]
	cmp r3, r2
	bls _0803788C
	b _080378A0
_08037832:
	ldrb r3, [r3]
	cmp r3, r2
	blo _0803788C
	b _080378A0
_0803783A:
	ldr r0, [r4, #0x18]
	add r0, r0, r2
	ldrb r3, [r3]
	ldrb r0, [r0]
	cmp r3, r0
	beq _0803788C
	b _080378A0
_08037848:
	ldr r0, [r4, #0x18]
	add r0, r0, r2
	ldrb r3, [r3]
	ldrb r0, [r0]
	cmp r3, r0
	bne _0803788C
	b _080378A0
_08037856:
	ldr r0, [r4, #0x18]
	add r0, r0, r2
	ldrb r3, [r3]
	ldrb r0, [r0]
	cmp r3, r0
	bhi _0803788C
	b _080378A0
_08037864:
	ldr r0, [r4, #0x18]
	add r0, r0, r2
	ldrb r3, [r3]
	ldrb r0, [r0]
	cmp r3, r0
	bhs _0803788C
	b _080378A0
_08037872:
	ldr r0, [r4, #0x18]
	add r0, r0, r2
	ldrb r3, [r3]
	ldrb r0, [r0]
	cmp r3, r0
	bls _0803788C
	b _080378A0
_08037880:
	ldr r0, [r4, #0x18]
	add r0, r0, r2
	ldrb r3, [r3]
	ldrb r0, [r0]
	cmp r3, r0
	bhs _080378A0
_0803788C:
	ldr r0, _0803789C
	ldr r2, [r0]
	add r0, r4, #0
	add r1, r6, #0
	bl call_r2
	b _080378A6
	.align 2, 0
_0803789C: .4byte 0x02001644
_080378A0:
	ldr r0, [r6, #0x40]
	add r0, #4
	str r0, [r6, #0x40]
_080378A6:
	pop {r4, r5, r6}
	pop {r0}
	bx r0

thumb_func_global ply_xcmd
ply_xcmd: @ 0x080378AC
	push {lr}
	ldr r2, [r1, #0x40]
	ldrb r3, [r2]
	add r2, #1
	str r2, [r1, #0x40]
	ldr r2, _080378C8
	lsl r3, r3, #2
	add r3, r3, r2
	ldr r2, [r3]
	bl call_r2
	pop {r0}
	bx r0
	.align 2, 0
_080378C8: .4byte off_8337918



	thumb_func_global sub_80378CC
sub_80378CC: @ 0x080378CC
	push {lr}
	ldr r2, _080378DC
	ldr r2, [r2]
	bl call_r2
	pop {r0}
	bx r0
	.align 2, 0
_080378DC: .4byte 0x02001640



	thumb_func_global sub_80378E0
sub_80378E0: @ 0x080378E0
	push {r4, lr}
	ldr r2, [r1, #0x40]
	ldr r0, _08037918
	and r4, r0
	ldrb r0, [r2]
	orr r4, r0
	ldrb r0, [r2, #1]
	lsl r3, r0, #8
	ldr r0, _0803791C
	and r4, r0
	orr r4, r3
	ldrb r0, [r2, #2]
	lsl r3, r0, #0x10
	ldr r0, _08037920
	and r4, r0
	orr r4, r3
	ldrb r0, [r2, #3]
	lsl r3, r0, #0x18
	ldr r0, _08037924
	and r4, r0
	orr r4, r3
	str r4, [r1, #0x28]
	add r2, #4
	str r2, [r1, #0x40]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_08037918: .4byte 0xFFFFFF00
_0803791C: .4byte 0xFFFF00FF
_08037920: .4byte 0xFF00FFFF
_08037924: .4byte 0x00FFFFFF



	thumb_func_global sub_8037928
sub_8037928: @ 0x08037928
	ldr r0, [r1, #0x40]
	ldrb r2, [r0]
	add r0, r1, #0
	add r0, #0x24
	strb r2, [r0]
	ldr r0, [r1, #0x40]
	add r0, #1
	str r0, [r1, #0x40]
	bx lr
	.align 2, 0
	
	
	
	thumb_func_global sub_803793C
sub_803793C: @ 0x0803793C
	ldr r0, [r1, #0x40]
	ldrb r2, [r0]
	add r0, r1, #0
	add r0, #0x2c
	strb r2, [r0]
	ldr r0, [r1, #0x40]
	add r0, #1
	str r0, [r1, #0x40]
	bx lr
	.align 2, 0
	
	
	
	thumb_func_global sub_8037950
sub_8037950: @ 0x08037950
	ldr r0, [r1, #0x40]
	ldrb r0, [r0]
	add r2, r1, #0
	add r2, #0x2d
	strb r0, [r2]
	ldr r0, [r1, #0x40]
	add r0, #1
	str r0, [r1, #0x40]
	bx lr
	.align 2, 0
	
	
	
	thumb_func_global sub_8037964
sub_8037964: @ 0x08037964
	ldr r0, [r1, #0x40]
	ldrb r0, [r0]
	add r2, r1, #0
	add r2, #0x2e
	strb r0, [r2]
	ldr r0, [r1, #0x40]
	add r0, #1
	str r0, [r1, #0x40]
	bx lr
	.align 2, 0
	
	
	
	thumb_func_global sub_8037978
sub_8037978: @ 0x08037978
	ldr r0, [r1, #0x40]
	ldrb r0, [r0]
	add r2, r1, #0
	add r2, #0x2f
	strb r0, [r2]
	ldr r0, [r1, #0x40]
	add r0, #1
	str r0, [r1, #0x40]
	bx lr
	.align 2, 0
	
	
	
	thumb_func_global sub_803798C
sub_803798C: @ 0x0803798C
	ldr r0, [r1, #0x40]
	ldrb r2, [r0]
	strb r2, [r1, #0x1e]
	add r0, #1
	str r0, [r1, #0x40]
	bx lr
	
	
	
	thumb_func_global sub_8037998
sub_8037998: @ 0x08037998
	ldr r0, [r1, #0x40]
	ldrb r2, [r0]
	strb r2, [r1, #0x1f]
	add r0, #1
	str r0, [r1, #0x40]
	bx lr
	
	
	
	thumb_func_global sub_80379A4
sub_80379A4: @ 0x080379A4
	ldr r0, [r1, #0x40]
	ldrb r0, [r0]
	add r2, r1, #0
	add r2, #0x26
	strb r0, [r2]
	ldr r0, [r1, #0x40]
	add r0, #1
	str r0, [r1, #0x40]
	bx lr
	.align 2, 0
	
	
	thumb_func_global sub_80379B8
sub_80379B8: @ 0x080379B8
	ldr r0, [r1, #0x40]
	ldrb r0, [r0]
	add r2, r1, #0
	add r2, #0x27
	strb r0, [r2]
	ldr r0, [r1, #0x40]
	add r0, #1
	str r0, [r1, #0x40]
	bx lr
	.align 2, 0
