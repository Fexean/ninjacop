.include "asm/macros.inc"


thumb_func_global main
main: @ 0x0800D5CC
	push {r4, r5, lr}
	sub sp, #4
	ldr r1, =main+1
	ldr r0, =0x07FFFFFF
	cmp r1, r0
	bls _0800D5E8
	bl clearGfx_and_Wram
	b _0800D602
	.align 2, 0
.pool
_0800D5E8:
	mov r0, #0
	str r0, [sp]
	ldr r1, =0x040000D4
	mov r0, sp
	str r0, [r1]
	mov r0, #0xc0
	lsl r0, r0, #0x12
	str r0, [r1, #4]
	ldr r0, =0x85001E80
	str r0, [r1, #8]
	ldr r0, [r1, #8]
	bl clearGraphics
_0800D602:
	bl loadSpriteHandler
	bl resetAffineTransformations
	bl clearSprites
	bl InitCallbacks
	bl clearPalBufs
	bl stopPalFade
	bl clearObjects_maybe
	bl copyInterruptHandler
	bl initSound
	mov r1, #0xa0
	lsl r1, r1, #0x13
	ldr r2, =0x0000FFFF
	add r0, r2, #0
	strh r0, [r1]
	ldr r1, =0x04000200
	mov r0, #0x81
	strh r0, [r1]
	ldr r2, =0x04000004
	ldr r1, =0x03003D08
	mov r0, #8
	strh r0, [r1]
	strh r0, [r2]
	ldr r1, =0x04000208
	mov r0, #1
	strh r0, [r1]
	sub r1, #4
	ldr r3, =0x000045B4
	add r0, r3, #0
	strh r0, [r1]
	sub r2, #4
	ldr r1, =0x03003CD4
	mov r3, #0x82
	lsl r3, r3, #5
	add r0, r3, #0
	strh r0, [r1]
	strh r0, [r2]
	bl initSaveData
	bl clearIntroData
	bl initTasks
	ldr r0, =(task_first+1)
	mov r1, #0
	bl createTask
	ldr r5, =0x0300208C
	add r4, r5, #0
	sub r4, #0xc
gameLoop:
	add r0, r5, #0
	bl updateKeys
	bl runTasks
	bl handleBgMode
	bl drawSprites
	ldr r0, [r4]
	add r0, #1
	str r0, [r4]
	ldrb r0, [r4, #8]
	mov r1, #1
	orr r0, r1
	strb r0, [r4, #8]
	bl Bios_waitVBlank
	b gameLoop
	.align 2, 0
.pool



