.include "asm/macros.inc"

@completely disassembled

thumb_func_global stopPalFade
stopPalFade: @ 0x08010FBC
	push {lr}
	sub sp, #4
	mov r0, #0
	str r0, [sp]
	ldr r1, =0x03003E30
	ldr r2, =0x05000230
	mov r0, sp
	bl Bios_memcpy
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global startPalAnimation
startPalAnimation: @ 0x08010FDC
	push {lr}
	lsl r2, r2, #0x18
	lsr r2, r2, #0x18
	mov r3, #2
	bl palFadeMaybe
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	pop {r1}
	bx r1

thumb_func_global palFadeMaybe
palFadeMaybe: @ 0x08010FF0
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	sub sp, #4
	add r6, r0, #0
	mov r8, r1
	add r5, r2, #0
	add r4, r3, #0
	lsl r5, r5, #0x18
	lsr r5, r5, #0x18
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	bl sub_080110B4
	mov sb, r0
	mov r0, #0x8c
	mov r1, sb
	mul r1, r0, r1
	ldr r0, =0x03003E30
	add r7, r1, r0
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r2, =0x01000046
	mov r0, sp
	add r1, r7, #0
	bl Bios_memcpy
	mov r0, #1
	strb r0, [r7, #9]
	strb r5, [r7, #8]
	mov r0, r8
	str r0, [r7, #4]
	ldr r0, =(task_fadePal+1)
	lsl r4, r4, #0x18
	lsr r4, r4, #0x18
	add r1, r4, #0
	bl createTask
	str r7, [r0, #0x14]
	str r0, [r7]
	add r1, r7, #0
	add r1, #0xc
	ldr r0, [r6]
	cmp r0, #0
	beq _0801108A
	ldr r4, =0x0000FFFF
	ldr r3, =0x0000FFFE
	mov r2, #0
_08011054:
	ldr r0, [r6]
	ldrh r0, [r0]
	cmp r0, #0
	beq _08011082
	cmp r0, r4
	beq _08011082
	cmp r0, r3
	beq _08011082
	mov r0, #1
	strb r0, [r1]
	strb r2, [r1, #1]
	str r6, [r1, #4]
	ldr r0, [r6]
	ldrh r0, [r0]
	strh r0, [r1, #2]
	add r1, #8
	ldrb r0, [r7, #0xb]
	add r0, #1
	strb r0, [r7, #0xb]
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0xf
	bhi _0801108A
_08011082:
	add r6, #8
	ldr r0, [r6]
	cmp r0, #0
	bne _08011054
_0801108A:
	mov r1, sb
	lsl r0, r1, #0x10
	lsr r0, r0, #0x10
	add sp, #4
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_080110B4
sub_080110B4: @ 0x080110B4
	mov r1, #0
	ldr r2, =0x03003E30
_080110B8:
	ldrb r0, [r2, #9]
	cmp r0, #0
	bne _080110C8
	add r0, r1, #0
	b _080110D4
	.align 2, 0
.pool
_080110C8:
	add r2, #0x8c
	add r1, #1
	cmp r1, #0xf
	ble _080110B8
	mov r0, #1
	neg r0, r0
_080110D4:
	bx lr
	.byte 0x00, 0x00

thumb_func_global task_fadePal
task_fadePal: @ 0x080110D8
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	mov r0, #0
	mov r8, r0
	ldr r0, =0x0201AF00
	mov r1, #0xfa
	lsl r1, r1, #4
	add r0, r0, r1
	ldr r0, [r0]
	ldr r5, [r0, #0x14]
	ldrb r0, [r5, #0xa]
	cmp r0, #0
	beq _08011104
	add r0, r5, #0
	bl freePalFadeTask
	b _080111A4
	.align 2, 0
.pool
_08011104:
	mov r0, #0
	mov ip, r0
	ldrb r1, [r5, #0xb]
	cmp r8, r1
	bge _08011198
	ldr r0, =0x0000FFFF
	mov sl, r0
	ldr r1, =0x0000FFFE
	mov sb, r1
	ldr r7, =0x040000D4
	add r3, r5, #0
	add r3, #0xc
_0801111C:
	ldrb r0, [r3]
	cmp r0, #0
	beq _0801118C
	mov r0, #1
	add r8, r0
	ldrh r0, [r3, #2]
	sub r0, #1
	mov r6, #0
	strh r0, [r3, #2]
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _0801118C
	ldr r0, [r3, #4]
	ldrb r4, [r3, #1]
	lsl r1, r4, #2
	add r1, #4
	ldr r2, [r0]
	add r1, r2, r1
	ldrh r1, [r1]
	add r2, r0, #0
	cmp r1, sl
	bne _08011158
	strb r6, [r3]
	b _0801118C
	.align 2, 0
.pool
_08011158:
	cmp r1, #0
	beq _08011160
	cmp r1, sb
	bne _08011164
_08011160:
	strb r6, [r3, #1]
	b _08011168
_08011164:
	add r0, r4, #1
	strb r0, [r3, #1]
_08011168:
	ldrb r0, [r3, #1]
	lsl r0, r0, #2
	ldr r1, [r2]
	add r1, r1, r0
	ldrh r0, [r1]
	strh r0, [r3, #2]
	add r0, r1, #2
	str r0, [r7]
	ldrb r1, [r5, #8]
	ldr r0, [r2, #4]
	add r1, r1, r0
	lsl r1, r1, #1
	ldr r0, [r5, #4]
	add r0, r0, r1
	str r0, [r7, #4]
	ldr r0, =0x80000001
	str r0, [r7, #8]
	ldr r0, [r7, #8]
_0801118C:
	add r3, #8
	mov r1, #1
	add ip, r1
	ldrb r0, [r5, #0xb]
	cmp ip, r0
	blt _0801111C
_08011198:
	mov r1, r8
	cmp r1, #0
	bne _080111A4
	add r0, r5, #0
	bl freePalFadeTask
_080111A4:
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global freePalFadeTaskAtIndex
freePalFadeTaskAtIndex: @ 0x080111B8
	push {lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x8c
	mul r0, r1, r0
	ldr r1, =0x03003E30
	add r0, r0, r1
	bl freePalFadeTask
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global freePalFadeTask
freePalFadeTask: @ 0x080111D4
	push {lr}
	add r1, r0, #0
	ldrb r0, [r1, #9]
	cmp r0, #0
	beq _080111E8
	mov r0, #0
	strb r0, [r1, #9]
	ldr r0, [r1]
	bl endTask
_080111E8:
	pop {r0}
	bx r0

thumb_func_global FreeAllPalFadeTasks
FreeAllPalFadeTasks: @ 0x080111EC
	push {r4, r5, lr}
	mov r4, #0
	ldr r5, =0x03003E30
_080111F2:
	mov r0, #0x8c
	mul r0, r4, r0
	add r1, r0, r5
	ldrb r0, [r1, #9]
	cmp r0, #0
	beq _08011204
	add r0, r1, #0
	bl freePalFadeTask
_08011204:
	add r4, #1
	cmp r4, #0xf
	ble _080111F2
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global clearPalBufs
clearPalBufs: @ 0x08011214
	push {r4, r5, lr}
	sub sp, #4
	mov r0, sp
	mov r4, #0
	strh r4, [r0]
	ldr r1, =0x03000B30
	ldr r5, =0x01000040
	add r2, r5, #0
	bl Bios_memcpy
	mov r0, sp
	add r0, #2
	strh r4, [r0]
	ldr r1, =0x03000BB0
	add r2, r5, #0
	bl Bios_memcpy
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global loadBGPalettes
loadBGPalettes: @ 0x0801124C
	push {lr}
	add r2, r0, #0
	lsl r3, r1, #0x18
	lsr r3, r3, #0x18
	mov r0, #0xa0
	lsl r0, r0, #0x13
	ldr r1, =0x03000BB0
	bl LoadPalettes
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global LoadSpritePalette
LoadSpritePalette: @ 0x0801126C
	push {lr}
	add r2, r0, #0
	lsl r3, r1, #0x18
	lsr r3, r3, #0x18
	ldr r0, =0x05000200
	ldr r1, =0x03000B30
	bl LoadPalettes
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global clearBGPalettes
clearBGPalettes: @ 0x0801128C
	push {lr}
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x18
	lsr r3, r3, #0x18
	lsl r2, r2, #0x18
	lsr r2, r2, #0x18
	ldr r0, =0x03000BB0
	add r1, r3, #0
	bl clearPalettesAtAddr
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global clearSpritePalettes
clearSpritePalettes: @ 0x080112AC
	push {lr}
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x18
	lsr r3, r3, #0x18
	lsl r2, r2, #0x18
	lsr r2, r2, #0x18
	ldr r0, =0x03000B30
	add r1, r3, #0
	bl clearPalettesAtAddr
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global LoadPalettes
LoadPalettes: @ 0x080112CC
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	add r7, r0, #0
	add r6, r1, #0
	add r5, r2, #0
	lsl r3, r3, #0x18
	lsr r4, r3, #0x18
	mov r8, r4
	add r0, r6, #0
	add r1, r5, #0
	add r2, r4, #0
	bl getIndexOfSelectedPal
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	asr r0, r0, #0x10
	mov r1, #1
	neg r1, r1
	cmp r0, r1
	bne _0801131A
	add r0, r6, #0
	add r1, r5, #0
	add r2, r4, #0
	bl findFirstVacantPal
	lsl r0, r0, #0x10
	ldr r2, =0x040000D4
	str r5, [r2]
	lsr r3, r0, #0x10
	asr r0, r0, #0xb
	add r0, r7, r0
	str r0, [r2, #4]
	lsl r0, r4, #3
	mov r1, #0x84
	lsl r1, r1, #0x18
	orr r0, r1
	str r0, [r2, #8]
	ldr r0, [r2, #8]
_0801131A:
	lsl r4, r3, #0x10
	asr r4, r4, #0x10
	lsl r0, r4, #3
	add r0, r6, r0
	add r1, r5, #0
	mov r2, r8
	bl AddPalsToTable
	add r0, r4, #0
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global clearPalettesAtAddr
clearPalettesAtAddr: @ 0x0801133C
	push {lr}
	lsl r1, r1, #0x18
	lsl r2, r2, #0x18
	lsr r2, r2, #0x18
	lsr r1, r1, #0x15
	add r0, r0, r1
	add r1, r2, #0
	bl clearPalettes_
	pop {r0}
	bx r0
	.byte 0x00, 0x00

thumb_func_global getIndexOfSelectedPal
getIndexOfSelectedPal: @ 0x08011354
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	add r3, r0, #0
	mov r8, r1
	lsl r2, r2, #0x18
	lsr r6, r2, #0x18
	cmp r6, #0
	bne _0801136E
	b _08011392
_08011368:
	lsl r0, r5, #0x10
	asr r0, r0, #0x10
	b _08011396
_0801136E:
	mov r5, #0
	mov r0, #0x10
	sub r0, r0, r6
	cmp r5, r0
	bgt _08011392
	add r7, r0, #0
	add r4, r3, #0
_0801137C:
	add r0, r4, #0
	mov r1, r8
	add r2, r6, #0
	bl isPalAlreadyLoaded
	cmp r0, #0
	bne _08011368
	add r4, #8
	add r5, #1
	cmp r5, r7
	ble _0801137C
_08011392:
	mov r0, #1
	neg r0, r0
_08011396:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1

thumb_func_global findFirstVacantPal
findFirstVacantPal: @ 0x080113A0
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	add r3, r0, #0
	mov r8, r1
	lsl r2, r2, #0x18
	lsr r6, r2, #0x18
	cmp r6, #0
	bne _080113BA
	b _080113DE
_080113B4:
	lsl r0, r5, #0x10
	asr r0, r0, #0x10
	b _080113E2
_080113BA:
	mov r5, #0
	mov r0, #0x10
	sub r0, r0, r6
	cmp r5, r0
	bgt _080113DE
	add r7, r0, #0
	add r4, r3, #0
_080113C8:
	add r0, r4, #0
	mov r1, r8
	add r2, r6, #0
	bl palNotLoadedWithFlag
	cmp r0, #0
	bne _080113B4
	add r4, #8
	add r5, #1
	cmp r5, r7
	ble _080113C8
_080113DE:
	mov r0, #1
	neg r0, r0
_080113E2:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1



	thumb_func_global sub_80113EC
sub_80113EC: @ 0x080113EC
	push {r4, r5, r6, r7, lr}
	add r2, r0, #0
	lsl r1, r1, #0x18
	lsr r6, r1, #0x18
	cmp r6, #0
	bne _08011400
	b _08011422
_080113FA:
	lsl r0, r5, #0x10
	asr r0, r0, #0x10
	b _08011426
_08011400:
	mov r5, #0
	mov r0, #0x10
	sub r0, r0, r6
	cmp r5, r0
	bgt _08011422
	add r7, r0, #0
	add r4, r2, #0
_0801140E:
	add r0, r4, #0
	add r1, r6, #0
	bl sub_801142C
	cmp r0, #0
	bne _080113FA
	add r4, #8
	add r5, #1
	cmp r5, r7
	ble _0801140E
_08011422:
	mov r0, #1
	neg r0, r0
_08011426:
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	
	
	
	thumb_func_global sub_801142C
sub_801142C: @ 0x0801142C
	push {r4, lr}
	add r2, r0, #0
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	mov r4, #1
	mov r3, #0
	b _0801143E
_0801143A:
	add r3, #1
	add r2, #8
_0801143E:
	cmp r3, r1
	bge _0801144A
	ldrb r0, [r2, #4]
	cmp r0, #0
	beq _0801143A
	mov r4, #0
_0801144A:
	add r0, r4, #0
	pop {r4}
	pop {r1}
	bx r1
	.byte 0x00, 0x00



thumb_func_global isPalAlreadyLoaded
isPalAlreadyLoaded: @ 0x08011454
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	add r4, r0, #0
	add r5, r1, #0
	lsl r2, r2, #0x18
	lsr r2, r2, #0x18
	mov r8, r2
	mov r7, #1
	mov r6, #0
	b _08011470
_0801146A:
	add r6, #1
	add r4, #8
	add r5, #0x20
_08011470:
	cmp r6, r8
	bge _0801148A
	ldrb r0, [r4, #4]
	cmp r0, #0
	beq _08011488
	ldr r0, [r4]
	add r1, r5, #0
	mov r2, #0x20
	bl memcmp
	cmp r0, #0
	beq _0801146A
_08011488:
	mov r7, #0
_0801148A:
	add r0, r7, #0
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.byte 0x00, 0x00

thumb_func_global palNotLoadedWithFlag
palNotLoadedWithFlag: @ 0x08011498
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	add r4, r0, #0
	add r5, r1, #0
	lsl r2, r2, #0x18
	lsr r2, r2, #0x18
	mov r8, r2
	mov r7, #1
	mov r6, #0
	b _080114B4
_080114AE:
	add r6, #1
	add r4, #8
	add r5, #0x20
_080114B4:
	cmp r6, r8
	bge _080114CE
	ldrb r0, [r4, #4]
	cmp r0, #0
	beq _080114AE
	ldr r0, [r4]
	add r1, r5, #0
	mov r2, #0x20
	bl memcmp
	cmp r0, #0
	beq _080114AE
	mov r7, #0
_080114CE:
	add r0, r7, #0
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.byte 0x00, 0x00

thumb_func_global AddPalsToTable
AddPalsToTable: @ 0x080114DC
	push {r4, lr}
	add r3, r0, #0
	add r4, r1, #0
	lsl r2, r2, #0x18
	lsr r2, r2, #0x18
	mov r1, #0
	cmp r1, r2
	bhs _08011504
_080114EC:
	lsl r0, r1, #5
	add r0, r4, r0
	str r0, [r3]
	ldrb r0, [r3, #4]
	add r0, #1
	strb r0, [r3, #4]
	add r0, r1, #1
	lsl r0, r0, #0x18
	lsr r1, r0, #0x18
	add r3, #8
	cmp r1, r2
	blo _080114EC
_08011504:
	pop {r4}
	pop {r0}
	bx r0
	.byte 0x00, 0x00

thumb_func_global clearPalettes_
clearPalettes_: @ 0x0801150C
	push {r4, lr}
	add r2, r0, #0
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	mov r3, #0
	cmp r3, r1
	bhs _0801153A
	mov r4, #0
_0801151C:
	ldrb r0, [r2, #4]
	cmp r0, #0
	beq _0801152C
	sub r0, #1
	strb r0, [r2, #4]
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0801152E
_0801152C:
	str r4, [r2]
_0801152E:
	add r0, r3, #1
	lsl r0, r0, #0x18
	lsr r3, r0, #0x18
	add r2, #8
	cmp r3, r1
	blo _0801151C
_0801153A:
	pop {r4}
	pop {r0}
	bx r0
	
	
	
	
	
	@TODO Split file here?
	
thumb_func_global sub_8011540
sub_8011540: @ 0x08011540
	push {r4, r5, r6, r7, lr}
	ldr r6, [r0]
	ldrh r2, [r0, #8]
	add r2, r6, r2
	ldr r7, [r0, #4]
	ldrh r0, [r0, #0xa]
	add r0, r0, r7
	mov ip, r0
	ldr r3, [r1]
	ldrh r0, [r1, #8]
	add r5, r3, r0
	ldr r4, [r1, #4]
	ldrh r0, [r1, #0xa]
	add r0, r4, r0
	cmp r2, r3
	ble _08011570
	cmp r6, r5
	bge _08011570
	cmp ip, r4
	ble _08011570
	cmp r7, r0
	bge _08011570
	mov r0, #1
	b _08011572
_08011570:
	mov r0, #0
_08011572:
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	
	
	
	
	thumb_func_global sub_8011578
sub_8011578: @ 0x08011578
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	mov sb, r0
	add r6, r1, #0
	ldr r1, [r0]
	ldr r0, [r6]
	sub r7, r1, r0
	mov r1, sb
	ldrh r0, [r1, #8]
	add r0, r0, r7
	mov r8, r0
	ldr r1, [r1, #4]
	ldr r0, [r6, #4]
	sub r5, r1, r0
	ldrh r0, [r6, #0xa]
	lsr r0, r0, #1
	cmp r5, r0
	bgt _080115E6
	cmn r5, r0
	blt _080115E6
	lsl r4, r5, #1
	ldrh r1, [r6, #0xa]
	add r2, r4, r1
	ldrh r0, [r6, #8]
	mul r0, r2, r0
	lsl r1, r1, #1
	bl Bios_Div
	cmp r0, r7
	blt _080115BC
	cmp r0, r8
	ble _0801163A
_080115BC:
	neg r0, r0
	cmp r0, r7
	blt _080115C6
	cmp r0, r8
	ble _0801163A
_080115C6:
	ldrh r1, [r6, #0xa]
	sub r2, r4, r1
	ldrh r0, [r6, #8]
	mul r0, r2, r0
	lsl r1, r1, #1
	bl Bios_Div
	cmp r0, r7
	blt _080115DC
	cmp r0, r8
	ble _0801163A
_080115DC:
	neg r0, r0
	cmp r0, r7
	blt _080115E6
	cmp r0, r8
	ble _0801163A
_080115E6:
	mov r1, sb
	ldrh r0, [r1, #0xa]
	add r5, r5, r0
	ldrh r0, [r6, #0xa]
	lsr r0, r0, #1
	cmp r5, r0
	bgt _0801163E
	cmn r5, r0
	blt _0801163E
	lsl r4, r5, #1
	ldrh r1, [r6, #0xa]
	add r2, r4, r1
	ldrh r0, [r6, #8]
	mul r0, r2, r0
	lsl r1, r1, #1
	bl Bios_Div
	cmp r0, r7
	blt _08011610
	cmp r0, r8
	ble _0801163A
_08011610:
	neg r0, r0
	cmp r0, r7
	blt _0801161A
	cmp r0, r8
	ble _0801163A
_0801161A:
	ldrh r1, [r6, #0xa]
	sub r2, r4, r1
	ldrh r0, [r6, #8]
	mul r0, r2, r0
	lsl r1, r1, #1
	bl Bios_Div
	cmp r0, r7
	blt _08011630
	cmp r0, r8
	ble _0801163A
_08011630:
	neg r0, r0
	cmp r0, r7
	blt _0801163E
	cmp r0, r8
	bgt _0801163E
_0801163A:
	mov r0, #1
	b _08011640
_0801163E:
	mov r0, #0
_08011640:
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	
	
	
	thumb_func_global sub_801164C
sub_801164C: @ 0x0801164C
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	add r5, r0, #0
	add r6, r1, #0
	ldr r1, [r5, #4]
	ldr r0, [r6, #4]
	sub r4, r1, r0
	ldr r1, [r5]
	ldr r0, [r6]
	sub r7, r1, r0
	ldrh r0, [r5, #8]
	add r0, r0, r7
	mov r8, r0
	ldrh r0, [r6, #8]
	add r1, r0, r4
	sub r0, r0, r4
	mul r0, r1, r0
	cmp r0, #0
	blt _0801168E
	bl Bios_Sqrt
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r7, r0
	bgt _08011684
	cmp r0, r8
	ble _080116B8
_08011684:
	neg r0, r0
	cmp r7, r0
	bgt _0801168E
	cmp r0, r8
	ble _080116B8
_0801168E:
	ldrh r0, [r5, #0xa]
	add r4, r4, r0
	ldrh r0, [r6, #8]
	add r1, r0, r4
	sub r0, r0, r4
	mul r0, r1, r0
	cmp r0, #0
	blt _080116BC
	bl Bios_Sqrt
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r7, r0
	bgt _080116AE
	cmp r0, r8
	ble _080116B8
_080116AE:
	neg r0, r0
	cmp r7, r0
	bgt _080116BC
	cmp r0, r8
	bgt _080116BC
_080116B8:
	mov r0, #1
	b _080116BE
_080116BC:
	mov r0, #0
_080116BE:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	
	
	
	thumb_func_global sub_80116C8
sub_80116C8: @ 0x080116C8
	push {r4, r5, lr}
	ldr r4, [r0]
	ldr r2, [r1]
	sub r4, r4, r2
	ldr r3, [r0, #4]
	ldr r2, [r1, #4]
	sub r3, r3, r2
	ldrh r2, [r0, #8]
	ldrh r0, [r1, #8]
	add r5, r2, r0
	add r1, r5, #0
	mul r1, r5, r1
	add r5, r1, #0
	sub r2, r2, r0
	add r0, r2, #0
	mul r0, r2, r0
	add r2, r0, #0
	add r1, r4, #0
	mul r1, r4, r1
	add r0, r3, #0
	mul r0, r3, r0
	add r1, r1, r0
	cmp r1, r5
	bls _080116FC
	mov r0, #0
	b _08011716
_080116FC:
	cmp r1, r5
	bne _08011704
	mov r0, #2
	b _08011716
_08011704:
	cmp r1, r2
	bhs _0801170C
	mov r0, #4
	b _08011716
_0801170C:
	cmp r1, r2
	beq _08011714
	mov r0, #1
	b _08011716
_08011714:
	mov r0, #3
_08011716:
	pop {r4, r5}
	pop {r1}
	bx r1
	
	
	
	thumb_func_global sub_801171C
sub_801171C: @ 0x0801171C
	push {r4, lr}
	mov r1, #0
	strh r1, [r0, #0xa]
	strh r1, [r0, #8]
	strh r1, [r0, #2]
	strh r1, [r0]
	add r4, r0, #0
	add r4, #0xc
	mov r2, #0
	add r3, r0, #0
	add r3, #0x16
_08011732:
	add r0, r4, r1
	strb r2, [r0]
	add r0, r3, r1
	strb r2, [r0]
	add r0, r1, #1
	lsl r0, r0, #0x18
	lsr r1, r0, #0x18
	cmp r1, #9
	bls _08011732
	pop {r4}
	pop {r0}
	bx r0
	.byte 0x00, 0x00


thumb_func_global updateKeys
updateKeys: @ 0x0801174C
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	add r3, r0, #0
	ldr r0, =0x04000130
	ldrh r0, [r0]
	mvn r0, r0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r8, r0
	ldrh r2, [r3, #2]
	mov r1, r8
	eor r1, r2
	add r0, r1, #0
	and r0, r2
	strh r0, [r3, #4]
	mov r0, r8
	and r1, r0
	strh r1, [r3]
	strh r0, [r3, #2]
	mov r2, r8
	strh r2, [r3, #8]
	mov r5, #0
	mov r6, #1
	mov r0, #0xc
	add r0, r0, r3
	mov ip, r0
	add r7, r3, #0
	add r7, #0x16
	ldr r2, =dword_8039E4C
	mov sb, r2
_0801178C:
	ldrh r1, [r3, #8]
	asr r1, r5
	and r1, r6
	cmp r1, #0
	bne _080117AC
	mov r2, ip
	add r0, r2, r5
	strb r1, [r0]
	add r0, r7, r5
	strb r1, [r0]
	b _08011808
	.align 2, 0
.pool
_080117AC:
	ldrh r1, [r3, #0xa]
	asr r1, r5
	and r1, r6
	cmp r1, #0
	beq _080117F4
	mov r0, ip
	add r2, r0, r5
	ldrb r0, [r2]
	add r0, #1
	strb r0, [r2]
	add r4, r7, r5
	ldrb r1, [r4]
	add r1, sb
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	ldrb r1, [r1]
	cmp r0, r1
	bls _080117EA
	add r0, r6, #0
	lsl r0, r5
	ldrh r1, [r3, #8]
	orr r0, r1
	mov r1, #0
	strh r0, [r3, #8]
	strb r1, [r2]
	ldrb r0, [r4]
	cmp r0, #1
	bhi _08011808
	add r0, #1
	strb r0, [r4]
	b _08011808
_080117EA:
	add r1, r6, #0
	lsl r1, r5
	ldrh r0, [r3, #8]
	bic r0, r1
	b _08011806
_080117F4:
	mov r2, ip
	add r0, r2, r5
	strb r1, [r0]
	add r0, r7, r5
	strb r1, [r0]
	add r0, r6, #0
	lsl r0, r5
	ldrh r1, [r3, #8]
	orr r0, r1
_08011806:
	strh r0, [r3, #8]
_08011808:
	add r0, r5, #1
	lsl r0, r0, #0x18
	lsr r5, r0, #0x18
	cmp r5, #9
	bls _0801178C
	mov r0, r8
	strh r0, [r3, #0xa]
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.byte 0x00, 0x00
