.include "asm/macros.inc"
	
	@complete
	

thumb_func_global sub_08027850
sub_08027850: @ 0x08027850
	push {r4, r5, r6, r7, lr}
	ldr r4, [sp, #0x14]
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	lsl r2, r2, #0x10
	lsr r6, r2, #0x10
	lsl r3, r3, #0x10
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	lsr r7, r3, #0x10
	cmp r3, #0
	blt _08027872
	lsl r0, r4, #0x10
	cmp r0, #0
	bge _0802788C
_08027872:
	ldr r0, =0x03003D90
	lsl r1, r5, #1
	add r0, r1, r0
	mov r2, #0
	strh r2, [r0]
	ldr r0, =0x03003D88
	add r1, r1, r0
	strh r2, [r1]
	b _080278A6
	.align 2, 0
.pool
_0802788C:
	ldr r2, =0x03003D90
	lsl r3, r5, #1
	add r2, r3, r2
	lsl r0, r1, #8
	add r1, r1, r7
	orr r0, r1
	strh r0, [r2]
	ldr r0, =0x03003D88
	add r3, r3, r0
	lsl r0, r6, #8
	add r1, r6, r4
	orr r0, r1
	strh r0, [r3]
_080278A6:
	ldr r1, =0x03003D7C
	mov r0, #0
	strh r0, [r1]
	ldr r1, =0x03003D8C
	mov r0, #0x3f
	strh r0, [r1]
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global cb_loadLevelIntroScroll
cb_loadLevelIntroScroll: @ 0x080278C8
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r0, =0x03000590
	mov r1, #2
	bl allocFont
	ldr r0, =0x0300058A
	mov r1, #0
	strh r1, [r0]
	ldr r0, =0x03000582
	strh r1, [r0]
	ldr r1, =0x03000584
	mov r0, #0
	str r0, [r1]
	bl stopSoundTracks
	ldr r0, =(cb_levelIntroScroll+1)
	bl setCurrentTaskFunc
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global cb_levelIntroScroll
cb_levelIntroScroll: @ 0x08027928
	push {r4, r5, lr}
	ldr r0, =0x03002080
	ldrh r1, [r0, #0xc]
	mov r0, #8
	and r0, r1
	ldr r5, =0x03000582
	cmp r0, #0
	beq _0802793C
	mov r0, #9
	strh r0, [r5]
_0802793C:
	ldr r1, =introScrollStates
	add r4, r5, #0
	ldrh r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl call_r0
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #2
	beq _0802797A
	cmp r0, #2
	bgt _0802796C
	cmp r0, #1
	beq _08027972
	b _0802798C
	.align 2, 0
.pool
_0802796C:
	cmp r0, #3
	beq _08027988
	b _0802798C
_08027972:
	ldrh r0, [r4]
	add r0, #1
	strh r0, [r4]
	b _0802798C
_0802797A:
	ldr r0, =(cb_startLevelLoad_2+1)
	bl setCurrentTaskFunc
	b _0802798C
	.align 2, 0
.pool
_08027988:
	mov r0, #9
	strh r0, [r5]
_0802798C:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global unused_ninja_sub_8027994
unused_ninja_sub_8027994: @ 0x08027994
	push {r4, lr}
	sub sp, #8
	add r0, sp, #4
	mov r4, #0
	strh r4, [r0]
	ldr r1, =0x06008000
	ldr r2, =0x01000400
	bl Bios_memcpy
	ldr r0, =ending_ninja_face_pal
	mov r2, #0xa0
	lsl r2, r2, #0x13
	mov r1, #2
	bl memcpy_pal
	ldr r0, =unused_ending_ninja_face_tiles
	mov r1, #0xc0
	lsl r1, r1, #0x13
	bl Bios_LZ77_16_2
	ldr r0, =unused_ending_ninja_face_map
	ldr r1, =0x06008180
	mov r4, #0
	str r4, [sp]
	mov r2, #0
	mov r3, #0
	bl LZ77_mapCpy
	ldr r1, =0x03003D00
	ldr r0, =0x00001003
	strh r0, [r1, #6]
	ldr r2, =0x03003CD4
	ldrh r0, [r2]
	mov r3, #0x80
	lsl r3, r3, #4
	add r1, r3, #0
	orr r0, r1
	strh r0, [r2]
	bl gfx_related_sub_800F674
	bl sub_0801025C
	ldr r0, =0x03000584
	str r4, [r0]
	mov r0, #1
	add sp, #8
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global unused_sub_8027A20
unused_sub_8027A20: @ 0x08027A20
	push {lr}
	mov r0, #2
	mov r1, #0x14
	mov r2, #0x78
	mov r3, #0x50
	bl undoScreenTransition
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _08027A38
	mov r0, #0
	b _08027A3A
_08027A38:
	mov r0, #1
_08027A3A:
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global unused_sub_8027A40
unused_sub_8027A40: @ 0x08027A40
	ldr r1, =0x03000584
	ldr r0, [r1]
	add r0, #1
	str r0, [r1]
	cmp r0, #0x3c
	bhi _08027A54
	mov r0, #0
	b _08027A5A
	.align 2, 0
.pool
_08027A54:
	mov r0, #0
	str r0, [r1]
	mov r0, #1
_08027A5A:
	bx lr

thumb_func_global unused_sub_8027A5C
unused_sub_8027A5C: @ 0x08027A5C
	push {lr}
	mov r0, #2
	mov r1, #0x14
	mov r2, #0x78
	mov r3, #0x50
	bl TransitionToBlack
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _08027A74
	mov r0, #0
	b _08027A8C
_08027A74:
	ldr r2, =0x03003CD4
	ldrh r1, [r2]
	ldr r0, =0x0000F7FF
	and r0, r1
	strh r0, [r2]
	bl sub_080102B0
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	mov r0, #1
_08027A8C:
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global unused_sub_8027A98
unused_sub_8027A98: @ 0x08027A98
	push {lr}
	ldr r0, =0x03002080
	ldrh r1, [r0, #0xe]
	mov r0, #0x30
	and r0, r1
	ldr r2, =0x0300059C
	cmp r0, #0
	beq _08027AD0
	mov r0, #0x20
	and r0, r1
	cmp r0, #0
	beq _08027AC4
	ldr r0, [r2]
	cmp r0, #0
	beq _08027AD0
	sub r0, #1
	b _08027ACE
	.align 2, 0
.pool
_08027AC4:
	ldr r1, [r2]
	ldr r0, =0x000002CF
	cmp r1, r0
	bhi _08027AD0
	add r0, r1, #1
_08027ACE:
	str r0, [r2]
_08027AD0:
	ldrh r0, [r2]
	bl updateScrollTilemap
	ldr r0, =0x03002080
	ldrh r1, [r0, #0xc]
	mov r0, #9
	and r0, r1
	cmp r0, #0
	bne _08027AF0
	mov r0, #0
	b _08027AF2
	.align 2, 0
.pool
_08027AF0:
	mov r0, #1
_08027AF2:
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global introScrollState0
introScrollState0: @ 0x08027AF8
	push {r4, r5, r6, lr}
	mov r6, r8
	push {r6}
	sub sp, #4
	bl resetAffineTransformations
	ldr r0, =levelintro_borderPal
	mov r1, #1
	bl LoadSpritePalette
	add r5, r0, #0
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	mov r0, #0xc2
	lsl r0, r0, #1
	bl allocateSpriteTiles
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	ldr r0, =unk_81F120C
	lsl r1, r4, #5
	ldr r2, =0x06010000
	mov r8, r2
	add r1, r8
	bl Bios_LZ77_16_2
	ldr r0, =levelintro_scrollAnim
	mov r6, #0
	str r6, [sp]
	mov r1, #0
	add r2, r4, #0
	mov r3, #0
	bl allocSprite
	ldr r1, =0x030005A0
	str r0, [r1]
	ldr r2, =0x0300059C
	mov r1, #0xf0
	str r1, [r2]
	strh r1, [r0, #0x10]
	strh r6, [r0, #0x12]
	add r0, #0x22
	strb r5, [r0]
	bl startLevelIntro
	ldr r0, =ending_ninja_face_pal
	mov r1, #2
	bl LoadSpritePalette
	add r5, r0, #0
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	mov r0, #0x20
	bl allocateSpriteTiles
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	ldr r0, =levelintro_sliceEffectTiles
	lsl r1, r4, #5
	add r1, r8
	bl Bios_LZ77_16_2
	ldr r0, =levelintro_sliceAnim
	str r6, [sp]
	mov r1, #0
	add r2, r4, #0
	mov r3, #0
	bl allocSprite
	ldr r1, =0x030005A4
	str r0, [r1]
	strh r6, [r0, #0x10]
	ldr r3, =0x0000FFF8
	strh r3, [r0, #0x12]
	add r0, #0x22
	strb r5, [r0]
	ldr r1, [r1]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	ldr r0, =0x030022F8
	strh r3, [r0, #6]
	mov r0, #0xa0
	str r0, [sp]
	mov r0, #1
	mov r1, #0
	mov r2, #0
	mov r3, #0xf0
	bl sub_08027850
	ldr r2, =0x03003CD4
	ldrh r0, [r2]
	mov r3, #0x80
	lsl r3, r3, #7
	add r1, r3, #0
	orr r0, r1
	strh r0, [r2]
	ldr r0, =0x03000584
	str r6, [r0]
	mov r0, #1
	add sp, #4
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global introScrollState1
introScrollState1: @ 0x08027C0C
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _08027C22
	mov r0, #0
	b _08027C34
_08027C22:
	ldr r1, =0x0300059C
	mov r0, #0xf0
	str r0, [r1]
	bl updateScrollTilemap
	mov r0, #0xd7
	bl playSong
	mov r0, #1
_08027C34:
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global introScrollState2
introScrollState2: @ 0x08027C3C
	push {r4, lr}
	sub sp, #4
	ldr r4, =0x030005A0
	ldr r2, [r4]
	mov r1, #0x10
	ldrsh r0, [r2, r1]
	sub r0, #6
	mov r1, #0x20
	neg r1, r1
	cmp r0, r1
	blt _08027C78
	ldrh r0, [r2, #0x10]
	sub r0, #6
	strh r0, [r2, #0x10]
	mov r0, #0x10
	ldrsh r3, [r2, r0]
	mov r0, #0xa0
	str r0, [sp]
	mov r0, #1
	mov r1, #0
	mov r2, #0
	bl sub_08027850
	ldr r0, [r4]
	bl updateSpriteAnimation
	mov r0, #0
	b _08027C90
	.align 2, 0
.pool
_08027C78:
	add r0, r2, #0
	bl freeSprite
	ldr r1, =0x03000584
	mov r0, #0
	str r0, [r1]
	ldr r2, =0x03003CD4
	ldrh r1, [r2]
	ldr r0, =0x0000BFFF
	and r0, r1
	strh r0, [r2]
	mov r0, #1
_08027C90:
	add sp, #4
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global introScrollState3
introScrollState3: @ 0x08027CA4
	push {lr}
	ldr r2, =0x03000584
	ldr r0, [r2]
	add r0, #1
	str r0, [r2]
	cmp r0, #0xf
	bhi _08027CBC
	mov r0, #0
	b _08027CCE
	.align 2, 0
.pool
_08027CBC:
	ldr r0, =0x03000598
	mov r1, #0
	strh r1, [r0]
	mov r0, #0
	str r0, [r2]
	mov r0, #3
	bl playSong
	mov r0, #1
_08027CCE:
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global introTextScrollStuff
introTextScrollStuff: @ 0x08027CD8
	push {lr}
	ldr r0, =0x03002080
	ldr r0, [r0]
	mov r1, #3
	bl Bios_modulo
	cmp r0, #0
	beq _08027D24
	ldr r0, =0x03000598
	bl drawIntroText
	ldr r2, =0x0300059C
	ldr r1, [r2]
	ldr r0, =0x0000032F
	cmp r1, r0
	bls _08027D20
	add r0, #1
	str r0, [r2]
	bl updateScrollTilemap
	ldr r1, =0x03000584
	mov r0, #0
	str r0, [r1]
	mov r0, #1
	b _08027D2E
	.align 2, 0
.pool
_08027D20:
	add r0, r1, #1
	str r0, [r2]
_08027D24:
	ldr r0, =0x0300059C
	ldrh r0, [r0]
	bl updateScrollTilemap
	mov r0, #0
_08027D2E:
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global drawIntroText
drawIntroText: @ 0x08027D38
	push {r4, r5, lr}
	sub sp, #4
	add r5, r0, #0
	ldr r0, =0x0300059C
	ldr r1, [r0]
	mov r0, #0xd8
	lsl r0, r0, #1
	cmp r1, r0
	beq _08027D6E
	cmp r1, r0
	bhi _08027D58
	cmp r1, #0xf0
	beq _08027D68
	b _08027D7E
	.align 2, 0
.pool
_08027D58:
	mov r0, #0x9c
	lsl r0, r0, #2
	cmp r1, r0
	beq _08027D74
	add r0, #0xc0
	cmp r1, r0
	beq _08027D7A
	b _08027D7E
_08027D68:
	mov r4, #0
	mov r2, #0
	b _08027D80
_08027D6E:
	mov r4, #1
	mov r2, #0
	b _08027D80
_08027D74:
	mov r4, #2
	mov r2, #3
	b _08027D80
_08027D7A:
	mov r4, #3
	b _08027D80
_08027D7E:
	mov r4, #4
_08027D80:
	ldrh r0, [r5]
	cmp r0, #6
	bhi _08027E66
	lsl r0, r0, #2
	ldr r1, =_08027D94
	add r0, r0, r1
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
.pool
_08027D94: @ jump table
	.4byte _08027DB0 @ case 0
	.4byte _08027DD8 @ case 1
	.4byte _08027E08 @ case 2
	.4byte _08027E24 @ case 3
	.4byte _08027DD8 @ case 4
	.4byte _08027DEE @ case 5
	.4byte _08027E5C @ case 6
_08027DB0:
	cmp r4, #2
	bhi _08027DCE
	add r0, r4, #0
	add r1, r2, #0
	bl drawIntroScrollStrings
	mov r0, #0x10
	mov r1, #1
	bl setBldCnt
	cmp r4, #1
	bls _08027E18
	mov r0, #4
	strh r0, [r5]
	b _08027E66
_08027DCE:
	cmp r4, #3
	bne _08027E66
	bl gfx_related_sub_800F674
	b _08027E66
_08027DD8:
	mov r0, #0x10
	mov r1, #1
	mov r2, #0x14
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	beq _08027E66
	ldrh r0, [r5]
	add r0, #1
	b _08027E5E
_08027DEE:
	ldr r0, =0x03000584
	ldr r0, [r0]
	mov r1, #4
	and r0, r1
	cmp r0, #0
	beq _08027E04
	bl sub_0802819C
	b _08027E08
	.align 2, 0
.pool
_08027E04:
	bl sub_080281CC
_08027E08:
	ldr r1, =0x03000584
	ldr r0, [r1]
	add r0, #1
	str r0, [r1]
	cmp r0, #0x78
	bls _08027E66
	bl sub_080281CC
_08027E18:
	ldrh r0, [r5]
	add r0, #1
	strh r0, [r5]
	b _08027E66
	.align 2, 0
.pool
_08027E24:
	mov r0, #0x10
	mov r1, #1
	mov r2, #0x14
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	beq _08027E66
	mov r0, sp
	mov r4, #0
	strh r4, [r0]
	ldr r1, =0x030037D0
	ldr r0, =0x0300058A
	ldrh r2, [r0]
	lsl r2, r2, #3
	mov r0, #0x80
	lsl r0, r0, #0x11
	orr r2, r0
	mov r0, sp
	bl Bios_memcpy
	strh r4, [r5]
	b _08027E60
	.align 2, 0
.pool
_08027E5C:
	mov r0, #0
_08027E5E:
	strh r0, [r5]
_08027E60:
	ldr r1, =0x03000584
	mov r0, #0
	str r0, [r1]
_08027E66:
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global introScrollState5
introScrollState5: @ 0x08027E74
	push {r4, lr}
	ldr r4, =0x03000584
	ldr r0, [r4]
	add r0, #1
	str r0, [r4]
	cmp r0, #0x1e
	bhi _08027E8C
	mov r0, #0
	b _08027EBC
	.align 2, 0
.pool
_08027E8C:
	mov r0, #3
	mov r1, #2
	mov r2, #0x1e
	mov r3, #0
	bl sub_0802AAB8
	ldr r1, =0x030022F8
	mov r3, #0
	ldr r0, =0x0000FFF8
	strh r0, [r1, #6]
	strh r0, [r1, #4]
	ldr r0, =0x030005A4
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	str r3, [r4]
	mov r0, #0xce
	bl playSong
	mov r0, #1
_08027EBC:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global introScrollState6
introScrollState6: @ 0x08027ED0
	push {r4, lr}
	ldr r4, =0x030005A4
	ldr r0, [r4]
	bl updateSpriteAnimation
	cmp r0, #0
	bne _08027EE8
	mov r0, #0
	b _08027EF0
	.align 2, 0
.pool
_08027EE8:
	ldr r0, [r4]
	bl freeSprite
	mov r0, #1
_08027EF0:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global introScrollState7
introScrollState7: @ 0x08027EF8
	push {r4, lr}
	ldr r0, =0x030022F8
	ldrh r1, [r0, #6]
	sub r1, #2
	strh r1, [r0, #6]
	ldrh r1, [r0, #4]
	add r1, #2
	strh r1, [r0, #4]
	mov r2, #0
	ldr r3, =0x0300058A
	ldrh r0, [r3]
	cmp r2, r0
	bhs _08027F2A
	ldr r4, =0x030037D0
_08027F14:
	lsl r0, r2, #4
	add r0, r0, r4
	ldrb r1, [r0]
	add r1, #2
	strb r1, [r0]
	add r0, r2, #1
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	ldrh r0, [r3]
	cmp r2, r0
	blo _08027F14
_08027F2A:
	mov r0, #0x3f
	mov r1, #0
	mov r2, #0xf
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _08027F4C
	mov r0, #0
	b _08027F54
	.align 2, 0
.pool
_08027F4C:
	ldr r1, =0x03003CD4
	mov r0, #0
	strh r0, [r1]
	mov r0, #1
_08027F54:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global introScrollState8
introScrollState8: @ 0x08027F60
	push {lr}
	mov r0, #0x3f
	mov r1, #0
	mov r2, #0x14
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _08027F76
	mov r0, #0
	b _08027F80
_08027F76:
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	mov r0, #1
_08027F80:
	pop {r1}
	bx r1

thumb_func_global investigateUnused
investigateUnused: @ 0x08027F84
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl gfx_related_sub_800F674
	mov r0, #1
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global unused_face
unused_face: @ 0x08027FA0
	push {r4, lr}
	ldr r4, =unusedFaceRegData
	add r0, r4, #0
	bl unused_loadFace
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08027FEC
	ldr r2, =0x03003D74
	mov r0, #1
	ldrb r4, [r4]
	lsl r0, r4
	mov r3, #0xfd
	lsl r3, r3, #6
	add r1, r3, #0
	orr r0, r1
	strh r0, [r2]
	ldr r1, =0x03003D94
	mov r2, #0x80
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	ldr r1, =0x03000584
	mov r0, #0
	str r0, [r1]
	mov r0, #0xc9
	bl playSong
	mov r0, #1
	b _08027FEE
	.align 2, 0
.pool
_08027FEC:
	mov r0, #3
_08027FEE:
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global unused_fadeFace
unused_fadeFace: @ 0x08027FF4
	push {r4, lr}
	ldr r3, =0x03000584
	ldr r0, [r3]
	add r0, #1
	str r0, [r3]
	mov r2, #4
	and r2, r0
	cmp r2, #0
	bne _08028024
	ldr r4, =0x03003D94
	ldrb r0, [r4]
	mov r1, #0xf
	and r1, r0
	add r1, #1
	cmp r1, #0xf
	bls _08028016
	mov r1, #0x10
_08028016:
	mov r0, #0x10
	sub r0, r0, r1
	lsl r0, r0, #8
	orr r0, r1
	strh r0, [r4]
	cmp r1, #0x10
	beq _08028030
_08028024:
	mov r0, #0
	b _08028038
	.align 2, 0
.pool
_08028030:
	ldr r0, =0x03003D74
	strh r2, [r0]
	str r2, [r3]
	mov r0, #1
_08028038:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool
	
	thumb_func_global sub_8028044
sub_8028044: @ 0x08028044
	ldr r1, =0x03000584
	ldr r0, [r1]
	add r0, #1
	str r0, [r1]
	cmp r0, #0xb4
	bhi _08028058
	mov r0, #0
	b locret_802805E
	.align 2, 0
.pool
_08028058:
	mov r0, #0
	str r0, [r1]
	mov r0, #1
locret_802805E:
	bx lr
	
	
thumb_func_global unused_screenFade
unused_screenFade: @ 0x08028060
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _08028078
	mov r0, #0
	b _0802807A
_08028078:
	mov r0, #1
_0802807A:
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global introScrollState9
introScrollState9: @ 0x08028080
	push {lr}
	bl stopSoundTracks
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl resetAffineTransformations
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	mov r0, #2
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global drawIntroScrollStrings
drawIntroScrollStrings: @ 0x080280A8
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #8
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov sl, r1
	ldr r5, =scrollIntroStrings
	bl getLevelIntroId
	ldr r1, =0x03002080
	add r1, #0x2c
	lsl r2, r4, #1
	add r2, r2, r4
	ldrb r1, [r1]
	add r2, r2, r1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	lsl r1, r0, #3
	add r1, r1, r0
	add r1, r1, r2
	lsl r1, r1, #2
	add r1, r1, r5
	ldr r1, [r1]
	mov r8, r1
	add r0, sp, #4
	mov r4, #0
	strh r4, [r0]
	ldr r1, =0x030037D0
	ldr r5, =0x0300058A
	ldrh r2, [r5]
	lsl r2, r2, #3
	mov r0, #0x80
	lsl r0, r0, #0x11
	orr r2, r0
	add r0, sp, #4
	bl Bios_memcpy
	strh r4, [r5]
	mov r7, #0
	mov r0, r8
	ldrb r0, [r0]
	cmp r7, r0
	bhs _08028176
	ldr r1, =0x03000590
	mov sb, r1
	add r6, r5, #0
_08028110:
	lsl r0, r7, #2
	mov r4, r8
	add r4, #4
	add r4, r4, r0
	ldr r0, [r4]
	ldr r5, [r0, #4]
	ldrb r2, [r6]
	mov r0, #0
	str r0, [sp]
	add r0, r5, #0
	mov r1, sb
	mov r3, #0
	bl bufferString
	ldrb r2, [r6]
	mov r0, sl
	lsl r3, r0, #0x18
	add r0, r5, #0
	mov r1, sb
	lsr r3, r3, #0x18
	bl setStringColor
	ldrb r1, [r6]
	add r0, r5, #0
	bl str_get_last_width_OamX_sum
	mov r1, #0xf0
	sub r1, r1, r0
	lsl r1, r1, #0xf
	lsr r1, r1, #0x10
	ldr r0, [r4]
	ldrb r2, [r0, #1]
	ldrb r3, [r6]
	mov r0, sb
	str r0, [sp]
	add r0, r5, #0
	bl setStringPos
	add r0, r5, #0
	bl strlen_noWhitespaces
	ldrh r1, [r6]
	add r0, r0, r1
	strh r0, [r6]
	add r0, r7, #1
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	mov r0, r8
	ldrb r0, [r0]
	cmp r7, r0
	blo _08028110
_08028176:
	add sp, #8
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802819C
sub_0802819C: @ 0x0802819C
	push {r4, lr}
	mov r1, #0
	ldr r2, =0x0300058A
	ldrh r0, [r2]
	cmp r1, r0
	bhs _080281BE
	ldr r4, =0x030037D0
	mov r3, #0
_080281AC:
	lsl r0, r1, #4
	add r0, r0, r4
	strb r3, [r0, #0xc]
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	ldrh r0, [r2]
	cmp r1, r0
	blo _080281AC
_080281BE:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_080281CC
sub_080281CC: @ 0x080281CC
	push {r4, lr}
	mov r1, #0
	ldr r2, =0x0300058A
	ldrh r0, [r2]
	cmp r1, r0
	bhs _080281EE
	ldr r4, =0x030037D0
	mov r3, #1
_080281DC:
	lsl r0, r1, #4
	add r0, r0, r4
	strb r3, [r0, #0xc]
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	ldrh r0, [r2]
	cmp r1, r0
	blo _080281DC
_080281EE:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
	
	thumb_func_global sub_80281FC
sub_80281FC: @ 0x080281FC
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r0, =0x03000590
	mov r1, #1
	bl allocFont
	ldr r0, =0x03000584
	mov r1, #0
	str r1, [r0]
	ldr r0, =0x03000582
	strh r1, [r0]
	ldr r0, =(cb_switch_postMissionSplash+1)
	bl setCurrentTaskFunc
	pop {r0}
	bx r0
	.align 2, 0
.pool
