.include "asm/macros.inc"

@not everything here is unused
@everything is disassembled

 

	.thumb_func @THUMB_FUNC_START unused_sub_800D6C8
unused_sub_800D6C8: @ 0x0800D6C8
	push {lr}
	ldr r1, =0x03002080
	add r1, #0x2d
	strb r0, [r1]
	mov r0, #0
	bl unused_sub_800D6E0
	pop {r0}
	bx r0
	.align 2, 0
.pool

	.thumb_func @THUMB_FUNC_START unused_sub_800D6E0
unused_sub_800D6E0: @ 0x0800D6E0
	push {lr}
	ldr r1, =0x03002080
	add r1, #0x2e
	strb r0, [r1]
	mov r0, #0
	bl unused_sub_800D6F8
	pop {r0}
	bx r0
	.align 2, 0
.pool

	.thumb_func @THUMB_FUNC_START unused_sub_800D6F8
unused_sub_800D6F8: @ 0x0800D6F8
	push {lr}
	ldr r1, =0x03002080
	add r1, #0x2f
	strb r0, [r1]
	mov r0, #0
	bl unused_sub_800D710
	pop {r0}
	bx r0
	.align 2, 0
.pool

	.thumb_func @THUMB_FUNC_START unused_sub_800D710
unused_sub_800D710: @ 0x0800D710
	push {lr}
	ldr r1, =0x03002080
	add r1, #0x30
	strb r0, [r1]
	mov r0, #0
	bl unused_sub_800D728
	pop {r0}
	bx r0
	.align 2, 0
.pool

	.thumb_func @THUMB_FUNC_START unused_sub_800D728
unused_sub_800D728: @ 0x0800D728
	push {lr}
	ldr r1, =0x03002080
	add r1, #0x31
	strb r0, [r1]
	mov r0, #0
	bl unused_sub_800D740
	pop {r0}
	bx r0
	.align 2, 0
.pool

	.thumb_func @THUMB_FUNC_START unused_sub_800D740
unused_sub_800D740: @ 0x0800D740
	ldr r1, =0x03002080
	add r1, #0x32
	strb r0, [r1]
	bx lr
	.align 2, 0
.pool

	.thumb_func @THUMB_FUNC_START unused_sub_800D74C
unused_sub_800D74C: @ 0x0800D74C
	push {lr}
	ldr r1, =0x03002080
	add r1, #0x2d
	ldrb r0, [r1]
	add r0, #1
	strb r0, [r1]
	mov r0, #0
	bl unused_sub_800D6E0
	pop {r0}
	bx r0
	.align 2, 0
.pool

	.thumb_func @THUMB_FUNC_START unused_sub_800D768
unused_sub_800D768: @ 0x0800D768
	push {lr}
	ldr r1, =0x03002080
	add r1, #0x2e
	ldrb r0, [r1]
	add r0, #1
	strb r0, [r1]
	mov r0, #0
	bl unused_sub_800D6F8
	pop {r0}
	bx r0
	.align 2, 0
.pool

	.thumb_func @THUMB_FUNC_START unused_sub_800D784
unused_sub_800D784: @ 0x0800D784
	push {lr}
	ldr r1, =0x03002080
	add r1, #0x2f
	ldrb r0, [r1]
	add r0, #1
	strb r0, [r1]
	mov r0, #0
	bl unused_sub_800D710
	pop {r0}
	bx r0
	.align 2, 0
.pool

	.thumb_func @THUMB_FUNC_START unused_sub_800D7A0
unused_sub_800D7A0: @ 0x0800D7A0
	push {lr}
	ldr r1, =0x03002080
	add r1, #0x30
	ldrb r0, [r1]
	add r0, #1
	strb r0, [r1]
	mov r0, #0
	bl unused_sub_800D728
	pop {r0}
	bx r0
	.align 2, 0
.pool

	.thumb_func @THUMB_FUNC_START unused_sub_800D7BC
unused_sub_800D7BC: @ 0x0800D7BC
	push {lr}
	ldr r1, =0x03002080
	add r1, #0x31
	ldrb r0, [r1]
	add r0, #1
	strb r0, [r1]
	mov r0, #0
	bl unused_sub_800D740
	pop {r0}
	bx r0
	.align 2, 0
.pool

	.thumb_func @THUMB_FUNC_START unusedd_sub_800D7D8
unusedd_sub_800D7D8: @ 0x0800D7D8
	ldr r1, =0x03002080
	add r1, #0x32
	ldrb r0, [r1]
	add r0, #1
	strb r0, [r1]
	bx lr
	.align 2, 0
.pool


/*
	-copies the function drawSpriteToBuffer to gDrawSpriteToBuffer (0x3000000)
*/
thumb_func_global loadSpriteHandler
loadSpriteHandler: @ 0x0800D7E8
	ldr r1, _0800D830
	ldr r3, _0800D834
	cmp r1, r3
	beq _0800D80A
	ldr r2, _0800D838
	str r1, [r2]
	ldr r0, _0800D83C
	str r0, [r2, #4]
	sub r0, r3, r1
	lsr r1, r0, #0x1f
	add r0, r0, r1
	asr r0, r0, #1
	mov r1, #0x80
	lsl r1, r1, #0x18
	orr r0, r1
	str r0, [r2, #8]
	ldr r0, [r2, #8]
_0800D80A:
	ldr r1, _0800D840
	ldr r3, _0800D844
	cmp r1, r3
	beq _0800D82C
	ldr r2, _0800D838
	str r1, [r2]
	ldr r0, _0800D848
	str r0, [r2, #4]
	sub r0, r3, r1
	lsr r1, r0, #0x1f
	add r0, r0, r1
	asr r0, r0, #1
	mov r1, #0x80
	lsl r1, r1, #0x18
	orr r0, r1
	str r0, [r2, #8]
	ldr r0, [r2, #8]
_0800D82C:
	bx lr
	.align 2, 0
_0800D830: .4byte drawSpriteToBuffer
_0800D834: .4byte dword_83B89E8
_0800D838: .4byte 0x040000D4
_0800D83C: .4byte gDrawSpriteToBuffer
_0800D840: .4byte dword_83B89E8
_0800D844: .4byte unk_83b8cd8
_0800D848: .4byte 0x02020674


thumb_func_global task_first
task_first: @ 0x0800D84C
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	bl clearSprites
	bl resetAffineTransformations
	bl InitCallbacks
	bl clearPalBufs
	bl stopPalFade
	bl clearObjects_maybe
	bl copyUnknown
	bl setPlayerStats_andtuff_8031B14
	ldr r0, =(sub_8025E44+1)
	mov r1, #1
	bl createTask
	ldr r1, =0x03003E20
	str r0, [r1]
	ldr r0, =(cb_checkSoftReset+1)
	bl setCurrentTaskFunc
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global cb_checkSoftReset
cb_checkSoftReset: @ 0x0800D898
	push {lr}
	ldr r2, =0x03002080
	ldrb r0, [r2, #9]
	lsl r0, r0, #0x1d
	cmp r0, #0
	bge _0800D8DE
	ldrh r0, [r2, #0xe]
	mov r1, #0xf
	and r1, r0
	cmp r1, #0xf
	bne _0800D8DE
	ldrh r0, [r2, #0xc]
	and r1, r0
	cmp r1, #0
	beq _0800D8DE
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	bl copyUnknown
	bl setPlayerStats_andtuff_8031B14
	mov r0, #2
	bl sub_8016A4C
	ldr r0, =(cb_ResetToTitle+1)
	bl setCurrentTaskFunc
	bl stopSoundTracks
	bl FreeAllPalFadeTasks
	bl sub_801CB9C
_0800D8DE:
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global cb_ResetToTitle
cb_ResetToTitle: @ 0x0800D8EC
	push {lr}
	ldr r3, =0x040000BA
	ldrh r2, [r3]
	ldr r1, =0x00007FFF
	add r0, r1, #0
	and r0, r2
	strh r0, [r3]
	add r3, #0xc
	ldrh r2, [r3]
	add r0, r1, #0
	and r0, r2
	strh r0, [r3]
	add r3, #0xc
	ldrh r2, [r3]
	add r0, r1, #0
	and r0, r2
	strh r0, [r3]
	ldr r2, =0x040000DE
	ldrh r0, [r2]
	and r1, r0
	strh r1, [r2]
	bl clearGraphics
	mov r0, #2
	bl endPriorityOrHigherTask
	bl resetAffineTransformations
	bl clearSprites
	bl InitCallbacks
	bl clearPalBufs
	bl stopPalFade
	ldr r0, =0x03003E20
	ldr r0, [r0]
	ldr r1, =(cb_loadTitleScreen+1)
	bl setTaskFunc
	ldr r0, =(cb_checkSoftReset+1)
	bl setCurrentTaskFunc
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global copyInterruptHandler
copyInterruptHandler: @ 0x0800D960
	ldr r1, =0x040000D4
	ldr r0, =isr_table
	str r0, [r1]
	ldr r0, =0x030020D0
	str r0, [r1, #4]
	ldr r0, =0x8400000E
	str r0, [r1, #8]
	ldr r0, [r1, #8]
	ldr r0, =interruptHandler
	str r0, [r1]
	ldr r2, =0x030021D0
	str r2, [r1, #4]
	ldr r0, =0x84000048
	str r0, [r1, #8]
	ldr r0, [r1, #8]
	ldr r0, =0x03007FFC
	str r2, [r0]
	bx lr
	.align 2, 0
.pool

thumb_func_global unused_storeInterrupt
unused_storeInterrupt: @ 0x0800D9A4
	add r2, r0, #0
	lsl r1, r1, #0x18
	lsr r0, r1, #0x18
	cmp r0, #0xe
	bhi _0800D9B6
	ldr r1, =0x030020D0
	lsl r0, r0, #2
	add r0, r0, r1
	str r2, [r0]
_0800D9B6:
	bx lr
	.align 2, 0
.pool

thumb_func_global vBlankInterrupt
vBlankInterrupt: @ 0x0800D9BC
	push {r4, lr}
	ldr r4, =0x03002080
	ldrb r0, [r4, #8]
	lsl r0, r0, #0x1c
	cmp r0, #0
	bge _0800D9CC
	bl sub_08035E80
_0800D9CC:
	ldr r3, =0x04000208
	mov r0, #0
	strh r0, [r3]
	ldr r2, =0x03007FF8
	ldrh r0, [r2]
	mov r1, #1
	orr r0, r1
	strh r0, [r2]
	strh r1, [r3]
	ldr r0, [r4, #4]
	add r0, #1
	str r0, [r4, #4]
	ldr r2, =0x040000BA
	ldrh r1, [r2]
	ldr r0, =0x00007FFF
	and r0, r1
	strh r0, [r2]
	ldr r1, =0x040000B0
	ldr r0, [r1, #8]
	mov r2, #0x80
	lsl r2, r2, #0x18
	cmp r0, #0
	bge _0800DA02
_0800D9FA:
	ldr r0, [r1, #8]
	and r0, r2
	cmp r0, #0
	bne _0800D9FA
_0800DA02:
	add r2, r4, #0
	ldrb r1, [r2, #8]
	lsl r0, r1, #0x1f
	cmp r0, #0
	beq _0800DA18
	mov r0, #2
	neg r0, r0
	and r0, r1
	strb r0, [r2, #8]
	bl run_callbacks
_0800DA18:
	bl sub_0801B970
	ldrb r0, [r4, #8]
	lsl r0, r0, #0x1c
	cmp r0, #0
	bge _0800DA2C
	bl m4aSoundMain
	bl sub_08010774
_0800DA2C:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global nullInterrupt
nullInterrupt: @ 0x0800DA4C
	bx lr
	.align 2, 0
