.include "asm/macros.inc"
	

@ Commands for entity scripting language


thumb_func_global objc_0
objc_0: @ 0x08022AB8
	push {r4, r5, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x3c]
	bl clearObjForces
	ldr r0, [r4, #0x3c]
	mov r5, #0xe0
	lsl r5, r5, #3
	add r1, r5, #0
	bl setObjMaxVelocityX
	ldr r0, [r4, #0x3c]
	add r1, r5, #0
	bl setObjMaxVelocityY
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_1
objc_1: @ 0x08022AE8
	push {r4, r5, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldrb r5, [r0, #4]
	ldrb r1, [r0, #5]
	add r2, r1, #0
	cmp r5, #0
	bne _08022B04
	ldr r0, [r4, #0x3c]
	lsl r1, r1, #8
	neg r1, r1
	bl setObjVelocity_X
	b _08022B10
_08022B04:
	cmp r5, #1
	bne _08022B10
	ldr r0, [r4, #0x3c]
	lsl r1, r2, #8
	bl setObjVelocity_X
_08022B10:
	add r3, r4, #0
	add r3, #0x67
	mov r1, #0xf
	and r1, r5
	ldrb r2, [r3]
	mov r0, #0x10
	neg r0, r0
	and r0, r2
	orr r0, r1
	strb r0, [r3]
	add r0, r4, #0
	mov r1, #1
	bl setObjAnimation_
	add r0, r4, #0
	add r1, r5, #0
	bl setObjDir_animFrame
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1

thumb_func_global objc_2_setDir
objc_2_setDir: @ 0x08022B44
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #4]
	add r3, r4, #0
	add r3, #0x67
	mov r0, #0xf
	and r0, r1
	ldrb r2, [r3]
	mov r1, #0x10
	neg r1, r1
	and r1, r2
	orr r1, r0
	strb r1, [r3]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	add r0, r4, #0
	bl setObjDir_animFrame
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_3_toggleDir
objc_3_toggleDir: @ 0x08022B7C
	push {r4, r5, lr}
	add r4, r0, #0
	mov r5, #0
	add r3, r4, #0
	add r3, #0x67
	ldrb r2, [r3]
	mov r1, #0xf
	add r0, r1, #0
	and r0, r2
	cmp r0, #0
	bne _08022B94
	mov r5, #1
_08022B94:
	add r0, r1, #0
	and r0, r5
	mov r1, #0x10
	neg r1, r1
	and r1, r2
	orr r1, r0
	strb r1, [r3]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	add r0, r4, #0
	bl setObjDir_animFrame
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1

thumb_func_global objc_4_waitFrames
objc_4_waitFrames: @ 0x08022BBC
	push {r4, r5, lr}
	mov ip, r0
	ldr r0, [r0, #0x20]
	ldrh r3, [r0, #4]
	mov r5, ip
	add r5, #0x62
	ldrb r1, [r5]
	lsl r1, r1, #1
	mov r4, ip
	add r4, #0x4c
	add r1, r4, r1
	ldrh r0, [r1]
	add r2, r0, #1
	strh r2, [r1]
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, r3
	blt _08022BF6
	ldrb r0, [r5]
	lsl r0, r0, #1
	add r0, r4, r0
	mov r1, #0
	strh r1, [r0]
	mov r1, ip
	ldr r0, [r1, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	mov r1, ip
	str r0, [r1, #0x20]
_08022BF6:
	mov r0, #1
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_5_for
objc_5_for: @ 0x08022C00
	push {r4, r5, lr}
	mov ip, r0
	ldr r0, [r0, #0x20]
	ldrh r3, [r0, #8]
	mov r5, ip
	add r5, #0x62
	ldrb r1, [r5]
	lsl r1, r1, #1
	mov r4, ip
	add r4, #0x4c
	add r1, r4, r1
	ldrh r0, [r1]
	add r2, r0, #1
	strh r2, [r1]
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, r3
	blt _08022C36
	ldrb r0, [r5]
	lsl r0, r0, #1
	add r0, r4, r0
	mov r1, #0
	strh r1, [r0]
	mov r1, ip
	ldr r0, [r1, #0x20]
	ldr r0, [r0, #4]
	b _08022C40
_08022C36:
	mov r1, ip
	ldr r0, [r1, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	mov r1, ip
_08022C40:
	str r0, [r1, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_6_forCall
objc_6_forCall: @ 0x08022C4C
	push {r4, r5, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldrh r3, [r0, #8]
	mov r0, #0x62
	add r0, r0, r4
	mov ip, r0
	ldrb r1, [r0]
	lsl r1, r1, #1
	add r5, r4, #0
	add r5, #0x4c
	add r1, r5, r1
	ldrh r0, [r1]
	add r2, r0, #1
	strh r2, [r1]
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, r3
	blt _08022C88
	mov r1, ip
	ldrb r0, [r1]
	lsl r0, r0, #1
	add r0, r5, r0
	mov r1, #0
	strh r1, [r0]
	add r0, r4, #0
	mov r1, #4
	bl script_call
	b _08022C90
_08022C88:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
_08022C90:
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1

thumb_func_global objc_7_setWord
objc_7_setWord: @ 0x08022C98
	add r2, r0, #0
	add r0, #0x62
	ldrb r0, [r0]
	lsl r0, r0, #1
	add r1, r2, #0
	add r1, #0x4c
	add r1, r1, r0
	ldr r0, [r2, #0x20]
	ldrh r0, [r0, #4]
	strh r0, [r1]
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_8_clearWord
objc_8_clearWord: @ 0x08022CB8
	add r2, r0, #0
	add r0, #0x62
	ldrb r0, [r0]
	lsl r0, r0, #1
	add r1, r2, #0
	add r1, #0x4c
	add r1, r1, r0
	mov r0, #0
	strh r0, [r1]
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_9_loadRandom
objc_9_loadRandom: @ 0x08022CD8
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldrh r0, [r0, #4]
	bl random
	add r1, r4, #0
	add r1, #0x62
	ldrb r2, [r1]
	lsl r2, r2, #1
	sub r1, #0x16
	add r1, r1, r2
	strh r0, [r1]
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_a
objc_a: @ 0x08022D04
	push {r4, r5, lr}
	mov ip, r0
	ldr r0, [r0, #0x20]
	ldrh r3, [r0, #4]
	mov r5, ip
	add r5, #0x62
	ldrb r0, [r5]
	lsl r0, r0, #1
	mov r4, ip
	add r4, #0x4c
	add r0, r4, r0
	ldrh r1, [r0]
	add r2, r1, #1
	strh r2, [r0]
	lsl r1, r1, #0x10
	lsl r3, r3, #0x10
	cmp r1, r3
	bne _08022D3E
	ldrb r0, [r5]
	lsl r0, r0, #1
	add r0, r4, r0
	mov r1, #0
	strh r1, [r0]
	mov r1, ip
	ldr r0, [r1, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	mov r1, ip
	str r0, [r1, #0x20]
_08022D3E:
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_b_forNot
objc_b_forNot: @ 0x08022D48
	push {r4, r5, lr}
	mov ip, r0
	ldr r0, [r0, #0x20]
	ldrh r3, [r0, #8]
	mov r5, ip
	add r5, #0x62
	ldrb r0, [r5]
	lsl r0, r0, #1
	mov r4, ip
	add r4, #0x4c
	add r0, r4, r0
	ldrh r1, [r0]
	add r2, r1, #1
	strh r2, [r0]
	lsl r1, r1, #0x10
	lsl r3, r3, #0x10
	cmp r1, r3
	bne _08022D7E
	ldrb r0, [r5]
	lsl r0, r0, #1
	add r0, r4, r0
	mov r1, #0
	strh r1, [r0]
	mov r1, ip
	ldr r0, [r1, #0x20]
	ldr r0, [r0, #4]
	b _08022D88
_08022D7E:
	mov r1, ip
	ldr r0, [r1, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	mov r1, ip
_08022D88:
	str r0, [r1, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_c_forCallNot
objc_c_forCallNot: @ 0x08022D94
	push {r4, r5, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldrh r3, [r0, #8]
	mov r0, #0x62
	add r0, r0, r4
	mov ip, r0
	ldrb r0, [r0]
	lsl r0, r0, #1
	add r5, r4, #0
	add r5, #0x4c
	add r0, r5, r0
	ldrh r1, [r0]
	add r2, r1, #1
	strh r2, [r0]
	lsl r1, r1, #0x10
	lsl r3, r3, #0x10
	cmp r1, r3
	bne _08022DD0
	mov r1, ip
	ldrb r0, [r1]
	lsl r0, r0, #1
	add r0, r5, r0
	mov r1, #0
	strh r1, [r0]
	add r0, r4, #0
	mov r1, #4
	bl script_call
	b _08022DD8
_08022DD0:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
_08022DD8:
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1

thumb_func_global objc_d
objc_d: @ 0x08022DE0
	mov ip, r0
	ldr r0, [r0, #0x20]
	ldrh r3, [r0, #4]
	mov r0, ip
	add r0, #0x62
	ldrb r0, [r0]
	lsl r0, r0, #1
	mov r1, ip
	add r1, #0x4c
	add r1, r1, r0
	ldrh r0, [r1]
	sub r2, r0, #1
	strh r2, [r1]
	lsl r0, r0, #0x10
	lsl r3, r3, #0x10
	cmp r0, r3
	bne _08022E0E
	mov r1, ip
	ldr r0, [r1, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	mov r1, ip
	str r0, [r1, #0x20]
_08022E0E:
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_e
objc_e: @ 0x08022E14
	mov ip, r0
	ldr r0, [r0, #0x20]
	ldrh r3, [r0, #8]
	mov r0, ip
	add r0, #0x62
	ldrb r0, [r0]
	lsl r0, r0, #1
	mov r1, ip
	add r1, #0x4c
	add r1, r1, r0
	ldrh r0, [r1]
	sub r2, r0, #1
	strh r2, [r1]
	lsl r0, r0, #0x10
	lsl r3, r3, #0x10
	cmp r0, r3
	bne _08022E3E
	mov r1, ip
	ldr r0, [r1, #0x20]
	ldr r0, [r0, #4]
	b _08022E48
_08022E3E:
	mov r1, ip
	ldr r0, [r1, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	mov r1, ip
_08022E48:
	str r0, [r1, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_f
objc_f: @ 0x08022E50
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldrh r3, [r0, #8]
	add r0, r4, #0
	add r0, #0x62
	ldrb r0, [r0]
	lsl r0, r0, #1
	add r1, r4, #0
	add r1, #0x4c
	add r1, r1, r0
	ldrh r0, [r1]
	sub r2, r0, #1
	strh r2, [r1]
	lsl r0, r0, #0x10
	lsl r3, r3, #0x10
	cmp r0, r3
	bne _08022E7E
	add r0, r4, #0
	mov r1, #4
	bl script_call
	b _08022E86
_08022E7E:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
_08022E86:
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_10
objc_10: @ 0x08022E90
	push {r4, lr}
	add r3, r0, #0
	ldr r2, [r3, #0x20]
	add r0, #0x62
	ldrb r0, [r0]
	lsl r0, r0, #1
	add r1, r3, #0
	add r1, #0x4c
	add r1, r1, r0
	mov r4, #4
	ldrsh r0, [r2, r4]
	ldrh r2, [r1]
	add r0, r0, r2
	strh r0, [r1]
	ldr r0, [r3, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r3, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_11
objc_11: @ 0x08022EBC
	add r2, r0, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #1
	bne _08022ED8
	ldr r0, [r2, #0x3c]
	add r0, #0x4f
	ldrb r1, [r0]
	mov r0, #8
	and r0, r1
	cmp r0, #0
	beq _08022EF4
_08022ED8:
	add r0, r2, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #0
	bne _08022EF8
	ldr r0, [r2, #0x3c]
	add r0, #0x4f
	ldrb r1, [r0]
	mov r0, #4
	and r0, r1
	cmp r0, #0
	bne _08022EF8
_08022EF4:
	mov r0, #1
	b _08022F02
_08022EF8:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r2, #0x20]
	mov r0, #0
_08022F02:
	bx lr

thumb_func_global objc_12
objc_12: @ 0x08022F04
	add r2, r0, #0
	add r0, #0x67
	ldrb r0, [r0]
	mov r3, #0xf
	and r3, r0
	cmp r3, #1
	bne _08022F20
	ldr r0, [r2, #0x3c]
	add r0, #0x4f
	ldrb r1, [r0]
	mov r0, #8
	and r0, r1
	cmp r0, #0
	beq _08022F32
_08022F20:
	cmp r3, #0
	bne _08022F3A
	ldr r0, [r2, #0x3c]
	add r0, #0x4f
	ldrb r1, [r0]
	mov r0, #4
	and r0, r1
	cmp r0, #0
	bne _08022F3A
_08022F32:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	b _08022F3E
_08022F3A:
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
_08022F3E:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_13
objc_13: @ 0x08022F44
	push {lr}
	add r2, r0, #0
	add r0, #0x67
	ldrb r0, [r0]
	mov r3, #0xf
	and r3, r0
	cmp r3, #1
	bne _08022F62
	ldr r0, [r2, #0x3c]
	add r0, #0x4f
	ldrb r1, [r0]
	mov r0, #8
	and r0, r1
	cmp r0, #0
	beq _08022F74
_08022F62:
	cmp r3, #0
	bne _08022F7E
	ldr r0, [r2, #0x3c]
	add r0, #0x4f
	ldrb r1, [r0]
	mov r0, #4
	and r0, r1
	cmp r0, #0
	bne _08022F7E
_08022F74:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r2, #0x20]
	b _08022F86
_08022F7E:
	add r0, r2, #0
	mov r1, #4
	bl script_call
_08022F86:
	mov r0, #0
	pop {r1}
	bx r1

thumb_func_global objc_14_reset
objc_14_reset: @ 0x08022F8C
	ldr r1, [r0, #8]
	ldr r1, [r1]
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_15_setVelYOnGround
objc_15_setVelYOnGround: @ 0x08022F98
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldrb r3, [r0, #4]
	ldr r2, [r4, #0x3c]
	add r0, r2, #0
	add r0, #0x4f
	ldrb r1, [r0]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _08022FB8
	lsl r1, r3, #8
	add r0, r2, #0
	bl setObjVelocity_Y
_08022FB8:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_16_waitForGround
objc_16_waitForGround: @ 0x08022FC8
	add r2, r0, #0
	ldr r0, [r2, #0x3c]
	add r0, #0x4f
	ldrb r1, [r0]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _08022FE4
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r2, #0x20]
	mov r0, #0
	b _08022FE6
_08022FE4:
	mov r0, #1
_08022FE6:
	bx lr

thumb_func_global objc_17_gotoOnGround
objc_17_gotoOnGround: @ 0x08022FE8
	add r2, r0, #0
	ldr r0, [r2, #0x3c]
	add r0, #0x4f
	ldrb r1, [r0]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _08022FFE
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _08023004
_08022FFE:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08023004:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_18_callOnGround
objc_18_callOnGround: @ 0x0802300C
	push {lr}
	add r2, r0, #0
	ldr r0, [r2, #0x3c]
	add r0, #0x4f
	ldrb r1, [r0]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _08023028
	add r0, r2, #0
	mov r1, #4
	bl script_call
	b _08023030
_08023028:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r2, #0x20]
_08023030:
	mov r0, #0
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_19_callScript
objc_19_callScript: @ 0x08023038
	push {lr}
	mov r1, #4
	bl script_call
	mov r0, #0
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_1a_ret
objc_1a_ret: @ 0x08023048
	add r2, r0, #0
	add r1, r2, #0
	add r1, #0x62
	ldrb r0, [r1]
	cmp r0, #0
	beq _08023066
	sub r0, #1
	strb r0, [r1]
	ldrb r1, [r1]
	lsl r1, r1, #2
	add r0, r2, #0
	add r0, #0x24
	add r0, r0, r1
	ldr r0, [r0]
	b _0802306C
_08023066:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_0802306C:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_1b_goto
objc_1b_goto: @ 0x08023074
	ldr r1, [r0, #0x20]
	ldr r1, [r1, #4]
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_waitFrame
objc_waitFrame: @ 0x08023080
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #1
	bx lr

thumb_func_global objc_destroyObj
objc_destroyObj: @ 0x0802308C
	mov r0, #2
	bx lr

thumb_func_global objc_1e_kill
objc_1e_kill: @ 0x08023090
	push {r4, lr}
	add r4, r0, #0
	add r1, r4, #0
	add r1, #0x5e
	ldrh r0, [r1]
	cmp r0, #0
	beq _080230B0
	ldr r2, [r4, #0x3c]
	ldr r1, [r2, #4]
	lsl r1, r1, #8
	lsr r1, r1, #0x10
	ldr r2, [r2, #8]
	lsl r2, r2, #8
	lsr r2, r2, #0x10
	bl createRunTimeMapObj
_080230B0:
	add r0, r4, #0
	bl getChargeToGive
	bl ModifySpecialCharge
	add r0, r4, #0
	add r0, #0x60
	ldrh r0, [r0]
	bl setEntDeletionFlag
	add r0, r4, #0
	bl sub_0802025C
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_1f_call
objc_1f_call: @ 0x080230DC
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldr r1, [r0, #4]
	add r0, r4, #0
	bl call_r1
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_20_callWait
objc_20_callWait: @ 0x080230FC
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldr r1, [r0, #4]
	add r0, r4, #0
	bl call_r1
	cmp r0, #0
	beq _08023116
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
_08023116:
	mov r0, #1
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_21_call3
objc_21_call3: @ 0x08023120
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldr r1, [r0, #4]
	add r0, r4, #0
	bl call_r1
	cmp r0, #0
	beq _08023138
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #8]
	b _0802313E
_08023138:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_0802313E:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_22_call4
objc_22_call4: @ 0x08023148
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldr r1, [r0, #4]
	add r0, r4, #0
	bl call_r1
	cmp r0, #0
	beq _08023164
	add r0, r4, #0
	mov r1, #8
	bl script_call
	b _0802316C
_08023164:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
_0802316C:
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_23_setSomeCode
objc_23_setSomeCode: @ 0x08023174
	add r2, r0, #0
	ldr r0, [r2, #0x20]
	ldr r1, [r0, #4]
	ldr r0, [r2, #0x10]
	cmp r0, #0
	bne _08023182
	str r1, [r2, #0x10]
_08023182:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_unused_setSomeCode
objc_unused_setSomeCode: @ 0x08023190
	ldr r1, [r0, #0x20]
	ldr r2, [r1, #4]
	str r2, [r0, #0x10]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_24_rmObjSomeCode
objc_24_rmObjSomeCode: @ 0x080231A0
	mov r1, #0
	str r1, [r0, #0x10]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_25
objc_25: @ 0x080231B0
	push {r4, r5, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldr r3, [r0, #4]
	ldr r5, [r0, #8]
	cmp r3, #0
	beq _080231CC
	ldr r0, [r4, #0x3c]
	ldr r0, [r0, #0x18]
	cmp r0, #0
	bge _080231C8
	neg r0, r0
_080231C8:
	cmp r0, r5
	blt _080231D6
_080231CC:
	ldr r0, [r4, #0x3c]
	mov r1, #0
	bl setObjAccelerationX
	b _080231F0
_080231D6:
	add r2, r4, #0
	add r2, #0x44
	ldrb r0, [r2]
	mov r1, #1
	orr r0, r1
	mov r1, #5
	neg r1, r1
	and r0, r1
	strb r0, [r2]
	ldr r0, [r4, #0x3c]
	add r1, r3, #0
	bl setObjAccelerationX
_080231F0:
	ldr r0, [r4, #0x3c]
	add r1, r5, #0
	bl setObjMaxVelocityX
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1

thumb_func_global objc_26
objc_26: @ 0x08023208
	push {r4, r5, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldr r1, [r0, #4]
	ldr r5, [r0, #8]
	cmp r1, #0
	beq _08023224
	ldr r0, [r4, #0x3c]
	ldr r0, [r0, #0x18]
	cmp r0, #0
	bge _08023220
	neg r0, r0
_08023220:
	cmp r0, r5
	blt _0802322E
_08023224:
	ldr r0, [r4, #0x3c]
	mov r1, #0
	bl setObjAccelerationY
	b _0802324A
_0802322E:
	add r2, r4, #0
	add r2, #0x44
	ldrb r0, [r2]
	mov r1, #2
	orr r0, r1
	mov r1, #9
	neg r1, r1
	and r0, r1
	strb r0, [r2]
	ldr r0, [r4, #0x3c]
	ldr r1, [r4, #0x20]
	ldr r1, [r1, #4]
	bl setObjAccelerationY
_0802324A:
	ldr r0, [r4, #0x3c]
	add r1, r5, #0
	bl setObjMaxVelocityY
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_27
objc_27: @ 0x08023264
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldr r3, [r0, #4]
	cmp r3, #0
	beq _08023278
	ldr r0, [r4, #0x3c]
	ldr r0, [r0, #0x18]
	cmp r0, #0
	bne _08023282
_08023278:
	ldr r0, [r4, #0x3c]
	mov r1, #0
	bl setObjAccelerationX
	b _080232AE
_08023282:
	add r2, r4, #0
	add r2, #0x44
	ldrb r0, [r2]
	mov r1, #4
	orr r0, r1
	mov r1, #2
	neg r1, r1
	and r0, r1
	strb r0, [r2]
	ldr r0, [r4, #0x3c]
	ldr r1, [r0, #0x18]
	cmp r1, #0
	ble _080232A4
	neg r1, r3
	bl setObjAccelerationX
	b _080232AE
_080232A4:
	cmp r1, #0
	bge _080232AE
	add r1, r3, #0
	bl setObjAccelerationX
_080232AE:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_28
objc_28: @ 0x080232C0
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldr r3, [r0, #4]
	cmp r3, #0
	beq _080232D4
	ldr r0, [r4, #0x3c]
	ldr r0, [r0, #0x1c]
	cmp r0, #0
	bne _080232DE
_080232D4:
	ldr r0, [r4, #0x3c]
	mov r1, #0
	bl setObjAccelerationY
	b _0802330A
_080232DE:
	add r2, r4, #0
	add r2, #0x44
	ldrb r0, [r2]
	mov r1, #8
	orr r0, r1
	mov r1, #3
	neg r1, r1
	and r0, r1
	strb r0, [r2]
	ldr r0, [r4, #0x3c]
	ldr r1, [r0, #0x1c]
	cmp r1, #0
	ble _08023300
	neg r1, r3
	bl setObjAccelerationY
	b _0802330A
_08023300:
	cmp r1, #0
	bge _0802330A
	add r1, r3, #0
	bl setObjAccelerationY
_0802330A:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_29_setXVelocity
objc_29_setXVelocity: @ 0x0802331C
	push {r4, lr}
	add r4, r0, #0
	add r2, r4, #0
	add r2, #0x44
	ldrb r1, [r2]
	mov r0, #5
	neg r0, r0
	and r0, r1
	mov r1, #2
	neg r1, r1
	and r0, r1
	strb r0, [r2]
	ldr r0, [r4, #0x3c]
	ldr r1, [r4, #0x20]
	ldr r1, [r1, #4]
	bl setObjVelocity_X
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_2a_setVelY
objc_2a_setVelY: @ 0x08023350
	push {r4, lr}
	add r4, r0, #0
	add r2, r4, #0
	add r2, #0x44
	ldrb r1, [r2]
	mov r0, #9
	neg r0, r0
	and r0, r1
	mov r1, #3
	neg r1, r1
	and r0, r1
	strb r0, [r2]
	ldr r0, [r4, #0x3c]
	ldr r1, [r4, #0x20]
	ldr r1, [r1, #4]
	bl setObjVelocity_Y
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_2b_branchNearPlayerX
objc_2b_branchNearPlayerX: @ 0x08023384
	push {r4, lr}
	add r4, r0, #0
	bl objHorizontalDistToPlayer
	ldr r2, [r4, #0x20]
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	mov r3, #8
	ldrsh r1, [r2, r3]
	cmp r0, r1
	bgt _0802339E
	ldr r0, [r2, #4]
	b _080233A2
_0802339E:
	ldrb r0, [r2, #2]
	add r0, r2, r0
_080233A2:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_2c_branchNearPlayerY
objc_2c_branchNearPlayerY: @ 0x080233AC
	push {r4, lr}
	add r4, r0, #0
	bl objVerticalDistToPlayer
	ldr r2, [r4, #0x20]
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	mov r3, #8
	ldrsh r1, [r2, r3]
	cmp r0, r1
	bgt _080233C6
	ldr r0, [r2, #4]
	b _080233CA
_080233C6:
	ldrb r0, [r2, #2]
	add r0, r2, r0
_080233CA:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_2d_branchAwayPlayerX
objc_2d_branchAwayPlayerX: @ 0x080233D4
	push {r4, lr}
	add r4, r0, #0
	bl objHorizontalDistToPlayer
	ldr r2, [r4, #0x20]
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	mov r3, #8
	ldrsh r1, [r2, r3]
	cmp r0, r1
	blt _080233EE
	ldr r0, [r2, #4]
	b _080233F2
_080233EE:
	ldrb r0, [r2, #2]
	add r0, r2, r0
_080233F2:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_2e_branchAwayPlayerY
objc_2e_branchAwayPlayerY: @ 0x080233FC
	push {r4, lr}
	add r4, r0, #0
	bl objVerticalDistToPlayer
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	ldr r1, [r4, #0x20]
	ldrh r2, [r1, #8]
	cmp r0, r2
	blt _08023414
	ldr r0, [r1, #4]
	b _08023418
_08023414:
	ldrb r0, [r1, #2]
	add r0, r1, r0
_08023418:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_2f_branchIfNotX
objc_2f_branchIfNotX: @ 0x08023424
	push {r4, lr}
	add r2, r0, #0
	ldr r0, [r2, #0x20]
	ldrh r1, [r0, #8]
	add r4, r1, #0
	ldr r0, [r2, #0x3c]
	ldr r0, [r0, #4]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	add r3, r0, #0
	cmp r0, r1
	bhi _08023440
	cmp r1, r0
	bls _08023448
_08023440:
	cmp r3, r4
	bhi _0802344E
	cmp r4, r3
	bhi _0802344E
_08023448:
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _08023454
_0802344E:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08023454:
	str r0, [r2, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_31_branchIfNotY
objc_31_branchIfNotY: @ 0x08023460
	push {r4, lr}
	add r2, r0, #0
	ldr r0, [r2, #0x20]
	ldrh r1, [r0, #8]
	add r4, r1, #0
	ldr r0, [r2, #0x3c]
	ldr r0, [r0, #8]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	add r3, r0, #0
	cmp r0, r1
	bhi _0802347C
	cmp r1, r0
	bls _08023484
_0802347C:
	cmp r3, r4
	bhi _0802348A
	cmp r4, r3
	bhi _0802348A
_08023484:
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _08023490
_0802348A:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08023490:
	str r0, [r2, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_30_branchIfX
objc_30_branchIfX: @ 0x0802349C
	push {r4, lr}
	add r2, r0, #0
	ldr r0, [r2, #0x20]
	ldrh r1, [r0, #8]
	add r4, r1, #0
	ldr r0, [r2, #0x3c]
	ldr r0, [r0, #4]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	add r3, r0, #0
	cmp r0, r1
	bhi _080234B8
	cmp r1, r0
	bls _080234C0
_080234B8:
	cmp r3, r4
	bhi _080234C8
	cmp r4, r3
	bhi _080234C8
_080234C0:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	b _080234CC
_080234C8:
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
_080234CC:
	str r0, [r2, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_32_branchIfY
objc_32_branchIfY: @ 0x080234D8
	push {r4, lr}
	add r2, r0, #0
	ldr r0, [r2, #0x20]
	ldrh r1, [r0, #8]
	add r4, r1, #0
	ldr r0, [r2, #0x3c]
	ldr r0, [r0, #8]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	add r3, r0, #0
	cmp r0, r1
	bhi _080234F4
	cmp r1, r0
	bls _080234FC
_080234F4:
	cmp r3, r4
	bhi _08023504
	cmp r4, r3
	bhi _08023504
_080234FC:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	b _08023508
_08023504:
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
_08023508:
	str r0, [r2, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_33_branchXBetween
objc_33_branchXBetween: @ 0x08023514
	push {r4, r5, lr}
	add r3, r0, #0
	ldr r4, [r3, #0x20]
	ldrh r5, [r4, #6]
	ldr r0, [r3, #0x3c]
	ldr r0, [r0, #4]
	lsl r0, r0, #8
	mov r2, #4
	ldrsh r1, [r4, r2]
	asr r2, r0, #0x10
	cmp r1, r2
	bgt _08023538
	lsl r0, r5, #0x10
	asr r0, r0, #0x10
	cmp r2, r0
	bgt _08023538
	ldr r0, [r4, #8]
	b _0802353E
_08023538:
	ldr r0, [r3, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_0802353E:
	str r0, [r3, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1

thumb_func_global objc_34_branchYBetween
objc_34_branchYBetween: @ 0x08023548
	push {r4, r5, lr}
	add r3, r0, #0
	ldr r4, [r3, #0x20]
	ldrh r5, [r4, #6]
	ldr r0, [r3, #0x3c]
	ldr r0, [r0, #8]
	lsl r0, r0, #8
	mov r2, #4
	ldrsh r1, [r4, r2]
	asr r2, r0, #0x10
	cmp r1, r2
	bgt _0802356C
	lsl r0, r5, #0x10
	asr r0, r0, #0x10
	cmp r2, r0
	bgt _0802356C
	ldr r0, [r4, #8]
	b _08023572
_0802356C:
	ldr r0, [r3, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08023572:
	str r0, [r3, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1

thumb_func_global objc_35
objc_35: @ 0x0802357C
	push {r4, lr}
	add r4, r0, #0
	bl getPlayerObj
	ldr r2, [r4, #0x20]
	ldrh r1, [r2, #4]
	ldrh r3, [r2, #6]
	ldr r0, [r0, #4]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	cmp r1, r0
	bhi _0802359C
	cmp r0, r3
	bhi _0802359C
	ldr r0, [r2, #8]
	b _080235A2
_0802359C:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_080235A2:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_36
objc_36: @ 0x080235AC
	push {r4, lr}
	add r4, r0, #0
	bl getPlayerObj
	ldr r2, [r4, #0x20]
	ldrh r1, [r2, #4]
	ldrh r3, [r2, #6]
	ldr r0, [r0, #8]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	cmp r1, r0
	bhi _080235CC
	cmp r0, r3
	bhi _080235CC
	ldr r0, [r2, #8]
	b _080235D2
_080235CC:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_080235D2:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_37
objc_37: @ 0x080235DC
	add r2, r0, #0
	ldr r1, [r2, #0x20]
	add r0, #0x67
	ldrb r0, [r0]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1c
	ldrb r3, [r1, #8]
	cmp r0, r3
	bne _080235F2
	ldr r0, [r1, #4]
	b _080235F6
_080235F2:
	ldrb r0, [r1, #2]
	add r0, r1, r0
_080235F6:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_38
objc_38: @ 0x080235FC
	push {r4, lr}
	add r4, r0, #0
	bl sub_0801FCD0
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	add r1, r4, #0
	add r1, #0x67
	ldrb r1, [r1]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	cmp r0, r1
	bne _0802361C
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #4]
	b _08023622
_0802361C:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08023622:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_39
objc_39: @ 0x0802362C
	push {r4, lr}
	add r4, r0, #0
	bl sub_0801FCD0
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	add r1, r4, #0
	add r1, #0x67
	ldrb r1, [r1]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	cmp r0, r1
	beq _0802364C
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #4]
	b _08023652
_0802364C:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08023652:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_3a_branchNotFloor
objc_3a_branchNotFloor: @ 0x0802365C
	add r2, r0, #0
	ldr r0, [r2, #0x3c]
	add r0, #0x4f
	ldrb r1, [r0]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _08023674
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	b _08023678
_08023674:
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
_08023678:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_3b
objc_3b: @ 0x08023680
	push {r4, lr}
	add r4, r0, #0
	bl sub_0801FCD0
	add r1, r0, #0
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	add r0, r4, #0
	bl setObjDir_animFrame
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_3c
objc_3c: @ 0x080236A4
	push {r4, lr}
	add r4, r0, #0
	add r0, #0x67
	ldrb r0, [r0]
	lsl r0, r0, #0x1c
	lsr r1, r0, #0x1c
	ldr r0, [r4, #0x3c]
	ldr r0, [r0, #4]
	lsl r0, r0, #8
	asr r0, r0, #0x10
	ldrh r2, [r4, #0x14]
	cmp r0, r2
	ble _080236C2
	mov r1, #0
	b _080236C8
_080236C2:
	cmp r0, r2
	bge _080236C8
	mov r1, #1
_080236C8:
	add r0, r4, #0
	bl setObjDir_animFrame
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_3d
objc_3d: @ 0x080236E0
	push {r4, r5, r6, lr}
	add r4, r0, #0
	add r2, r4, #0
	add r2, #0x44
	ldrb r1, [r2]
	mov r0, #5
	neg r0, r0
	and r0, r1
	mov r1, #2
	neg r1, r1
	and r0, r1
	strb r0, [r2]
	add r5, r4, #0
	add r5, #0x67
	ldrb r1, [r5]
	mov r6, #0xf
	add r0, r6, #0
	and r0, r1
	cmp r0, #0
	bne _08023714
	ldr r0, [r4, #0x3c]
	ldr r1, [r4, #0x20]
	ldr r1, [r1, #4]
	neg r1, r1
	bl setObjVelocity_X
_08023714:
	ldrb r1, [r5]
	add r0, r6, #0
	and r0, r1
	cmp r0, #1
	bne _08023728
	ldr r0, [r4, #0x3c]
	ldr r1, [r4, #0x20]
	ldr r1, [r1, #4]
	bl setObjVelocity_X
_08023728:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4, r5, r6}
	pop {r1}
	bx r1

thumb_func_global objc_3e
objc_3e: @ 0x08023738
	push {r4, r5, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldr r3, [r0, #4]
	ldr r5, [r0, #8]
	add r0, r4, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #0
	bne _08023752
	neg r3, r3
_08023752:
	ldr r0, [r4, #0x3c]
	cmp r3, #0
	beq _08023764
	ldr r1, [r0, #0x18]
	cmp r1, #0
	bge _08023760
	neg r1, r1
_08023760:
	cmp r1, r5
	blt _0802376C
_08023764:
	mov r1, #0
	bl setObjAccelerationX
	b _0802378E
_0802376C:
	add r2, r4, #0
	add r2, #0x44
	ldrb r0, [r2]
	mov r1, #1
	orr r0, r1
	mov r1, #5
	neg r1, r1
	and r0, r1
	strb r0, [r2]
	ldr r0, [r4, #0x3c]
	add r1, r3, #0
	bl setObjAccelerationX
	ldr r0, [r4, #0x3c]
	add r1, r5, #0
	bl setObjMaxVelocityX
_0802378E:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_3f_setSprite23b4
objc_3f_setSprite23b4: @ 0x080237A0
	ldr r1, [r0, #0x3c]
	ldr r2, [r1]
	add r2, #0x23
	ldrb r1, [r2]
	mov r3, #0x10
	orr r1, r3
	strb r1, [r2]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_40_clearSprite23b4
objc_40_clearSprite23b4: @ 0x080237BC
	ldr r1, [r0, #0x3c]
	ldr r2, [r1]
	add r2, #0x23
	ldrb r3, [r2]
	mov r1, #0x11
	neg r1, r1
	and r1, r3
	strb r1, [r2]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_41_setAnimation
objc_41_setAnimation: @ 0x080237D8
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldrh r1, [r0, #4]
	add r0, r4, #0
	bl setObjAnimation_
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_42_forceAnimation
objc_42_forceAnimation: @ 0x080237F8
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x3c]
	ldr r1, [r0]
	mov r0, #0
	str r0, [r1, #4]
	ldr r0, [r4, #0x20]
	ldrh r1, [r0, #4]
	add r0, r4, #0
	bl setObjAnimation_
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_43_setNextAnimation
objc_43_setNextAnimation: @ 0x08023820
	push {r4, lr}
	add r4, r0, #0
	add r0, #0x65
	ldrb r1, [r0]
	add r1, #1
	add r0, r4, #0
	bl setObjAnimation_
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_44_setAnimation_
objc_44_setAnimation_: @ 0x08023840
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldrh r1, [r0, #4]
	add r0, r4, #0
	bl setObjAnimation
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_45_branchOnScreen
objc_45_branchOnScreen: @ 0x08023860
	push {r4, r5, lr}
	add r5, r0, #0
	ldr r3, [r5, #0x3c]
	ldrh r2, [r3, #0x38]
	ldrh r0, [r3, #0x14]
	add r2, r2, r0
	ldrh r4, [r3, #0x16]
	ldrh r0, [r3, #0x3a]
	sub r4, r4, r0
	ldrh r1, [r3, #0x3c]
	lsl r2, r2, #0x10
	asr r2, r2, #0x10
	add r1, r2, r1
	ldrh r0, [r3, #0x3e]
	lsl r4, r4, #0x10
	asr r4, r4, #0x10
	add r0, r4, r0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	mov r3, #8
	neg r3, r3
	cmp r1, r3
	blt _080238A8
	cmp r2, #0xf8
	bgt _080238A8
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, r3
	blt _080238A8
	cmp r4, #0xa8
	bgt _080238A8
	ldr r0, [r5, #0x20]
	ldr r0, [r0, #4]
	b _080238AE
_080238A8:
	ldr r0, [r5, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_080238AE:
	str r0, [r5, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1

thumb_func_global objc_46_ifOffScreen
objc_46_ifOffScreen: @ 0x080238B8
	push {r4, r5, lr}
	add r5, r0, #0
	ldr r3, [r5, #0x3c]
	ldrh r2, [r3, #0x38]
	ldrh r0, [r3, #0x14]
	add r2, r2, r0
	ldrh r4, [r3, #0x16]
	ldrh r0, [r3, #0x3a]
	sub r4, r4, r0
	ldrh r1, [r3, #0x3c]
	lsl r2, r2, #0x10
	asr r2, r2, #0x10
	add r1, r2, r1
	ldrh r0, [r3, #0x3e]
	lsl r4, r4, #0x10
	asr r4, r4, #0x10
	add r0, r4, r0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	mov r3, #8
	neg r3, r3
	cmp r1, r3
	blt _080238FA
	cmp r2, #0xf7
	bgt _080238FA
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, r3
	blt _080238FA
	cmp r4, #0xa7
	ble _08023900
_080238FA:
	ldr r0, [r5, #0x20]
	ldr r0, [r0, #4]
	b _08023906
_08023900:
	ldr r0, [r5, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08023906:
	str r0, [r5, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1

thumb_func_global objc_47_createChildObj
objc_47_createChildObj: @ 0x08023910
	push {r4, r5, r6, r7, lr}
	sub sp, #8
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldrh r1, [r0, #0x10]
	ldr r5, [r0, #8]
	ldr r6, [r0, #0xc]
	mov r3, #4
	ldrsh r2, [r0, r3]
	mov r7, #6
	ldrsh r3, [r0, r7]
	str r5, [sp]
	str r6, [sp, #4]
	add r0, r4, #0
	bl createChildEntity
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	add sp, #8
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_48
objc_48: @ 0x08023944
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #8
	add r5, r0, #0
	ldr r0, [r5, #0x20]
	ldrh r1, [r0, #0x10]
	mov ip, r1
	ldrh r2, [r0, #4]
	ldrh r3, [r0, #6]
	ldr r4, [r0, #8]
	ldr r7, [r0, #0xc]
	add r6, r5, #0
	add r6, #0x67
	ldrb r1, [r6]
	mov r0, #0xf
	mov r8, r0
	and r0, r1
	cmp r0, #0
	bne _08023974
	neg r4, r4
	lsl r0, r2, #0x10
	neg r0, r0
	lsr r2, r0, #0x10
_08023974:
	lsl r2, r2, #0x10
	asr r2, r2, #0x10
	lsl r3, r3, #0x10
	asr r3, r3, #0x10
	str r4, [sp]
	str r7, [sp, #4]
	add r0, r5, #0
	mov r1, ip
	bl createChildEntity
	ldrb r1, [r6]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	mov r2, #0x67
	add r2, r2, r0
	mov ip, r2
	mov r2, r8
	and r2, r1
	mov r1, ip
	ldrb r3, [r1]
	mov r1, #0x10
	neg r1, r1
	and r1, r3
	orr r1, r2
	mov r2, ip
	strb r1, [r2]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	bl setObjDir_animFrame
	ldr r0, [r5, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r5, #0x20]
	mov r0, #0
	add sp, #8
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_49
objc_49: @ 0x080239C8
	push {r4, lr}
	sub sp, #8
	add r4, r0, #0
	ldr r3, [r4, #0x20]
	ldrh r1, [r3, #8]
	ldr r2, =dirLutMaybe
	add r0, #0x67
	ldrb r0, [r0]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1c
	add r0, r0, r2
	mov r2, #0
	ldrsb r2, [r0, r2]
	ldrh r0, [r3, #4]
	mul r2, r0, r2
	lsl r2, r2, #0x10
	asr r2, r2, #0x10
	mov r0, #6
	ldrsh r3, [r3, r0]
	mov r0, #0
	str r0, [sp]
	str r0, [sp, #4]
	add r0, r4, #0
	bl createChildEntity
	str r0, [r4, #0x40]
	str r4, [r0, #0x40]
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	add sp, #8
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global objc_4a_killParent
objc_4a_killParent: @ 0x08023A14
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x40]
	cmp r0, #0
	beq _08023A22
	bl destroyObject
_08023A22:
	mov r0, #0
	str r0, [r4, #0x40]
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_4b
objc_4b: @ 0x08023A38
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	mov r2, #4
	ldrsh r1, [r0, r2]
	mov r3, #6
	ldrsh r2, [r0, r3]
	add r0, r4, #0
	bl sub_08020830
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_4c
objc_4c: @ 0x08023A5C
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #8
	add r5, r0, #0
	ldr r0, [r5, #0x20]
	ldrh r1, [r0, #0x10]
	mov ip, r1
	ldrh r2, [r0, #4]
	ldrh r3, [r0, #6]
	ldr r4, [r0, #8]
	ldr r7, [r0, #0xc]
	add r6, r5, #0
	add r6, #0x67
	ldrb r1, [r6]
	mov r0, #0xf
	mov r8, r0
	and r0, r1
	cmp r0, #0
	bne _08023A8C
	neg r4, r4
	lsl r0, r2, #0x10
	neg r0, r0
	lsr r2, r0, #0x10
_08023A8C:
	lsl r2, r2, #0x10
	asr r2, r2, #0x10
	lsl r3, r3, #0x10
	asr r3, r3, #0x10
	str r4, [sp]
	str r7, [sp, #4]
	add r0, r5, #0
	mov r1, ip
	bl createChildEntity
	add r4, r0, #0
	ldrb r0, [r6]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1c
	add r3, r4, #0
	add r3, #0x67
	mov r2, r8
	and r2, r0
	ldrb r0, [r3]
	mov r1, #0x10
	neg r1, r1
	and r1, r0
	orr r1, r2
	strb r1, [r3]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	add r0, r4, #0
	bl setObjDir_animFrame
	str r5, [r4, #0x40]
	ldr r0, [r5, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r5, #0x20]
	mov r0, #0
	add sp, #8
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_4d_setInit4E
objc_4d_setInit4E: @ 0x08023AE0
	ldr r1, [r0, #0x3c]
	ldr r2, [r0, #0x20]
	ldrh r2, [r2, #4]
	add r1, #0x4e
	strb r2, [r1]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_4e_branchRNG
objc_4e_branchRNG: @ 0x08023AF8
	push {r4, r5, lr}
	add r5, r0, #0
	ldr r0, [r5, #0x20]
	ldrb r4, [r0, #8]
	mov r0, #0x64
	bl random
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r4, r0
	bls _08023B14
	ldr r0, [r5, #0x20]
	ldr r0, [r0, #4]
	b _08023B1A
_08023B14:
	ldr r0, [r5, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08023B1A:
	str r0, [r5, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1

thumb_func_global objc_4f_callScriptRNG
objc_4f_callScriptRNG: @ 0x08023B24
	push {r4, r5, lr}
	add r5, r0, #0
	ldr r0, [r5, #0x20]
	ldrb r4, [r0, #8]
	mov r0, #0x64
	bl random
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r4, r0
	bls _08023B44
	add r0, r5, #0
	mov r1, #4
	bl script_call
	b _08023B4C
_08023B44:
	ldr r0, [r5, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r5, #0x20]
_08023B4C:
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1

thumb_func_global objc_50
objc_50: @ 0x08023B54
	push {r4, lr}
	add r3, r0, #0
	ldr r0, [r3, #0x3c]
	ldr r1, [r0, #4]
	lsl r1, r1, #8
	ldr r0, [r0, #0xc]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	add r4, r0, #0
	asr r1, r1, #0x10
	ldrh r2, [r3, #0x14]
	cmp r1, r2
	bgt _08023B76
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r2, r0
	ble _08023B82
_08023B76:
	lsl r0, r4, #0x10
	asr r0, r0, #0x10
	cmp r0, r2
	bgt _08023B88
	cmp r2, r1
	bgt _08023B88
_08023B82:
	ldr r0, [r3, #0x20]
	ldr r0, [r0, #4]
	b _08023B8E
_08023B88:
	ldr r0, [r3, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08023B8E:
	str r0, [r3, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_52
objc_52: @ 0x08023B98
	push {r4, r5, lr}
	add r2, r0, #0
	ldr r1, [r2, #0x3c]
	ldr r0, [r1, #4]
	lsl r0, r0, #8
	lsr r4, r0, #0x10
	ldr r0, [r1, #0xc]
	lsl r0, r0, #8
	lsr r3, r0, #0x10
	add r5, r3, #0
	add r0, r2, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #0
	bne _08023BCE
	ldrh r0, [r2, #0x18]
	cmp r4, r0
	bls _08023BC4
	cmp r3, r0
	bhi _08023BE2
_08023BC4:
	cmp r0, r3
	bhi _08023BE2
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _08023BE8
_08023BCE:
	ldrh r0, [r2, #0x1a]
	cmp r4, r0
	bls _08023BD8
	cmp r3, r0
	bhi _08023BE2
_08023BD8:
	cmp r0, r5
	bhi _08023BE2
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _08023BE8
_08023BE2:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08023BE8:
	str r0, [r2, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_51
objc_51: @ 0x08023BF4
	push {r4, lr}
	add r3, r0, #0
	ldr r0, [r3, #0x3c]
	ldr r1, [r0, #8]
	lsl r1, r1, #8
	ldr r0, [r0, #0x10]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	add r4, r0, #0
	asr r1, r1, #0x10
	ldrh r2, [r3, #0x16]
	cmp r1, r2
	bgt _08023C16
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r2, r0
	ble _08023C22
_08023C16:
	lsl r0, r4, #0x10
	asr r0, r0, #0x10
	cmp r0, r2
	bgt _08023C28
	cmp r2, r1
	bgt _08023C28
_08023C22:
	ldr r0, [r3, #0x20]
	ldr r0, [r0, #4]
	b _08023C2E
_08023C28:
	ldr r0, [r3, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08023C2E:
	str r0, [r3, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_53_copyInitPos
objc_53_copyInitPos: @ 0x08023C38
	ldr r2, [r0, #0x3c]
	ldr r1, [r2, #4]
	asr r1, r1, #8
	strh r1, [r0, #0x14]
	ldr r1, [r2, #8]
	asr r1, r1, #8
	strh r1, [r0, #0x16]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_54_teleportToParent
objc_54_teleportToParent: @ 0x08023C54
	push {r4, lr}
	add r4, r0, #0
	ldr r1, [r4, #0x40]
	ldr r0, [r4, #0x3c]
	ldr r3, [r1, #0x3c]
	mov r1, #0x1c
	ldrsh r2, [r4, r1]
	lsl r2, r2, #8
	ldr r1, [r3, #4]
	add r1, r1, r2
	str r1, [r0, #4]
	mov r1, #0x1e
	ldrsh r2, [r4, r1]
	lsl r2, r2, #8
	ldr r1, [r3, #8]
	add r1, r1, r2
	str r1, [r0, #8]
	bl clearObjForces
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_55_teleportToParent2
objc_55_teleportToParent2: @ 0x08023C8C
	push {r4, lr}
	add r4, r0, #0
	ldr r3, [r4, #0x40]
	ldr r0, [r4, #0x3c]
	mov r1, #0x1c
	ldrsh r2, [r4, r1]
	lsl r2, r2, #8
	ldr r1, [r3, #4]
	add r1, r1, r2
	str r1, [r0, #4]
	mov r1, #0x1e
	ldrsh r2, [r4, r1]
	lsl r2, r2, #8
	ldr r1, [r3, #8]
	add r1, r1, r2
	str r1, [r0, #8]
	bl clearObjForces
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_56
objc_56: @ 0x08023CC0
	add r2, r0, #0
	add r0, #0x44
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	cmp r0, #0
	bge _08023CD2
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _08023CD8
_08023CD2:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08023CD8:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_57_condBranchField44_4
objc_57_condBranchField44_4: @ 0x08023CE0
	add r2, r0, #0
	add r0, #0x44
	ldrb r0, [r0]
	lsl r0, r0, #0x1b
	cmp r0, #0
	bge _08023CF2
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _08023CF8
_08023CF2:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08023CF8:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_58
objc_58: @ 0x08023D00
	add r2, r0, #0
	add r0, #0x44
	ldrb r0, [r0]
	lsr r0, r0, #7
	cmp r0, #0
	bne _08023D14
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	b _08023D18
_08023D14:
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
_08023D18:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_5a_ifChildFlag23
objc_5a_ifChildFlag23: @ 0x08023D20
	add r2, r0, #0
	ldr r0, [r2, #0x48]
	ldr r0, [r0, #0x44]
	lsl r0, r0, #5
	lsr r0, r0, #0x10
	mov r1, #0x80
	lsl r1, r1, #5
	and r0, r1
	cmp r0, #0
	beq _08023D3A
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _08023D40
_08023D3A:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08023D40:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_5b
objc_5b: @ 0x08023D48
	add r2, r0, #0
	add r0, #0x44
	ldrb r0, [r0]
	lsr r0, r0, #7
	cmp r0, #1
	bne _08023D6C
	ldr r0, [r2, #0x48]
	ldr r0, [r0, #0x44]
	lsl r0, r0, #5
	lsr r0, r0, #0x10
	mov r1, #0x80
	lsl r1, r1, #5
	and r0, r1
	cmp r0, #0
	beq _08023D6C
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _08023D72
_08023D6C:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08023D72:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_5c_ifChildFlag22
objc_5c_ifChildFlag22: @ 0x08023D78
	add r2, r0, #0
	ldr r0, [r2, #0x48]
	ldr r0, [r0, #0x44]
	lsl r0, r0, #5
	lsr r0, r0, #0x10
	mov r1, #0x80
	lsl r1, r1, #4
	and r0, r1
	cmp r0, #0
	beq _08023D92
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _08023D98
_08023D92:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08023D98:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_5d_if_f7_and_childF22
objc_5d_if_f7_and_childF22: @ 0x08023DA0
	add r2, r0, #0
	add r0, #0x44
	ldrb r0, [r0]
	lsr r0, r0, #7
	cmp r0, #1
	bne _08023DC4
	ldr r0, [r2, #0x48]
	ldr r0, [r0, #0x44]
	lsl r0, r0, #5
	lsr r0, r0, #0x10
	mov r1, #0x80
	lsl r1, r1, #4
	and r0, r1
	cmp r0, #0
	beq _08023DC4
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _08023DCA
_08023DC4:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08023DCA:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_5e_ifChildFlag21
objc_5e_ifChildFlag21: @ 0x08023DD0
	add r2, r0, #0
	ldr r0, [r2, #0x48]
	ldr r0, [r0, #0x44]
	lsl r0, r0, #5
	lsr r0, r0, #0x10
	mov r1, #0x80
	lsl r1, r1, #3
	and r0, r1
	cmp r0, #0
	beq _08023DEA
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _08023DF0
_08023DEA:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08023DF0:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_5f_if_f7_and_ChildFlag21
objc_5f_if_f7_and_ChildFlag21: @ 0x08023DF8
	add r2, r0, #0
	add r0, #0x44
	ldrb r0, [r0]
	lsr r0, r0, #7
	cmp r0, #1
	bne _08023E1C
	ldr r0, [r2, #0x48]
	ldr r0, [r0, #0x44]
	lsl r0, r0, #5
	lsr r0, r0, #0x10
	mov r1, #0x80
	lsl r1, r1, #3
	and r0, r1
	cmp r0, #0
	beq _08023E1C
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _08023E22
_08023E1C:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08023E22:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_59_callF44_7_cond
objc_59_callF44_7_cond: @ 0x08023E28
	push {lr}
	add r2, r0, #0
	add r0, #0x44
	ldrb r0, [r0]
	lsr r0, r0, #7
	cmp r0, #0
	bne _08023E40
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r2, #0x20]
	b _08023E48
_08023E40:
	add r0, r2, #0
	mov r1, #4
	bl script_call
_08023E48:
	mov r0, #0
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_60
objc_60: @ 0x08023E50
	push {r4, lr}
	add r4, r0, #0
	ldr r3, =byte_81428D4
	ldr r2, =0x03003D40
	ldr r0, [r4, #0x48]
	ldr r0, [r0, #8]
	ldrh r1, [r0, #0x1c]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r2, [r2, #0x11]
	add r0, r0, r2
	add r0, r0, r3
	ldrb r1, [r0]
	add r2, r4, #0
	add r2, #0x66
	ldrb r0, [r2]
	sub r0, r0, r1
	cmp r0, #0
	bge _08023E78
	mov r0, #0
_08023E78:
	strb r0, [r2]
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global objc_62
objc_62: @ 0x08023E94
	push {r4, r5, r6, lr}
	add r5, r0, #0
	ldr r3, =byte_81428D4
	ldr r6, =0x03003D40
	ldr r2, [r5, #0x48]
	ldr r0, [r2, #8]
	ldrh r1, [r0, #0x1c]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r1, [r6, #0x11]
	add r0, r0, r1
	add r0, r0, r3
	ldrb r4, [r0]
	ldr r0, [r2, #0x44]
	lsl r0, r0, #5
	lsr r0, r0, #0x10
	mov r1, #0x80
	lsl r1, r1, #5
	and r0, r1
	cmp r0, #0
	beq _08023EC0
	mov r4, #5
_08023EC0:
	add r1, r5, #0
	add r1, #0x66
	ldrb r0, [r1]
	sub r0, r0, r4
	cmp r0, #0
	bge _08023ECE
	mov r0, #0
_08023ECE:
	strb r0, [r1]
	neg r0, r4
	bl sub_0803226C
	ldr r0, [r5, #0x48]
	add r0, #0x68
	ldrb r3, [r0]
	ldr r2, =unk_8142986
	ldr r0, [r5, #8]
	ldrh r1, [r0, #0x22]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r6, [r6, #0x11]
	add r0, r0, r6
	lsl r0, r0, #1
	add r0, r0, r2
	ldrh r0, [r0]
	mul r0, r3, r0
	mul r0, r4, r0
	bl sub_8035044
	ldr r0, [r5, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r5, #0x20]
	mov r0, #0
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global objc_64
objc_64: @ 0x08023F14
	push {r4, lr}
	add r4, r0, #0
	ldr r3, =byte_81428D4
	ldr r2, =0x03003D40
	ldr r0, [r4, #0x48]
	ldr r0, [r0, #8]
	ldrh r1, [r0, #0x1c]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r2, [r2, #0x11]
	add r0, r0, r2
	add r0, r0, r3
	ldrb r0, [r0]
	add r2, r0, #0
	add r1, r4, #0
	add r1, #0x66
	ldrb r0, [r1]
	sub r0, r0, r2
	cmp r0, #0
	bge _08023F3E
	mov r0, #0
_08023F3E:
	strb r0, [r1]
	neg r0, r2
	lsl r0, r0, #2
	bl sub_08032280
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global objc_61_offsetPlayerHp
objc_61_offsetPlayerHp: @ 0x08023F60
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	mov r1, #4
	ldrsh r0, [r0, r1]
	bl modifyPlayerHealth
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_63_modifyPlayerCharge
objc_63_modifyPlayerCharge: @ 0x08023F80
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	mov r1, #4
	ldrsh r0, [r0, r1]
	bl ModifySpecialCharge
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_65_HostageDamage2
objc_65_HostageDamage2: @ 0x08023FA0
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	mov r1, #4
	ldrsh r0, [r0, r1]
	bl dealHostageDamage
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_66_removeHitbox
objc_66_removeHitbox: @ 0x08023FC0
	add r3, r0, #0
	add r3, #0x44
	ldrb r1, [r3]
	mov r2, #0x10
	orr r1, r2
	strb r1, [r3]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_67_addHitBox
objc_67_addHitBox: @ 0x08023FD8
	add r3, r0, #0
	add r3, #0x44
	ldrb r2, [r3]
	mov r1, #0x11
	neg r1, r1
	and r1, r2
	strb r1, [r3]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_68_setbit44_7
objc_68_setbit44_7: @ 0x08023FF4
	add r3, r0, #0
	add r3, #0x44
	ldrb r1, [r3]
	mov r2, #0x80
	orr r1, r2
	strb r1, [r3]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_69_clearbit44_7
objc_69_clearbit44_7: @ 0x0802400C
	add r3, r0, #0
	add r3, #0x44
	ldrb r2, [r3]
	mov r1, #0x7f
	and r1, r2
	strb r1, [r3]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_6b_setBit44_5
objc_6b_setBit44_5: @ 0x08024024
	add r3, r0, #0
	add r3, #0x44
	ldrb r2, [r3]
	mov r1, #0x21
	neg r1, r1
	and r1, r2
	strb r1, [r3]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_6a_clearBit44_5
objc_6a_clearBit44_5: @ 0x08024040
	add r3, r0, #0
	add r3, #0x44
	ldrb r1, [r3]
	mov r2, #0x20
	orr r1, r2
	strb r1, [r3]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_6c
objc_6c: @ 0x08024058
	push {lr}
	add r2, r0, #0
	add r0, #0x66
	ldrb r0, [r0]
	cmp r0, #0
	bne _08024072
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	str r0, [r2, #0x20]
	add r0, r2, #0
	bl sub_08013980
	b _0802407A
_08024072:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r2, #0x20]
_0802407A:
	mov r0, #0
	pop {r1}
	bx r1

thumb_func_global objc_6d_setBit9Field44
objc_6d_setBit9Field44: @ 0x08024080
	add r3, r0, #0
	add r3, #0x45
	ldrb r1, [r3]
	mov r2, #2
	orr r1, r2
	strb r1, [r3]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_6e_clearBit9Field44
objc_6e_clearBit9Field44: @ 0x08024098
	add r3, r0, #0
	add r3, #0x45
	ldrb r2, [r3]
	mov r1, #3
	neg r1, r1
	and r1, r2
	strb r1, [r3]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_6f_BranchCollisionOffset
objc_6f_BranchCollisionOffset: @ 0x080240B4
	push {r4, r5, r6, r7, lr}
	add r6, r0, #0
	ldr r3, [r6, #0x20]
	ldrh r5, [r3, #0xc]
	ldr r4, [r6, #0x3c]
	ldr r0, [r4, #4]
	asr r0, r0, #8
	ldr r2, =dirLutMaybe
	add r1, r6, #0
	add r1, #0x67
	ldrb r1, [r1]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	add r1, r1, r2
	mov r2, #0
	ldrsb r2, [r1, r2]
	ldrh r1, [r3, #8]
	add r7, r2, #0
	mul r7, r1, r7
	add r1, r7, #0
	add r0, r0, r1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r1, [r4, #8]
	asr r1, r1, #8
	ldrh r3, [r3, #0xa]
	add r1, r1, r3
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl getCollision
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, r5
	bne _08024104
	ldr r0, [r6, #0x20]
	ldr r0, [r0, #4]
	b _0802410A
	.align 2, 0
.pool
_08024104:
	ldr r0, [r6, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_0802410A:
	str r0, [r6, #0x20]
	mov r0, #0
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1

thumb_func_global objc_70
objc_70: @ 0x08024114
	push {r4, r5, lr}
	add r5, r0, #0
	ldr r0, [r5, #0x20]
	ldrh r4, [r0, #8]
	ldr r2, [r5, #0x3c]
	ldr r0, [r2, #4]
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	ldr r1, [r2, #8]
	asr r1, r1, #8
	ldrh r3, [r2, #0x3a]
	add r1, r1, r3
	ldrh r2, [r2, #0x3e]
	sub r1, r1, r2
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl getCollision
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, r4
	bne _08024146
	ldr r0, [r5, #0x20]
	ldr r0, [r0, #4]
	b _0802414C
_08024146:
	ldr r0, [r5, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_0802414C:
	str r0, [r5, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_71
objc_71: @ 0x08024158
	push {r4, r5, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldrh r5, [r0, #8]
	ldr r2, [r4, #0x3c]
	ldr r0, [r2, #4]
	asr r0, r0, #8
	ldrh r1, [r2, #0x38]
	add r0, r0, r1
	sub r0, #2
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	add r0, r4, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #1
	bne _0802418C
	ldrh r1, [r2, #0x3c]
	add r1, #3
	lsl r0, r3, #0x10
	asr r0, r0, #0x10
	add r0, r0, r1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
_0802418C:
	ldr r1, [r2, #8]
	asr r1, r1, #8
	ldrh r0, [r2, #0x3a]
	add r1, r1, r0
	ldrh r0, [r2, #0x3e]
	sub r1, r1, r0
	sub r1, #4
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	add r0, r3, #0
	bl getCollision
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, r5
	bne _080241B2
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #4]
	b _080241B8
_080241B2:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_080241B8:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_72
objc_72: @ 0x080241C4
	push {r4, r5, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldrh r5, [r0, #8]
	ldr r2, [r4, #0x3c]
	ldr r0, [r2, #4]
	asr r0, r0, #8
	ldrh r1, [r2, #0x38]
	add r0, r0, r1
	add r0, #2
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	add r0, r4, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #1
	bne _080241FC
	ldr r0, =0x0000FFFC
	add r1, r0, #0
	ldrh r0, [r2, #0x3c]
	add r1, r1, r0
	lsl r0, r3, #0x10
	asr r0, r0, #0x10
	add r0, r0, r1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
_080241FC:
	ldr r1, [r2, #8]
	asr r1, r1, #8
	ldrh r0, [r2, #0x3a]
	add r1, r1, r0
	ldrh r0, [r2, #0x3e]
	sub r1, r1, r0
	sub r1, #4
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	add r0, r3, #0
	bl getCollision
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, r5
	bne _08024228
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #4]
	b _0802422E
	.align 2, 0
.pool
_08024228:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_0802422E:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1

thumb_func_global objc_73
objc_73: @ 0x08024238
	push {r4, r5, r6, r7, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldrh r7, [r0, #8]
	ldr r2, [r4, #0x3c]
	ldr r0, [r2, #4]
	asr r0, r0, #8
	ldrh r1, [r2, #0x38]
	add r0, r0, r1
	sub r0, #1
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	add r0, r4, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #1
	bne _0802426C
	ldrh r1, [r2, #0x3c]
	add r1, #1
	lsl r0, r6, #0x10
	asr r0, r0, #0x10
	add r0, r0, r1
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
_0802426C:
	ldr r0, [r2, #8]
	asr r0, r0, #8
	ldrh r1, [r2, #0x3a]
	add r0, r0, r1
	ldrh r1, [r2, #0x3e]
	sub r0, r0, r1
	add r0, #8
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	add r0, r6, #0
	add r1, r5, #0
	bl getCollision
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, r7
	beq _080242A4
	add r1, r5, #0
	add r1, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	add r0, r6, #0
	bl getCollision
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, r7
	bne _080242AA
_080242A4:
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #4]
	b _080242B0
_080242AA:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_080242B0:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_74
objc_74: @ 0x080242BC
	push {r4, r5, lr}
	add r4, r0, #0
	ldr r2, [r4, #0x3c]
	ldr r0, [r2, #4]
	asr r0, r0, #8
	ldrh r1, [r2, #0x38]
	add r0, r0, r1
	sub r0, #2
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	ldr r0, [r2, #8]
	asr r0, r0, #8
	ldrh r1, [r2, #0x3a]
	add r0, r0, r1
	ldrh r1, [r2, #0x3e]
	sub r0, r0, r1
	sub r0, #4
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	add r0, r4, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #1
	bne _080242FE
	ldrh r1, [r2, #0x3c]
	add r1, #3
	lsl r0, r3, #0x10
	asr r0, r0, #0x10
	add r0, r0, r1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
_080242FE:
	add r0, r3, #0
	add r1, r5, #0
	bl getCollision
	lsl r0, r0, #0x10
	ldr r1, =0xFFEF0000
	add r0, r0, r1
	lsr r0, r0, #0x10
	cmp r0, #1
	bhi _0802431C
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #4]
	b _08024322
	.align 2, 0
.pool
_0802431C:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08024322:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1

thumb_func_global objc_75
objc_75: @ 0x0802432C
	push {r4, r5, r6, r7, lr}
	add r5, r0, #0
	ldr r2, [r5, #0x3c]
	ldr r0, [r2, #4]
	asr r0, r0, #8
	ldrh r1, [r2, #0x38]
	add r0, r0, r1
	sub r0, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	ldr r0, [r2, #8]
	asr r0, r0, #8
	ldrh r1, [r2, #0x3a]
	add r0, r0, r1
	ldrh r1, [r2, #0x3e]
	sub r0, r0, r1
	add r0, #8
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	add r0, r5, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #1
	bne _0802436E
	ldrh r1, [r2, #0x3c]
	add r1, #1
	lsl r0, r4, #0x10
	asr r0, r0, #0x10
	add r0, r0, r1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
_0802436E:
	add r0, r4, #0
	add r1, r7, #0
	bl getCollision
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	add r1, r7, #0
	add r1, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	add r0, r4, #0
	bl getCollision
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r6, #0x10
	beq _0802439E
	ldr r1, =0x0000FFFF
	cmp r6, r1
	beq _0802439E
	cmp r0, #0x10
	beq _0802439E
	cmp r0, r1
	bne _080243A8
_0802439E:
	ldr r0, [r5, #0x20]
	ldr r0, [r0, #4]
	b _080243AE
	.align 2, 0
.pool
_080243A8:
	ldr r0, [r5, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_080243AE:
	str r0, [r5, #0x20]
	mov r0, #0
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1

thumb_func_global objc_76
objc_76: @ 0x080243B8
	push {r4, r5, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldrh r5, [r0, #4]
	mov r2, #4
	ldrsh r1, [r0, r2]
	add r0, r1, #0
	cmp r1, #0
	bge _080243CC
	neg r0,r1
_080243CC:
	cmp r0, #0xf
	ble _080243E4
	add r0, r1, #0
	cmp r1, #0
	bge _080243D8
	neg r0,r1
_080243D8:
	bl divide
	lsl r1, r0, #1
	add r1, r1, r0
	lsl r1, r1, #0x12
	lsr r5, r1, #0x10
_080243E4:
	ldr r3, [r4, #0x3c]
	ldr r0, [r3, #4]
	asr r0, r0, #8
	ldr r2, =dirLutMaybe
	add r1, r4, #0
	add r1, #0x67
	ldrb r1, [r1]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	add r1, r1, r2
	mov r2, #0
	ldrsb r2, [r1, r2]
	lsl r1, r5, #0x10
	asr r1, r1, #0x10
	mul r1, r2, r1
	add r0, r0, r1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r1, [r3, #8]
	ldr r2, [r4, #0x20]
	asr r1, r1, #8
	ldrh r2, [r2, #6]
	add r1, r1, r2
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl getCollision
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x10
	and r0, r1
	cmp r0, #0
	beq _08024430
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #8]
	b _08024436
	.align 2, 0
.pool
_08024430:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08024436:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1

thumb_func_global objc_77
objc_77: @ 0x08024440
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldrh r0, [r0, #4]
	ldr r2, [r4, #0x3c]
	ldr r1, [r2, #4]
	lsl r1, r1, #8
	lsr r1, r1, #0x10
	ldr r2, [r2, #8]
	lsl r2, r2, #8
	lsr r2, r2, #0x10
	bl createObj_801ED54
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_78
objc_78: @ 0x0802446C
	push {r4, lr}
	add r4, r0, #0
	ldr r2, [r4, #0x3c]
	ldr r1, [r2, #4]
	ldr r0, [r4, #0x20]
	asr r1, r1, #8
	ldrh r3, [r0, #6]
	add r1, r1, r3
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r2, [r2, #8]
	asr r2, r2, #8
	ldrh r3, [r0, #8]
	add r2, r2, r3
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldrh r0, [r0, #4]
	bl createObj_801ED54
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_79_unbox
objc_79_unbox: @ 0x080244A4
	push {r4, lr}
	add r4, r0, #0
	add r1, r4, #0
	add r1, #0x5e
	ldrh r0, [r1]
	cmp r0, #0
	beq _080244C4
	ldr r2, [r4, #0x3c]
	ldr r1, [r2, #4]
	lsl r1, r1, #8
	lsr r1, r1, #0x10
	ldr r2, [r2, #8]
	lsl r2, r2, #8
	lsr r2, r2, #0x10
	bl createRunTimeMapObj
_080244C4:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_7a
objc_7a: @ 0x080244D4
	push {r4, lr}
	add r4, r0, #0
	add r0, #0x67
	ldrb r1, [r0]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	add r0, r4, #0
	bl sub_0801FD54
	cmp r0, #0
	beq _080244F0
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #4]
	b _080244F6
_080244F0:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_080244F6:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_7b
objc_7b: @ 0x08024500
	push {r4, lr}
	add r4, r0, #0
	add r0, #0x67
	ldrb r1, [r0]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	add r0, r4, #0
	bl sub_0801FD54
	cmp r0, #0
	bne _0802451C
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #4]
	b _08024522
_0802451C:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08024522:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_7c
objc_7c: @ 0x0802452C
	push {r4, lr}
	add r4, r0, #0
	mov r1, #1
	bl sub_0801FD54
	cmp r0, #0
	bne _08024546
	add r0, r4, #0
	mov r1, #0
	bl sub_0801FD54
	cmp r0, #0
	beq _0802454C
_08024546:
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #4]
	b _08024552
_0802454C:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08024552:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_7d
objc_7d: @ 0x0802455C
	push {r4, lr}
	add r4, r0, #0
	mov r1, #1
	bl sub_0801FD54
	cmp r0, #0
	bne _08024576
	add r0, r4, #0
	mov r1, #0
	bl sub_0801FD54
	cmp r0, #0
	beq _0802457E
_08024576:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	b _08024582
_0802457E:
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #4]
_08024582:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_7e
objc_7e: @ 0x0802458C
	push {r4, lr}
	add r4, r0, #0
	mov r1, #2
	bl sub_0801FD54
	cmp r0, #0
	beq _080245A0
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #4]
	b _080245A6
_080245A0:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_080245A6:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_7f
objc_7f: @ 0x080245B0
	push {r4, lr}
	add r4, r0, #0
	mov r1, #2
	bl sub_0801FD54
	cmp r0, #0
	bne _080245C4
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #4]
	b _080245CA
_080245C4:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_080245CA:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_80
objc_80: @ 0x080245D4
	push {r4, r5, lr}
	add r4, r0, #0
	ldr r3, [r4, #0x3c]
	ldr r0, [r3, #8]
	asr r0, r0, #8
	add r0, #0x14
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldr r0, [r3, #4]
	asr r0, r0, #8
	ldrh r1, [r3, #0x38]
	add r0, r0, r1
	sub r0, #4
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	add r0, r4, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #1
	bne _0802460E
	ldrh r1, [r3, #0x3c]
	add r1, #8
	lsl r0, r2, #0x10
	asr r0, r0, #0x10
	add r0, r0, r1
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
_0802460E:
	add r0, r2, #0
	add r1, r5, #0
	bl getCollision
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08024624
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	b _08024628
_08024624:
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #4]
_08024628:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_81
objc_81: @ 0x08024634
	push {r4, r5, lr}
	add r4, r0, #0
	ldr r3, [r4, #0x3c]
	ldr r0, [r3, #8]
	asr r0, r0, #8
	add r0, #0x24
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldr r0, [r3, #4]
	asr r0, r0, #8
	ldrh r1, [r3, #0x38]
	add r0, r0, r1
	sub r0, #4
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	add r0, r4, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #1
	bne _0802466E
	ldrh r1, [r3, #0x3c]
	add r1, #8
	lsl r0, r2, #0x10
	asr r0, r0, #0x10
	add r0, r0, r1
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
_0802466E:
	add r0, r2, #0
	add r1, r5, #0
	bl getCollision
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08024684
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	b _08024688
_08024684:
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #4]
_08024688:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_82_branchNotSprite20
objc_82_branchNotSprite20: @ 0x08024694
	add r2, r0, #0
	ldr r0, [r2, #0x3c]
	ldr r0, [r0]
	mov r1, #0x20
	ldrsh r0, [r0, r1]
	cmp r0, #0
	bne _080246A8
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _080246AE
_080246A8:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_080246AE:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_83_setField44_b10
objc_83_setField44_b10: @ 0x080246B4
	add r3, r0, #0
	add r3, #0x45
	ldrb r1, [r3]
	mov r2, #4
	orr r1, r2
	strb r1, [r3]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_84_clearFlag10
objc_84_clearFlag10: @ 0x080246CC
	add r3, r0, #0
	add r3, #0x45
	ldrb r2, [r3]
	mov r1, #5
	neg r1, r1
	and r1, r2
	strb r1, [r3]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_85_setConstSize
objc_85_setConstSize: @ 0x080246E8
	ldr r3, [r0, #0x3c]
	ldr r1, =word_814259C
	ldr r2, [r1, #4]
	ldr r1, [r1]
	str r1, [r3, #0x38]
	str r2, [r3, #0x3c]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0
.pool

thumb_func_global objc_86_setConstSize2
objc_86_setConstSize2: @ 0x08024704
	ldr r3, [r0, #0x3c]
	ldr r1, =word_8142594
	ldr r2, [r1, #4]
	ldr r1, [r1]
	str r1, [r3, #0x38]
	str r2, [r3, #0x3c]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0
.pool

thumb_func_global objc_87_condBranchIfCollides
objc_87_condBranchIfCollides: @ 0x08024720
	add r2, r0, #0
	ldr r0, [r2, #0x3c]
	add r0, #0x4f
	ldrb r0, [r0]
	cmp r0, #0
	beq _08024732
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _08024738
_08024732:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08024738:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_88_clearInit52b0
objc_88_clearInit52b0: @ 0x08024740
	ldr r2, [r0, #0x3c]
	add r2, #0x52
	ldrb r3, [r2]
	mov r1, #2
	neg r1, r1
	and r1, r3
	strb r1, [r2]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_89_setbit0Init52
objc_89_setbit0Init52: @ 0x0802475C
	ldr r2, [r0, #0x3c]
	add r2, #0x52
	ldrb r1, [r2]
	mov r3, #1
	orr r1, r3
	strb r1, [r2]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_8a_switch
objc_8a_switch: @ 0x08024774
	push {r4, lr}
	add r4, r0, #0
	ldr r1, [r4, #0x20]
	ldrh r0, [r1, #4]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r4, #0x20]
	bl random
	lsl r0, r0, #0x10
	ldr r1, [r4, #0x20]
	lsr r0, r0, #0xe
	add r0, r0, r1
	ldr r0, [r0]
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_8b
objc_8b: @ 0x0802479C
	mov ip, r0
	add r0, #0x44
	ldrb r0, [r0]
	lsl r0, r0, #0x1b
	cmp r0, #0
	bge _080247E8
	mov r0, ip
	add r0, #0x5c
	ldrh r0, [r0]
	cmp r0, #2
	beq _080247D8
	mov r0, ip
	ldr r1, [r0, #0x48]
	add r0, r1, #0
	add r0, #0x5c
	ldrh r0, [r0]
	cmp r0, #1
	beq _080247D8
	ldr r3, =byte_81429F2
	ldr r2, =0x03003D40
	ldr r0, [r1, #8]
	ldrh r1, [r0, #0x24]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r2, [r2, #0x11]
	add r0, r0, r2
	add r0, r0, r3
	ldrb r0, [r0]
	cmp r0, #0
	beq _080247E8
_080247D8:
	mov r1, ip
	ldr r0, [r1, #0x20]
	ldr r0, [r0, #4]
	b _080247F2
	.align 2, 0
.pool
_080247E8:
	mov r1, ip
	ldr r0, [r1, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	mov r1, ip
_080247F2:
	str r0, [r1, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_8c_offsetPosition
objc_8c_offsetPosition: @ 0x080247F8
	push {r4, r5, lr}
	ldr r5, [r0, #0x20]
	ldrh r3, [r5, #6]
	ldr r4, [r0, #0x3c]
	mov r1, #4
	ldrsh r2, [r5, r1]
	lsl r2, r2, #8
	ldr r1, [r4, #4]
	add r1, r1, r2
	str r1, [r4, #4]
	lsl r3, r3, #0x10
	asr r3, r3, #8
	ldr r1, [r4, #8]
	add r1, r1, r3
	str r1, [r4, #8]
	ldrb r1, [r5, #2]
	add r5, r5, r1
	str r5, [r0, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1

thumb_func_global objc_8d_offsetPosition2
objc_8d_offsetPosition2: @ 0x08024824
	push {r4, r5, r6, lr}
	add r5, r0, #0
	ldr r4, [r5, #0x20]
	ldrh r3, [r4, #4]
	ldrh r6, [r4, #6]
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #0
	bne _08024840
	lsl r0, r3, #0x10
	neg r0, r0
	lsr r3, r0, #0x10
_08024840:
	ldr r2, [r5, #0x3c]
	lsl r1, r3, #0x10
	asr r1, r1, #8
	ldr r0, [r2, #4]
	add r0, r0, r1
	str r0, [r2, #4]
	lsl r1, r6, #0x10
	asr r1, r1, #8
	ldr r0, [r2, #8]
	add r0, r0, r1
	str r0, [r2, #8]
	ldrb r0, [r4, #2]
	add r0, r4, r0
	str r0, [r5, #0x20]
	mov r0, #0
	pop {r4, r5, r6}
	pop {r1}
	bx r1

thumb_func_global objc_8e_setObjIdVelX
objc_8e_setObjIdVelX: @ 0x08024864
	push {r4, lr}
	add r4, r0, #0
	ldr r2, =cmd83_velocities
	add r0, #0x5a
	ldrh r1, [r0]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #1
	add r0, r0, r2
	ldrh r2, [r0]
	add r0, r4, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #0
	bne _0802488C
	lsl r0, r2, #0x10
	neg r0, r0
	lsr r2, r0, #0x10
_0802488C:
	ldr r0, [r4, #0x3c]
	lsl r1, r2, #0x10
	asr r1, r1, #0x10
	bl setObjVelocity_X
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global objc_8f_setObjIdVelX2
objc_8f_setObjIdVelX2: @ 0x080248AC
	push {r4, lr}
	add r4, r0, #0
	ldr r2, =cmd83_velocities
	add r0, #0x5a
	ldrh r1, [r0]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #1
	add r2, #4
	add r0, r0, r2
	ldrh r2, [r0]
	add r0, r4, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #0
	bne _080248D6
	lsl r0, r2, #0x10
	neg r0, r0
	lsr r2, r0, #0x10
_080248D6:
	ldr r0, [r4, #0x3c]
	lsl r1, r2, #0x10
	asr r1, r1, #0x10
	bl setObjVelocity_X
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global objc_90
objc_90: @ 0x080248F4
	push {r4, lr}
	add r4, r0, #0
	ldr r2, =cmd83_velocities
	add r0, #0x5a
	ldrh r1, [r0]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #1
	add r2, #2
	add r0, r0, r2
	ldrh r2, [r0]
	add r0, r4, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #0
	bne _0802491E
	lsl r0, r2, #0x10
	neg r0, r0
	lsr r2, r0, #0x10
_0802491E:
	ldr r0, [r4, #0x3c]
	lsl r1, r2, #0x10
	asr r1, r1, #0x10
	bl setObjVelocity_X
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global objc_91
objc_91: @ 0x0802493C
	add r2, r0, #0
	ldr r1, =dword_8142410
	add r0, #0x5a
	ldrh r0, [r0]
	add r0, r0, r1
	ldrb r1, [r0]
	ldr r0, =0x03006BF8
	add r1, r1, r0
	ldrb r0, [r1]
	cmp r0, #0
	bne _0802495A
	add r0, r2, #0
	add r0, #0x63
	ldrb r0, [r0]
	strb r0, [r1]
_0802495A:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0
.pool

thumb_func_global objc_92
objc_92: @ 0x08024970
	add r3, r0, #0
	ldr r1, =dword_8142410
	add r0, #0x5a
	ldrh r0, [r0]
	add r0, r0, r1
	ldrb r1, [r0]
	ldr r0, =0x03006BF8
	add r1, r1, r0
	add r2, r3, #0
	add r2, #0x63
	ldrb r0, [r1]
	ldrb r2, [r2]
	cmp r0, r2
	bne _08024990
	mov r0, #0
	strb r0, [r1]
_08024990:
	ldr r0, [r3, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r3, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0
.pool

thumb_func_global objc_93
objc_93: @ 0x080249A4
	add r2, r0, #0
	ldr r1, =dword_8142410
	add r0, #0x5a
	ldrh r0, [r0]
	add r0, r0, r1
	ldrb r0, [r0]
	ldr r1, =0x03006BF8
	add r0, r0, r1
	ldrb r1, [r0]
	cmp r1, #0
	beq _080249D4
	add r0, r2, #0
	add r0, #0x63
	ldrb r0, [r0]
	cmp r1, r0
	beq _080249D4
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _080249DA
	.align 2, 0
.pool
_080249D4:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_080249DA:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_94
objc_94: @ 0x080249E0
	push {r4, lr}
	add r4, r0, #0
	bl ObjPlayerDiff_Y
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #0
	blt _08024A00
	cmp r0, #0
	bne _08024A0C
	ldr r0, =0x03004750
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x1f
	cmp r0, #0
	beq _08024A0C
_08024A00:
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #4]
	b _08024A12
	.align 2, 0
.pool
_08024A0C:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08024A12:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_95
objc_95: @ 0x08024A1C
	push {r4, r5, r6, lr}
	add r5, r0, #0
	bl ObjPlayerDiff_Y
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	add r0, r5, #0
	bl objHorizontalDistToPlayer
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	add r0, r5, #0
	bl objVerticalDistToPlayer
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	lsl r4, r4, #0x10
	cmp r4, #0
	ble _08024A5A
	cmp r6, #0x20
	bls _08024A5A
	sub r0, r6, r0
	cmp r0, #0
	bge _08024A50
	neg r0, r0
_08024A50:
	cmp r0, #0xf
	bgt _08024A5A
	ldr r0, [r5, #0x20]
	ldr r0, [r0, #4]
	b _08024A60
_08024A5A:
	ldr r0, [r5, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08024A60:
	str r0, [r5, #0x20]
	mov r0, #0
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_96
objc_96: @ 0x08024A6C
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	mov ip, r0
	ldr r1, [r0, #0x3c]
	ldr r0, [r1, #4]
	lsl r0, r0, #8
	lsr r6, r0, #0x10
	ldr r0, [r1, #8]
	lsl r0, r0, #8
	lsr r7, r0, #0x10
	ldr r0, =0x03006770
	ldr r4, [r0, #4]
	add r0, #0x74
	cmp r4, r0
	beq _08024B08
	mov r0, #0xf
	mov r8, r0
_08024A90:
	ldr r0, [r4, #0x44]
	lsl r0, r0, #5
	lsr r0, r0, #0x10
	mov r1, #0x80
	lsl r1, r1, #3
	and r0, r1
	cmp r0, #0
	beq _08024B00
	ldr r0, [r4, #0x3c]
	ldr r1, [r0, #4]
	lsl r1, r1, #8
	lsr r3, r1, #0x10
	ldr r1, [r0, #8]
	lsl r1, r1, #8
	lsr r1, r1, #0x10
	add r5, r0, #0
	cmp r7, r1
	bhi _08024B00
	add r0, r7, #0
	add r0, #0x20
	cmp r1, r0
	bgt _08024B00
	sub r0, r6, r3
	cmp r0, #0
	bge _08024AC4
	neg r0, r0
_08024AC4:
	cmp r0, #0x77
	bgt _08024B00
	mov r2, ip
	add r2, #0x67
	ldrb r1, [r2]
	mov r0, r8
	and r0, r1
	cmp r0, #1
	bne _08024AE0
	ldr r0, [r5, #0x18]
	cmp r0, #0
	bge _08024AE0
	cmp r6, r3
	blo _08024AF4
_08024AE0:
	ldrb r1, [r2]
	mov r0, r8
	and r0, r1
	cmp r0, #0
	bne _08024B00
	ldr r0, [r5, #0x18]
	cmp r0, #0
	ble _08024B00
	cmp r6, r3
	bls _08024B00
_08024AF4:
	mov r1, ip
	ldr r0, [r1, #0x20]
	ldr r0, [r0, #4]
	b _08024B12
	.align 2, 0
.pool
_08024B00:
	ldr r4, [r4, #4]
	ldr r0, =0x030067E4
	cmp r4, r0
	bne _08024A90
_08024B08:
	mov r1, ip
	ldr r0, [r1, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	mov r1, ip
_08024B12:
	str r0, [r1, #0x20]
	mov r0, #0
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global objc_97_hostageSaved
objc_97_hostageSaved: @ 0x08024B24
	push {r4, lr}
	add r4, r0, #0
	bl getChargeToGive
	bl ModifySpecialCharge
	add r0, r4, #0
	bl sub_0802025C
	bl saveHostage
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_98_hostageDies
objc_98_hostageDies: @ 0x08024B4C
	push {r4, lr}
	add r4, r0, #0
	ldr r1, =0x03003D40
	mov r0, #0x14
	neg r0, r0
	ldrb r1, [r1, #0x11]
	lsl r0, r1
	bl dealHostageDamage
	bl hostageDeathGfx
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global objc_99
objc_99: @ 0x08024B78
	push {r4, lr}
	add r4, r0, #0
	add r0, #0x60
	ldrh r0, [r0]
	bl setEntDeletionFlag
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_9a
objc_9a: @ 0x08024B94
	push {r4, lr}
	add r4, r0, #0
	add r0, #0x60
	ldrh r0, [r0]
	bl getEntDeletionFlag
	cmp r0, #0
	beq _08024BAA
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #4]
	b _08024BB0
_08024BAA:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08024BB0:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_9b
objc_9b: @ 0x08024BBC
	push {r4, lr}
	add r4, r0, #0
	ldr r2, [r4, #0x3c]
	ldr r1, [r2, #4]
	ldr r0, [r4, #0x20]
	asr r1, r1, #8
	ldrh r3, [r0, #6]
	add r1, r1, r3
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r2, [r2, #8]
	asr r2, r2, #8
	ldrh r3, [r0, #8]
	add r2, r2, r3
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldrh r0, [r0, #4]
	bl ent_create_801EE30
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_9c
objc_9c: @ 0x08024BF4
	push {r4, r5, lr}
	add r4, r0, #0
	bl getChargeToGive
	ldr r5, =byte_81428D4
	ldr r3, =0x03003D40
	ldr r1, [r4, #0x48]
	ldr r1, [r1, #8]
	ldrh r2, [r1, #0x1c]
	lsl r1, r2, #1
	add r1, r1, r2
	ldrb r3, [r3, #0x11]
	add r1, r1, r3
	add r1, r1, r5
	ldrb r1, [r1]
	mul r0, r1, r0
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	bl ModifySpecialCharge
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global objc_9d
objc_9d: @ 0x08024C34
	push {r4, r5, lr}
	add r4, r0, #0
	ldr r3, =byte_81428D4
	ldr r0, =0x03003D40
	ldrb r5, [r0, #0x11]
	ldr r2, [r4, #0x48]
	ldr r0, [r2, #8]
	ldrh r1, [r0, #0x1c]
	lsl r0, r1, #1
	add r0, r0, r1
	add r0, r5, r0
	add r0, r0, r3
	ldrb r1, [r0]
	add r2, #0x68
	ldrb r0, [r2]
	add r3, r1, #0
	mul r3, r0, r3
	ldr r2, =unk_8142986
	ldr r0, [r4, #8]
	ldrh r1, [r0, #0x22]
	lsl r0, r1, #1
	add r0, r0, r1
	add r0, r0, r5
	lsl r0, r0, #1
	add r0, r0, r2
	ldrh r0, [r0]
	mul r0, r3, r0
	bl sub_8035044
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global objc_9e
objc_9e: @ 0x08024C8C
	push {r4, lr}
	add r4, r0, #0
	bl getChargeToGive
	bl ModifySpecialCharge
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_9f
objc_9f: @ 0x08024CA8
	push {r4, lr}
	add r4, r0, #0
	bl sub_0802025C
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_a0_completeLevel
objc_a0_completeLevel: @ 0x08024CC0
	push {lr}
	ldr r2, =0x03003D40
	mov r1, #1
	strb r1, [r2, #0x10]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	bl sub_080139CC
	mov r0, #0
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global objc_a1
objc_a1: @ 0x08024CE0
	add r2, r0, #0
	add r0, #0x5a
	ldrh r0, [r0]
	sub r0, #0x28
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #2
	bhi _08024CF6
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _08024CFC
_08024CF6:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08024CFC:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_a2
objc_a2: @ 0x08024D04
	add r2, r0, #0
	ldr r1, [r2, #0x20]
	ldr r0, [r2, #0x48]
	add r0, #0x5a
	ldrh r0, [r0]
	ldrh r3, [r1, #8]
	cmp r0, r3
	bne _08024D18
	ldr r0, [r1, #4]
	b _08024D1C
_08024D18:
	ldrb r0, [r1, #2]
	add r0, r1, r0
_08024D1C:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_a3
objc_a3: @ 0x08024D24
	add r2, r0, #0
	ldr r1, [r2, #0x20]
	ldrh r3, [r1, #8]
	add r0, #0x44
	ldrb r0, [r0]
	lsr r0, r0, #7
	cmp r0, #0
	beq _08024D42
	ldr r0, [r2, #0x48]
	add r0, #0x5a
	ldrh r0, [r0]
	cmp r0, r3
	bne _08024D42
	ldr r0, [r1, #4]
	b _08024D48
_08024D42:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08024D48:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_a4_playSound
objc_a4_playSound: @ 0x08024D50
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldrh r0, [r0, #4]
	bl playSong
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_a5
objc_a5: @ 0x08024D6C
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldrh r0, [r0, #4]
	bl stopSong_maybe
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_a6
objc_a6: @ 0x08024D88
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #4]
	ldrb r2, [r0, #5]
	ldr r0, [r4, #0x3c]
	ldr r0, [r0]
	bl setSpritePriority
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_a7_setTaskPriority
objc_a7_setTaskPriority: @ 0x08024DAC
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #4]
	ldr r0, [r4, #0xc]
	bl setTaskPriority
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_a8
objc_a8: @ 0x08024DCC
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x3c]
	ldr r1, [r0, #0x18]
	str r1, [r0, #0x20]
	ldr r1, [r0, #0x1c]
	str r1, [r0, #0x24]
	mov r1, #0
	mov r2, #0
	bl setObjVelocity
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_a9_modifyVel
objc_a9_modifyVel: @ 0x08024DF4
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x3c]
	ldr r1, [r0, #0x20]
	ldr r2, [r0, #0x24]
	bl setObjVelocity
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_aa
objc_aa: @ 0x08024E14
	push {r4, lr}
	ldr r3, [r0, #0x3c]
	ldr r1, [r0, #0x20]
	mov r4, #4
	ldrsh r2, [r1, r4]
	str r2, [r3, #0x20]
	mov r4, #6
	ldrsh r2, [r1, r4]
	str r2, [r3, #0x24]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_ab
objc_ab: @ 0x08024E34
	add r2, r0, #0
	add r0, #0x5a
	ldrh r0, [r0]
	cmp r0, #0x81
	blt _08024E4E
	cmp r0, #0x82
	ble _08024E4E
	cmp r0, #0x84
	bgt _08024E4E
	ldr r0, [r2, #0x3c]
	ldr r1, [r0, #8]
	ldr r0, [r0, #0x1c]
	b _08024E54
_08024E4E:
	ldr r0, [r2, #0x3c]
	ldr r1, [r0, #4]
	ldr r0, [r0, #0x18]
_08024E54:
	add r1, r1, r0
	lsl r1, r1, #8
	lsr r1, r1, #0x10
	mov r3, #0x1c
	ldrsh r0, [r2, r3]
	ldrh r3, [r2, #0x1c]
	cmp r0, #0
	bne _08024E68
	ldrh r0, [r2, #0x1a]
	b _08024E6A
_08024E68:
	ldrh r0, [r2, #0x18]
_08024E6A:
	cmp r1, r0
	bne _08024E7C
	add r0, r3, #1
	mov r1, #1
	and r0, r1
	strh r0, [r2, #0x1c]
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _08024E82
_08024E7C:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08024E82:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_ac
objc_ac: @ 0x08024E88
	push {r4, r5, r6, lr}
	ldr r5, [r0, #0x3c]
	ldr r2, [r0, #0x20]
	mov r1, #4
	ldrsh r3, [r2, r1]
	ldr r4, =dirLutMaybe
	mov r6, #0x67
	ldrb r1, [r6, r0]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	add r1, r1, r4
	ldrb r1, [r1]
	lsl r1, r1, #0x18
	asr r1, r1, #0x18
	mul r1, r3, r1
	str r1, [r5, #0x20]
	mov r1, #6
	ldrsh r3, [r2, r1]
	ldrb r1, [r6, r0]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	add r1, r1, r4
	ldrb r1, [r1]
	lsl r1, r1, #0x18
	asr r1, r1, #0x18
	mul r1, r3, r1
	str r1, [r5, #0x24]
	ldrb r1, [r2, #2]
	add r2, r2, r1
	str r2, [r0, #0x20]
	mov r0, #0
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global objc_ad
objc_ad: @ 0x08024ED0
	push {r4, r5, lr}
	add r4, r0, #0
	ldr r2, [r4, #0x3c]
	ldrh r1, [r2, #0x38]
	ldrh r0, [r2, #0x14]
	add r1, r1, r0
	ldrh r3, [r2, #0x16]
	ldrh r0, [r2, #0x3a]
	sub r3, r3, r0
	ldrh r0, [r2, #0x3c]
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	add r0, r1, r0
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldrh r0, [r2, #0x3e]
	lsl r3, r3, #0x10
	asr r3, r3, #0x10
	add r0, r3, r0
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	cmp r1, #0
	blt _08024F18
	lsl r0, r5, #0x10
	asr r0, r0, #0x10
	cmp r0, #0xf0
	bgt _08024F18
	cmp r3, #0
	blt _08024F18
	lsl r0, r2, #0x10
	asr r0, r0, #0x10
	cmp r0, #0xa0
	bgt _08024F18
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #4]
	b _08024F1E
_08024F18:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08024F1E:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1

thumb_func_global objc_ae_setHighBit
objc_ae_setHighBit: @ 0x08024F28
	push {r4, r5, lr}
	ldr r3, [r0, #0x20]
	ldr r4, [r0, #0x44]
	lsl r2, r4, #5
	lsr r2, r2, #0x10
	mov r1, #1
	ldrh r5, [r3, #4]
	lsl r1, r5
	orr r2, r1
	ldr r1, =0x0000FFFF
	and r2, r1
	lsl r2, r2, #0xb
	ldr r1, =0xF80007FF
	and r1, r4
	orr r1, r2
	str r1, [r0, #0x44]
	ldrb r1, [r3, #2]
	add r3, r3, r1
	str r3, [r0, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global objc_af_clearHighBit
objc_af_clearHighBit: @ 0x08024F60
	push {r4, r5, lr}
	ldr r3, [r0, #0x20]
	ldr r4, [r0, #0x44]
	lsl r2, r4, #5
	lsr r2, r2, #0x10
	mov r1, #1
	ldrh r5, [r3, #4]
	lsl r1, r5
	bic r2, r1
	lsl r2, r2, #0xb
	ldr r1, =0xF80007FF
	and r1, r4
	orr r1, r2
	str r1, [r0, #0x44]
	ldrb r1, [r3, #2]
	add r3, r3, r1
	str r3, [r0, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global objc_b0_BranchHighFlag
objc_b0_BranchHighFlag: @ 0x08024F90
	add r3, r0, #0
	ldr r2, [r3, #0x20]
	ldr r0, [r3, #0x44]
	lsl r0, r0, #5
	lsr r0, r0, #0x10
	ldrh r1, [r2, #8]
	asr r0, r1
	mov r1, #1
	and r0, r1
	cmp r0, #0
	beq _08024FAA
	ldr r0, [r2, #4]
	b _08024FAE
_08024FAA:
	ldrb r0, [r2, #2]
	add r0, r2, r0
_08024FAE:
	str r0, [r3, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_b1_branchNotHighFlag
objc_b1_branchNotHighFlag: @ 0x08024FB4
	add r3, r0, #0
	ldr r2, [r3, #0x20]
	ldr r0, [r3, #0x44]
	lsl r0, r0, #5
	lsr r0, r0, #0x10
	ldrh r1, [r2, #8]
	asr r0, r1
	mov r1, #1
	and r0, r1
	cmp r0, #0
	beq _08024FD0
	ldrb r0, [r2, #2]
	add r0, r2, r0
	b _08024FD2
_08024FD0:
	ldr r0, [r2, #4]
_08024FD2:
	str r0, [r3, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_b2_setParentHighFlag
objc_b2_setParentHighFlag: @ 0x08024FD8
	push {r4, r5, lr}
	ldr r3, [r0, #0x20]
	ldr r5, [r0, #0x40]
	ldr r4, [r5, #0x44]
	lsl r2, r4, #5
	lsr r2, r2, #0x10
	mov r1, #1
	ldrh r3, [r3, #4]
	lsl r1, r3
	orr r2, r1
	ldr r1, =0x0000FFFF
	and r2, r1
	lsl r2, r2, #0xb
	ldr r1, =0xF80007FF
	and r1, r4
	orr r1, r2
	str r1, [r5, #0x44]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global objc_b3_clearParentHighFlag
objc_b3_clearParentHighFlag: @ 0x08025014
	push {r4, r5, lr}
	ldr r4, [r0, #0x20]
	ldr r5, [r0, #0x40]
	ldr r3, [r5, #0x44]
	lsl r2, r3, #5
	lsr r2, r2, #0x10
	mov r1, #1
	ldrh r4, [r4, #4]
	lsl r1, r4
	bic r2, r1
	lsl r2, r2, #0xb
	ldr r1, =0xF80007FF
	and r1, r3
	orr r1, r2
	str r1, [r5, #0x44]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global objc_b4_checkParentHighFlag
objc_b4_checkParentHighFlag: @ 0x08025048
	add r3, r0, #0
	ldr r2, [r3, #0x20]
	ldr r0, [r3, #0x40]
	ldr r0, [r0, #0x44]
	lsl r0, r0, #5
	lsr r0, r0, #0x10
	ldrh r1, [r2, #8]
	asr r0, r1
	mov r1, #1
	and r0, r1
	cmp r0, #0
	beq _08025064
	ldr r0, [r2, #4]
	b _08025068
_08025064:
	ldrb r0, [r2, #2]
	add r0, r2, r0
_08025068:
	str r0, [r3, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_b5_checkNOTParentHighFlag
objc_b5_checkNOTParentHighFlag: @ 0x08025070
	add r3, r0, #0
	ldr r2, [r3, #0x20]
	ldr r0, [r3, #0x40]
	ldr r0, [r0, #0x44]
	lsl r0, r0, #5
	lsr r0, r0, #0x10
	ldrh r1, [r2, #8]
	asr r0, r1
	mov r1, #1
	and r0, r1
	cmp r0, #0
	beq _0802508E
	ldrb r0, [r2, #2]
	add r0, r2, r0
	b _08025090
_0802508E:
	ldr r0, [r2, #4]
_08025090:
	str r0, [r3, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_b6_branchIfChildHighFlag
objc_b6_branchIfChildHighFlag: @ 0x08025098
	add r3, r0, #0
	ldr r2, [r3, #0x20]
	ldr r0, [r3, #0x48]
	ldr r0, [r0, #0x44]
	lsl r0, r0, #5
	lsr r0, r0, #0x10
	ldrh r1, [r2, #8]
	asr r0, r1
	mov r1, #1
	and r0, r1
	cmp r0, #0
	beq _080250B4
	ldr r0, [r2, #4]
	b _080250B8
_080250B4:
	ldrb r0, [r2, #2]
	add r0, r2, r0
_080250B8:
	str r0, [r3, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_b7_branchNotChildHighFlag
objc_b7_branchNotChildHighFlag: @ 0x080250C0
	add r3, r0, #0
	ldr r2, [r3, #0x20]
	ldr r0, [r3, #0x48]
	ldr r0, [r0, #0x44]
	lsl r0, r0, #5
	lsr r0, r0, #0x10
	ldrh r1, [r2, #8]
	asr r0, r1
	mov r1, #1
	and r0, r1
	cmp r0, #0
	beq _080250DE
	ldrb r0, [r2, #2]
	add r0, r2, r0
	b _080250E0
_080250DE:
	ldr r0, [r2, #4]
_080250E0:
	str r0, [r3, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_b8_incStack
objc_b8_incStack: @ 0x080250E8
	add r2, r0, #0
	add r1, r2, #0
	add r1, #0x62
	ldrb r0, [r1]
	cmp r0, #5
	bhi _080250F8
	add r0, #1
	strb r0, [r1]
_080250F8:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_b9_decStack
objc_b9_decStack: @ 0x08025104
	add r2, r0, #0
	add r1, r2, #0
	add r1, #0x62
	ldrb r0, [r1]
	cmp r0, #0
	beq _08025114
	sub r0, #1
	strb r0, [r1]
_08025114:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_ba_phantomCall
objc_ba_phantomCall: @ 0x08025120
	add r2, r0, #0
	add r3, r2, #0
	add r3, #0x62
	ldrb r0, [r3]
	add r1, r0, #0
	cmp r1, #5
	bhi _08025140
	add r0, #1
	strb r0, [r3]
	lsl r1, r1, #2
	add r0, r2, #0
	add r0, #0x24
	add r0, r0, r1
	ldr r1, [r2, #0x20]
	ldr r1, [r1, #4]
	str r1, [r0]
_08025140:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_bb_clearField62
objc_bb_clearField62: @ 0x0802514C
	add r2, r0, #0
	add r2, #0x62
	mov r1, #0
	strb r1, [r2]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_bc
objc_bc: @ 0x08025160
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x14
	add r6, r0, #0
	ldr r0, [r6, #0x20]
	ldrh r0, [r0, #4]
	str r0, [sp, #8]
	ldr r1, [r6, #0x3c]
	ldr r0, [r1, #4]
	asr r0, r0, #8
	ldrh r2, [r1, #0x38]
	add r0, r0, r2
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	ldrh r0, [r1, #0x3c]
	add r0, r7, r0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r8, r0
	ldr r0, [r1, #8]
	asr r0, r0, #8
	ldrh r3, [r1, #0x3a]
	add r0, r0, r3
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	ldrh r0, [r1, #0x3e]
	sub r0, r4, r0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov ip, r0
	mov r1, r8
	sub r0, r1, r7
	lsr r1, r0, #0x1f
	add r0, r0, r1
	asr r0, r0, #1
	add r0, r7, r0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sl, r0
	mov r2, ip
	sub r0, r4, r2
	lsr r1, r0, #0x1f
	add r0, r0, r1
	asr r0, r0, #1
	add r0, ip
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sb, r0
	ldr r0, [r6, #0x48]
	ldr r3, [r0, #0x3c]
	ldr r1, [r3, #4]
	asr r1, r1, #8
	ldrh r0, [r3, #0x38]
	add r1, r1, r0
	mov r2, #0x3c
	ldrsh r0, [r3, r2]
	lsr r2, r0, #0x1f
	add r0, r0, r2
	asr r0, r0, #1
	add r1, r1, r0
	lsl r1, r1, #0x10
	lsr r5, r1, #0x10
	ldr r1, [r3, #8]
	asr r1, r1, #8
	ldrh r0, [r3, #0x3a]
	add r1, r1, r0
	mov r2, #0x3e
	ldrsh r0, [r3, r2]
	lsr r2, r0, #0x1f
	add r0, r0, r2
	asr r0, r0, #1
	sub r1, r1, r0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r3, [r6, #0x20]
	ldrh r3, [r3, #6]
	str r3, [sp, #0xc]
	ldr r0, [r6, #0x20]
	ldrh r0, [r0, #8]
	str r0, [sp, #0x10]
	cmp ip, r1
	bhi _0802521A
	cmp r1, r4
	bhi _0802521A
	mov r0, r8
	cmp r5, sl
	bhi _08025216
	add r0, r7, #0
_08025216:
	add r3, r0, #0
	b _0802523A
_0802521A:
	cmp r7, r5
	bhi _08025226
	cmp r5, r8
	bhi _08025226
	add r3, r5, #0
	b _08025230
_08025226:
	mov r0, r8
	cmp r5, sl
	bhi _0802522E
	add r0, r7, #0
_0802522E:
	add r3, r0, #0
_08025230:
	add r0, r4, #0
	cmp r1, sb
	bhi _08025238
	mov r0, ip
_08025238:
	add r1, r0, #0
_0802523A:
	ldr r0, [r6, #0x3c]
	ldr r2, [r0, #4]
	asr r2, r2, #8
	sub r2, r3, r2
	ldr r3, [sp, #0xc]
	add r2, r2, r3
	lsl r2, r2, #0x10
	asr r2, r2, #0x10
	ldr r3, [r0, #8]
	asr r3, r3, #8
	sub r3, r1, r3
	ldr r0, [sp, #0x10]
	add r3, r3, r0
	lsl r3, r3, #0x10
	asr r3, r3, #0x10
	mov r0, #0
	str r0, [sp]
	str r0, [sp, #4]
	add r0, r6, #0
	ldr r1, [sp, #8]
	bl createChildEntity
	ldr r0, [r6, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r6, #0x20]
	mov r0, #0
	add sp, #0x14
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1

thumb_func_global objc_bd
objc_bd: @ 0x08025280
	push {r4, lr}
	add r4, r0, #0
	bl sub_8021F04
	cmp r0, #0
	beq _08025292
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #4]
	b _08025298
_08025292:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08025298:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_be
objc_be: @ 0x080252A4
	push {r4, lr}
	add r4, r0, #0
	add r0, #0x60
	ldrh r0, [r0]
	bl sub_08025BE8
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_bf
objc_bf: @ 0x080252C0
	push {r4, r5, r6, r7, lr}
	add r6, r0, #0
	ldr r7, [r6, #0x20]
	ldrh r4, [r7, #8]
	add r0, #0x66
	ldrb r5, [r0]
	ldr r3, =byte_814295B
	ldr r2, =0x03003D40
	ldr r0, [r6, #8]
	ldrh r1, [r0, #0x1e]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r2, [r2, #0x11]
	add r0, r0, r2
	add r0, r0, r3
	ldrb r0, [r0]
	mul r0, r4, r0
	mov r1, #0x64
	bl divide
	cmp r5, r0
	bgt _080252F8
	ldr r0, [r7, #4]
	b _080252FC
	.align 2, 0
.pool
_080252F8:
	ldrb r0, [r7, #2]
	add r0, r7, r0
_080252FC:
	str r0, [r6, #0x20]
	mov r0, #0
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_c0
objc_c0: @ 0x08025308
	push {r4, r5, r6, r7, lr}
	add r6, r0, #0
	ldr r7, [r6, #0x20]
	ldrh r4, [r7, #8]
	add r0, #0x66
	ldrb r5, [r0]
	ldr r3, =byte_814295B
	ldr r2, =0x03003D40
	ldr r0, [r6, #8]
	ldrh r1, [r0, #0x1e]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r2, [r2, #0x11]
	add r0, r0, r2
	add r0, r0, r3
	ldrb r0, [r0]
	mul r0, r4, r0
	mov r1, #0x64
	bl divide
	cmp r5, r0
	blt _08025340
	ldr r0, [r7, #4]
	b _08025344
	.align 2, 0
.pool
_08025340:
	ldrb r0, [r7, #2]
	add r0, r7, r0
_08025344:
	str r0, [r6, #0x20]
	mov r0, #0
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_c1_branchID
objc_c1_branchID: @ 0x08025350
	add r2, r0, #0
	add r0, #0x5a
	ldrh r0, [r0]
	cmp r0, #0xf
	bhi _08025360
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _08025366
_08025360:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08025366:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_c2_branchID2
objc_c2_branchID2: @ 0x0802536C
	add r2, r0, #0
	add r0, #0x5a
	ldrh r0, [r0]
	sub r0, #0x10
	cmp r0, #0x11
	bhi _0802537E
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _08025384
_0802537E:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08025384:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_c3_fadeScreen
objc_c3_fadeScreen: @ 0x0802538C
	push {r4, lr}
	add r4, r0, #0
	ldr r2, [r4, #0x20]
	ldrb r0, [r2, #6]
	ldrb r1, [r2, #7]
	ldrh r2, [r2, #4]
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	beq _080253AC
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
_080253AC:
	mov r0, #1
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_c4
objc_c4: @ 0x080253B4
	push {r4, lr}
	add r4, r0, #0
	ldr r2, [r4, #0x20]
	ldrb r0, [r2, #6]
	ldrb r1, [r2, #7]
	ldrh r2, [r2, #4]
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	beq _080253D2
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
_080253D2:
	mov r0, #1
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_c5_setInitPos
objc_c5_setInitPos: @ 0x080253DC
	ldr r3, [r0, #0x3c]
	ldr r2, [r0, #0x20]
	ldrh r1, [r2, #4]
	lsl r1, r1, #8
	str r1, [r3, #4]
	ldrh r1, [r2, #6]
	lsl r1, r1, #8
	str r1, [r3, #8]
	ldrb r1, [r2, #2]
	add r2, r2, r1
	str r2, [r0, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_c6_disablePlayer
objc_c6_disablePlayer: @ 0x080253F8
	ldr r2, =0x03004750
	add r2, #0x23
	ldrb r1, [r2]
	mov r3, #2
	orr r1, r3
	strb r1, [r2]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0
.pool

thumb_func_global objc_c7_enablePlayer
objc_c7_enablePlayer: @ 0x08025414
	ldr r2, =0x03004750
	add r2, #0x23
	ldrb r3, [r2]
	mov r1, #3
	neg r1, r1
	and r1, r3
	strb r1, [r2]
	ldr r1, [r0, #0x20]
	ldrb r2, [r1, #2]
	add r1, r1, r2
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0
.pool

thumb_func_global objc_c8_branchPlayerVulnerable
objc_c8_branchPlayerVulnerable: @ 0x08025434
	add r2, r0, #0
	ldr r0, =0x03004750
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	cmp r0, #0
	blt _0802544C
	ldr r0, [r2, #0x20]
	ldr r0, [r0, #4]
	b _08025452
	.align 2, 0
.pool
_0802544C:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08025452:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr

thumb_func_global objc_c9_SwitchDifficultyCall
objc_c9_SwitchDifficultyCall: @ 0x08025458
	push {lr}
	ldr r1, =0x03003D40
	ldrb r1, [r1, #0x11]
	lsl r1, r1, #0x1a
	mov r2, #0x80
	lsl r2, r2, #0x13
	add r1, r1, r2
	lsr r1, r1, #0x18
	bl script_call
	mov r0, #0
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global objc_ca_SwitchDifficulty
objc_ca_SwitchDifficulty: @ 0x08025478
	ldr r1, =0x03003D40
	ldrb r1, [r1, #0x11]
	lsl r1, r1, #0x1a
	mov r2, #0x80
	lsl r2, r2, #0x13
	add r1, r1, r2
	lsr r1, r1, #0x18
	ldr r2, [r0, #0x20]
	add r2, r2, r1
	ldr r1, [r2]
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0
.pool

thumb_func_global objc_cb_branchDifficulty
objc_cb_branchDifficulty: @ 0x08025498
	add r2, r0, #0
	ldr r1, [r2, #0x20]
	ldr r0, =0x03003D40
	ldrb r0, [r0, #0x11]
	ldrh r3, [r1, #4]
	cmp r0, r3
	bne _080254B0
	ldr r0, [r1, #8]
	b _080254B4
	.align 2, 0
.pool
_080254B0:
	ldrb r0, [r1, #2]
	add r0, r1, r0
_080254B4:
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_cc_loadDifficulty
objc_cc_loadDifficulty: @ 0x080254BC
	add r3, r0, #0
	ldr r0, =0x03003D40
	ldrb r1, [r0, #0x11]
	lsl r1, r1, #0x19
	mov r0, #0x80
	lsl r0, r0, #0x13
	add r1, r1, r0
	lsr r1, r1, #0x18
	add r0, r3, #0
	add r0, #0x62
	ldrb r0, [r0]
	lsl r0, r0, #1
	add r2, r3, #0
	add r2, #0x4c
	add r2, r2, r0
	ldr r0, [r3, #0x20]
	add r0, r0, r1
	ldrh r0, [r0]
	strh r0, [r2]
	ldr r0, [r3, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r3, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0
.pool

thumb_func_global objc_cd_branchLang
objc_cd_branchLang: @ 0x080254F4
	ldr r1, =0x03002080
	add r1, #0x2c
	ldrb r1, [r1]
	lsl r1, r1, #0x1a
	mov r2, #0x80
	lsl r2, r2, #0x13
	add r1, r1, r2
	lsr r1, r1, #0x18
	ldr r2, [r0, #0x20]
	add r2, r2, r1
	ldr r1, [r2]
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0
.pool

thumb_func_global objc_ce
objc_ce: @ 0x08025514
	push {r4, r5, lr}
	add r2, r0, #0
	ldr r0, [r2, #0x48]
	ldr r0, [r0, #0x3c]
	ldr r1, [r2, #0x3c]
	ldr r0, [r0, #4]
	ldr r1, [r1, #4]
	sub r0, r0, r1
	lsl r0, r0, #8
	asr r4, r0, #0x10
	ldr r1, [r2, #0x20]
	mov r3, #8
	ldrsh r0, [r1, r3]
	mov r5, #0xa
	ldrsh r3, [r1, r5]
	cmp r0, r4
	bgt _0802553E
	cmp r4, r3
	bgt _0802553E
	ldr r0, [r1, #4]
	b _08025544
_0802553E:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_08025544:
	str r0, [r2, #0x20]
	mov r0, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_cf
objc_cf: @ 0x08025550
	push {r4, lr}
	add r2, r0, #0
	ldr r0, [r2, #0x48]
	ldr r0, [r0, #0x3c]
	ldr r1, [r2, #0x3c]
	ldr r0, [r0, #8]
	ldr r1, [r1, #8]
	sub r0, r0, r1
	lsl r0, r0, #8
	asr r4, r0, #0x10
	ldr r1, [r2, #0x20]
	ldrh r0, [r1, #8]
	ldrh r3, [r1, #0xa]
	cmp r0, r4
	bgt _08025576
	cmp r4, r3
	bgt _08025576
	ldr r0, [r1, #4]
	b _0802557C
_08025576:
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_0802557C:
	str r0, [r2, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_d0_condSong
objc_d0_condSong: @ 0x08025588
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x48]
	ldr r0, [r0, #0x44]
	lsl r0, r0, #5
	lsr r1, r0, #0x10
	mov r0, #0x80
	lsl r0, r0, #3
	and r0, r1
	cmp r0, #0
	beq _080255A8
	ldr r0, [r4, #0x20]
	ldrh r0, [r0, #4]
	bl playSong
	b _080255C4
_080255A8:
	mov r0, #0x80
	lsl r0, r0, #5
	and r1, r0
	cmp r1, #0
	beq _080255BC
	ldr r0, [r4, #0x20]
	ldrh r0, [r0, #8]
	bl playSong
	b _080255C4
_080255BC:
	ldr r0, [r4, #0x20]
	ldrh r0, [r0, #6]
	bl playSong
_080255C4:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_d2
objc_d2: @ 0x080255D4
	push {r4, r5, lr}
	sub sp, #0x80
	add r5, r0, #0
	mov r0, sp
	add r1, r5, #0
	mov r2, #0x74
	bl memcpy
	add r4, sp, #0x74
	add r0, r4, #0
	mov r1, #0
	mov r2, #0xc
	bl memfill
	ldr r0, [r5, #0x48]
	ldr r0, [r0, #0x44]
	lsl r0, r0, #5
	lsr r1, r0, #0x10
	mov r0, #0x80
	lsl r0, r0, #3
	and r0, r1
	cmp r0, #0
	beq _08025608
	ldr r0, [r5, #0x20]
	ldrh r1, [r0, #4]
	b _0802561C
_08025608:
	mov r0, #0x80
	lsl r0, r0, #5
	and r1, r0
	cmp r1, #0
	beq _08025618
	ldr r0, [r5, #0x20]
	ldrh r1, [r0, #8]
	b _0802561C
_08025618:
	ldr r0, [r5, #0x20]
	ldrh r1, [r0, #6]
_0802561C:
	add r0, sp, #0x78
	strh r1, [r0]
	str r4, [sp, #0x20]
	mov r0, sp
	bl objc_bc
	ldr r0, [r5, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r5, #0x20]
	mov r0, #0
	add sp, #0x80
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_d1_playSongObjId
objc_d1_playSongObjId: @ 0x0802563C
	push {r4, lr}
	add r4, r0, #0
	add r0, #0x5a
	ldrh r0, [r0]
	cmp r0, #0xf
	bhi _08025652
	ldr r0, [r4, #0x20]
	ldrh r0, [r0, #4]
	bl playSong
	b _0802565A
_08025652:
	ldr r0, [r4, #0x20]
	ldrh r0, [r0, #6]
	bl playSong
_0802565A:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_d3
objc_d3: @ 0x0802566C
	push {r4, lr}
	add r4, r0, #0
	ldr r1, [r4, #0x20]
	ldrh r0, [r1, #4]
	ldrh r1, [r1, #6]
	bl sub_08015754
	cmp r0, #0
	beq _08025684
	ldr r0, [r4, #0x20]
	ldr r0, [r0, #8]
	b _0802568A
_08025684:
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
_0802568A:
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_d4
objc_d4: @ 0x08025694
	push {r4, r5, r6, r7, lr}
	add r7, r0, #0
	ldr r0, [r7, #0x20]
	mov r2, #4
	ldrsh r1, [r0, r2]
	add r0, r7, #0
	bl sub_08020220
	ldr r1, [r7, #0x20]
	ldrh r4, [r1, #6]
	ldr r6, [r7, #0x3c]
	ldr r1, [r6, #8]
	sub r0, r0, r1
	lsl r0, r0, #1
	add r1, r4, #0
	bl divide
	add r5, r0, #0
	add r1, r4, #0
	bl divide
	add r4, r0, #0
	add r0, r6, #0
	add r1, r5, #0
	bl setObjVelocity_Y
	ldr r0, [r7, #0x3c]
	neg r4, r4
	add r1, r4, #0
	bl setObjAccelerationY
	ldr r0, [r7, #0x3c]
	add r1, r5, #0
	cmp r1, #0
	bge _080256DC
	neg r1, r1
_080256DC:
	bl setObjMaxVelocityY
	ldr r0, [r7, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r7, #0x20]
	mov r0, #0
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1

thumb_func_global objc_d5
objc_d5: @ 0x080256F0
	push {r4, r5, r6, lr}
	add r6, r0, #0
	ldr r0, [r6, #0x20]
	mov r2, #4
	ldrsh r1, [r0, r2]
	add r0, r6, #0
	bl sub_080201E4
	ldr r2, [r6, #0x20]
	ldr r5, [r6, #0x3c]
	ldr r1, [r5, #4]
	sub r0, r0, r1
	mov r3, #6
	ldrsh r1, [r2, r3]
	bl divide
	add r4, r0, #0
	add r0, r5, #0
	add r1, r4, #0
	bl setObjVelocity_X
	ldr r0, [r6, #0x3c]
	add r1, r4, #0
	cmp r1, #0
	bge _08025724
	neg r1, r1
_08025724:
	bl setObjMaxVelocityX
	ldr r0, [r6, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r6, #0x20]
	mov r0, #0
	pop {r4, r5, r6}
	pop {r1}
	bx r1

thumb_func_global objc_d6
objc_d6: @ 0x08025738
	push {r4, r5, r6, lr}
	add r6, r0, #0
	ldr r0, [r6, #0x20]
	mov r2, #4
	ldrsh r1, [r0, r2]
	add r0, r6, #0
	bl sub_08020220
	ldr r2, [r6, #0x20]
	ldr r5, [r6, #0x3c]
	ldr r1, [r5, #8]
	sub r0, r0, r1
	mov r3, #6
	ldrsh r1, [r2, r3]
	bl divide
	add r4, r0, #0
	add r0, r5, #0
	add r1, r4, #0
	bl setObjVelocity_Y
	ldr r0, [r6, #0x3c]
	add r1, r4, #0
	cmp r1, #0
	bge _0802576C
	neg r1, r1
_0802576C:
	bl setObjMaxVelocityY
	ldr r0, [r6, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r6, #0x20]
	mov r0, #0
	pop {r4, r5, r6}
	pop {r1}
	bx r1

thumb_func_global objc_d7
objc_d7: @ 0x08025780
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	add r7, r0, #0
	ldr r0, [r7, #0x20]
	mov r2, #6
	ldrsh r1, [r0, r2]
	add r0, r7, #0
	bl sub_08020220
	mov sb, r0
	ldr r0, [r7, #0x20]
	mov r2, #8
	ldrsh r1, [r0, r2]
	add r0, r7, #0
	bl sub_08020220
	mov sl, r0
	ldr r0, [r7, #0x20]
	ldrh r0, [r0, #0xa]
	mov r8, r0
	ldr r4, [r7, #0x3c]
	ldr r0, [r4, #8]
	mov r1, sb
	sub r0, r1, r0
	lsl r0, r0, #1
	mov r1, r8
	bl divide
	add r6, r0, #0
	mov r1, r8
	bl divide
	add r5, r0, #0
	add r0, r4, #0
	add r1, r6, #0
	bl setObjVelocity_Y
	ldr r0, [r7, #0x3c]
	neg r1, r5
	bl setObjAccelerationY
	ldr r0, [r7, #0x3c]
	add r1, r6, #0
	cmp r6, #0
	bge _080257E2
	neg r1, r6
_080257E2:
	bl setObjMaxVelocityY
	mov r2, sb
	mov r1, sl
	sub r0, r2, r1
	lsl r0, r0, #1
	add r1, r5, #0
	bl divide
	bl Bios_Sqrt
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	ldr r0, [r7, #0x20]
	mov r2, #4
	ldrsh r1, [r0, r2]
	add r0, r7, #0
	bl sub_080201E4
	ldr r5, [r7, #0x3c]
	ldr r1, [r5, #4]
	sub r0, r0, r1
	add r4, r8
	add r1, r4, #0
	bl divide
	add r6, r0, #0
	add r0, r5, #0
	add r1, r6, #0
	bl setObjVelocity_X
	ldr r0, [r7, #0x3c]
	add r1, r6, #0
	cmp r1, #0
	bge _0802582C
	neg r1, r1
_0802582C:
	bl setObjMaxVelocityX
	ldr r0, [r7, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r7, #0x20]
	mov r0, #0
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1

thumb_func_global objc_d8
objc_d8: @ 0x08025848
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	add r6, r0, #0
	ldr r1, [r6, #0x20]
	ldrh r0, [r1, #4]
	mov r8, r0
	mov r2, #6
	ldrsh r0, [r1, r2]
	lsl r0, r0, #9
	mov r2, #8
	ldrsh r7, [r1, r2]
	add r1, r7, #0
	bl divide
	add r5, r0, #0
	add r1, r7, #0
	bl divide
	add r4, r0, #0
	ldr r0, [r6, #0x3c]
	add r1, r5, #0
	bl setObjVelocity_Y
	ldr r0, [r6, #0x3c]
	neg r4, r4
	add r1, r4, #0
	bl setObjAccelerationY
	ldr r0, [r6, #0x3c]
	add r1, r5, #0
	cmp r5, #0
	bge _0802588C
	neg r1, r5
_0802588C:
	bl setObjMaxVelocityY
	ldr r1, =dirLutMaybe
	add r0, r6, #0
	add r0, #0x67
	ldrb r0, [r0]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1c
	add r0, r0, r1
	mov r1, #0
	ldrsb r1, [r0, r1]
	mov r2, r8
	lsl r0, r2, #0x10
	asr r0, r0, #8
	mul r0, r1, r0
	lsl r1, r7, #1
	bl divide
	add r5, r0, #0
	ldr r0, [r6, #0x3c]
	add r1, r5, #0
	bl setObjVelocity_X
	ldr r0, [r6, #0x3c]
	add r1, r5, #0
	cmp r1, #0
	bge _080258C4
	neg r1, r1
_080258C4:
	bl setObjMaxVelocityX
	ldr r0, [r6, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r6, #0x20]
	mov r0, #0
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global objc_d9_BlinkObj
objc_d9_BlinkObj: @ 0x080258E0
	push {r4, lr}
	add r4, r0, #0
	bl setObjCodeBlink
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objc_da
objc_da: @ 0x080258F8
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x20]
	ldr r1, [r0, #4]
	add r0, r4, #0
	bl sub_08020138
	ldr r0, [r4, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r4, #0x20]
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objc_db_branchIfAnimation
objc_db_branchIfAnimation: @ 0x08025918
	add r3, r0, #0
	ldr r2, [r3, #0x20]
	ldrh r0, [r2, #4]
	add r1, r3, #0
	add r1, #0x65
	ldrb r1, [r1]
	cmp r0, r1
	bne _0802592C
	ldr r0, [r2, #8]
	b _08025930
_0802592C:
	ldrb r0, [r2, #2]
	add r0, r2, r0
_08025930:
	str r0, [r3, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_dc_BranchIfParentId
objc_dc_BranchIfParentId: @ 0x08025938
	add r3, r0, #0
	ldr r2, [r3, #0x20]
	ldrh r1, [r2, #4]
	ldr r0, [r3, #0x40]
	add r0, #0x5a
	ldrh r0, [r0]
	cmp r1, r0
	bne _0802594C
	ldr r0, [r2, #8]
	b _08025950
_0802594C:
	ldrb r0, [r2, #2]
	add r0, r2, r0
_08025950:
	str r0, [r3, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_dd_branchIfObjID
objc_dd_branchIfObjID: @ 0x08025958
	add r3, r0, #0
	ldr r2, [r3, #0x20]
	ldrh r0, [r2, #4]
	add r1, r3, #0
	add r1, #0x5a
	ldrh r1, [r1]
	cmp r0, r1
	bne _0802596C
	ldr r0, [r2, #8]
	b _08025970
_0802596C:
	ldrb r0, [r2, #2]
	add r0, r2, r0
_08025970:
	str r0, [r3, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_de_goto
objc_de_goto: @ 0x08025978
	ldr r1, [r0, #0x20]
	ldr r1, [r1, #4]
	str r1, [r0, #0x20]
	mov r0, #0
	bx lr
	.align 2, 0

thumb_func_global objc_df_clearLateFields
objc_df_clearLateFields: @ 0x08025984
	add r2, r0, #0
	add r0, #0x6e
	mov r1, #0
	strh r1, [r0]
	add r0, #2
	strh r1, [r0]
	sub r0, #6
	strh r1, [r0]
	add r0, #2
	strh r1, [r0]
	ldr r0, [r2, #0x20]
	ldrb r1, [r0, #2]
	add r0, r0, r1
	str r0, [r2, #0x20]
	mov r0, #0
	bx lr

thumb_func_global loadMapObjects
loadMapObjects: @ 0x080259A4
	push {r4, r5, r6, lr}
	sub sp, #4
	lsl r0, r0, #0x10
	lsl r1, r1, #0x10
	lsl r2, r2, #0x10
	ldr r4, =0x03003D40
	mov r3, #0x80
	lsl r3, r3, #0x11
	ldrb r4, [r4, #0x11]
	lsl r3, r4
	lsr r5, r3, #0x18
	ldr r3, =levelEntities
	lsr r0, r0, #0xe
	add r0, r0, r3
	ldr r0, [r0]
	lsr r1, r1, #0xe
	add r1, r1, r0
	ldr r0, [r1]
	lsr r2, r2, #0xe
	add r2, r2, r0
	ldr r4, [r2]
	bl clearObjects_maybe
	bl loadSpecialSpritePals
	ldrh r0, [r4]
	ldr r1, =0x0000FFFF
	cmp r0, r1
	beq _08025A86
	add r6, r1, #0
_080259E0:
	ldrb r1, [r4, #0x12]
	cmp r1, #0
	beq _080259EE
	add r0, r5, #0
	and r0, r1
	cmp r0, #0
	beq _08025A7E
_080259EE:
	ldrh r0, [r4]
	cmp r0, #2
	beq _08025A54
	cmp r0, #2
	bgt _08025A0C
	cmp r0, #1
	beq _08025A16
	b _08025A7E
	.align 2, 0
.pool
_08025A0C:
	cmp r0, #3
	beq _08025A20
	cmp r0, #4
	beq _08025A36
	b _08025A7E
_08025A16:
	ldr r1, =mapEntLoadFunctions
	b _08025A38
	.align 2, 0
.pool
_08025A20:
	ldrh r0, [r4, #0x10]
	bl getEntDeletionFlag
	cmp r0, #0
	bne _08025A7E
	ldrh r0, [r4, #2]
	ldrh r1, [r4, #4]
	ldrh r2, [r4, #6]
	bl createObj_801ED54
	b _08025A6E
_08025A36:
	ldr r1, =func_8025B44
_08025A38:
	ldr r0, =0x03003D40
	ldrb r0, [r0, #0x11]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r1, [r0]
	add r0, r4, #0
	bl call_r1
	b _08025A7E
	.align 2, 0
.pool
_08025A54:
	ldrh r0, [r4, #0x10]
	bl getEntDeletionFlag
	add r3, r0, #0
	cmp r3, #0
	bne _08025A7E
	ldrh r0, [r4, #2]
	ldrh r1, [r4, #4]
	ldrh r2, [r4, #6]
	str r3, [sp]
	mov r3, #0
	bl createEntWithVel
_08025A6E:
	add r2, r0, #0
	ldrh r1, [r4, #0x10]
	add r0, #0x60
	strh r1, [r0]
	ldrh r0, [r4, #8]
	strh r0, [r2, #0x18]
	ldrh r0, [r4, #0xa]
	strh r0, [r2, #0x1a]
_08025A7E:
	add r4, #0x14
	ldrh r0, [r4]
	cmp r0, r6
	bne _080259E0
_08025A86:
	bl sub_08025D58
	add sp, #4
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global loadMapOBJ_mayb
loadMapOBJ_mayb: @ 0x08025A94
	push {r4, r5, lr}
	sub sp, #4
	add r5, r0, #0
	ldrh r0, [r5, #0x10]
	bl getEntDeletionFlag
	cmp r0, #0
	bne _08025AE0
	ldr r0, =0x03004EE0
	ldrh r2, [r5, #2]
	ldrh r3, [r5, #4]
	ldrh r1, [r5, #6]
	str r1, [sp]
	mov r1, #1
	bl createEntity
	add r4, r0, #0
	cmp r4, #0
	beq _08025AE0
	ldrh r0, [r5, #0xe]
	add r1, r4, #0
	add r1, #0x5e
	strh r0, [r1]
	ldrh r0, [r5, #8]
	strh r0, [r4, #0x18]
	ldrh r0, [r5, #0xa]
	strh r0, [r4, #0x1a]
	ldrh r1, [r5, #0x10]
	add r0, r4, #0
	add r0, #0x60
	strh r1, [r0]
	ldrb r1, [r5, #0xc]
	add r0, r4, #0
	bl setObjDir_animFrame
	add r0, r4, #0
	bl runEntityCallbacks
_08025AE0:
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

	thumb_func_global sub_8025AEC
sub_8025AEC: @ 0x08025AEC
	push {r4, r5, lr}
	sub sp, #4
	add r5, r0, #0
	ldr r0, =0x03004EE0
	ldrh r2, [r5, #2]
	ldrh r3, [r5, #4]
	ldrh r1, [r5, #6]
	str r1, [sp]
	mov r1, #1
	bl createEntity
	add r4, r0, #0
	cmp r4, #0
	beq _08025B38
	ldrh r0, [r5, #0x10]
	bl getEntDeletionFlag
	cmp r0, #0
	bne _08025B1A
	ldrh r0, [r5, #0xe]
	add r1, r4, #0
	add r1, #0x5e
	strh r0, [r1]
_08025B1A:
	ldrh r0, [r5, #8]
	strh r0, [r4, #0x18]
	ldrh r0, [r5, #0xa]
	strh r0, [r4, #0x1a]
	ldrh r1, [r5, #0x10]
	add r0, r4, #0
	add r0, #0x60
	strh r1, [r0]
	ldrb r1, [r5, #0xc]
	add r0, r4, #0
	bl setObjDir_animFrame
	add r0, r4, #0
	bl runEntityCallbacks
_08025B38:
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_8025B44
sub_8025B44: @ 0x08025B44
	push {r4, lr}
	add r4, r0, #0
	ldrh r0, [r4, #0x10]
	bl getEntDeletionFlag
	cmp r0, #0
	bne _08025B8C
	ldrh r0, [r4, #2]
	ldrh r1, [r4, #4]
	ldrh r2, [r4, #6]
	bl ent_create_801EE30
	add r3, r0, #0
	cmp r3, #0
	beq _08025B8C
	ldrh r1, [r4, #0x10]
	add r0, #0x60
	strh r1, [r0]
	ldrh r0, [r4, #8]
	strh r0, [r3, #0x18]
	ldrh r0, [r4, #0xa]
	strh r0, [r3, #0x1a]
	ldrh r0, [r4, #0xe]
	add r1, r3, #0
	add r1, #0x5e
	strh r0, [r1]
	ldrb r0, [r4, #0xc]
	add r3, #0x67
	mov r1, #0xf
	and r1, r0
	ldrb r2, [r3]
	mov r0, #0x10
	neg r0, r0
	and r0, r2
	orr r0, r1
	strb r0, [r3]
_08025B8C:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
	
	
	
	
	thumb_func_global sub_8025B94
sub_8025B94: @ 0x08025B94
	push {r4, lr}
	add r4, r0, #0
	ldrh r0, [r4, #0x10]
	bl getEntDeletionFlag
	cmp r0, #0
	beq _08025BA8
	ldrh r0, [r4, #2]
	cmp r0, #0x85
	bne _08025BE2
_08025BA8:
	ldrh r0, [r4, #2]
	ldrh r1, [r4, #4]
	ldrh r2, [r4, #6]
	bl ent_create_801EE30
	add r3, r0, #0
	cmp r3, #0
	beq _08025BE2
	ldrh r1, [r4, #0x10]
	add r0, #0x60
	strh r1, [r0]
	ldrh r0, [r4, #8]
	strh r0, [r3, #0x18]
	ldrh r0, [r4, #0xa]
	strh r0, [r3, #0x1a]
	ldrh r0, [r4, #0xe]
	add r1, r3, #0
	add r1, #0x5e
	strh r0, [r1]
	ldrb r0, [r4, #0xc]
	add r3, #0x67
	mov r1, #0xf
	and r1, r0
	ldrb r2, [r3]
	mov r0, #0x10
	neg r0, r0
	and r0, r2
	orr r0, r1
	strb r0, [r3]
_08025BE2:
	pop {r4}
	pop {r0}
	bx r0

thumb_func_global sub_08025BE8
sub_08025BE8: @ 0x08025BE8
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #4
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	ldr r1, =levelEntities
	ldr r2, =0x03003D40
	ldrb r0, [r2, #1]
	lsl r0, r0, #2
	add r0, r0, r1
	ldrb r1, [r2, #2]
	ldr r0, [r0]
	lsl r1, r1, #2
	add r1, r1, r0
	ldrb r0, [r2, #3]
	ldr r1, [r1]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r5, [r0]
	mov r0, #0x80
	lsl r0, r0, #0x11
	ldrb r2, [r2, #0x11]
	lsl r0, r2
	lsr r6, r0, #0x18
	ldrh r0, [r5]
	ldr r1, =0x0000FFFF
	cmp r0, r1
	beq _08025C7E
	mov r8, r1
_08025C24:
	ldrh r0, [r5, #0x10]
	cmp r0, r7
	bne _08025C76
	ldrb r1, [r5, #0x12]
	cmp r1, #0
	beq _08025C38
	add r0, r6, #0
	and r0, r1
	cmp r0, #0
	beq _08025C76
_08025C38:
	ldrh r0, [r5]
	cmp r0, #9
	bls _08025C76
	ldrh r0, [r5, #2]
	ldrh r1, [r5, #4]
	ldrh r2, [r5, #6]
	ldrb r3, [r5, #0xc]
	ldrh r4, [r5, #0x10]
	str r4, [sp]
	bl createObjWithDir_field60
	add r4, r0, #0
	cmp r4, #0
	beq _08025C76
	ldrh r0, [r5, #0x10]
	bl getEntDeletionFlag
	cmp r0, #0
	bne _08025C66
	ldrh r0, [r5, #0xe]
	add r1, r4, #0
	add r1, #0x5e
	strh r0, [r1]
_08025C66:
	ldrh r1, [r5, #0x10]
	add r0, r4, #0
	add r0, #0x60
	strh r1, [r0]
	ldrh r0, [r5, #8]
	strh r0, [r4, #0x18]
	ldrh r0, [r5, #0xa]
	strh r0, [r4, #0x1a]
_08025C76:
	add r5, #0x14
	ldrh r0, [r5]
	cmp r0, r8
	bne _08025C24
_08025C7E:
	add sp, #4
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool


thumb_func_global ClearIWRAMRegions
ClearIWRAMRegions: @ 0x08025C98
	push {r4, lr}
	sub sp, #4
	mov r0, sp
	mov r4, #0
	strh r4, [r0]
	ldr r1, =0x030005A0
	ldr r2, =0x01000168
	bl Bios_memcpy
	mov r0, sp
	add r0, #2
	strh r4, [r0]
	ldr r1, =0x03000580
	ldr r2, =0x0100000C
	bl Bios_memcpy
	add sp, #4
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global findItemStorage
findItemStorage: @ 0x08025CD0
	ldr r0, =0x03003D40
	ldrb r1, [r0, #3]
	lsl r0, r1, #2
	add r0, r0, r1
	lsl r0, r0, #4
	ldr r1, =0x030005A0
	add r2, r0, r1
	mov r1, #0
_08025CE0:
	ldrh r0, [r2]
	cmp r0, #0
	bne _08025CF4
	add r0, r1, #0
	b _08025D04
	.align 2, 0
.pool
_08025CF4:
	add r0, r1, #1
	lsl r0, r0, #0x18
	lsr r1, r0, #0x18
	add r2, #8
	cmp r1, #9
	bls _08025CE0
	mov r0, #1
	neg r0, r0
_08025D04:
	bx lr
	.align 2, 0

thumb_func_global storeRoomItem
storeRoomItem: @ 0x08025D08
	push {r4, r5, lr}
	ldr r4, =0x03003D40
	ldrb r5, [r4, #3]
	lsl r4, r5, #2
	add r4, r4, r5
	lsl r4, r4, #4
	lsl r0, r0, #0x10
	asr r0, r0, #0xd
	ldr r5, =0x030005A0
	add r0, r0, r5
	add r4, r4, r0
	strh r1, [r4]
	strh r2, [r4, #2]
	strh r3, [r4, #4]
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08025D34
sub_08025D34: @ 0x08025D34
	ldr r1, =0x03003D40
	ldrb r2, [r1, #3]
	lsl r1, r2, #2
	add r1, r1, r2
	lsl r1, r1, #4
	lsl r0, r0, #0x10
	asr r0, r0, #0xd
	ldr r2, =0x030005A0
	add r0, r0, r2
	add r1, r1, r0
	mov r0, #0
	strh r0, [r1]
	bx lr
	.align 2, 0
.pool

thumb_func_global sub_08025D58
sub_08025D58: @ 0x08025D58
	push {r4, r5, lr}
	ldr r0, =0x03003D40
	ldrb r1, [r0, #3]
	lsl r0, r1, #2
	add r0, r0, r1
	lsl r0, r0, #4
	ldr r1, =0x030005A0
	add r4, r0, r1
	mov r1, #0
_08025D6A:
	ldrh r0, [r4]
	add r5, r1, #1
	cmp r0, #0
	beq _08025D7E
	ldrh r1, [r4, #2]
	ldrh r2, [r4, #4]
	bl createObj_801ED54
	add r0, #0x5e
	strh r5, [r0]
_08025D7E:
	lsl r0, r5, #0x18
	lsr r1, r0, #0x18
	add r4, #8
	cmp r1, #9
	bls _08025D6A
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global setEntDeletionFlag
setEntDeletionFlag: @ 0x08025D98
	lsl r0, r0, #0x10
	ldr r1, =0x03000580
	lsr r3, r0, #0x15
	lsl r3, r3, #2
	add r3, r3, r1
	mov r1, #0xf8
	lsl r1, r1, #0xd
	and r1, r0
	lsr r1, r1, #0x10
	mov r2, #1
	lsl r2, r1
	ldr r0, [r3]
	orr r0, r2
	str r0, [r3]
	bx lr
	.align 2, 0
.pool


	thumb_func_global clearEntDeletionFlag
clearEntDeletionFlag: @ 0x08025DBC
	lsl r0, r0, #0x10
	ldr r1, =0x03000580
	lsr r3, r0, #0x15
	lsl r3, r3, #2
	add r3, r3, r1
	mov r1, #0xf8
	lsl r1, r1, #0xd
	and r1, r0
	lsr r1, r1, #0x10
	mov r2, #1
	lsl r2, r1
	ldr r0, [r3]
	bic r0, r2
	str r0, [r3]
	bx lr
	.align 2, 0
.pool
	
	
thumb_func_global getEntDeletionFlag
getEntDeletionFlag: @ 0x08025DE0
	lsl r0, r0, #0x10
	ldr r1, =0x03000580
	lsr r3, r0, #0x15
	lsl r3, r3, #2
	add r3, r3, r1
	mov r1, #0xf8
	lsl r1, r1, #0xd
	and r1, r0
	lsr r1, r1, #0x10
	mov r2, #1
	lsl r2, r1
	ldr r0, [r3]
	and r0, r2
	bx lr
	.align 2, 0
.pool

@0x03003D40 struct
thumb_func_global copyUnknown
copyUnknown: @ 0x08025E00
	push {r4, lr}
	sub sp, #4
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r4, =0x03003D40
	ldr r2, =0x01000012
	mov r0, sp
	add r1, r4, #0
	bl Bios_memcpy
	mov r2, #0
	add r4, #0xa
	ldr r3, =0x03003C05
_08025E1C:
	add r1, r2, r4
	add r0, r2, r3
	ldrb r0, [r0]
	strb r0, [r1]
	add r0, r2, #1
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	cmp r2, #5
	bls _08025E1C
	add sp, #4
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global sub_8025E44
sub_8025E44: @ 0x08025E44
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r0, =(cb_licensedByNintendo+1)
	bl setCurrentTaskFunc
	pop {r0}
	bx r0
	.align 2, 0
.pool


thumb_func_global cb_startLevelLoad_2
cb_startLevelLoad_2: @ 0x08025E5C
	push {lr}
	mov r0, #2
	bl endPriorityOrHigherTask
	ldr r0, =(cb_startLevelLoad+1)
	bl setCurrentTaskFunc
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
	thumb_func_global sub_8025E74
	sub_8025E74:
	push {lr}
	ldr r0, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r1, r2, #0
	strh r1, [r0]
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	bl sub_80125BC
	bl stopSoundTracks
	ldr r3, =0x03003D40
	ldrb r0, [r3, #0x10]
	cmp r0, #0
	beq _08025EE0
	add r2, r3, #0
	add r2, #0xa
	ldrb r0, [r3, #1]
	add r2, r2, r0
	mov r0, #1
	ldrb r1, [r3, #2]
	lsl r0, r1
	ldrb r1, [r2]
	orr r0, r1
	strb r0, [r2]
	ldrb r0, [r3]
	cmp r0, #2
	beq _08025EE6
	ldrb r1, [r3, #2]
	add r1, #1
	ldr r2, =stageCount
	ldrb r0, [r3, #1]
	lsl r0, r0, #1
	add r0, r0, r2
	ldrh r0, [r0]
	sub r0, #1
	cmp r1, r0
	ble _08025F0A
	ldr r0, =(sub_80281FC+1)
	bl setCurrentTaskFunc
	b _08025F22
	.align 2, 0
.pool
_08025EE0:
	ldrb r0, [r3]
	cmp r0, #2
	bne _08025EF4
_08025EE6:
	ldr r0, =(loadTimeTrialLevelCompletionScreen+1)
	bl setCurrentTaskFunc
	b _08025F22
	.align 2, 0
.pool
_08025EF4:
	ldrb r0, [r3, #1]
	cmp r0, #5
	beq _08025F0A
	ldr r0, =0x03003C00
	add r0, #0xce
	ldrh r1, [r0]
	mov r0, #0x64
	mul r1, r0, r1
	ldr r0, [r3, #0x1c]
	cmp r0, r1
	bls _08025F1C
_08025F0A:
	ldr r0, =(cb_saving+1)
	bl setCurrentTaskFunc
	b _08025F22
	.align 2, 0
.pool
_08025F1C:
	ldr r0, =(loadGameOverScreen+1)
	bl setCurrentTaskFunc
_08025F22:
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global cb_moveToNextStage
cb_moveToNextStage: @ 0x08025F2C
	push {r4, r5, lr}
	bl ClearIWRAMRegions
	bl resetKeys
	ldr r2, =0x03003D40
	mov r0, #0
	strb r0, [r2, #3]
	mov r4, #0
	strh r0, [r2, #6]
	strb r4, [r2, #4]
	ldrb r5, [r2, #1]
	cmp r5, #5
	bne _08025F6C
	ldrb r0, [r2, #0x10]
	cmp r0, #0
	beq _08025F60
	ldr r0, =(startGameEnding+1)
	bl setCurrentTaskFunc
	b _08025FEE
	.align 2, 0
.pool
_08025F60:
	ldr r0, =(startGameEndingBad+1)
	bl setCurrentTaskFunc
	b _08025FEE
	.align 2, 0
.pool
_08025F6C:
	strb r4, [r2, #0x10]
	ldrb r0, [r2, #2]
	add r3, r0, #1
	ldr r1, =stageCount
	ldrb r0, [r2, #1]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	sub r0, #1
	cmp r3, r0
	ble _08025FC4
	strb r4, [r2, #2]
	cmp r5, #4
	bne _08025F94
	mov r0, #5
	strb r0, [r2, #1]
	b _08025FD2
	.align 2, 0
.pool
_08025F94:
	ldrb r0, [r2, #0x11]
	cmp r0, #0
	bne _08025FB8
	ldr r0, =0x03003C00
	ldrb r0, [r0]
	lsl r0, r0, #0x1a
	lsr r0, r0, #0x1a
	cmp r0, #7
	bne _08025FB8
	ldr r0, =(cb_easymodeCredits+1)
	bl setCurrentTaskFunc
	b _08025FEE
	.align 2, 0
.pool
_08025FB8:
	ldr r0, =(cb_loadLevelSelect+1)
	bl setCurrentTaskFunc
	b _08025FEE
	.align 2, 0
.pool
_08025FC4:
	strb r3, [r2, #2]
	ldr r0, [r2]
	ldr r1, =0x00FFFF00
	and r0, r1
	ldr r1, =0x00020200
	cmp r0, r1
	bne _08025FE8
_08025FD2:
	ldr r0, =(cb_loadLevelIntroScroll+1)
	bl setCurrentTaskFunc
	b _08025FEE
	.align 2, 0
.pool
_08025FE8:
	ldr r0, =(cb_startLevelLoad_2+1)
	bl setCurrentTaskFunc
_08025FEE:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global startGameEnding
startGameEnding: @ 0x08025FF8
	push {lr}
	ldr r0, =(cb_goodEnding+1)
	bl setCurrentTaskFunc
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global startGameEndingBad
startGameEndingBad: @ 0x08026008
	push {lr}
	ldr r0, =(cb_badEnding+1)
	bl setCurrentTaskFunc
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
	
	
	
		thumb_func_global sub_8026018
sub_8026018: @ 0x08026018
	push {r4, lr}
	bl sub_801CAC4
	ldr r4, =0x03003D40
	ldrb r0, [r4, #0x10]
	cmp r0, #0
	bne _08026054
	bl getPlayerHealth
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0802604C
	bl doorEnterVisibility
	ldr r0, =0x03003E20
	ldr r0, [r0]
	ldr r1, =(sub_8026178+1)
	bl setTaskFunc
	b _080260F4
	.align 2, 0
.pool
_0802604C:
	ldrb r0, [r4, #0x10]
	add r1, r4, #0
	cmp r0, #0
	beq _080260BC
_08026054:
	ldr r1, =0x03003D40
	ldrb r3, [r1, #2]
	ldr r2, =stageCount
	ldrb r4, [r1, #1]
	lsl r0, r4, #1
	add r0, r0, r2
	ldrh r0, [r0]
	sub r0, #1
	cmp r3, r0
	bne _080260BC
	cmp r4, #4
	bne _08026094
	ldrb r0, [r1]
	cmp r0, #2
	beq _08026094
	ldr r1, =0x03002080
	mov r0, #0
	strh r0, [r1, #0x3a]
	ldr r0, =0x03003E20
	ldr r0, [r0]
	ldr r1, =(sub_8026108+1)
	b _080260A2
	.align 2, 0
.pool
_08026094:
	ldr r0, =0x03002080
	add r0, #0x2d
	mov r1, #0
	strb r1, [r0]
	ldr r0, =0x03003E20
	ldr r0, [r0]
	ldr r1, =(sub_80261CC+1)
_080260A2:
	bl setTaskFunc
	mov r0, #3
	bl sub_8016A4C
	b _080260F4
	.align 2, 0
.pool
_080260BC:
	ldrb r0, [r1, #1]
	cmp r0, #4
	bhi _080260D8
	ldr r0, =0x03003E20
	ldr r0, [r0]
	ldr r1, =(sub_80263E0+1)
	bl setTaskFunc
	b _080260EA
	.align 2, 0
.pool
_080260D8:
	ldr r0, =0x03002080
	add r0, #0x2d
	mov r1, #0
	strb r1, [r0]
	ldr r0, =0x03003E20
	ldr r0, [r0]
	ldr r1, =(sub_8026474+1)
	bl setTaskFunc
_080260EA:
	bl sub_8010618
	mov r0, #3
	bl sub_8016A4C
_080260F4:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_8026108
sub_8026108: @ 0x08026108
	push {lr}
	ldr r1, =0x03002080
	ldrh r0, [r1, #0x3a]
	add r0, #1
	strh r0, [r1, #0x3a]
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #0x1e
	ble _08026128
	mov r0, #0
	strh r0, [r1, #0x3a]
	bl sub_8015630
	ldr r0, =(sub_8026140+1)
	bl setCurrentTaskFunc
_08026128:
	mov r0, #3
	bl sub_8016A4C
	bl sub_8015590
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_8026140
sub_8026140: @ 0x08026140
	push {lr}
	ldr r1, =0x03002080
	ldrh r0, [r1, #0x3a]
	add r0, #1
	strh r0, [r1, #0x3a]
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #0xf0
	ble _08026160
	mov r0, #0
	strh r0, [r1, #0x3a]
	add r1, #0x2d
	strb r0, [r1]
	ldr r0, =(sub_80261CC+1)
	bl setCurrentTaskFunc
_08026160:
	mov r0, #3
	bl sub_8016A4C
	bl sub_8015590
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_8026178
sub_8026178: @ 0x08026178
	push {lr}
	bl setClosingDoorPos
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _080261C4
	ldr r0, =0x03003D40
	ldrb r0, [r0, #6]
	cmp r0, #8
	bne _080261B4
	bl stopSoundTracks
	mov r0, #5
	bl playSong
	ldr r0, =0x03002080
	add r0, #0x2d
	mov r1, #0
	strb r1, [r0]
	ldr r0, =(sub_80261CC+1)
	bl setCurrentTaskFunc
	b _080261BE
	.align 2, 0
.pool
_080261B4:
	ldr r0, =(sub_80263E0+1)
	bl setCurrentTaskFunc
	bl sub_8010618
_080261BE:
	mov r0, #3
	bl sub_8016A4C
_080261C4:
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_80261CC
sub_80261CC: @ 0x080261CC
	push {r4, r5, r6, lr}
	ldr r2, =musicPlayer_1
	ldr r0, =m4a_songs
	ldrh r1, [r0, #0x2c]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r0, [r0]
	ldr r1, [r0, #4]
	ldrh r0, [r0, #4]
	cmp r0, #0
	beq _080261EC
	cmp r1, #0
	blt _080261EC
	b _0802633A
_080261EC:
	ldr r4, =0x03003D40
	ldrb r0, [r4]
	cmp r0, #2
	bne _080261F6
	b _08026334
_080261F6:
	mov r6, #0
	bl getHealthBarValue
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0x50
	beq _08026216
	ldrb r2, [r4, #2]
	ldr r1, =stageCount
	ldrb r0, [r4, #1]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	sub r0, #1
	cmp r2, r0
	bne _08026234
_08026216:
	ldr r0, =0x0000010F
	bl playSong
	b _0802626A
	.align 2, 0
.pool
_08026234:
	bl getByte3001F8E
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _08026254
	ldr r0, =unk_814d2d2
	ldrb r1, [r4, #0x11]
	lsl r1, r1, #1
	add r1, r1, r0
	ldrh r0, [r1]
	bl modifyPlayerHealth
	b _08026262
	.align 2, 0
.pool
_08026254:
	ldr r0, =word_814D2CC
	ldrb r1, [r4, #0x11]
	lsl r1, r1, #1
	add r1, r1, r0
	ldrh r0, [r1]
	bl modifyPlayerHealth
_08026262:
	mov r0, #0x83
	lsl r0, r0, #1
	bl playSong
_0802626A:
	ldr r4, =0x03003D40
	ldrb r2, [r4, #2]
	ldr r1, =stageCount
	ldrb r5, [r4, #1]
	lsl r0, r5, #1
	add r0, r0, r1
	ldrh r0, [r0]
	sub r0, #1
	cmp r2, r0
	bne _080262A4
	ldr r3, =dword_814D2D8
	lsl r1, r5, #2
	ldrb r2, [r4, #0x11]
	lsl r0, r2, #1
	add r0, r0, r2
	lsl r0, r0, #3
	add r1, r1, r0
	add r1, r1, r3
	ldr r0, [r1]
	b _080262AE
	.align 2, 0
.pool
_080262A4:
	ldr r0, =unk_814D320
	ldrb r1, [r4, #0x11]
	lsl r1, r1, #1
	add r1, r1, r0
	ldrh r0, [r1]
_080262AE:
	add r6, r6, r0
	ldr r3, =hostageCounts
	ldr r4, =0x03003D40
	ldrb r1, [r4, #2]
	lsl r1, r1, #1
	ldrb r0, [r4, #1]
	lsl r0, r0, #3
	add r1, r1, r0
	ldrb r2, [r4, #0x11]
	lsl r0, r2, #1
	add r0, r0, r2
	lsl r0, r0, #4
	add r1, r1, r0
	add r1, r1, r3
	ldrh r0, [r1]
	cmp r0, #0
	beq _080262E6
	bl getByte3001F8E
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _080262E6
	ldr r0, =unk_814d326
	ldrb r1, [r4, #0x11]
	lsl r1, r1, #1
	add r1, r1, r0
	ldrh r0, [r1]
	add r6, r6, r0
_080262E6:
	ldr r0, =0x03003D40
	ldrb r0, [r0, #1]
	cmp r0, #5
	bne _08026304
	bl getLaunchSiteTime
	mov r1, #0x3c
	bl Bios_Div
	lsl r1, r0, #5
	sub r1, r1, r0
	lsl r1, r1, #2
	add r1, r1, r0
	lsl r1, r1, #3
	add r6, r6, r1
_08026304:
	ldr r1, =0x03002080
	mov r0, #0
	strh r0, [r1, #0x3c]
	strh r0, [r1, #0x3a]
	add r0, r6, #0
	bl sub_8035044
	ldr r0, =(sub_8026374+1)
	bl setCurrentTaskFunc
	b _0802633A
	.align 2, 0
.pool
_08026334:
	ldr r0, =(sub_80263E0+1)
	bl setCurrentTaskFunc
_0802633A:
	ldr r0, =0x030020AD
	bl sub_8034100
	mov r0, #2
	bl sub_8016A4C
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	cmp r0, #4
	bne _0802635E
	ldrb r1, [r1, #2]
	ldr r0, =stageCount
	ldrh r0, [r0, #8]
	sub r0, #1
	cmp r1, r0
	bne _0802635E
	bl sub_8015590
_0802635E:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_8026374
sub_8026374: @ 0x08026374
	push {r4, lr}
	ldr r4, =0x030020AD
	add r0, r4, #0
	bl sub_8034100
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08026394
	add r1, r4, #0
	sub r1, #0x2d
	mov r0, #0
	strh r0, [r1, #0x3a]
	strh r0, [r1, #0x3c]
	ldr r0, =(sub_80263AC+1)
	bl setCurrentTaskFunc
_08026394:
	mov r0, #2
	bl sub_8016A4C
	bl sub_8015590
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_80263AC
sub_80263AC: @ 0x080263AC
	push {lr}
	ldr r1, =0x03002080
	ldrh r0, [r1, #0x3c]
	add r0, #1
	strh r0, [r1, #0x3c]
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #0x3c
	ble _080263C4
	ldr r0, =(sub_80263E0+1)
	bl setCurrentTaskFunc
_080263C4:
	bl sub_8032150
	bl sub_8015590
	mov r0, #2
	bl sub_8016A4C
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_80263E0
sub_80263E0: @ 0x080263E0
	push {r4, lr}
	ldr r4, =0x03003D40
	ldrb r0, [r4, #6]
	cmp r0, #8
	beq _080263F0
	ldrb r0, [r4, #0x10]
	cmp r0, #0
	beq _08026408
_080263F0:
	mov r0, #3
	mov r1, #0x28
	mov r2, #0x78
	mov r3, #0x50
	bl TransitionToBlack
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _08026418
	ldrb r0, [r4, #0x10]
	cmp r0, #0
	bne _08026448
_08026408:
	ldr r0, =0x03003D40
	ldrb r0, [r0, #6]
	cmp r0, #8
	beq _08026448
	bl sub_8010BE4
	cmp r0, #0
	beq _08026448
_08026418:
	ldr r1, =0x03003D40
	ldrb r0, [r1, #6]
	cmp r0, #8
	beq _08026426
	ldrb r0, [r1, #0x10]
	cmp r0, #0
	beq _0802642A
_08026426:
	bl sub_8010950
_0802642A:
	bl sub_0801025C
	bl sub_801095C
	mov r0, #2
	bl endPriorityOrHigherTask
	ldr r0, =(sub_80264D0+1)
	bl setCurrentTaskFunc
	b _08026466
	.align 2, 0
.pool
_08026448:
	mov r0, #2
	bl sub_8016A4C
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	cmp r0, #4
	bne _08026466
	ldrb r1, [r1, #2]
	ldr r0, =stageCount
	ldrh r0, [r0, #8]
	sub r0, #1
	cmp r1, r0
	bne _08026466
	bl sub_8015590
_08026466:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_8026474
sub_8026474: @ 0x08026474
	push {lr}
	ldr r0, =0x030020AD
	add r1, r0, #0
	add r1, #0xd
	bl sub_8032EB8
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0802648C
	ldr r0, =(sub_80264A0+1)
	bl setCurrentTaskFunc
_0802648C:
	mov r0, #3
	bl sub_8016A4C
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_80264A0
sub_80264A0: @ 0x080264A0
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	beq _080264C0
	mov r0, #2
	bl endPriorityOrHigherTask
	ldr r0, =(sub_80264D0+1)
	bl setCurrentTaskFunc
_080264C0:
	mov r0, #3
	bl sub_8016A4C
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_80264D0
sub_80264D0: @ 0x080264D0
	push {r4, r5, lr}
	bl sub_0801025C
	ldr r4, =0x03003D40
	ldrb r5, [r4, #0x10]
	cmp r5, #0
	bne _08026518
	bl getPlayerHealth
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _0802650C
	strb r5, [r4, #0x10]
	ldr r0, =(sub_8025E74+1)
	bl setCurrentTaskFunc
	add r2, r4, #0
	add r2, #0x20
	ldrb r1, [r2]
	mov r0, #2
	neg r0, r0
	and r0, r1
	mov r1, #2
	orr r0, r1
	strb r0, [r2]
	b _08026534
	.align 2, 0
.pool
_0802650C:
	ldrh r1, [r4, #6]
	ldrb r0, [r4, #6]
	cmp r0, #8
	bne _08026524
	mov r0, #1
	strb r0, [r4, #0x10]
_08026518:
	ldr r0, =(sub_8025E74+1)
	bl setCurrentTaskFunc
	b _08026534
	.align 2, 0
.pool
_08026524:
	strb r1, [r4, #3]
	lsr r0, r1, #8
	strb r0, [r4, #4]
	strb r5, [r4, #0x10]
	strh r5, [r4, #6]
	ldr r0, =(cb_startLevelLoad+1)
	bl setCurrentTaskFunc
_08026534:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool
