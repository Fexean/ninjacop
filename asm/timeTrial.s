.include "asm/macros.inc"

@ completely disassembled

@ Time trial screen


thumb_func_global cb_loadTimeTrialMenu
cb_loadTimeTrialMenu: @ 0x0802BEA4
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #8
	mov r1, #0x82
	lsl r1, r1, #5
	add r0, r1, #0
	ldr r2, =0x03003CD4
	strh r0, [r2]
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r0, =menuBgPal
	mov r2, #0xa0
	lsl r2, r2, #0x13
	mov r1, #1
	bl memcpy_pal
	ldr r0, =menuBgTilesCompressed
	mov r1, #0xc0
	lsl r1, r1, #0x13
	bl Bios_LZ77_16_2
	ldr r0, =menuBgMapCompressed
	ldr r1, =0x0600F000
	mov r3, #0
	mov sb, r3
	str r3, [sp]
	mov r2, #0
	bl LZ77_mapCpy
	ldr r5, =0x03003D00
	ldr r0, =0x00001E02
	strh r0, [r5, #4]
	add r1, sp, #4
	mov r4, #0x88
	lsl r4, r4, #5
	add r0, r4, #0
	strh r0, [r1]
	ldr r4, =0x0600F800
	ldr r2, =0x01000400
	add r0, r1, #0
	add r1, r4, #0
	bl Bios_memcpy
	ldr r0, =HUDPalette
	ldr r2, =0x05000020
	mov r1, #1
	bl memcpy_pal
	ldr r0, =HUDTiles
	ldr r1, =0x06002000
	bl Bios_LZ77_16_2
	ldr r0, =0x00001F03
	strh r0, [r5, #6]
	ldr r7, =0x06010000
	mov r8, r7
	mov r2, #0x80
	lsl r2, r2, #3
	add r0, r4, #0
	mov r1, r8
	bl Bios_memcpy
	ldr r0, =0x030005D8
	mov r1, #0
	bl allocFont
	ldr r0, =0x030005C8
	mov r1, #1
	bl allocFont
	ldr r0, =0x030005D0
	mov r1, #2
	bl allocFont
	bl sub_0802C990
	bl sub_0802CCE8
	bl sub_0802C6A8
	mov r0, #0x18
	bl allocateSpriteTiles
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	ldr r0, =unk_8206178
	lsl r1, r7, #5
	add r1, r8
	bl Bios_LZ77_16_2
	ldr r0, =dword_8206228
	mov r1, #1
	bl LoadSpritePalette
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	ldr r0, =off_8206428
	mov r1, sb
	str r1, [sp]
	mov r1, #0
	add r2, r7, #0
	mov r3, #3
	bl allocSprite
	ldr r4, =0x03000600
	str r0, [r4]
	mov r2, #1
	neg r2, r2
	add r5, r2, #0
	ldr r1, =0x0000FFFF
	strh r1, [r0, #0x10]
	ldr r0, [r4]
	mov r3, sb
	strh r3, [r0, #0x12]
	ldr r0, [r4]
	add r0, #0x22
	strb r6, [r0]
	ldr r1, =0x03000580
	ldrb r0, [r1, #2]
	cmp r0, #0xd
	bhi _0802BFB8
	ldr r0, [r4]
	add r0, #0x23
	ldrb r1, [r0]
	mov r2, #0x10
	orr r1, r2
	strb r1, [r0]
_0802BFB8:
	ldr r0, =off_8206464
	mov r2, sb
	str r2, [sp]
	mov r1, #0
	add r2, r7, #0
	mov r3, #3
	bl allocSprite
	str r0, [r4, #4]
	ldrh r1, [r0, #0x10]
	orr r1, r5
	strh r1, [r0, #0x10]
	ldr r0, [r4, #4]
	mov r3, sb
	strh r3, [r0, #0x12]
	ldr r0, [r4, #4]
	add r0, #0x22
	strb r6, [r0]
	ldr r1, =0x03000580
	ldrb r0, [r1, #2]
	cmp r0, #0xd
	bhi _0802BFF0
	ldr r0, [r4, #4]
	add r0, #0x23
	ldrb r1, [r0]
	mov r2, #0x10
	orr r1, r2
	strb r1, [r0]
_0802BFF0:
	ldr r0, =off_820644C
	mov r2, sb
	str r2, [sp]
	mov r1, #0
	add r2, r7, #0
	mov r3, #3
	bl allocSprite
	ldr r2, =0x030005FC
	str r0, [r2]
	mov r1, #0xa
	strh r1, [r0, #0x10]
	ldr r3, =0x03000580
	ldrb r3, [r3]
	add r1, r5, r3
	strh r1, [r0, #0x12]
	add r0, #0x22
	strb r6, [r0]
	ldr r1, [r2]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	mov r0, #0xc
	bl allocateSpriteTiles
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	ldr r0, =dword_820918C
	mov r1, #1
	bl LoadSpritePalette
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	ldr r0, =buttonTiles
	lsl r1, r7, #5
	add r1, r8
	bl Bios_LZ77_16_2
	ldr r0, =off_82093A4
	mov r4, sb
	str r4, [sp]
	mov r1, #0
	add r2, r7, #0
	mov r3, #2
	bl allocSprite
	ldr r2, =0x0300060C
	str r0, [r2]
	add r0, #0x23
	ldrb r3, [r0]
	mov r4, #0x11
	neg r4, r4
	add r1, r4, #0
	and r1, r3
	strb r1, [r0]
	ldr r0, [r2]
	add r0, #0x22
	strb r6, [r0]
	ldr r1, [r2]
	ldr r5, =0x03002080
	add r5, #0x2c
	ldrb r0, [r5]
	lsl r0, r0, #1
	ldr r2, =unk_8206652
	add r0, r0, r2
	ldrh r0, [r0]
	strh r0, [r1, #0x10]
	mov r3, #0x94
	mov sl, r3
	mov r0, sl
	strh r0, [r1, #0x12]
	ldr r0, =off_82093BC
	mov r1, sb
	str r1, [sp]
	mov r1, #0
	add r2, r7, #0
	mov r3, #2
	bl allocSprite
	ldr r3, =0x03000610
	str r0, [r3]
	add r0, #0x23
	ldrb r2, [r0]
	add r1, r4, #0
	and r1, r2
	strb r1, [r0]
	ldr r0, [r3]
	add r0, #0x22
	strb r6, [r0]
	ldr r1, [r3]
	ldr r2, =dword_8206658
	mov r8, r2
	ldrb r0, [r5]
	lsl r0, r0, #1
	add r0, r8
	ldrh r0, [r0]
	strh r0, [r1, #0x10]
	mov r3, sl
	strh r3, [r1, #0x12]
	ldr r0, =off_820938C
	mov r1, sb
	str r1, [sp]
	mov r1, #0
	add r2, r7, #0
	mov r3, #2
	bl allocSprite
	ldr r2, =0x03000608
	str r0, [r2]
	add r0, #0x23
	ldrb r1, [r0]
	and r4, r1
	strb r4, [r0]
	ldr r0, [r2]
	add r0, #0x22
	strb r6, [r0]
	ldr r1, [r2]
	ldr r4, =aSss_0
	ldrb r0, [r5]
	lsl r0, r0, #1
	add r0, r0, r4
	ldrh r0, [r0]
	strh r0, [r1, #0x10]
	mov r2, sl
	strh r2, [r1, #0x12]
	bl drawSelStr
	ldrb r0, [r5]
	lsl r0, r0, #1
	add r0, r0, r4
	ldrh r0, [r0]
	add r0, #0xd
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x95
	bl positionSelStr
	bl drawOkStr
	ldrb r0, [r5]
	lsl r0, r0, #1
	ldr r3, =unk_8206652
	add r0, r0, r3
	ldrh r0, [r0]
	add r0, #0xd
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x95
	bl positionOkStr
	bl drawBackStr
	ldrb r0, [r5]
	lsl r0, r0, #1
	add r0, r8
	ldrh r0, [r0]
	add r0, #0xd
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x95
	bl positionBackStr
	bl drawSelectStageStr
	ldr r0, =0x0000FF78
	mov r1, #4
	bl positionSelectStageStr
	bl drawLatestTimeStr
	ldr r1, =unk_820665e
	ldrb r0, [r5]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	mov r1, #0x16
	bl positionLatestTimeStr
	bl drawBestTimeStr
	ldr r1, =aNnn
	ldrb r0, [r5]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	mov r1, #0x16
	bl positionBestTimeStr
	bl drawQualifyStr
	ldr r1, =unk_820666A
	ldrb r0, [r5]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	mov r1, #0x16
	bl positionQualifyStr
	bl sub_0802CA40
	ldr r6, =0x03003D7C
	mov r4, #0xfc
	lsl r4, r4, #6
	add r5, r4, #0
	strh r5, [r6]
	ldr r1, =0x03003D8C
	mov r0, #0x3f
	strh r0, [r1]
	ldr r4, =0x03003D90
	ldr r3, =0x000008E8
	strh r3, [r4, #2]
	ldr r7, =0x03000580
	ldrb r0, [r7, #2]
	cmp r0, #0xe
	bls _0802C270
	ldr r1, =0x03003D88
	ldr r0, =0x00001F90
	strh r0, [r1, #2]
	b _0802C2B0
	.align 2, 0
.pool
_0802C270:
	ldr r2, =0x03003D88
	ldr r1, =0x03000580
	ldrb r0, [r1, #2]
	lsl r0, r0, #3
	add r0, #0x20
	mov r7, #0xf8
	lsl r7, r7, #5
	add r1, r7, #0
	orr r0, r1
	mov r1, #0
	orr r0, r1
	strh r0, [r2, #2]
	strh r3, [r4]
	mov r1, #0xff
	and r0, r1
	lsl r0, r0, #8
	ldr r3, =0x03000580
	ldrb r1, [r3, #2]
	lsl r1, r1, #3
	add r1, #0x28
	orr r0, r1
	strh r0, [r2]
	mov r0, #0x30
	orr r0, r5
	strh r0, [r6]
	ldr r4, =0x03003CD4
	ldrh r0, [r4]
	mov r7, #0x80
	lsl r7, r7, #6
	add r1, r7, #0
	orr r0, r1
	strh r0, [r4]
_0802C2B0:
	ldr r2, =0x03003D7C
	ldrh r1, [r2]
	ldr r0, =0x0000FBFF
	and r0, r1
	strh r0, [r2]
	ldr r2, =0x03003D8C
	ldrh r1, [r2]
	ldr r0, =0x0000FFF7
	and r0, r1
	strh r0, [r2]
	ldr r2, =0x03003CD4
	ldrh r0, [r2]
	mov r3, #0x80
	lsl r3, r3, #3
	add r1, r3, #0
	orr r0, r1
	mov r4, #0x80
	lsl r4, r4, #4
	add r1, r4, #0
	orr r0, r1
	mov r7, #0x80
	lsl r7, r7, #7
	add r1, r7, #0
	orr r0, r1
	strh r0, [r2]
	ldr r1, =0x030005E0
	mov r0, #0
	strh r0, [r1]
	ldr r0, =(sub_802CBE4+1)
	mov r1, #0
	bl addCallback
	ldr r0, =(cb_timeTrialMenu+1)
	bl setCurrentTaskFunc
	add sp, #8
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global cb_timeTrialMenu
cb_timeTrialMenu: @ 0x0802C330
	push {r4, lr}
	ldr r1, =timeTrialMenuStates
	ldr r0, =0x030005E0
	ldrh r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl call_r0
	lsl r0, r0, #0x10
	mov r1, #0x80
	lsl r1, r1, #9
	add r0, r0, r1
	asr r0, r0, #0x10
	cmp r0, #5
	bhi _0802C3F2
	lsl r0, r0, #2
	ldr r1, =_0802C368
	add r0, r0, r1
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
.pool
_0802C368: @ jump table
	.4byte _0802C38C @ case 0
	.4byte _0802C3F2 @ case 1
	.4byte _0802C380 @ case 2
	.4byte _0802C3A4 @ case 3
	.4byte _0802C3DC @ case 4
	.4byte _0802C3E8 @ case 5
_0802C380:
	ldr r1, =0x030005E0
	ldrh r0, [r1]
	add r0, #1
	b _0802C3F0
	.align 2, 0
.pool
_0802C38C:
	ldr r0, =0x03003D40
	mov r1, #0
	strb r1, [r0, #0x10]
	ldr r0, =(cb_loadMenu+1)
	bl setCurrentTaskFunc
	b _0802C3F2
	.align 2, 0
.pool
_0802C3A4:
	bl setPlayerStats_andtuff_8031B14
	ldr r1, =0x03003D40
	mov r2, #0
	strb r2, [r1, #0x10]
	ldr r0, =0x030005EC
	ldrh r0, [r0]
	strb r0, [r1, #1]
	ldr r0, =0x030005EA
	ldrh r0, [r0]
	strb r0, [r1, #2]
	strb r2, [r1, #4]
	mov r0, #0
	strh r2, [r1, #6]
	strb r0, [r1, #3]
	ldr r0, =(cb_startLevelLoad_2+1)
	bl setCurrentTaskFunc
	b _0802C3F2
	.align 2, 0
.pool
_0802C3DC:
	ldr r1, =0x030005E0
	mov r0, #4
	b _0802C3F0
	.align 2, 0
.pool
_0802C3E8:
	bl sub_0802CA40
	ldr r1, =0x030005E0
	mov r0, #1
_0802C3F0:
	strh r0, [r1]
_0802C3F2:
	ldr r4, =0x03000600
	ldr r0, [r4]
	bl updateSpriteAnimation
	ldr r0, [r4, #4]
	bl updateSpriteAnimation
	bl sub_0802CE48
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global timetrialState0
timetrialState0: @ 0x0802C414
	push {lr}
	bl sub_0802C43C
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0802C42E
	mov r0, #0
	b _0802C436
_0802C42E:
	mov r0, #2
	bl playSong
	mov r0, #1
_0802C436:
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global sub_0802C43C
sub_0802C43C: @ 0x0802C43C
	push {lr}
	bl sub_0802D2D4
	lsl r0, r0, #0x10
	asr r1, r0, #0x10
	ldr r2, =0xFFFF0000
	add r0, r0, r2
	lsr r0, r0, #0x10
	cmp r0, #0xe6
	bls _0802C468
	add r0, r1, #0
	add r0, #8
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #4
	bl positionSelectStageStr
	mov r0, #0
	b _0802C482
	.align 2, 0
.pool
_0802C468:
	mov r0, #8
	mov r1, #4
	bl positionSelectStageStr
	ldr r0, =0x030005FC
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	mov r0, #1
_0802C482:
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global timetrialState1
timetrialState1: @ 0x0802C48C
	push {r4, lr}
	ldr r2, =0x03002080
	ldrh r1, [r2, #0xc]
	mov r4, #2
	add r0, r4, #0
	and r0, r1
	cmp r0, #0
	beq _0802C4B8
	mov r0, #0xcd
	bl playSong
	ldr r1, =0x030005E8
	mov r2, #1
	neg r2, r2
	add r0, r2, #0
	strh r0, [r1]
	mov r0, #1
	b _0802C516
	.align 2, 0
.pool
_0802C4B8:
	mov r0, #9
	and r0, r1
	cmp r0, #0
	beq _0802C4D4
	mov r0, #0xcc
	bl playSong
	ldr r0, =0x030005E8
	strh r4, [r0]
	mov r0, #1
	b _0802C516
	.align 2, 0
.pool
_0802C4D4:
	ldrh r1, [r2, #0x14]
	mov r0, #0xc0
	and r0, r1
	cmp r0, #0
	beq _0802C504
	mov r0, #0x40
	and r0, r1
	cmp r0, #0
	beq _0802C4EC
	mov r0, #1
	neg r0, r0
	b _0802C4EE
_0802C4EC:
	mov r0, #1
_0802C4EE:
	bl sub_0802CAFC
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0
	beq _0802C504
	mov r0, #0xcb
	bl playSong
	mov r0, #3
	b _0802C516
_0802C504:
	ldr r0, =0x030005FC
	ldr r2, [r0]
	ldr r1, =0x03000580
	ldr r3, =0x0000FFFF
	add r0, r3, #0
	ldrb r1, [r1]
	add r0, r0, r1
	strh r0, [r2, #0x12]
	mov r0, #0
_0802C516:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global timetrialState2
timetrialState2: @ 0x0802C528
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0802C540
	mov r0, #0
	b _0802C542
_0802C540:
	mov r0, #1
_0802C542:
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global timetrialState3
timetrialState3: @ 0x0802C548
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl resetAffineTransformations
	bl clearSprites
	bl clearPalBufs
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r0, =0x030005E8
	mov r1, #0
	ldrsh r0, [r0, r1]
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global timetrialState4
timetrialState4: @ 0x0802C57C
	push {r4, r5, r6, lr}
	mov r5, #0
	ldr r0, =0x03000580
	ldrb r1, [r0, #1]
	mov r3, #0xe
	ldrsb r3, [r0, r3]
	add r1, r1, r3
	add r2, r0, #0
	cmp r1, #0
	bge _0802C596
	ldrb r0, [r2, #2]
	lsl r0, r0, #3
	add r1, r1, r0
_0802C596:
	ldrb r0, [r2, #2]
	lsl r0, r0, #3
	cmp r1, r0
	ble _0802C5A0
	sub r1, r1, r0
_0802C5A0:
	mov r4, #0xa
	ldrsh r0, [r2, r4]
	add r4, r0, r3
	mov r3, #0xe
	ldrsb r3, [r2, r3]
	cmp r3, #0
	bge _0802C5B6
	mov r6, #0xc
	ldrsh r0, [r2, r6]
	cmp r4, r0
	ble _0802C5C2
_0802C5B6:
	cmp r3, #0
	ble _0802C5D8
	mov r3, #0xc
	ldrsh r0, [r2, r3]
	cmp r4, r0
	blt _0802C5D8
_0802C5C2:
	ldrb r0, [r2, #9]
	strb r0, [r2, #1]
	ldrb r0, [r2, #8]
	strb r0, [r2]
	mov r6, #0xc
	ldrsh r4, [r2, r6]
	mov r5, #4
	b _0802C5E2
	.align 2, 0
.pool
_0802C5D8:
	strb r1, [r2, #1]
	ldrb r0, [r2, #0xe]
	ldrb r1, [r2]
	add r0, r0, r1
	strb r0, [r2]
_0802C5E2:
	add r1, r2, #0
	ldrb r0, [r1]
	cmp r0, #0x2f
	bhi _0802C5EE
	mov r0, #0x30
	strb r0, [r1]
_0802C5EE:
	ldrb r0, [r1]
	cmp r0, #0x78
	bls _0802C5F8
	mov r0, #0x78
	strb r0, [r1]
_0802C5F8:
	strh r4, [r2, #0xa]
	ldr r0, =0x030005FC
	ldr r1, [r0]
	ldr r3, =0x0000FFFF
	add r0, r3, #0
	ldrb r2, [r2]
	add r0, r0, r2
	strh r0, [r1, #0x12]
	ldr r0, =(sub_802CBE4+1)
	mov r1, #0
	bl addCallback
	lsl r0, r5, #0x10
	asr r0, r0, #0x10
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global unused_sub_802C628
unused_sub_802C628: @ 0x0802C628
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	sub sp, #4
	mov r2, #0
	ldr r0, =0x03003C05
	mov sb, r0
_0802C638:
	lsl r1, r2, #3
	ldr r0, =byte_82065BC
	add r6, r1, r0
	mov r1, sb
	add r0, r2, r1
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	add r2, #1
	mov r8, r2
	cmp r0, #0
	beq _0802C680
	ldrb r0, [r6, #4]
	lsl r0, r0, #6
	ldr r1, =0x0600F804
	add r5, r0, r1
	mov r4, #0
	ldrb r0, [r6]
	cmp r4, r0
	bhs _0802C680
	mov r7, sp
_0802C662:
	ldrh r0, [r6, #2]
	add r0, r4, r0
	strh r0, [r7]
	mov r0, sp
	add r1, r5, #0
	ldr r2, =0x01000001
	bl Bios_memcpy
	add r5, #2
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	ldrb r1, [r6]
	cmp r4, r1
	blo _0802C662
_0802C680:
	mov r1, r8
	lsl r0, r1, #0x10
	lsr r2, r0, #0x10
	cmp r2, #5
	bls _0802C638
	add sp, #4
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802C6A8
sub_0802C6A8: @ 0x0802C6A8
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x20
	mov r1, sp
	mov r2, #0x88
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	ldr r4, =0x02001920
	ldr r2, =0x01000400
	mov r0, sp
	add r1, r4, #0
	bl Bios_memcpy
	mov r8, r4
	mov r6, #0
	mov sl, r6
_0802C6D0:
	mov r0, sl
	lsl r1, r0, #3
	ldr r0, =byte_82065BC
	add r1, r1, r0
	str r1, [sp, #8]
	mov r3, #0
	ldr r0, =stageCount
	mov r2, sl
	lsl r1, r2, #1
	add r2, r1, r0
	add r4, r0, #0
	str r1, [sp, #0x18]
	mov r6, sl
	add r6, #1
	str r6, [sp, #0x14]
	ldrh r2, [r2]
	cmp r3, r2
	bhs _0802C7BC
	ldr r0, =0x03003C00
	str r0, [sp, #0xc]
	add r0, #5
	add r0, sl
	str r0, [sp, #0x10]
_0802C6FE:
	ldr r1, [sp, #0x10]
	ldrb r0, [r1]
	asr r0, r3
	mov r1, #1
	and r0, r1
	add r7, r3, #1
	cmp r0, #0
	beq _0802C7AE
	mov r2, sp
	add r2, #2
	lsl r5, r3, #3
	mov r6, sl
	lsl r6, r6, #5
	mov sb, r6
	lsl r4, r3, #2
	mov r0, sl
	lsl r0, r0, #4
	str r0, [sp, #0x1c]
	cmp r3, #0
	bne _0802C742
	mov r1, #0x40
	add r8, r1
	mov r0, #0
	ldr r6, [sp, #8]
	ldrb r1, [r6]
	cmp r3, r1
	bhs _0802C73E
_0802C734:
	add r0, #1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, r1
	blo _0802C734
_0802C73E:
	mov r0, #0x40
	add r8, r0
_0802C742:
	mov r6, r8
	add r6, #4
	ldr r1, =0x0000110B
	add r0, r3, r1
	strh r0, [r2]
	add r0, r2, #0
	add r1, r6, #0
	ldr r2, =0x01000001
	bl Bios_memcpy
	add r6, #4
	add r5, sb
	ldr r0, [sp, #0xc]
	add r0, #0xc
	add r0, r5, r0
	ldr r0, [r0]
	add r1, r6, #0
	mov r2, #0
	bl sub_0802C800
	add r6, #0x12
	ldr r2, =0x03003C10
	add r5, r5, r2
	ldr r0, [r5]
	add r1, r6, #0
	mov r2, #0
	bl sub_0802C800
	add r6, #0x12
	ldr r1, =dword_82065EC
	ldr r2, [sp, #0x1c]
	add r0, r4, r2
	add r0, r0, r1
	ldr r4, [r0]
	add r0, r4, #0
	add r1, r6, #0
	mov r2, #1
	bl sub_0802C800
	add r6, #0xc
	ldr r0, [r5]
	cmp r0, r4
	bhs _0802C7A8
	add r0, sp, #4
	ldr r2, =0x00001116
	add r1, r2, #0
	strh r1, [r0]
	add r1, r6, #0
	ldr r2, =0x01000001
	bl Bios_memcpy
_0802C7A8:
	mov r6, #0x40
	add r8, r6
	ldr r4, =stageCount
_0802C7AE:
	lsl r0, r7, #0x10
	lsr r3, r0, #0x10
	ldr r1, [sp, #0x18]
	add r0, r1, r4
	ldrh r0, [r0]
	cmp r3, r0
	blo _0802C6FE
_0802C7BC:
	ldr r2, [sp, #0x14]
	lsl r0, r2, #0x10
	lsr r0, r0, #0x10
	mov sl, r0
	cmp r0, #5
	bls _0802C6D0
	add sp, #0x20
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802C800
sub_0802C800: @ 0x0802C800
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x24
	str r0, [sp, #0x10]
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	str r2, [sp, #0x14]
	add r6, r1, #0
	mov r8, r0
	mov r0, #0
	str r0, [sp, #0x18]
	mov r0, r8
	mov r1, #0xe1
	lsl r1, r1, #4
	bl Bios_Div
	add r5, r0, #0
	cmp r5, #0x63
	bls _0802C832
	sub r5, #0x63
	str r5, [sp, #0x18]
	mov r5, #0x63
_0802C832:
	add r0, r5, #0
	mov r1, #0xa
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	add r0, r5, #0
	mov r1, #0xa
	bl Bios_modulo
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	mov r1, sp
	ldr r2, =0x0000110A
	mov sl, r2
	mov r2, sl
	add r0, r7, r2
	strh r0, [r1]
	ldr r0, =0x01000001
	mov sb, r0
	mov r0, sp
	add r1, r6, #0
	mov r2, sb
	bl Bios_memcpy
	add r6, #2
	mov r0, sp
	add r0, #2
	mov r2, sl
	add r1, r4, r2
	strh r1, [r0]
	add r1, r6, #0
	mov r2, sb
	bl Bios_memcpy
	add r6, #2
	add r0, sp, #4
	ldr r2, =0x00001114
	add r1, r2, #0
	strh r1, [r0]
	add r1, r6, #0
	mov r2, sb
	bl Bios_memcpy
	add r6, #2
	mov r0, r8
	mov r1, #0xe1
	lsl r1, r1, #4
	bl Bios_modulo
	mov r8, r0
	mov r1, #0x3c
	bl Bios_Div
	ldr r2, [sp, #0x18]
	lsl r1, r2, #4
	sub r1, r1, r2
	lsl r1, r1, #2
	add r5, r0, r1
	add r0, r5, #0
	mov r1, #0xa
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	add r0, r5, #0
	mov r1, #0xa
	bl Bios_modulo
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	mov r0, sp
	add r0, #6
	mov r2, sl
	add r1, r7, r2
	strh r1, [r0]
	add r1, r6, #0
	mov r2, sb
	bl Bios_memcpy
	add r6, #2
	add r0, sp, #8
	add r4, sl
	strh r4, [r0]
	add r1, r6, #0
	mov r2, sb
	bl Bios_memcpy
	ldr r2, [sp, #0x14]
	cmp r2, #0
	bne _0802C978
	add r6, #2
	mov r0, sp
	add r0, #0xa
	ldr r2, =0x00001115
	add r1, r2, #0
	strh r1, [r0]
	add r1, r6, #0
	mov r2, sb
	bl Bios_memcpy
	add r6, #2
	mov r0, r8
	mov r1, #0x3c
	bl Bios_modulo
	mov r8, r0
	ldr r0, =0x0005879F
	ldr r1, [sp, #0x10]
	cmp r1, r0
	bne _0802C928
	mov r5, #0x63
	b _0802C938
	.align 2, 0
.pool
_0802C928:
	mov r0, #0x64
	mov r2, r8
	mul r2, r0, r2
	add r0, r2, #0
	mov r1, #0x3c
	bl Bios_Div
	add r5, r0, #0
_0802C938:
	add r0, r5, #0
	mov r1, #0xa
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	add r0, r5, #0
	mov r1, #0xa
	bl Bios_modulo
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	add r0, sp, #0xc
	ldr r1, =0x0000110A
	mov r8, r1
	mov r2, r8
	add r1, r7, r2
	strh r1, [r0]
	ldr r5, =0x01000001
	add r1, r6, #0
	add r2, r5, #0
	bl Bios_memcpy
	add r6, #2
	mov r0, sp
	add r0, #0xe
	add r4, r8
	strh r4, [r0]
	add r1, r6, #0
	add r2, r5, #0
	bl Bios_memcpy
_0802C978:
	add sp, #0x24
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802C990
sub_0802C990: @ 0x0802C990
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	mov r3, #0
	ldr r0, =0x03000580
	str r3, [r0, #4]
	mov r4, #0
	add r5, r0, #0
	ldr r0, =stageCount
	mov sb, r0
_0802C9A6:
	mov r2, #0
	lsl r0, r4, #1
	add r0, sb
	ldrh r1, [r0]
	add r0, r4, #1
	mov r8, r0
	cmp r2, r1
	bhs _0802CA06
	ldr r0, =0x03003C05
	add r0, r0, r4
	mov ip, r0
	add r7, r1, #0
	ldr r6, =0x03003D40
_0802C9C0:
	mov r1, ip
	ldrb r0, [r1]
	asr r0, r2
	mov r1, #1
	and r0, r1
	cmp r0, #0
	beq _0802C9FC
	cmp r2, #0
	bne _0802C9DC
	lsl r0, r3, #0x10
	mov r3, #0x80
	lsl r3, r3, #0xa
	add r0, r0, r3
	lsr r3, r0, #0x10
_0802C9DC:
	add r0, r1, #0
	lsl r0, r3
	ldr r1, [r5, #4]
	orr r1, r0
	str r1, [r5, #4]
	ldrb r0, [r6, #1]
	cmp r0, r4
	bne _0802C9F6
	ldrb r0, [r6, #2]
	cmp r0, r2
	bne _0802C9F6
	lsl r0, r3, #3
	strb r0, [r5, #1]
_0802C9F6:
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
_0802C9FC:
	add r0, r2, #1
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	cmp r2, r7
	blo _0802C9C0
_0802CA06:
	mov r1, r8
	lsl r0, r1, #0x10
	lsr r4, r0, #0x10
	cmp r4, #5
	bls _0802C9A6
	strb r3, [r5, #2]
	cmp r3, #0xe
	bls _0802CA2C
	mov r0, #0x30
	b _0802CA30
	.align 2, 0
.pool
_0802CA2C:
	ldrb r0, [r5, #1]
	add r0, #0x20
_0802CA30:
	strb r0, [r5]
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global sub_0802CA40
sub_0802CA40: @ 0x0802CA40
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	mov r0, #0
	mov ip, r0
	ldr r1, =0x03000580
	ldrb r0, [r1, #1]
	lsr r4, r0, #3
	mov r3, #0
	cmp r3, r4
	bhs _0802CA7C
	mov r2, #1
	ldr r1, [r1, #4]
_0802CA5E:
	add r0, r2, #0
	lsl r0, r3
	and r0, r1
	cmp r0, #0
	beq _0802CA72
	mov r0, ip
	add r0, #1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov ip, r0
_0802CA72:
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, r4
	blo _0802CA5E
_0802CA7C:
	mov r4, #0
	mov r3, #0
	ldr r5, =stageCount
	mov r8, r5
	ldr r6, =0x030005EC
	mov sb, r6
	ldr r0, =0x030005EA
	mov sl, r0
_0802CA8C:
	mov r2, #0
	lsl r1, r3, #1
	mov r5, r8
	add r0, r1, r5
	ldrh r0, [r0]
	cmp r2, r0
	bhs _0802CAE4
	ldr r6, =0x03003C05
	add r5, r3, r6
	ldr r6, =stageCount
	add r0, r1, r6
	ldrh r1, [r0]
	mov r6, #1
	mov r7, sl
_0802CAA8:
	ldrb r0, [r5]
	asr r0, r2
	and r0, r6
	cmp r0, #0
	beq _0802CADA
	cmp ip, r4
	bne _0802CAD4
	strh r2, [r7]
	mov r0, sb
	strh r3, [r0]
	b _0802CAEE
	.align 2, 0
.pool
_0802CAD4:
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
_0802CADA:
	add r0, r2, #1
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	cmp r2, r1
	blo _0802CAA8
_0802CAE4:
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, #5
	bls _0802CA8C
_0802CAEE:
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0

thumb_func_global sub_0802CAFC
sub_0802CAFC: @ 0x0802CAFC
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	ldr r1, =0x03000580
	ldrb r0, [r1, #1]
	lsr r0, r0, #3
	mov r8, r0
	mov r7, #0
	mov r5, #1
	ldrb r0, [r1, #2]
	cmp r5, r0
	bhs _0802CBD6
	add r4, r1, #0
	lsl r0, r2, #0x10
	asr r6, r0, #0x10
_0802CB1E:
	lsl r0, r7, #0x10
	mov r1, #0x80
	lsl r1, r1, #9
	add r0, r0, r1
	lsr r7, r0, #0x10
	ldrb r1, [r4, #2]
	mov r2, r8
	add r0, r2, r1
	add r2, r6, #0
	mul r2, r5, r2
	add r0, r0, r2
	bl Bios_modulo
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	mov r1, #1
	lsl r1, r2
	ldr r0, [r4, #4]
	and r0, r1
	cmp r0, #0
	beq _0802CBCA
	lsl r1, r2, #3
	strb r1, [r4, #9]
	ldrb r0, [r4, #2]
	cmp r0, #0xe
	bls _0802CB7C
	lsl r0, r7, #0x10
	asr r0, r0, #0x10
	lsl r1, r6, #3
	mul r0, r1, r0
	ldrb r1, [r4]
	add r0, r0, r1
	strb r0, [r4, #8]
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0x2f
	bhi _0802CB6C
	mov r0, #0x30
	strb r0, [r4, #8]
_0802CB6C:
	ldrb r0, [r4, #8]
	cmp r0, #0x78
	bls _0802CBAC
	mov r0, #0x78
	strb r0, [r4, #8]
	b _0802CBAC
	.align 2, 0
.pool
_0802CB7C:
	add r0, r1, #0
	add r0, #0x20
	strb r0, [r4, #8]
	ldrb r0, [r4, #1]
	cmp r0, #0x10
	bne _0802CB94
	cmp r6, #0
	bge _0802CB94
	ldrb r1, [r4, #2]
	mov r0, #3
	sub r0, r0, r1
	b _0802CBA8
_0802CB94:
	ldrb r1, [r4, #1]
	ldrb r2, [r4, #2]
	sub r0, r2, #1
	lsl r0, r0, #3
	cmp r1, r0
	bne _0802CBAC
	cmp r6, #0
	ble _0802CBAC
	mov r0, #3
	sub r0, r0, r2
_0802CBA8:
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
_0802CBAC:
	mov r0, #0
	strh r0, [r4, #0xa]
	lsl r0, r7, #0x10
	asr r0, r0, #0x10
	mul r0, r6, r0
	lsl r0, r0, #3
	strh r0, [r4, #0xc]
	mov r2, #0xc
	ldrsh r0, [r4, r2]
	mov r1, #4
	bl Bios_Div
	strb r0, [r4, #0xe]
	mov r0, #1
	b _0802CBD8
_0802CBCA:
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldrb r0, [r4, #2]
	cmp r5, r0
	blo _0802CB1E
_0802CBD6:
	mov r0, #0
_0802CBD8:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	thumb_func_global sub_802CBE4
sub_802CBE4: @ 0x0802CBE4
	push {r4, r5, r6, r7, lr}
	ldr r3, =0x03000580
	ldrb r1, [r3, #1]
	ldrb r0, [r3]
	sub r0, #0x20
	sub r1, r1, r0
	lsl r1, r1, #0x10
	lsr r2, r1, #0x10
	asr r1, r1, #0x10
	cmp r1, #0
	bge _0802CC04
	ldrb r0, [r3, #2]
	lsl r0, r0, #3
	add r0, r1, r0
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
_0802CC04:
	lsl r0, r2, #0x10
	asr r1, r0, #0x10
	ldrb r0, [r3, #2]
	lsl r0, r0, #3
	cmp r1, r0
	blt _0802CC16
	sub r0, r1, r0
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
_0802CC16:
	lsl r1, r2, #0x10
	mov r0, #0xe0
	lsl r0, r0, #0xb
	and r0, r1
	lsr r7, r0, #0x10
	ldr r5, =0x0600F800
	mov r4, #0
	asr r6, r1, #0x13
_0802CC26:
	add r0, r6, r4
	ldr r1, =0x03000580
	ldrb r1, [r1, #2]
	bl Bios_modulo
	lsl r0, r0, #6
	ldr r1, =0x02001920
	add r0, r0, r1
	add r1, r5, #0
	mov r2, #0x20
	bl Bios_memcpy
	add r5, #0x40
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0xf
	bls _0802CC26
	ldr r1, =0x030022F8
	mov r0, #0x20
	sub r0, r0, r7
	strh r0, [r1, #6]
	ldr r1, =0x03003D68
	neg r0, r0
	strh r0, [r1, #6]
	ldr r0, =(sub_802CBE4+1)
	bl deleteCallback
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global sub_0802CC7C
sub_0802CC7C: @ 0x0802CC7C
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	mov r4, #0
	ldr r7, =stageCount
	ldr r0, =0x03003C05
	mov sb, r0
	ldr r6, =0x03003D40
	mov r8, r6
	mov ip, r7
_0802CC92:
	mov r1, #0
	lsl r2, r4, #1
	add r0, r2, r7
	ldrh r0, [r0]
	cmp r1, r0
	bhs _0802CCD2
	mov r0, sb
	add r5, r4, r0
	mov r3, r8
	mov r6, ip
	add r0, r2, r6
	ldrh r2, [r0]
	mov r6, #1
_0802CCAC:
	ldrb r0, [r5]
	asr r0, r1
	and r0, r6
	cmp r0, #0
	beq _0802CCC8
	strb r4, [r3, #1]
	strb r1, [r3, #2]
	b _0802CCDC
	.align 2, 0
.pool
_0802CCC8:
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	cmp r1, r2
	blo _0802CCAC
_0802CCD2:
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #5
	bls _0802CC92
_0802CCDC:
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0

thumb_func_global sub_0802CCE8
sub_0802CCE8: @ 0x0802CCE8
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #4
	mov r4, #0
	ldr r3, =0x030005F0
	ldr r0, =0x0000FFFF
	add r2, r0, #0
_0802CCFC:
	lsl r0, r4, #1
	add r0, r0, r3
	ldrh r1, [r0]
	orr r1, r2
	strh r1, [r0]
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #5
	bls _0802CCFC
	mov r4, #0
	mov r6, sp
_0802CD14:
	mov r0, #0
	strh r0, [r6]
	lsl r1, r4, #3
	ldr r5, =0x03000598
	add r1, r1, r5
	mov r0, sp
	ldr r2, =0x01000004
	bl Bios_memcpy
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #5
	bls _0802CD14
	mov r6, #0
	mov r4, #0
	add r3, r5, #0
	ldr r2, =0x03003C05
	ldr r1, =0x030005F0
_0802CD3A:
	add r0, r4, r2
	ldrb r0, [r0]
	cmp r0, #0
	beq _0802CD4E
	lsl r0, r6, #1
	add r0, r0, r1
	strh r4, [r0]
	add r0, r6, #1
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
_0802CD4E:
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #5
	bls _0802CD3A
	add r5, r3, #0
	mov r2, #0
	mov sb, r2
	mov r7, #0
	mov r0, #0
	mov r8, r0
	mov r4, #0
	ldr r1, =0x03000580
	ldrb r1, [r1, #2]
	cmp r4, r1
	bhs _0802CDF0
	ldr r2, =0x03000580
	mov sl, r2
_0802CD72:
	mov r0, sl
	ldrb r1, [r0, #2]
	add r0, r4, #0
	bl Bios_modulo
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #1
	lsl r1, r0
	mov r2, sl
	ldr r0, [r2, #4]
	and r0, r1
	cmp r0, #0
	bne _0802CDE2
	cmp r7, #0
	beq _0802CDB0
	mov r7, #0
	b _0802CDE2
	.align 2, 0
.pool
_0802CDB0:
	mov r0, r8
	cmp r0, #5
	bhi _0802CDD6
	mov r1, #1
	strb r1, [r5]
	strb r7, [r5, #1]
	mov r0, sb
	add r1, r6, #0
	bl Bios_modulo
	strb r0, [r5, #2]
	lsl r0, r4, #3
	strb r0, [r5, #4]
	add r5, #8
	mov r0, r8
	add r0, #1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r8, r0
_0802CDD6:
	mov r0, sb
	add r0, #1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sb, r0
	mov r7, #1
_0802CDE2:
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	ldr r2, =0x03000580
	ldrb r2, [r2, #2]
	cmp r4, r2
	blo _0802CD72
_0802CDF0:
	add sp, #4
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802CE04
sub_0802CE04: @ 0x0802CE04
	push {r4, r5, lr}
	sub sp, #8
	ldr r5, [sp, #0x14]
	lsl r0, r0, #0x10
	ldr r4, =string_levelNames
	ldr r1, =0x03002080
	add r1, #0x2c
	ldrb r1, [r1]
	lsl r1, r1, #2
	add r1, r1, r4
	ldr r4, [r1]
	lsr r0, r0, #0xb
	add r4, r4, r0
	ldr r1, =0x030005C8
	lsl r3, r3, #0x10
	asr r3, r3, #0x10
	str r3, [sp]
	lsl r5, r5, #0x10
	asr r5, r5, #0x10
	str r5, [sp, #4]
	add r0, r4, #0
	mov r3, #3
	bl sub_0802CF94
	add sp, #8
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802CE48
sub_0802CE48: @ 0x0802CE48
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #8
	ldr r2, =0x03000580
	ldrb r0, [r2, #1]
	add r0, #0x20
	ldrb r1, [r2]
	sub r0, r0, r1
	mov r8, r0
	cmp r0, #0
	bge _0802CE6A
	ldrb r0, [r2, #2]
	lsl r0, r0, #3
	add r8, r0
_0802CE6A:
	ldrb r0, [r2, #2]
	lsl r0, r0, #3
	cmp r8, r0
	ble _0802CE78
	mov r1, r8
	sub r1, r1, r0
	mov r8, r1
_0802CE78:
	mov r4, #0
	ldr r5, =0x03000618
_0802CE7C:
	lsl r0, r4, #3
	ldr r1, =0x03000598
	add r2, r0, r1
	ldrb r0, [r2, #1]
	cmp r0, #0
	beq _0802CE98
	mov r0, #0
	strb r0, [r2, #1]
	ldrb r0, [r2, #3]
	lsl r0, r0, #2
	add r0, r0, r5
	ldr r0, [r0]
	bl spriteFreeHeader
_0802CE98:
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #5
	bls _0802CE7C
	mov r6, #0
	ldr r0, =0x03000580
	ldrb r1, [r0, #2]
	add r2, r0, #0
	cmp r1, #0xe
	bls _0802CEC0
	mov r0, #0x70
	b _0802CEC4
	.align 2, 0
.pool
_0802CEC0:
	ldrb r0, [r2, #2]
	lsl r0, r0, #3
_0802CEC4:
	str r0, [sp, #4]
	mov r7, #0x20
	mov r4, #0
	ldr r1, [sp, #4]
	cmp r4, r1
	bge _0802CF84
_0802CED0:
	mov r0, r8
	add r3, r0, r4
	ldr r2, =0x03000580
	cmp r3, #0
	bge _0802CEE0
	ldrb r0, [r2, #2]
	lsl r0, r0, #3
	add r3, r3, r0
_0802CEE0:
	ldrb r0, [r2, #2]
	lsl r0, r0, #3
	cmp r3, r0
	blt _0802CEEA
	sub r3, r3, r0
_0802CEEA:
	mov r5, #0
	ldr r1, =0x03000598
	mov ip, r1
	add r4, #1
	mov sl, r4
	add r0, r7, #1
	mov sb, r0
	ldr r4, =0x030005F0
_0802CEFA:
	lsl r0, r5, #3
	mov r1, ip
	add r2, r0, r1
	ldrb r0, [r2]
	cmp r0, #0
	beq _0802CF6C
	ldrb r0, [r2, #1]
	cmp r0, #0
	bne _0802CF6C
	ldrb r1, [r2, #4]
	cmp r3, r1
	blt _0802CF6C
	add r0, r1, #0
	add r0, #0x10
	cmp r3, r0
	bge _0802CF6C
	mov r0, #1
	strb r0, [r2, #1]
	strb r6, [r2, #3]
	cmp r7, #0x20
	bne _0802CF4C
	ldrb r0, [r2, #2]
	lsl r0, r0, #1
	add r0, r0, r4
	ldrh r0, [r0]
	sub r1, r1, r3
	add r1, #0x24
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	str r1, [sp]
	add r1, r6, #0
	mov r3, #0x14
	bl sub_0802CE04
	b _0802CF64
	.align 2, 0
.pool
_0802CF4C:
	ldrb r0, [r2, #2]
	lsl r0, r0, #1
	add r0, r0, r4
	ldrh r0, [r0]
	add r1, r7, #4
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	str r1, [sp]
	add r1, r6, #0
	mov r3, #0x14
	bl sub_0802CE04
_0802CF64:
	add r0, r6, #1
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	b _0802CF76
_0802CF6C:
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, #5
	bls _0802CEFA
_0802CF76:
	mov r7, sb
	mov r1, sl
	lsl r0, r1, #0x10
	lsr r4, r0, #0x10
	ldr r0, [sp, #4]
	cmp r4, r0
	blt _0802CED0
_0802CF84:
	add sp, #8
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0

thumb_func_global sub_0802CF94
sub_0802CF94: @ 0x0802CF94
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #4
	add r7, r1, #0
	add r4, r2, #0
	ldr r1, [sp, #0x1c]
	ldr r2, [sp, #0x20]
	lsl r3, r3, #0x18
	lsr r5, r3, #0x18
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r8, r1
	lsl r2, r2, #0x10
	lsr r6, r2, #0x10
	ldrb r2, [r4, #3]
	mov r1, #0xb4
	mul r1, r2, r1
	ldr r2, =0x03000630
	add r1, r1, r2
	add r2, r5, #0
	bl sub_0802D020
	lsl r0, r6, #0x10
	asr r0, r0, #0x10
	cmp r0, #0
	blt _0802D006
	cmp r0, #0xa0
	bgt _0802D006
	ldrb r1, [r4, #3]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #3
	ldr r1, =dword_8206670
	add r0, r0, r1
	ldrh r2, [r7]
	mov r1, #0
	str r1, [sp]
	add r3, r5, #0
	bl allocSprite
	ldr r2, =0x03000618
	ldrb r1, [r4, #3]
	lsl r1, r1, #2
	add r1, r1, r2
	str r0, [r1]
	ldrb r0, [r4, #3]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r0, [r0]
	mov r1, r8
	strh r1, [r0, #0x10]
	ldrb r0, [r4, #3]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r0, [r0]
	strh r6, [r0, #0x12]
_0802D006:
	add sp, #4
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802D020
sub_0802D020: @ 0x0802D020
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #8
	mov sl, r0
	mov sb, r1
	mov r5, sl
	mov r3, #0
	b _0802D046
_0802D036:
	cmp r1, #0xfe
	bne _0802D03E
	add r5, #2
	b _0802D046
_0802D03E:
	add r5, #2
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
_0802D046:
	ldrb r1, [r5]
	add r0, r1, #0
	cmp r0, #0xff
	bne _0802D036
	mov r4, sb
	mov r5, #0
	add r6, r3, #1
	cmp r5, r6
	bge _0802D07C
	mov r7, sp
	mov r0, #0
	mov r8, r0
_0802D05E:
	mov r0, r8
	strh r0, [r7]
	mov r0, sp
	add r1, r4, #0
	ldr r2, =0x01000006
	str r3, [sp, #4]
	bl Bios_memcpy
	add r4, #0xc
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldr r3, [sp, #4]
	cmp r5, r6
	blt _0802D05E
_0802D07C:
	mov r4, sb
	strh r3, [r4]
	add r4, #0xc
	mov r5, sl
	ldr r7, =0x030005C8
	mov r6, #0
	ldrb r1, [r5]
	add r0, r1, #0
	cmp r0, #0xff
	beq _0802D106
	mov r0, #0x31
	neg r0, r0
	mov r8, r0
_0802D096:
	cmp r1, #0xfe
	bne _0802D0B0
	ldrb r0, [r5, #1]
	add r0, r6, r0
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	add r5, #2
	b _0802D0FE
	.align 2, 0
.pool
_0802D0B0:
	ldr r0, [r7, #4]
	ldr r1, [r0]
	ldrb r0, [r5]
	lsl r0, r0, #2
	add r1, r1, r0
	ldr r3, [r1]
	ldrh r0, [r3]
	strh r0, [r4]
	strh r6, [r4, #2]
	ldrb r0, [r7, #2]
	strb r0, [r4, #6]
	ldrb r0, [r3, #2]
	lsr r0, r0, #2
	mov r1, #3
	and r0, r1
	lsl r0, r0, #4
	ldrb r2, [r4, #7]
	mov r1, r8
	and r1, r2
	orr r1, r0
	strb r1, [r4, #7]
	ldrb r0, [r3, #2]
	mov r2, #3
	and r2, r0
	lsl r2, r2, #6
	mov r0, #0x3f
	and r1, r0
	orr r1, r2
	strb r1, [r4, #7]
	ldrb r0, [r4, #8]
	mov r1, #0x1f
	orr r0, r1
	strb r0, [r4, #8]
	ldrb r0, [r5, #1]
	add r0, r6, r0
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	add r5, #2
	add r4, #0xc
_0802D0FE:
	ldrb r1, [r5]
	add r0, r1, #0
	cmp r0, #0xff
	bne _0802D096
_0802D106:
	add sp, #8
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global drawSelStr
drawSelStr: @ 0x0802D118
	push {lr}
	sub sp, #4
	ldr r1, =string_select
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005D0
	mov r2, #0
	str r2, [sp]
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global positionSelStr
positionSelStr: @ 0x0802D148
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =string_select
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005D0
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global drawOkStr
drawOkStr: @ 0x0802D184
	push {lr}
	sub sp, #4
	ldr r1, =string_ok
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005D0
	mov r2, #0
	str r2, [sp]
	mov r2, #6
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global positionOkStr
positionOkStr: @ 0x0802D1B8
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =string_ok
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005D0
	str r1, [sp]
	add r1, r3, #0
	mov r3, #6
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global drawBackStr
drawBackStr: @ 0x0802D1F4
	push {lr}
	sub sp, #4
	ldr r1, =string_back
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005D0
	mov r2, #0
	str r2, [sp]
	mov r2, #0xc
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global positionBackStr
positionBackStr: @ 0x0802D228
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =string_back
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005D0
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0xc
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global drawSelectStageStr
drawSelectStageStr: @ 0x0802D264
	push {lr}
	sub sp, #4
	ldr r1, =selectStage_strings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005D8
	mov r2, #0
	str r2, [sp]
	mov r2, #0x10
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global positionSelectStageStr
positionSelectStageStr: @ 0x0802D298
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =selectStage_strings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005D8
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0x10
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802D2D4
sub_0802D2D4: @ 0x0802D2D4
	push {lr}
	ldr r1, =selectStage_strings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0x10
	bl str_getX_ofFirstChar
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global drawLatestTimeStr
drawLatestTimeStr: @ 0x0802D2FC
	push {lr}
	sub sp, #4
	ldr r1, =latestTime_strings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005D0
	mov r2, #0
	str r2, [sp]
	mov r2, #0x1b
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global positionLatestTimeStr
positionLatestTimeStr: @ 0x0802D330
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =latestTime_strings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005D0
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0x1b
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global drawBestTimeStr
drawBestTimeStr: @ 0x0802D36C
	push {lr}
	sub sp, #4
	ldr r1, =bestTime_string
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005D0
	mov r2, #0
	str r2, [sp]
	mov r2, #0x25
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global positionBestTimeStr
positionBestTimeStr: @ 0x0802D3A0
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =bestTime_string
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005D0
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0x25
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global drawQualifyStr
drawQualifyStr: @ 0x0802D3DC
	push {lr}
	sub sp, #4
	ldr r1, =qualify_str
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005D0
	mov r2, #0
	str r2, [sp]
	mov r2, #0x2d
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global positionQualifyStr
positionQualifyStr: @ 0x0802D410
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =qualify_str
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005D0
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0x2d
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool
