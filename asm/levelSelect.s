.include "asm/macros.inc"

@completely disassembled

thumb_func_global cb_loadLevelSelect
cb_loadLevelSelect: @ 0x0802ACE8
	push {r4, r5, r6, r7, lr}
	sub sp, #4
	ldr r5, =0x03003CD4
	mov r1, #0x82
	lsl r1, r1, #5
	add r0, r1, #0
	strh r0, [r5]
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r0, =levelSelectIconPal
	mov r2, #0xa0
	lsl r2, r2, #0x13
	mov r1, #1
	bl memcpy_pal
	ldr r0, =levelSelectIconTiles
	mov r1, #0xc0
	lsl r1, r1, #0x13
	bl Bios_LZ77_16_2
	ldr r0, =levelSelectIconMap
	ldr r1, =0x0600F000
	mov r4, #0
	str r4, [sp]
	mov r2, #0
	mov r3, #0
	bl LZ77_mapCpy
	ldr r6, =0x03003D00
	mov r7, #0
	ldr r0, =0x00001E02
	strh r0, [r6, #4]
	ldrh r0, [r5]
	mov r2, #0x80
	lsl r2, r2, #3
	add r1, r2, #0
	orr r0, r1
	strh r0, [r5]
	ldr r0, =menuBgPal
	ldr r2, =0x05000020
	mov r1, #1
	bl memcpy_pal
	ldr r0, =menuBgTilesCompressed
	ldr r1, =0x06001800
	bl Bios_LZ77_16_2
	ldr r0, =menuBgMapCompressed
	ldr r1, =0x0600F800
	mov r2, #1
	str r2, [sp]
	mov r2, #0
	mov r3, #0xc0
	bl LZ77_mapCpy
	ldr r0, =0x00001F02
	strh r0, [r6, #6]
	ldrh r0, [r5]
	mov r3, #0x80
	lsl r3, r3, #4
	add r1, r3, #0
	orr r0, r1
	strh r0, [r5]
	ldr r0, =0x03000580
	strh r4, [r0]
	ldr r0, =0x03000584
	str r4, [r0]
	ldr r1, =0x03003D40
	strb r7, [r1, #2]
	strb r7, [r1, #1]
	strh r4, [r1, #6]
	strb r7, [r1, #3]
	ldrb r0, [r1]
	cmp r0, #1
	bne _0802ADE4
	ldr r0, =0x03003C00
	ldrb r0, [r0]
	lsl r0, r0, #0x1a
	lsr r0, r0, #0x1a
	strb r0, [r1, #8]
	b _0802ADE6
	.align 2, 0
.pool
_0802ADE4:
	strb r7, [r1, #8]
_0802ADE6:
	bl ls_defaultCursorPos
	bl ls_setUnlockedLevels
	ldr r0, =0x03000620
	mov r1, #0
	bl allocFont
	ldr r0, =0x03000628
	mov r1, #1
	bl allocFont
	ldr r0, =0x03000630
	mov r1, #2
	bl allocFont
	bl levelSelect_loadStrMissionSel
	ldr r0, =0x0000FF5C
	mov r1, #8
	bl setSelectMissionStrPos
	bl bufferLevelNameStr
	ldr r4, =word_8205FC0
	ldr r5, =0x0300058C
	ldrb r1, [r5]
	lsl r1, r1, #2
	add r0, r1, r4
	ldrh r0, [r0]
	add r6, r4, #2
	add r1, r1, r6
	ldrh r1, [r1]
	bl ls_setLevelNameStrPos
	bl levelSelect_HideLevelName
	bl levelSelectStrings_getX
	add r1, r0, #0
	ldrb r0, [r5]
	lsl r0, r0, #2
	add r0, r0, r4
	ldrh r0, [r0]
	sub r1, r1, r0
	mov r0, #0x80
	sub r0, r0, r1
	mov r1, #2
	bl Bios_Div
	add r0, #0x60
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldrb r1, [r5]
	lsl r1, r1, #2
	add r1, r1, r6
	ldrh r1, [r1]
	bl ls_setLevelNameStrPos
	bl levelSelect_drawSelMissStr
	ldr r1, =word_8205FD4
	ldr r4, =0x03002080
	add r4, #0x2c
	ldrb r0, [r4]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	add r0, #0xd
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x91
	bl ls_setSelectStrPos
	bl ls_drawStrOK
	ldr r1, =word_8205FDA
	ldrb r0, [r4]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	add r0, #0xd
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x91
	bl ls_setOkStrPos
	bl ls_loadBackStr
	ldr r1, =dword_8205FE0
	ldrb r0, [r4]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	add r0, #0xd
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x91
	bl ls_setBackStrPos
	bl levelSelect_loadSprites
	bl sub_0802BDBC
	bl sub_0802BE04
	ldr r1, =0x03003D40
	ldrb r0, [r1, #8]
	ldrb r1, [r1, #9]
	cmp r0, r1
	beq _0802AEFC
	ldr r1, =0x0300058A
	mov r0, #1
	b _0802AF00
	.align 2, 0
.pool
_0802AEFC:
	ldr r1, =0x0300058A
	mov r0, #0
_0802AF00:
	strb r0, [r1]
	ldr r0, =0x0300058C
	ldrb r2, [r0, #1]
	mov r0, #0x10
	and r0, r2
	cmp r0, #0
	bne _0802AF60
	ldr r1, =0x03003D88
	mov r3, #0
	ldr r0, =0x00003888
	strh r0, [r1, #2]
	mov r0, #8
	and r0, r2
	cmp r0, #0
	beq _0802AF3C
	ldr r1, =0x03003D90
	ldr r0, =0x000090F0
	b _0802AF40
	.align 2, 0
.pool
_0802AF3C:
	ldr r1, =0x03003D90
	ldr r0, =0x000040F0
_0802AF40:
	strh r0, [r1, #2]
	ldr r1, =0x03003D7C
	mov r2, #0xfb
	lsl r2, r2, #8
	add r0, r2, #0
	strh r0, [r1]
	ldr r1, =0x03003D8C
	mov r0, #0x3f
	strh r0, [r1]
	ldr r2, =0x03003CD4
	ldrh r0, [r2]
	mov r3, #0x80
	lsl r3, r3, #7
	add r1, r3, #0
	orr r0, r1
	strh r0, [r2]
_0802AF60:
	ldr r0, =off_8205EC4
	mov r1, #0xa0
	lsl r1, r1, #0x13
	mov r2, #0
	bl startPalAnimation
	ldr r1, =0x03000638
	strh r0, [r1]
	bl nullsub_7
	bl sub_0802B658
	bl nullsub_8
	ldr r0, =(cb_levelSelect+1)
	bl setCurrentTaskFunc
	add sp, #4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global cb_levelSelect
cb_levelSelect: @ 0x0802AFAC
	push {r4, lr}
	ldr r1, =levelSelectStates
	ldr r4, =0x03000580
	ldrh r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl call_r0
	lsl r0, r0, #0x10
	asr r1, r0, #0x10
	cmp r1, #1
	beq _0802AFE2
	cmp r1, #1
	bgt _0802AFDC
	mov r0, #1
	neg r0, r0
	cmp r1, r0
	beq _0802AFEA
	b _0802B02C
	.align 2, 0
.pool
_0802AFDC:
	cmp r1, #2
	beq _0802B00C
	b _0802B02C
_0802AFE2:
	ldrh r0, [r4]
	add r0, #1
	strh r0, [r4]
	b _0802B02C
_0802AFEA:
	ldr r1, =0x03003D40
	ldrb r0, [r1]
	cmp r0, #1
	bne _0802AFFC
	mov r0, #0
	b _0802AFFE
	.align 2, 0
.pool
_0802AFFC:
	mov r0, #1
_0802AFFE:
	strb r0, [r1, #0x10]
	ldr r0, =(cb_loadMenu+1)
	bl setCurrentTaskFunc
	b _0802B02C
	.align 2, 0
.pool
_0802B00C:
	bl setPlayerStats_andtuff_8031B14
	ldr r1, =0x03003D40
	mov r2, #0
	strb r2, [r1, #0x10]
	ldr r0, =0x0300058C
	ldrb r0, [r0]
	strb r0, [r1, #1]
	strb r2, [r1, #4]
	mov r0, #0
	strh r2, [r1, #6]
	strb r0, [r1, #3]
	strb r0, [r1, #2]
	ldr r0, =(cb_loadLevelIntroScroll+1)
	bl setCurrentTaskFunc
_0802B02C:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global levelSelState0
levelSelState0: @ 0x0802B040
	push {lr}
	bl moveSelectMissionStr
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0802B05A
	mov r0, #0
	b _0802B062
_0802B05A:
	mov r0, #2
	bl playSong
	mov r0, #1
_0802B062:
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global moveSelectMissionStr
moveSelectMissionStr: @ 0x0802B068
	push {lr}
	bl levelSelect_getSelMissionStrX
	lsl r0, r0, #0x10
	asr r1, r0, #0x10
	ldr r2, =0xFFFF0000
	add r0, r0, r2
	lsr r0, r0, #0x10
	cmp r0, #0xe6
	bls _0802B094
	add r0, r1, #0
	add r0, #8
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #8
	bl setSelectMissionStrPos
	mov r0, #0
	b _0802B09E
	.align 2, 0
.pool
_0802B094:
	mov r0, #8
	mov r1, #8
	bl setSelectMissionStrPos
	mov r0, #1
_0802B09E:
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global levelSelState2
levelSelState2: @ 0x0802B0A4
	push {r4, lr}
	mov r4, #0
	ldr r0, =0x0300058A
	ldrb r0, [r0]
	cmp r0, #0
	bne _0802B0B8
	bl levelSelect_updateSelection
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
_0802B0B8:
	bl sub_0802B430
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r4, #0
	bne _0802B0C8
	cmp r0, #0
	beq _0802B0D0
_0802B0C8:
	mov r0, #1
	b _0802B0D2
	.align 2, 0
.pool
_0802B0D0:
	mov r0, #0
_0802B0D2:
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global levelSelState3
levelSelState3: @ 0x0802B0D8
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0802B0F0
	mov r0, #0
	b _0802B0F2
_0802B0F0:
	mov r0, #1
_0802B0F2:
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global levelSelState4
levelSelState4: @ 0x0802B0F8
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl FreeAllPalFadeTasks
	bl resetAffineTransformations
	bl clearSprites
	bl clearPalBufs
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r0, =0x03000588
	mov r1, #0
	ldrsh r0, [r0, r1]
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global ls_defaultCursorPos
ls_defaultCursorPos: @ 0x0802B130
	push {r4, lr}
	ldr r3, =0x0300058C
	mov r0, #0xff
	strb r0, [r3]
	mov r4, #0
	ldr r2, =0x03003D40
	ldrb r1, [r2, #8]
	mov r0, #1
	and r1, r0
	cmp r1, #0
	beq _0802B15C
_0802B146:
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #4
	bhi _0802B15E
	ldrb r0, [r2, #8]
	asr r0, r4
	mov r1, #1
	and r0, r1
	cmp r0, #0
	bne _0802B146
_0802B15C:
	strb r4, [r3]
_0802B15E:
	mov r0, #7
	strb r0, [r3, #1]
	ldrb r1, [r2, #8]
	mov r0, #0x1f
	and r0, r1
	cmp r0, #0x1f
	bne _0802B170
	mov r0, #0
	strb r0, [r3]
_0802B170:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global ls_setUnlockedLevels
ls_setUnlockedLevels: @ 0x0802B180
	push {r4, r5, r6, r7, lr}
	ldr r3, =0x0300058C
	mov r0, #7
	strb r0, [r3, #1]
	ldr r2, =0x03003D40
	ldrb r1, [r2, #9]
	and r0, r1
	add r5, r2, #0
	cmp r0, #7
	bne _0802B198
	mov r0, #0xf
	strb r0, [r3, #1]
_0802B198:
	ldrb r1, [r5, #9]
	mov r0, #8
	and r0, r1
	cmp r0, #0
	beq _0802B1AA
	ldrb r0, [r3, #1]
	mov r1, #0x10
	orr r0, r1
	strb r0, [r3, #1]
_0802B1AA:
	mov r0, #0
	strb r0, [r3, #2]
	mov r2, #0
	add r7, r5, #0
	mov r4, #1
	add r6, r3, #0
_0802B1B6:
	ldrb r0, [r7, #9]
	asr r0, r2
	and r0, r4
	cmp r0, #0
	bne _0802B1D4
	ldrb r0, [r6, #1]
	asr r0, r2
	and r0, r4
	cmp r0, #0
	beq _0802B1D4
	add r0, r4, #0
	lsl r0, r2
	ldrb r1, [r3, #2]
	orr r0, r1
	strb r0, [r3, #2]
_0802B1D4:
	add r0, r2, #1
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	cmp r2, #4
	bls _0802B1B6
	ldrb r0, [r5, #8]
	mov r1, #0x1f
	and r1, r0
	cmp r1, #0x1f
	bne _0802B1EA
	strb r1, [r3, #2]
_0802B1EA:
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global levelSelect_updateSelection
levelSelect_updateSelection: @ 0x0802B1F8
	push {r4, r5, lr}
	ldr r0, =0x03002080
	ldrh r1, [r0, #0xc]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _0802B224
	mov r0, #0xcd
	bl playSong
	ldr r1, =0x03000588
	mov r2, #1
	neg r2, r2
	add r0, r2, #0
	strh r0, [r1]
	mov r0, #1
	b _0802B346
	.align 2, 0
.pool
_0802B224:
	mov r0, #9
	and r0, r1
	cmp r0, #0
	beq _0802B24C
	ldr r0, =0x0300058C
	ldrb r0, [r0]
	cmp r0, #0xff
	beq _0802B24C
	mov r0, #0xcc
	bl playSong
	ldr r1, =0x0300058A
	mov r0, #4
	strb r0, [r1]
	mov r0, #1
	b _0802B346
	.align 2, 0
.pool
_0802B24C:
	ldr r0, =0x03002080
	ldrh r1, [r0, #0x14]
	mov r0, #0xc0
	and r0, r1
	cmp r0, #0
	beq _0802B2A6
	ldr r5, =0x0300058C
	ldrb r0, [r5]
	cmp r0, #2
	bhi _0802B2A6
	add r4, r0, #0
	mov r0, #0x40
	and r0, r1
	cmp r0, #0
	beq _0802B284
	add r0, r4, #2
	mov r1, #3
	bl Bios_modulo
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0
	b _0802B292
	.align 2, 0
.pool
_0802B284:
	add r0, r4, #1
	mov r1, #3
	bl Bios_modulo
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #1
_0802B292:
	bl levelSelect_getNewPosVertical
	strb r0, [r5]
	ldr r0, =0x0300058C
	ldrb r0, [r0]
	cmp r4, r0
	beq _0802B2A6
	mov r0, #0xcb
	bl playSong
_0802B2A6:
	ldr r0, =0x03002080
	ldrh r1, [r0, #0x14]
	mov r0, #0x30
	and r0, r1
	cmp r0, #0
	beq _0802B344
	ldr r0, =0x0300058C
	ldrb r4, [r0]
	mov r0, #0x20
	and r0, r1
	cmp r0, #0
	beq _0802B2F6
	cmp r4, #3
	beq _0802B2DE
	cmp r4, #3
	ble _0802B2D4
	cmp r4, #4
	beq _0802B2E4
	b _0802B2EA
	.align 2, 0
.pool
_0802B2D4:
	cmp r4, #0
	blt _0802B2EA
	mov r1, #4
	lsl r4, r4, #0x10
	b _0802B2F0
_0802B2DE:
	mov r1, #1
	lsl r4, r4, #0x10
	b _0802B2F0
_0802B2E4:
	mov r1, #3
	lsl r4, r4, #0x10
	b _0802B2F0
_0802B2EA:
	lsl r0, r4, #0x10
	lsr r1, r0, #0x10
	add r4, r0, #0
_0802B2F0:
	add r0, r1, #0
	mov r1, #0
	b _0802B324
_0802B2F6:
	cmp r4, #3
	beq _0802B30E
	cmp r4, #3
	ble _0802B304
	cmp r4, #4
	beq _0802B314
	b _0802B31A
_0802B304:
	cmp r4, #0
	blt _0802B31A
	mov r1, #3
	lsl r4, r4, #0x10
	b _0802B320
_0802B30E:
	mov r1, #4
	lsl r4, r4, #0x10
	b _0802B320
_0802B314:
	mov r1, #1
	lsl r4, r4, #0x10
	b _0802B320
_0802B31A:
	lsl r0, r4, #0x10
	lsr r1, r0, #0x10
	add r4, r0, #0
_0802B320:
	add r0, r1, #0
	mov r1, #1
_0802B324:
	bl sub_0802B3A0
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	cmp r0, #0
	blt _0802B334
	ldr r0, =0x0300058C
	strb r1, [r0]
_0802B334:
	asr r1, r4, #0x10
	ldr r0, =0x0300058C
	ldrb r0, [r0]
	cmp r1, r0
	beq _0802B344
	mov r0, #0xcb
	bl playSong
_0802B344:
	mov r0, #0
_0802B346:
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global levelSelect_getNewPosVertical
levelSelect_getNewPosVertical: @ 0x0802B350
	push {r4, r5, r6, lr}
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	lsl r1, r1, #0x10
	lsr r6, r1, #0x10
	mov r4, #0
_0802B35C:
	cmp r6, #0
	beq _0802B364
	add r0, r5, r4
	b _0802B368
_0802B364:
	sub r0, r4, #3
	sub r0, r5, r0
_0802B368:
	mov r1, #3
	bl Bios_modulo
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	ldr r0, =0x0300058C
	ldrb r0, [r0, #2]
	asr r0, r2
	mov r1, #1
	and r0, r1
	cmp r0, #0
	beq _0802B38C
	lsl r0, r2, #0x10
	asr r0, r0, #0x10
	b _0802B39A
	.align 2, 0
.pool
_0802B38C:
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #2
	bls _0802B35C
	mov r0, #1
	neg r0, r0
_0802B39A:
	pop {r4, r5, r6}
	pop {r1}
	bx r1

thumb_func_global sub_0802B3A0
sub_0802B3A0: @ 0x0802B3A0
	push {r4, r5, r6, lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r6, r1, #0x10
	cmp r0, #2
	bhi _0802B3EC
	ldr r0, =0x0300058C
	ldrb r1, [r0, #2]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _0802B3C4
	mov r0, #1
	b _0802B426
	.align 2, 0
.pool
_0802B3C4:
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _0802B3D0
	mov r0, #0
	b _0802B426
_0802B3D0:
	mov r0, #4
	and r0, r1
	cmp r0, #0
	beq _0802B3DC
	mov r0, #2
	b _0802B426
_0802B3DC:
	mov r0, #4
	sub r0, r0, r6
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	b _0802B3EE
_0802B3E6:
	lsl r0, r2, #0x10
	asr r0, r0, #0x10
	b _0802B426
_0802B3EC:
	add r5, r0, #0
_0802B3EE:
	mov r4, #0
_0802B3F0:
	cmp r6, #0
	beq _0802B3FA
	sub r0, r4, #3
	add r0, r5, r0
	b _0802B3FE
_0802B3FA:
	add r0, r4, #1
	sub r0, r5, r0
_0802B3FE:
	mov r1, #2
	bl Bios_modulo
	add r0, #3
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	ldr r0, =0x0300058C
	ldrb r0, [r0, #2]
	asr r0, r2
	mov r1, #1
	and r0, r1
	cmp r0, #0
	bne _0802B3E6
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #1
	bls _0802B3F0
	mov r0, #1
	neg r0, r0
_0802B426:
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_0802B430
sub_0802B430: @ 0x0802B430
	push {r4, lr}
	ldr r0, =0x0300058A
	ldrb r0, [r0]
	cmp r0, #4
	bhi _0802B4F6
	lsl r0, r0, #2
	ldr r1, =_0802B44C
	add r0, r0, r1
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
.pool
_0802B44C: @ jump table
	.4byte _0802B460 @ case 0
	.4byte _0802B472 @ case 1
	.4byte _0802B4A8 @ case 2
	.4byte _0802B4C0 @ case 3
	.4byte _0802B4F0 @ case 4
_0802B460:
	bl nullsub_7
	bl sub_0802B588
	bl sub_0802B658
	bl nullsub_8
	b _0802B4F6
_0802B472:
	bl sub_0802B6BC
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0802B4F6
	ldr r0, =0x03000584
	mov r4, #0
	str r4, [r0]
	ldr r2, =0x03003D40
	ldrb r1, [r2, #8]
	add r0, r1, #0
	cmp r0, #7
	bne _0802B4A0
	ldr r1, =0x0300058A
	mov r0, #2
	strb r0, [r1]
	b _0802B4F6
	.align 2, 0
.pool
_0802B4A0:
	cmp r0, #0xf
	beq _0802B4B2
	strb r1, [r2, #9]
	b _0802B4D6
_0802B4A8:
	bl return_1_
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0802B4F6
_0802B4B2:
	ldr r1, =0x0300058A
	mov r0, #3
	strb r0, [r1]
	b _0802B4F6
	.align 2, 0
.pool
_0802B4C0:
	bl sub_0802B508
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0802B4F6
	ldr r0, =0x03003D40
	ldrb r1, [r0, #8]
	mov r4, #0
	strb r1, [r0, #9]
	ldr r0, =0x03000584
	str r4, [r0]
_0802B4D6:
	bl ls_defaultCursorPos
	bl ls_setUnlockedLevels
	ldr r0, =0x0300058A
	strb r4, [r0]
	b _0802B4F6
	.align 2, 0
.pool
_0802B4F0:
	ldr r1, =0x03000588
	mov r0, #2
	strh r0, [r1]
_0802B4F6:
	mov r0, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global nullsub_7
nullsub_7: @ 0x0802B504
	bx lr
	.align 2, 0

thumb_func_global sub_0802B508
sub_0802B508: @ 0x0802B508
	push {r4, r5, r6, r7, lr}
	ldr r0, =0x03000584
	ldr r1, [r0]
	add r1, #1
	str r1, [r0]
	mov r2, #1
	and r1, r2
	add r7, r0, #0
	cmp r1, #0
	bne _0802B57E
	ldr r4, =0x03003D90
	ldrh r0, [r4, #2]
	lsr r6, r0, #8
	mov r2, #0xff
	and r2, r0
	mov r5, #0x5c
	mov r3, #0x90
	ldr r0, =0x03003C00
	ldrb r0, [r0]
	lsl r0, r0, #0x1a
	lsr r0, r0, #0x1a
	mov r1, #8
	and r0, r1
	cmp r0, #0
	beq _0802B53E
	mov r5, #0xb0
	mov r3, #0
_0802B53E:
	add r0, r6, #1
	cmp r0, r5
	blt _0802B576
	cmp r3, #0
	bne _0802B568
	ldr r2, =0x03003CD4
	ldrh r1, [r2]
	ldr r0, =0x0000BFFF
	and r0, r1
	strh r0, [r2]
	b _0802B56E
	.align 2, 0
.pool
_0802B568:
	lsl r0, r3, #8
	orr r2, r0
	strh r2, [r4, #2]
_0802B56E:
	mov r0, #0
	str r0, [r7]
	mov r0, #1
	b _0802B580
_0802B576:
	lsl r0, r0, #0x10
	lsr r0, r0, #8
	orr r2, r0
	strh r2, [r4, #2]
_0802B57E:
	mov r0, #0
_0802B580:
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global sub_0802B588
sub_0802B588: @ 0x0802B588
	push {r4, r5, r6, lr}
	ldr r6, =0x0300058C
	ldrb r0, [r6]
	cmp r0, #0xff
	beq _0802B630
	ldr r0, =0x03000590
	ldr r2, [r0]
	ldr r1, =word_8206014
	ldrb r0, [r6]
	lsl r0, r0, #2
	add r0, r0, r1
	ldrh r0, [r0]
	strh r0, [r2, #0x10]
	ldrb r0, [r6]
	lsl r0, r0, #2
	add r1, #2
	add r0, r0, r1
	ldrh r0, [r0]
	strh r0, [r2, #0x12]
	add r2, #0x23
	ldrb r1, [r2]
	mov r0, #0x11
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	bl bufferLevelNameStr
	ldr r4, =word_8205FC0
	ldrb r1, [r6]
	lsl r1, r1, #2
	add r0, r1, r4
	ldrh r0, [r0]
	add r5, r4, #2
	add r1, r1, r5
	ldrh r1, [r1]
	bl ls_setLevelNameStrPos
	bl levelSelectStrings_getX
	add r1, r0, #0
	ldrb r0, [r6]
	lsl r0, r0, #2
	add r0, r0, r4
	ldrh r0, [r0]
	sub r1, r1, r0
	mov r0, #0x80
	sub r0, r0, r1
	mov r1, #2
	bl Bios_Div
	add r0, #0x60
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldrb r1, [r6]
	lsl r1, r1, #2
	add r1, r1, r5
	ldrh r1, [r1]
	bl ls_setLevelNameStrPos
	bl sub_0802BCC0
	bl sub_0802BDA4
	add r4, r0, #0
	bl sub_0802BD8C
	sub r4, r4, r0
	mov r0, #0xe0
	sub r0, r0, r4
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x1f
	bl setStarStringPos
	b _0802B646
	.align 2, 0
.pool
_0802B630:
	ldr r0, =0x03000590
	ldr r1, [r0]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	bl levelSelect_HideLevelName
	bl sub_0802BD74
_0802B646:
	ldr r0, =0x03000590
	ldr r0, [r0]
	bl updateSpriteAnimation
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802B658
sub_0802B658: @ 0x0802B658
	push {r4, r5, r6, r7, lr}
	mov r3, #0
	ldr r7, =0x03003D40
	mov r5, #1
	ldr r4, =0x03000598
	mov r6, #0x11
	neg r6, r6
_0802B666:
	ldrb r0, [r7, #8]
	asr r0, r3
	and r0, r5
	cmp r0, #0
	beq _0802B67C
	ldr r0, =0x0300058C
	ldrb r0, [r0, #1]
	asr r0, r3
	and r0, r5
	cmp r0, #0
	bne _0802B69C
_0802B67C:
	lsl r0, r3, #2
	add r0, r0, r4
	ldr r2, [r0]
	add r2, #0x23
	ldrb r0, [r2]
	mov r1, #0x10
	orr r0, r1
	strb r0, [r2]
	b _0802B6AC
	.align 2, 0
.pool
_0802B69C:
	lsl r0, r3, #2
	add r0, r0, r4
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	add r0, r6, #0
	and r0, r2
	strb r0, [r1]
_0802B6AC:
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, #4
	bls _0802B666
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0

thumb_func_global sub_0802B6BC
sub_0802B6BC: @ 0x0802B6BC
	push {r4, r5, lr}
	mov r3, #0
	ldr r1, =0x03003D40
	ldrb r0, [r1, #8]
	mov r2, #1
	and r0, r2
	cmp r0, #0
	beq _0802B6DC
	ldrb r0, [r1, #9]
	and r0, r2
	cmp r0, #0
	bne _0802B6DC
	mov r5, #0
	b _0802B6FE
	.align 2, 0
.pool
_0802B6DC:
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, #4
	bhi _0802B6FE
	ldrb r0, [r1, #8]
	asr r0, r3
	mov r2, #1
	and r0, r2
	cmp r0, #0
	beq _0802B6DC
	ldrb r0, [r1, #9]
	asr r0, r3
	and r0, r2
	cmp r0, #0
	bne _0802B6DC
	add r5, r3, #0
_0802B6FE:
	ldr r4, =0x03000584
	ldr r0, [r4]
	cmp r0, #0
	bne _0802B70C
	ldr r0, =0x00000141
	bl playSong
_0802B70C:
	ldr r0, [r4]
	mov r1, #8
	and r0, r1
	cmp r0, #0
	beq _0802B734
	ldr r1, =0x03000598
	lsl r0, r5, #2
	add r0, r0, r1
	ldr r1, [r0]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	b _0802B746
	.align 2, 0
.pool
_0802B734:
	ldr r1, =0x03000598
	lsl r0, r5, #2
	add r0, r0, r1
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
_0802B746:
	strb r0, [r1]
	ldr r0, =0x03000584
	ldr r1, [r0]
	add r2, r1, #0
	add r1, #1
	str r1, [r0]
	cmp r2, #0x30
	bhi _0802B764
	mov r0, #0
	b _0802B766
	.align 2, 0
.pool
_0802B764:
	mov r0, #1
_0802B766:
	pop {r4, r5}
	pop {r1}
	bx r1

thumb_func_global nullsub_8
nullsub_8: @ 0x0802B76C
	bx lr
	.align 2, 0

thumb_func_global return_1_
return_1_: @ 0x0802B770
	mov r0, #1
	bx lr

thumb_func_global levelSelect_loadSprites
levelSelect_loadSprites: @ 0x0802B774
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #4
	ldr r0, =levelSelectIconPal
	mov r1, #1
	bl LoadSpritePalette
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	mov r0, #0x6c
	bl allocateSpriteTiles
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	ldr r0, =unk_8205BA4
	lsl r1, r7, #5
	ldr r2, =0x06010000
	add r1, r1, r2
	bl Bios_LZ77_16_2
	ldr r0, =off_8205E24
	mov r1, #0
	str r1, [sp]
	add r2, r7, #0
	mov r3, #2
	bl allocSprite
	ldr r3, =0x03000590
	str r0, [r3]
	add r0, #0x23
	ldrb r1, [r0]
	mov r2, #0x10
	orr r1, r2
	strb r1, [r0]
	ldr r0, [r3]
	add r0, #0x22
	strb r6, [r0]
	mov r5, #0
	ldr r0, =word_8206014
	mov r8, r0
	ldr r1, =0x030020AC
	mov sl, r1
_0802B7CE:
	mov r2, sl
	ldrb r0, [r2]
	lsl r0, r0, #2
	ldr r1, =off_8206028
	add r0, r0, r1
	ldr r0, [r0]
	mov r2, #0
	mov sb, r2
	str r2, [sp]
	mov r1, #0
	add r2, r7, #0
	mov r3, #2
	bl allocSprite
	ldr r3, =0x03000598
	lsl r4, r5, #2
	add r3, r4, r3
	str r0, [r3]
	add r0, #0x23
	ldrb r1, [r0]
	mov r2, #0x10
	orr r1, r2
	strb r1, [r0]
	ldr r0, [r3]
	add r0, #0x22
	strb r6, [r0]
	ldr r1, [r3]
	mov r2, r8
	add r0, r4, r2
	ldrh r0, [r0]
	add r0, #4
	strh r0, [r1, #0x10]
	ldr r1, [r3]
	ldr r0, =word_8206016
	add r4, r4, r0
	ldrh r0, [r4]
	add r0, #0xc
	strh r0, [r1, #0x12]
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, #4
	bls _0802B7CE
	ldr r0, =dword_820918C
	mov r1, #1
	bl LoadSpritePalette
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	mov r0, #0xc
	bl allocateSpriteTiles
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	ldr r0, =buttonTiles
	lsl r1, r7, #5
	ldr r2, =0x06010000
	add r1, r1, r2
	bl Bios_LZ77_16_2
	ldr r0, =off_820938C
	mov r1, sb
	str r1, [sp]
	mov r1, #0
	add r2, r7, #0
	mov r3, #2
	bl allocSprite
	ldr r2, =0x030005AC
	str r0, [r2]
	add r0, #0x23
	ldrb r3, [r0]
	mov r4, #0x11
	neg r4, r4
	add r1, r4, #0
	and r1, r3
	strb r1, [r0]
	ldr r0, [r2]
	add r0, #0x22
	strb r6, [r0]
	ldr r2, [r2]
	ldr r1, =word_8205FD4
	ldr r5, =0x03002080
	add r5, #0x2c
	ldrb r0, [r5]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	strh r0, [r2, #0x10]
	mov r0, #0x90
	mov r8, r0
	mov r1, r8
	strh r1, [r2, #0x12]
	ldr r0, =off_82093A4
	mov r2, sb
	str r2, [sp]
	mov r1, #0
	add r2, r7, #0
	mov r3, #2
	bl allocSprite
	ldr r3, =0x030005B0
	str r0, [r3]
	add r0, #0x23
	ldrb r2, [r0]
	add r1, r4, #0
	and r1, r2
	strb r1, [r0]
	ldr r0, [r3]
	add r0, #0x22
	strb r6, [r0]
	ldr r2, [r3]
	ldr r1, =word_8205FDA
	ldrb r0, [r5]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	strh r0, [r2, #0x10]
	mov r0, r8
	strh r0, [r2, #0x12]
	ldr r0, =off_82093BC
	mov r1, sb
	str r1, [sp]
	mov r1, #0
	add r2, r7, #0
	mov r3, #2
	bl allocSprite
	ldr r2, =0x030005B4
	str r0, [r2]
	add r0, #0x23
	ldrb r1, [r0]
	and r4, r1
	strb r4, [r0]
	ldr r0, [r2]
	add r0, #0x22
	strb r6, [r0]
	ldr r2, [r2]
	ldr r1, =dword_8205FE0
	ldrb r0, [r5]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	strh r0, [r2, #0x10]
	mov r0, r8
	strh r0, [r2, #0x12]
	add sp, #4
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global levelSelect_loadStrMissionSel
levelSelect_loadStrMissionSel: @ 0x0802B95C
	push {lr}
	sub sp, #4
	ldr r1, =string_SelectMission
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000620
	mov r2, #0
	str r2, [sp]
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global setSelectMissionStrPos
setSelectMissionStrPos: @ 0x0802B98C
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =string_SelectMission
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000620
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global levelSelect_getSelMissionStrX
levelSelect_getSelMissionStrX: @ 0x0802B9C8
	push {lr}
	ldr r1, =string_SelectMission
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0
	bl str_getX_ofFirstChar
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global bufferLevelNameStr
bufferLevelNameStr: @ 0x0802B9F0
	push {r4, r5, r6, lr}
	sub sp, #4
	mov r4, #0
	ldr r5, =0x030037D0
	add r6, r5, #0
	add r6, #0xd0
_0802B9FC:
	add r0, r4, #0
	add r0, #0xd
	lsl r0, r0, #4
	add r1, r5, #0
	add r1, #8
	add r0, r0, r1
	ldr r0, [r0]
	ldrb r1, [r0, #0xf]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _0802BA1C
	lsl r0, r4, #4
	add r0, r0, r6
	bl freeSimpleSpriteHeader
_0802BA1C:
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0xe
	bls _0802B9FC
	ldr r2, =0x0300058C
	ldrb r0, [r2]
	cmp r0, #0xff
	beq _0802BA50
	ldr r1, =string_levelNames
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldrb r1, [r2]
	lsl r1, r1, #5
	add r0, r0, r1
	ldr r1, =0x03000628
	mov r2, #0
	str r2, [sp]
	mov r2, #0xd
	mov r3, #2
	bl bufferString
_0802BA50:
	add sp, #4
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global ls_setLevelNameStrPos
ls_setLevelNameStrPos: @ 0x0802BA6C
	push {r4, lr}
	sub sp, #4
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	lsl r1, r1, #0x10
	lsr r2, r1, #0x10
	ldr r3, =0x0300058C
	ldrb r0, [r3]
	cmp r0, #0xff
	beq _0802BAA0
	ldr r1, =string_levelNames
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldrb r1, [r3]
	lsl r1, r1, #5
	add r0, r0, r1
	ldr r1, =0x03000628
	str r1, [sp]
	add r1, r4, #0
	mov r3, #0xd
	bl setStringPos
_0802BAA0:
	add sp, #4
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global unused_sub_802BAB8
unused_sub_802BAB8: @ 0x0802BAB8
	push {lr}
	ldr r2, =0x0300058C
	ldrb r0, [r2]
	cmp r0, #0xff
	beq _0802BADC
	ldr r1, =string_levelNames
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldrb r1, [r2]
	lsl r1, r1, #5
	add r0, r0, r1
	mov r1, #0xd
	bl showString
_0802BADC:
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global levelSelect_HideLevelName
levelSelect_HideLevelName: @ 0x0802BAEC
	push {lr}
	ldr r2, =0x0300058C
	ldrb r0, [r2]
	cmp r0, #0xff
	beq _0802BB10
	ldr r1, =string_levelNames
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldrb r1, [r2]
	lsl r1, r1, #5
	add r0, r0, r1
	mov r1, #0xd
	bl hideString
_0802BB10:
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global levelSelectStrings_getX
levelSelectStrings_getX: @ 0x0802BB20
	push {lr}
	ldr r1, =string_levelNames
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03003D40
	ldrb r1, [r1, #1]
	lsl r1, r1, #5
	add r0, r0, r1
	mov r1, #0xd
	bl str_get_last_width_OamX_sum
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global levelSelect_drawSelMissStr
levelSelect_drawSelMissStr: @ 0x0802BB50
	push {lr}
	sub sp, #4
	ldr r1, =string_select
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000630
	mov r2, #0
	str r2, [sp]
	mov r2, #0x1c
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global ls_setSelectStrPos
ls_setSelectStrPos: @ 0x0802BB84
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =string_select
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000630
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0x1c
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global ls_drawStrOK
ls_drawStrOK: @ 0x0802BBC0
	push {lr}
	sub sp, #4
	ldr r1, =string_ok
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000630
	mov r2, #0
	str r2, [sp]
	mov r2, #0x22
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global ls_setOkStrPos
ls_setOkStrPos: @ 0x0802BBF4
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =string_ok
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000630
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0x22
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global ls_loadBackStr
ls_loadBackStr: @ 0x0802BC30
	push {lr}
	sub sp, #4
	ldr r1, =string_back
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000630
	mov r2, #0
	str r2, [sp]
	mov r2, #0x28
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global ls_setBackStrPos
ls_setBackStrPos: @ 0x0802BC64
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =string_back
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000630
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0x28
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global getStarString
getStarString: @ 0x0802BCA0
	lsl r0, r0, #0x10
	ldr r3, =starStrings
	lsr r0, r0, #0xe
	ldr r1, =0x03003D40
	ldrb r2, [r1, #0x11]
	lsl r1, r2, #2
	add r1, r1, r2
	lsl r1, r1, #2
	add r0, r0, r1
	add r0, r0, r3
	ldr r0, [r0]
	bx lr
	.align 2, 0
.pool

thumb_func_global sub_0802BCC0
sub_0802BCC0: @ 0x0802BCC0
	push {r4, r5, r6, lr}
	sub sp, #4
	mov r4, #0
	ldr r5, =0x030037D0
	mov r0, #0xb0
	lsl r0, r0, #2
	add r6, r5, r0
_0802BCCE:
	add r0, r4, #0
	add r0, #0x2c
	lsl r0, r0, #4
	add r1, r5, #0
	add r1, #8
	add r0, r0, r1
	ldr r0, [r0]
	ldrb r1, [r0, #0xf]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _0802BCEE
	lsl r0, r4, #4
	add r0, r0, r6
	bl freeSimpleSpriteHeader
_0802BCEE:
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #8
	bls _0802BCCE
	ldr r0, =0x0300058C
	ldrb r0, [r0]
	bl getStarString
	ldr r1, =0x03000630
	mov r2, #0
	str r2, [sp]
	mov r2, #0x2c
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global setStarStringPos
setStarStringPos: @ 0x0802BD24
	push {r4, r5, lr}
	sub sp, #4
	add r4, r0, #0
	add r5, r1, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	ldr r0, =0x0300058C
	ldrb r0, [r0]
	bl getStarString
	ldr r1, =0x03000630
	str r1, [sp]
	add r1, r4, #0
	add r2, r5, #0
	mov r3, #0x2c
	bl setStringPos
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global unused_sub_802BD5C
unused_sub_802BD5C: @ 0x0802BD5C
	push {lr}
	ldr r0, =0x0300058C
	ldrb r0, [r0]
	bl getStarString
	mov r1, #0x2c
	bl showString
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802BD74
sub_0802BD74: @ 0x0802BD74
	push {lr}
	ldr r0, =0x0300058C
	ldrb r0, [r0]
	bl getStarString
	mov r1, #0x2c
	bl hideString
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802BD8C
sub_0802BD8C: @ 0x0802BD8C
	push {lr}
	ldr r0, =0x0300058C
	ldrb r0, [r0]
	bl getStarString
	mov r1, #0x2c
	bl str_getX_ofFirstChar
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_0802BDA4
sub_0802BDA4: @ 0x0802BDA4
	push {lr}
	ldr r0, =0x0300058C
	ldrb r0, [r0]
	bl getStarString
	mov r1, #0x2c
	bl str_get_last_width_OamX_sum
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_0802BDBC
sub_0802BDBC: @ 0x0802BDBC
	push {r4, r5, lr}
	sub sp, #4
	ldr r1, =pts_strings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r5, [r0]
	ldr r4, =0x03000628
	mov r0, #0
	str r0, [sp]
	add r0, r5, #0
	add r1, r4, #0
	mov r2, #0x35
	mov r3, #2
	bl bufferString
	str r4, [sp]
	add r0, r5, #0
	mov r1, #0xc0
	mov r2, #0x3a
	mov r3, #0x35
	bl setStringPos
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802BE04
sub_0802BE04: @ 0x0802BE04
	push {r4, r5, lr}
	sub sp, #4
	mov r0, #0xb
	bl allocateSpriteTiles
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	asr r1, r0, #0x10
	cmp r1, #0
	blt _0802BE8C
	ldr r0, =score_numberTiles
	lsl r1, r1, #5
	ldr r2, =0x06010000
	add r1, r1, r2
	bl Bios_LZ77_16_2
	ldr r0, =dword_8208DA0
	mov r1, #1
	bl LoadSpritePalette
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r0, #0
	blt _0802BE8C
	ldr r0, =0x03003D40
	ldrb r0, [r0]
	cmp r0, #0
	bne _0802BE5C
	ldr r1, =0x030005C0
	mov r0, #0
	mov r2, #0
	bl createScoreString
	b _0802BE6E
	.align 2, 0
.pool
_0802BE5C:
	ldr r0, =0x03003C00
	add r0, #0xcc
	ldrh r1, [r0]
	mov r0, #0x64
	mul r0, r1, r0
	ldr r1, =0x030005C0
	mov r2, #0
	bl createScoreString
_0802BE6E:
	ldr r0, =unk_8205FFC
	add r2, r5, #0
	mov r1, #0
	str r1, [sp]
	mov r3, #2
	bl allocSprite
	ldr r1, =0x030005B8
	str r0, [r1]
	mov r1, #0x86
	strh r1, [r0, #0x10]
	mov r1, #0x3a
	strh r1, [r0, #0x12]
	add r0, #0x22
	strb r4, [r0]
_0802BE8C:
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool
