.include "asm/macros.inc"



/*	void drawSpriteToBuffer(struct Sprite* sprite)
	- Creates OAMs from sprite and stores them in OAM buffer
	- This function is copied to IWRAM and executed there
*/
arm_func_global drawSpriteToBuffer
drawSpriteToBuffer: @ 0x083B8468
	mov ip, sp
	push {r4, r5, r6, r7, r8, sb, sl, fp, ip, lr, pc}
	sub sp, sp, #0x24
	mov sl, r0
	ldrb r5, [sl, #0x23]
	sub fp, ip, #4
	and r3, r5, #0x11
	cmp r3, #1
	bne _083B89E0
	ldrsh r0, [sl, #0xc]
	ldr r4, =0x03002820
	ldr lr, =0x03002420
	str r0, [fp, #-0x38]
	ldrb r6, [r4]
	ldrh r3, [sl, #0xe]
	ldr ip, [sl, #4]
	add r3, r3, r3, lsl #1
	lsl r3, r3, #2
	ldr r1, [ip, r3]
	mov r2, #0
	str r2, [fp, #-0x2c]
	ldrsh r0, [r1, #2]
	ldrh r2, [r1]
	cmp r0, #0
	movne r0, #2
	moveq r0, #1
	str r0, [fp, #-0x34]
	add r8, lr, r6, lsl #3
	ldr r0, [fp, #-0x2c]
	add ip, ip, r3
	str ip, [fp, #-0x30]
	cmp r0, r2
	bhs _083B89E0
	tst r6, #0x80
	bne _083B89E0
	and r1, r5, #0xff
	lsr r3, r1, #5
	str r1, [fp, #-0x44]
	and r3, r3, #3
	str r3, [fp, #-0x48]
	and r5, r5, #2
	str r5, [fp, #-0x4c]
_083B8510:
	sub r0, fp, #0x30
	ldm r0, {r0, r2}
	ldr r1, [fp, #-0x44]
	add r3, r2, r2, lsl #1
	lsl r3, r3, #2
	add r5, r3, #0xc
	lsr r3, r1, #2
	and r3, r3, #1
	ldr r6, [r0]
	cmp r3, #0
	ldrb r2, [sl, #0x25]
	add sb, r6, r5
	ldrb r0, [r8, #1]
	and r2, r2, #3
	ldrb ip, [sb, #7]
	bic r0, r0, #0xc
	orr r0, r0, r2, lsl #2
	ldr r2, [fp, #-0x34]
	orrne r0, r0, #0x10
	andeq r0, r0, #0xef
	lsr r3, r2, #1
	cmp r3, #0
	ldrb r2, [r8, #5]
	orrne r0, r0, #0x20
	andeq r0, r0, #0xdf
	strb r0, [r8, #1]
	ldr r3, [fp, #-0x48]
	bic r2, r2, #0xc
	orr r2, r2, r3, lsl #2
	strb r2, [r8, #5]
	ldrsb r1, [sl, #0x22]
	ldrb r3, [sb, #6]
	bic r2, r2, #0xf0
	add r1, r1, r3
	and r1, r1, #0xf
	orr r2, r2, r1, lsl #4
	lsr r1, ip, #4
	and r1, r1, #3
	strb r2, [r8, #5]
	lsr ip, ip, #6
	ldrb r3, [r8, #3]
	orr r1, r1, ip, lsl #2
	ldr ip, =objShapeLUT
	ldr r2, =objSizeLUT
	bic r0, r0, #0xc0
	ldrb r4, [r2, r1]
	bic r3, r3, #0xc0
	and lr, r4, #3
	ldrb r2, [ip, r1]
	orr r3, r3, lr, lsl #6
	strb r3, [r8, #3]
	and r1, r2, #3
	orr r0, r0, r1, lsl #6
	strb r0, [r8, #1]
	ldr r1, =objDimensionHalves+1
	lsl r2, r2, #3
	ldr r0, =objDimensionHalves
	add r2, r2, r4, lsl #1
	ldrb r0, [r0, r2]
	str r0, [fp, #-0x3c]
	ldrb r2, [r1, r2]
	ldr r3, [sl, #8]
	str r2, [fp, #-0x40]
	cmp r3, #0
	beq _083B8668
	ldr r0, [fp, #-0x38]
	ldrh r2, [r8, #4]
	lsl r3, r0, #0x16
	orr r3, r3, r2, lsr #10
	ror r3, r3, #0x16
	strh r3, [r8, #4]
	ldrb r2, [sb, #7]
	ldr r1, =dword_8038260
	lsr r3, r2, #3
	and r3, r3, #6
	lsr r2, r2, #6
	orr r3, r3, r2, lsl #3
	lsl r2, r0, #0x10
	ldrh r0, [r1, r3]
	ldr r3, [fp, #-0x34]
	lsr r2, r2, #0x10
	mla r3, r0, r3, r2
	lsl r3, r3, #0x10
	asr r3, r3, #0x10
	str r3, [fp, #-0x38]
	b _083B8688
_083B8668:
	ldrh r3, [sl, #0xc]
	ldrh r2, [r6, r5]
	ldrh r1, [r8, #4]
	add r3, r3, r2
	lsl r3, r3, #0x16
	orr r3, r3, r1, lsr #10
	ror r3, r3, #0x16
	strh r3, [r8, #4]
_083B8688:
	ldrh r3, [sl, #0x1c]
	cmp r3, #0
	bne _083B86A8
	mov r3, #0x100
	ldr r2, [sl, #0x14]
	add r3, r3, #0x1000000
	cmp r2, r3
	beq _083B87E8
_083B86A8:
	ldrb r3, [r8, #1]
	ldrb r1, [sl, #0x24]
	ldrb r2, [r8, #3]
	ldr ip, =0x03002F50
	orr r3, r3, #3
	lsr r1, r1, #3
	and r0, r1, #0xff
	and r1, r1, #7
	bic r2, r2, #0xe
	orr r2, r2, r1, lsl #1
	strb r3, [r8, #1]
	lsr r1, r0, #4
	lsr r3, r0, #3
	and r3, r3, #1
	cmp r3, #0
	orrne r2, r2, #0x10
	andeq r2, r2, #0xef
	cmp r1, #0
	orrne r2, r2, #0x20
	andeq r2, r2, #0xdf
	strb r2, [r8, #3]
	ldr r1, [fp, #-0x40]
	ldrsh r6, [sl, #0x12]
	lsl r0, r0, #3
	ldrsh lr, [sb, #4]
	add r5, r0, ip
	ldrsh r3, [sl, #0x1a]
	add lr, r6, lr
	add lr, lr, r1
	add r6, r6, r3
	ldrsh r3, [r5, #2]
	rsb lr, r6, lr
	mul r7, lr, r3
	ldrsh r4, [sl, #0x10]
	ldrsh r1, [sb, #2]
	ldr r2, [fp, #-0x3c]
	add r1, r4, r1
	ldrsh r3, [sl, #0x18]
	add r1, r1, r2
	ldrsh r2, [ip, r0]
	add r4, r4, r3
	rsb r1, r4, r1
	mla r0, r1, r2, r7
	ldrsh r3, [r5, #6]
	mul ip, lr, r3
	ldrsh r2, [r5, #4]
	mla lr, r1, r2, ip
	add r0, r0, r4, lsl #8
	lsl r0, r0, #8
	asr r0, r0, #0x10
	ldrh r2, [r8, #2]
	lsl r3, r0, #0x17
	orr r3, r3, r2, lsr #9
	ror r3, r3, #0x17
	strh r3, [r8, #2]
	add lr, lr, r6, lsl #8
	ldr r3, [fp, #-0x40]
	asr lr, lr, #8
	ldrh r1, [r8, #2]
	sub r2, lr, r3, lsl #1
	strb r2, [r8]
	lsl lr, lr, #0x10
	ldr r2, [fp, #-0x3c]
	lsl r3, r1, #0x17
	lsr r3, r3, #0x17
	sub r3, r3, r2, lsl #1
	lsl r3, r3, #0x17
	orr r3, r3, r1, lsr #9
	ror r3, r3, #0x17
	strh r3, [r8, #2]
	asr r1, lr, #0x10
	b _083B894C
	.align 2, 0
.pool
_083B87E8:
	ldrb r3, [r8, #1]
	ldrb r2, [r8, #3]
	bic r3, r3, #3
	strb r3, [r8, #1]
	bic r2, r2, #0xe
	strb r2, [r8, #3]
	ldrb r3, [sb, #7]
	lsr r3, r3, #3
	and r3, r3, #1
	cmp r3, #0
	orrne r2, r2, #0x20
	andeq r2, r2, #0xd1
	strb r2, [r8, #3]
	ldrb r3, [sb, #7]
	ldrb r1, [sl, #0x24]
	lsr r3, r3, #2
	and r3, r3, #1
	cmp r3, #0
	orrne r2, r2, #0x10
	andeq r2, r2, #0xe1
	tst r1, #2
	strb r2, [r8, #3]
	mov ip, r1
	beq _083B8894
	lsr r3, r2, #4
	and r3, r3, #1
	cmp r3, #0
	orreq r3, r2, #0x10
	andne r3, r2, #0xe1
	strb r3, [r8, #3]
	ldrh r2, [sl, #0x10]
	ldrh r3, [sl, #0x18]
	ldrh r1, [sb, #2]
	add r2, r2, r3
	ldr r3, [fp, #-0x3c]
	rsb r2, r1, r2
	ldrh r1, [r8, #2]
	sub r2, r2, r3, lsl #1
	lsl r2, r2, #0x10
	asr r0, r2, #0x10
	lsl r3, r0, #0x17
	orr r3, r3, r1, lsr #9
	b _083B88B4
_083B8894:
	ldrh r3, [sl, #0x10]
	ldrh r2, [sb, #2]
	add r3, r3, r2
	lsl r3, r3, #0x10
	asr r0, r3, #0x10
	ldrh r2, [r8, #2]
	lsl r3, r0, #0x17
	orr r3, r3, r2, lsr #9
_083B88B4:
	ror r3, r3, #0x17
	strh r3, [r8, #2]
	tst ip, #4
	beq _083B890C
	ldrb r2, [r8, #3]
	lsr r3, r2, #5
	and r3, r3, #1
	cmp r3, #0
	orreq r2, r2, #0x20
	andne r2, r2, #0xdf
	strb r2, [r8, #3]
	ldrh r3, [sl, #0x12]
	ldrh r2, [sl, #0x1a]
	ldrh r1, [sb, #4]
	add r3, r3, r2
	rsb r3, r1, r3
	ldr r1, [fp, #-0x40]
	sub r3, r3, r1, lsl #1
	strb r3, [r8]
	lsl r3, r3, #0x10
	asr r1, r3, #0x10
	b _083B8924
_083B890C:
	ldrh r3, [sl, #0x12]
	ldrh r2, [sb, #4]
	add r3, r3, r2
	lsl r1, r3, #0x10
	asr r1, r1, #0x10
	strb r3, [r8]
_083B8924:
	ldr r2, [fp, #-0x3c]
	lsl r3, r0, #0x10
	add r3, r2, r3, lsr #16
	lsl r3, r3, #0x10
	asr r0, r3, #0x10
	ldr r3, [fp, #-0x40]
	lsl r2, r1, #0x10
	add r2, r3, r2, lsr #16
	lsl r2, r2, #0x10
	asr r1, r2, #0x10
_083B894C:
	ldr r2, [fp, #-0x4c]
	cmp r2, #0
	beq _083B8990
	ldr r3, [fp, #-0x3c]
	lsl ip, r3, #1
	rsb r3, ip, r0
	cmp r3, #0xf0
	bgt _083B89A4
	cmn r0, ip
	bmi _083B89A4
	ldr r2, [fp, #-0x40]
	lsl r0, r2, #1
	rsb r3, r0, r1
	cmp r3, #0xa0
	bgt _083B89A4
	cmn r1, r0
	bmi _083B89A4
_083B8990:
	ldr r0, =0x03002820
	ldrb r3, [r0]
	add r8, r8, #8
	add r3, r3, #1
	strb r3, [r0]
_083B89A4:
	ldr r1, [fp, #-0x30]
	ldr r0, [fp, #-0x2c]
	ldr r3, [r1]
	add r2, r0, #1
	ldrh r1, [r3]
	and r2, r2, #0xff
	str r2, [fp, #-0x2c]
	cmp r2, r1
	bhs _083B89E0
	ldr r1, =0x03002820
	ldrsb r3, [r1]
	cmp r3, #0
	b _083B89DC
	.align 2, 0
.pool
_083B89DC:
	bge _083B8510
_083B89E0:
	ldmdb fp, {r4, r5, r6, r7, r8, sb, sl, fp, sp, lr}
	bx lr



@Data at end of rom, used by RNG among other things

.global dword_83B89E8
dword_83B89E8:

.long 0, 0x02020858, 0x020208B0, 0x02020908, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, unk_83B8364
.fill 32
.long 0x1
.fill 0xA4, 4, 0

.global unk_83b8cd8
.long 0x02020674
unk_83b8cd8:
