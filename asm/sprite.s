.include "asm/macros.inc"



thumb_func_global unused_sub_800E508
unused_sub_800E508: @ 0x0800E508
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #8
	add r5, r0, #0
	mov sl, r1
	add r6, r2, #0
	mov sb, r3
	lsl r5, r5, #0x18
	lsr r5, r5, #0x18
	lsl r6, r6, #0x10
	lsr r6, r6, #0x10
	mov r0, sb
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sb, r0
	ldr r7, =dword_80382C8
	add r0, #0x40
	lsl r0, r0, #1
	add r0, r0, r7
	mov r2, #0
	ldrsh r1, [r0, r2]
	str r1, [sp]
	mov r3, sl
	lsl r3, r3, #0x10
	asr r3, r3, #0x10
	mov sl, r3
	mov r0, sl
	bl invert_16bit
	add r1, r0, #0
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	ldr r0, [sp]
	bl multiply_fixed8_unk
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	str r0, [sp, #4]
	mov r0, sb
	lsl r4, r0, #1
	add r4, r4, r7
	mov r2, #0
	ldrsh r1, [r4, r2]
	mov r8, r1
	mov r0, sl
	bl invert_16bit
	add r1, r0, #0
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	mov r0, r8
	bl multiply_fixed8_unk
	mov r8, r0
	mov r3, r8
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	mov r8, r3
	ldrh r4, [r4]
	neg r4, r4
	lsl r4, r4, #0x10
	asr r4, r4, #0x10
	lsl r6, r6, #0x10
	asr r6, r6, #0x10
	add r0, r6, #0
	bl invert_16bit
	add r1, r0, #0
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	add r0, r4, #0
	bl multiply_fixed8_unk
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	add r0, r6, #0
	bl invert_16bit
	add r1, r0, #0
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	ldr r0, [sp]
	bl multiply_fixed8_unk
	ldr r1, =0x03002320
	lsl r5, r5, #3
	add r1, r5, r1
	mov r2, sp
	ldrh r2, [r2, #4]
	strh r2, [r1]
	mov r3, r8
	strh r3, [r1, #2]
	strh r4, [r1, #4]
	strh r0, [r1, #6]
	mov r4, #0x80
	lsl r4, r4, #1
	mov r0, sb
	sub r4, r4, r0
	mov r0, #0xff
	and r4, r0
	add r0, r4, #0
	add r0, #0x40
	lsl r0, r0, #1
	add r0, r0, r7
	mov r2, #0
	ldrsh r1, [r0, r2]
	mov r8, r1
	mov r0, r8
	mov r1, sl
	bl multiply_fixed8_unk
	ldr r1, =0x03002F50
	add r5, r5, r1
	strh r0, [r5]
	lsl r4, r4, #1
	add r4, r4, r7
	mov r3, #0
	ldrsh r0, [r4, r3]
	add r1, r6, #0
	bl multiply_fixed8_unk
	strh r0, [r5, #2]
	ldrh r0, [r4]
	neg r0, r0
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	mov r1, sl
	bl multiply_fixed8_unk
	strh r0, [r5, #4]
	mov r0, r8
	add r1, r6, #0
	bl multiply_fixed8_unk
	strh r0, [r5, #6]
	add sp, #8
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global unused_sub_800E63C
unused_sub_800E63C: @ 0x0800E63C
	push {r4, r5, lr}
	add r5, r0, #0
	lsl r1, r1, #0x10
	mov r4, #0xff
	lsl r4, r4, #0x10
	and r4, r1
	lsr r4, r4, #0x10
	add r0, #0x24
	ldrb r0, [r0]
	lsr r0, r0, #3
	ldrh r1, [r5, #0x14]
	ldrh r2, [r5, #0x16]
	add r3, r4, #0
	bl unused_sub_800E508
	strh r4, [r5, #0x1c]
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global unused_sub_800E664
unused_sub_800E664: @ 0x0800E664
	push {r4, r5, r6, lr}
	add r6, r0, #0
	add r4, r1, #0
	add r5, r2, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	add r0, #0x24
	ldrb r0, [r0]
	lsr r0, r0, #3
	ldrh r3, [r6, #0x1c]
	add r1, r4, #0
	add r2, r5, #0
	bl unused_sub_800E508
	strh r4, [r6, #0x14]
	strh r5, [r6, #0x16]
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0


/*	bool8 updateSpriteAnimation(struct sprite* sprite)
	- clears lowest bit of field24, decrements anim_timer and calls advanceSpriteAnimation if anim_timer is zero
*/
thumb_func_global updateSpriteAnimation
updateSpriteAnimation: @ 0x0800E690
	push {lr}
	add r3, r0, #0
	add r2, r3, #0
	add r2, #0x24
	ldrb r1, [r2]
	mov r0, #2
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	ldrh r1, [r3, #0x20]
	mov r2, #0x20
	ldrsh r0, [r3, r2]
	cmp r0, #0
	bne _0800E6B0
	mov r0, #1
	b _0800E6C4
_0800E6B0:
	sub r0, r1, #1
	strh r0, [r3, #0x20]
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0800E6BE
	mov r0, #0
	b _0800E6C4
_0800E6BE:
	add r0, r3, #0
	bl advanceSpriteAnimation
_0800E6C4:
	pop {r1}
	bx r1



/*	bool8 setSpriteAnimationReverse(struct sprite* sprite)
	- Unused
	- same as the function above but uses sprite_prevFrame instead of advanceSpriteAnimation
*/
thumb_func_global setSpriteAnimationReverse
setSpriteAnimationReverse: @ 0x0800E6C8
	push {lr}
	add r1, r0, #0
	ldrh r2, [r1, #0x20]
	mov r3, #0x20
	ldrsh r0, [r1, r3]
	cmp r0, #0
	bne _0800E6DA
	mov r0, #1
	b _0800E6EE
_0800E6DA:
	sub r0, r2, #1
	strh r0, [r1, #0x20]
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0800E6E8
	mov r0, #0
	b _0800E6EE
_0800E6E8:
	add r0, r1, #0
	bl sprite_prevFrame
_0800E6EE:
	pop {r1}
	bx r1
	.align 2, 0


/*	bool8 advanceSpriteAnimation(struct sprite* sprite)	
	- sets the sprite's frame to the next one
	- returns true if animation is finished
*/
thumb_func_global advanceSpriteAnimation
advanceSpriteAnimation: @ 0x0800E6F4
	push {r4, r5, lr}
	add r3, r0, #0
	ldrh r1, [r3, #0xe]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #2
	add r0, #0xc
	ldr r1, [r3, #4]
	add r4, r1, r0
	add r2, r3, #0
	add r2, #0x24
	ldrb r1, [r2]
	mov r0, #2
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	ldr r0, [r4]
	ldr r2, =0x0000FFFF
	cmp r0, r2
	bne _0800E730
	add r2, r3, #0
	add r2, #0x23
	ldrb r0, [r2]
	mov r1, #0x10
	orr r0, r1
	strb r0, [r2]
	b _0800E736
	.align 2, 0
.pool
_0800E730:
	ldrh r1, [r4, #4]
	cmp r1, r2
	bne _0800E73E
_0800E736:
	mov r0, #0
	strh r0, [r3, #0x20]
	mov r0, #1
	b _0800E79A
_0800E73E:
	cmp r0, #0
	beq _0800E746
	cmp r1, #0
	bne _0800E74E
_0800E746:
	mov r0, #0
	strh r0, [r3, #0xe]
	mov r5, #1
	b _0800E756
_0800E74E:
	ldrh r0, [r3, #0xe]
	add r0, #1
	strh r0, [r3, #0xe]
	mov r5, #0
_0800E756:
	ldrh r1, [r3, #0xe]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #2
	ldr r1, [r3, #4]
	add r4, r1, r0
	mov r0, #0x23
	add r0, r0, r3
	mov ip, r0
	ldrb r0, [r0]
	mov r1, #0x80
	orr r0, r1
	mov r1, ip
	strb r0, [r1]
	add r2, r3, #0
	add r2, #0x24
	ldrb r0, [r2]
	mov r1, #1
	orr r0, r1
	strb r0, [r2]
	ldrh r0, [r4, #4]
	strh r0, [r3, #0x20]
	mov r0, ip
	ldrb r1, [r0]
	mov r0, #8
	and r0, r1
	cmp r0, #0
	beq _0800E798
	ldrh r0, [r4, #0xa]
	cmp r0, #0
	beq _0800E798
	bl playSong
_0800E798:
	add r0, r5, #0
_0800E79A:
	pop {r4, r5}
	pop {r1}
	bx r1


/*	u16 sprite_prevFrame(struct sprite* sprite)	
	- sets sprite's animation frame to the one before the current one
	- returns frame number
*/
thumb_func_global sprite_prevFrame
sprite_prevFrame: @ 0x0800E7A0
	push {r4, r5, r6, r7, lr}
	add r3, r0, #0
	add r2, r3, #0
	add r2, #0x24
	ldrb r1, [r2]
	mov r0, #2
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	ldrh r0, [r3, #0xe]
	cmp r0, #0
	bne _0800E7E8
	mov r4, #0
	ldr r6, [r3, #4]
	ldr r0, [r6, #0xc]
	add r5, r2, #0
	sub r2, #1
	cmp r0, #0
	beq _0800E7DE
	ldr r1, =0x0000FFFF
	cmp r0, r1
	beq _0800E7DE
	add r7, r1, #0
	add r1, r6, #0
_0800E7D0:
	add r1, #0xc
	add r4, #1
	ldr r0, [r1, #0xc]
	cmp r0, #0
	beq _0800E7DE
	cmp r0, r7
	bne _0800E7D0
_0800E7DE:
	strh r4, [r3, #0xe]
	mov r4, #1
	b _0800E7F4
	.align 2, 0
.pool
_0800E7E8:
	sub r0, #1
	strh r0, [r3, #0xe]
	mov r4, #0
	add r5, r2, #0
	add r2, r3, #0
	add r2, #0x23
_0800E7F4:
	ldrb r0, [r2]
	mov r1, #0x80
	orr r0, r1
	strb r0, [r2]
	ldrb r0, [r5]
	mov r1, #1
	orr r0, r1
	strb r0, [r5]
	ldrh r1, [r3, #0xe]
	ldr r2, [r3, #4]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #2
	add r0, r0, r2
	ldrh r0, [r0, #4]
	strh r0, [r3, #0x20]
	add r0, r4, #0
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	
	
/*	void setSpriteFrame(struct sprite* sprite, u8 frame)
	- Unused
*/
thumb_func_global setSpriteFrame	@unused
setSpriteFrame: @ 0x0800E81C
	push {lr}
	add r3, r0, #0
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	strh r1, [r3, #0xe]
	ldr r2, [r3, #4]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #2
	add r0, r0, r2
	ldrh r0, [r0, #4]
	strh r0, [r3, #0x20]
	add r2, r3, #0
	add r2, #0x23
	ldrb r0, [r2]
	mov r1, #0x80
	orr r0, r1
	strb r0, [r2]
	mov r1, #8
	and r0, r1
	cmp r0, #0
	beq _0800E85E
	ldrh r1, [r3, #0xe]
	ldr r2, [r3, #4]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #2
	add r1, r0, r2
	ldrh r0, [r1, #0xa]
	cmp r0, #0
	beq _0800E85E
	bl playSong
_0800E85E:
	pop {r0}
	bx r0
	.align 2, 0



/*	void setSpriteAnimation(struct sprite* sprite, void* animation, u16 vramTileNum, void* tileset)	*/
thumb_func_global setSpriteAnimation
setSpriteAnimation: @ 0x0800E864
	push {r4, r5, lr}
	mov ip, r0
	add r5, r1, #0
	str r5, [r0, #4]
	mov r4, #0
	strh r2, [r0, #0xc]
	str r3, [r0, #8]
	mov r3, ip
	add r3, #0x23
	ldrb r0, [r3]
	mov r1, #0x80
	orr r0, r1
	strb r0, [r3]
	mov r2, ip
	add r2, #0x24
	ldrb r0, [r2]
	mov r1, #1
	orr r0, r1
	strb r0, [r2]
	ldrh r0, [r5, #4]
	mov r1, ip
	strh r0, [r1, #0x20]
	strh r4, [r1, #0xe]
	ldrb r1, [r3]
	mov r0, #8
	and r0, r1
	cmp r0, #0
	beq _0800E8A6
	ldrh r0, [r5, #0xa]
	cmp r0, #0
	beq _0800E8A6
	bl playSong
_0800E8A6:
	pop {r4, r5}
	pop {r0}
	bx r0


/*	void findFreeSprite(u16 startIndex, u16 endIndex)
	-returns first sprite with clear inUse bit within the given index range
*/
thumb_func_global findFreeSprite
findFreeSprite: @ 0x0800E8AC
	push {r4, r5, lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r4, r1, #0x10
	lsl r1, r0, #2
	add r1, r1, r0
	lsl r1, r1, #3
	ldr r2, =0x03003050
	add r3, r1, r2
	add r2, r0, #0
	cmp r2, r4
	bge _0800E8EC
	add r0, r3, #0
	add r0, #0x23
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _0800E8EC
	mov r5, #1
_0800E8D6:
	add r2, #1
	add r3, #0x28
	cmp r2, r4
	bge _0800E8EC
	add r0, r3, #0
	add r0, #0x23
	ldrb r1, [r0]
	add r0, r5, #0
	and r0, r1
	cmp r0, #0
	bne _0800E8D6
_0800E8EC:
	cmp r2, r4
	beq _0800E8FC
	lsl r0, r2, #0x10
	asr r0, r0, #0x10
	b _0800E900
	.align 2, 0
.pool
_0800E8FC:
	mov r0, #1
	neg r0, r0
_0800E900:
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0


/*	createSpriteStruct(u8 index, void* animation, u16 vramTileNum, void* tileset, u8 unk) */
thumb_func_global createSpriteStruct
createSpriteStruct: @ 0x0800E908
	push {r4, r5, r6, lr}
	mov r6, sl
	mov r5, sb
	mov r4, r8
	push {r4, r5, r6}
	sub sp, #4
	mov sl, r1
	add r6, r2, #0
	mov sb, r3
	ldr r5, [sp, #0x20]
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	lsl r6, r6, #0x10
	lsr r6, r6, #0x10
	lsl r5, r5, #0x18
	lsr r5, r5, #0x18
	lsl r4, r0, #2
	add r4, r4, r0
	lsl r4, r4, #3
	ldr r0, =0x03003050
	add r4, r4, r0
	mov r0, #0
	mov r8, r0
	str r0, [sp]
	ldr r2, =0x0500000A
	mov r0, sp
	add r1, r4, #0
	bl Bios_memcpy
	mov r1, sl
	str r1, [r4, #4]
	strh r6, [r4, #0xc]
	mov r0, sb
	str r0, [r4, #8]
	add r2, r4, #0
	add r2, #0x24
	lsl r5, r5, #3
	ldrb r1, [r2]
	mov r0, #7
	and r0, r1
	orr r0, r5
	strb r0, [r2]
	sub r2, #1
	ldrb r0, [r2]
	mov r1, #1
	orr r0, r1
	mov r1, #0x80
	orr r0, r1
	strb r0, [r2]
	mov r1, sl
	ldrh r0, [r1, #4]
	strh r0, [r4, #0x20]
	mov r0, #0x80
	lsl r0, r0, #1
	strh r0, [r4, #0x14]
	strh r0, [r4, #0x16]
	mov r0, r8
	strh r0, [r4, #0x1c]
	add r0, r4, #0
	add sp, #4
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool


/*	struct sprite* createSprite(void* animation, void* tileset, u16 tileCount, u8 priority, u8 subpriority)	*/
thumb_func_global createSprite
createSprite: @ 0x0800E998
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #4
	add r7, r0, #0
	mov r8, r1
	ldr r0, [sp, #0x1c]
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	add r5, r2, #0
	lsl r3, r3, #0x18
	lsr r6, r3, #0x18
	lsl r0, r0, #0x18
	lsr r4, r0, #0x18
	add r0, r5, #0
	bl allocateSpriteTiles
	lsl r0, r0, #0x10
	cmp r0, #0
	blt _0800E9D6
	lsr r2, r0, #0x10
	str r4, [sp]
	add r0, r7, #0
	mov r1, r8
	add r3, r6, #0
	bl allocSprite
	cmp r0, #0
	beq _0800E9D6
	strh r5, [r0, #0x1e]
	b _0800E9D8
_0800E9D6:
	mov r0, #0
_0800E9D8:
	add sp, #4
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1


/*	struct sprite* allocSprite(void* animation, void* tileset, u16 vramTileNum, u8 priority, u8 subpriority)	*/
thumb_func_global allocSprite
allocSprite: @ 0x0800E9E4
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #4
	add r7, r0, #0
	mov r8, r1
	ldr r0, [sp, #0x1c]
	lsl r2, r2, #0x10
	lsr r4, r2, #0x10
	lsl r3, r3, #0x18
	lsr r5, r3, #0x18
	lsl r0, r0, #0x18
	lsr r6, r0, #0x18
	mov r0, #0
	mov r1, #0x30
	bl findFreeSprite
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	cmp r0, #0
	blt _0800EA48
	lsl r0, r1, #0x18
	lsr r0, r0, #0x18
	mov r1, #0
	str r1, [sp]
	add r1, r7, #0
	add r2, r4, #0
	mov r3, r8
	bl createSpriteStruct
	add r4, r0, #0
	mov r1, #0
	add r2, r5, #0
	add r3, r6, #0
	bl createSpriteHeader
	str r0, [r4]
	add r3, r4, #0
	add r3, #0x23
	mov r1, #3
	and r1, r5
	lsl r1, r1, #5
	ldrb r2, [r3]
	mov r0, #0x61
	neg r0, r0
	and r0, r2
	orr r0, r1
	strb r0, [r3]
	add r0, r4, #0
	b _0800EA4A
_0800EA48:
	mov r0, #0
_0800EA4A:
	add sp, #4
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0


/*	void setSpritePriority(struct sprite* sprite, u8 priority, u8 subpriority)	*/
thumb_func_global setSpritePriority
setSpritePriority: @ 0x0800EA58
	push {r4, r5, lr}
	add r5, r0, #0
	add r4, r1, #0
	lsl r4, r4, #0x18
	lsr r4, r4, #0x18
	lsl r2, r2, #0x18
	lsr r2, r2, #0x18
	ldr r0, [r5]
	add r1, r4, #0
	bl setSpriteHeaderPriority
	add r5, #0x23
	mov r1, #3
	and r1, r4
	lsl r1, r1, #5
	ldrb r2, [r5]
	mov r0, #0x61
	neg r0, r0
	and r0, r2
	orr r0, r1
	strb r0, [r5]
	pop {r4, r5}
	pop {r0}
	bx r0


/*	void freeSprite(struct sprite* sprite)*/
thumb_func_global freeSprite
freeSprite: @ 0x0800EA88
	push {r4, lr}
	add r4, r0, #0
	bl spriteFreeHeader
	ldrh r0, [r4, #0xc]
	ldrh r1, [r4, #0x1e]
	bl freeSpriteTiles
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0


/*	void spriteFreeHeader(struct sprite* sprite)*/
thumb_func_global spriteFreeHeader
spriteFreeHeader: @ 0x0800EAA0
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4]
	bl freeSpriteHeader
	mov r0, #0
	str r0, [r4]
	add r4, #0x23
	ldrb r1, [r4]
	sub r0, #2
	and r0, r1
	strb r0, [r4]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0


/*	void clearSpriteTileAllocTable(void)	*/
thumb_func_global clearSpriteTileAllocTable
clearSpriteTileAllocTable: @ 0x0800EAC0
	push {lr}
	sub sp, #4
	mov r0, #0
	str r0, [sp]
	ldr r1, =0x03000A78
	ldr r2, =0x05000020
	mov r0, sp
	bl Bios_memcpy
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool


/*	u16 allocateSpriteTiles(u16 tileCount)	*/
thumb_func_global allocateSpriteTiles
allocateSpriteTiles: @ 0x0800EAE0
	push {r4, r5, r6, lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r6, #0
	add r2, r0, #7
	lsr r2, r2, #3
	cmp r2, #0
	bne _0800EAF4
	mov r0, #0
	b _0800EB48
_0800EAF4:
	mov r5, #0
	mov r3, #0
	ldr r0, =0x03000A78
	ldrb r1, [r0]
	add r4, r0, #0
	cmp r1, #0
	bne _0800EB10
	mov r1, #1
	cmp r1, r2
	bne _0800EB18
	b _0800EB3C
	.align 2, 0
.pool
_0800EB10:
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	mov r1, #0
_0800EB18:
	add r3, #1
	cmp r3, #0x7f
	bgt _0800EB32
	add r0, r3, r4
	ldrb r0, [r0]
	cmp r0, #0
	bne _0800EB10
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	cmp r1, r2
	bne _0800EB18
	mov r6, #1
_0800EB32:
	cmp r6, #0
	bne _0800EB3C
	mov r0, #1
	neg r0, r0
	b _0800EB48
_0800EB3C:
	add r0, r5, r4
	mov r1, #1
	bl memfill
	lsl r0, r5, #0x13
	asr r0, r0, #0x10
_0800EB48:
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0


/*	u16 allocateSpriteTilesAt(u16 startingIndex, u16 tileCount)	
	- UNUSED
*/
thumb_func_global allocateSpriteTilesAt
allocateSpriteTilesAt: @ 0x0800EB50
	push {lr}
	lsl r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	lsr r3, r0, #0x13
	add r2, r1, #7
	lsr r2, r2, #3
	cmp r2, #0
	beq _0800EB6C
	ldr r0, =0x03000A78
	add r0, r3, r0
	mov r1, #1
	bl memfill
_0800EB6C:
	pop {r0}
	bx r0
	.align 2, 0
.pool


/*	void freeSpriteTiles(u16 startingIndex, u16 tileCount)	*/
thumb_func_global freeSpriteTiles
freeSpriteTiles: @ 0x0800EB74
	push {lr}
	lsl r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	lsr r3, r0, #0x13
	add r2, r1, #7
	lsr r2, r2, #3
	cmp r2, #0
	beq _0800EB90
	ldr r0, =0x03000A78
	add r0, r3, r0
	mov r1, #0
	bl memfill
_0800EB90:
	pop {r0}
	bx r0
	.align 2, 0
.pool


/*	void bufferSimpleSprite(struct SimpleSprite* spr)
	- draws SimpleSprite to OAM buffer
*/
thumb_func_global bufferSimpleSprite
bufferSimpleSprite: @ 0x0800EB98
	push {r4, r5, r6, r7, lr}
	add r5, r0, #0
	ldrb r0, [r5, #0xc]
	cmp r0, #0
	beq _0800EC16
	ldr r1, =0x040000D4
	str r5, [r1]
	ldr r7, =0x03002820
	ldrb r0, [r7]
	lsl r0, r0, #3
	ldr r2, =0x03002420
	add r0, r0, r2
	str r0, [r1, #4]
	ldr r0, =0x80000003
	str r0, [r1, #8]
	ldr r0, [r1, #8]
	ldrb r6, [r5, #1]
	lsl r0, r6, #0x1e
	lsr r0, r0, #0x1e
	mov r1, #2
	and r0, r1
	cmp r0, #0
	beq _not_double_size
	ldrb r3, [r7]
	lsl r3, r3, #3
	add r3, r3, r2
	ldrh r2, [r5, #2]
	lsl r2, r2, #0x17
	lsr r2, r2, #0x17
	ldr r4, =objDimensionHalves
	ldrb r0, [r5, #3]
	lsr r0, r0, #6
	lsl r0, r0, #1
	lsr r1, r6, #6
	lsl r1, r1, #3
	add r0, r0, r1
	add r0, r0, r4
	ldrb r0, [r0]
	sub r2, r2, r0
	ldr r1, =0x000001FF
	add r0, r1, #0
	and r2, r0
	ldrh r1, [r3, #2]
	ldr r0, =0xFFFFFE00
	and r0, r1
	orr r0, r2
	strh r0, [r3, #2]
	ldrb r1, [r5, #3]
	lsr r1, r1, #6
	lsl r1, r1, #1
	ldrb r0, [r5, #1]
	lsr r0, r0, #6
	lsl r0, r0, #3
	add r1, r1, r0
	add r4, #1
	add r1, r1, r4
	ldrb r0, [r5]
	ldrb r1, [r1]
	sub r0, r0, r1
	strb r0, [r3]
_not_double_size:
	ldrb r0, [r7]
	add r0, #1
	strb r0, [r7]
_0800EC16:
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool



/*	struct SimpleSprite* createSimpleSprite(u8 index, u16 x, u8 y, u16 vramChar, u8 a5, int a6, u8 a7, u32 a8)
	- inserts a SimpleSprite in the global list at index
	- the unnamed parameters control parts of the SimpleSprite's OAM attributes 
*/
thumb_func_global createSimpleSprite
createSimpleSprite: @ 0x0800EC38
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	add r5, r0, #0
	add r7, r2, #0
	ldr r0, [sp, #0x20]
	mov sb, r0
	ldr r2, [sp, #0x24]
	ldr r6, [sp, #0x28]
	ldr r0, [sp, #0x2c]
	mov r8, r0
	lsl r5, r5, #0x18
	mov r0, sb
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sb, r0
	lsl r2, r2, #0x10
	lsr r0, r2, #0x10
	mov sl, r0
	lsl r6, r6, #0x18
	lsr r6, r6, #0x18
	mov r0, r8
	lsl r0, r0, #0x18
	mov r8, r0
	lsr r5, r5, #0x14
	ldr r0, =0x030037D0
	add r5, r5, r0
	mov r0, #1
	strb r0, [r5, #0xc]
	lsl r1, r1, #0x17
	lsr r1, r1, #0x17
	ldrh r4, [r5, #2]
	ldr r0, =0xFFFFFE00
	and r0, r4
	orr r0, r1
	strh r0, [r5, #2]
	strb r7, [r5]
	lsl r3, r3, #0x16
	lsr r3, r3, #0x16
	ldrh r1, [r5, #4]
	ldr r0, =0xFFFFFC00
	and r0, r1
	orr r0, r3
	strh r0, [r5, #4]
	mov r1, sb
	lsr r0, r1, #8
	mov r3, #1
	and r0, r3
	lsl r0, r0, #5
	ldrb r1, [r5, #1]
	sub r3, #0x22
	mov ip, r3
	mov r4, ip
	and r4, r1
	orr r4, r0
	mov r1, #0xff
	mov r0, sb
	and r1, r0
	lsl r1, r1, #4
	ldrb r3, [r5, #5]
	mov r0, #0xf
	mov sb, r0
	and r0, r3
	orr r0, r1
	mov r3, sl
	lsr r1, r3, #8
	mov r3, #3
	and r1, r3
	lsl r1, r1, #2
	mov r7, #0xd
	neg r7, r7
	and r0, r7
	orr r0, r1
	strb r0, [r5, #5]
	lsr r0, r6, #4
	lsl r0, r0, #6
	mov r1, #0x3f
	and r4, r1
	orr r4, r0
	mov r0, sb
	and r6, r0
	lsl r6, r6, #6
	ldrb r0, [r5, #3]
	and r1, r0
	orr r1, r6
	mov r0, #4
	neg r0, r0
	and r4, r0
	mov r0, #0xe0
	lsl r0, r0, #0x13
	mov r3, r8
	and r0, r3
	lsr r0, r0, #0x17
	mov r3, #0xf
	neg r3, r3
	and r1, r3
	orr r1, r0
	mov r3, r8
	lsr r0, r3, #0x1b
	mov r3, #1
	and r0, r3
	and r0, r3
	lsl r0, r0, #4
	sub r3, #0x12
	and r1, r3
	orr r1, r0
	mov r0, r8
	lsr r0, r0, #0x1c
	mov r3, #1
	and r0, r3
	and r0, r3
	lsl r0, r0, #5
	mov r8, r0
	mov r0, ip
	and r1, r0
	mov r3, r8
	orr r1, r3
	strb r1, [r5, #3]
	and r4, r7
	mov r0, #0x10
	orr r4, r0
	strb r4, [r5, #1]
	lsr r2, r2, #0x18
	mov r0, sl
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	mov sl, r0
	add r0, r5, #0
	mov r1, #1
	mov r3, sl
	bl createSpriteHeader
	str r0, [r5, #8]
	add r0, r5, #0
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool

/*	void freeSimpleSpriteHeader(SimpleSprite *a1)	*/
thumb_func_global freeSimpleSpriteHeader
freeSimpleSpriteHeader: @ 0x0800ED64
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #8]
	bl freeSpriteHeader
	mov r0, #0
	str r0, [r4, #8]
	strb r0, [r4, #0xc]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global unused_sub_800ED7C
unused_sub_800ED7C: @ 0x0800ED7C
	push {r4, r5, lr}
	add r5, r0, #0
	add r4, r1, #0
	lsl r4, r4, #0x18
	lsr r4, r4, #0x18
	lsl r2, r2, #0x18
	lsr r2, r2, #0x18
	ldr r0, [r5, #8]
	add r1, r4, #0
	bl setSpriteHeaderPriority
	mov r0, #3
	and r4, r0
	lsl r4, r4, #2
	ldrb r1, [r5, #5]
	mov r0, #0xd
	neg r0, r0
	and r0, r1
	orr r0, r4
	strb r0, [r5, #5]
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global CB_UpdateOAM
CB_UpdateOAM: @ 0x0800EDAC
	push {lr}
	mov r0, #0xe0
	lsl r0, r0, #0x13
	bl ClearOAM
	bl loadAllSpriteTiles
	bl loadOAM_rotsca
	pop {r0}
	bx r0
	.align 2, 0


/*	void drawSprites(void)
	- sets oamCount variable to 0 and calls drawSpriteHeaders
	- this runs every frame in the main loop
*/
thumb_func_global drawSprites
drawSprites: @ 0x0800EDC4
	push {lr}
	ldr r1, =0x03002820
	mov r0, #0
	strb r0, [r1]
	mov r0, #0x80
	lsl r0, r0, #0x13
	ldrh r1, [r0]
	mov r2, #0x80
	lsl r2, r2, #5
	add r0, r2, #0
	orr r0, r1	@ I think this is meant to be 'and'
	cmp r0, #0	@ r0 can never be 0
	beq _0800EDE2
	bl drawSpriteHeaders
_0800EDE2:
	pop {r0}
	bx r0
	.align 2, 0
.pool


/*	void loadAllSpriteTiles(void)	
	-calls loadSpriteTiles for each sprite that has a tileset and have bits 0 & 7 set in field23
*/
thumb_func_global loadAllSpriteTiles
loadAllSpriteTiles: @ 0x0800EDEC
	push {r4, r5, lr}
	ldr r4, =0x03003050
	mov r5, #0x2f
_0800EDF2:
	add r0, r4, #0
	add r0, #0x23
	ldrb r1, [r0]
	mov r0, #0x81
	and r0, r1
	cmp r0, #0x81
	bne _0800EE0C
	ldr r0, [r4, #8]
	cmp r0, #0
	beq _0800EE0C
	add r0, r4, #0
	bl loadSpriteTiles
_0800EE0C:
	sub r5, #1
	add r4, #0x28
	cmp r5, #0
	bge _0800EDF2
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool


/*	void loadSpriteTiles(struct sprite* sprite)
	-copies tiles to vram for all subsprites of the current frame of animation
	-clears bit 7 of field23
*/
thumb_func_global loadSpriteTiles
loadSpriteTiles: @ 0x0800EE20
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	add r3, r0, #0
	ldrh r0, [r3, #0xe]
	lsl r1, r0, #1
	add r1, r1, r0
	lsl r1, r1, #2
	ldr r0, [r3, #4]
	add r0, r0, r1
	ldr r1, [r0]
	add r2, r1, #0
	add r2, #0xc
	mov r4, #2
	ldrsh r0, [r1, r4]
	mov r4, #2
	cmp r0, #0
	bne _0800EE48
	mov r4, #1
_0800EE48:
	add r7, r4, #0
	ldrh r5, [r3, #0xc]
	mov r6, #0
	mov r0, #0x23
	add r0, r0, r3
	mov sb, r0
	ldrh r4, [r1]
	cmp r6, r4
	bhs _0800EEC2
	ldr r4, =0x040000D4
	ldr r0, =dword_8038260
	mov ip, r0
	ldr r3, [r3, #8]
	mov r8, r3
	add r3, r1, #0
_0800EE66:
	ldrh r0, [r2]
	lsl r0, r0, #5
	add r0, r8
	str r0, [r4]
	lsl r0, r5, #5
	ldr r1, =0x06010000
	add r0, r0, r1
	str r0, [r4, #4]
	ldrb r1, [r2, #7]
	lsl r0, r1, #0x1a
	lsr r0, r0, #0x1e
	lsl r0, r0, #1
	lsr r1, r1, #6
	lsl r1, r1, #3
	add r0, r0, r1
	add r0, ip
	ldrh r0, [r0]
	lsl r0, r0, #3
	mul r0, r7, r0
	mov r1, #0x84
	lsl r1, r1, #0x18
	orr r0, r1
	str r0, [r4, #8]
	ldr r0, [r4, #8]
	ldrb r1, [r2, #7]
	lsl r0, r1, #0x1a
	lsr r0, r0, #0x1e
	lsl r0, r0, #1
	lsr r1, r1, #6
	lsl r1, r1, #3
	add r0, r0, r1
	add r0, ip
	ldrh r0, [r0]
	add r1, r0, #0
	mul r1, r7, r1
	add r0, r1, #0
	add r0, r5, r0
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	add r2, #0xc
	add r0, r6, #1
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	ldrh r0, [r3]
	cmp r6, r0
	blo _0800EE66
_0800EEC2:
	mov r4, sb
	ldrb r1, [r4]
	mov r0, #0x7f
	and r0, r1
	strb r0, [r4]
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool


/*	void clearSprites(void)	*/
thumb_func_global clearSprites
clearSprites: @ 0x0800EEE4
	push {r4, r5, lr}
	ldr r0, =dword_80382B8
	ldr r4, [r0]
	ldr r5, [r0, #4]
	ldr r0, =0x03003050
	mov r2, #0xf0
	lsl r2, r2, #3
	mov r1, #0
	bl memfill
	ldr r0, =0x030037D0
	mov r2, #0x80
	lsl r2, r2, #3
	mov r1, #0
	bl memfill
	ldr r0, =0x03002830
	mov r2, #0xe4
	lsl r2, r2, #3
	mov r1, #0
	bl memfill
	ldr r0, =0x03002420
	bl ClearOAM
	mov r0, #0
	ldr r2, =0x03002F50
	ldr r1, =0x03002320
_0800EF1C:
	stm r1!, {r4, r5}
	stm r2!, {r4, r5}
	add r0, #1
	cmp r0, #0x1f
	bls _0800EF1C
	bl clearSpriteTileAllocTable
	ldr r2, =0x03002830
	ldrb r0, [r2, #0xf]
	mov r4, #1
	orr r0, r4
	strb r0, [r2, #0xf]
	mov r3, #0
	strh r3, [r2, #0xc]
	str r3, [r2, #4]
	add r1, r2, #0
	add r1, #0x10
	str r1, [r2, #8]
	ldrb r0, [r1, #0xf]
	orr r0, r4
	strb r0, [r1, #0xf]
	ldr r0, =0x0000FFFF
	strh r0, [r1, #0xc]
	str r2, [r1, #4]
	str r3, [r1, #8]
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool


/*	struct SpriteHeader* createSpriteHeader(void* arg1, u8 type, u8 priority, u8 subpriority)	
	- arg1 is either a pointer to a sprite or SimpleSprite
*/
thumb_func_global createSpriteHeader
createSpriteHeader: @ 0x0800EF74
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	mov r8, r0
	lsl r1, r1, #0x18
	lsr r7, r1, #0x18
	lsl r2, r2, #0x18
	lsr r4, r2, #0x18
	lsl r3, r3, #0x18
	lsr r6, r3, #0x18
	bl findFree_drawSpriteHeaders
	add r5, r0, #0
	cmp r5, #0
	beq _0800EFBA
	lsl r0, r4, #8
	add r4, r0, r6
	orr r0, r6
	bl findSpriteHeaderPrio
	ldr r3, [r0, #4]
	ldrb r1, [r5, #0xf]
	mov r2, #1
	orr r1, r2
	strb r1, [r5, #0xf]
	mov r1, r8
	str r1, [r5]
	strb r7, [r5, #0xe]
	strh r4, [r5, #0xc]
	str r3, [r5, #4]
	str r0, [r5, #8]
	str r5, [r3, #8]
	str r5, [r0, #4]
	add r0, r5, #0
	b _0800EFBC
_0800EFBA:
	mov r0, #0
_0800EFBC:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0


/*	void setSpriteHeaderPriority(struct SpriteHeader* header, u8 newPriority, u8 newSubpriority)	*/
thumb_func_global setSpriteHeaderPriority
setSpriteHeaderPriority: @ 0x0800EFC8
	push {r4, r5, lr}
	add r4, r0, #0
	add r0, r1, #0
	lsl r0, r0, #0x18
	lsl r2, r2, #0x18
	lsr r2, r2, #0x18
	ldr r3, [r4, #4]
	ldr r1, [r4, #8]
	str r1, [r3, #8]
	ldr r3, [r4, #8]
	ldr r1, [r4, #4]
	str r1, [r3, #4]
	lsr r0, r0, #0x10
	add r5, r0, r2
	orr r0, r2
	bl findSpriteHeaderPrio
	ldr r1, [r0, #4]
	str r1, [r4, #4]
	str r0, [r4, #8]
	str r4, [r1, #8]
	str r4, [r0, #4]
	strh r5, [r4, #0xc]
	pop {r4, r5}
	pop {r0}
	bx r0

/*	void freeSpriteHeader(struct SpriteHeader* header)	*/
thumb_func_global freeSpriteHeader
freeSpriteHeader: @ 0x0800EFFC
	ldr r2, [r0, #4]
	ldr r1, [r0, #8]
	str r1, [r2, #8]
	ldr r2, [r0, #8]
	ldr r1, [r0, #4]
	str r1, [r2, #4]
	mov r1, #0
	str r1, [r0, #4]
	str r1, [r0, #8]
	str r1, [r0]
	ldrb r2, [r0, #0xf]
	sub r1, #2
	and r1, r2
	strb r1, [r0, #0xf]
	bx lr
	.align 2, 0



/*	struct SpriteHeader* findFree_drawSpriteHeaders(void)	
	- returns first one with inUse bit clear
*/
thumb_func_global findFree_drawSpriteHeaders
findFree_drawSpriteHeaders: @ 0x0800F01C
	mov r2, #2
	ldr r0, =0x03002830
	add r1, r0, #0
	add r1, #0x20
_0800F024:
	ldrb r0, [r1, #0xf]
	lsl r0, r0, #0x1f
	cmp r0, #0
	bne _0800F034
	add r0, r1, #0
	b _0800F03E
	.align 2, 0
.pool
_0800F034:
	add r1, #0x10
	add r2, #1
	cmp r2, #0x6f
	ble _0800F024
	mov r0, #0
_0800F03E:
	bx lr


/*	struct SpriteHeader* findSpriteHeaderPrio(u16 priority)
	-finds a SpriteHeader with a larger priority than the argument
*/
thumb_func_global findSpriteHeaderPrio
findSpriteHeaderPrio: @ 0x0800F040
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	ldr r0, =0x03002830
	ldr r1, [r0, #8]
	add r3, r0, #0
	add r3, #0x10
	b _0800F056
	.align 2, 0
.pool
_0800F054:
	ldr r1, [r1, #8]
_0800F056:
	cmp r1, r3
	beq _0800F060
	ldrh r0, [r1, #0xc]
	cmp r0, r2
	bls _0800F054
_0800F060:
	add r0, r1, #0
	bx lr


/*	void drawSpriteHeaders(void)
	- iterates through every SpriteHeader, calls rendering function for each header with inUse = 1
*/
thumb_func_global drawSpriteHeaders
drawSpriteHeaders: @ 0x0800F064
	push {r4, r5, lr}
	ldr r0, =0x03002830
	ldr r4, [r0, #8]
	add r5, r0, #0
	add r5, #0x10
	b _0800F076
	.align 2, 0
.pool
_0800F074:
	ldr r4, [r4, #8]
_0800F076:
	cmp r4, r5
	beq _0800F0A0
	ldrb r1, [r4, #0xf]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _0800F076
	ldr r0, =spriteRenderFuncs
	ldrb r1, [r4, #0xe]
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r0, [r4]
	ldr r1, [r1]
	bl call_r1
	ldr r0, =0x03002820
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	cmp r0, #0
	bge _0800F074
_0800F0A0:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool
