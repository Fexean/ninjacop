.include "asm/macros.inc"



thumb_func_global multiply_fixed8_unk
multiply_fixed8_unk: @ 0x0800F260
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	mul r0, r1, r0
	add r1, r0, #0
	cmp r0, #0
	bge _0800F272
	add r1, #0xff
_0800F272:
	lsl r0, r1, #8
	asr r0, r0, #0x10
	bx lr

thumb_func_global multiply_fixed8
multiply_fixed8: @ 0x0800F278
	mul r0, r1, r0
	asr r1, r0, #0x1f
	lsl r3, r1, #0x18
	lsr r2, r0, #8
	add r0, r3, #0
	orr r0, r2
	bx lr
	.align 2, 0
	
	
thumb_func_global div_fixed8
div_fixed8:
	push {lr}
	lsl r0, r0, #0x10
	asr r0, r0, #8
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	bl divide
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	pop {r1}
	bx r1
	.align 2, 0


thumb_func_global sub_800F2A0
sub_800F2A0: @ 0x0800F2A0
	push {r4, r5, lr}
	add r4, r0, #0
	add r2, r1, #0
	asr r5, r4, #0x1f
	lsr r1, r4, #0x18
	lsl r0, r5, #8
	add r5, r1, #0
	orr r5, r0
	lsl r4, r4, #8
	asr r3, r2, #0x1f
	add r1, r5, #0
	add r0, r4, #0
	bl sub_08037A0C
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global invert_16bit
invert_16bit: @ 0x0800F2C4
	push {lr}
	add r1, r0, #0
	mov r0, #0x80
	lsl r0, r0, #9
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	bl divide
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	pop {r1}
	bx r1

thumb_func_global fadeScreen
fadeScreen: @ 0x0800F2DC
	push {r4, r5, lr}
	lsl r0, r0, #0x18
	lsr r5, r0, #0x18
	lsl r1, r1, #0x18
	lsr r4, r1, #0x18
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	ldr r1, =0x03003BD0
	mov r0, #1
	strb r0, [r1, #0x1c]
	cmp r4, #0
	blt _0800F30C
	cmp r4, #1
	bgt _0800F30C
	add r0, r5, #0
	add r1, r4, #0
	bl sub_0800F358
	b _0800F314
	.align 2, 0
.pool
_0800F30C:
	add r0, r5, #0
	add r1, r4, #0
	bl sub_0800F3FC
_0800F314:
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global sub_0800F320
sub_0800F320: @ 0x0800F320
	push {lr}
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	cmp r1, #0
	blt _0800F33C
	cmp r1, #1
	bgt _0800F33C
	bl sub_0800F4FC
	b _0800F340
_0800F33C:
	bl sub_0800F590
_0800F340:
	lsl r0, r0, #0x18
	lsr r2, r0, #0x18
	cmp r2, #0
	beq _0800F34E
	ldr r1, =0x03003BD0
	mov r0, #0
	strb r0, [r1, #0x1c]
_0800F34E:
	add r0, r2, #0
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_0800F358
sub_0800F358: @ 0x0800F358
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	lsl r0, r0, #0x18
	lsr r4, r0, #0x18
	lsl r1, r1, #0x18
	lsr r5, r1, #0x18
	lsl r2, r2, #0x10
	lsr r6, r2, #0x10
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	mov r8, r3
	ldr r0, =0x03003BD0
	ldrb r1, [r0]
	add r7, r0, #0
	cmp r1, #0
	beq _0800F388
	cmp r1, #1
	beq _0800F3AA
	mov r0, #0
	strb r0, [r7]
	b _0800F3E2
	.align 2, 0
.pool
_0800F388:
	ldr r1, =0x03003D74
	lsl r0, r5, #6
	add r0, #0x80
	orr r0, r4
	strh r0, [r1]
	ldr r0, =0x03003D70
	ldrh r1, [r0]
	mov r0, #0x1f
	and r0, r1
	mul r0, r6, r0
	mov r1, #0x10
	bl Bios_Div
	strh r0, [r7, #2]
	ldrb r0, [r7]
	add r0, #1
	strb r0, [r7]
_0800F3AA:
	ldr r5, =0x03003D70
	ldr r4, =0x03003BD0
	mov r1, #2
	ldrsh r0, [r4, r1]
	lsl r0, r0, #4
	add r1, r6, #0
	bl Bios_Div
	strh r0, [r5]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	add r7, r4, #0
	cmp r0, r8
	bne _0800F3DC
	mov r0, #0
	strb r0, [r7]
	mov r0, #1
	b _0800F3F0
	.align 2, 0
.pool
_0800F3DC:
	ldrh r0, [r7, #2]
	add r0, #1
	strh r0, [r7, #2]
_0800F3E2:
	mov r1, #2
	ldrsh r0, [r7, r1]
	cmp r0, r6
	ble _0800F3EE
	mov r0, #0
	strb r0, [r7]
_0800F3EE:
	mov r0, #0
_0800F3F0:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global sub_0800F3FC
sub_0800F3FC: @ 0x0800F3FC
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	lsl r0, r0, #0x18
	lsr r4, r0, #0x18
	lsl r1, r1, #0x18
	lsr r0, r1, #0x18
	lsl r2, r2, #0x10
	lsr r7, r2, #0x10
	lsl r3, r3, #0x10
	lsr r5, r3, #0x10
	ldr r6, =0x03003BD0
	ldrb r2, [r6]
	cmp r2, #0
	beq _0800F428
	cmp r2, #1
	beq _0800F494
	mov r0, #0
	strb r0, [r6]
	b _0800F4DE
	.align 2, 0
.pool
_0800F428:
	lsl r0, r0, #1
	ldr r1, =unk_803854a
	add r0, r0, r1
	mov r1, #0xa0
	lsl r1, r1, #0x13
	mov r2, #1
	bl Bios_memcpy
	ldr r2, =0x03003D74
	mov r0, #0xfd
	lsl r0, r0, #6
	add r1, r0, #0
	add r0, r4, #0
	orr r0, r1
	strh r0, [r2]
	ldr r1, =0x03003D94
	mov r8, r1
	ldrh r0, [r1]
	lsr r0, r0, #8
	mul r0, r7, r0
	mov r1, #0x10
	bl Bios_Div
	strh r0, [r6, #2]
	mov r2, #2
	ldrsh r0, [r6, r2]
	lsl r0, r0, #4
	add r1, r7, #0
	bl Bios_Div
	add r4, r0, #0
	mov r1, #2
	ldrsh r0, [r6, r1]
	lsl r0, r0, #4
	add r1, r7, #0
	bl Bios_Div
	lsl r4, r4, #8
	mov r1, #0x10
	sub r1, r1, r0
	orr r4, r1
	mov r2, r8
	strh r4, [r2]
	ldrb r0, [r6]
	add r0, #1
	strb r0, [r6]
	b _0800F4DE
	.align 2, 0
.pool
_0800F494:
	ldr r0, =0x03003D94
	mov r8, r0
	mov r1, #2
	ldrsh r0, [r6, r1]
	lsl r0, r0, #4
	add r1, r7, #0
	bl Bios_Div
	add r4, r0, #0
	mov r2, #2
	ldrsh r0, [r6, r2]
	lsl r0, r0, #4
	add r1, r7, #0
	bl Bios_Div
	lsl r4, r4, #8
	mov r1, #0x10
	sub r1, r1, r0
	orr r4, r1
	mov r0, r8
	strh r4, [r0]
	ldrh r2, [r0]
	lsl r1, r5, #8
	mov r0, #0x10
	sub r0, r0, r5
	orr r1, r0
	cmp r2, r1
	bne _0800F4D8
	mov r0, #0
	strb r0, [r6]
	mov r0, #1
	b _0800F4EE
	.align 2, 0
.pool
_0800F4D8:
	ldrh r0, [r6, #2]
	add r0, #1
	strh r0, [r6, #2]
_0800F4DE:
	ldr r1, =0x03003BD0
	mov r2, #2
	ldrsh r0, [r1, r2]
	cmp r0, r7
	ble _0800F4EC
	mov r0, #0
	strb r0, [r1]
_0800F4EC:
	mov r0, #0
_0800F4EE:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_0800F4FC
sub_0800F4FC: @ 0x0800F4FC
	push {r4, r5, r6, lr}
	lsl r0, r0, #0x18
	lsr r5, r0, #0x18
	lsl r1, r1, #0x18
	lsr r3, r1, #0x18
	lsl r2, r2, #0x10
	lsr r6, r2, #0x10
	ldr r0, =0x03003BD0
	ldrb r1, [r0]
	add r4, r0, #0
	cmp r1, #0
	beq _0800F524
	cmp r1, #1
	beq _0800F546
	mov r0, #0
	strb r0, [r4]
	b _0800F57C
	.align 2, 0
.pool
_0800F524:
	ldr r1, =0x03003D74
	lsl r0, r3, #6
	add r0, #0x80
	orr r0, r5
	strh r0, [r1]
	ldr r0, =0x03003D70
	ldrh r1, [r0]
	mov r0, #0x1f
	and r0, r1
	mul r0, r6, r0
	mov r1, #0x10
	bl Bios_Div
	strh r0, [r4, #2]
	ldrb r0, [r4]
	add r0, #1
	strb r0, [r4]
_0800F546:
	ldr r5, =0x03003D70
	ldr r4, =0x03003BD0
	mov r1, #2
	ldrsh r0, [r4, r1]
	lsl r0, r0, #4
	add r1, r6, #0
	bl Bios_Div
	strh r0, [r5]
	ldrh r1, [r4, #2]
	mov r2, #2
	ldrsh r0, [r4, r2]
	cmp r0, #0
	bgt _0800F578
	bl gfx_related_sub_800F674
	mov r0, #1
	b _0800F58A
	.align 2, 0
.pool
_0800F578:
	sub r0, r1, #1
	strh r0, [r4, #2]
_0800F57C:
	mov r1, #2
	ldrsh r0, [r4, r1]
	cmp r0, r6
	ble _0800F588
	mov r0, #0
	strb r0, [r4]
_0800F588:
	mov r0, #0
_0800F58A:
	pop {r4, r5, r6}
	pop {r1}
	bx r1

thumb_func_global sub_0800F590
sub_0800F590: @ 0x0800F590
	push {r4, r5, r6, r7, lr}
	lsl r0, r0, #0x18
	lsr r4, r0, #0x18
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	lsl r2, r2, #0x10
	lsr r7, r2, #0x10
	ldr r0, =0x03003BD0
	ldrb r2, [r0]
	add r6, r0, #0
	cmp r2, #0
	beq _0800F5B8
	cmp r2, #1
	beq _0800F610
	mov r0, #0
	strb r0, [r6]
	b _0800F660
	.align 2, 0
.pool
_0800F5B8:
	lsl r0, r1, #1
	ldr r1, =unk_803854a
	add r0, r0, r1
	mov r1, #0xa0
	lsl r1, r1, #0x13
	mov r2, #1
	bl Bios_memcpy
	ldr r2, =0x03003D74
	mov r0, #0xfd
	lsl r0, r0, #6
	add r1, r0, #0
	add r0, r4, #0
	orr r0, r1
	strh r0, [r2]
	ldr r5, =0x03003D94
	ldrh r0, [r5]
	lsr r0, r0, #8
	mul r0, r7, r0
	mov r1, #0x10
	bl Bios_Div
	strh r0, [r6, #2]
	mov r1, #2
	ldrsh r0, [r6, r1]
	lsl r0, r0, #4
	add r1, r7, #0
	bl Bios_Div
	add r4, r0, #0
	mov r2, #2
	ldrsh r0, [r6, r2]
	lsl r0, r0, #4
	add r1, r7, #0
	bl Bios_Div
	lsl r4, r4, #8
	mov r1, #0x10
	sub r1, r1, r0
	orr r4, r1
	strh r4, [r5]
	ldrb r0, [r6]
	add r0, #1
	strb r0, [r6]
_0800F610:
	ldr r6, =0x03003D94
	ldr r5, =0x03003BD0
	mov r1, #2
	ldrsh r0, [r5, r1]
	lsl r0, r0, #4
	add r1, r7, #0
	bl Bios_Div
	add r4, r0, #0
	mov r2, #2
	ldrsh r0, [r5, r2]
	lsl r0, r0, #4
	add r1, r7, #0
	bl Bios_Div
	lsl r4, r4, #8
	mov r1, #0x10
	sub r1, r1, r0
	orr r4, r1
	strh r4, [r6]
	ldrh r1, [r5, #2]
	mov r2, #2
	ldrsh r0, [r5, r2]
	add r6, r5, #0
	cmp r0, #0
	bgt _0800F65C
	bl gfx_related_sub_800F674
	mov r0, #1
	b _0800F66E
	.align 2, 0
.pool
_0800F65C:
	sub r0, r1, #1
	strh r0, [r6, #2]
_0800F660:
	mov r1, #2
	ldrsh r0, [r6, r1]
	cmp r0, r7
	ble _0800F66C
	mov r0, #0
	strb r0, [r6]
_0800F66C:
	mov r0, #0
_0800F66E:
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1

thumb_func_global gfx_related_sub_800F674
gfx_related_sub_800F674: @ 0x0800F674
	ldr r0, =0x03003BD0
	mov r1, #0
	strb r1, [r0]
	mov r2, #0
	strh r1, [r0, #4]
	strh r1, [r0, #2]
	strb r2, [r0, #0x1c]
	ldr r0, =0x03003D70
	strh r1, [r0]
	ldr r0, =0x03003D74
	strh r1, [r0]
	ldr r1, =0x03003D94
	mov r0, #0x10
	strh r0, [r1]
	bx lr
	.align 2, 0
.pool

thumb_func_global setBldCnt
setBldCnt: @ 0x0800F6A4
	push {r4, lr}
	lsl r0, r0, #0x18
	lsr r4, r0, #0x18
	lsl r1, r1, #0x18
	lsr r3, r1, #0x18
	ldr r1, =0x03003BD0
	mov r0, #0
	strb r0, [r1]
	strh r0, [r1, #4]
	strh r0, [r1, #2]
	mov r0, #1
	strb r0, [r1, #0x1c]
	cmp r3, #0
	beq _0800F700
	cmp r3, #1
	beq _0800F714
	ldr r1, =0x03003D94
	mov r2, #0x80
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	ldr r2, =0x03003D74
	mov r0, #0xfd
	lsl r0, r0, #6
	add r1, r0, #0
	add r0, r4, #0
	orr r0, r1
	strh r0, [r2]
	lsl r0, r3, #1
	ldr r1, =unk_803854a
	add r0, r0, r1
	mov r1, #0xa0
	lsl r1, r1, #0x13
	mov r2, #1
	bl Bios_memcpy
	b _0800F724
	.align 2, 0
.pool
_0800F700:
	ldr r1, =0x03003D70
	mov r0, #0x10
	strh r0, [r1]
	ldr r2, =0x03003D74
	mov r1, #0x80
	b _0800F71E
	.align 2, 0
.pool
_0800F714:
	ldr r1, =0x03003D70
	mov r0, #0x10
	strh r0, [r1]
	ldr r2, =0x03003D74
	mov r1, #0xc0
_0800F71E:
	add r0, r4, #0
	orr r0, r1
	strh r0, [r2]
_0800F724:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global unused_sub_800F734
unused_sub_800F734: @ 0x0800F734
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	mov r8, r0
	lsl r1, r1, #0x18
	lsr r7, r1, #0x18
	ldr r2, =0x03003BD0
	ldrb r0, [r2, #6]
	add r5, r2, #0
	cmp r0, #0
	beq _0800F75C
	cmp r0, #1
	beq _0800F7DC
	mov r0, #0
	strb r0, [r2, #6]
	b _0800F810
	.align 2, 0
.pool
_0800F75C:
	add r4, r7, #0
	mov r3, #0
	ldr r6, =0x03003D00
	ldr r0, =0x0000FFBF
	mov ip, r0
_0800F766:
	mov r0, #1
	and r0, r4
	cmp r0, #0
	beq _0800F784
	lsl r0, r3, #1
	add r0, r0, r6
	ldrh r2, [r0]
	mov r1, #0x40
	orr r1, r2
	b _0800F78E
	.align 2, 0
.pool
_0800F784:
	lsl r0, r3, #1
	add r0, r0, r6
	ldrh r2, [r0]
	mov r1, ip
	and r1, r2
_0800F78E:
	strh r1, [r0]
	lsr r4, r4, #1
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, #3
	bls _0800F766
	mov r1, #0
	mov r3, #0
	mov r0, #0xf
	and r0, r7
	cmp r0, #0
	beq _0800F7AC
	mov r3, #0x11
	mov r1, #0xff
_0800F7AC:
	mov r0, #0x10
	and r0, r7
	cmp r0, #0
	beq _0800F7C4
	mov r2, #0x88
	lsl r2, r2, #5
	add r0, r2, #0
	orr r3, r0
	mov r2, #0xff
	lsl r2, r2, #8
	add r0, r2, #0
	orr r1, r0
_0800F7C4:
	mov r0, #0
	strh r3, [r5, #0xc]
	strh r1, [r5, #0xe]
	mov r1, r8
	strh r1, [r5, #8]
	strh r0, [r5, #0xa]
	mov r0, #1
	strb r0, [r5, #0x1c]
	ldrb r0, [r5, #6]
	add r0, #1
	strb r0, [r5, #6]
	b _0800F810
_0800F7DC:
	ldrh r0, [r2, #8]
	sub r0, #1
	strh r0, [r2, #8]
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _0800F810
	ldrh r0, [r2, #0xc]
	ldrh r1, [r2, #0xa]
	add r0, r0, r1
	strh r0, [r2, #0xa]
	ldr r1, =0x03003D20
	strh r0, [r1]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldrh r1, [r2, #0xe]
	cmp r0, r1
	bne _0800F80C
	mov r0, #0
	strb r0, [r2, #6]
	mov r0, #1
	b _0800F812
	.align 2, 0
.pool
_0800F80C:
	mov r0, r8
	strh r0, [r2, #8]
_0800F810:
	mov r0, #0
_0800F812:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1

thumb_func_global unused_sub_800F81C
unused_sub_800F81C: @ 0x0800F81C
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	mov r8, r0
	lsl r1, r1, #0x18
	lsr r5, r1, #0x18
	ldr r2, =0x03003BD0
	ldrb r0, [r2, #6]
	add r7, r2, #0
	cmp r0, #0
	beq _0800F844
	cmp r0, #1
	beq _0800F8B0
	mov r0, #0
	strb r0, [r2, #6]
	b _0800F8E0
	.align 2, 0
.pool
_0800F844:
	add r4, r5, #0
	mov r3, #0
	ldr r6, =0x03003D00
	ldr r0, =0x0000FFBF
	mov ip, r0
_0800F84E:
	mov r0, #1
	and r0, r4
	cmp r0, #0
	beq _0800F86C
	lsl r0, r3, #1
	add r0, r0, r6
	ldrh r2, [r0]
	mov r1, #0x40
	orr r1, r2
	b _0800F876
	.align 2, 0
.pool
_0800F86C:
	lsl r0, r3, #1
	add r0, r0, r6
	ldrh r2, [r0]
	mov r1, ip
	and r1, r2
_0800F876:
	strh r1, [r0]
	lsr r4, r4, #1
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, #3
	bls _0800F84E
	mov r3, #0
	mov r0, #0xf
	and r0, r5
	cmp r0, #0
	beq _0800F890
	mov r3, #0xff
_0800F890:
	mov r0, #0x10
	and r0, r5
	cmp r0, #0
	beq _0800F8A0
	mov r1, #0xff
	lsl r1, r1, #8
	add r0, r1, #0
	orr r3, r0
_0800F8A0:
	strh r3, [r7, #0xc]
	mov r0, r8
	strh r0, [r7, #8]
	strh r3, [r7, #0xa]
	ldrb r0, [r7, #6]
	add r0, #1
	strb r0, [r7, #6]
	b _0800F8E0
_0800F8B0:
	ldrh r0, [r2, #8]
	sub r0, #1
	mov r3, #0
	strh r0, [r2, #8]
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _0800F8E0
	ldrh r0, [r2, #0xa]
	ldrh r1, [r2, #0xc]
	sub r0, r0, r1
	strh r0, [r2, #0xa]
	ldr r1, =0x03003D20
	strh r0, [r1]
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _0800F8DC
	strb r3, [r2, #6]
	strb r3, [r2, #0x1c]
	mov r0, #1
	b _0800F8E2
	.align 2, 0
.pool
_0800F8DC:
	mov r1, r8
	strh r1, [r2, #8]
_0800F8E0:
	mov r0, #0
_0800F8E2:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1

thumb_func_global unused_sub_800F8EC
unused_sub_800F8EC: @ 0x0800F8EC
	ldr r0, =0x03003BD0
	mov r1, #0
	strb r1, [r0, #6]
	mov r2, #0
	strh r1, [r0, #0xa]
	strh r1, [r0, #0xc]
	strh r1, [r0, #8]
	strb r2, [r0, #0x1c]
	ldr r0, =0x03003D20
	strh r1, [r0]
	bx lr
	.align 2, 0
.pool

thumb_func_global unused_sub_800F90C
unused_sub_800F90C: @ 0x0800F90C
	push {r4, lr}
	add r3, r0, #0
	add r4, r1, #0
	mov r1, #3
	and r0, r1
	cmp r0, #0
	beq _0800F920
	lsl r2, r2, #0xa
	lsr r2, r2, #0xb
	b _0800F956
_0800F920:
	add r0, r4, #0
	and r0, r1
	cmp r0, #0
	beq _0800F92E
	lsl r2, r2, #0xa
	lsr r2, r2, #0xb
	b _0800F956
_0800F92E:
	mov r0, #0x1f
	and r0, r2
	cmp r0, #0
	bne _0800F944
	lsl r2, r2, #9
	lsr r2, r2, #0xb
	add r0, r3, #0
	add r1, r4, #0
	bl Bios_memcpy_32b
	b _0800F96C
_0800F944:
	add r0, r2, #0
	and r0, r1
	cmp r0, #0
	bne _0800F960
	lsl r2, r2, #9
	lsr r2, r2, #0xb
	mov r0, #0x80
	lsl r0, r0, #0x13
	orr r2, r0
_0800F956:
	add r0, r3, #0
	add r1, r4, #0
	bl Bios_memcpy
	b _0800F96C
_0800F960:
	lsl r2, r2, #0xa
	lsr r2, r2, #0xb
	add r0, r3, #0
	add r1, r4, #0
	bl Bios_memcpy
_0800F96C:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global TransitionToBlack
TransitionToBlack: @ 0x0800F974
	push {r4, r5, lr}
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	lsl r1, r1, #0x10
	lsr r5, r1, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	ldr r1, =0x03003BD0
	mov r0, #2
	strb r0, [r1, #0x1c]
	cmp r4, #1
	beq _0800F9A8
	cmp r4, #1
	ble _0800F99C
	cmp r4, #2
	beq _0800F9B2
	cmp r4, #3
	beq _0800F9BC
_0800F99C:
	add r0, r5, #0
	bl sub_0800FA38
	b _0800F9C8
	.align 2, 0
.pool
_0800F9A8:
	add r0, r5, #0
	add r1, r2, #0
	add r2, r3, #0
	mov r3, #0
	b _0800F9C4
_0800F9B2:
	add r0, r5, #0
	add r1, r2, #0
	add r2, r3, #0
	mov r3, #1
	b _0800F9C4
_0800F9BC:
	add r0, r5, #0
	add r1, r2, #0
	add r2, r3, #0
	mov r3, #2
_0800F9C4:
	bl TransitionToBlackCore
_0800F9C8:
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global undoScreenTransition
undoScreenTransition: @ 0x0800F9D4
	push {lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	cmp r0, #1
	beq _0800F9FE
	cmp r0, #1
	ble _0800F9F6
	cmp r0, #2
	beq _0800FA08
	cmp r0, #3
	beq _0800FA12
_0800F9F6:
	add r0, r1, #0
	bl sub_0800FB00
	b _0800FA1E
_0800F9FE:
	add r0, r1, #0
	add r1, r2, #0
	add r2, r3, #0
	mov r3, #0
	b _0800FA1A
_0800FA08:
	add r0, r1, #0
	add r1, r2, #0
	add r2, r3, #0
	mov r3, #1
	b _0800FA1A
_0800FA12:
	add r0, r1, #0
	add r1, r2, #0
	add r2, r3, #0
	mov r3, #2
_0800FA1A:
	bl sub_0800FE34
_0800FA1E:
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	cmp r2, #0
	beq _0800FA2C
	ldr r1, =0x03003BD0
	mov r0, #0
	strb r0, [r1, #0x1c]
_0800FA2C:
	add r0, r2, #0
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_0800FA38
sub_0800FA38: @ 0x0800FA38
	push {r4, r5, r6, lr}
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	ldr r5, =0x03003BD0
	ldrb r2, [r5, #0x10]
	cmp r2, #0
	beq _0800FA58
	cmp r2, #1
	beq _0800FA9C
	mov r0, #0
	strb r0, [r5, #0x10]
	mov r0, #1
	b _0800FAF2
	.align 2, 0
.pool
_0800FA58:
	strh r2, [r5, #0x12]
	ldrb r0, [r5, #0x10]
	add r0, #1
	strb r0, [r5, #0x10]
	ldr r1, =0x03003D90
	mov r0, #0xf0
	strh r0, [r1]
	ldr r1, =0x03003D88
	mov r0, #0xa0
	strh r0, [r1]
	ldr r1, =0x03003D7C
	mov r0, #0x3f
	strh r0, [r1]
	ldr r0, =0x03003D8C
	strh r2, [r0]
	ldr r2, =0x03003CD4
	ldrh r0, [r2]
	mov r3, #0x80
	lsl r3, r3, #6
	add r1, r3, #0
	orr r0, r1
	strh r0, [r2]
	b _0800FAF0
	.align 2, 0
.pool
_0800FA9C:
	ldrh r0, [r5, #0x12]
	add r0, #1
	strh r0, [r5, #0x12]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, r6
	bls _0800FAB2
	strh r6, [r5, #0x12]
	ldrb r0, [r5, #0x10]
	add r0, #1
	strb r0, [r5, #0x10]
_0800FAB2:
	ldrh r1, [r5, #0x12]
	lsl r0, r1, #4
	sub r0, r0, r1
	lsl r0, r0, #3
	add r1, r6, #0
	bl Bios_Div
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	ldrh r1, [r5, #0x12]
	lsl r0, r1, #2
	add r0, r0, r1
	lsl r0, r0, #4
	add r1, r6, #0
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r3, =0x03003D90
	lsl r2, r4, #8
	mov r1, #0xf0
	sub r1, r1, r4
	orr r2, r1
	strh r2, [r3]
	ldr r3, =0x03003D88
	lsl r2, r0, #8
	mov r1, #0xa0
	sub r1, r1, r0
	orr r2, r1
	strh r2, [r3]
_0800FAF0:
	mov r0, #0
_0800FAF2:
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_0800FB00
sub_0800FB00: @ 0x0800FB00
	push {r4, r5, r6, lr}
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	ldr r5, =0x03003BD0
	ldrb r0, [r5, #0x10]
	cmp r0, #0
	beq _0800FB50
	cmp r0, #1
	beq _0800FB5A
	mov r1, #0
	strb r1, [r5, #0x10]
	ldr r0, =0x03003D7C
	strh r1, [r0]
	ldr r0, =0x03003D8C
	strh r1, [r0]
	ldr r0, =0x03003D90
	strh r1, [r0]
	ldr r0, =0x03003D88
	strh r1, [r0]
	ldr r2, =0x03003CD4
	ldrh r1, [r2]
	ldr r0, =0x0000DFFF
	and r0, r1
	strh r0, [r2]
	mov r0, #1
	b _0800FBB4
	.align 2, 0
.pool
_0800FB50:
	strh r0, [r5, #0x12]
	ldrb r0, [r5, #0x10]
	add r0, #1
	strb r0, [r5, #0x10]
	b _0800FBB2
_0800FB5A:
	ldrh r0, [r5, #0x12]
	add r0, #1
	strh r0, [r5, #0x12]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, r6
	bls _0800FB70
	strh r6, [r5, #0x12]
	ldrb r0, [r5, #0x10]
	add r0, #1
	strb r0, [r5, #0x10]
_0800FB70:
	ldrh r1, [r5, #0x12]
	lsl r0, r1, #4
	sub r0, r0, r1
	lsl r0, r0, #3
	add r1, r6, #0
	bl Bios_Div
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	ldrh r1, [r5, #0x12]
	lsl r0, r1, #2
	add r0, r0, r1
	lsl r0, r0, #4
	add r1, r6, #0
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r2, =0x03003D90
	mov r1, #0x78
	sub r1, r1, r4
	lsl r1, r1, #8
	add r4, #0x78
	orr r1, r4
	strh r1, [r2]
	ldr r2, =0x03003D88
	mov r1, #0x50
	sub r1, r1, r0
	lsl r1, r1, #8
	add r0, #0x50
	orr r1, r0
	strh r1, [r2]
_0800FBB2:
	mov r0, #0
_0800FBB4:
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global TransitionToBlackCore
TransitionToBlackCore: @ 0x0800FBC4
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #8
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	lsl r1, r1, #0x10
	lsr r5, r1, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	mov r8, r2
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	str r3, [sp, #4]
	ldr r3, =0x03003BD0
	ldrb r2, [r3, #0x10]
	cmp r2, #0
	beq _0800FC08
	cmp r2, #1
	bne _0800FBF2
	b _0800FD00
_0800FBF2:
	mov r0, #0
	strb r0, [r3, #0x10]
	ldr r0, [sp, #4]
	cmp r0, #0
	bne _0800FBFE
	b _0800FE1C
_0800FBFE:
	bl endScreenTransition
	b _0800FE1C
	.align 2, 0
.pool
_0800FC08:
	strh r2, [r3, #0x12]
	ldrb r0, [r3, #0x10]
	add r0, #1
	strb r0, [r3, #0x10]
	ldr r1, =0x03003D90
	mov r0, #0xf0
	strh r0, [r1]
	ldr r1, =0x03003D88
	mov r0, #0xa0
	strh r0, [r1]
	ldr r1, =0x03003D7C
	mov r0, #0x3f
	strh r0, [r1]
	ldr r0, =0x03003D8C
	strh r2, [r0]
	ldr r2, =0x03003CD4
	ldrh r0, [r2]
	mov r4, #0x80
	lsl r4, r4, #6
	add r1, r4, #0
	orr r0, r1
	strh r0, [r2]
	strh r5, [r3, #0x16]
	mov r0, r8
	strh r0, [r3, #0x18]
	cmp r5, #0x77
	bhi _0800FC60
	ldr r1, =0x03000AF8
	mov r0, #0xf0
	sub r0, r0, r5
	strh r0, [r1]
	b _0800FC64
	.align 2, 0
.pool
_0800FC60:
	ldr r0, =0x03000AF8
	strh r5, [r0]
_0800FC64:
	mov r0, sp
	mov r4, #0xf0
	strh r4, [r0]
	ldr r1, =0x0200A210
	ldr r5, =0x010001E0
	add r2, r5, #0
	bl Bios_memcpy
	mov r0, sp
	add r0, #2
	strh r4, [r0]
	ldr r1, =0x02020160
	add r2, r5, #0
	bl Bios_memcpy
	ldr r1, [sp, #4]
	cmp r1, #1
	beq _0800FCC0
	cmp r1, #1
	bgt _0800FCA4
	cmp r1, #0
	beq _0800FCAC
	b _0800FE20
	.align 2, 0
.pool
_0800FCA4:
	ldr r2, [sp, #4]
	cmp r2, #2
	beq _0800FCE4
	b _0800FE20
_0800FCAC:
	ldr r0, =0x03003BD0
	ldr r1, =0x03000AF8
	ldrh r1, [r1]
	strh r1, [r0, #0x1a]
	b _0800FE20
	.align 2, 0
.pool
_0800FCC0:
	ldr r4, =0x03000AF8
	mov r0, #0
	ldrsh r1, [r4, r0]
	lsl r0, r1, #1
	mul r0, r1, r0
	bl Bios_Sqrt
	strh r0, [r4]
	ldr r1, =0x03003BD0
	strh r0, [r1, #0x1a]
	bl startScreenTransition
	b _0800FE20
	.align 2, 0
.pool
_0800FCE4:
	ldr r0, =0x03000AF8
	ldrh r1, [r0]
	lsl r1, r1, #3
	strh r1, [r0]
	ldr r0, =0x03003BD0
	strh r1, [r0, #0x1a]
	bl startScreenTransition
	b _0800FE20
	.align 2, 0
.pool
_0800FD00:
	ldrh r0, [r3, #0x12]
	add r0, #1
	strh r0, [r3, #0x12]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, r7
	bls _0800FD16
	strh r7, [r3, #0x12]
	ldrb r0, [r3, #0x10]
	add r0, #1
	strb r0, [r3, #0x10]
_0800FD16:
	ldrh r1, [r3, #0x1a]
	sub r0, r5, r1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sb, r0
	add r0, r5, r1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	mov r2, r8
	sub r0, r2, r1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sl, r0
	add r0, r2, r1
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	ldrh r0, [r3, #0x12]
	cmp r7, r0
	bne _0800FD40
	mov r2, #0
	b _0800FD50
_0800FD40:
	lsl r0, r1, #0x10
	asr r0, r0, #0x10
	ldrh r1, [r3, #0x12]
	sub r1, r7, r1
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
_0800FD50:
	mov r0, sb
	add r1, r2, r0
	lsl r1, r1, #0x10
	sub r0, r4, r2
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	lsr r0, r1, #0x10
	mov sb, r0
	asr r1, r1, #0x10
	cmp r1, r5
	ble _0800FD68
	mov sb, r5
_0800FD68:
	lsl r0, r4, #0x10
	asr r0, r0, #0x10
	cmp r0, r5
	bge _0800FD72
	add r4, r5, #0
_0800FD72:
	mov r1, sb
	lsl r0, r1, #0x10
	cmp r0, #0
	bge _0800FD7E
	mov r0, #0
	mov sb, r0
_0800FD7E:
	lsl r0, r4, #0x10
	asr r0, r0, #0x10
	cmp r0, #0xf0
	ble _0800FD88
	mov r4, #0xf0
_0800FD88:
	mov r0, sl
	add r1, r2, r0
	lsl r1, r1, #0x10
	sub r0, r6, r2
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	lsr r0, r1, #0x10
	mov sl, r0
	asr r1, r1, #0x10
	cmp r1, r8
	ble _0800FDA0
	mov sl, r8
_0800FDA0:
	lsl r0, r6, #0x10
	asr r0, r0, #0x10
	cmp r0, r8
	bge _0800FDAA
	mov r6, r8
_0800FDAA:
	mov r1, sl
	lsl r0, r1, #0x10
	cmp r0, #0
	bge _0800FDB6
	mov r0, #0
	mov sl, r0
_0800FDB6:
	lsl r0, r6, #0x10
	asr r0, r0, #0x10
	cmp r0, #0xa0
	ble _0800FDC0
	mov r6, #0xa0
_0800FDC0:
	ldr r1, =0x03003BD0
	strh r5, [r1, #0x16]
	mov r0, r8
	strh r0, [r1, #0x18]
	ldrh r0, [r1, #0x1a]
	sub r0, r0, r2
	cmp r0, #0
	bge _0800FDD2
	mov r0, #0
_0800FDD2:
	strh r0, [r1, #0x1a]
	ldr r2, =0x03003D90
	mov r0, sb
	lsl r1, r0, #0x10
	asr r1, r1, #8
	lsl r0, r4, #0x10
	asr r0, r0, #0x10
	orr r0, r1
	strh r0, [r2]
	ldr r2, =0x03003D88
	mov r4, sl
	lsl r1, r4, #0x10
	asr r1, r1, #8
	lsl r0, r6, #0x10
	asr r0, r0, #0x10
	orr r0, r1
	strh r0, [r2]
	ldr r0, [sp, #4]
	cmp r0, #1
	beq _0800FE10
	cmp r0, #1
	ble _0800FE20
	cmp r0, #2
	beq _0800FE16
	b _0800FE20
	.align 2, 0
.pool
_0800FE10:
	bl sub_080100B4
	b _0800FE20
_0800FE16:
	bl sub_080101C4
	b _0800FE20
_0800FE1C:
	mov r0, #1
	b _0800FE22
_0800FE20:
	mov r0, #0
_0800FE22:
	add sp, #8
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global sub_0800FE34
sub_0800FE34: @ 0x0800FE34
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #4
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov ip, r0
	lsl r1, r1, #0x10
	lsr r6, r1, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	mov sl, r2
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	mov sb, r3
	ldr r2, =0x03003BD0
	ldrb r3, [r2, #0x10]
	cmp r3, #0
	beq _0800FEAC
	cmp r3, #1
	bne _0800FE64
	b _0800FF5C
_0800FE64:
	mov r1, #0
	strb r1, [r2, #0x10]
	ldr r0, =0x03003D7C
	strh r1, [r0]
	ldr r0, =0x03003D8C
	strh r1, [r0]
	ldr r0, =0x03003D90
	strh r1, [r0]
	ldr r0, =0x03003D88
	strh r1, [r0]
	ldr r2, =0x03003CD4
	ldrh r1, [r2]
	ldr r0, =0x0000DFFF
	and r0, r1
	strh r0, [r2]
	mov r0, sb
	cmp r0, #0
	bne _0800FE8A
	b _08010088
_0800FE8A:
	bl endScreenTransition
	b _08010088
	.align 2, 0
.pool
_0800FEAC:
	strh r3, [r2, #0x12]
	ldrb r0, [r2, #0x10]
	add r0, #1
	strb r0, [r2, #0x10]
	ldr r1, =0x03003D90
	lsl r0, r6, #8
	orr r0, r6
	strh r0, [r1]
	ldr r1, =0x03003D88
	mov r4, sl
	lsl r0, r4, #8
	orr r0, r4
	strh r0, [r1]
	strh r6, [r2, #0x16]
	strh r4, [r2, #0x18]
	strh r3, [r2, #0x1a]
	cmp r6, #0x77
	bhi _0800FEE8
	ldr r1, =0x03000AF8
	mov r0, #0xf0
	sub r0, r0, r6
	strh r0, [r1]
	b _0800FEEC
	.align 2, 0
.pool
_0800FEE8:
	ldr r0, =0x03000AF8
	strh r6, [r0]
_0800FEEC:
	mov r0, sp
	mov r4, #0
	strh r4, [r0]
	ldr r1, =0x0200A210
	ldr r5, =0x010001E0
	add r2, r5, #0
	bl Bios_memcpy
	mov r0, sp
	add r0, #2
	strh r4, [r0]
	ldr r1, =0x02020160
	add r2, r5, #0
	bl Bios_memcpy
	mov r0, sb
	cmp r0, #1
	beq _0800FF2C
	cmp r0, #1
	bgt _0800FF16
	b _0801008C
_0800FF16:
	cmp r0, #2
	beq _0800FF48
	b _0801008C
	.align 2, 0
.pool
_0800FF2C:
	ldr r4, =0x03000AF8
	mov r2, #0
	ldrsh r1, [r4, r2]
	lsl r0, r1, #1
	mul r0, r1, r0
	bl Bios_Sqrt
	strh r0, [r4]
	bl startScreenTransition
	b _0801008C
	.align 2, 0
.pool
_0800FF48:
	ldr r1, =0x03000AF8
	ldrh r0, [r1]
	lsl r0, r0, #3
	strh r0, [r1]
	bl startScreenTransition
	b _0801008C
	.align 2, 0
.pool
_0800FF5C:
	ldrh r0, [r2, #0x12]
	add r0, #1
	strh r0, [r2, #0x12]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, ip
	bls _0800FF74
	mov r3, ip
	strh r3, [r2, #0x12]
	ldrb r0, [r2, #0x10]
	add r0, #1
	strb r0, [r2, #0x10]
_0800FF74:
	ldrh r2, [r2, #0x1a]
	sub r1, r6, r2
	lsl r1, r1, #0x10
	add r0, r6, r2
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	mov r3, sl
	sub r0, r3, r2
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r8, r0
	add r0, r3, r2
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	lsr r7, r1, #0x10
	cmp r1, #0
	bge _0800FF98
	mov r7, #0
_0800FF98:
	lsl r0, r4, #0x10
	asr r0, r0, #0x10
	cmp r0, #0xf0
	ble _0800FFA2
	mov r4, #0xf0
_0800FFA2:
	mov r1, r8
	lsl r0, r1, #0x10
	cmp r0, #0
	bge _0800FFAE
	mov r2, #0
	mov r8, r2
_0800FFAE:
	lsl r0, r5, #0x10
	asr r0, r0, #0x10
	cmp r0, #0xa0
	ble _0800FFB8
	mov r5, #0xa0
_0800FFB8:
	mov r0, #0xf0
	lsl r1, r4, #0x10
	asr r1, r1, #0x10
	sub r0, r0, r1
	lsl r1, r7, #0x10
	lsl r0, r0, #0x10
	ldr r0, =0x03000AF8
	ldr r3, =0x03003BD0
	ldrh r0, [r0]
	ldrh r1, [r3, #0x1a]
	sub r0, r0, r1
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	ldrh r0, [r3, #0x12]
	cmp ip, r0
	bne _0800FFE4
	mov r2, #0
	b _0800FFF6
	.align 2, 0
.pool
_0800FFE4:
	lsl r0, r2, #0x10
	asr r0, r0, #0x10
	ldrh r1, [r3, #0x12]
	mov r2, ip
	sub r1, r2, r1
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
_0800FFF6:
	sub r1, r7, r2
	lsl r1, r1, #0x10
	add r0, r2, r4
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	lsr r7, r1, #0x10
	cmp r1, #0
	bge _08010008
	mov r7, #0
_08010008:
	lsl r0, r4, #0x10
	asr r0, r0, #0x10
	cmp r0, #0xf0
	ble _08010012
	mov r4, #0xf0
_08010012:
	mov r3, r8
	sub r1, r3, r2
	lsl r1, r1, #0x10
	add r0, r2, r5
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	lsr r0, r1, #0x10
	mov r8, r0
	cmp r1, #0
	bge _0801002A
	mov r1, #0
	mov r8, r1
_0801002A:
	lsl r0, r5, #0x10
	asr r0, r0, #0x10
	cmp r0, #0xa0
	ble _08010034
	mov r5, #0xa0
_08010034:
	ldr r1, =0x03003BD0
	strh r6, [r1, #0x16]
	mov r3, sl
	strh r3, [r1, #0x18]
	ldrh r0, [r1, #0x1a]
	add r0, r2, r0
	strh r0, [r1, #0x1a]
	ldr r2, =0x03003D90
	lsl r1, r7, #0x10
	asr r1, r1, #8
	lsl r0, r4, #0x10
	asr r0, r0, #0x10
	orr r0, r1
	strh r0, [r2]
	ldr r2, =0x03003D88
	mov r4, r8
	lsl r1, r4, #0x10
	asr r1, r1, #8
	lsl r0, r5, #0x10
	asr r0, r0, #0x10
	orr r0, r1
	strh r0, [r2]
	mov r0, sb
	cmp r0, #1
	beq _0801007C
	cmp r0, #1
	ble _0801008C
	cmp r0, #2
	beq _08010082
	b _0801008C
	.align 2, 0
.pool
_0801007C:
	bl sub_080100B4
	b _0801008C
_08010082:
	bl sub_080101C4
	b _0801008C
_08010088:
	mov r0, #1
	b _0801008E
_0801008C:
	mov r0, #0
_0801008E:
	add sp, #4
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global startScreenTransition
startScreenTransition: @ 0x080100A0
	push {lr}
	ldr r0, =(callb_screenTransition+1)
	mov r1, #0xff
	bl addCallback
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_080100B4
sub_080100B4: @ 0x080100B4
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	ldr r1, =0x03003BD0
	ldrh r0, [r1, #0x1a]
	add r2, r0, #0
	mul r2, r0, r2
	mov r8, r2
	mov r5, #0
	add r6, r1, #0
	ldr r7, =0x0200A210
_080100CA:
	ldrh r2, [r6, #0x18]
	ldrh r1, [r6, #0x1a]
	sub r0, r2, r1
	cmp r5, r0
	blt _080100DA
	add r0, r2, r1
	cmp r5, r0
	ble _080100F0
_080100DA:
	lsl r0, r5, #1
	add r0, r0, r7
	ldr r1, =0x0000FFFF
	strh r1, [r0]
	b _08010136
	.align 2, 0
.pool
_080100F0:
	sub r0, r2, r5
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	add r1, r0, #0
	mul r1, r0, r1
	add r0, r1, #0
	mov r2, r8
	sub r0, r2, r0
	bl Bios_Sqrt
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldrh r2, [r6, #0x16]
	sub r1, r2, r0
	lsl r1, r1, #0x10
	add r0, r0, r2
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	lsr r4, r1, #0x10
	cmp r1, #0
	bge _0801011C
	mov r4, #0
_0801011C:
	lsl r0, r3, #0x10
	asr r0, r0, #0x10
	cmp r0, #0xf0
	ble _08010126
	mov r3, #0xf0
_08010126:
	lsl r2, r5, #1
	add r2, r2, r7
	lsl r1, r4, #0x10
	asr r1, r1, #8
	lsl r0, r3, #0x10
	asr r0, r0, #0x10
	orr r0, r1
	strh r0, [r2]
_08010136:
	ldr r2, =0x0200A210
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, #0x9f
	bls _080100CA
	ldr r1, =0x03003D90
	ldrh r0, [r2]
	strh r0, [r1]
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global callb_screenTransition
callb_screenTransition: @ 0x0801015C
	push {r4, lr}
	ldr r0, =0x0200A212
	ldr r4, =0x02020160
	mov r2, #0xf0
	lsl r2, r2, #1
	add r1, r4, #0
	bl Bios_memcpy
	mov r0, #0x9f
	lsl r0, r0, #1
	add r1, r4, r0
	mov r0, #0
	strh r0, [r1]
	ldr r2, =0x040000B0
	ldr r0, [r2, #8]
	mov r1, #0x80
	lsl r1, r1, #0x18
	cmp r0, #0
	bge _0801018A
_08010182:
	ldr r0, [r2, #8]
	and r0, r1
	cmp r0, #0
	bne _08010182
_0801018A:
	ldr r1, =0x040000B0
	ldr r0, =0x02020160
	str r0, [r1]
	ldr r0, =0x04000040
	str r0, [r1, #4]
	ldr r0, =0xA2400001
	str r0, [r1, #8]
	ldr r0, [r1, #8]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global endScreenTransition
endScreenTransition: @ 0x080101B4
	push {lr}
	ldr r0, =(callb_screenTransition+1)
	bl deleteCallback
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_080101C4
sub_080101C4: @ 0x080101C4
	push {r4, r5, r6, lr}
	mov r4, #0
	ldr r5, =0x03003BD0
	ldr r6, =0x0200A210
_080101CC:
	ldrh r0, [r5, #0x18]
	sub r0, r0, r4
	cmp r0, #0
	bne _080101E8
	ldrh r1, [r5, #0x16]
	ldrh r0, [r5, #0x1a]
	sub r3, r1, r0
	add r2, r1, r0
	b _08010220
	.align 2, 0
.pool
_080101E8:
	lsl r2, r0, #3
	ldrh r1, [r5, #0x1a]
	cmp r2, r1
	ble _080101F6
	sub r0, r0, r1
	mov r1, #7
	b _08010210
_080101F6:
	cmp r0, #0
	ble _08010200
	sub r0, r2, r0
	sub r2, r0, r1
	b _08010216
_08010200:
	cmn r2, r1
	ble _0801020A
	sub r0, r0, r2
	sub r2, r0, r1
	b _08010216
_0801020A:
	add r0, r0, r1
	mov r1, #7
	neg r1, r1
_08010210:
	bl Bios_Div
	add r2, r0, #0
_08010216:
	ldr r1, =0x03003BD0
	ldrh r0, [r1, #0x16]
	add r3, r2, r0
	sub r2, r0, r2
	add r5, r1, #0
_08010220:
	cmp r3, #0
	bge _08010226
	mov r3, #0
_08010226:
	cmp r2, #0xf0
	ble _0801022C
	mov r2, #0xf0
_0801022C:
	ldr r0, =0x0200A210
	lsl r1, r4, #1
	add r1, r1, r0
	lsl r0, r3, #8
	orr r0, r2
	strh r0, [r1]
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0x9f
	bls _080101CC
	ldr r1, =0x03003D90
	ldrh r0, [r6]
	strh r0, [r1]
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0801025C
sub_0801025C: @ 0x0801025C
	ldr r1, =0x03003BD0
	mov r0, #0
	mov r2, #0
	strh r2, [r1, #0x12]
	strb r0, [r1, #0x10]
	mov r0, #2
	strb r0, [r1, #0x1c]
	ldr r1, =0x03003D7C
	mov r0, #0x3f
	strh r0, [r1]
	ldr r0, =0x03003D8C
	strh r2, [r0]
	ldr r2, =0x03003CD4
	ldrh r0, [r2]
	mov r3, #0x80
	lsl r3, r3, #6
	add r1, r3, #0
	orr r0, r1
	strh r0, [r2]
	ldr r1, =0x03003D90
	ldr r0, =0x00007878
	strh r0, [r1]
	ldr r1, =0x03003D88
	ldr r0, =0x00005050
	strh r0, [r1]
	bx lr
	.align 2, 0
.pool

thumb_func_global sub_080102B0
sub_080102B0: @ 0x080102B0
	ldr r0, =0x03003BD0
	mov r2, #0
	mov r1, #0
	strh r1, [r0, #0x12]
	strb r2, [r0, #0x10]
	strb r2, [r0, #0x1c]
	ldr r0, =0x03003D7C
	strh r1, [r0]
	ldr r0, =0x03003D8C
	strh r1, [r0]
	ldr r0, =0x03003D90
	strh r1, [r0]
	ldr r0, =0x03003D88
	strh r1, [r0]
	ldr r2, =0x03003CD4
	ldrh r1, [r2]
	ldr r0, =0x0000DFFF
	and r0, r1
	strh r0, [r2]
	bx lr
	.align 2, 0
.pool

thumb_func_global sub_80102F4
sub_80102F4: @ 0x080102F4
	push {lr}
	bl rand
	ldr r1, =0x7FFF8000
	bl idivmod
	lsl r0, r0, #1
	lsr r0, r0, #0x10
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global random
random: @ 0x0801030C
	push {r4, lr}
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	bl sub_80102F4
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mul r0, r4, r0
	lsr r0, r0, #0x10
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global unused_sub_8010328
unused_sub_8010328: @ 0x08010328
	ldr r1, =0x03000AFC
	mov r0, #0
	str r0, [r1]
	bx lr
	.align 2, 0
.pool
	
	
	.thumb_func @ unused_sub_8010334
unused_sub_8010334: @ 0x08010334
	ldr r1, =0x03000AFC
	str r0, [r1]
	bx lr
	.align 2, 0
.pool



	.thumb_func @ unused_sub_8010340
unused_sub_8010340: @ 0x08010340
	push {lr}
	ldr r0, =0x03000AFC
	ldr r0, [r0]
	cmp r0, #0
	beq _0801034E
	bl call_r0
_0801034E:
	pop {r0}
	bx r0
	.align 2, 0
.pool



	.thumb_func @ unused_sub_8010358
unused_sub_8010358: @ 0x08010358
	ldr r1, =0x03000B00
	mov r0, #0
	str r0, [r1]
	bx lr
	.align 2, 0
.pool



	.thumb_func @ unused_sub_8010364
unused_sub_8010364: @ 0x08010364
	ldr r1, =0x03000B00
	str r0, [r1]
	bx lr
	.align 2, 0
.pool



	.thumb_func @ unused_sub_8010370
unused_sub_8010370: @ 0x08010370
	push {lr}
	ldr r0, =0x03000B00
	ldr r0, [r0]
	cmp r0, #0
	beq _0801037E
	bl call_r0
_0801037E:
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global initSound
initSound: @ 0x08010388
	push {lr}
	sub sp, #4
	ldr r2, =0x03002080
	ldrb r0, [r2, #8]
	mov r1, #8
	orr r0, r1
	strb r0, [r2, #8]
	bl m4aSoundInit
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r1, =0x03000B08
	ldr r2, =0x0100000A
	mov r0, sp
	bl Bios_memcpy
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
	.thumb_func @ sub_80103BC
sub_80103BC: @ 0x080103BC
	push {lr}
	ldr r2, =0x03002080
	ldrb r1, [r2, #8]
	mov r0, #9
	neg r0, r0
	and r0, r1
	strb r0, [r2, #8]
	bl m4aMPlayAllStop
	bl Bios_SoundDriverVSyncOff
	pop {r0}
	bx r0
	.align 2, 0
.pool



	.thumb_func @ sub_80103DC
sub_80103DC: @ 0x080103DC
	push {lr}
	ldr r2, =0x03002080
	ldrb r1, [r2, #8]
	lsl r0, r1, #0x1c
	cmp r0, #0
	blt _080103F2
	mov r0, #8
	orr r0, r1
	strb r0, [r2, #8]
	bl Bios_SoundDriverVSyncOn
_080103F2:
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	

thumb_func_global nullsub_2
nullsub_2: @ 0x080103FC
	bx lr
	.align 2, 0

thumb_func_global nullsub_3
nullsub_3: @ 0x08010400
	bx lr
	.align 2, 0

thumb_func_global nullsub_14
nullsub_14: @ 0x08010404
	bx lr
	.align 2, 0

thumb_func_global nullsub_15
nullsub_15: @ 0x08010408
	bx lr
	.align 2, 0
	
	
	.thumb_func @ sub_801040C
sub_801040C: @ 0x0801040C
	ldr r2, =0x03002080
	ldrb r1, [r2, #8]
	lsl r0, r1, #0x1a
	cmp r0, #0
	bge locret_801041E
	mov r0, #0x21
	neg r0, r0
	and r0, r1
	strb r0, [r2, #8]
locret_801041E:
	bx lr
	.align 2, 0
.pool


	.thumb_func @ nullsub_19
nullsub_19: @ 0x08010424
	bx lr
	.align 2, 0
	
	
	.thumb_func @ sub_8010428
sub_8010428: @ 0x08010428
	ldr r2, =0x03002080
	ldrb r1, [r2, #8]
	mov r0, #0x41
	neg r0, r0
	and r0, r1
	strb r0, [r2, #8]
	bx lr
	.align 2, 0
.pool



	.thumb_func @ sub_801043C
sub_801043C: @ 0x0801043C
	push {lr}
	ldr r2, =0x03002080
	ldrb r1, [r2, #8]
	lsr r0, r1, #7
	cmp r0, #0
	bne _08010456
	mov r0, #0x80
	orr r0, r1
	strb r0, [r2, #8]
	ldr r0, =musicPlayer_1
	ldr r0, [r0, #0x3c]
	bl MPlayStop_rev01
_08010456:
	pop {r0}
	bx r0
	.align 2, 0
.pool



	.thumb_func @ sub_8010464
sub_8010464: @ 0x08010464
	ldr r2, =0x03002080
	ldrb r1, [r2, #8]
	mov r0, #0x7f
	and r0, r1
	strb r0, [r2, #8]
	bx lr
	.align 2, 0
.pool
	
	

thumb_func_global playSong
playSong: @ 0x08010474
	push {r4, r5, lr}
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0
	beq _080104E2
	add r0, r4, #0
	sub r0, #0x10
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0xb7
	bls _080104E2
	mov r0, #0xe3
	lsl r0, r0, #1
	cmp r4, r0
	bhi _080104E2
	ldr r1, =m4a_songs
	lsl r0, r4, #3
	add r0, r0, r1
	ldrb r5, [r0, #4]
	cmp r5, #1
	bgt _080104B0
	cmp r5, #0
	blt _080104B0
	add r0, r4, #0
	bl m4aSongNumStartOrChange
	b _080104CA
	.align 2, 0
.pool
_080104B0:
	ldr r0, =0x03003D40
	ldrb r0, [r0, #0x12]
	cmp r0, #0
	bne _080104E2
	add r0, r4, #0
	bl sub_08013DE0
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _080104E2
	add r0, r4, #0
	bl song_play_and_auto_config
_080104CA:
	ldr r1, =0x03000B08
	lsl r0, r5, #2
	add r5, r0, r1
	mov r0, #1
	strh r0, [r5, #2]
	add r0, r4, #0
	bl sub_08010730
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _080104E2
	strh r4, [r5]
_080104E2:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
	
thumb_func_global sub_80104F0
sub_80104F0: @ 0x080104F0
	push {lr}
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r0, =0x03003D18
	ldrb r0, [r0]
	cmp r0, #2
	beq _08010510
	add r0, r2, #0
	bl playSong
	b _08010516
	.align 2, 0
.pool
_08010510:
	add r0, r1, #0
	bl playSong
_08010516:
	pop {r0}
	bx r0
	.align 2, 0


thumb_func_global fadeoutSong
fadeoutSong: @ 0x0801051C
	push {r4, lr}
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r2, =musicPlayer_1
	lsl r0, r4, #1
	add r0, r0, r4
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r0, [r0]
	bl m4aMPlayFadeOut
	ldr r0, =0x03000B08
	lsl r4, r4, #2
	add r4, r4, r0
	mov r0, #2
	strh r0, [r4, #2]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global sub_8010550
sub_8010550:
	push {r4, lr}
	lsl r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r2, =m4a_songs
	lsr r0, r0, #0xd
	add r0, r0, r2
	ldrb r4, [r0, #4]
	ldr r2, =musicPlayer_1
	lsl r0, r4, #1
	add r0, r0, r4
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r0, [r0]
	bl m4aMPlayFadeOut
	ldr r0, =0x03000B08
	lsl r4, r4, #2
	add r4, r4, r0
	mov r0, #2
	strh r0, [r4, #2]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global stopTrack
stopTrack: @ 0x0801058C
	push {r4, lr}
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	ldr r1, =musicPlayer_1
	lsl r0, r4, #1
	add r0, r0, r4
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl MPlayStop_rev01
	ldr r0, =0x03000B08
	lsl r4, r4, #2
	add r4, r4, r0
	mov r0, #0
	strh r0, [r4]
	strh r0, [r4, #2]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global stopSong_maybe
stopSong_maybe: @ 0x080105C0
	push {r4, lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r2, =m4a_songs
	lsl r1, r0, #3
	add r1, r1, r2
	ldrb r4, [r1, #4]
	bl m4aSongNumStop
	ldr r0, =0x03000B08
	lsl r4, r4, #2
	add r4, r4, r0
	mov r0, #0
	strh r0, [r4]
	strh r0, [r4, #2]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global stopSoundTracks
stopSoundTracks: @ 0x080105EC
	push {lr}
	mov r0, #0
	bl stopTrack
	mov r0, #1
	bl stopTrack
	mov r0, #2
	bl stopTrack
	mov r0, #3
	bl stopTrack
	mov r0, #4
	bl stopTrack
	mov r0, #5
	bl stopTrack
	pop {r0}
	bx r0
	.align 2, 0
	


	thumb_func_global sub_8010618
	sub_8010618:
	push {r4, lr}
	mov r4, #0
_0801061C:
	cmp r4, #1
	bls _08010626
	add r0, r4, #0
	bl stopTrack
_08010626:
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #4
	bls _0801061C
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
	
	
	.thumb_func @ sub_8010638
sub_8010638: @ 0x08010638
	push {r4, lr}
	ldr r4, =musicPlayer_1
	ldr r0, [r4]
	bl MPlayStop_rev01
	ldr r0, [r4, #0xc]
	bl MPlayStop_rev01
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool


	.thumb_func @ nullsub_18
nullsub_18: @ 0x08010654
	bx lr
	.align 2, 0

thumb_func_global sub_08010658
sub_08010658: @ 0x08010658
	push {r4, lr}
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	ldr r1, =musicPlayer_1
	lsl r0, r4, #1
	add r0, r0, r4
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl MPlayStop_rev01
	ldr r0, =0x03000B08
	lsl r4, r4, #2
	add r4, r4, r0
	mov r0, #3
	strh r0, [r4, #2]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08010688
sub_08010688: @ 0x08010688
	push {r4, r5, r6, lr}
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	mov r2, #0
	ldr r0, =word_8038554
	ldrh r1, [r0]
	ldr r3, =0x0000FFFF
	add r6, r0, #0
	cmp r1, r3
	beq _080106E8
	ldr r1, =0x03000B08
	lsl r0, r5, #2
	add r4, r0, r1
_080106A2:
	lsl r0, r2, #1
	add r0, r0, r6
	ldrh r1, [r4]
	ldrh r0, [r0]
	cmp r1, r0
	bne _080106D8
	add r0, r1, #0
	bl song_play_and_auto_config
	mov r0, #1
	strh r0, [r4, #2]
	ldrh r0, [r4]
	bl sub_08010730
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0
	bne _08010702
	strh r0, [r4]
	strh r0, [r4, #2]
	b _08010702
	.align 2, 0
.pool
_080106D8:
	add r0, r2, #1
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	lsl r0, r2, #1
	add r0, r0, r6
	ldrh r0, [r0]
	cmp r0, r3
	bne _080106A2
_080106E8:
	ldr r1, =musicPlayer_1
	lsl r0, r5, #1
	add r0, r0, r5
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl m4aMPlayContinue
	ldr r1, =0x03000B08
	lsl r0, r5, #2
	add r0, r0, r1
	mov r1, #1
	strh r1, [r0, #2]
_08010702:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08010710
sub_08010710: @ 0x08010710
	lsl r0, r0, #0x10
	ldr r1, =0x03000B08
	lsr r0, r0, #0xe
	add r0, r0, r1
	ldrh r0, [r0, #2]
	bx lr
	.align 2, 0
.pool

thumb_func_global sub_08010720
sub_08010720: @ 0x08010720
	lsl r0, r0, #0x10
	ldr r1, =0x03000B08
	lsr r0, r0, #0xe
	add r0, r0, r1
	ldrh r0, [r0]
	bx lr
	.align 2, 0
.pool

thumb_func_global sub_08010730
sub_08010730: @ 0x08010730
	push {r4, lr}
	lsl r0, r0, #0x10
	ldr r2, =0x03000B08
	ldr r1, =m4a_songs
	lsr r0, r0, #0xd
	add r4, r0, r1
	ldrh r3, [r4, #4]
	lsl r0, r3, #2
	add r0, r0, r2
	ldrh r0, [r0, #2]
	cmp r0, #1
	bne _0801076C
	ldr r1, =musicPlayer_1
	lsl r0, r3, #1
	add r0, r0, r3
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, [r0]
	ldr r0, [r4]
	cmp r1, r0
	bne _0801076C
	mov r0, #1
	b _0801076E
	.align 2, 0
.pool
_0801076C:
	mov r0, #0
_0801076E:
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global sub_08010774
sub_08010774: @ 0x08010774
	push {r4, r5, r6, lr}
	mov r3, #0
	ldr r6, =0x03000B08
	ldr r5, =musicPlayer_1
	mov r4, #0
_0801077E:
	lsl r0, r3, #2
	add r2, r0, r6
	ldrh r0, [r2, #2]
	cmp r0, #1
	bne _080107A2
	lsl r0, r3, #1
	add r0, r0, r3
	lsl r0, r0, #2
	add r0, r0, r5
	ldr r0, [r0]
	ldr r1, [r0, #4]
	ldrh r0, [r0, #4]
	cmp r0, #0
	beq _0801079E
	cmp r1, #0
	bge _080107A2
_0801079E:
	strh r4, [r2]
	strh r4, [r2, #2]
_080107A2:
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, #4
	bls _0801077E
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global unused_sub_80107BC
unused_sub_80107BC: @ 0x080107BC
	push {lr}
	ldr r2, =0x03002080
	ldrb r3, [r2, #8]
	lsl r0, r3, #0x1c
	cmp r0, #0
	bge _0801082C
	add r0, r2, #0
	add r0, #0x2d
	ldrb r0, [r0]
	cmp r0, #2
	bls _0801082C
	cmp r0, #0xe
	beq _0801082C
	ldrh r1, [r2, #0xe]
	mov r0, #4
	and r0, r1
	cmp r0, #0
	beq _08010826
	ldrb r0, [r2, #9]
	lsl r0, r0, #0x1e
	cmp r0, #0
	blt _08010826
	ldr r2, =0x03000B1C
	ldr r1, [r2]
	mov r0, #1
	neg r0, r0
	cmp r1, r0
	beq _080107F8
	add r0, r1, #1
	str r0, [r2]
_080107F8:
	ldr r0, [r2]
	cmp r0, #0x3c
	bne _0801082C
	lsl r0, r3, #0x1a
	lsr r0, r0, #0x1f
	lsl r1, r3, #0x19
	lsr r1, r1, #0x1f
	add r0, r0, r1
	lsl r1, r3, #0x18
	lsr r1, r1, #0x1f
	add r0, r0, r1
	cmp r0, #3
	bne _08010820
	bl nullsub_3
	b _0801082C
	.align 2, 0
.pool
_08010820:
	bl nullsub_2
	b _0801082C
_08010826:
	ldr r1, =0x03000B1C
	mov r0, #0
	str r0, [r1]
_0801082C:
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global konamiLogoSwitch
konamiLogoSwitch: @ 0x08010834
	push {r4, r5, lr}
	sub sp, #4
	add r5, r0, #0
	ldrh r0, [r5]
	cmp r0, #4
	bls _08010842
	b _08010944
_08010842:
	lsl r0, r0, #2
	ldr r1, =_08010850
	add r0, r0, r1
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
.pool
_08010850: @ jump table
	.4byte _08010864 @ case 0
	.4byte _080108D4 @ case 1
	.4byte _080108E0 @ case 2
	.4byte _080108F8 @ case 3
	.4byte _0801091C @ case 4
_08010864:
	bl stopSoundTracks
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r0, =konamiLogoTiles
	mov r1, #0xc0
	lsl r1, r1, #0x13
	bl Bios_LZ77_16_2
	ldr r0, =konamiLogoMap
	ldr r1, =0x0600F800
	mov r2, #0
	str r2, [sp]
	mov r3, #0
	bl LZ77_mapCpy
	ldr r0, =konamiLogoPal
	mov r2, #0xa0
	lsl r2, r2, #0x13
	mov r1, #2
	bl memcpy_pal
	ldr r4, =0x03003CD4
	mov r1, #0x82
	lsl r1, r1, #5
	add r0, r1, #0
	strh r0, [r4]
	bl clearSprites
	bl resetAffineTransformations
	ldrh r0, [r4]
	mov r2, #0x80
	lsl r2, r2, #1
	add r1, r2, #0
	orr r0, r1
	strh r0, [r4]
	ldr r1, =0x03003D00
	mov r0, #0xf8
	lsl r0, r0, #5
	strh r0, [r1]
	b _08010910
	.align 2, 0
.pool
_080108D4:
	mov r0, #0x3f
	mov r1, #1
	mov r2, #9
	bl sub_0800F320
	b _08010904
_080108E0:
	ldr r1, =0x03002080
	ldrh r0, [r1, #0x3c]
	add r0, #1
	strh r0, [r1, #0x3c]
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #0x87
	ble _08010944
	b _08010910
	.align 2, 0
.pool
_080108F8:
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0xf
	mov r3, #0x10
	bl fadeScreen
_08010904:
	lsl r0, r0, #0x18
	cmp r0, #0
	beq _08010944
	ldr r1, =0x03002080
	mov r0, #0
	strh r0, [r1, #0x3c]
_08010910:
	ldrh r0, [r5]
	add r0, #1
	strh r0, [r5]
	b _08010944
	.align 2, 0
.pool
_0801091C:
	ldr r1, =0x03002080
	ldrh r0, [r1, #0x3c]
	add r0, #1
	strh r0, [r1, #0x3c]
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #5
	ble _08010944
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	mov r0, #1
	b _08010946
	.align 2, 0
.pool
_08010944:
	mov r0, #0
_08010946:
	add sp, #4
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0



thumb_func_global sub_8010950
sub_8010950: @ 0x08010950
	push {lr}
	bl ClearIWRAMRegions
	pop {r0}
	bx r0
	.align 2, 0
	


thumb_func_global sub_801095C
sub_801095C:
	push {lr}
	bl stopPalFade
	bl sub_80125BC
	bl sub_8016B9C
	pop {r0}
	bx r0
	.align 2, 0
	
	

thumb_func_global cb_startLevelLoad
cb_startLevelLoad: @ 0x08010970
	push {lr}
	ldr r0, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r1, r2, #0
	strh r1, [r0]
	bl gfx_related_sub_800F674
	bl sub_0801025C
	mov r0, #2
	bl endPriorityOrHigherTask
	ldr r0, =(cb_loadStage+1)
	bl setCurrentTaskFunc
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global cb_loadStage
cb_loadStage: @ 0x0801099C
	push {r4, lr}
	bl FreeAllPalFadeTasks
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	bl stopPalFade
	ldr r2, =0x03003D40
	ldrb r0, [r2, #0x12]
	cmp r0, #0
	beq _080109E4
	mov r0, #0
	strb r0, [r2]
	ldr r1, [r2, #0x14]
	ldrb r0, [r1]
	strb r0, [r2, #1]
	ldrb r0, [r1, #1]
	strb r0, [r2, #2]
	ldrb r0, [r1, #2]
	strb r0, [r2, #3]
	ldrb r0, [r1, #3]
	strb r0, [r2, #4]
	mov r0, #1
	strb r0, [r2, #0x11]
	mov r0, #0
	bl srand
	bl ClearIWRAMRegions
	b _080109FE
	.align 2, 0
.pool
_080109E4:
	ldrb r0, [r2]
	cmp r0, #2
	beq _080109F8
	ldr r0, =0x03002080
	ldr r0, [r0]
	bl srand
	b _080109FE
	.align 2, 0
.pool
_080109F8:
	mov r0, #0
	bl srand
_080109FE:
	ldr r1, =0x03003D40
	mov r0, #0
	strb r0, [r1, #0x10]
	ldrb r0, [r1, #0x12]
	cmp r0, #0
	beq _08010A18
	ldr r0, [r1, #0x14]
	ldrb r0, [r0, #0xc]
	bl setPowerupLevel
	b _08010A4E
	.align 2, 0
.pool
_08010A18:
	ldrb r0, [r1, #3]
	cmp r0, #0
	bne _08010A4E
	ldrb r0, [r1, #4]
	cmp r0, #0
	bne _08010A4E
	ldrb r0, [r1, #2]
	cmp r0, #0
	beq _08010A44
	ldrb r0, [r1]
	cmp r0, #2
	beq _08010A44
	ldr r0, =0x03003C00
	ldrb r0, [r0, #4]
	lsl r0, r0, #0x1a
	lsr r0, r0, #0x1e
	bl setPowerupLevel
	b _08010A4A
	.align 2, 0
.pool
_08010A44:
	mov r0, #0
	bl setPowerupLevel
_08010A4A:
	bl initSound
_08010A4E:
	bl sub_08016AC0
	bl player_init_maybe
	ldr r4, =0x03003D40
	ldrb r0, [r4, #1]
	ldrb r1, [r4, #2]
	ldrb r2, [r4, #3]
	ldrb r3, [r4, #4]
	bl setCameraStruct
	ldrb r0, [r4, #1]
	ldrb r1, [r4, #2]
	ldrb r2, [r4, #3]
	bl loadMap
	bl loadHUD
	ldrb r0, [r4, #3]
	cmp r0, #0
	bne _08010A82
	ldrb r0, [r4, #4]
	cmp r0, #0
	bne _08010A82
	bl ClearIWRAMRegions
_08010A82:
	ldr r4, =0x03003D40
	ldrb r0, [r4, #1]
	ldrb r1, [r4, #2]
	ldrb r2, [r4, #3]
	bl loadMapObjects
	ldr r1, =0x03004720
	ldrh r0, [r1]
	ldrh r1, [r1, #2]
	bl handleParallaxScroll
	bl sub_08032D1C
	bl sub_0803310C
	bl sub_08033164
	bl sub_080124DC
	bl sub_08032128
	bl sub_08032DDC
	bl sub_080331C0
	ldrb r0, [r4, #1]
	cmp r0, #1
	bne _08010AF8
	ldrb r1, [r4, #2]
	ldr r0, =stageCount
	ldrh r0, [r0, #2]
	sub r0, #1
	cmp r1, r0
	bne _08010AF8
	ldr r1, =0x03003D74
	ldr r2, =0x00003F44
	add r0, r2, #0
	strh r0, [r1]
	ldr r1, =0x03003D94
	ldr r2, =0x00000808
	add r0, r2, #0
	strh r0, [r1]
	add r3, r4, #0
	b _08010B56
	.align 2, 0
.pool
_08010AF8:
	ldr r0, =0x03003D40
	ldrb r1, [r0, #1]
	add r3, r0, #0
	cmp r1, #3
	bne _08010B24
	ldr r1, =0x03003D74
	ldr r2, =0x00003F42
	add r0, r2, #0
	strh r0, [r1]
	ldr r1, =0x03003D94
	ldr r2, =0x00000E0C
	b _08010B52
	.align 2, 0
.pool
_08010B24:
	cmp r1, #5
	bne _08010B44
	ldr r1, =0x03003D74
	ldr r2, =0x00003F41
	add r0, r2, #0
	strh r0, [r1]
	ldr r1, =0x03003D94
	mov r2, #0x80
	lsl r2, r2, #5
	b _08010B52
	.align 2, 0
.pool
_08010B44:
	ldr r1, =0x03003D74
	mov r2, #0xfd
	lsl r2, r2, #6
	add r0, r2, #0
	strh r0, [r1]
	ldr r1, =0x03003D94
	ldr r2, =0x0000080C
_08010B52:
	add r0, r2, #0
	strh r0, [r1]
_08010B56:
	ldrb r0, [r3, #0x12]
	cmp r0, #0
	bne _08010BA0
	ldr r2, =dword_8039E1C
	ldrb r1, [r3, #2]
	lsl r1, r1, #1
	ldrb r0, [r3, #1]
	lsl r0, r0, #3
	add r1, r1, r0
	add r1, r1, r2
	ldrh r0, [r1]
	cmp r0, #0
	beq _08010B88
	bl playSong
	b _08010B8E
	.align 2, 0
.pool
_08010B88:
	mov r0, #0
	bl stopTrack
_08010B8E:
	ldr r4, =0x03003D40
	ldrb r0, [r4, #0x12]
	cmp r0, #0
	bne _08010BA0
	bl createLevelTask
	ldrb r0, [r4, #0x12]
	cmp r0, #0
	beq _08010BB0
_08010BA0:
	ldr r0, =(intro_button_related+1)
	bl setCurrentTaskFunc
	b _08010BB6
	.align 2, 0
.pool
_08010BB0:
	ldr r0, =(sub_8010C2C+1)
	bl setCurrentTaskFunc
_08010BB6:
	bl task_unknown
	mov r0, #3
	bl sub_8016A4C
	ldr r2, =0x03003D40
	add r2, #0x20
	ldrb r1, [r2]
	mov r0, #2
	neg r0, r0
	and r0, r1
	mov r1, #3
	neg r1, r1
	and r0, r1
	strb r0, [r2]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool


thumb_func_global sub_8010BE4
sub_8010BE4:
	push {lr}
	ldr r0, =0x03004750
	ldr r0, [r0]
	ldrh r2, [r0, #0x14]
	ldrh r3, [r0, #0x16]
	sub r3, #0x10
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	mov r0, #2
	mov r1, #0x14
	bl TransitionToBlack
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	pop {r1}
	bx r1
	.align 2, 0
.pool
	
	
	thumb_func_global sub_8010C08
sub_8010C08: @ 0x08010C08
	push {lr}
	ldr r0, =0x03004750
	ldr r0, [r0]
	ldrh r2, [r0, #0x14]
	ldrh r3, [r0, #0x16]
	sub r3, #0x10
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	mov r0, #2
	mov r1, #0x14
	bl undoScreenTransition
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	pop {r1}
	bx r1
	.align 2, 0
.pool
	
	
	thumb_func_global sub_8010C2C
sub_8010C2C: @ 0x08010C2C
	push {lr}
	ldr r1, =0x03003D40
	ldrb r0, [r1, #3]
	cmp r0, #0
	bne _08010C3C
	ldrb r0, [r1, #4]
	cmp r0, #0
	beq _08010C42
_08010C3C:
	ldrb r0, [r1, #0x12]
	cmp r0, #0
	beq _08010C90
_08010C42:
	mov r0, #3
	mov r1, #0x28
	mov r2, #0x78
	mov r3, #0x50
	bl undoScreenTransition
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08010CA4
	bl sub_080102B0
	ldr r0, =0x03004720
	ldrh r0, [r0, #8]
	cmp r0, #6
	bne _08010C78
	ldr r0, =(task_inGame+1)
	bl setCurrentTaskFunc
	bl sub_0801C9E8
	b _08010CA4
	.align 2, 0
.pool
_08010C78:
	ldr r0, =0x03002080
	add r0, #0x2d
	mov r1, #0
	strb r1, [r0]
	ldr r0, =(sub_8010E10+1)
	bl setCurrentTaskFunc
	b _08010CA4
	.align 2, 0
.pool
_08010C90:
	bl sub_8010C08
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08010CA4
	bl sub_08032DB4
	ldr r0, =(task_inGame+1)
	bl setCurrentTaskFunc
_08010CA4:
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	cmp r0, #4
	bne _08010CBC
	ldrb r1, [r1, #2]
	ldr r0, =stageCount
	ldrh r0, [r0, #8]
	sub r0, #1
	cmp r1, r0
	bne _08010CBC
	bl sub_8015590
_08010CBC:
	mov r0, #3
	bl sub_8016A4C
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global intro_button_related
intro_button_related: @ 0x08010CD4
	push {r4, r5, lr}
	ldr r4, =0x03002080
	ldrh r1, [r4, #0xc]
	mov r0, #8
	and r0, r1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, #0
	beq _08010CF8
	ldr r0, =(sub_8010F88+1)
	bl setCurrentTaskFunc
	b _08010D46
	.align 2, 0
.pool
_08010CF8:
	mov r0, #3
	mov r1, #0x14
	mov r2, #0x78
	mov r3, #0x50
	bl undoScreenTransition
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08010D22
	bl sub_080102B0
	bl sub_08032DB4
	bl sub_0801C9E8
	ldr r0, =(task_inGame+1)
	bl setCurrentTaskFunc
	mov r0, #0xd0
	bl stopSong_maybe
_08010D22:
	ldr r2, =0x03003D40
	ldr r3, [r2, #0x14]
	ldrh r0, [r2, #0x18]
	ldr r1, [r3, #8]
	lsl r0, r0, #2
	add r0, r0, r1
	ldrh r0, [r0]
	strh r0, [r4, #0xc]
	ldrh r0, [r2, #0x18]
	ldr r1, [r3, #8]
	lsl r0, r0, #2
	add r0, r0, r1
	ldrh r0, [r0, #2]
	strh r0, [r4, #0xe]
	strh r5, [r4, #0x14]
	ldrh r0, [r2, #0x18]
	add r0, #1
	strh r0, [r2, #0x18]
_08010D46:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global sub_8010D54
sub_8010D54: @ 0x08010D54
	push {lr}
	bl task_unknown
	mov r0, #3
	bl sub_8016A4C
	bl sub_8015590
	pop {r0}
	bx r0



thumb_func_global sub_8010D68
sub_8010D68: @ 0x08010D68
	push {lr}
	ldr r0, =0x03003D40
	ldrb r0, [r0, #0x12]
	cmp r0, #0
	bne _08010D94
	bl sub_08013064
	ldr r0, =0x03003E20
	ldr r0, [r0]
	ldr r1, =(sub_8010D54+1)
	bl setTaskFunc
	mov r0, #3
	bl sub_8016A4C
	b _08010DA8
	.align 2, 0
.pool
_08010D94:
	bl sub_08032DB4
	ldr r0, =0x03003E20
	ldr r0, [r0]
	ldr r1, =(task_inGame+1)
	bl setTaskFunc
	mov r0, #3
	bl sub_8016A4C
_08010DA8:
	bl sub_8015590
	pop {r0}
	bx r0
	.align 2, 0
.pool




thumb_func_global sub_8010DB8
sub_8010DB8: @ 0x08010DB8
	push {lr}
	ldr r0, =0x03003D40
	ldrb r1, [r0, #0x12]
	cmp r1, #0
	bne _08010DEC
	ldr r0, =0x03002080
	add r0, #0x2d
	strb r1, [r0]
	ldr r0, =0x03003E20
	ldr r0, [r0]
	ldr r1, =(sub_8010E10+1)
	bl setTaskFunc
	mov r0, #3
	bl sub_8016A4C
	b _08010E00
	.align 2, 0
.pool
_08010DEC:
	bl sub_08032DB4
	ldr r0, =0x03003E20
	ldr r0, [r0]
	ldr r1, =(task_inGame+1)
	bl setTaskFunc
	mov r0, #3
	bl sub_8016A4C
_08010E00:
	bl sub_8015590
	pop {r0}
	bx r0
	.align 2, 0
.pool
	.thumb_func @ sub_8010E10
sub_8010E10: @ 0x08010E10
	push {r4, lr}
	ldr r4, =0x030020AD
	add r1, r4, #0
	add r1, #0xd
	add r0, r4, #0
	bl sub_8033DE8
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08010E36
	bl sub_08032DB4
	bl sub_8033198
	mov r0, #0
	strb r0, [r4]
	ldr r0, =(task_inGame+1)
	bl setCurrentTaskFunc
_08010E36:
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	cmp r0, #4
	bne _08010E4E
	ldrb r1, [r1, #2]
	ldr r0, =stageCount
	ldrh r0, [r0, #8]
	sub r0, #1
	cmp r1, r0
	bne _08010E4E
	bl sub_8015590
_08010E4E:
	mov r0, #3
	bl sub_8016A4C
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global task_inGame
task_inGame: @ 0x08010E6C
	push {r4, r5, lr}
	ldr r2, =0x03003D40
	ldrb r0, [r2, #0x12]
	cmp r0, #0
	beq _08010EF0
	ldr r0, =0x03002080
	ldrh r1, [r0, #0xc]
	mov r0, #8
	and r0, r1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	cmp r1, #0
	beq _08010E9C
	ldr r0, =(sub_8010F88+1)
	bl setCurrentTaskFunc
	b _08010EB4
	.align 2, 0
.pool
_08010E9C:
	ldr r0, [r2, #0x14]
	ldrh r0, [r0, #4]
	ldrh r2, [r2, #0x18]
	cmp r0, r2
	bne _08010EB4
	ldr r0, =0x03000B20
	strh r1, [r0]
	ldr r0, =(sub_8010F0C+1)
	bl setCurrentTaskFunc
	bl sub_801CAC4
_08010EB4:
	ldr r3, =0x03002080
	ldr r2, =0x03003D40
	ldr r4, [r2, #0x14]
	ldrh r0, [r2, #0x18]
	ldr r1, [r4, #8]
	lsl r0, r0, #2
	add r0, r0, r1
	ldrh r0, [r0]
	mov r5, #0
	strh r0, [r3, #0xc]
	ldrh r0, [r2, #0x18]
	ldr r1, [r4, #8]
	lsl r0, r0, #2
	add r0, r0, r1
	ldrh r0, [r0, #2]
	strh r0, [r3, #0xe]
	strh r5, [r3, #0x14]
	ldrh r0, [r2, #0x18]
	add r0, #1
	strh r0, [r2, #0x18]
	b _08010F00
	.align 2, 0
.pool
_08010EF0:
	ldr r0, =0x03002080
	ldrh r1, [r0, #0xc]
	mov r0, #8
	and r0, r1
	cmp r0, #0
	beq _08010F00
	bl openPauseMenu
_08010F00:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
	
	
	thumb_func_global sub_8010F0C
sub_8010F0C: @ 0x08010F0C
	push {r4, r5, lr}
	ldr r1, =0x03000B20
	ldrh r0, [r1]
	add r0, #1
	strh r0, [r1]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0xa
	bne _08010F30
	bl getIntroSceneNum
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #3
	bhi _08010F30
	mov r0, #0xc8
	bl song_play_and_auto_config
_08010F30:
	mov r0, #3
	mov r1, #0x14
	mov r2, #0x78
	mov r3, #0x50
	bl TransitionToBlack
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08010F46
	bl sub_8026FA4
_08010F46:
	ldr r3, =0x03002080
	ldr r2, =0x03003D40
	ldr r4, [r2, #0x14]
	ldrh r0, [r2, #0x18]
	ldr r1, [r4, #8]
	lsl r0, r0, #2
	add r0, r0, r1
	ldrh r0, [r0]
	mov r5, #0
	strh r0, [r3, #0xc]
	ldrh r0, [r2, #0x18]
	ldr r1, [r4, #8]
	lsl r0, r0, #2
	add r0, r0, r1
	ldrh r0, [r0, #2]
	strh r0, [r3, #0xe]
	strh r5, [r3, #0x14]
	ldrh r0, [r2, #0x18]
	add r0, #1
	strh r0, [r2, #0x18]
	mov r0, #3
	bl sub_8016A4C
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool


	thumb_func_global sub_8010F88
sub_8010F88: @ 0x08010F88
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	bl sub_801CB9C
	mov r0, #2
	bl endPriorityOrHigherTask
	ldr r0, =0x03003E20
	ldr r0, [r0]
	ldr r1, =(cb_loadTitleScreen+1)
	bl setTaskFunc
	ldr r1, =0x03003D40
	mov r0, #0
	strb r0, [r1, #0x12]
	pop {r0}
	bx r0
	.align 2, 0
.pool
