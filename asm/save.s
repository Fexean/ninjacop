.include "asm/macros.inc"

@completely disassembled


thumb_func_global initSaveData
initSaveData: @ 0x08011824
	push {r4, r5, r6, lr}
	sub sp, #4
	mov r0, #4
	bl setSaveType
	ldr r0, =0x03003C00
	bl loadSaveGame
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0
	beq _080118A2
	bl sub_08028D50
	cmp r4, #1
	bne _080118A2
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r1, =0x03000C30
	ldr r2, =0x0100007A
	mov r0, sp
	bl Bios_memcpy
	mov r4, #0
	ldr r2, =0x03000D28
	ldr r3, =savChecksum
	add r5, r2, #0
_0801185C:
	add r1, r4, r2
	add r0, r4, r3
	ldrb r0, [r0]
	strb r0, [r1]
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0xb
	bls _0801185C
	mov r4, #0
	str r4, [r5, #0xc]
	bl muteSound
	ldr r1, =0x030020E8
	mov r0, #2
	bl setEepromTimer
	ldr r1, =0x04000200
	ldrh r0, [r1]
	add r6, r0, #0
	strh r4, [r1]
_08011886:
	add r0, r4, #0
	add r1, r5, #0
	bl writeEepromVerified
	add r5, #8
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #1
	bls _08011886
	ldr r0, =0x04000200
	strh r6, [r0]
	bl sub_08036BE0
_080118A2:
	add sp, #4
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global saveGame
saveGame: @ 0x080118C8
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	mov r7, #0
	bl muteSound
	mov r4, #0
	ldr r2, =0x03000D28
	ldr r3, =savChecksum
	add r5, r2, #0
_080118DC:
	add r1, r4, r2
	add r0, r4, r3
	ldrb r0, [r0]
	strb r0, [r1]
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0xb
	bls _080118DC
	ldr r1, =0x03000C30
	ldrb r0, [r1, #1]
	str r0, [r5, #0xc]
	add r6, r1, #4
	mov r4, #0
	ldr r3, =0x03003C00
	add r5, r1, #0
	add r5, #0x14
	ldr r2, =aGa0me1S0id5
_08011900:
	add r1, r6, r4
	add r0, r4, r2
	ldrb r0, [r0]
	strb r0, [r1]
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0xb
	bls _08011900
	add r0, r3, #0
	add r1, r5, #0
	mov r2, #0x6a
	bl Bios_memcpy
	add r5, r6, #0
	add r5, #0x10
	mov r0, #0
	str r0, [r6, #0xc]
	mov r4, #0
_08011926:
	ldrb r0, [r5]
	ldr r1, [r6, #0xc]
	add r1, r1, r0
	str r1, [r6, #0xc]
	add r5, #1
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0xdf
	bls _08011926
	ldr r1, =0x030020E8
	mov r0, #2
	bl setEepromTimer
	ldr r1, =0x04000200
	ldrh r0, [r1]
	mov r8, r0
	mov r0, #0
	strh r0, [r1]
	ldr r5, =0x03000D28
	mov r4, #0
_08011950:
	add r0, r4, #0
	add r1, r5, #0
	bl writeEepromVerified
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _080119AE
	add r5, #8
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #1
	bls _08011950
	add r5, r6, #0
	ldr r0, =0x03000D28
	ldr r1, [r0, #0xc]
	lsl r0, r1, #4
	sub r0, r0, r1
	lsl r0, r0, #0x11
	mov r1, #0x80
	lsl r1, r1, #0xa
	add r0, r0, r1
	lsr r4, r0, #0x10
	add r6, r4, #0
	add r0, r4, #0
	b _080119BC
	.align 2, 0
.pool
_080119A0:
	add r0, r4, #0
	add r1, r5, #0
	bl writeEepromVerified
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _080119B2
_080119AE:
	mov r7, #1
	b _080119E2
_080119B2:
	add r5, #8
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	add r0, r6, #0
_080119BC:
	add r0, #0x1e
	cmp r4, r0
	blt _080119A0
	ldr r0, =0x04000200
	mov r1, r8
	strh r1, [r0]
	ldr r2, =0x03000C30
	ldrb r0, [r2, #1]
	add r0, #1
	strb r0, [r2, #1]
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #1
	bls _080119DC
	mov r0, #0
	strb r0, [r2, #1]
_080119DC:
	ldr r1, =0x03000D28
	ldrb r0, [r2, #1]
	str r0, [r1, #0xc]
_080119E2:
	bl sub_08036BE0
	add r0, r7, #0
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool



thumb_func_global loadSaveGame
loadSaveGame: @ 0x08011A00
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	sub sp, #4
	mov sb, r0
	bl muteSound
	ldr r5, =0x03000D28
	mov r7, #0
_08011A14:
	add r0, r7, #0
	add r1, r5, #0
	bl read64bitEeprom
	add r5, #8
	add r0, r7, #1
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	cmp r7, #1
	bls _08011A14
	mov r7, #0
	ldr r2, =0x03000D28
	add r4, r2, #0
	ldr r3, =savChecksum
_08011A30:
	add r0, r7, r4
	add r1, r7, r3
	ldrb r0, [r0]
	ldrb r1, [r1]
	cmp r0, r1
	bne _08011A4C
	add r0, r7, #1
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	cmp r7, #0xb
	bls _08011A30
	ldr r0, [r2, #0xc]
	cmp r0, #1
	bls _08011A58
_08011A4C:
	mov r4, #1
	b _08011B30
	.align 2, 0
.pool
_08011A58:
	mov r7, #0
	mov r8, r2
_08011A5C:
	sub r1, r7, #2
	mov r2, r8
	ldr r0, [r2, #0xc]
	sub r0, r0, r1
	mov r1, #2
	bl Bios_modulo
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	ldr r5, =0x03000C34
	lsl r0, r3, #4
	sub r0, r0, r3
	lsl r0, r0, #0x11
	mov r1, #0x80
	lsl r1, r1, #0xa
	add r0, r0, r1
	lsr r4, r0, #0x10
	add r0, r4, #0
	add r0, #0x1e
	cmp r4, r0
	bge _08011AA0
	add r6, r0, #0
_08011A88:
	add r0, r4, #0
	add r1, r5, #0
	str r3, [sp]
	bl read64bitEeprom
	add r5, #8
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	ldr r3, [sp]
	cmp r4, r6
	blt _08011A88
_08011AA0:
	mov r4, #0
	ldr r0, =0x03000C30
	ldr r2, =aGa0me1S0id5
	ldrb r1, [r0, #4]
	add r6, r0, #0
	ldrb r0, [r2]
	cmp r1, r0
	bne _08011AC8
	add r5, r6, #4
_08011AB2:
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0xb
	bhi _08011ACC
	add r0, r4, r5
	add r1, r4, r2
	ldrb r0, [r0]
	ldrb r1, [r1]
	cmp r0, r1
	beq _08011AB2
_08011AC8:
	cmp r4, #0xb
	bls _08011B24
_08011ACC:
	mov r1, #0
	ldr r5, =0x03000C44
	mov r4, #0
_08011AD2:
	ldrb r0, [r5]
	add r1, r1, r0
	add r5, #1
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0xdf
	bls _08011AD2
	add r2, r6, #0
	ldr r0, [r2, #0x10]
	cmp r1, r0
	bne _08011B24
	add r0, r3, #1
	mov r1, #0
	strb r0, [r2, #1]
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #1
	bls _08011AFA
	strb r1, [r2, #1]
_08011AFA:
	ldrb r0, [r6, #1]
	mov r1, r8
	str r0, [r1, #0xc]
	add r0, r6, #0
	add r0, #0x14
	mov r1, sb
	mov r2, #0x6a
	bl Bios_memcpy
	mov r0, #1
	strb r0, [r6]
	mov r4, #0
	b _08011B30
	.align 2, 0
.pool
_08011B24:
	add r0, r7, #1
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	cmp r7, #1
	bls _08011A5C
	mov r4, #2
_08011B30:
	bl sub_08036BE0
	add r0, r4, #0
	add sp, #4
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
