	.include "asm/macros.inc"
	
	@completely disassembled!
	
	
	thumb_func_global loadTimeTrialLevelCompletionScreen
loadTimeTrialLevelCompletionScreen: @ 0x080290D8
	push {r4, r5, r6, r7, lr}
	sub sp, #0x14
	ldr r4, =0x03003CD4
	mov r1, #0x82
	lsl r1, r1, #5
	add r0, r1, #0
	strh r0, [r4]
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	bl FreeAllPalFadeTasks
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r0, =dword_81C5288
	ldr r6, =0x05000020
	mov r1, #1
	add r2, r6, #0
	bl memcpy_pal
	ldr r0, =unk_81C5274
	ldr r1, =0x06000840
	bl Bios_LZ77_16_2
	ldr r0, =byte_81C5134
	ldr r1, =0x0600F000
	mov r2, #1
	str r2, [sp]
	mov r2, #0
	mov r3, #0x42
	bl LZ77_mapCpy
	ldr r5, =0x03003D00
	mov r7, #0
	ldr r0, =0x00001E02
	strh r0, [r5, #4]
	ldrh r0, [r4]
	mov r2, #0x80
	lsl r2, r2, #3
	add r1, r2, #0
	orr r0, r1
	strh r0, [r4]
	ldr r0, =off_81C54A4
	add r1, r6, #0
	mov r2, #0
	bl startPalAnimation
	ldr r0, =menuBgPal
	ldr r2, =0x05000040
	mov r1, #1
	bl memcpy_pal
	ldr r0, =menuBgTilesCompressed
	ldr r1, =0x06000880
	bl Bios_LZ77_16_2
	ldr r0, =menuBgMapCompressed
	ldr r1, =0x0600F800
	mov r2, #2
	str r2, [sp]
	mov r2, #0
	mov r3, #0x44
	bl LZ77_mapCpy
	ldr r0, =0x00001F03
	strh r0, [r5, #6]
	ldrh r0, [r4]
	mov r2, #0x80
	lsl r2, r2, #4
	add r1, r2, #0
	orr r0, r1
	strh r0, [r4]
	ldr r0, =HUDPalette
	mov r2, #0xa0
	lsl r2, r2, #0x13
	mov r1, #1
	bl memcpy_pal
	ldr r0, =HUDTiles
	mov r1, #0xc0
	lsl r1, r1, #0x13
	bl Bios_LZ77_16_2
	add r0, sp, #0x10
	strh r7, [r0]
	ldr r1, =0x0600E800
	ldr r2, =0x01000400
	bl Bios_memcpy
	bl sub_8029AB8
	ldr r0, =0x00001D01
	strh r0, [r5, #2]
	ldr r1, =0x030022F8
	mov r0, #4
	strh r0, [r1, #2]
	ldr r0, =0x03000588
	mov r1, #0
	bl allocFont
	ldr r0, =0x03000590
	mov r1, #1
	bl allocFont
	bl sub_80296D0
	mov r0, #0
	mov r1, #8
	bl sub_8029700
	bl sub_8029760
	neg r0, r0
	mov r1, #8
	bl sub_8029700
	mov r0, #0xb
	bl allocateSpriteTiles
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	ldr r0, =score_numberTiles
	lsl r1, r4, #5
	ldr r2, =0x06010000
	add r1, r1, r2
	bl Bios_LZ77_16_2
	ldr r0, =dword_8208DA0
	mov r1, #1
	bl LoadSpritePalette
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	cmp r0, #4
	bhi _08029224
	ldrb r3, [r1, #2]
	add r3, r4, r3
	add r3, #1
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	str r2, [sp]
	mov r0, #0x80
	lsl r0, r0, #1
	str r0, [sp, #4]
	ldr r0, =byte_8209AC8
	ldrb r0, [r0]
	str r0, [sp, #8]
	str r7, [sp, #0xc]
	mov r0, #0x24
	mov r1, #0
	mov r2, #0x3c
	bl createSimpleSprite
	ldr r0, =0x030037D0
	mov r1, #0x93
	lsl r1, r1, #2
	add r0, r0, r1
	mov r1, #0
	strb r1, [r0]
_08029224:
	ldr r0, =0x03000580
	strh r7, [r0]
	ldr r0, =(cb_TimeTrialLevelCompleteScreen+1)
	bl setCurrentTaskFunc
	add sp, #0x14
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global cb_TimeTrialLevelCompleteScreen
cb_TimeTrialLevelCompleteScreen: @ 0x080292BC
	push {r4, r5, r6, r7, lr}
	ldr r1, =TimeTrialLevelCompleteFuncs
	ldr r7, =0x03000580
	ldrh r0, [r7]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl call_r0
	lsl r0, r0, #0x10
	asr r1, r0, #0x10
	cmp r1, #1
	beq _080292F6
	cmp r1, #1
	bgt _080292EC
	mov r0, #1
	neg r0, r0
	cmp r1, r0
	beq _08029342
	b _08029362
	.align 2, 0
.pool
_080292EC:
	cmp r1, #2
	beq _080292FE
	cmp r1, #3
	beq _0802933C
	b _08029362
_080292F6:
	ldrh r0, [r7]
	add r0, #1
	strh r0, [r7]
	b _08029362
_080292FE:
	ldr r5, =0x03000584
	ldrh r0, [r5]
	bl sub_8029784
	ldrh r2, [r5]
	mov r0, #0xf0
	mov r1, #0x70
	bl sub_80297D4
	ldr r6, =0x03000586
	ldrh r0, [r5]
	bl sub_8029870
	add r4, r0, #0
	ldrh r0, [r5]
	bl sub_8029830
	sub r4, r4, r0
	mov r0, #0xf0
	sub r0, r0, r4
	mov r1, #2
	bl Bios_Div
	strh r0, [r6]
	mov r0, #9
	strh r0, [r7]
	b _08029362
	.align 2, 0
.pool
_0802933C:
	mov r0, #6
	strh r0, [r7]
	b _08029362
_08029342:
	ldr r0, =0x03003D40
	ldrb r0, [r0, #0x10]
	cmp r0, #0
	beq _0802935C
	ldr r0, =(cb_saving+1)
	bl setCurrentTaskFunc
	b _08029362
	.align 2, 0
.pool
_0802935C:
	ldr r0, =(cb_loadTimeTrialMenu+1)
	bl setCurrentTaskFunc
_08029362:
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool


	thumb_func_global TimeTrialLevelCompleteFunc0
TimeTrialLevelCompleteFunc0: @ 0x0802936C
	push {lr}
	bl TimeTrialLevelCompleteFunc1
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _08029386
	mov r0, #0
	b _08029388
_08029386:
	mov r0, #1
_08029388:
	pop {r1}
	bx r1
	
	
	thumb_func_global TimeTrialLevelCompleteFunc1
TimeTrialLevelCompleteFunc1: @ 0x0802938C
	push {lr}
	bl sub_802973C
	add r1, r0, #0
	sub r0, r1, #1
	cmp r0, #0xe6
	bls _080293A6
	add r0, #9
	mov r1, #8
	bl sub_8029700
	mov r0, #0
	b _080293B6
_080293A6:
	mov r0, #8
	mov r1, #8
	bl sub_8029700
	ldr r1, =0x03000582
	mov r0, #0
	strh r0, [r1]
	mov r0, #1
_080293B6:
	pop {r1}
	bx r1
	.align 2, 0
.pool


	thumb_func_global TimeTrialLevelCompleteFunc2
TimeTrialLevelCompleteFunc2: @ 0x080293C0
	push {r4, lr}
	ldr r4, =0x03000582
	ldrh r0, [r4]
	add r0, #1
	strh r0, [r4]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0xf
	bhi _080293DC
	mov r0, #0
	b _0802940E
	.align 2, 0
.pool
_080293DC:
	bl sub_8029930
	mov r0, #0
	mov r1, #0x2c
	bl sub_8029970
	bl sub_80299B8
	add r1, r0, #0
	mov r0, #0xf0
	sub r0, r0, r1
	mov r1, #2
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x2c
	bl sub_8029970
	mov r0, #0xcb
	bl playSong
	mov r0, #0
	strh r0, [r4]
	mov r0, #1
_0802940E:
	pop {r4}
	pop {r1}
	bx r1
	
	
	thumb_func_global TimeTrialLevelCompleteFunc3
TimeTrialLevelCompleteFunc3: @ 0x08029414
	push {r4, lr}
	ldr r1, =0x03000582
	ldrh r0, [r1]
	add r0, #1
	strh r0, [r1]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0xf
	bls _080294CC
	bl sub_80299E8
	ldr r0, =0x03003D40
	ldrb r0, [r0, #1]
	cmp r0, #4
	bhi _08029494
	mov r0, #0
	mov r1, #0x3c
	bl sub_8029A30
	bl sub_8029A80
	add r1, r0, #0
	mov r0, #0xe6
	sub r0, r0, r1
	mov r1, #2
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x3c
	bl sub_8029A30
	bl sub_8029A80
	ldr r3, =0x030037D0
	add r0, #2
	ldr r1, =0x00000242
	add r4, r3, r1
	ldr r2, =0x000001FF
	add r1, r2, #0
	and r0, r1
	ldrh r2, [r4]
	ldr r1, =0xFFFFFE00
	and r1, r2
	orr r1, r0
	strh r1, [r4]
	mov r0, #0x93
	lsl r0, r0, #2
	add r3, r3, r0
	mov r0, #1
	strb r0, [r3]
	b _080294B6
	.align 2, 0
.pool
_08029494:
	mov r0, #0
	mov r1, #0x3c
	bl sub_8029A30
	bl sub_8029A80
	mov r1, #0xf0
	sub r1, r1, r0
	add r0, r1, #0
	mov r1, #2
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x3c
	bl sub_8029A30
_080294B6:
	mov r0, #0xcb
	bl playSong
	ldr r1, =0x03000582
	mov r0, #0
	strh r0, [r1]
	mov r0, #1
	b _080294CE
	.align 2, 0
.pool
_080294CC:
	mov r0, #0
_080294CE:
	pop {r4}
	pop {r1}
	bx r1
	
	
	thumb_func_global TimeTrialLevelCompleteFunc4
TimeTrialLevelCompleteFunc4: @ 0x080294D4
	push {r4, lr}
	ldr r4, =0x03000582
	ldrh r0, [r4]
	add r0, #1
	strh r0, [r4]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0xf
	bhi _080294F0
	mov r0, #0
	b _0802950A
	.align 2, 0
.pool
_080294F0:
	ldr r2, =0x03003CD4
	ldrh r0, [r2]
	mov r3, #0x80
	lsl r3, r3, #2
	add r1, r3, #0
	orr r0, r1
	strh r0, [r2]
	mov r0, #0xcb
	bl playSong
	mov r0, #0
	strh r0, [r4]
	mov r0, #1
_0802950A:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool


	thumb_func_global TimeTrialLevelCompleteFunc5
TimeTrialLevelCompleteFunc5: @ 0x08029514
	push {r4, r5, r6, lr}
	ldr r5, =0x03003D40
	ldrb r0, [r5, #0x10]
	cmp r0, #0
	beq _08029584
	bl getDword3001F7C
	add r6, r0, #0
	ldrb r3, [r5, #1]
	lsl r4, r3, #5
	ldrb r0, [r5, #2]
	lsl r1, r0, #3
	ldr r2, =0x03003C0C
	add r1, r1, r2
	add r4, r4, r1
	ldr r1, =dword_82065EC
	lsl r0, r0, #2
	lsl r3, r3, #4
	add r0, r0, r3
	add r0, r0, r1
	ldr r1, [r0]
	ldr r0, [r4, #4]
	cmp r0, r1
	blo _08029560
	cmp r6, r1
	bhs _08029560
	ldr r1, =0x03000584
	mov r0, #1
	b _08029588
	.align 2, 0
.pool
_08029560:
	ldr r0, [r4, #4]
	cmp r6, r0
	bhs _08029570
	ldr r1, =0x03000584
	mov r0, #0
	b _08029588
	.align 2, 0
.pool
_08029570:
	ldr r1, =0x03000584
	mov r2, #1
	neg r2, r2
	add r0, r2, #0
	strh r0, [r1]
	mov r0, #1
	b _0802958C
	.align 2, 0
.pool
_08029584:
	ldr r1, =0x03000584
	mov r0, #2
_08029588:
	strh r0, [r1]
	mov r0, #2
_0802958C:
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool


	thumb_func_global TimeTrialLevelCompleteFunc6
TimeTrialLevelCompleteFunc6: @ 0x08029598
	ldr r0, =0x03002080
	ldrh r1, [r0, #0xc]
	mov r0, #0xb
	and r0, r1
	cmp r0, #0
	bne _080295AC
	mov r0, #0
	b locret_80295AE
	.align 2, 0
.pool
_080295AC:
	mov r0, #1
locret_80295AE:
	bx lr
	
	
	thumb_func_global TimeTrialLevelCompleteFunc7
TimeTrialLevelCompleteFunc7: @ 0x080295B0
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _080295C8
	mov r0, #0
	b _080295CA
_080295C8:
	mov r0, #1
_080295CA:
	pop {r1}
	bx r1
	.byte 0x00, 0x00
	
	
	thumb_func_global TimeTrialLevelCompleteFunc8
TimeTrialLevelCompleteFunc8: @ 0x080295D0
	push {lr}
	bl stopSoundTracks
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	bl FreeAllPalFadeTasks
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	mov r0, #1
	neg r0, r0
	pop {r1}
	bx r1
	.align 2, 0
.pool



	thumb_func_global TimeTrialLevelCompleteFunc9
TimeTrialLevelCompleteFunc9: @ 0x08029604
	push {r4, lr}
	ldr r4, =0x03000584
	ldrh r0, [r4]
	bl sub_8029830
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	add r1, r0, #0
	sub r1, #8
	ldr r0, =0x03000586
	ldrh r0, [r0]
	cmp r1, r0
	bge _0802965C
	ldrh r2, [r4]
	mov r1, #0x70
	bl sub_80297D4
	ldr r0, =0x03003D40
	ldrb r0, [r0, #0x10]
	cmp r0, #0
	beq _08029644
	mov r0, #0xf
	bl playSong
	b _0802964A
	.align 2, 0
.pool
_08029644:
	ldr r0, =0x00000105
	bl playSong
_0802964A:
	ldr r1, =0x03000582
	mov r0, #0
	strh r0, [r1]
	mov r0, #1
	b _08029668
	.align 2, 0
.pool
_0802965C:
	ldrh r2, [r4]
	add r0, r1, #0
	mov r1, #0x70
	bl sub_80297D4
	mov r0, #0
_08029668:
	pop {r4}
	pop {r1}
	bx r1
	.byte 0x00, 0x00
	
	
	thumb_func_global TimeTrialLevelCompleteFunc10
TimeTrialLevelCompleteFunc10: @ 0x08029670
	push {r4, lr}
	ldr r2, =0x03000584
	mov r1, #0
	ldrsh r0, [r2, r1]
	cmp r0, #1
	bgt _0802969E
	ldr r0, =0x03002080
	ldr r0, [r0]
	mov r1, #8
	and r0, r1
	cmp r0, #0
	beq _08029698
	ldrh r0, [r2]
	bl sub_80298B0
	b _0802969E
	.align 2, 0
.pool
_08029698:
	ldrh r0, [r2]
	bl sub_80298F0
_0802969E:
	ldr r4, =0x03000582
	ldrh r0, [r4]
	add r0, #1
	strh r0, [r4]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0x3c
	bhi _080296B8
	mov r0, #0
	b _080296C6
	.align 2, 0
.pool
_080296B8:
	ldr r0, =0x03000584
	ldrh r0, [r0]
	bl sub_80298B0
	mov r0, #0
	strh r0, [r4]
	mov r0, #3
_080296C6:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool


	thumb_func_global sub_80296D0
sub_80296D0: @ 0x080296D0
	push {lr}
	sub sp, #4
	ldr r1, =TimeTrialStrings_0
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000588
	mov r2, #0
	str r2, [sp]
	mov r3, #1
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_8029700
sub_8029700: @ 0x08029700
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	ldr r1, =TimeTrialStrings_0
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =0x03000588
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_802973C
sub_802973C: @ 0x0802973C
	push {lr}
	ldr r1, =TimeTrialStrings_0
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0
	bl str_getX_ofFirstChar
	pop {r1}
	bx r1
	.align 2, 0
.pool


	thumb_func_global sub_8029760
sub_8029760: @ 0x08029760
	push {lr}
	ldr r1, =TimeTrialStrings_0
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0
	bl str_get_last_width_OamX_sum
	pop {r1}
	bx r1
	.align 2, 0
.pool
	thumb_func_global sub_8029784
sub_8029784: @ 0x08029784
	push {lr}
	sub sp, #4
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0
	beq _0802979C
	cmp r0, #1
	beq _080297A4
	ldr r0, =off_81C55E8
	b _080297A6
	.align 2, 0
.pool
_0802979C:
	ldr r0, =off_81C5558
	b _080297A6
	.align 2, 0
.pool
_080297A4:
	ldr r0, =QualifiedStrings
_080297A6:
	ldr r1, =0x03002080
	add r1, #0x2c
	ldrb r1, [r1]
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r2, [r1]
	ldr r1, =0x03000588
	mov r0, #0
	str r0, [sp]
	add r0, r2, #0
	mov r2, #0x25
	mov r3, #1
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool


	thumb_func_global sub_80297D4
sub_80297D4: @ 0x080297D4
	push {r4, r5, lr}
	sub sp, #4
	add r4, r0, #0
	add r5, r1, #0
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	cmp r2, #0
	beq _080297F0
	cmp r2, #1
	beq _080297F8
	ldr r0, =off_81C55E8
	b _080297FA
	.align 2, 0
.pool
_080297F0:
	ldr r0, =off_81C5558
	b _080297FA
	.align 2, 0
.pool
_080297F8:
	ldr r0, =QualifiedStrings
_080297FA:
	ldr r1, =0x03002080
	add r1, #0x2c
	ldrb r1, [r1]
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r3, [r1]
	lsl r1, r4, #0x10
	lsr r1, r1, #0x10
	lsl r2, r5, #0x10
	lsr r2, r2, #0x10
	ldr r0, =0x03000588
	str r0, [sp]
	add r0, r3, #0
	mov r3, #0x25
	bl setStringPos
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool


	thumb_func_global sub_8029830
sub_8029830: @ 0x08029830
	push {lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0
	beq _08029848
	cmp r0, #1
	beq _08029850
	ldr r0, =off_81C55E8
	b _08029852
	.align 2, 0
.pool
_08029848:
	ldr r0, =off_81C5558
	b _08029852
	.align 2, 0
.pool
_08029850:
	ldr r0, =QualifiedStrings
_08029852:
	ldr r1, =0x03002080
	add r1, #0x2c
	ldrb r1, [r1]
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r0, [r1]
	mov r1, #0x25
	bl str_getX_ofFirstChar
	pop {r1}
	bx r1
	.align 2, 0
.pool


	thumb_func_global sub_8029870
sub_8029870: @ 0x08029870
	push {lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0
	beq _08029888
	cmp r0, #1
	beq _08029890
	ldr r0, =off_81C55E8
	b _08029892
	.align 2, 0
.pool
_08029888:
	ldr r0, =off_81C5558
	b _08029892
	.align 2, 0
.pool
_08029890:
	ldr r0, =QualifiedStrings
_08029892:
	ldr r1, =0x03002080
	add r1, #0x2c
	ldrb r1, [r1]
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r0, [r1]
	mov r1, #0x25
	bl str_get_last_width_OamX_sum
	pop {r1}
	bx r1
	.align 2, 0
.pool


	thumb_func_global sub_80298B0
sub_80298B0: @ 0x080298B0
	push {lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0
	beq _080298C8
	cmp r0, #1
	beq _080298D0
	ldr r0, =off_81C55E8
	b _080298D2
	.align 2, 0
.pool
_080298C8:
	ldr r0, =off_81C5558
	b _080298D2
	.align 2, 0
.pool
_080298D0:
	ldr r0, =QualifiedStrings
_080298D2:
	ldr r1, =0x03002080
	add r1, #0x2c
	ldrb r1, [r1]
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r0, [r1]
	mov r1, #0x25
	bl showString
	pop {r0}
	bx r0
	.align 2, 0
.pool


	thumb_func_global sub_80298F0
sub_80298F0: @ 0x080298F0
	push {lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0
	beq _08029908
	cmp r0, #1
	beq _08029910
	ldr r0, =off_81C55E8
	b _08029912
	.align 2, 0
.pool
_08029908:
	ldr r0, =off_81C5558
	b _08029912
	.align 2, 0
.pool
_08029910:
	ldr r0, =QualifiedStrings
_08029912:
	ldr r1, =0x03002080
	add r1, #0x2c
	ldrb r1, [r1]
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r0, [r1]
	mov r1, #0x25
	bl hideString
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_8029930
	
	
	
sub_8029930: @ 0x08029930
	push {lr}
	sub sp, #4
	ldr r1, =string_levelNames
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03003D40
	ldrb r1, [r1, #1]
	lsl r1, r1, #5
	add r0, r0, r1
	ldr r1, =0x03000590
	mov r2, #0
	str r2, [sp]
	mov r2, #0xa
	mov r3, #1
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
	
	thumb_func_global sub_8029970
sub_8029970: @ 0x08029970
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =string_levelNames
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03003D40
	ldrb r1, [r1, #1]
	lsl r1, r1, #5
	add r0, r0, r1
	ldr r1, =0x03000590
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0xa
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_80299B8
sub_80299B8: @ 0x080299B8
	push {lr}
	ldr r1, =string_levelNames
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03003D40
	ldrb r1, [r1, #1]
	lsl r1, r1, #5
	add r0, r0, r1
	mov r1, #0xa
	bl str_get_last_width_OamX_sum
	pop {r1}
	bx r1
	.align 2, 0
.pool



	thumb_func_global sub_80299E8
sub_80299E8: @ 0x080299E8
	push {lr}
	sub sp, #4
	ldr r0, =0x03003D40
	ldrb r0, [r0, #1]
	cmp r0, #4
	bhi _08029A00
	ldr r0, =stageStrings
	b _08029A02
	.align 2, 0
.pool
_08029A00:
	ldr r0, =FinalStageStrings
_08029A02:
	ldr r1, =0x03002080
	add r1, #0x2c
	ldrb r1, [r1]
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r2, [r1]
	ldr r1, =0x03000590
	mov r0, #0
	str r0, [sp]
	add r0, r2, #0
	mov r2, #0x1a
	mov r3, #1
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_8029A30
sub_8029A30: @ 0x08029A30
	push {lr}
	sub sp, #4
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	lsl r1, r1, #0x10
	lsr r2, r1, #0x10
	ldr r0, =0x03003D40
	ldrb r0, [r0, #1]
	cmp r0, #4
	bhi _08029A50
	ldr r0, =stageStrings
	b _08029A52
	.align 2, 0
.pool
_08029A50:
	ldr r0, =FinalStageStrings
_08029A52:
	ldr r1, =0x03002080
	add r1, #0x2c
	ldrb r1, [r1]
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r1, [r1]
	ldr r0, =0x03000590
	str r0, [sp]
	add r0, r1, #0
	add r1, r3, #0
	mov r3, #0x1a
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_8029A80
sub_8029A80: @ 0x08029A80
	push {lr}
	ldr r0, =0x03003D40
	ldrb r0, [r0, #1]
	cmp r0, #4
	bhi _08029A98
	ldr r0, =stageStrings
	b _08029A9A
	.align 2, 0
.pool
_08029A98:
	ldr r0, =FinalStageStrings
_08029A9A:
	ldr r1, =0x03002080
	add r1, #0x2c
	ldrb r1, [r1]
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r0, [r1]
	mov r1, #0x1a
	bl str_get_last_width_OamX_sum
	pop {r1}
	bx r1
	.align 2, 0
.pool



	thumb_func_global sub_8029AB8
sub_8029AB8: @ 0x08029AB8
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x20
	ldr r7, =0x0600EA96
	ldr r0, =0x03003D40
	ldrb r0, [r0, #0x10]
	cmp r0, #0
	bne _08029AD0
	b _08029C28
_08029AD0:
	bl getDword3001F7C
	add r5, r0, #0
	mov r0, #0
	mov sb, r0
	mov r2, #0xe1
	lsl r2, r2, #4
	mov sl, r2
	add r0, r5, #0
	mov r1, sl
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	cmp r6, #0x63
	bls _08029AFC
	add r0, r6, #0
	sub r0, #0x63
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sb, r0
	mov r6, #0x63
_08029AFC:
	mov r4, sp
	add r0, r6, #0
	mov r1, #0xa
	bl Bios_Div
	add r0, #0xa
	strh r0, [r4]
	ldr r0, =0x01000001
	mov r8, r0
	mov r0, sp
	add r1, r7, #0
	mov r2, r8
	bl Bios_memcpy
	add r7, #2
	mov r4, sp
	add r4, #2
	add r0, r6, #0
	mov r1, #0xa
	bl Bios_modulo
	add r0, #0xa
	strh r0, [r4]
	add r0, r4, #0
	add r1, r7, #0
	mov r2, r8
	bl Bios_memcpy
	add r7, #2
	add r0, sp, #4
	mov r1, #0x14
	strh r1, [r0]
	add r1, r7, #0
	mov r2, r8
	bl Bios_memcpy
	add r7, #2
	add r0, r5, #0
	mov r1, sl
	bl Bios_modulo
	add r5, r0, #0
	mov r1, #0x3c
	bl Bios_Div
	mov r2, sb
	lsl r1, r2, #4
	sub r1, r1, r2
	lsl r1, r1, #2
	add r0, r0, r1
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	cmp r6, #0x63
	bls _08029B6A
	mov r6, #0x63
_08029B6A:
	mov r4, sp
	add r4, #6
	add r0, r6, #0
	mov r1, #0xa
	bl Bios_Div
	add r0, #0xa
	strh r0, [r4]
	add r0, r4, #0
	add r1, r7, #0
	mov r2, r8
	bl Bios_memcpy
	add r7, #2
	add r4, sp, #8
	add r0, r6, #0
	mov r1, #0xa
	bl Bios_modulo
	add r0, #0xa
	strh r0, [r4]
	add r0, r4, #0
	add r1, r7, #0
	mov r2, r8
	bl Bios_memcpy
	add r7, #2
	mov r0, sp
	add r0, #0xa
	mov r1, #0x15
	strh r1, [r0]
	add r1, r7, #0
	mov r2, r8
	bl Bios_memcpy
	add r7, #2
	add r0, r5, #0
	mov r1, #0x3c
	bl Bios_modulo
	add r5, r0, #0
	bl getDword3001F7C
	ldr r1, =0x0005879F
	cmp r0, r1
	bne _08029BDC
	mov r6, #0x63
	b _08029BEA
	.align 2, 0
.pool
_08029BDC:
	mov r0, #0x64
	mul r0, r5, r0
	mov r1, #0x3c
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
_08029BEA:
	add r4, sp, #0xc
	add r0, r6, #0
	mov r1, #0xa
	bl Bios_Div
	add r0, #0xa
	strh r0, [r4]
	ldr r5, =0x01000001
	add r0, r4, #0
	add r1, r7, #0
	add r2, r5, #0
	bl Bios_memcpy
	add r7, #2
	mov r4, sp
	add r4, #0xe
	add r0, r6, #0
	mov r1, #0xa
	bl Bios_modulo
	add r0, #0xa
	strh r0, [r4]
	add r0, r4, #0
	add r1, r7, #0
	add r2, r5, #0
	bl Bios_memcpy
	b _08029CA6
	.align 2, 0
.pool
_08029C28:
	add r0, sp, #0x10
	mov r5, #0x1b
	strh r5, [r0]
	ldr r4, =0x01000001
	add r1, r7, #0
	add r2, r4, #0
	bl Bios_memcpy
	ldr r7, =0x0600EA98
	mov r0, sp
	add r0, #0x12
	strh r5, [r0]
	add r1, r7, #0
	add r2, r4, #0
	bl Bios_memcpy
	add r7, #2
	add r0, sp, #0x14
	mov r1, #0x14
	strh r1, [r0]
	add r1, r7, #0
	add r2, r4, #0
	bl Bios_memcpy
	add r7, #2
	mov r0, sp
	add r0, #0x16
	strh r5, [r0]
	add r1, r7, #0
	add r2, r4, #0
	bl Bios_memcpy
	add r7, #2
	add r0, sp, #0x18
	strh r5, [r0]
	add r1, r7, #0
	add r2, r4, #0
	bl Bios_memcpy
	add r7, #2
	mov r0, sp
	add r0, #0x1a
	mov r1, #0x15
	strh r1, [r0]
	add r1, r7, #0
	add r2, r4, #0
	bl Bios_memcpy
	add r7, #2
	add r0, sp, #0x1c
	strh r5, [r0]
	add r1, r7, #0
	add r2, r4, #0
	bl Bios_memcpy
	add r7, #2
	mov r0, sp
	add r0, #0x1e
	strh r5, [r0]
	add r1, r7, #0
	add r2, r4, #0
	bl Bios_memcpy
_08029CA6:
	add sp, #0x20
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool
