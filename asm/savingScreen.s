.include "asm/macros.inc"

@completely disassembled

@ Screen that shows up after completing a stage in time trial mode


@ t�lle oma tiedosto
thumb_func_global cb_switch_postMissionSplash
cb_switch_postMissionSplash: @ 0x0802824C
	push {r4, lr}
	ldr r0, =0x03002080
	ldrh r1, [r0, #0xc]
	mov r0, #8
	and r0, r1
	ldr r4, =0x03000582
	cmp r0, #0
	beq _08028260
	mov r0, #6
	strh r0, [r4]
_08028260:
	ldr r1, =missioncompletionSplashFuncs
	ldrh r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl call_r0
	lsl r0, r0, #0x10
	asr r1, r0, #0x10
	mov r0, #1
	neg r0, r0
	cmp r1, r0
	beq _08028294
	cmp r1, #1
	bne _080282A8
	ldrh r0, [r4]
	add r0, #1
	strh r0, [r4]
	b _080282A8
	.align 2, 0
.pool
_08028294:
	ldr r2, =0x03003D40
	mov r0, #1
	ldrb r1, [r2, #1]
	lsl r0, r1
	ldrb r1, [r2, #8]
	orr r0, r1
	strb r0, [r2, #8]
	ldr r0, =(cb_saving+1)
	bl setCurrentTaskFunc
_080282A8:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

	thumb_func_global sub_80282B8
sub_80282B8: @ 0x080282B8
	push {r4, r5, lr}
	sub sp, #4
	ldr r0, =unk_81C4FBC
	bl loadLevelCompletionBG
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0802832C
	ldr r0, =levelCompletionPal
	mov r1, #5
	bl LoadSpritePalette
	add r5, r0, #0
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	mov r0, #0x78
	bl allocateSpriteTiles
	add r4, r0, #0
	ldr r0, =missionCompleteTiles
	lsl r4, r4, #0x10
	asr r1, r4, #0xb
	ldr r2, =0x06010000
	add r1, r1, r2
	bl Bios_LZ77_16_2
	ldr r0, =missionCompleteAnim
	lsr r4, r4, #0x10
	mov r1, #0
	str r1, [sp]
	add r2, r4, #0
	mov r3, #0
	bl allocSprite
	ldr r1, =0x030005A0
	str r0, [r1]
	add r0, #0x22
	strb r5, [r0]
	ldr r1, [r1]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	mov r0, #1
	b _08028330
	.align 2, 0
.pool
_0802832C:
	mov r0, #1
	neg r0, r0
_08028330:
	add sp, #4
	pop {r4, r5}
	pop {r1}
	bx r1
	thumb_func_global sub_8028338
sub_8028338: @ 0x08028338
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0802834E
	mov r0, #0
	b _08028366
_0802834E:
	ldr r1, =unk_81C4FC4
	ldr r0, =0x03003D40
	ldrb r0, [r0, #1]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	bl playSong
	ldr r1, =0x03000584
	mov r0, #0
	str r0, [r1]
	mov r0, #1
_08028366:
	pop {r1}
	bx r1
	.align 2, 0
.pool
	thumb_func_global sub_8028378
sub_8028378: @ 0x08028378
	push {lr}
	ldr r1, =0x03000584
	ldr r0, [r1]
	add r0, #1
	str r0, [r1]
	cmp r0, #0x3c
	bhi _08028390
	mov r0, #0
	b _080283AC
	.align 2, 0
.pool
_08028390:
	mov r0, #0
	str r0, [r1]
	ldr r0, =0x030005A0
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	mov r0, #0xe2
	bl playSong
	mov r0, #1
_080283AC:
	pop {r1}
	bx r1
	.align 2, 0
.pool
	thumb_func_global sub_80283B4
sub_80283B4: @ 0x080283B4
	push {lr}
	ldr r1, =0x03000584
	ldr r0, [r1]
	add r0, #1
	str r0, [r1]
	ldr r0, =0x030005A0
	ldr r0, [r0]
	bl updateSpriteAnimation
	add r1, r0, #0
	cmp r1, #0
	beq _080283D2
	bl sub_802845C
	mov r0, #1
_080283D2:
	pop {r1}
	bx r1
	.align 2, 0
.pool
	thumb_func_global sub_80283E0
sub_80283E0: @ 0x080283E0
	push {lr}
	ldr r1, =0x03000584
	ldr r0, [r1]
	add r0, #1
	str r0, [r1]
	cmp r0, #0xf0
	bhi _08028404
	ldr r0, =0x030005A0
	ldr r0, [r0]
	bl updateSpriteAnimation
	mov r0, #0
	b _0802840A
	.align 2, 0
.pool
_08028404:
	mov r0, #0
	str r0, [r1]
	mov r0, #1
_0802840A:
	pop {r1}
	bx r1
	.align 2, 0
	thumb_func_global sub_8028410
sub_8028410: @ 0x08028410
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _08028428
	mov r0, #0
	b _0802842A
_08028428:
	mov r0, #1
_0802842A:
	pop {r1}
	bx r1
	.align 2, 0
	thumb_func_global sub_8028430
sub_8028430: @ 0x08028430
	push {lr}
	bl stopSoundTracks
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl resetAffineTransformations
	bl clearSprites
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	mov r0, #1
	neg r0, r0
	pop {r1}
	bx r1
	.align 2, 0
.pool
	thumb_func_global sub_802845C
sub_802845C: @ 0x0802845C
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x1c
	ldr r0, =dword_8208DA0
	mov r1, #1
	bl LoadSpritePalette
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	str r1, [sp, #0x14]
	cmp r0, #0
	bge _0802847C
	b _0802868E
_0802847C:
	mov r0, #0xb
	bl allocateSpriteTiles
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	str r2, [sp, #0x10]
	asr r1, r0, #0x10
	cmp r1, #0
	bge _08028490
	b _0802868E
_08028490:
	ldr r0, =score_numberTiles
	lsl r1, r1, #5
	ldr r2, =0x06010000
	add r1, r1, r2
	bl Bios_LZ77_16_2
	mov r0, #0
	mov sl, r0
	mov r2, #0
	ldr r1, =stageCount
	ldr r5, =0x03003D40
	ldrb r4, [r5, #1]
	lsl r0, r4, #1
	add r0, r0, r1
	ldrh r3, [r0]
	cmp sl, r3
	bhs _080284DA
	ldr r6, =hostageCounts
	ldrb r1, [r5, #0x11]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r5, r0, #4
	lsl r1, r4, #3
_080284BE:
	lsl r0, r2, #1
	add r0, r0, r1
	add r0, r0, r5
	add r0, r0, r6
	ldrh r0, [r0]
	add r0, sl
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sl, r0
	add r0, r2, #1
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	cmp r2, r3
	blo _080284BE
_080284DA:
	mov r1, sl
	cmp r1, #0
	bne _080284E2
	b _0802868E
_080284E2:
	ldr r0, =0x03003C00
	ldrb r2, [r0, #3]
	lsr r2, r2, #7
	ldrb r0, [r0, #4]
	mov r1, #0xf
	and r0, r1
	lsl r0, r0, #1
	orr r0, r2
	mov r2, sl
	sub r0, r2, r0
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	ldr r1, =off_81C4FE4
	ldr r0, =0x03002080
	add r0, #0x2c
	mov r8, r0
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r5, [r0]
	ldr r7, =0x03000590
	mov r0, #0
	mov sb, r0
	str r0, [sp]
	add r0, r5, #0
	add r1, r7, #0
	mov r2, #0
	mov r3, #0
	bl bufferString
	add r0, r5, #0
	mov r1, #0
	bl str_get_last_width_OamX_sum
	lsl r0, r0, #0x10
	mov r1, #0xa0
	lsl r1, r1, #0xd
	add r0, r0, r1
	lsr r1, r0, #0x10
	cmp r6, #9
	bls _0802853C
	add r0, r1, #0
	add r0, #8
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
_0802853C:
	mov r2, sl
	cmp r2, #9
	bls _0802854A
	add r0, r1, #0
	add r0, #8
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
_0802854A:
	mov r0, #0xf0
	sub r0, r0, r1
	lsl r0, r0, #0xf
	lsr r4, r0, #0x10
	str r7, [sp]
	add r0, r5, #0
	add r1, r4, #0
	mov r2, #0x64
	mov r3, #0
	bl setStringPos
	ldr r1, =aBbb
	mov r2, r8
	ldrb r0, [r2]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	add r0, r4, r0
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	mov r5, #7
	ldr r0, [sp, #0x14]
	lsl r0, r0, #0x10
	str r0, [sp, #0x18]
	ldr r1, [sp, #0x10]
	lsl r7, r1, #0x10
	cmp r6, #9
	bls _080285BC
	add r0, r6, #0
	mov r1, #0xa
	bl Bios_Div
	asr r3, r7, #0x10
	add r3, r3, r0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r1, r4, #0x10
	asr r1, r1, #0x10
	ldr r2, [sp, #0x18]
	lsr r0, r2, #0x10
	str r0, [sp]
	mov r0, sb
	str r0, [sp, #4]
	ldr r0, =byte_8209AC8
	ldrb r0, [r0]
	str r0, [sp, #8]
	mov r2, sb
	str r2, [sp, #0xc]
	mov r0, #7
	mov r2, #0x64
	bl createSimpleSprite
	add r0, r4, #0
	add r0, #8
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	mov r5, #8
_080285BC:
	add r0, r6, #0
	mov r1, #0xa
	bl Bios_modulo
	add r3, r0, #0
	asr r7, r7, #0x10
	mov r8, r7
	add r3, r8
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r0, r5, #0x18
	lsr r0, r0, #0x18
	lsl r1, r4, #0x10
	asr r1, r1, #0x10
	ldr r2, [sp, #0x18]
	lsr r7, r2, #0x10
	str r7, [sp]
	mov r2, sb
	str r2, [sp, #4]
	ldr r2, =byte_8209AC8
	ldrb r6, [r2]
	str r6, [sp, #8]
	mov r2, sb
	str r2, [sp, #0xc]
	mov r2, #0x64
	bl createSimpleSprite
	add r1, r4, #0
	add r1, #8
	lsl r1, r1, #0x10
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	lsl r0, r5, #0x18
	lsr r0, r0, #0x18
	lsr r4, r1, #0x10
	asr r1, r1, #0x10
	mov r3, r8
	add r3, #0xa
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	str r7, [sp]
	mov r2, sb
	str r2, [sp, #4]
	str r6, [sp, #8]
	str r2, [sp, #0xc]
	mov r2, #0x64
	bl createSimpleSprite
	add r0, r4, #0
	add r0, #8
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	mov r0, sl
	cmp r0, #9
	bls _08028666
	mov r1, #0xa
	bl Bios_Div
	add r3, r0, #0
	add r3, r8
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r0, r5, #0x18
	lsr r0, r0, #0x18
	lsl r1, r4, #0x10
	asr r1, r1, #0x10
	str r7, [sp]
	mov r2, sb
	str r2, [sp, #4]
	str r6, [sp, #8]
	str r2, [sp, #0xc]
	mov r2, #0x64
	bl createSimpleSprite
	add r0, r4, #0
	add r0, #8
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
_08028666:
	mov r0, sl
	mov r1, #0xa
	bl Bios_modulo
	add r3, r0, #0
	add r3, r8
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r0, r5, #0x18
	lsr r0, r0, #0x18
	lsl r1, r4, #0x10
	asr r1, r1, #0x10
	str r7, [sp]
	mov r2, sb
	str r2, [sp, #4]
	str r6, [sp, #8]
	str r2, [sp, #0xc]
	mov r2, #0x64
	bl createSimpleSprite
_0802868E:
	add sp, #0x1c
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool








thumb_func_global cb_saving
cb_saving: @ 0x080286D0
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r0, =0x03000588
	mov r1, #0
	bl allocFont
	ldr r0, =0x03000590
	mov r1, #2
	bl allocFont
	ldr r1, =0x03000580
	mov r0, #0
	strh r0, [r1]
	bl sub_08028DAC
	bl sub_08028E3C
	add r4, r0, #0
	bl sub_08028E18
	sub r4, r4, r0
	mov r5, #0xf0
	sub r4, r5, r4
	add r0, r4, #0
	mov r1, #2
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x30
	bl sub_08028DDC
	bl sub_08028E60
	bl sub_08028EF4
	add r4, r0, #0
	bl sub_08028ED0
	sub r4, r4, r0
	sub r4, r5, r4
	add r0, r4, #0
	mov r1, #2
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x50
	bl sub_08028E94
	bl sub_08028F18
	bl sub_08028FAC
	add r4, r0, #0
	bl sub_08028F88
	sub r4, r4, r0
	sub r5, r5, r4
	add r0, r5, #0
	mov r1, #2
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x60
	bl sub_08028F4C
	ldr r2, =0x03002080
	ldrb r1, [r2, #9]
	mov r0, #5
	neg r0, r0
	and r0, r1
	strb r0, [r2, #9]
	ldr r5, =0x03003D40
	ldrb r0, [r5]
	cmp r0, #0
	bge _0802878E
	b _08028B34
_0802878E:
	cmp r0, #1
	ble _080287B4
	cmp r0, #2
	bne _08028798
	b _08028ACC
_08028798:
	b _08028B34
	.align 2, 0
.pool
_080287B4:
	ldr r3, =0x03003C00
	add r0, r2, #0
	add r0, #0x2c
	ldrb r1, [r0]
	lsl r1, r1, #6
	ldrb r2, [r3, #4]
	mov r0, #0x3f
	and r0, r2
	orr r0, r1
	strb r0, [r3, #4]
	ldrb r0, [r5, #1]
	add r4, r3, #0
	cmp r0, #4
	bls _080287D2
	b _080288C6
_080287D2:
	ldrb r6, [r5, #0x11]
	cmp r6, #0
	bne _08028804
	ldrb r0, [r5, #8]
	cmp r0, #7
	bne _08028804
	add r7, r4, #0
	add r7, #0xce
	ldrh r0, [r7]
	mov r1, #0x64
	mul r0, r1, r0
	ldr r1, [r5, #0x1c]
	cmp r1, r0
	bls _080287F8
	add r0, r1, #0
	mov r1, #0x64
	bl Bios_Div
	strh r0, [r7]
_080287F8:
	add r0, r4, #0
	add r0, #0xcc
	strh r6, [r0]
	b _080288DE
	.align 2, 0
.pool
_08028804:
	ldr r0, =0x03003D40
	ldrb r2, [r0, #8]
	add r5, r0, #0
	cmp r2, #0
	bne _08028830
	add r0, #0x20
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _08028830
	ldrb r0, [r4]
	mov r1, #0x41
	neg r1, r1
	and r1, r0
	strb r1, [r4]
	add r0, r4, #0
	add r0, #0xcc
	strh r2, [r0]
	b _08028B34
	.align 2, 0
.pool
_08028830:
	mov r0, #0x20
	add r0, r0, r5
	mov r8, r0
	ldrb r1, [r0]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _0802885C
	add r4, #0xce
	ldrh r0, [r4]
	mov r1, #0x64
	mul r0, r1, r0
	ldr r1, [r5, #0x1c]
	cmp r1, r0
	bhi _08028850
	b _08028B34
_08028850:
	add r0, r1, #0
	mov r1, #0x64
	bl Bios_Div
	strh r0, [r4]
	b _08028B34
_0802885C:
	ldr r0, [r5, #0x1c]
	mov r1, #0x64
	bl Bios_Div
	ldr r4, =0x03003C00
	add r7, r4, #0
	add r7, #0xcc
	strh r0, [r7]
	add r6, r4, #0
	add r6, #0xce
	ldrh r1, [r6]
	mov r0, #0x64
	mul r0, r1, r0
	ldr r1, [r5, #0x1c]
	cmp r1, r0
	bls _08028886
	add r0, r1, #0
	mov r1, #0x64
	bl Bios_Div
	strh r0, [r6]
_08028886:
	ldrb r0, [r5, #0x10]
	cmp r0, #0
	beq _080288DE
	ldrb r2, [r5, #2]
	ldr r1, =stageCount
	ldrb r0, [r5, #1]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	sub r0, #1
	cmp r2, r0
	bne _080288DE
	mov r2, r8
	ldrb r1, [r2]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	bne _080288BC
	ldrh r1, [r7]
	add r0, r4, #0
	add r0, #0xd0
	strh r1, [r0]
	b _080288DE
	.align 2, 0
.pool
_080288BC:
	add r0, r4, #0
	add r0, #0xd0
	ldrh r0, [r0]
	strh r0, [r7]
	b _080288DE
_080288C6:
	add r4, #0xce
	ldrh r0, [r4]
	mov r1, #0x64
	mul r0, r1, r0
	ldr r1, [r5, #0x1c]
	cmp r1, r0
	bls _080288DE
	add r0, r1, #0
	mov r1, #0x64
	bl Bios_Div
	strh r0, [r4]
_080288DE:
	ldr r0, =0x03003C00
	ldrb r1, [r0]
	mov r2, #0x40
	orr r1, r2
	strb r1, [r0]
	mov r2, #0
	ldr r5, =0x03003D40
	add r4, r0, #0
	add r6, r4, #5
	add r3, r5, #0
	add r3, #0xa
_080288F4:
	add r1, r2, r6
	add r0, r2, r3
	ldrb r0, [r0]
	strb r0, [r1]
	add r0, r2, #1
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	cmp r2, #5
	bls _080288F4
	ldr r3, =0x03003D40
	ldrb r1, [r3, #8]
	mov r0, #0x3f
	and r1, r0
	ldrb r2, [r4]
	mov r0, #0x40
	neg r0, r0
	and r0, r2
	orr r0, r1
	strb r0, [r4]
	ldrb r1, [r3, #1]
	mov r0, #7
	and r1, r0
	lsl r1, r1, #7
	ldrh r2, [r4]
	ldr r0, =0xFFFFFC7F
	and r0, r2
	orr r0, r1
	strh r0, [r4]
	ldrb r1, [r3, #2]
	mov r0, #3
	and r1, r0
	lsl r1, r1, #2
	ldrb r2, [r4, #1]
	mov r0, #0xd
	neg r0, r0
	and r0, r2
	orr r0, r1
	strb r0, [r4, #1]
	ldrb r0, [r3, #1]
	cmp r0, #5
	bne _0802895C
	mov r0, #0x50
	bl sub_0803242C
	b _08028974
	.align 2, 0
.pool
_0802895C:
	bl getPlayerHealth
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x7f
	and r0, r1
	lsl r0, r0, #0xc
	ldr r1, [r4]
	ldr r2, =0xFFF80FFF
	and r1, r2
	orr r1, r0
	str r1, [r4]
_08028974:
	bl getPlayerSpecial
	ldr r5, =0x03003C00
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x7f
	and r0, r1
	lsl r0, r0, #3
	ldrh r2, [r5, #2]
	ldr r1, =0xFFFFFC07
	and r1, r2
	orr r1, r0
	strh r1, [r5, #2]
	bl getPowerUpLevel
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	mov r6, #3
	and r0, r6
	lsl r0, r0, #4
	ldrb r2, [r5, #4]
	mov r1, #0x31
	neg r1, r1
	and r1, r2
	orr r1, r0
	strb r1, [r5, #4]
	ldr r4, =0x03003D40
	ldrb r1, [r4, #8]
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	beq _080289B6
	strb r1, [r4, #9]
_080289B6:
	ldrb r0, [r4, #8]
	mov ip, r0
	mov r0, #0x20
	mov r1, ip
	and r0, r1
	ldrb r3, [r4, #0x11]
	cmp r0, #0
	beq _080289E4
	ldrb r2, [r5, #3]
	lsl r1, r2, #0x19
	lsr r1, r1, #0x1d
	mov r0, #1
	lsl r0, r3
	orr r1, r0
	mov r0, #7
	and r1, r0
	lsl r1, r1, #4
	mov r0, #0x71
	neg r0, r0
	and r0, r2
	orr r0, r1
	strb r0, [r5, #3]
	ldrb r3, [r4, #0x11]
_080289E4:
	add r0, r3, #0
	and r0, r6
	lsl r0, r0, #2
	ldrb r1, [r5, #3]
	mov r2, #0xd
	neg r2, r2
	and r2, r1
	orr r2, r0
	strb r2, [r5, #3]
	mov r7, #1
	strb r7, [r4]
	ldrb r6, [r4, #0x11]
	cmp r6, #0
	bne _08028A30
	mov r1, ip
	lsl r0, r1, #0x18
	lsr r0, r0, #0x18
	cmp r0, #7
	bne _08028A30
	ldrb r1, [r5]
	mov r0, #0x41
	neg r0, r0
	and r0, r1
	strb r0, [r5]
	lsl r1, r2, #0x19
	lsr r1, r1, #0x1d
	add r0, r7, #0
	lsl r0, r3
	orr r1, r0
	mov r0, #7
	and r1, r0
	lsl r1, r1, #4
	mov r0, #0x71
	neg r0, r0
	and r2, r0
	orr r2, r1
	strb r2, [r5, #3]
	strb r6, [r4]
_08028A30:
	ldr r3, =0x03003D40
	ldrb r0, [r3, #1]
	cmp r0, #5
	bne _08028A4E
	ldrb r0, [r3, #0x10]
	cmp r0, #0
	beq _08028B34
	ldr r2, =0x03003C00
	ldrb r1, [r2]
	mov r0, #0x41
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	mov r0, #0
	strb r0, [r3]
_08028A4E:
	ldrb r0, [r3, #0x10]
	cmp r0, #0
	beq _08028B34
	ldrb r2, [r3, #2]
	ldr r1, =stageCount
	ldrb r0, [r3, #1]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	sub r0, #1
	cmp r2, r0
	bge _08028AB0
	bl getByte3001F8E
	ldr r3, =0x03003C00
	ldrb r4, [r3, #3]
	lsr r2, r4, #7
	ldrb r1, [r3, #4]
	mov r5, #0xf
	and r1, r5
	lsl r1, r1, #1
	orr r1, r2
	add r1, r1, r0
	mov r2, #1
	and r2, r1
	lsl r2, r2, #7
	mov r0, #0x7f
	and r0, r4
	orr r0, r2
	strb r0, [r3, #3]
	lsr r1, r1, #1
	and r1, r5
	ldrb r2, [r3, #4]
	mov r0, #0x10
	neg r0, r0
	and r0, r2
	orr r0, r1
	strb r0, [r3, #4]
	b _08028B34
	.align 2, 0
.pool
_08028AB0:
	ldr r1, =0x03003C00
	ldrb r2, [r1, #3]
	mov r0, #0x7f
	and r0, r2
	strb r0, [r1, #3]
	ldrb r2, [r1, #4]
	mov r0, #0x10
	neg r0, r0
	and r0, r2
	strb r0, [r1, #4]
	b _08028B34
	.align 2, 0
.pool
_08028ACC:
	ldr r2, =0x03000598
	ldr r3, =0x03003C00
	ldrb r1, [r5, #2]
	lsl r1, r1, #3
	ldrb r0, [r5, #1]
	lsl r0, r0, #5
	add r1, r1, r0
	add r4, r3, #0
	add r4, #0xc
	add r1, r1, r4
	ldr r0, [r1]
	str r0, [r2]
	ldr r2, =0x0300059C
	ldrb r1, [r5, #2]
	lsl r1, r1, #3
	ldrb r0, [r5, #1]
	lsl r0, r0, #5
	add r1, r1, r0
	add r6, r3, #0
	add r6, #0x10
	add r1, r1, r6
	ldr r0, [r1]
	str r0, [r2]
	bl getDword3001F7C
	ldrb r1, [r5, #2]
	lsl r1, r1, #3
	ldrb r2, [r5, #1]
	lsl r2, r2, #5
	add r1, r1, r2
	add r1, r1, r4
	str r0, [r1]
	ldrb r1, [r5, #2]
	lsl r1, r1, #3
	ldrb r0, [r5, #1]
	lsl r0, r0, #5
	add r1, r1, r0
	add r4, r1, r4
	add r1, r1, r6
	ldr r2, [r4]
	ldr r0, [r1]
	cmp r2, r0
	bhs _08028B34
	bl getDword3001F7C
	ldrb r1, [r5, #2]
	lsl r1, r1, #3
	ldrb r2, [r5, #1]
	lsl r2, r2, #5
	add r1, r1, r2
	add r1, r1, r6
	str r0, [r1]
_08028B34:
	ldr r0, =(cb_saving_switchcase+1)
	bl setCurrentTaskFunc
	ldr r1, =0x03000582
	mov r0, #0
	strh r0, [r1]
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global cb_saving_switchcase
cb_saving_switchcase: @ 0x08028B60
	push {r4, lr}
	ldr r1, =savingFunctions
	ldr r4, =0x03000580
	ldrh r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl call_r0
	lsl r0, r0, #0x10
	asr r1, r0, #0x10
	mov r0, #1
	neg r0, r0
	cmp r1, r0
	beq _08028B94
	cmp r1, #1
	bne _08028C4E
	ldrh r0, [r4]
	add r0, #1
	strh r0, [r4]
	b _08028C4E
	.align 2, 0
.pool
_08028B94:
	ldr r2, =0x03002080
	ldrb r0, [r2, #9]
	mov r1, #4
	orr r0, r1
	strb r0, [r2, #9]
	ldr r2, =0x03003D40
	ldrb r0, [r2]
	cmp r0, #2
	bne _08028BD0
	bl sub_08028FD0
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08028BC4
	ldr r0, =(sub_80318A4+1)
	bl setCurrentTaskFunc
	b _08028C4E
	.align 2, 0
.pool
_08028BC4:
	ldr r0, =(cb_loadTimeTrialMenu+1)
	bl setCurrentTaskFunc
	b _08028C4E
	.align 2, 0
.pool
_08028BD0:
	add r0, r2, #0
	add r0, #0x20
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _08028C34
	ldrb r0, [r2, #1]
	cmp r0, #5
	bne _08028C08
	mov r0, #4
	strb r0, [r2, #1]
	mov r0, #2
	strb r0, [r2, #2]
	ldr r0, =0x03003C00
	add r0, #0xd0
	ldrh r1, [r0]
	mov r0, #0x64
	mul r0, r1, r0
	str r0, [r2, #0x1c]
	ldr r0, =(cb_moveToNextStage+1)
	bl setCurrentTaskFunc
	b _08028C4E
	.align 2, 0
.pool
_08028C08:
	ldr r1, =0x03003C00
	ldrb r0, [r1]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1f
	cmp r0, #0
	bne _08028C1C
	strb r0, [r2]
	b _08028C26
	.align 2, 0
.pool
_08028C1C:
	add r0, r1, #0
	add r0, #0xd0
	ldrh r1, [r0]
	mov r0, #0x64
	mul r0, r1, r0
_08028C26:
	str r0, [r2, #0x1c]
	ldr r0, =(cb_loadLevelSelect+1)
	bl setCurrentTaskFunc
	b _08028C4E
	.align 2, 0
.pool
_08028C34:
	ldrb r0, [r2, #0x10]
	cmp r0, #0
	bne _08028C48
	ldr r0, =(loadGameOverScreen+1)
	bl setCurrentTaskFunc
	b _08028C4E
	.align 2, 0
.pool
_08028C48:
	ldr r0, =(cb_moveToNextStage+1)
	bl setCurrentTaskFunc
_08028C4E:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global savingState0
savingState0: @ 0x08028C58
	push {r4, lr}
	ldr r4, =0x030005A0
	add r0, r4, #0
	bl loadSaveGame
	cmp r0, #1
	beq _08028C96
	cmp r0, #2
	beq _08028C74
	b _08028C7A
	.align 2, 0
.pool
_08028C70:
	mov r0, #1
	b _08028C9A
_08028C74:
	add r0, r4, #0
	bl sub_08028D60
_08028C7A:
	ldr r3, =0x03003C00
	ldr r2, =0x030005A0
	mov r1, #0
_08028C80:
	ldrb r0, [r3]
	ldrb r4, [r2]
	cmp r0, r4
	bne _08028C70
	add r3, #1
	add r2, #1
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	cmp r1, #0xd3
	bls _08028C80
_08028C96:
	mov r0, #1
	neg r0, r0
_08028C9A:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global savingState1
savingState1: @ 0x08028CA8
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _08028CBE
	mov r0, #0
	b _08028CC0
_08028CBE:
	mov r0, #1
_08028CC0:
	pop {r1}
	bx r1

thumb_func_global savingState2
savingState2: @ 0x08028CC4
	push {lr}
	bl saveGame
	add r1, r0, #0
	cmp r1, #0
	beq _08028CD4
	mov r0, #0
	b _08028CDA
_08028CD4:
	ldr r0, =0x03000582
	strh r1, [r0]
	mov r0, #1
_08028CDA:
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global savingState3
savingState3: @ 0x08028CE4
	ldr r1, =0x03000582
	ldrh r0, [r1]
	add r0, #1
	strh r0, [r1]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0x2d
	bhi _08028CFC
	mov r0, #0
	b _08028D02
	.align 2, 0
.pool
_08028CFC:
	mov r0, #0
	strh r0, [r1]
	mov r0, #1
_08028D02:
	bx lr

thumb_func_global savingState4
savingState4: @ 0x08028D04
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _08028D1C
	mov r0, #0
	b _08028D1E
_08028D1C:
	mov r0, #1
_08028D1E:
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global savingState5
savingState5: @ 0x08028D24
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	mov r0, #1
	neg r0, r0
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_08028D50
sub_08028D50: @ 0x08028D50
	push {lr}
	ldr r0, =0x03003C00
	bl sub_08028D60
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08028D60
sub_08028D60: @ 0x08028D60
	push {r4, r5, lr}
	sub sp, #4
	add r4, r0, #0
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r2, =0x0100006A
	mov r0, sp
	add r1, r4, #0
	bl Bios_memcpy
	mov r0, #0
	add r4, #0x10
	ldr r5, =0x0005879F
_08028D7C:
	mov r1, #0
	add r3, r0, #1
	lsl r2, r0, #5
_08028D82:
	lsl r0, r1, #3
	add r0, r0, r2
	add r0, r4, r0
	str r5, [r0]
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	cmp r1, #3
	bls _08028D82
	lsl r0, r3, #0x10
	lsr r0, r0, #0x10
	cmp r0, #5
	bls _08028D7C
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08028DAC
sub_08028DAC: @ 0x08028DAC
	push {lr}
	sub sp, #4
	ldr r1, =str_saving
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000588
	mov r2, #0
	str r2, [sp]
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08028DDC
sub_08028DDC: @ 0x08028DDC
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =str_saving
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000588
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08028E18
sub_08028E18: @ 0x08028E18
	push {lr}
	ldr r1, =str_saving
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0
	bl str_getX_ofFirstChar
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_08028E3C
sub_08028E3C: @ 0x08028E3C
	push {lr}
	ldr r1, =str_saving
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0
	bl str_get_last_width_OamX_sum
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_08028E60
sub_08028E60: @ 0x08028E60
	push {lr}
	sub sp, #4
	ldr r1, =off_81C50A8
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000590
	mov r2, #0
	str r2, [sp]
	mov r2, #0xa
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08028E94
sub_08028E94: @ 0x08028E94
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =off_81C50A8
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000590
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0xa
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08028ED0
sub_08028ED0: @ 0x08028ED0
	push {lr}
	ldr r1, =off_81C50A8
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0xa
	bl str_getX_ofFirstChar
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_08028EF4
sub_08028EF4: @ 0x08028EF4
	push {lr}
	ldr r1, =off_81C50A8
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0xa
	bl str_get_last_width_OamX_sum
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_08028F18
sub_08028F18: @ 0x08028F18
	push {lr}
	sub sp, #4
	ldr r1, =off_81C5110
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000590
	mov r2, #0
	str r2, [sp]
	mov r2, #0x1c
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08028F4C
sub_08028F4C: @ 0x08028F4C
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =off_81C5110
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000590
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0x1c
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08028F88
sub_08028F88: @ 0x08028F88
	push {lr}
	ldr r1, =off_81C5110
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0x1c
	bl str_getX_ofFirstChar
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_08028FAC
sub_08028FAC: @ 0x08028FAC
	push {lr}
	ldr r1, =off_81C5110
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0x1c
	bl str_get_last_width_OamX_sum
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_08028FD0
sub_08028FD0: @ 0x08028FD0
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	ldr r0, =0x03003D40
	ldrb r1, [r0]
	mov sb, r0
	cmp r1, #2
	bne _0802903E
	ldr r3, =0x03000598
	ldr r2, =dword_82065EC
	mov r0, sb
	ldrb r1, [r0, #2]
	lsl r1, r1, #2
	ldrb r0, [r0, #1]
	lsl r0, r0, #4
	add r1, r1, r0
	add r1, r1, r2
	ldr r3, [r3]
	ldr r0, [r1]
	add r6, r2, #0
	cmp r3, r0
	bhs _08029004
	cmp r3, #0
	bne _0802903E
_08029004:
	ldr r2, =0x0300059C
	mov r1, sb
	ldrb r4, [r1, #2]
	lsl r0, r4, #2
	ldrb r3, [r1, #1]
	lsl r1, r3, #4
	add r0, r0, r1
	add r0, r0, r6
	ldr r1, [r2]
	ldr r5, [r0]
	cmp r1, r5
	blo _0802903E
	ldr r2, =0x03003C00
	lsl r1, r4, #3
	lsl r0, r3, #5
	add r1, r1, r0
	add r0, r2, #0
	add r0, #0xc
	add r0, r1, r0
	ldr r3, [r0]
	mov ip, r2
	cmp r3, r5
	bhs _0802903E
	mov r0, ip
	add r0, #0x10
	add r0, r1, r0
	ldr r0, [r0]
	cmp r3, r0
	bls _08029058
_0802903E:
	mov r0, #0
	b _080290BC
	.align 2, 0
.pool
_08029058:
	mov r5, #0
	mov sl, r6
_0802905C:
	mov r4, #0
	lsl r0, r5, #1
	ldr r1, =stageCount
	add r0, r0, r1
	ldrh r1, [r0]
	cmp r4, r1
	bhs _080290B0
	ldr r0, =0x03003D40
	ldrb r7, [r0, #1]
	ldr r0, =0x03003C10
	mov r8, r0
	add r6, r1, #0
_08029074:
	cmp r5, r7
	bne _08029080
	mov r1, sb
	ldrb r1, [r1, #2]
	cmp r4, r1
	beq _080290A6
_08029080:
	lsl r1, r4, #3
	lsl r0, r5, #5
	add r3, r1, r0
	mov r0, r8
	add r2, r3, r0
	lsl r0, r4, #2
	lsl r1, r5, #4
	add r0, r0, r1
	add r0, sl
	ldr r1, [r2]
	ldr r0, [r0]
	cmp r1, r0
	bhs _0802903E
	mov r0, ip
	add r0, #0xc
	add r0, r3, r0
	ldr r0, [r0]
	cmp r0, #0
	beq _0802903E
_080290A6:
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, r6
	blo _08029074
_080290B0:
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, #5
	bls _0802905C
	mov r0, #1
_080290BC:
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool

