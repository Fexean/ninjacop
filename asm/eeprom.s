.include "asm/macros.inc"

@completely disassembled!


thumb_func_global setSaveType
setSaveType: @ 0x0803540C
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r2, #0
	cmp r0, #4
	bne _08035428
	ldr r1, =0x03006C34
	ldr r0, =eepromInfo4k
	str r0, [r1]
	b _08035444
	.align 2, 0
.pool
_08035428:
	cmp r0, #0x40
	bne _0803543C
	ldr r1, =0x03006C34
	ldr r0, =eepromInfo64k
	str r0, [r1]
	b _08035444
	.align 2, 0
.pool
_0803543C:
	ldr r1, =0x03006C34
	ldr r0, =eepromInfo4k
	str r0, [r1]
	mov r2, #1
_08035444:
	add r0, r2, #0
	bx lr
	.align 2, 0
.pool
	
	
	thumb_func_global sub_8035450
sub_8035450: @ 0x08035450
	ldr r1, =0x03002066
	ldrh r0, [r1]
	cmp r0, #0
	beq locret_803546A
	ldrh r0, [r1]
	sub r0, #1
	strh r0, [r1]
	lsl r0, r0, #0x10
	cmp r0, #0
	bne locret_803546A
	ldr r1, =0x03002068
	mov r0, #1
	strb r0, [r1]
locret_803546A:
	bx lr
	.align 2, 0
.pool
	
	
	
thumb_func_global setEepromTimer
setEepromTimer: @ 0x08035474
	add r2, r1, #0
	lsl r0, r0, #0x18
	lsr r1, r0, #0x18
	cmp r1, #3
	bhi _080354A8
	ldr r0, =0x03002064
	strb r1, [r0]
	ldr r1, =0x0300206C
	ldrb r0, [r0]
	lsl r0, r0, #2
	ldr r3, =0x04000100
	add r0, r0, r3
	str r0, [r1]
	ldr r0, =(sub_8035450+1)
	str r0, [r2]
	mov r0, #0
	b _080354AA
	.align 2, 0
.pool
_080354A8:
	mov r0, #1
_080354AA:
	bx lr

thumb_func_global eepromStartTimer
eepromStartTimer: @ 0x080354AC
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	ldr r2, =0x03002070
	ldr r1, =0x04000208
	mov sb, r1
	ldrh r1, [r1]
	strh r1, [r2]
	mov r6, #0
	mov r2, sb
	strh r6, [r2]
	ldr r3, =0x0300206C
	mov r8, r3
	ldr r5, [r3]
	strh r6, [r5, #2]
	ldr r3, =0x04000202
	ldr r4, =0x03002064
	ldrb r1, [r4]
	mov r2, #8
	add r7, r2, #0
	lsl r7, r1
	add r1, r7, #0
	strh r1, [r3]
	sub r3, #2
	ldrb r1, [r4]
	lsl r2, r1
	ldrh r1, [r3]
	orr r1, r2
	strh r1, [r3]
	ldr r1, =0x03002068
	strb r6, [r1]
	ldr r2, =0x03002066
	ldrh r1, [r0]
	strh r1, [r2]
	add r0, #2
	ldrh r1, [r0]
	strh r1, [r5]
	add r1, r5, #2
	mov r2, r8
	str r1, [r2]
	ldrh r0, [r0, #2]
	strh r0, [r5, #2]
	str r5, [r2]
	mov r0, #1
	mov r3, sb
	strh r0, [r3]
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global disableGlobalTimer
disableGlobalTimer: @ 0x08035534
	ldr r3, =0x04000208
	mov r1, #0
	strh r1, [r3]
	ldr r2, =0x0300206C
	ldr r0, [r2]
	strh r1, [r0]
	add r0, #2
	str r0, [r2]
	strh r1, [r0]
	sub r0, #2
	str r0, [r2]
	ldr r2, =0x04000200
	ldr r0, =0x03002064
	ldrb r0, [r0]
	mov r1, #8
	lsl r1, r0
	ldrh r0, [r2]
	bic r0, r1
	strh r0, [r2]
	ldr r0, =0x03002070
	ldrh r0, [r0]
	strh r0, [r3]
	bx lr
	.align 2, 0
.pool

thumb_func_global unused_noIrqDMA
unused_noIrqDMA: @ 0x08035578
	push {r4, r5, r6, lr}
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r4, =0x04000208
	ldrh r3, [r4]
	add r6, r3, #0
	mov r3, #0
	strh r3, [r4]
	ldr r5, =0x04000204
	ldrh r4, [r5]
	ldr r3, =0x0000F8FF
	and r4, r3
	ldr r3, =0x03006C34
	ldr r3, [r3]
	ldrh r3, [r3, #6]
	orr r4, r3
	strh r4, [r5]
	ldr r3, =0x040000D4
	str r0, [r3]
	ldr r0, =0x040000D8
	str r1, [r0]
	ldr r1, =0x040000DC
	mov r0, #0x80
	lsl r0, r0, #0x18
	orr r2, r0
	str r2, [r1]
	add r1, #2
	mov r2, #0x80
	lsl r2, r2, #8
	add r0, r2, #0
	ldrh r1, [r1]
	and r0, r1
	cmp r0, #0
	beq _080355CC
	ldr r2, =0x040000DE
	mov r0, #0x80
	lsl r0, r0, #8
	add r1, r0, #0
_080355C4:
	ldrh r0, [r2]
	and r0, r1
	cmp r0, #0
	bne _080355C4
_080355CC:
	ldr r0, =0x04000208
	strh r6, [r0]
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global read64bitEeprom
read64bitEeprom: @ 0x080355F8
	push {r4, r5, r6, lr}
	sub sp, #0x88
	add r5, r1, #0
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	ldr r0, =0x03006C34
	ldr r0, [r0]
	ldrh r0, [r0, #4]
	cmp r3, r0
	blo _08035618
	ldr r0, =0x000080FF
	b _0803569A
	.align 2, 0
.pool
_08035618:
	ldr r0, =0x03006C34
	add r6, r0, #0
	ldr r0, [r0]
	ldrb r1, [r0, #8]
	lsl r0, r1, #1
	mov r4, sp
	add r2, r0, r4
	add r2, #2
	mov r4, #0
	cmp r4, r1
	bhs _08035642
_0803562E:
	strh r3, [r2]
	sub r2, #2
	lsr r3, r3, #1
	add r0, r4, #1
	lsl r0, r0, #0x18
	lsr r4, r0, #0x18
	ldr r0, [r6]
	ldrb r0, [r0, #8]
	cmp r4, r0
	blo _0803562E
_08035642:
	mov r0, #1
	strh r0, [r2]
	sub r2, #2
	strh r0, [r2]
	mov r4, #0xd0
	lsl r4, r4, #0x14
	ldr r0, =0x03006C34
	ldr r0, [r0]
	ldrb r2, [r0, #8]
	add r2, #3
	mov r0, sp
	add r1, r4, #0
	bl unused_noIrqDMA
	add r0, r4, #0
	mov r1, sp
	mov r2, #0x44
	bl unused_noIrqDMA
	add r2, sp, #8
	add r5, #6
	mov r4, #0
	mov r6, #1
_08035670:
	mov r1, #0
	mov r3, #0
_08035674:
	lsl r1, r1, #0x11
	ldrh r0, [r2]
	and r0, r6
	lsr r1, r1, #0x10
	orr r1, r0
	add r2, #2
	add r0, r3, #1
	lsl r0, r0, #0x18
	lsr r3, r0, #0x18
	cmp r3, #0xf
	bls _08035674
	strh r1, [r5]
	sub r5, #2
	add r0, r4, #1
	lsl r0, r0, #0x18
	lsr r4, r0, #0x18
	cmp r4, #3
	bls _08035670
	mov r0, #0
_0803569A:
	add sp, #0x88
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global write64bitEeprom
write64bitEeprom: @ 0x080356A8
	push {r4, r5, lr}
	sub sp, #0xa4
	add r5, r1, #0
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	ldr r0, =0x03006C34
	ldr r0, [r0]
	ldrh r0, [r0, #4]
	cmp r4, r0
	blo _080356C8
	ldr r0, =0x000080FF
	b _0803576C
	.align 2, 0
.pool
_080356C8:
	ldr r0, =0x03006C34
	ldr r0, [r0]
	ldrb r0, [r0, #8]
	lsl r0, r0, #1
	mov r1, sp
	add r3, r0, r1
	add r3, #0x84
	mov r0, #0
	strh r0, [r3]
	sub r3, #2
	mov r1, #0
_080356DE:
	ldrh r2, [r5]
	add r5, #2
	mov r0, #0
_080356E4:
	strh r2, [r3]
	sub r3, #2
	lsr r2, r2, #1
	add r0, #1
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0xf
	bls _080356E4
	add r0, r1, #1
	lsl r0, r0, #0x18
	lsr r1, r0, #0x18
	cmp r1, #3
	bls _080356DE
	mov r1, #0
	ldr r0, =0x03006C34
	add r2, r0, #0
	ldr r0, [r0]
	b _0803571A
	.align 2, 0
.pool
_0803570C:
	strh r4, [r3]
	sub r3, #2
	lsr r4, r4, #1
	add r0, r1, #1
	lsl r0, r0, #0x18
	lsr r1, r0, #0x18
	ldr r0, [r2]
_0803571A:
	ldrb r0, [r0, #8]
	cmp r1, r0
	blo _0803570C
	mov r0, #0
	strh r0, [r3]
	sub r3, #2
	mov r0, #1
	strh r0, [r3]
	mov r1, #0xd0
	lsl r1, r1, #0x14
	ldr r0, =0x03006C34
	ldr r0, [r0]
	ldrb r2, [r0, #8]
	add r2, #0x43
	mov r0, sp
	bl unused_noIrqDMA
	ldr r0, =dword_8337610
	bl eepromStartTimer
	mov r4, #0
	mov r1, #0xd0
	lsl r1, r1, #0x14
	mov r3, #1
	ldr r2, =0x03002068
_0803574C:
	ldrh r0, [r1]
	and r0, r3
	cmp r0, #0
	bne _08035766
	ldrb r0, [r2]
	cmp r0, #0
	beq _0803574C
	ldrh r0, [r1]
	mov r1, #1
	and r0, r1
	cmp r0, #0
	bne _08035766
	ldr r4, =0x0000C001
_08035766:
	bl disableGlobalTimer
	add r0, r4, #0
_0803576C:
	add sp, #0xa4
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global EepromVerifyWrite
EepromVerifyWrite: @ 0x08035784
	push {r4, r5, lr}
	sub sp, #8
	add r4, r1, #0
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	mov r5, #0
	ldr r0, =0x03006C34
	ldr r0, [r0]
	ldrh r0, [r0, #4]
	cmp r1, r0
	blo _080357A8
	ldr r0, =0x000080FF
	b _080357D2
	.align 2, 0
.pool
_080357A8:
	add r0, r1, #0
	mov r1, sp
	bl read64bitEeprom
	mov r2, sp
	mov r3, #0
	b _080357C0
_080357B6:
	add r0, r3, #1
	lsl r0, r0, #0x18
	lsr r3, r0, #0x18
	cmp r3, #3
	bhi _080357D0
_080357C0:
	ldrh r1, [r4]
	ldrh r0, [r2]
	add r2, #2
	add r4, #2
	cmp r1, r0
	beq _080357B6
	mov r5, #0x80
	lsl r5, r5, #8
_080357D0:
	add r0, r5, #0
_080357D2:
	add sp, #8
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global writeEepromVerified
writeEepromVerified: @ 0x080357DC
	push {r4, r5, r6, lr}
	add r5, r1, #0
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	mov r6, #0
	b _080357EE
_080357E8:
	add r0, r6, #1
	lsl r0, r0, #0x18
	lsr r6, r0, #0x18
_080357EE:
	cmp r6, #2
	bhi _08035812
	add r0, r4, #0
	add r1, r5, #0
	bl write64bitEeprom
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	cmp r2, #0
	bne _080357E8
	add r0, r4, #0
	add r1, r5, #0
	bl EepromVerifyWrite
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	cmp r2, #0
	bne _080357E8
_08035812:
	add r0, r2, #0
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
