.include "asm/macros.inc"

@completely disassembled


thumb_func_global cb_loadContinueScreen
cb_loadContinueScreen: @ 0x0802EFE0
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r1, =0x03000582
	mov r0, #0
	strh r0, [r1]
	ldr r0, =(cb_continueScreen+1)
	bl setCurrentTaskFunc
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global cb_continueScreen
cb_continueScreen: @ 0x0802F01C
	push {r4, r5, r6, r7, lr}
	ldr r1, =continueStates
	ldr r4, =0x03000582
	ldrh r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl call_r0
	lsl r0, r0, #0x10
	asr r1, r0, #0x10
	cmp r1, #1
	beq _0802F056
	cmp r1, #1
	bgt _0802F04C
	mov r0, #1
	neg r0, r0
	cmp r1, r0
	beq _0802F05E
	b _0802F132
	.align 2, 0
.pool
_0802F04C:
	cmp r1, #2
	beq _0802F074
	cmp r1, #3
	beq _0802F104
	b _0802F132
_0802F056:
	ldrh r0, [r4]
	add r0, #1
	strh r0, [r4]
	b _0802F132
_0802F05E:
	ldr r0, =0x03003D40
	mov r1, #0
	strb r1, [r0, #0x10]
	ldr r0, =(cb_loadMenu+1)
	bl setCurrentTaskFunc
	b _0802F132
	.align 2, 0
.pool
_0802F074:
	bl setPlayerStats_andtuff_8031B14
	ldr r0, =0x03003D40
	ldr r2, =0x03003C00
	ldrb r1, [r2]
	lsl r1, r1, #0x1a
	lsr r1, r1, #0x1a
	strb r1, [r0, #8]
	strb r1, [r0, #9]
	mov r3, #0
	add r7, r0, #0
	add r7, #0xa
	add r4, r2, #5
	add r5, r0, #0
	add r6, r2, #0
_0802F092:
	add r1, r3, r7
	add r0, r3, r4
	ldrb r0, [r0]
	strb r0, [r1]
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, #5
	bls _0802F092
	ldrh r0, [r6]
	lsl r0, r0, #0x16
	lsr r0, r0, #0x1d
	cmp r0, #5
	bne _0802F0C8
	mov r0, #4
	strb r0, [r5, #1]
	ldr r0, =stageCount
	ldrb r0, [r0, #8]
	sub r0, #1
	b _0802F0D0
	.align 2, 0
.pool
_0802F0C8:
	strb r0, [r5, #1]
	ldrb r0, [r6, #1]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1e
_0802F0D0:
	strb r0, [r5, #2]
	ldr r0, [r6]
	lsl r0, r0, #0xd
	lsr r0, r0, #0x19
	bl sub_0803242C
	ldrh r0, [r6, #2]
	lsl r0, r0, #0x16
	lsr r0, r0, #0x19
	bl sub_08032444
	ldr r0, =0x03003D40
	mov r1, #0
	strb r1, [r0, #0x10]
	mov r2, #0
	strh r1, [r0, #6]
	strb r2, [r0, #3]
	strb r2, [r0, #4]
	ldr r0, =(cb_moveToNextStage+1)
	bl setCurrentTaskFunc
	b _0802F132
	.align 2, 0
.pool
_0802F104:
	mov r3, #0
	ldr r5, =0x03003D40
	ldr r6, =0x03003C00
	ldr r7, =(cb_loadLevelSelect+1)
	add r4, r5, #0
	add r4, #0xa
	add r2, r6, #5
_0802F112:
	add r1, r3, r4
	add r0, r3, r2
	ldrb r0, [r0]
	strb r0, [r1]
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, #5
	bls _0802F112
	ldrb r0, [r6]
	lsl r0, r0, #0x1a
	lsr r0, r0, #0x1a
	strb r0, [r5, #9]
	add r0, r7, #0
	bl setCurrentTaskFunc
_0802F132:
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global continueState0
continueState0: @ 0x0802F144
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x14
	ldr r4, =0x03003D40
	ldrb r7, [r4, #1]
	ldrb r0, [r4, #2]
	mov sl, r0
	ldr r3, =0x03003C00
	ldrh r5, [r3]
	lsl r0, r5, #0x16
	lsr r2, r0, #0x1d
	cmp r2, #4
	bne _0802F174
	ldrb r0, [r3, #1]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1e
	ldr r1, =stageCount
	ldrh r1, [r1, #8]
	sub r1, #1
	cmp r0, r1
	beq _0802F178
_0802F174:
	cmp r2, #5
	bne _0802F188
_0802F178:
	mov r0, #2
	b _0802F478
	.align 2, 0
.pool
_0802F188:
	ldrb r0, [r3, #1]
	lsl r0, r0, #0x1c
	lsr r3, r0, #0x1e
	ldr r1, =stageCount
	lsl r0, r2, #1
	add r0, r0, r1
	ldrh r0, [r0]
	sub r0, #1
	cmp r3, r0
	bne _0802F1A4
	mov r0, #3
	b _0802F478
	.align 2, 0
.pool
_0802F1A4:
	mov r0, #0xf8
	lsl r0, r0, #4
	and r0, r5
	mov r1, #0xa0
	lsl r1, r1, #3
	cmp r0, r1
	bne _0802F1B8
	mov r0, #2
	strb r0, [r4, #1]
	b _0802F1BC
_0802F1B8:
	strb r2, [r4, #1]
	add r0, r3, #1
_0802F1BC:
	strb r0, [r4, #2]
	ldr r4, =scrollIntroStructs
	bl getLevelIntroId
	lsl r0, r0, #0x10
	lsr r0, r0, #0xe
	add r0, r0, r4
	ldr r4, [r0]
	ldr r5, =continueScrollIntroImgs
	bl getLevelIntroId
	lsl r0, r0, #0x10
	lsr r0, r0, #0xf
	add r0, r0, r5
	ldrh r6, [r0]
	add r0, sp, #0x10
	mov r5, #0
	strh r5, [r0]
	ldr r1, =0x06008000
	ldr r2, =0x01000010
	bl Bios_memcpy
	mov r0, sp
	add r0, #0x12
	strh r5, [r0]
	ldr r5, =0x0600E800
	ldr r2, =0x01000400
	add r1, r5, #0
	bl Bios_memcpy
	ldr r0, [r4, #0x20]
	mov r2, #0xa0
	lsl r2, r2, #0x13
	mov r1, #3
	bl memcpy_pal
	lsl r6, r6, #2
	add r0, r4, #0
	add r0, #8
	add r0, r0, r6
	ldr r0, [r0]
	ldr r1, =0x06008020
	bl Bios_LZ77_16_2
	add r4, #0x28
	add r4, r4, r6
	ldr r0, [r4]
	mov r1, #0
	mov sb, r1
	str r1, [sp]
	add r1, r5, #0
	mov r2, #0
	mov r3, #1
	bl LZ77_mapCpy
	ldr r5, =0x03003D00
	ldr r0, =0x00001D0A
	strh r0, [r5, #2]
	ldr r4, =0x03003CD4
	ldrh r0, [r4]
	mov r2, #0x80
	lsl r2, r2, #2
	add r1, r2, #0
	orr r0, r1
	strh r0, [r4]
	ldr r1, =0x03002308
	mov r0, #0x30
	strh r0, [r1, #2]
	ldr r1, =0x030022F8
	mov r0, #0x2c
	strh r0, [r1, #2]
	ldr r0, =continueLinesPal
	ldr r6, =0x05000060
	mov r1, #1
	add r2, r6, #0
	bl memcpy_pal
	ldr r0, =continueLinesTiles
	mov r1, #0xc0
	lsl r1, r1, #0x13
	bl Bios_LZ77_16_2
	ldr r0, =continueLinesMap
	ldr r1, =0x0600F000
	mov r2, #3
	str r2, [sp]
	mov r2, #0
	mov r3, #0
	bl LZ77_mapCpy
	ldr r0, =0x00001E03
	strh r0, [r5, #4]
	ldrh r0, [r4]
	mov r2, #0x80
	lsl r2, r2, #3
	add r1, r2, #0
	orr r0, r1
	strh r0, [r4]
	ldr r0, =menuBgPal
	ldr r2, =0x05000080
	mov r1, #1
	bl memcpy_pal
	ldr r0, =menuBgTilesCompressed
	ldr r1, =0x06000140
	bl Bios_LZ77_16_2
	ldr r0, =menuBgMapCompressed
	ldr r1, =0x0600F800
	mov r2, #4
	str r2, [sp]
	mov r2, #0
	mov r3, #0xa
	bl LZ77_mapCpy
	ldr r0, =0x00001F03
	strh r0, [r5, #6]
	ldrh r0, [r4]
	mov r2, #0x80
	lsl r2, r2, #4
	add r1, r2, #0
	orr r0, r1
	strh r0, [r4]
	ldr r0, =off_820E744
	add r1, r6, #0
	mov r2, #0
	bl startPalAnimation
	ldr r0, =0x03000590
	mov r1, #0
	bl allocFont
	ldr r0, =0x03000598
	mov r1, #1
	bl allocFont
	ldr r0, =0x030005A0
	mov r1, #2
	bl allocFont
	bl drawContinueMissionStr
	mov r0, #0x21
	mov r1, #8
	bl setContinueMissionStrPos
	bl drawLevelNameString
	mov r0, #0x30
	mov r1, #0x20
	bl setLevelNameStrPos
	bl DrawOkString
	ldr r1, =unk_820E7F4
	ldr r4, =0x03002080
	add r4, #0x2c
	ldrb r0, [r4]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	add r0, #0xd
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x93
	bl setOkStringPos
	bl drawBackString
	ldr r1, =unk_820e7fa
	ldrb r0, [r4]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	add r0, #0xd
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x93
	bl setBackStrPos
	bl drawStageString
	bl sub_0802F6F4
	add r0, #0xc
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x20
	bl setStageStringPos
	mov r0, #0xb
	bl allocateSpriteTiles
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	ldr r0, =score_numberTiles
	lsl r1, r4, #5
	ldr r2, =0x06010000
	add r1, r1, r2
	bl Bios_LZ77_16_2
	ldr r0, =dword_8208DA0
	mov r1, #1
	bl LoadSpritePalette
	add r5, r0, #0
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	bl sub_0802F874
	add r1, r0, #0
	add r1, #2
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	ldr r0, =0x03003D40
	mov r8, r0
	ldrb r3, [r0, #2]
	add r3, r4, r3
	add r3, #1
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	str r5, [sp]
	mov r0, #0x80
	lsl r0, r0, #2
	str r0, [sp, #4]
	ldr r0, =byte_8209AC8
	ldrb r0, [r0]
	str r0, [sp, #8]
	mov r2, sb
	str r2, [sp, #0xc]
	mov r0, #0x2e
	mov r2, #0x20
	bl createSimpleSprite
	ldr r0, =0x03003C00
	add r0, #0xcc
	ldrh r1, [r0]
	mov r0, #0x64
	mul r0, r1, r0
	ldr r1, =0x030005B8
	mov r2, #0
	bl createScoreString
	ldr r0, =dword_820E800
	mov r1, sb
	str r1, [sp]
	mov r1, #0
	add r2, r4, #0
	mov r3, #2
	bl allocSprite
	ldr r6, =0x030005B0
	str r0, [r6]
	ldr r4, =0x030037D0
	ldr r2, =0x000002E2
	add r4, r4, r2
	ldrh r1, [r4]
	lsl r1, r1, #0x17
	lsr r1, r1, #0x17
	add r1, #0x14
	strh r1, [r0, #0x10]
	mov r1, #0x20
	strh r1, [r0, #0x12]
	add r0, #0x22
	strb r5, [r0]
	bl drawPtsString
	ldrh r0, [r4]
	lsl r0, r0, #0x17
	lsr r0, r0, #0x17
	ldr r1, [r6]
	ldr r1, [r1, #4]
	ldr r1, [r1]
	ldrh r1, [r1]
	lsl r1, r1, #3
	add r0, r0, r1
	add r0, #0x16
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x20
	bl setPtsStringPos
	bl sub_0802F908
	add r1, r0, #0
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	mov r0, #0x90
	lsl r0, r0, #1
	sub r0, r0, r1
	mov r1, #2
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x20
	bl setLevelNameStrPos
	bl sub_0802F6F4
	add r0, #0xc
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x20
	bl setStageStringPos
	bl sub_0802F874
	add r0, #2
	ldr r2, =0x000001FF
	add r1, r2, #0
	and r0, r1
	ldrh r2, [r4]
	ldr r1, =0xFFFFFE00
	and r1, r2
	orr r1, r0
	strh r1, [r4]
	ldr r3, [r6]
	lsl r1, r1, #0x17
	lsr r1, r1, #0x17
	add r1, #0x14
	ldr r5, [r3, #4]
	ldr r0, [r5]
	ldrh r2, [r0]
	mov r0, #7
	sub r0, r0, r2
	lsl r0, r0, #3
	sub r1, r1, r0
	strh r1, [r3, #0x10]
	ldrh r0, [r4]
	lsl r0, r0, #0x17
	lsr r0, r0, #0x17
	ldr r1, [r5]
	ldrh r1, [r1]
	lsl r1, r1, #3
	add r0, r0, r1
	add r0, #0x16
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x20
	bl setPtsStringPos
	bl continue_createABSprites
	mov r0, r8
	strb r7, [r0, #1]
	mov r1, sl
	strb r1, [r0, #2]
	mov r0, #1
_0802F478:
	add sp, #0x14
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global continueState1
continueState1: @ 0x0802F53C
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0802F552
	mov r0, #0
	b _0802F554
_0802F552:
	mov r0, #1
_0802F554:
	pop {r1}
	bx r1

thumb_func_global continueState2
continueState2: @ 0x0802F558
	push {r4, lr}
	ldr r0, =0x03002080
	ldrh r1, [r0, #0xc]
	mov r4, #2
	add r0, r4, #0
	and r0, r1
	cmp r0, #0
	beq _0802F584
	mov r0, #0xcd
	bl playSong
	ldr r1, =0x03000588
	mov r2, #1
	neg r2, r2
	add r0, r2, #0
	strh r0, [r1]
	b _0802F59A
	.align 2, 0
.pool
_0802F584:
	mov r0, #9
	and r0, r1
	cmp r0, #0
	bne _0802F590
	mov r0, #0
	b _0802F59C
_0802F590:
	mov r0, #0xcc
	bl playSong
	ldr r0, =0x03000588
	strh r4, [r0]
_0802F59A:
	mov r0, #1
_0802F59C:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global continueState3
continueState3: @ 0x0802F5A8
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0802F5C0
	mov r0, #0
	b _0802F5C2
_0802F5C0:
	mov r0, #1
_0802F5C2:
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global continueState4
continueState4: @ 0x0802F5C8
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	bl FreeAllPalFadeTasks
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r0, =0x03000588
	mov r1, #0
	ldrsh r0, [r0, r1]
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global drawContinueMissionStr
drawContinueMissionStr: @ 0x0802F600
	push {lr}
	sub sp, #4
	ldr r1, =off_820E7BC
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000590
	mov r2, #0
	str r2, [sp]
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global setContinueMissionStrPos
setContinueMissionStrPos: @ 0x0802F630
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =off_820E7BC
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000590
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global drawLevelNameString
drawLevelNameString: @ 0x0802F66C
	push {lr}
	sub sp, #4
	ldr r1, =string_levelNames
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03003D40
	ldrb r1, [r1, #1]
	lsl r1, r1, #5
	add r0, r0, r1
	ldr r1, =0x03000598
	mov r2, #0
	str r2, [sp]
	mov r2, #0xf
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global setLevelNameStrPos
setLevelNameStrPos: @ 0x0802F6AC
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =string_levelNames
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03003D40
	ldrb r1, [r1, #1]
	lsl r1, r1, #5
	add r0, r0, r1
	ldr r1, =0x03000598
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0xf
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802F6F4
sub_0802F6F4: @ 0x0802F6F4
	push {lr}
	ldr r1, =string_levelNames
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03003D40
	ldrb r1, [r1, #1]
	lsl r1, r1, #5
	add r0, r0, r1
	mov r1, #0xf
	bl str_get_last_width_OamX_sum
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global DrawOkString
DrawOkString: @ 0x0802F724
	push {lr}
	sub sp, #4
	ldr r1, =string_ok
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005A0
	mov r2, #0
	str r2, [sp]
	mov r2, #0x1f
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global setOkStringPos
setOkStringPos: @ 0x0802F758
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =string_ok
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005A0
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0x1f
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global drawBackString
drawBackString: @ 0x0802F794
	push {lr}
	sub sp, #4
	ldr r1, =string_back
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005A0
	mov r2, #0
	str r2, [sp]
	mov r2, #0x25
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global setBackStrPos
setBackStrPos: @ 0x0802F7C8
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =string_back
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005A0
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0x25
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global drawStageString
drawStageString: @ 0x0802F804
	push {lr}
	sub sp, #4
	ldr r1, =stageStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000598
	mov r2, #0
	str r2, [sp]
	mov r2, #0x29
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global setStageStringPos
setStageStringPos: @ 0x0802F838
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =stageStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000598
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0x29
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802F874
sub_0802F874: @ 0x0802F874
	push {lr}
	ldr r1, =stageStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0x29
	bl str_get_last_width_OamX_sum
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global drawPtsString
drawPtsString: @ 0x0802F898
	push {lr}
	sub sp, #4
	ldr r1, =pts_strings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000598
	mov r2, #0
	str r2, [sp]
	mov r2, #0x2f
	mov r3, #2
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global setPtsStringPos
setPtsStringPos: @ 0x0802F8CC
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =pts_strings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000598
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0x2f
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802F908
sub_0802F908: @ 0x0802F908
	push {lr}
	ldr r1, =pts_strings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0x2f
	bl str_get_last_width_OamX_sum
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global continue_createABSprites
continue_createABSprites: @ 0x0802F92C
	push {r4, r5, r6, lr}
	mov r6, sl
	mov r5, sb
	mov r4, r8
	push {r4, r5, r6}
	sub sp, #4
	ldr r0, =dword_820918C
	mov r1, #1
	bl LoadSpritePalette
	mov r8, r0
	mov r0, r8
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r8, r0
	mov r0, #0xc
	bl allocateSpriteTiles
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	ldr r0, =buttonTiles
	lsl r1, r4, #5
	ldr r2, =0x06010000
	add r1, r1, r2
	bl Bios_LZ77_16_2
	ldr r0, =off_82093A4
	mov r1, #0
	mov sl, r1
	str r1, [sp]
	add r2, r4, #0
	mov r3, #1
	bl allocSprite
	ldr r2, =0x030005A8
	str r0, [r2]
	add r0, #0x23
	ldrb r3, [r0]
	mov r5, #0x11
	neg r5, r5
	add r1, r5, #0
	and r1, r3
	strb r1, [r0]
	ldr r0, [r2]
	add r0, #0x22
	mov r1, r8
	strb r1, [r0]
	ldr r2, [r2]
	ldr r1, =unk_820E7F4
	ldr r6, =0x03002080
	add r6, #0x2c
	ldrb r0, [r6]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	strh r0, [r2, #0x10]
	mov r0, #0x92
	mov sb, r0
	mov r1, sb
	strh r1, [r2, #0x12]
	ldr r0, =off_82093BC
	mov r2, sl
	str r2, [sp]
	mov r1, #0
	add r2, r4, #0
	mov r3, #1
	bl allocSprite
	ldr r2, =0x030005AC
	str r0, [r2]
	add r0, #0x23
	ldrb r1, [r0]
	and r5, r1
	strb r5, [r0]
	ldr r0, [r2]
	add r0, #0x22
	mov r1, r8
	strb r1, [r0]
	ldr r2, [r2]
	ldr r1, =unk_820e7fa
	ldrb r0, [r6]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	strh r0, [r2, #0x10]
	mov r0, sb
	strh r0, [r2, #0x12]
	add sp, #4
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool
