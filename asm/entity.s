.include "asm/macros.inc"


thumb_func_global clearObjects_maybe
clearObjects_maybe: @ 0x0801E6DC
	push {lr}
	sub sp, #4
	ldr r0, =0x03006770
	mov r1, #0
	bl sub_0801E73C
	ldr r0, =0x03004EE0
	mov r1, #1
	bl sub_0801E73C
	ldr r0, =0x03005A40
	mov r1, #2
	bl sub_0801E73C
	ldr r0, =0x030063D0
	mov r1, #3
	bl sub_0801E73C
	ldr r0, =0x03004880
	mov r1, #4
	bl sub_0801E73C
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r1, =0x03006BF8
	ldr r2, =0x01000003
	mov r0, sp
	bl Bios_memcpy
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0801E73C
sub_0801E73C: @ 0x0801E73C
	push {r4, r5, r6, lr}
	sub sp, #4
	add r4, r0, #0
	lsl r1, r1, #0x10
	add r6, r4, #0
	add r6, #0x74
	mov r5, #0
	str r5, [sp]
	ldr r0, =word_81423D4
	lsr r1, r1, #0xf
	add r1, r1, r0
	ldrh r0, [r1]
	lsl r2, r0, #3
	sub r2, r2, r0
	lsl r2, r2, #2
	add r2, r2, r0
	ldr r0, =0x001FFFFF
	and r2, r0
	mov r0, #0xa0
	lsl r0, r0, #0x13
	orr r2, r0
	mov r0, sp
	add r1, r4, #0
	bl Bios_memcpy
	add r2, r4, #0
	add r2, #0x67
	ldrb r0, [r2]
	mov r1, #0x10
	orr r0, r1
	strb r0, [r2]
	str r5, [r4]
	str r6, [r4, #4]
	add r2, #0x74
	ldrb r0, [r2]
	orr r0, r1
	strb r0, [r2]
	str r4, [r4, #0x74]
	str r5, [r6, #4]
	add sp, #4
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global createEntity
createEntity: @ 0x0801E79C
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x28
	mov sl, r0
	ldr r0, [sp, #0x48]
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov sb, r1
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	str r2, [sp, #0x1c]
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	str r3, [sp, #0x20]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	str r0, [sp, #0x24]
	ldr r1, =entityTemplates
	lsl r0, r2, #2
	add r0, r0, r1
	ldr r6, [r0]
	ldr r1, =unk_81423de
	add r0, sp, #4
	mov r2, #0xa
	bl memcpy
	add r7, sp, #0x10
	ldr r1, =dword_81423E8
	add r0, r7, #0
	mov r2, #5
	bl memcpy
	mov r0, sl
	mov r1, sb
	bl findFreeObject
	add r4, r0, #0
	cmp r4, #0
	bne _0801E7F2
	b _0801E99E
_0801E7F2:
	add r0, sp, #0x18
	mov r1, #0
	strh r1, [r0]
	ldr r2, =0x0100003A
	add r1, r4, #0
	bl Bios_memcpy
	ldr r0, =(task_runObjectCode+1)
	mov r2, sb
	lsl r1, r2, #1
	add r1, sp
	add r1, #4
	ldrb r1, [r1]
	bl createTask
	add r5, r0, #0
	cmp r5, #0
	bne _0801E818
	b _0801E99E
_0801E818:
	str r4, [r5, #0x14]
	ldrh r2, [r6, #0x1a]
	mov r3, sb
	add r0, r7, r3
	ldrb r0, [r0]
	str r0, [sp]
	mov r0, #0
	mov r1, #0
	mov r3, #2
	bl createSprite
	add r7, r0, #0
	cmp r7, #0
	bne _0801E850
	add r0, r5, #0
	bl endTask
	b _0801E99E
	.align 2, 0
.pool
_0801E850:
	ldr r0, [r6, #0x14]
	cmp r0, #0
	beq _0801E872
	ldrb r1, [r6, #0x18]
	bl LoadSpritePalette
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	cmp r0, #0
	bge _0801E874
	add r0, r5, #0
	bl endTask
	add r0, r7, #0
	bl freeSprite
	b _0801E99E
_0801E872:
	ldr r1, =0x0000FFFF
_0801E874:
	mov r0, #0x22
	add r0, r0, r7
	mov r8, r0
	strb r1, [r0]
	add r2, r7, #0
	add r2, #0x23
	ldrb r0, [r2]
	mov r1, #2
	orr r0, r1
	mov r1, #8
	orr r0, r1
	strb r0, [r2]
	add r0, r7, #0
	bl initPlayerInit
	str r0, [r4, #0x3c]
	cmp r0, #0
	beq _0801E988
	add r2, r4, #0
	add r2, #0x67
	ldrb r0, [r2]
	mov r1, #0x10
	orr r0, r1
	strb r0, [r2]
	mov r2, sl
	ldr r1, [r2, #0x74]
	str r1, [r4]
	mov r0, sl
	add r0, #0x74
	str r0, [r4, #4]
	str r4, [r1, #4]
	ldr r0, [r4, #4]
	str r4, [r0]
	str r5, [r4, #0xc]
	add r0, r4, #0
	add r0, #0x5a
	mov r3, sp
	ldrh r3, [r3, #0x1c]
	strh r3, [r0]
	str r6, [r4, #8]
	ldr r0, [r6]
	str r0, [r4, #0x20]
	mov r0, sp
	ldrh r0, [r0, #0x20]
	strh r0, [r4, #0x14]
	mov r1, sp
	ldrh r1, [r1, #0x24]
	strh r1, [r4, #0x16]
	bl sub_0801E6C8
	add r5, r4, #0
	add r5, #0x63
	strb r0, [r5]
	ldr r3, =byte_814295B
	ldr r2, =0x03003D40
	ldr r0, [r4, #8]
	ldrh r1, [r0, #0x1e]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r2, [r2, #0x11]
	add r0, r0, r2
	add r0, r0, r3
	ldrb r0, [r0]
	add r1, r4, #0
	add r1, #0x66
	strb r0, [r1]
	add r0, r4, #0
	add r0, #0x5c
	mov r2, sb
	strh r2, [r0]
	ldr r0, [r4, #8]
	ldr r0, [r0, #4]
	str r0, [r4, #0x10]
	ldr r0, [r4, #0x3c]
	ldr r3, [sp, #0x20]
	lsl r1, r3, #0x10
	asr r1, r1, #0x10
	ldr r3, [sp, #0x24]
	lsl r2, r3, #0x10
	asr r2, r2, #0x10
	bl cameraRelativeXY
	ldr r0, [r4, #0x3c]
	mov r1, #0
	str r1, [sp]
	mov r2, #0
	mov r3, #0
	bl setObjSize
	ldr r0, [r4, #0x3c]
	ldrb r1, [r6, #0x11]
	add r0, #0x4e
	strb r1, [r0]
	ldr r2, [r4, #0x3c]
	ldr r0, [r6, #8]
	ldr r1, [r6, #0xc]
	str r0, [r2, #0x38]
	str r1, [r2, #0x3c]
	ldr r2, [r4, #0x3c]
	mov r0, #0xe0
	lsl r0, r0, #3
	str r0, [r2, #0x28]
	str r0, [r2, #0x2c]
	ldrb r0, [r6, #0x10]
	add r2, #0x52
	mov r1, #1
	and r1, r0
	ldrb r3, [r2]
	mov r0, #2
	neg r0, r0
	and r0, r3
	orr r0, r1
	strb r0, [r2]
	add r0, r4, #0
	mov r1, #0
	bl setObjAnimation
	ldrb r0, [r5]
	mov r1, #0x1f
	bl modulo
	add r0, #1
	add r3, r7, #0
	add r3, #0x24
	lsl r0, r0, #3
	ldrb r2, [r3]
	mov r1, #7
	and r1, r2
	orr r1, r0
	strb r1, [r3]
	add r0, r4, #0
	b _0801E9A0
	.align 2, 0
.pool
_0801E988:
	add r0, r5, #0
	bl endTask
	add r0, r7, #0
	bl freeSprite
	mov r1, r8
	ldrb r0, [r1]
	ldrb r1, [r6, #0x18]
	bl clearSpritePalettes
_0801E99E:
	mov r0, #0
_0801E9A0:
	add sp, #0x28
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1

thumb_func_global createEntWithDirection
createEntWithDirection: @ 0x0801E9B0
	push {r4, r5, r6, lr}
	sub sp, #4
	add r4, r0, #0
	add r5, r1, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	lsl r3, r3, #0x18
	lsr r6, r3, #0x18
	ldr r0, =0x03004EE0
	str r2, [sp]
	mov r1, #1
	add r2, r4, #0
	add r3, r5, #0
	bl createEntity
	add r5, r0, #0
	cmp r5, #0
	beq _0801EA08
	add r1, r6, #0
	bl setObjDir_animFrame
	add r4, r5, #0
	add r4, #0x45
	ldrb r0, [r4]
	mov r1, #4
	orr r0, r1
	strb r0, [r4]
	add r0, r5, #0
	bl runEntityCallbacks
	ldrb r1, [r4]
	mov r0, #5
	neg r0, r0
	and r0, r1
	strb r0, [r4]
	add r0, r5, #0
	b _0801EA0A
	.align 2, 0
.pool
_0801EA08:
	mov r0, #0
_0801EA0A:
	add sp, #4
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global createObjWithDir_field60
createObjWithDir_field60: @ 0x0801EA14
	push {r4, r5, r6, r7, lr}
	sub sp, #4
	add r4, r0, #0
	add r5, r1, #0
	ldr r0, [sp, #0x18]
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	lsl r3, r3, #0x18
	lsr r6, r3, #0x18
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	ldr r0, =0x03004EE0
	str r2, [sp]
	mov r1, #1
	add r2, r4, #0
	add r3, r5, #0
	bl createEntity
	add r5, r0, #0
	cmp r5, #0
	beq _0801EA78
	add r1, r6, #0
	bl setObjDir_animFrame
	add r0, r5, #0
	add r0, #0x60
	strh r7, [r0]
	add r4, r5, #0
	add r4, #0x45
	ldrb r0, [r4]
	mov r1, #4
	orr r0, r1
	strb r0, [r4]
	add r0, r5, #0
	bl runEntityCallbacks
	ldrb r1, [r4]
	mov r0, #5
	neg r0, r0
	and r0, r1
	strb r0, [r4]
	add r0, r5, #0
	b _0801EA7A
	.align 2, 0
.pool
_0801EA78:
	mov r0, #0
_0801EA7A:
	add sp, #4
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global createPlayerAttackObj
createPlayerAttackObj: @ 0x0801EA84
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #4
	add r6, r0, #0
	add r5, r1, #0
	add r4, r2, #0
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	lsl r4, r4, #0x10
	lsl r3, r3, #0x10
	ldr r0, =0x03006770
	ldr r2, [r6]
	ldr r1, [r2, #4]
	asr r1, r1, #8
	lsr r7, r4, #0x10
	mov r8, r7
	asr r4, r4, #0x10
	add r4, r4, r1
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	ldr r1, [r2, #8]
	asr r1, r1, #8
	lsr r7, r3, #0x10
	asr r3, r3, #0x10
	add r3, r3, r1
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	str r3, [sp]
	mov r1, #0
	add r2, r5, #0
	add r3, r4, #0
	bl createEntity
	add r5, r0, #0
	cmp r5, #0
	bne _0801EAD8
	mov r0, #0
	b _0801EB36
	.align 2, 0
.pool
_0801EAD8:
	bl sub_0801E6A4
	add r1, r5, #0
	add r1, #0x63
	strb r0, [r1]
	mov r0, r8
	strh r0, [r5, #0x1c]
	strh r7, [r5, #0x1e]
	ldr r0, [r6]
	str r0, [r5, #0x40]
	ldr r1, [r5, #0x3c]
	ldr r0, [sp, #0x1c]
	str r0, [r1, #0x18]
	ldr r0, [sp, #0x20]
	str r0, [r1, #0x1c]
	ldrb r1, [r6, #0x16]
	add r0, r5, #0
	bl setObjDir_animFrame
	add r4, r5, #0
	add r4, #0x45
	ldrb r0, [r4]
	mov r1, #4
	orr r0, r1
	strb r0, [r4]
	add r0, r5, #0
	bl runEntityCallbacks
	ldrb r1, [r4]
	mov r0, #5
	neg r0, r0
	and r0, r1
	strb r0, [r4]
	ldr r0, [r5, #0x3c]
	ldr r0, [r0]
	add r2, r0, #0
	add r2, #0x22
	mov r1, #0
	ldrsb r1, [r2, r1]
	mov r0, #1
	neg r0, r0
	cmp r1, r0
	bne _0801EB34
	ldr r0, =0x03001E28
	ldrh r0, [r0]
	strb r0, [r2]
_0801EB34:
	add r0, r5, #0
_0801EB36:
	add sp, #4
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global createChildEntity
createChildEntity: @ 0x0801EB48
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #4
	add r6, r0, #0
	add r5, r1, #0
	add r4, r2, #0
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	lsl r4, r4, #0x10
	lsl r3, r3, #0x10
	ldr r0, =0x03005A40
	ldr r2, [r6, #0x3c]
	ldr r1, [r2, #4]
	asr r1, r1, #8
	lsr r7, r4, #0x10
	mov r8, r7
	asr r4, r4, #0x10
	add r4, r4, r1
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	ldr r1, [r2, #8]
	asr r1, r1, #8
	lsr r7, r3, #0x10
	asr r3, r3, #0x10
	add r3, r3, r1
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	str r3, [sp]
	mov r1, #2
	add r2, r5, #0
	add r3, r4, #0
	bl createEntity
	add r5, r0, #0
	cmp r5, #0
	bne _0801EB9C
	mov r0, #0
	b _0801EC00
	.align 2, 0
.pool
_0801EB9C:
	bl sub_0801E6C8
	add r1, r5, #0
	add r1, #0x63
	strb r0, [r1]
	mov r0, r8
	strh r0, [r5, #0x1c]
	strh r7, [r5, #0x1e]
	str r6, [r5, #0x40]
	ldr r1, [r5, #0x3c]
	ldr r0, [sp, #0x1c]
	str r0, [r1, #0x18]
	ldr r0, [sp, #0x20]
	str r0, [r1, #0x1c]
	add r0, r6, #0
	add r0, #0x67
	ldrb r1, [r0]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	add r0, r5, #0
	bl setObjDir_animFrame
	add r4, r5, #0
	add r4, #0x45
	ldrb r0, [r4]
	mov r1, #4
	orr r0, r1
	strb r0, [r4]
	add r0, r5, #0
	bl runEntityCallbacks
	ldrb r1, [r4]
	mov r0, #5
	neg r0, r0
	and r0, r1
	strb r0, [r4]
	ldr r0, [r5, #0x3c]
	ldr r0, [r0]
	add r2, r0, #0
	add r2, #0x22
	mov r1, #0
	ldrsb r1, [r2, r1]
	mov r0, #1
	neg r0, r0
	cmp r1, r0
	bne _0801EBFE
	ldr r0, =0x03001E28
	ldrh r0, [r0]
	strb r0, [r2]
_0801EBFE:
	add r0, r5, #0
_0801EC00:
	add sp, #4
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global createChildEnt_absPos
createChildEnt_absPos: @ 0x0801EC10
	push {r4, r5, r6, r7, lr}
	sub sp, #8
	ldr r6, [sp, #0x1c]
	ldr r7, [sp, #0x20]
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	ldr r5, [r0, #0x3c]
	ldr r4, [r5, #4]
	asr r4, r4, #8
	sub r2, r2, r4
	lsl r2, r2, #0x10
	asr r2, r2, #0x10
	ldr r4, [r5, #8]
	asr r4, r4, #8
	sub r3, r3, r4
	lsl r3, r3, #0x10
	asr r3, r3, #0x10
	str r6, [sp]
	str r7, [sp, #4]
	bl createChildEntity
	add sp, #8
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global createEntWithVel
createEntWithVel: @ 0x0801EC4C
	push {r4, r5, r6, lr}
	sub sp, #4
	add r4, r0, #0
	add r5, r1, #0
	add r6, r3, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r0, =0x03005A40
	str r2, [sp]
	mov r1, #2
	add r2, r4, #0
	add r3, r5, #0
	bl createEntity
	add r5, r0, #0
	cmp r5, #0
	bne _0801EC80
	mov r0, #0
	b _0801ECD2
	.align 2, 0
.pool
_0801EC80:
	bl sub_0801E6C8
	add r1, r5, #0
	add r1, #0x63
	strb r0, [r1]
	ldr r1, [r5, #0x3c]
	str r6, [r1, #0x18]
	ldr r0, [sp, #0x14]
	str r0, [r1, #0x1c]
	add r0, r5, #0
	mov r1, #1
	bl setObjDir_animFrame
	add r4, r5, #0
	add r4, #0x45
	ldrb r0, [r4]
	mov r1, #4
	orr r0, r1
	strb r0, [r4]
	add r0, r5, #0
	bl runEntityCallbacks
	ldrb r1, [r4]
	mov r0, #5
	neg r0, r0
	and r0, r1
	strb r0, [r4]
	ldr r0, [r5, #0x3c]
	ldr r0, [r0]
	add r2, r0, #0
	add r2, #0x22
	mov r1, #0
	ldrsb r1, [r2, r1]
	mov r0, #1
	neg r0, r0
	cmp r1, r0
	bne _0801ECD0
	ldr r0, =0x03001E28
	ldrh r0, [r0]
	strb r0, [r2]
_0801ECD0:
	add r0, r5, #0
_0801ECD2:
	add sp, #4
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global loadSpecialSpritePals
loadSpecialSpritePals: @ 0x0801ECE0
	push {r4, r5, r6, lr}
	ldr r0, =unk_82CCAB8
	mov r1, #1
	bl LoadSpritePalette
	add r1, r0, #0
	lsl r1, r1, #0x10
	ldr r0, =unk_82CE424
	lsr r4, r1, #0x10
	asr r1, r1, #0xb
	ldr r5, =0x05000200
	add r1, r1, r5
	mov r2, #0
	bl startPalAnimation
	ldr r0, =0x03001E24
	strh r4, [r0]
	ldr r0, =unk_82D0298
	mov r1, #1
	bl LoadSpritePalette
	add r4, r0, #0
	lsl r4, r4, #0x10
	ldr r0, =unk_82D1714
	lsr r6, r4, #0x10
	asr r4, r4, #0xb
	add r4, r4, r5
	add r1, r4, #0
	mov r2, #0
	bl startPalAnimation
	ldr r0, =unk_8332CD8
	add r1, r4, #0
	mov r2, #0
	bl startPalAnimation
	ldr r0, =0x03001E28
	strh r6, [r0]
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global createObj_801ED54
createObj_801ED54: @ 0x0801ED54
	push {r4, lr}
	sub sp, #4
	add r4, r0, #0
	add r3, r1, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r0, =0x030063D0
	str r2, [sp]
	mov r1, #3
	add r2, r4, #0
	bl createEntity
	add r2, r0, #0
	cmp r2, #0
	bne _0801ED84
	mov r0, #0
	b _0801EDA0
	.align 2, 0
.pool
_0801ED84:
	ldr r0, [r2, #0x3c]
	ldr r0, [r0]
	add r3, r0, #0
	add r3, #0x22
	mov r1, #0
	ldrsb r1, [r3, r1]
	mov r0, #1
	neg r0, r0
	cmp r1, r0
	bne _0801ED9E
	ldr r0, =0x03001E24
	ldrh r0, [r0]
	strb r0, [r3]
_0801ED9E:
	add r0, r2, #0
_0801EDA0:
	add sp, #4
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global createRunTimeMapObj
createRunTimeMapObj: @ 0x0801EDAC
	push {r4, r5, r6, r7, lr}
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	lsl r1, r1, #0x10
	lsr r6, r1, #0x10
	lsl r2, r2, #0x10
	lsr r5, r2, #0x10
	add r0, r7, #0
	add r1, r6, #0
	add r2, r5, #0
	bl createObj_801ED54
	add r4, r0, #0
	cmp r4, #0
	beq _0801EDE8
	bl findItemStorage
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	add r1, r0, #1
	add r2, r4, #0
	add r2, #0x5e
	strh r1, [r2]
	add r1, r7, #0
	add r2, r6, #0
	add r3, r5, #0
	bl storeRoomItem
	add r0, r4, #0
	b _0801EDEA
_0801EDE8:
	mov r0, #0
_0801EDEA:
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1

thumb_func_global grab_item_structured
grab_item_structured: @ 0x0801EDF0
	push {r4, lr}
	add r4, r0, #0
	add r0, #0x5a
	ldrh r0, [r0]
	sub r0, #0x77
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	bl giveItem
	add r0, r4, #0
	add r0, #0x5e
	ldrh r0, [r0]
	cmp r0, #0
	beq _0801EE18
	sub r0, #1
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	bl sub_08025D34
	b _0801EE22
_0801EE18:
	add r0, r4, #0
	add r0, #0x60
	ldrh r0, [r0]
	bl setEntDeletionFlag
_0801EE22:
	add r0, r4, #0
	bl destroyObject
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global ent_create_801EE30
ent_create_801EE30: @ 0x0801EE30
	push {r4, r5, lr}
	sub sp, #4
	add r4, r0, #0
	add r3, r1, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r0, =0x03004880
	str r2, [sp]
	mov r1, #4
	add r2, r4, #0
	bl createEntity
	add r5, r0, #0
	cmp r5, #0
	bne _0801EE60
	mov r0, #0
	b _0801EE98
	.align 2, 0
.pool
_0801EE60:
	ldr r0, [r5, #0x3c]
	ldr r0, [r0]
	add r2, r0, #0
	add r2, #0x22
	mov r1, #0
	ldrsb r1, [r2, r1]
	mov r0, #1
	neg r0, r0
	cmp r1, r0
	bne _0801EE7A
	ldr r0, =0x03001E28
	ldrh r0, [r0]
	strb r0, [r2]
_0801EE7A:
	add r4, r5, #0
	add r4, #0x45
	ldrb r0, [r4]
	mov r1, #4
	orr r0, r1
	strb r0, [r4]
	add r0, r5, #0
	bl runEntityCallbacks
	ldrb r1, [r4]
	mov r0, #5
	neg r0, r0
	and r0, r1
	strb r0, [r4]
	add r0, r5, #0
_0801EE98:
	add sp, #4
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global task_runObjectCode
task_runObjectCode: @ 0x0801EEA4
	push {lr}
	ldr r0, =0x0201AF00
	mov r1, #0xfa
	lsl r1, r1, #4
	add r0, r0, r1
	ldr r0, [r0]
	ldr r0, [r0, #0x14]
	bl runEntityCallbacks
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global runEntityCallbacks
runEntityCallbacks: @ 0x0801EEC0
	push {r4, r5, lr}
	add r4, r0, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0x20
	and r0, r1
	cmp r0, #0
	bne _0801EF66
	ldr r1, =entityCollisionFuncs
	add r0, r4, #0
	add r0, #0x5c
	ldrh r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r1, [r0]
	add r0, r4, #0
	bl call_r1
	ldr r0, [r4, #0x20]
	cmp r0, #0
	beq _0801EF18
	ldr r5, =ent_scriptcmds
	b _0801EEFC
	.align 2, 0
.pool
_0801EEF8:
	cmp r0, #2
	beq _0801EF66
_0801EEFC:
	ldr r0, [r4, #0x20]
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r5
	ldr r1, [r0]
	add r0, r4, #0
	bl call_r1
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0
	beq _0801EEFC
	cmp r0, #1
	bne _0801EEF8
_0801EF18:
	ldr r1, [r4, #0x10]
	cmp r1, #0
	beq _0801EF24
	add r0, r4, #0
	bl call_r1
_0801EF24:
	add r1, r4, #0
	add r1, #0x45
	ldrb r0, [r1]
	lsl r0, r0, #0x1e
	add r5, r1, #0
	cmp r0, #0
	blt _0801EF6E
	ldr r0, [r4, #0x3c]
	bl runObjPhysics
	add r0, r4, #0
	mov r1, #0x20
	bl sub_0801FC34
	cmp r0, #0
	beq _0801EF54
	ldr r0, [r4, #0x3c]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #8
	orr r0, r2
	strb r0, [r1]
	b _0801EF74
_0801EF54:
	ldr r0, [r4, #0x3c]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #9
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	b _0801EF74
_0801EF66:
	add r0, r4, #0
	bl destroyObject
	b _0801EF96
_0801EF6E:
	ldr r0, [r4, #0x3c]
	bl updateSpriteScreenPos
_0801EF74:
	ldrb r0, [r5]
	lsl r0, r0, #0x1d
	cmp r0, #0
	blt _0801EF84
	ldr r0, [r4, #0x3c]
	ldr r0, [r0]
	bl updateSpriteAnimation
_0801EF84:
	add r2, r4, #0
	add r2, #0x44
	ldrb r1, [r2]
	mov r0, #0x7f
	and r0, r1
	mov r1, #0x41
	neg r1, r1
	and r0, r1
	strb r0, [r2]
_0801EF96:
	pop {r4, r5}
	pop {r0}
	bx r0

thumb_func_global nullCollision
nullCollision: @ 0x0801EF9C
	bx lr
	.align 2, 0

thumb_func_global entity_detect_player_attacks
entity_detect_player_attacks: @ 0x0801EFA0
	push {r4, r5, r6, r7, lr}
	add r5, r0, #0
	add r4, r5, #0
	add r4, #0x44
	ldrb r0, [r4]
	lsl r0, r0, #0x1a
	cmp r0, #0
	blt _0801F010
	add r0, r5, #0
	bl getCollidingPlayerAttackEnt
	add r2, r0, #0
	cmp r2, #0
	beq _0801F010
	ldrb r0, [r4]
	mov r1, #0x80
	orr r0, r1
	strb r0, [r4]
	str r2, [r5, #0x48]
	add r0, r2, #0
	add r0, #0x63
	ldrb r1, [r0]
	add r0, r5, #0
	add r0, #0x64
	strb r1, [r0]
	add r4, r2, #0
	add r4, #0x44
	ldrb r1, [r4]
	mov r0, #0x40
	add r3, r1, #0
	orr r3, r0
	strb r3, [r4]
	ldr r7, =byte_81429F2
	ldr r0, =0x03003D40
	ldrb r6, [r0, #0x11]
	ldr r0, [r2, #8]
	ldrh r1, [r0, #0x24]
	lsl r0, r1, #1
	add r0, r0, r1
	add r0, r6, r0
	add r0, r0, r7
	ldrb r2, [r0]
	cmp r2, #0
	beq _0801F010
	ldr r0, [r5, #8]
	ldrh r1, [r0, #0x24]
	lsl r0, r1, #1
	add r0, r0, r1
	add r0, r6, r0
	add r0, r0, r7
	ldrb r0, [r0]
	cmp r2, r0
	bhi _0801F010
	mov r0, #0x10
	orr r3, r0
	strb r3, [r4]
_0801F010:
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_801F020
sub_801F020: @ 0x0801F020
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	add r7, r0, #0
	add r4, r7, #0
	add r4, #0x44
	ldrb r0, [r4]
	lsl r0, r0, #0x1a
	cmp r0, #0
	blt _0801F0CC
	add r0, r7, #0
	bl getCollidingPlayerAttackEnt
	add r6, r0, #0
	cmp r6, #0
	beq _0801F0CC
	str r6, [r7, #0x48]
	add r0, #0x63
	ldrb r1, [r0]
	add r0, r7, #0
	add r0, #0x64
	strb r1, [r0]
	ldrb r0, [r4]
	mov r1, #0x80
	mov sb, r1
	mov r2, sb
	orr r2, r0
	strb r2, [r4]
	ldr r0, =byte_81429F2
	mov ip, r0
	ldr r1, =0x03003D40
	mov r8, r1
	ldrb r5, [r1, #0x11]
	ldr r0, [r7, #8]
	ldrh r1, [r0, #0x24]
	lsl r0, r1, #1
	add r0, r0, r1
	add r0, r5, r0
	add r0, ip
	ldrb r3, [r0]
	cmp r3, #0
	beq _0801F0CC
	ldr r0, [r6, #8]
	ldrh r1, [r0, #0x24]
	lsl r0, r1, #1
	add r0, r0, r1
	add r0, r5, r0
	add r0, ip
	ldrb r0, [r0]
	cmp r0, #0
	beq _0801F0CC
	cmp r3, r0
	bhi _0801F096
	mov r0, #0x10
	orr r2, r0
	mov r0, #0x20
	orr r2, r0
	strb r2, [r4]
_0801F096:
	add r5, r6, #0
	add r5, #0x44
	ldrb r0, [r5]
	mov r4, sb
	orr r4, r0
	strb r4, [r5]
	mov r0, r8
	ldrb r3, [r0, #0x11]
	ldr r0, [r6, #8]
	ldrh r0, [r0, #0x24]
	lsl r1, r0, #1
	add r1, r1, r0
	add r1, r3, r1
	add r1, ip
	ldr r0, [r7, #8]
	ldrh r2, [r0, #0x24]
	lsl r0, r2, #1
	add r0, r0, r2
	add r3, r3, r0
	add r3, ip
	ldrb r0, [r1]
	ldrb r3, [r3]
	cmp r0, r3
	bhi _0801F0CC
	mov r0, #0x10
	orr r4, r0
	strb r4, [r5]
_0801F0CC:
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_801F0E0
sub_801F0E0: @ 0x0801F0E0
	push {r4, r5, r6, lr}
	add r5, r0, #0
	add r4, r5, #0
	add r4, #0x44
	ldrb r0, [r4]
	lsl r0, r0, #0x1a
	cmp r0, #0
	blt _0801F14A
	add r0, r5, #0
	bl getCollidingPlayerAttackEnt
	add r2, r0, #0
	cmp r2, #0
	beq _0801F14A
	ldrb r0, [r4]
	mov r1, #0x80
	orr r0, r1
	strb r0, [r4]
	str r2, [r5, #0x48]
	add r0, r2, #0
	add r0, #0x63
	ldrb r1, [r0]
	add r0, r5, #0
	add r0, #0x64
	strb r1, [r0]
	str r5, [r2, #0x48]
	ldr r6, =byte_81429F2
	ldr r0, =0x03003D40
	ldrb r4, [r0, #0x11]
	ldr r0, [r2, #8]
	ldrh r1, [r0, #0x24]
	lsl r0, r1, #1
	add r0, r0, r1
	add r0, r4, r0
	add r0, r0, r6
	ldrb r3, [r0]
	cmp r3, #0
	beq _0801F14A
	ldr r0, [r5, #8]
	ldrh r1, [r0, #0x24]
	lsl r0, r1, #1
	add r0, r0, r1
	add r0, r4, r0
	add r0, r0, r6
	ldrb r0, [r0]
	cmp r3, r0
	bhi _0801F14A
	add r0, r2, #0
	add r0, #0x44
	ldrb r1, [r0]
	mov r2, #0x10
	orr r1, r2
	strb r1, [r0]
_0801F14A:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global destroyObject
destroyObject: @ 0x0801F158
	push {r4, lr}
	add r4, r0, #0
	ldr r1, [r4]
	ldr r0, [r4, #4]
	str r0, [r1, #4]
	ldr r1, [r4, #4]
	ldr r0, [r4]
	str r0, [r1]
	add r2, r4, #0
	add r2, #0x67
	ldrb r1, [r2]
	mov r0, #0x11
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	mov r0, #0
	str r0, [r4, #4]
	str r0, [r4]
	ldr r0, [r4, #0x3c]
	bl clearObjField50
	ldr r0, [r4, #0x3c]
	ldr r0, [r0]
	bl freeSprite
	ldr r1, [r4, #8]
	ldr r0, [r1, #0x14]
	cmp r0, #0
	beq _0801F1A0
	ldr r0, [r4, #0x3c]
	ldr r0, [r0]
	add r0, #0x22
	ldrb r0, [r0]
	ldrb r1, [r1, #0x18]
	bl clearSpritePalettes
_0801F1A0:
	ldr r0, [r4, #0xc]
	bl endTask
	pop {r4}
	pop {r0}
	bx r0




thumb_func_global sub_801F1AC
sub_801F1AC: @ 0x0801F1AC
	push {lr}
	ldr r0, =0x03006770
	bl sub_801F1E4
	ldr r0, =0x03004EE0
	bl sub_801F1E4
	ldr r0, =0x03005A40
	bl sub_801F1E4
	ldr r0, =0x030063D0
	bl sub_801F1E4
	ldr r0, =0x03004880
	bl sub_801F1E4
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_801F1E4
sub_801F1E4: @ 0x0801F1E4
	push {r4, lr}
	ldr r2, [r0, #4]
	add r0, #0x74
	cmp r2, r0
	beq _0801F202
	mov r4, #0x20
	add r3, r0, #0
_0801F1F2:
	add r1, r2, #0
	add r1, #0x67
	ldrb r0, [r1]
	orr r0, r4
	strb r0, [r1]
	ldr r2, [r2, #4]
	cmp r2, r3
	bne _0801F1F2
_0801F202:
	pop {r4}
	pop {r0}
	bx r0


thumb_func_global findFreeObject
findFreeObject: @ 0x0801F208
	push {r4, r5, r6, r7, lr}
	add r5, r0, #0
	lsl r1, r1, #0x10
	mov r3, #2
	ldr r0, =word_81423D4
	lsr r1, r1, #0xf
	add r1, r1, r0
	ldrh r0, [r1]
	cmp r3, r0
	bhs _0801F24A
	mov r7, #0x74
	add r4, r0, #0
	mov r6, #0x10
_0801F222:
	add r0, r3, #0
	mul r0, r7, r0
	add r2, r0, r5
	add r0, r2, #0
	add r0, #0x67
	ldrb r1, [r0]
	add r0, r6, #0
	and r0, r1
	cmp r0, #0
	bne _0801F240
	add r0, r2, #0
	b _0801F24C
	.align 2, 0
.pool
_0801F240:
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, r4
	blo _0801F222
_0801F24A:
	mov r0, #0
_0801F24C:
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
	
