.include "asm/macros.inc"

@completely disassembled

thumb_func_global nullsub_13
nullsub_13: @ 0x080379CC
	bx lr
	.byte 0x00, 0x00

thumb_func_global call_r0
call_r0: @ 0x080379D0
	bx r0
	.byte 0xC0, 0x46

thumb_func_global call_r1
call_r1: @ 0x080379D4
	bx r1
	.byte 0xC0, 0x46

thumb_func_global call_r2
call_r2: @ 0x080379D8
	bx r2
	.byte 0xC0, 0x46

thumb_func_global call_r3
call_r3: @ 0x080379DC
	bx r3
	nop
call_r4:	
	bx r4
	nop
call_r5:
	bx r5
	nop
call_r6:
	bx r6
	nop
call_r7:
	bx r7
	nop
call_r8:
	bx r8
	nop
call_r9:
	bx r9
	nop
call_r10:	
	bx r10
	nop
call_r11:	
	bx r11
	nop
call_r12:	
	bx r12
	nop
call_sp:	
	bx sp
	nop
call_lr:	
	bx lr
	nop

thumb_func_global sub_08037A0C
sub_08037A0C: @ 0x08037A0C
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x34
	mov r4, #0
	str r4, [sp]
	add r5, r1, #0
	add r4, r0, #0
	cmp r5, #0
	bge _08037A3C
	ldr r0, [sp]
	mvn r0, r0
	str r0, [sp]
	neg r0, r4
	add r6, r0, #0
	neg r1, r5
	cmp r0, #0
	beq _08037A36
	sub r1, #1
_08037A36:
	add r7, r1, #0
	add r5, r7, #0
	add r4, r6, #0
_08037A3C:
	cmp r3, #0
	bge _08037A58
	ldr r1, [sp]
	mvn r1, r1
	str r1, [sp]
	neg r0, r2
	str r0, [sp, #4]
	neg r2, r3
	cmp r0, #0
	beq _08037A52
	sub r2, #1
_08037A52:
	str r2, [sp, #8]
	ldr r2, [sp, #4]
	ldr r3, [sp, #8]
_08037A58:
	add r7, r2, #0
	add r6, r3, #0
	mov sl, r4
	mov r8, r5
	cmp r6, #0
	beq _08037A66
	b _08037CB4
_08037A66:
	cmp r7, r8
	bls _08037B40
	ldr r0, =0x0000FFFF
	cmp r7, r0
	bhi _08037A80
	mov r1, #0
	cmp r7, #0xff
	bls _08037A8A
	mov r1, #8
	b _08037A8A
	.align 2, 0
.pool
_08037A80:
	ldr r0, =0x00FFFFFF
	mov r1, #0x18
	cmp r7, r0
	bhi _08037A8A
	mov r1, #0x10
_08037A8A:
	ldr r0, =unk_83B8264
	lsr r2, r1
	add r0, r2, r0
	ldrb r0, [r0]
	add r0, r0, r1
	mov r1, #0x20
	sub r2, r1, r0
	cmp r2, #0
	beq _08037AB2
	lsl r7, r2
	mov r3, r8
	lsl r3, r2
	sub r1, r1, r2
	mov r0, sl
	lsr r0, r1
	orr r3, r0
	mov r8, r3
	mov r4, sl
	lsl r4, r2
	mov sl, r4
_08037AB2:
	lsr r0, r7, #0x10
	mov sb, r0
	ldr r1, =0x0000FFFF
	and r1, r7
	str r1, [sp, #0xc]
	mov r0, r8
	mov r1, sb
	bl modulo
	add r4, r0, #0
	mov r0, r8
	mov r1, sb
	bl sub_08037FA8
	add r6, r0, #0
	ldr r3, [sp, #0xc]
	add r2, r6, #0
	mul r2, r3, r2
	lsl r4, r4, #0x10
	mov r1, sl
	lsr r0, r1, #0x10
	orr r4, r0
	cmp r4, r2
	bhs _08037AF2
	sub r6, #1
	add r4, r4, r7
	cmp r4, r7
	blo _08037AF2
	cmp r4, r2
	bhs _08037AF2
	sub r6, #1
	add r4, r4, r7
_08037AF2:
	sub r4, r4, r2
	add r0, r4, #0
	mov r1, sb
	bl modulo
	add r5, r0, #0
	add r0, r4, #0
	mov r1, sb
	bl sub_08037FA8
	add r1, r0, #0
	ldr r3, [sp, #0xc]
	add r2, r1, #0
	mul r2, r3, r2
	lsl r5, r5, #0x10
	ldr r0, =0x0000FFFF
	mov r4, sl
	and r4, r0
	orr r5, r4
	cmp r5, r2
	bhs _08037B2A
	sub r1, #1
	add r5, r5, r7
	cmp r5, r7
	blo _08037B2A
	cmp r5, r2
	bhs _08037B2A
	sub r1, #1
_08037B2A:
	lsl r6, r6, #0x10
	orr r6, r1
	mov r0, #0
	str r0, [sp, #0x10]
	b _08037E0A
	.align 2, 0
.pool
_08037B40:
	cmp r2, #0
	bne _08037B4E
	mov r0, #1
	mov r1, #0
	bl sub_08037FA8
	add r7, r0, #0
_08037B4E:
	add r1, r7, #0
	ldr r0, =0x0000FFFF
	cmp r7, r0
	bhi _08037B64
	mov r2, #0
	cmp r7, #0xff
	bls _08037B6E
	mov r2, #8
	b _08037B6E
	.align 2, 0
.pool
_08037B64:
	ldr r0, =0x00FFFFFF
	mov r2, #0x18
	cmp r7, r0
	bhi _08037B6E
	mov r2, #0x10
_08037B6E:
	ldr r0, =unk_83B8264
	lsr r1, r2
	add r0, r1, r0
	ldrb r0, [r0]
	add r0, r0, r2
	mov r1, #0x20
	sub r2, r1, r0
	cmp r2, #0
	bne _08037B94
	mov r1, r8
	sub r1, r1, r7
	mov r8, r1
	mov r2, #1
	str r2, [sp, #0x10]
	b _08037C32
	.align 2, 0
.pool
_08037B94:
	sub r1, r1, r2
	lsl r7, r2
	mov r5, r8
	lsr r5, r1
	mov r3, r8
	lsl r3, r2
	mov r0, sl
	lsr r0, r1
	orr r3, r0
	mov r8, r3
	mov r4, sl
	lsl r4, r2
	mov sl, r4
	lsr r0, r7, #0x10
	mov sb, r0
	ldr r1, =0x0000FFFF
	and r1, r7
	str r1, [sp, #0x14]
	add r0, r5, #0
	mov r1, sb
	bl modulo
	add r4, r0, #0
	add r0, r5, #0
	mov r1, sb
	bl sub_08037FA8
	add r6, r0, #0
	ldr r2, [sp, #0x14]
	add r1, r6, #0
	mul r1, r2, r1
	lsl r4, r4, #0x10
	mov r3, r8
	lsr r0, r3, #0x10
	orr r4, r0
	cmp r4, r1
	bhs _08037BEE
	sub r6, #1
	add r4, r4, r7
	cmp r4, r7
	blo _08037BEE
	cmp r4, r1
	bhs _08037BEE
	sub r6, #1
	add r4, r4, r7
_08037BEE:
	sub r4, r4, r1
	add r0, r4, #0
	mov r1, sb
	bl modulo
	add r5, r0, #0
	add r0, r4, #0
	mov r1, sb
	bl sub_08037FA8
	add r2, r0, #0
	ldr r4, [sp, #0x14]
	add r1, r2, #0
	mul r1, r4, r1
	lsl r5, r5, #0x10
	ldr r0, =0x0000FFFF
	mov r3, r8
	and r3, r0
	orr r5, r3
	cmp r5, r1
	bhs _08037C28
	sub r2, #1
	add r5, r5, r7
	cmp r5, r7
	blo _08037C28
	cmp r5, r1
	bhs _08037C28
	sub r2, #1
	add r5, r5, r7
_08037C28:
	lsl r6, r6, #0x10
	orr r6, r2
	str r6, [sp, #0x10]
	sub r1, r5, r1
	mov r8, r1
_08037C32:
	lsr r4, r7, #0x10
	mov sb, r4
	ldr r0, =0x0000FFFF
	and r0, r7
	str r0, [sp, #0x18]
	mov r0, r8
	mov r1, sb
	bl modulo
	add r4, r0, #0
	mov r0, r8
	mov r1, sb
	bl sub_08037FA8
	add r6, r0, #0
	ldr r1, [sp, #0x18]
	add r2, r6, #0
	mul r2, r1, r2
	lsl r4, r4, #0x10
	mov r3, sl
	lsr r0, r3, #0x10
	orr r4, r0
	cmp r4, r2
	bhs _08037C72
	sub r6, #1
	add r4, r4, r7
	cmp r4, r7
	blo _08037C72
	cmp r4, r2
	bhs _08037C72
	sub r6, #1
	add r4, r4, r7
_08037C72:
	sub r4, r4, r2
	add r0, r4, #0
	mov r1, sb
	bl modulo
	add r5, r0, #0
	add r0, r4, #0
	mov r1, sb
	bl sub_08037FA8
	add r1, r0, #0
	ldr r4, [sp, #0x18]
	add r2, r1, #0
	mul r2, r4, r2
	lsl r5, r5, #0x10
	ldr r0, =0x0000FFFF
	mov r3, sl
	and r3, r0
	orr r5, r3
	cmp r5, r2
	bhs _08037CAA
	sub r1, #1
	add r5, r5, r7
	cmp r5, r7
	blo _08037CAA
	cmp r5, r2
	bhs _08037CAA
	sub r1, #1
_08037CAA:
	lsl r6, r6, #0x10
	orr r6, r1
	b _08037E0A
	.align 2, 0
.pool
_08037CB4:
	cmp r6, r8
	bls _08037CC0
	mov r6, #0
	mov r4, #0
	str r4, [sp, #0x10]
	b _08037E0A
_08037CC0:
	add r1, r6, #0
	ldr r0, =0x0000FFFF
	cmp r6, r0
	bhi _08037CD8
	mov r2, #0
	cmp r6, #0xff
	bls _08037CE2
	mov r2, #8
	b _08037CE2
	.align 2, 0
.pool
_08037CD8:
	ldr r0, =0x00FFFFFF
	mov r2, #0x18
	cmp r6, r0
	bhi _08037CE2
	mov r2, #0x10
_08037CE2:
	ldr r0, =unk_83B8264
	lsr r1, r2
	add r0, r1, r0
	ldrb r0, [r0]
	add r0, r0, r2
	mov r1, #0x20
	sub r2, r1, r0
	cmp r2, #0
	bne _08037D10
	cmp r8, r6
	bhi _08037CFC
	cmp sl, r7
	blo _08037D0C
_08037CFC:
	mov r6, #1
	mov r1, sl
	b _08037E04
	.align 2, 0
.pool
_08037D0C:
	mov r6, #0
	b _08037E06
_08037D10:
	sub r1, r1, r2
	lsl r6, r2
	add r0, r7, #0
	lsr r0, r1
	orr r6, r0
	lsl r7, r2
	mov r5, r8
	lsr r5, r1
	mov r3, r8
	lsl r3, r2
	mov r0, sl
	lsr r0, r1
	orr r3, r0
	mov r8, r3
	mov r4, sl
	lsl r4, r2
	mov sl, r4
	lsr r0, r6, #0x10
	mov sb, r0
	ldr r1, =0x0000FFFF
	and r1, r6
	str r1, [sp, #0x1c]
	add r0, r5, #0
	mov r1, sb
	bl modulo
	add r4, r0, #0
	add r0, r5, #0
	mov r1, sb
	bl sub_08037FA8
	add r3, r0, #0
	ldr r2, [sp, #0x1c]
	add r1, r3, #0
	mul r1, r2, r1
	lsl r4, r4, #0x10
	mov r2, r8
	lsr r0, r2, #0x10
	orr r4, r0
	cmp r4, r1
	bhs _08037D72
	sub r3, #1
	add r4, r4, r6
	cmp r4, r6
	blo _08037D72
	cmp r4, r1
	bhs _08037D72
	sub r3, #1
	add r4, r4, r6
_08037D72:
	sub r4, r4, r1
	add r0, r4, #0
	mov r1, sb
	str r3, [sp, #0x30]
	bl modulo
	add r5, r0, #0
	add r0, r4, #0
	mov r1, sb
	bl sub_08037FA8
	add r2, r0, #0
	ldr r4, [sp, #0x1c]
	add r1, r2, #0
	mul r1, r4, r1
	lsl r5, r5, #0x10
	ldr r0, =0x0000FFFF
	mov r4, r8
	and r4, r0
	orr r5, r4
	ldr r3, [sp, #0x30]
	cmp r5, r1
	bhs _08037DB0
	sub r2, #1
	add r5, r5, r6
	cmp r5, r6
	blo _08037DB0
	cmp r5, r1
	bhs _08037DB0
	sub r2, #1
	add r5, r5, r6
_08037DB0:
	lsl r6, r3, #0x10
	orr r6, r2
	sub r1, r5, r1
	mov r8, r1
	ldr r0, =0x0000FFFF
	mov sb, r0
	add r1, r6, #0
	and r1, r0
	lsr r3, r6, #0x10
	add r0, r7, #0
	mov r2, sb
	and r0, r2
	lsr r2, r7, #0x10
	add r5, r1, #0
	mul r5, r0, r5
	add r4, r1, #0
	mul r4, r2, r4
	add r1, r3, #0
	mul r1, r0, r1
	mul r3, r2, r3
	lsr r0, r5, #0x10
	add r4, r4, r0
	add r4, r4, r1
	cmp r4, r1
	bhs _08037DE8
	mov r0, #0x80
	lsl r0, r0, #9
	add r3, r3, r0
_08037DE8:
	lsr r0, r4, #0x10
	add r3, r3, r0
	mov r1, sb
	and r4, r1
	lsl r0, r4, #0x10
	and r5, r1
	add r1, r0, r5
	cmp r3, r8
	bhi _08037E02
	cmp r3, r8
	bne _08037E06
	cmp r1, sl
	bls _08037E06
_08037E02:
	sub r6, #1
_08037E04:
	sub r0, r1, r7
_08037E06:
	mov r2, #0
	str r2, [sp, #0x10]
_08037E0A:
	str r6, [sp, #0x20]
	ldr r3, [sp, #0x10]
	str r3, [sp, #0x24]
	ldr r1, [sp, #0x20]
	ldr r2, [sp, #0x24]
	ldr r4, [sp]
	cmp r4, #0
	beq _08037E2C
	neg r0,r1
	str r0, [sp, #0x28]
	neg r1, r2
	cmp r0, #0
	beq _08037E26
	sub r1, #1
_08037E26:
	str r1, [sp, #0x2c]
	ldr r1, [sp, #0x28]
	ldr r2, [sp, #0x2c]
_08037E2C:
	add r0, r1, #0
	add r1, r2, #0
	add sp, #0x34
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7, pc}
	.align 2, 0
.pool

thumb_func_global divide
divide: @ 0x08037E40
	cmp r1, #0
	beq _08037EC8
	push {r4}
	add r4, r0, #0
	eor r4, r1
	mov ip, r4
	mov r3, #1
	mov r2, #0
	cmp r1, #0
	bpl _08037E56
	neg r1, r1
_08037E56:
	cmp r0, #0
	bpl _08037E5C
	neg r0, r0
_08037E5C:
	cmp r0, r1
	blo _08037EBA
	mov r4, #1
	lsl r4, r4, #0x1c
_08037E64:
	cmp r1, r4
	bhs _08037E72
	cmp r1, r0
	bhs _08037E72
	lsl r1, r1, #4
	lsl r3, r3, #4
	b _08037E64
_08037E72:
	lsl r4, r4, #3
_08037E74:
	cmp r1, r4
	bhs _08037E82
	cmp r1, r0
	bhs _08037E82
	lsl r1, r1, #1
	lsl r3, r3, #1
	b _08037E74
_08037E82:
	cmp r0, r1
	blo _08037E8A
	sub r0, r0, r1
	orr r2, r3
_08037E8A:
	lsr r4, r1, #1
	cmp r0, r4
	blo _08037E96
	sub r0, r0, r4
	lsr r4, r3, #1
	orr r2, r4
_08037E96:
	lsr r4, r1, #2
	cmp r0, r4
	blo _08037EA2
	sub r0, r0, r4
	lsr r4, r3, #2
	orr r2, r4
_08037EA2:
	lsr r4, r1, #3
	cmp r0, r4
	blo _08037EAE
	sub r0, r0, r4
	lsr r4, r3, #3
	orr r2, r4
_08037EAE:
	cmp r0, #0
	beq _08037EBA
	lsr r3, r3, #4
	beq _08037EBA
	lsr r1, r1, #4
	b _08037E82
_08037EBA:
	add r0, r2, #0
	mov r4, ip
	cmp r4, #0
	bpl _08037EC4
	neg r0, r0
_08037EC4:
	pop {r4}
	mov pc, lr
_08037EC8:
	push {lr}
	bl nullsub_1
	mov r0, #0
	pop {pc}
	.byte 0x00, 0x00

thumb_func_global nullsub_1
nullsub_1: @ 0x08037ED4
	mov pc, lr
	.byte 0x00, 0x00

thumb_func_global idivmod
idivmod: @ 0x08037ED8
	mov r3, #1
	cmp r1, #0
	beq _08037F9C
	bpl _08037EE2
	neg r1, r1
_08037EE2:
	push {r4}
	push {r0}
	cmp r0, #0
	bpl _08037EEC
	neg r0, r0
_08037EEC:
	cmp r0, r1
	blo _08037F90
	mov r4, #1
	lsl r4, r4, #0x1c
_08037EF4:
	cmp r1, r4
	bhs _08037F02
	cmp r1, r0
	bhs _08037F02
	lsl r1, r1, #4
	lsl r3, r3, #4
	b _08037EF4
_08037F02:
	lsl r4, r4, #3
_08037F04:
	cmp r1, r4
	bhs _08037F12
	cmp r1, r0
	bhs _08037F12
	lsl r1, r1, #1
	lsl r3, r3, #1
	b _08037F04
_08037F12:
	mov r2, #0
	cmp r0, r1
	blo _08037F1A
	sub r0, r0, r1
_08037F1A:
	lsr r4, r1, #1
	cmp r0, r4
	blo _08037F2C
	sub r0, r0, r4
	mov ip, r3
	mov r4, #1
	ror r3, r4
	orr r2, r3
	mov r3, ip
_08037F2C:
	lsr r4, r1, #2
	cmp r0, r4
	blo _08037F3E
	sub r0, r0, r4
	mov ip, r3
	mov r4, #2
	ror r3, r4
	orr r2, r3
	mov r3, ip
_08037F3E:
	lsr r4, r1, #3
	cmp r0, r4
	blo _08037F50
	sub r0, r0, r4
	mov ip, r3
	mov r4, #3
	ror r3, r4
	orr r2, r3
	mov r3, ip
_08037F50:
	mov ip, r3
	cmp r0, #0
	beq _08037F5E
	lsr r3, r3, #4
	beq _08037F5E
	lsr r1, r1, #4
	b _08037F12
_08037F5E:
	mov r4, #0xe
	lsl r4, r4, #0x1c
	and r2, r4
	beq _08037F90
	mov r3, ip
	mov r4, #3
	ror r3, r4
	tst r2, r3
	beq _08037F74
	lsr r4, r1, #3
	add r0, r0, r4
_08037F74:
	mov r3, ip
	mov r4, #2
	ror r3, r4
	tst r2, r3
	beq _08037F82
	lsr r4, r1, #2
	add r0, r0, r4
_08037F82:
	mov r3, ip
	mov r4, #1
	ror r3, r4
	tst r2, r3
	beq _08037F90
	lsr r4, r1, #1
	add r0, r0, r4
_08037F90:
	pop {r4}
	cmp r4, #0
	bpl _08037F98
	neg r0, r0
_08037F98:
	pop {r4}
	mov pc, lr
_08037F9C:
	push {lr}
	bl nullsub_1
	mov r0, #0
	pop {pc}
	.byte 0x00, 0x00

thumb_func_global sub_08037FA8
sub_08037FA8: @ 0x08037FA8
	cmp r1, #0
	beq _08038016
	mov r3, #1
	mov r2, #0
	push {r4}
	cmp r0, r1
	blo _08038010
	mov r4, #1
	lsl r4, r4, #0x1c
_08037FBA:
	cmp r1, r4
	bhs _08037FC8
	cmp r1, r0
	bhs _08037FC8
	lsl r1, r1, #4
	lsl r3, r3, #4
	b _08037FBA
_08037FC8:
	lsl r4, r4, #3
_08037FCA:
	cmp r1, r4
	bhs _08037FD8
	cmp r1, r0
	bhs _08037FD8
	lsl r1, r1, #1
	lsl r3, r3, #1
	b _08037FCA
_08037FD8:
	cmp r0, r1
	blo _08037FE0
	sub r0, r0, r1
	orr r2, r3
_08037FE0:
	lsr r4, r1, #1
	cmp r0, r4
	blo _08037FEC
	sub r0, r0, r4
	lsr r4, r3, #1
	orr r2, r4
_08037FEC:
	lsr r4, r1, #2
	cmp r0, r4
	blo _08037FF8
	sub r0, r0, r4
	lsr r4, r3, #2
	orr r2, r4
_08037FF8:
	lsr r4, r1, #3
	cmp r0, r4
	blo _08038004
	sub r0, r0, r4
	lsr r4, r3, #3
	orr r2, r4
_08038004:
	cmp r0, #0
	beq _08038010
	lsr r3, r3, #4
	beq _08038010
	lsr r1, r1, #4
	b _08037FD8
_08038010:
	add r0, r2, #0
	pop {r4}
	mov pc, lr
_08038016:
	push {lr}
	bl nullsub_1
	mov r0, #0
	pop {pc}

thumb_func_global modulo
modulo: @ 0x08038020
	cmp r1, #0
	beq _080380D6
	mov r3, #1
	cmp r0, r1
	bhs _0803802C
	mov pc, lr
_0803802C:
	push {r4}
	mov r4, #1
	lsl r4, r4, #0x1c
_08038032:
	cmp r1, r4
	bhs _08038040
	cmp r1, r0
	bhs _08038040
	lsl r1, r1, #4
	lsl r3, r3, #4
	b _08038032
_08038040:
	lsl r4, r4, #3
_08038042:
	cmp r1, r4
	bhs _08038050
	cmp r1, r0
	bhs _08038050
	lsl r1, r1, #1
	lsl r3, r3, #1
	b _08038042
_08038050:
	mov r2, #0
	cmp r0, r1
	blo _08038058
	sub r0, r0, r1
_08038058:
	lsr r4, r1, #1
	cmp r0, r4
	blo _0803806A
	sub r0, r0, r4
	mov ip, r3
	mov r4, #1
	ror r3, r4
	orr r2, r3
	mov r3, ip
_0803806A:
	lsr r4, r1, #2
	cmp r0, r4
	blo _0803807C
	sub r0, r0, r4
	mov ip, r3
	mov r4, #2
	ror r3, r4
	orr r2, r3
	mov r3, ip
_0803807C:
	lsr r4, r1, #3
	cmp r0, r4
	blo _0803808E
	sub r0, r0, r4
	mov ip, r3
	mov r4, #3
	ror r3, r4
	orr r2, r3
	mov r3, ip
_0803808E:
	mov ip, r3
	cmp r0, #0
	beq _0803809C
	lsr r3, r3, #4
	beq _0803809C
	lsr r1, r1, #4
	b _08038050
_0803809C:
	mov r4, #0xe
	lsl r4, r4, #0x1c
	and r2, r4
	bne _080380A8
	pop {r4}
	mov pc, lr
_080380A8:
	mov r3, ip
	mov r4, #3
	ror r3, r4
	tst r2, r3
	beq _080380B6
	lsr r4, r1, #3
	add r0, r0, r4
_080380B6:
	mov r3, ip
	mov r4, #2
	ror r3, r4
	tst r2, r3
	beq _080380C4
	lsr r4, r1, #2
	add r0, r0, r4
_080380C4:
	mov r3, ip
	mov r4, #1
	ror r3, r4
	tst r2, r3
	beq _080380D2
	lsr r4, r1, #1
	add r0, r0, r4
_080380D2:
	pop {r4}
	mov pc, lr
_080380D6:
	push {lr}
	bl nullsub_1
	mov r0, #0
	pop {pc}

thumb_func_global memcmp
memcmp: @ 0x080380E0
	push {r4, lr}
	add r4, r2, #0
	add r3, r0, #0
	add r2, r1, #0
	cmp r4, #3
	bls _0803811C
	orr r0, r2
	mov r1, #3
	and r0, r1
	cmp r0, #0
	bne _0803811C
	b _08038102
_080380F8:
	add r3, #4
	add r2, #4
	sub r4, #4
	cmp r4, #3
	bls _0803811C
_08038102:
	ldr r1, [r3]
	ldr r0, [r2]
	cmp r1, r0
	beq _080380F8
	b _0803811C
_0803810C:
	ldrb r0, [r3]
	ldrb r1, [r2]
	cmp r0, r1
	beq _08038118
	sub r0, r0, r1
	b _08038126
_08038118:
	add r3, #1
	add r2, #1
_0803811C:
	add r0, r4, #0
	sub r4, #1
	cmp r0, #0
	bne _0803810C
	mov r0, #0
_08038126:
	pop {r4, pc}

thumb_func_global memcpy
memcpy: @ 0x08038128
	push {r4, r5, lr}
	add r5, r0, #0
	add r4, r5, #0
	add r3, r1, #0
	cmp r2, #0xf
	bls _08038168
	add r0, r3, #0
	orr r0, r5
	mov r1, #3
	and r0, r1
	cmp r0, #0
	bne _08038168
	add r1, r5, #0
_08038142:
	ldm r3!, {r0}
	stm r1!, {r0}
	ldm r3!, {r0}
	stm r1!, {r0}
	ldm r3!, {r0}
	stm r1!, {r0}
	ldm r3!, {r0}
	stm r1!, {r0}
	sub r2, #0x10
	cmp r2, #0xf
	bhi _08038142
	cmp r2, #3
	bls _08038166
_0803815C:
	ldm r3!, {r0}
	stm r1!, {r0}
	sub r2, #4
	cmp r2, #3
	bhi _0803815C
_08038166:
	add r4, r1, #0
_08038168:
	sub r2, #1
	mov r0, #1
	neg r0, r0
	cmp r2, r0
	beq _08038182
	add r1, r0, #0
_08038174:
	ldrb r0, [r3]
	strb r0, [r4]
	add r3, #1
	add r4, #1
	sub r2, #1
	cmp r2, r1
	bne _08038174
_08038182:
	add r0, r5, #0
	pop {r4, r5, pc}
	.byte 0x00, 0x00

thumb_func_global memfill
memfill: @ 0x08038188
	push {r4, r5, lr}
	add r5, r0, #0
	add r4, r1, #0
	add r3, r5, #0
	cmp r2, #3
	bls _080381CE
	mov r0, #3
	and r0, r5
	cmp r0, #0
	bne _080381CE
	add r1, r5, #0
	mov r0, #0xff
	and r4, r0
	lsl r3, r4, #8
	orr r3, r4
	lsl r0, r3, #0x10
	orr r3, r0
	cmp r2, #0xf
	bls _080381C2
_080381AE:
	stm r1!, {r3}
	stm r1!, {r3}
	stm r1!, {r3}
	stm r1!, {r3}
	sub r2, #0x10
	cmp r2, #0xf
	bhi _080381AE
	b _080381C2
_080381BE:
	stm r1!, {r3}
	sub r2, #4
_080381C2:
	cmp r2, #3
	bhi _080381BE
	add r3, r1, #0
	b _080381CE
_080381CA:
	strb r4, [r3]
	add r3, #1
_080381CE:
	add r0, r2, #0
	sub r2, #1
	cmp r0, #0
	bne _080381CA
	add r0, r5, #0
	pop {r4, r5, pc}
	.byte 0x00, 0x00

thumb_func_global srand
srand: @ 0x080381DC
	ldr r1, =0x02020960
	ldr r1, [r1]
	str r0, [r1, #0x58]
	bx lr
	.align 2, 0
.pool



thumb_func_global rand
rand: @ 0x080381E8
	ldr r0, =0x02020960
	ldr r2, [r0]
	ldr r1, [r2, #0x58]
	ldr r0, =0x41C64E6D
	mul r0, r1, r0
	ldr r1, =0x00003039
	add r0, r0, r1
	str r0, [r2, #0x58]
	ldr r1, =0x7FFFFFFF
	and r0, r1
	bx lr
	.align 2, 0
.pool



thumb_func_global setjmp
setjmp: @ 0x08038210
	bx pc
	.align 2
	.arm
	b setjmp_0
	

thumb_func_global runTask
runTask: @ 0x08038218
	bx pc
	.align 2
	.arm
	b runTask_core


thumb_func_global longjmp
longjmp: @ 0x08038220
	bx pc
	.align 2
	.arm
	b LoadRegisters

