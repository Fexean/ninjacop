.include "asm/macros.inc"

@completely disassembled!


thumb_func_global loadGfxFromStruct
loadGfxFromStruct: @ 0x0802FA14
	push {r4, lr}
	sub sp, #4
	add r4, r0, #0
	ldr r3, [r4]
	cmp r3, #0
	beq _0802FA36
	ldrh r1, [r4, #0x10]
	lsl r1, r1, #0xe
	ldrh r0, [r4, #0x12]
	lsl r0, r0, #5
	mov r2, #0xc0
	lsl r2, r2, #0x13
	add r0, r0, r2
	add r1, r1, r0
	add r0, r3, #0
	bl Bios_LZ77_16_2
_0802FA36:
	ldr r2, [r4, #4]
	cmp r2, #0
	beq _0802FA56
	ldrh r1, [r4, #0x14]
	lsl r1, r1, #0xb
	mov r0, #0xc0
	lsl r0, r0, #0x13
	add r1, r1, r0
	ldrh r3, [r4, #0x12]
	mov r0, #0x16
	ldrsb r0, [r4, r0]
	str r0, [sp]
	add r0, r2, #0
	mov r2, #0
	bl LZ77_mapCpy
_0802FA56:
	ldr r3, [r4, #8]
	cmp r3, #0
	beq _0802FA6E
	ldrb r1, [r4, #0x18]
	ldrh r2, [r4, #0x16]
	lsl r2, r2, #5
	mov r0, #0xa0
	lsl r0, r0, #0x13
	add r2, r2, r0
	add r0, r3, #0
	bl memcpy_pal
_0802FA6E:
	ldr r2, [r4, #0xc]
	cmp r2, #0
	beq _0802FA86
	ldrh r1, [r4, #0x16]
	lsl r1, r1, #5
	mov r0, #0xa0
	lsl r0, r0, #0x13
	add r1, r1, r0
	add r0, r2, #0
	mov r2, #0
	bl startPalAnimation
_0802FA86:
	ldr r0, =0x03003D00
	ldrb r2, [r4, #0x1a]
	lsl r2, r2, #1
	add r2, r2, r0
	ldrh r1, [r4, #0x10]
	lsl r1, r1, #2
	ldrh r0, [r4, #0x14]
	lsl r0, r0, #8
	orr r1, r0
	ldrb r0, [r4, #0x1b]
	orr r0, r1
	mov r3, #0
	strh r0, [r2]
	ldr r1, =0x03002308
	ldrb r0, [r4, #0x1a]
	lsl r0, r0, #1
	add r0, r0, r1
	strh r3, [r0]
	ldr r1, =0x030022F8
	ldrb r0, [r4, #0x1a]
	lsl r0, r0, #1
	add r0, r0, r1
	strh r3, [r0]
	ldr r2, =0x03003CD4
	mov r0, #0x80
	lsl r0, r0, #1
	ldrb r4, [r4, #0x1a]
	lsl r0, r4
	ldrh r1, [r2]
	orr r0, r1
	strh r0, [r2]
	add sp, #4
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global loadSpriteGfxFromStruct
loadSpriteGfxFromStruct: @ 0x0802FADC
	push {r4, r5, lr}
	sub sp, #4
	add r5, r0, #0
	ldr r0, [r5, #4]
	cmp r0, #0
	beq _0802FAF4
	ldrh r1, [r5, #0xe]
	lsl r1, r1, #5
	ldr r2, =0x06010000
	add r1, r1, r2
	bl Bios_LZ77_16_2
_0802FAF4:
	ldr r0, [r5, #8]
	ldrb r1, [r5, #0xc]
	bl LoadSpritePalette
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	ldr r0, [r5]
	ldrh r2, [r5, #0xe]
	ldrb r3, [r5, #0x10]
	ldrb r1, [r5, #0x11]
	str r1, [sp]
	mov r1, #0
	bl allocSprite
	add r1, r0, #0
	add r1, #0x22
	strb r4, [r1]
	add r3, r0, #0
	add r3, #0x23
	ldrb r1, [r3]
	mov r2, #2
	orr r1, r2
	strb r1, [r3]
	ldrh r1, [r5, #0x12]
	strh r1, [r0, #0x10]
	ldrh r1, [r5, #0x14]
	strh r1, [r0, #0x12]
	ldr r3, =0x03001ED0
	ldr r4, =0x03001ECC
	ldr r2, [r4]
	lsl r1, r2, #2
	add r1, r1, r3
	str r0, [r1]
	add r2, #1
	str r2, [r4]
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global badEndingState1
badEndingState1: @ 0x0802FB50
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x30
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0802FB66
	mov r0, #0
	b _0802FB6E
_0802FB66:
	ldr r1, =0x03001EC8
	mov r0, #0
	str r0, [r1]
	mov r0, #1
_0802FB6E:
	pop {r1}
	bx r1
	.align 2, 0
.pool
	
	
	
	thumb_func_global sub_802FB78
sub_802FB78: @ 0x0802FB78
	push {lr}
	mov r0, #0x3f
	mov r1, #0
	mov r2, #0x30
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0802FB8E
	mov r0, #0
	b _0802FB96
_0802FB8E:
	ldr r1, =0x03001EC8
	mov r0, #0
	str r0, [r1]
	mov r0, #1
_0802FB96:
	pop {r1}
	bx r1
	.align 2, 0
.pool
	
	

thumb_func_global badEndingState12
badEndingState12: @ 0x0802FBA0
	push {r4, r5, r6, lr}
	ldr r5, =0x03003D94
	ldr r6, =0x03001EBC
	ldr r0, [r6]
	lsl r0, r0, #4
	mov r1, #0x60
	bl Bios_Div
	add r4, r0, #0
	ldr r0, [r6]
	lsl r0, r0, #4
	mov r1, #0x60
	bl Bios_Div
	mov r1, #0x10
	sub r1, r1, r4
	lsl r1, r1, #8
	orr r1, r0
	strh r1, [r5]
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	cmp r1, #0x10
	beq _0802FBE0
	ldr r0, [r6]
	add r0, #1
	str r0, [r6]
	mov r0, #0
	b _0802FBFA
	.align 2, 0
.pool
_0802FBE0:
	ldr r2, =0x03003CD4
	ldrh r1, [r2]
	ldr r0, =0x0000FDFF
	and r0, r1
	ldr r1, =0x0000FBFF
	and r0, r1
	ldr r1, =0x0000F7FF
	and r0, r1
	strh r0, [r2]
	ldr r1, =0x03001EC8
	mov r0, #0
	str r0, [r1]
	mov r0, #1
_0802FBFA:
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool



thumb_func_global sub_802FC14
sub_802FC14: @ 0x0802FC14
	push {lr}
	mov r0, #0x3f
	mov r1, #0
	mov r2, #0x30
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0802FC2C
	mov r0, #0
	b _0802FC34
_0802FC2C:
	ldr r1, =0x03001EC8
	mov r0, #0
	str r0, [r1]
	mov r0, #1
_0802FC34:
	pop {r1}
	bx r1
	.align 2, 0
.pool


thumb_func_global unused_credits1
unused_credits1: @ 0x0802FC3C
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x30
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0802FC54
	mov r0, #0
	b _0802FC5C
_0802FC54:
	ldr r1, =0x03001EC8
	mov r0, #0
	str r0, [r1]
	mov r0, #1
_0802FC5C:
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_0802FC64
sub_0802FC64: @ 0x0802FC64
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl clearSprites
	bl resetAffineTransformations
	bl clearGraphics
	bl FreeAllPalFadeTasks
	bl stopPalFade
	ldr r1, =0x03001ECC
	mov r0, #0
	str r0, [r1]
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global unused_credits2
unused_credits2: @ 0x0802FC98
	push {lr}
	ldr r0, =0x03001EC0
	ldr r0, [r0]
	cmp r0, #0
	beq _0802FCA6
	bl endTask
_0802FCA6:
	ldr r1, =0x040000B0
	ldrh r2, [r1, #0xa]
	ldr r0, =0x0000C5FF
	and r0, r2
	strh r0, [r1, #0xa]
	ldrh r2, [r1, #0xa]
	ldr r0, =0x00007FFF
	and r0, r2
	strh r0, [r1, #0xa]
	ldrh r0, [r1, #0xa]
	ldr r0, =(cb_startHBlankDMA+1)
	bl deleteCallback
	mov r0, #2
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global badEndingState0
badEndingState0: @ 0x0802FCDC
	push {lr}
	bl sub_0802FC64
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r0, =unk_8222724
	bl loadGfxFromStruct
	mov r0, #0xd0
	bl playSong
	mov r0, #1
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global badEndingState2
badEndingState2: @ 0x0802FD00
	push {lr}
	ldr r2, =0x03001EC8
	ldr r0, [r2]
	ldr r3, =0x0000017B
	add r1, r0, #0
	add r0, #1
	str r0, [r2]
	cmp r1, r3
	bgt _0802FD20
	mov r0, #0
	b _0802FD2A
	.align 2, 0
.pool
_0802FD20:
	mov r0, #2
	mov r1, #3
	bl fadeoutSong
	mov r0, #1
_0802FD2A:
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	thumb_func_global cb_startHBlankDMA
cb_startHBlankDMA: @ 0x0802FD30
	ldr r1, =0x040000B0
	ldr r0, =0x03001EE0
	ldr r0, [r0]
	str r0, [r1]
	ldr r0, =0x04000010
	str r0, [r1, #4]
	ldr r0, =0xA6600001
	str r0, [r1, #8]
	ldr r0, [r1, #8]
	bx lr
	.align 2, 0
.pool



thumb_func_global sub_0802FD54
sub_0802FD54: @ 0x0802FD54
	push {r4, lr}
	sub sp, #4
	ldr r1, =0x03001EE8
	mov r0, #0x50
	strh r0, [r1]
	ldr r1, =0x03001EEA
	mov r0, #0
	strh r0, [r1]
	ldr r1, =0x040000B0
	ldrh r2, [r1, #0xa]
	ldr r0, =0x0000C5FF
	and r0, r2
	strh r0, [r1, #0xa]
	ldrh r2, [r1, #0xa]
	ldr r0, =0x00007FFF
	and r0, r2
	strh r0, [r1, #0xa]
	ldrh r0, [r1, #0xa]
	mov r2, #0x50
	neg r2, r2
	str r2, [sp]
	ldr r0, =0x040000D4
	mov r1, sp
	str r1, [r0]
	ldr r3, =0x0200A210
	str r3, [r0, #4]
	ldr r1, =0x850000A0
	str r1, [r0, #8]
	ldr r4, [r0, #8]
	str r2, [sp]
	mov r2, sp
	str r2, [r0]
	ldr r2, =0x02020160
	str r2, [r0, #4]
	str r1, [r0, #8]
	ldr r0, [r0, #8]
	ldr r0, =0x03001EE0
	str r3, [r0]
	ldr r0, =0x03001EE4
	str r2, [r0]
	ldr r0, =(cb_startHBlankDMA+1)
	mov r1, #0xff
	bl addCallback
	add sp, #4
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool




thumb_func_global sub_802FDE4
sub_802FDE4: @ 0x0802FDE4
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	ldr r0, =0x03002080
	ldrh r5, [r0]
	ldr r0, =0x03001EE4
	ldr r2, [r0]
	mov r4, #0
	mov sb, r0
	ldr r0, =0x03001EEA
	mov ip, r0
	ldr r1, =0x03001EE0
	mov r8, r1
	ldr r6, =dword_80382C8
	ldr r7, =0x03001EE8
_0802FE04:
	lsl r0, r4, #1
	add r0, r0, r4
	add r0, r0, r5
	lsl r0, r0, #0x18
	lsr r3, r0, #0x18
	mov r1, ip
	ldrh r0, [r1]
	cmp r0, #0
	beq _0802FE38
	add r1, r4, r5
	lsl r0, r1, #1
	add r0, r0, r1
	b _0802FE3C
	.align 2, 0
.pool
_0802FE38:
	lsl r0, r4, #2
	add r0, r0, r5
_0802FE3C:
	lsl r0, r0, #0x18
	lsr r1, r0, #0x18
	add r0, r3, #0
	add r0, #0x40
	lsl r0, r0, #1
	add r0, r0, r6
	ldrh r0, [r0]
	lsl r0, r0, #0x10
	asr r0, r0, #0x18
	sub r0, #8
	neg r0, r0
	strh r0, [r2]
	add r2, #2
	lsl r0, r1, #1
	add r0, r0, r6
	mov r3, #0
	ldrsh r0, [r0, r3]
	asr r0, r0, #7
	ldrh r1, [r7]
	add r0, r0, r1
	neg r0, r0
	strh r0, [r2]
	add r2, #2
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0x9f
	bls _0802FE04
	ldr r1, =0x04000208
	mov r0, #0
	strh r0, [r1]
	mov r3, sb
	ldr r2, [r3]
	mov r3, r8
	ldr r0, [r3]
	mov r3, sb
	str r0, [r3]
	mov r0, r8
	str r2, [r0]
	mov r0, #1
	strh r0, [r1]
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_802FEA0
sub_802FEA0: @ 0x0802FEA0
	push {lr}
	ldr r0, =0x03002080
	ldr r0, [r0]
	mov r1, #1
	and r0, r1
	cmp r0, #0
	beq _0802FECC
	ldr r2, =0x03003CD4
	ldr r0, =off_8222778
	add r1, #0xff
	ldrb r0, [r0, #0x1a]
	lsl r1, r0
	ldrh r0, [r2]
	bic r0, r1
	b _0802FEDC
	.align 2, 0
.pool
_0802FECC:
	ldr r2, =0x03003CD4
	ldr r1, =off_8222778
	mov r0, #0x80
	lsl r0, r0, #1
	ldrb r1, [r1, #0x1a]
	lsl r0, r1
	ldrh r1, [r2]
	orr r0, r1
_0802FEDC:
	strh r0, [r2]
	bl sub_802FDE4
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_802FEF0
sub_802FEF0: @ 0x0802FEF0
	push {r4, r5, lr}
	ldr r4, =0x03002080
	ldr r0, [r4]
	mov r5, #1
	and r0, r5
	cmp r0, #0
	bne _0802FF0C
	ldr r1, =0x03002308
	ldr r0, =off_822275C
	ldrb r0, [r0, #0x1a]
	lsl r0, r0, #1
	add r0, r0, r1
	ldr r1, =0x0000FFF8
	strh r1, [r0]
_0802FF0C:
	ldr r0, [r4]
	mov r1, #3
	and r0, r1
	cmp r0, #0
	bne _0802FF26
	ldr r2, =0x030022F8
	ldr r0, =off_822275C
	ldrb r1, [r0, #0x1a]
	lsl r1, r1, #1
	add r1, r1, r2
	ldrh r0, [r1]
	sub r0, #1
	strh r0, [r1]
_0802FF26:
	mov r0, #3
	bl random
	ldr r3, =0x03002308
	ldr r1, =off_8222740
	ldrb r2, [r1, #0x1a]
	lsl r2, r2, #1
	add r2, r2, r3
	mov r3, #7
	neg r3, r3
	add r1, r3, #0
	sub r1, r1, r0
	strh r1, [r2]
	ldr r1, [r4]
	mov r0, #7
	and r1, r0
	cmp r1, #0
	bne _0802FF80
	ldr r3, =0x03001EE8
	ldrh r2, [r3]
	cmp r2, #0xf
	bls _0802FF7C
	ldr r0, =0x03001EEA
	strh r5, [r0]
	sub r0, r2, #1
	strh r0, [r3]
	b _0802FF80
	.align 2, 0
.pool
_0802FF7C:
	ldr r0, =0x03001EEA
	strh r1, [r0]
_0802FF80:
	bl sub_802FEA0
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_802FF90
sub_802FF90: @ 0x0802FF90
	push {r4, r5, r6, lr}
	ldr r6, =0x03002080
	ldr r0, [r6]
	mov r1, #1
	and r0, r1
	cmp r0, #0
	bne _0802FFBA
	ldr r1, =0x03002308
	ldr r0, =off_822275C
	ldrb r4, [r0, #0x1a]
	lsl r4, r4, #1
	add r1, r4, r1
	ldr r5, =0x0000FFF8
	strh r5, [r1]
	mov r0, #3
	bl random
	ldr r1, =0x030022F8
	add r4, r4, r1
	sub r5, r5, r0
	strh r5, [r4]
_0802FFBA:
	ldr r0, =0x03001EC8
	ldr r1, [r0]
	ldr r0, =0x0000012B
	cmp r1, r0
	ble _0802FFD8
	ldr r1, =0x030022F8
	ldr r0, =off_822275C
	ldrb r0, [r0, #0x1a]
	lsl r0, r0, #1
	add r0, r0, r1
	ldr r1, =0x0000FFF8
	strh r1, [r0]
	ldr r0, =(sub_802FEF0+1)
	bl setCurrentTaskFunc
_0802FFD8:
	mov r0, #3
	bl random
	ldr r3, =0x03002308
	ldr r1, =off_8222740
	ldrb r2, [r1, #0x1a]
	lsl r2, r2, #1
	add r2, r2, r3
	mov r3, #7
	neg r3, r3
	add r1, r3, #0
	sub r1, r1, r0
	strh r1, [r2]
	ldr r1, [r6]
	mov r0, #7
	and r1, r0
	cmp r1, #0
	bne _08030038
	ldr r2, =0x03001EE8
	ldrh r0, [r2]
	cmp r0, #0xf
	bls _08030034
	sub r0, #1
	strh r0, [r2]
	b _08030038
	.align 2, 0
.pool
_08030034:
	ldr r0, =0x03001EEA
	strh r1, [r0]
_08030038:
	bl sub_802FEA0
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_8030048
sub_8030048: @ 0x08030048
	push {r4, lr}
	ldr r0, =0x03002080
	ldr r0, [r0]
	mov r1, #1
	and r0, r1
	cmp r0, #0
	bne _08030078
	mov r0, #3
	bl random
	ldr r3, =0x03002308
	ldr r1, =off_822275C
	ldrb r2, [r1, #0x1a]
	lsl r2, r2, #1
	add r3, r2, r3
	mov r4, #7
	neg r4, r4
	add r1, r4, #0
	sub r1, r1, r0
	strh r1, [r3]
	ldr r0, =0x030022F8
	add r2, r2, r0
	ldr r0, =0x0000FFF8
	strh r0, [r2]
_08030078:
	ldr r0, =0x03001EC8
	ldr r0, [r0]
	cmp r0, #0xc7
	ble _08030086
	ldr r0, =(sub_802FF90+1)
	bl setCurrentTaskFunc
_08030086:
	mov r0, #3
	bl random
	ldr r3, =0x03002308
	ldr r1, =off_8222740
	ldrb r2, [r1, #0x1a]
	lsl r2, r2, #1
	add r2, r2, r3
	mov r3, #7
	neg r3, r3
	add r1, r3, #0
	sub r1, r1, r0
	strh r1, [r2]
	bl sub_802FEA0
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool





thumb_func_global badEndingState4
badEndingState4: @ 0x080300CC
	push {r4, r5, r6, lr}
	bl sub_0802FC64
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r4, =off_8222740
	add r0, r4, #0
	bl loadGfxFromStruct
	ldr r1, =0x03002308
	ldrb r0, [r4, #0x1a]
	lsl r0, r0, #1
	add r1, r0, r1
	mov r6, #0
	ldr r2, =0x0000FFF8
	strh r2, [r1]
	ldr r5, =0x030022F8
	add r0, r0, r5
	strh r2, [r0]
	ldr r0, =off_822275C
	bl loadGfxFromStruct
	ldr r4, =off_8222778
	add r0, r4, #0
	bl loadGfxFromStruct
	ldrb r0, [r4, #0x1a]
	lsl r0, r0, #1
	add r0, r0, r5
	mov r1, #0x50
	strh r1, [r0]
	ldr r0, =0x03001EC8
	str r6, [r0]
	bl sub_0802FD54
	ldr r0, =(sub_8030048+1)
	mov r1, #0x80
	bl createTask
	ldr r1, =0x03001EC0
	str r0, [r1]
	mov r0, #0xd4
	bl playSong
	mov r0, #1
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global badEndingState6
badEndingState6: @ 0x08030154
	push {lr}
	ldr r2, =0x03001EC8
	ldr r0, [r2]
	ldr r3, =0x000001B7
	add r1, r0, #0
	add r0, #1
	str r0, [r2]
	cmp r1, r3
	bgt _08030174
	mov r0, #0
	b _08030184
	.align 2, 0
.pool
_08030174:
	ldr r1, =0x03001EEA
	mov r0, #0
	strh r0, [r1]
	mov r0, #2
	mov r1, #3
	bl fadeoutSong
	mov r0, #1
_08030184:
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global badEndingState8
badEndingState8: @ 0x0803018C
	push {r4, lr}
	ldr r4, =0x03001EC0
	ldr r0, [r4]
	bl endTask
	mov r0, #0
	str r0, [r4]
	ldr r0, =(cb_startHBlankDMA+1)
	bl deleteCallback
	mov r0, #1
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool





thumb_func_global sub_80301B0
sub_80301B0: @ 0x080301B0
	push {r4, lr}
	ldr r4, =0x03001EC4
	ldr r1, [r4]
	cmp r1, #0x77
	ble _080301BC
	mov r1, #0x77
_080301BC:
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	ldr r3, =0x03003D90
	mov r0, #0x77
	sub r0, r0, r1
	mov r2, #0xff
	and r0, r2
	lsl r0, r0, #8
	add r1, #0x78
	and r1, r2
	orr r0, r1
	strh r0, [r3]
	ldr r1, =0x03003D88
	mov r0, #0x9f
	strh r0, [r1]
	ldr r0, [r4]
	add r0, #1
	str r0, [r4]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool




	thumb_func_global sub_80301F4
sub_80301F4: @ 0x080301F4
	push {lr}
	ldr r0, =0x03001EC8
	ldr r0, [r0]
	sub r0, #0xc8
	lsl r0, r0, #0x19
	lsr r0, r0, #0x18
	ldr r2, =0x03003D90
	ldr r1, =0x00007778
	strh r1, [r2]
	ldr r1, =0x03003D88
	strh r0, [r1]
	cmp r0, #0x9e
	bls _0803021A
	ldr r0, =0x03001EC4
	mov r1, #0
	str r1, [r0]
	ldr r0, =(sub_80301B0+1)
	bl setCurrentTaskFunc
_0803021A:
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global badEndingState9
badEndingState9: @ 0x08030238
	push {lr}
	sub sp, #4
	bl sub_0802FC64
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r0, =off_8222794
	bl loadGfxFromStruct
	ldr r0, =off_82227B0
	bl loadGfxFromStruct
	ldr r0, =off_82227CC
	bl loadGfxFromStruct
	mov r1, sp
	ldr r2, =0x0000FFFF
	add r0, r2, #0
	strh r0, [r1]
	mov r1, #0xa0
	lsl r1, r1, #0x13
	ldr r2, =0x01000001
	mov r0, sp
	bl Bios_memcpy
	mov r0, #1
	add sp, #4
	pop {r1}
	bx r1
	.align 2, 0
.pool




thumb_func_global badEndingState11
badEndingState11: @ 0x0803028C
	push {r4, lr}
	ldr r1, =0x03003D74
	ldr r2, =0x00003F41
	add r0, r2, #0
	strh r0, [r1]
	ldr r0, =0x03003D94
	mov r3, #0xff
	lsl r3, r3, #8
	add r4, r3, #0
	strh r4, [r0]
	ldr r1, =0x03001EC8
	ldr r3, [r1]
	ldr r2, =0x00000167
	add r0, r3, #0
	add r3, #1
	str r3, [r1]
	cmp r0, r2
	ble _080302E4
	ldr r0, =dword_82227E8
	bl loadGfxFromStruct
	ldr r1, =0x03001EBC
	mov r0, #0
	str r0, [r1]
	mov r0, #0xd6
	bl playSong
	mov r0, #1
	b _08030320
	.align 2, 0
.pool
_080302E4:
	cmp r3, #0xc8
	bne _0803031E
	ldr r3, =0x03003D7C
	ldrh r2, [r3]
	add r1, r4, #0
	add r0, r1, #0
	and r0, r2
	mov r2, #0x33
	orr r0, r2
	strh r0, [r3]
	ldr r2, =0x03003D8C
	ldrh r0, [r2]
	and r1, r0
	mov r0, #0x3f
	orr r1, r0
	strh r1, [r2]
	ldr r2, =0x03003CD4
	ldrh r0, [r2]
	mov r3, #0x80
	lsl r3, r3, #6
	add r1, r3, #0
	orr r0, r1
	strh r0, [r2]
	ldr r0, =(sub_80301F4+1)
	mov r1, #0x80
	bl createTask
	ldr r1, =0x03001EC0
	str r0, [r1]
_0803031E:
	mov r0, #0
_08030320:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool



thumb_func_global badEndingState13
badEndingState13: @ 0x0803033C
	push {r4, r5, lr}
	ldr r2, =0x03001EC8
	ldr r0, [r2]
	add r1, r0, #0
	add r0, #1
	str r0, [r2]
	cmp r1, #0x59
	bgt _08030354
	mov r0, #0
	b _08030374
	.align 2, 0
.pool
_08030354:
	mov r5, #0
	str r5, [r2]
	ldr r2, =0x03003CD4
	ldrh r1, [r2]
	ldr r0, =0x0000DFFF
	and r0, r1
	strh r0, [r2]
	ldr r4, =0x03001EC0
	ldr r0, [r4]
	bl endTask
	str r5, [r4]
	mov r0, #2
	bl stopTrack
	mov r0, #1
_08030374:
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
.pool



thumb_func_global badEndingState15
badEndingState15: @ 0x08030388
	ldr r2, =0x03001EC8
	ldr r0, [r2]
	add r1, r0, #0
	add r0, #1
	str r0, [r2]
	cmp r1, #0x77
	bgt _080303A0
	mov r0, #0
	b _080303A6
	.align 2, 0
.pool
_080303A0:
	mov r0, #0
	str r0, [r2]
	mov r0, #1
_080303A6:
	bx lr



thumb_func_global unused_credits0
unused_credits0: @ 0x080303A8
	push {lr}
	mov r0, #0
	mov r1, #2
	bl fadeoutSong
	mov r0, #1
	mov r1, #2
	bl fadeoutSong
	mov r0, #2
	mov r1, #2
	bl fadeoutSong
	mov r0, #3
	mov r1, #2
	bl fadeoutSong
	mov r0, #4
	mov r1, #2
	bl fadeoutSong
	mov r0, #5
	mov r1, #2
	bl fadeoutSong
	mov r0, #1
	pop {r1}
	bx r1



thumb_func_global cb_unused_credits_switch
cb_unused_credits_switch: @ 0x080303E0
	push {r4, lr}
	ldr r0, =unused_credits_functions
	ldr r4, =0x03001EB8
	ldr r1, [r4]
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r0, [r1]
	bl call_r0
	cmp r0, #1
	beq _08030408
	cmp r0, #1
	ble _08030416
	cmp r0, #2
	beq _08030410
	b _08030416
	.align 2, 0
.pool
_08030408:
	ldr r0, [r4]
	add r0, #1
	str r0, [r4]
	b _08030416
_08030410:
	ldr r0, =(sub_8030E30+1)
	bl setCurrentTaskFunc
_08030416:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global cb_badEnding_switch
cb_badEnding_switch: @ 0x08030420
	push {r4, lr}
	ldr r0, =badEndingStates
	ldr r4, =0x03001EB8
	ldr r1, [r4]
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r0, [r1]
	bl call_r0
	cmp r0, #1
	beq _08030448
	cmp r0, #1
	ble _08030456
	cmp r0, #2
	beq _08030450
	b _08030456
	.align 2, 0
.pool
_08030448:
	ldr r0, [r4]
	add r0, #1
	str r0, [r4]
	b _08030456
_08030450:
	ldr r0, =(sub_8030E30+1)
	bl setCurrentTaskFunc
_08030456:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global cb_badEnding
cb_badEnding: @ 0x08030460
	push {lr}
	ldr r0, =0x03001EB8
	mov r1, #0
	str r1, [r0]
	ldr r0, =0x03001EC0
	str r1, [r0]
	bl stopSoundTracks
	mov r0, #0xd
	bl playSong
	ldr r0, =(cb_badEnding_switch+1)
	bl setCurrentTaskFunc
	pop {r0}
	bx r0
	.align 2, 0
.pool




	thumb_func_global sub_803048C
sub_803048C: @ 0x0803048C
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	ldr r0, =0x03001EC4
	ldr r7, [r0]
	mov r0, #0x10
	lsl r1, r7, #0x13
	asr r1, r1, #0x10
	sub r0, r0, r1
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	cmp r0, #0
	bge _080304AC
	mov r2, #0
_080304AC:
	mov r0, #0x5c
	sub r0, r0, r1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	mov r8, r3
	cmp r0, #0
	bge _080304BE
	mov r0, #0
	mov r8, r0
_080304BE:
	mov r0, #0x90
	sub r0, r0, r1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	mov sb, r3
	cmp r0, #0
	bge _080304D0
	mov r0, #0
	mov sb, r0
_080304D0:
	mov r0, #8
	sub r0, r0, r1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	mov sl, r3
	cmp r0, #0
	bge _080304E2
	mov r0, #0
	mov sl, r0
_080304E2:
	add r0, r1, #0
	add r0, #0x60
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #0xef
	ble _080304F2
	mov r3, #0xef
_080304F2:
	add r0, r1, #0
	add r0, #0x98
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #0x9f
	ble _08030502
	mov r5, #0x9f
_08030502:
	add r0, r1, #0
	add r0, #0xe0
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #0xef
	ble _08030512
	mov r6, #0xef
_08030512:
	add r0, r1, #0
	add r0, #0x44
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #0x9f
	ble _08030522
	mov r4, #0x9f
_08030522:
	ldr r1, =0x03003D90
	mov ip, r1
	lsl r1, r2, #0x10
	asr r1, r1, #0x10
	mov r2, #0xff
	and r1, r2
	lsl r1, r1, #8
	lsl r0, r3, #0x10
	asr r0, r0, #0x10
	and r0, r2
	orr r1, r0
	mov r3, ip
	strh r1, [r3]
	ldr r3, =0x03003D88
	mov r0, r8
	lsl r1, r0, #0x10
	asr r1, r1, #0x10
	and r1, r2
	lsl r1, r1, #8
	lsl r0, r5, #0x10
	asr r0, r0, #0x10
	and r0, r2
	orr r1, r0
	strh r1, [r3]
	mov r0, sb
	lsl r1, r0, #0x10
	asr r1, r1, #0x10
	and r1, r2
	lsl r1, r1, #8
	lsl r0, r6, #0x10
	asr r0, r0, #0x10
	and r0, r2
	orr r1, r0
	mov r0, ip
	strh r1, [r0, #2]
	mov r0, sl
	lsl r1, r0, #0x10
	asr r1, r1, #0x10
	and r1, r2
	lsl r1, r1, #8
	lsl r0, r4, #0x10
	asr r0, r0, #0x10
	and r0, r2
	orr r1, r0
	strh r1, [r3, #2]
	add r0, r7, #1
	ldr r1, =0x03001EC4
	str r0, [r1]
	cmp r7, #0x1f
	ble _0803059E
	ldr r2, =0x03003CD4
	ldrh r1, [r2]
	ldr r0, =0x0000DFFF
	and r0, r1
	ldr r1, =0x0000BFFF
	and r0, r1
	strh r0, [r2]
	bl endCurrentTask
	ldr r1, =0x03001EC0
	mov r0, #0
	str r0, [r1]
_0803059E:
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_80305C8
sub_80305C8: @ 0x080305C8
	push {lr}
	ldr r2, =0x03001EC4
	ldr r0, [r2]
	add r1, r0, #0
	add r0, #1
	str r0, [r2]
	cmp r1, #0xf
	ble _080305E2
	mov r0, #0
	str r0, [r2]
	ldr r0, =(sub_803048C+1)
	bl setCurrentTaskFunc
_080305E2:
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_80305F0
sub_80305F0: @ 0x080305F0
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	ldr r7, =0x03001EC4
	ldr r4, [r7]
	ldrb r3, [r7]
	add r4, #1
	str r4, [r7]
	ldr r6, =0x03003D90
	mov r0, #0x83
	lsl r0, r0, #5
	mov r8, r0
	mov r0, r8
	strh r0, [r6]
	ldr r5, =0x03003D88
	add r1, r3, #0
	add r1, #8
	mov r2, #0xff
	and r1, r2
	lsl r1, r1, #8
	add r0, r3, #0
	add r0, #0x44
	and r0, r2
	orr r1, r0
	strh r1, [r5]
	ldr r0, =0x000090E0
	mov ip, r0
	mov r0, ip
	strh r0, [r6, #2]
	mov r1, #0x5c
	sub r1, r1, r3
	and r1, r2
	lsl r1, r1, #8
	mov r0, #0x98
	sub r0, r0, r3
	and r0, r2
	orr r1, r0
	strh r1, [r5, #2]
	cmp r4, #0x53
	ble _0803065A
	mov r0, r8
	strh r0, [r6]
	ldr r0, =0x00005C98
	strh r0, [r5]
	mov r0, ip
	strh r0, [r6, #2]
	ldr r0, =0x00000844
	strh r0, [r5, #2]
	mov r0, #0
	str r0, [r7]
	ldr r0, =(sub_80305C8+1)
	bl setCurrentTaskFunc
_0803065A:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_8030680
sub_8030680: @ 0x08030680
	push {lr}
	ldr r2, =0x03001EC4
	ldr r0, [r2]
	add r1, r0, #0
	add r0, #1
	str r0, [r2]
	cmp r1, #0x2f
	ble _0803069A
	mov r0, #0
	str r0, [r2]
	ldr r0, =(sub_80305F0+1)
	bl setCurrentTaskFunc
_0803069A:
	pop {r0}
	bx r0
	.align 2, 0
.pool






thumb_func_global goodEndingState0
goodEndingState0: @ 0x080306A8
	push {r4, lr}
	bl sub_0802FC64
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r0, =off_8222804
	bl loadGfxFromStruct
	mov r0, #0xd0
	bl playSong
	mov r0, #2
	mov r1, #0xc
	bl fadeoutSong
	ldr r2, =0x03003D8C
	ldrh r1, [r2]
	mov r0, #0xff
	lsl r0, r0, #8
	and r0, r1
	mov r1, #0x20
	orr r0, r1
	strh r0, [r2]
	ldr r1, =0x03003D7C
	ldr r0, =0x00003F3F
	strh r0, [r1]
	ldr r2, =0x03003D90
	mov r3, #0
	mov r0, #0x83
	lsl r0, r0, #5
	strh r0, [r2]
	ldr r1, =0x03003D88
	ldr r0, =0x00000844
	strh r0, [r1]
	ldr r0, =0x000090E0
	strh r0, [r2, #2]
	ldr r0, =0x00005C98
	strh r0, [r1, #2]
	ldr r2, =0x03003CD4
	ldrh r0, [r2]
	mov r4, #0x80
	lsl r4, r4, #6
	add r1, r4, #0
	orr r0, r1
	mov r4, #0x80
	lsl r4, r4, #7
	add r1, r4, #0
	orr r0, r1
	strh r0, [r2]
	ldr r0, =0x03001EC4
	str r3, [r0]
	ldr r0, =(sub_8030680+1)
	mov r1, #0x80
	bl createTask
	mov r0, #1
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global goodEndingState2
goodEndingState2: @ 0x08030754
	push {lr}
	ldr r0, =0x03001EC8
	ldr r3, [r0]
	ldr r2, =0x000001B7
	add r1, r3, #0
	add r3, #1
	str r3, [r0]
	cmp r1, r2
	ble _08030774
	mov r0, #1
	b _08030796
	.align 2, 0
.pool
_08030774:
	cmp r3, #0xa0
	bne _08030794
	bl FreeAllPalFadeTasks
	bl stopPalFade
	ldr r0, =off_821A974
	ldr r1, =off_8222804
	ldrh r1, [r1, #0x16]
	lsl r1, r1, #5
	mov r2, #0xa0
	lsl r2, r2, #0x13
	add r1, r1, r2
	mov r2, #0
	bl startPalAnimation
_08030794:
	mov r0, #0
_08030796:
	pop {r1}
	bx r1
	.align 2, 0
.pool
	
	
	
	
	thumb_func_global sub_80307A4
sub_80307A4: @ 0x080307A4
	push {lr}
	ldr r1, =0x03001EC8
	ldr r2, [r1]
	ldr r1, =0x000001B7
	cmp r2, r1
	ble _080307CA
	ldr r0, =0x03001ED0
	ldr r1, [r0, #4]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	bl endCurrentTask
	ldr r1, =0x03001EC0
	mov r0, #0
	str r0, [r1]
_080307CA:
	pop {r1}
	bx r1
	.align 2, 0
.pool



	thumb_func_global sub_80307E0
sub_80307E0: @ 0x080307E0
	push {lr}
	ldr r1, =0x03001EC8
	ldr r2, [r1]
	ldr r1, =0x00000185
	cmp r2, r1
	ble _08030802
	ldr r0, =0x03001ED0
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	ldr r0, =(sub_80307A4+1)
	bl setCurrentTaskFunc
_08030802:
	pop {r1}
	bx r1
	.align 2, 0
.pool



	thumb_func_global sub_8030818
sub_8030818: @ 0x08030818
	push {r4, r5, r6, lr}
	ldr r1, =0x03001EC8
	ldr r2, [r1]
	ldr r1, =0x00000153
	cmp r2, r1
	ble _08030874
	bl FreeAllPalFadeTasks
	bl stopPalFade
	ldr r4, =off_821633C
	ldr r0, =off_8222820
	ldrh r1, [r0, #0x16]
	lsl r1, r1, #5
	mov r0, #0xa0
	lsl r0, r0, #0x13
	add r1, r1, r0
	add r0, r4, #0
	mov r2, #0
	bl startPalAnimation
	ldr r6, =0x03001ED0
	ldr r0, [r6]
	add r0, #0x22
	mov r1, #0
	ldrsb r1, [r0, r1]
	lsl r1, r1, #5
	ldr r5, =0x05000200
	add r1, r1, r5
	add r0, r4, #0
	mov r2, #0
	bl startPalAnimation
	ldr r0, [r6, #4]
	add r0, #0x22
	mov r1, #0
	ldrsb r1, [r0, r1]
	lsl r1, r1, #5
	add r1, r1, r5
	add r0, r4, #0
	mov r2, #0
	bl startPalAnimation
	ldr r0, =(sub_80307E0+1)
	bl setCurrentTaskFunc
_08030874:
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool



	thumb_func_global sub_8030898
sub_8030898: @ 0x08030898
	push {lr}
	ldr r0, =0x03001EC8
	ldr r0, [r0]
	cmp r0, #0xef
	ble _080308C4
	bl FreeAllPalFadeTasks
	bl stopPalFade
	ldr r0, =off_8215F30
	ldr r1, =off_8222820
	ldrh r1, [r1, #0x16]
	lsl r1, r1, #5
	mov r2, #0xa0
	lsl r2, r2, #0x13
	add r1, r1, r2
	mov r2, #0
	bl startPalAnimation
	ldr r0, =(sub_8030818+1)
	bl setCurrentTaskFunc
_080308C4:
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
	
	

thumb_func_global goodEndingState5
goodEndingState5: @ 0x080308D8
	push {r4, r5, r6, lr}
	mov r6, r8
	push {r6}
	bl sub_0802FC64
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r4, =off_8222820
	add r0, r4, #0
	bl loadGfxFromStruct
	ldr r6, =0x03002308
	ldrb r0, [r4, #0x1a]
	lsl r0, r0, #1
	add r1, r0, r6
	ldr r5, =0x0000FFF8
	strh r5, [r1]
	ldr r1, =0x030022F8
	mov r8, r1
	add r0, r8
	strh r5, [r0]
	ldr r4, =off_822283C
	add r0, r4, #0
	bl loadGfxFromStruct
	ldrb r0, [r4, #0x1a]
	lsl r0, r0, #1
	add r6, r0, r6
	strh r5, [r6]
	add r0, r8
	strh r5, [r0]
	ldr r0, =off_8222858
	bl loadSpriteGfxFromStruct
	ldr r0, =off_8222870
	bl loadSpriteGfxFromStruct
	ldr r3, =0x03001ED0
	ldr r1, [r3]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	ldr r1, [r3, #4]
	add r1, #0x23
	ldrb r0, [r1]
	orr r0, r2
	strb r0, [r1]
	ldr r0, =(sub_8030898+1)
	mov r1, #0x80
	bl createTask
	mov r0, #1
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global goodEndingState7
goodEndingState7: @ 0x08030978
	ldr r2, =0x03001EC8
	ldr r0, [r2]
	ldr r3, =0x000001DF
	add r1, r0, #0
	add r0, #1
	str r0, [r2]
	cmp r1, r3
	bgt _08030994
	mov r0, #0
	b _08030996
	.align 2, 0
.pool
_08030994:
	mov r0, #1
_08030996:
	bx lr

thumb_func_global goodEndingState9
goodEndingState9: @ 0x08030998
	push {r4, lr}
	ldr r4, =0x03001ED0
	ldr r0, [r4]
	bl spriteFreeHeader
	ldr r0, [r4, #4]
	bl spriteFreeHeader
	mov r0, #1
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global goodEndingState4
goodEndingState4: @ 0x080309B4
	ldr r2, =0x03001EC8
	ldr r0, [r2]
	add r1, r0, #0
	add r0, #1
	str r0, [r2]
	cmp r1, #0x25
	bgt _080309CC
	mov r0, #0
	b _080309D2
	.align 2, 0
.pool
_080309CC:
	mov r0, #0
	str r0, [r2]
	mov r0, #1
_080309D2:
	bx lr

thumb_func_global goodEndingState11
goodEndingState11: @ 0x080309D4
	push {lr}
	bl sub_0802FC64
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r1, =0x03001ECC
	mov r0, #0
	str r0, [r1]
	ldr r0, =endingCityBackgroundStruct
	bl loadGfxFromStruct
	ldr r0, =endingNinjaFaceBackground
	bl loadGfxFromStruct
	ldr r0, =off_82228C0
	bl loadSpriteGfxFromStruct
	ldr r0, =off_82228D8
	bl loadSpriteGfxFromStruct
	ldr r3, =0x03003D8C
	ldrh r2, [r3]
	mov r1, #0xff
	lsl r1, r1, #8
	add r0, r1, #0
	and r0, r2
	mov r2, #0x3e
	orr r0, r2
	strh r0, [r3]
	ldr r2, =0x03003D7C
	ldrh r0, [r2]
	and r1, r0
	mov r0, #0x3f
	orr r1, r0
	strh r1, [r2]
	ldr r1, =0x03003D90
	mov r0, #0xf0
	strh r0, [r1]
	ldr r1, =0x03003D88
	ldr r0, =0x00005050
	strh r0, [r1]
	ldr r2, =0x03003CD4
	ldrh r0, [r2]
	mov r3, #0x80
	lsl r3, r3, #6
	add r1, r3, #0
	orr r0, r1
	strh r0, [r2]
	mov r0, #1
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global goodEndingState13
goodEndingState13: @ 0x08030A6C
	push {r4, lr}
	ldr r4, =0x03001EC8
	ldr r0, [r4]
	add r1, r0, #0
	add r0, #1
	str r0, [r4]
	cmp r1, #0xa9
	bgt _08030A84
	mov r0, #0
	b _08030A9E
	.align 2, 0
.pool
_08030A84:
	ldr r0, =off_821F2FC
	ldr r1, =endingCityBackgroundStruct
	ldrh r1, [r1, #0x16]
	lsl r1, r1, #5
	mov r2, #0xa0
	lsl r2, r2, #0x13
	add r1, r1, r2
	mov r2, #0
	bl startPalAnimation
	mov r0, #0
	str r0, [r4]
	mov r0, #1
_08030A9E:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global goodEndingState14
goodEndingState14: @ 0x08030AAC
	ldr r3, =0x03001EC8
	ldr r0, [r3]
	ldr r2, =0x00000135
	add r1, r0, #0
	add r0, #1
	str r0, [r3]
	cmp r1, r2
	bgt _08030AC8
	mov r0, #0
	b _08030ACE
	.align 2, 0
.pool
_08030AC8:
	mov r0, #0
	str r0, [r3]
	mov r0, #1
_08030ACE:
	bx lr

thumb_func_global goodEndingState15
goodEndingState15: @ 0x08030AD0
	ldr r2, =0x03001EC8
	ldr r1, [r2]
	add r0, r1, #0
	add r1, #1
	str r1, [r2]
	cmp r0, #0x6b
	ble _08030AEC
	mov r0, #0
	str r0, [r2]
	mov r0, #1
	b _08030B18
	.align 2, 0
.pool
_08030AEC:
	mov r0, #1
	and r1, r0
	cmp r1, #0
	beq _08030B16
	ldr r2, =0x030022F8
	ldr r0, =endingCityBackgroundStruct
	ldrb r1, [r0, #0x1a]
	lsl r1, r1, #1
	add r1, r1, r2
	ldrh r0, [r1]
	sub r0, #1
	strh r0, [r1]
	ldr r2, =0x03001ED0
	ldr r1, [r2]
	ldrh r0, [r1, #0x12]
	sub r0, #2
	strh r0, [r1, #0x12]
	ldr r1, [r2, #4]
	ldrh r0, [r1, #0x12]
	sub r0, #2
	strh r0, [r1, #0x12]
_08030B16:
	mov r0, #0
_08030B18:
	bx lr
	.align 2, 0
.pool

thumb_func_global goodEndingState16
goodEndingState16: @ 0x08030B28
	ldr r2, =0x03001EC8
	ldr r0, [r2]
	add r1, r0, #0
	add r0, #1
	str r0, [r2]
	cmp r1, #0xef
	bgt _08030B40
	mov r0, #0
	b _08030B46
	.align 2, 0
.pool
_08030B40:
	mov r0, #0
	str r0, [r2]
	mov r0, #1
_08030B46:
	bx lr

thumb_func_global goodEndingState17
goodEndingState17: @ 0x08030B48
	push {r4, r5, lr}
	ldr r1, =0x03003D90
	mov r0, #0xf0
	strh r0, [r1]
	ldr r4, =0x03003D88
	ldr r5, =0x03001EC8
	ldr r3, [r5]
	lsl r1, r3, #2
	mov r0, #0x50
	sub r0, r0, r1
	mov r2, #0xff
	and r0, r2
	lsl r0, r0, #8
	add r1, #0x50
	and r1, r2
	orr r0, r1
	strh r0, [r4]
	add r0, r3, #1
	str r0, [r5]
	cmp r3, #0xb
	bgt _08030B84
	mov r0, #0
	b _08030B8A
	.align 2, 0
.pool
_08030B84:
	mov r0, #0
	str r0, [r5]
	mov r0, #1
_08030B8A:
	pop {r4, r5}
	pop {r1}
	bx r1

thumb_func_global goodEndingState18
goodEndingState18: @ 0x08030B90
	ldr r2, =0x03001EC8
	ldr r0, [r2]
	add r1, r0, #0
	add r0, #1
	str r0, [r2]
	cmp r1, #0x77
	bgt _08030BA8
	mov r0, #0
	b _08030BAE
	.align 2, 0
.pool
_08030BA8:
	mov r0, #0
	str r0, [r2]
	mov r0, #1
_08030BAE:
	bx lr

thumb_func_global goodEndingState19
goodEndingState19: @ 0x08030BB0
	push {r4, r5, lr}
	ldr r1, =0x03003D90
	mov r0, #0xf0
	strh r0, [r1]
	ldr r4, =0x03003D88
	ldr r5, =0x03001EC8
	ldr r3, [r5]
	mov r1, #0xc
	sub r1, r1, r3
	lsl r1, r1, #2
	mov r0, #0x50
	sub r0, r0, r1
	mov r2, #0xff
	and r0, r2
	lsl r0, r0, #8
	add r1, #0x50
	and r1, r2
	orr r0, r1
	strh r0, [r4]
	add r0, r3, #1
	str r0, [r5]
	cmp r3, #0xb
	bgt _08030BF0
	mov r0, #0
	b _08030C0C
	.align 2, 0
.pool
_08030BF0:
	ldr r2, =0x03003CD4
	ldr r0, =endingNinjaFaceBackground
	mov r1, #0x80
	lsl r1, r1, #1
	ldrb r0, [r0, #0x1a]
	lsl r1, r0
	ldrh r0, [r2]
	bic r0, r1
	ldr r1, =0x0000DFFF
	and r0, r1
	strh r0, [r2]
	mov r0, #0
	str r0, [r5]
	mov r0, #1
_08030C0C:
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global goodEndingState20
goodEndingState20: @ 0x08030C20
	push {lr}
	ldr r2, =0x03001EC8
	ldr r0, [r2]
	add r1, r0, #0
	add r0, #1
	str r0, [r2]
	cmp r1, #0x4f
	bgt _08030C38
	mov r0, #0
	b _08030C52
	.align 2, 0
.pool
_08030C38:
	mov r0, #0
	str r0, [r2]
	ldr r0, =0x03001ED0
	ldr r0, [r0]
	ldr r1, =off_821E940
	ldrh r2, [r0, #0xc]
	mov r3, #0
	bl setSpriteAnimation
	mov r0, #0xd2
	bl playSong
	mov r0, #1
_08030C52:
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global goodEndingState21
goodEndingState21: @ 0x08030C60
	push {r4, lr}
	ldr r4, =0x03001ED0
	ldr r0, [r4]
	bl updateSpriteAnimation
	cmp r0, #0
	bne _08030C78
	mov r0, #0
	b _08030C8C
	.align 2, 0
.pool
_08030C78:
	ldr r1, [r4]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	ldr r1, =0x03001EC8
	mov r0, #0
	str r0, [r1]
	mov r0, #1
_08030C8C:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global goodEndingState22
goodEndingState22: @ 0x08030C98
	push {lr}
	sub sp, #4
	ldr r0, =0x03001EC8
	ldr r1, [r0]
	add r2, r1, #0
	add r1, #1
	str r1, [r0]
	cmp r2, #0x63
	bgt _08030CB4
	mov r0, #0
	b _08030CF2
	.align 2, 0
.pool
_08030CB4:
	mov r1, sp
	ldr r2, =0x0000FFFF
	add r0, r2, #0
	strh r0, [r1]
	mov r1, #0xa0
	lsl r1, r1, #0x13
	ldr r2, =0x01000001
	mov r0, sp
	bl Bios_memcpy
	bl sub_0801025C
	ldr r3, =0x03003D8C
	ldrh r2, [r3]
	mov r1, #0xff
	lsl r1, r1, #8
	add r0, r1, #0
	and r0, r2
	mov r2, #0x3f
	orr r0, r2
	strh r0, [r3]
	ldr r2, =0x03003D7C
	ldrh r0, [r2]
	and r1, r0
	mov r0, #0x20
	orr r1, r0
	strh r1, [r2]
	mov r0, #0xd3
	bl playSong
	mov r0, #1
_08030CF2:
	add sp, #4
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global goodEndingState23
goodEndingState23: @ 0x08030D08
	push {r4, lr}
	mov r0, #3
	mov r1, #0x28
	mov r2, #0x78
	mov r3, #0x50
	bl undoScreenTransition
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _08030D20
	mov r0, #0
	b _08030D58
_08030D20:
	ldr r3, =0x03003D8C
	ldrh r2, [r3]
	mov r1, #0xff
	lsl r1, r1, #8
	add r0, r1, #0
	and r0, r2
	mov r4, #0x20
	orr r0, r4
	strh r0, [r3]
	ldr r2, =0x03003D7C
	ldrh r0, [r2]
	and r1, r0
	orr r1, r4
	strh r1, [r2]
	ldr r1, =0x03003D90
	mov r0, #0xf0
	strh r0, [r1]
	ldr r1, =0x03003D88
	mov r0, #0xa0
	strh r0, [r1]
	ldr r2, =0x03003CD4
	ldrh r0, [r2]
	mov r3, #0x80
	lsl r3, r3, #6
	add r1, r3, #0
	orr r0, r1
	strh r0, [r2]
	mov r0, #1
_08030D58:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global goodEndingState25
goodEndingState25: @ 0x08030D74
	push {r4, lr}
	ldr r4, =0x03001ED0
	ldr r0, [r4]
	bl spriteFreeHeader
	ldr r0, [r4, #4]
	bl spriteFreeHeader
	ldr r2, =0x03003CD4
	ldrh r1, [r2]
	ldr r0, =0x0000DFFF
	and r0, r1
	strh r0, [r2]
	mov r0, #1
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global goodEndingState26
goodEndingState26: @ 0x08030DA4
	ldr r2, =0x03001EC8
	ldr r0, [r2]
	add r1, r0, #0
	add r0, #1
	str r0, [r2]
	cmp r1, #0x77
	bgt _08030DBC
	mov r0, #0
	b _08030DC2
	.align 2, 0
.pool
_08030DBC:
	mov r0, #0
	str r0, [r2]
	mov r0, #1
_08030DC2:
	bx lr

thumb_func_global cb_goodEndingSwitch
cb_goodEndingSwitch: @ 0x08030DC4
	push {r4, lr}
	ldr r0, =goodEndingStates
	ldr r4, =0x03001EB8
	ldr r1, [r4]
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r0, [r1]
	bl call_r0
	cmp r0, #1
	beq _08030DEC
	cmp r0, #1
	ble _08030DFA
	cmp r0, #2
	beq _08030DF4
	b _08030DFA
	.align 2, 0
.pool
_08030DEC:
	ldr r0, [r4]
	add r0, #1
	str r0, [r4]
	b _08030DFA
_08030DF4:
	ldr r0, =(sub_8030E30+1)
	bl setCurrentTaskFunc
_08030DFA:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global cb_goodEnding
cb_goodEnding: @ 0x08030E04
	push {lr}
	ldr r0, =0x03001EB8
	mov r1, #0
	str r1, [r0]
	ldr r0, =0x03001EC0
	str r1, [r0]
	bl stopSoundTracks
	mov r0, #0xc
	bl playSong
	ldr r0, =(cb_goodEndingSwitch+1)
	bl setCurrentTaskFunc
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_8030E30
sub_8030E30: @ 0x08030E30
	push {r4, lr}
	sub sp, #4
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl FreeAllPalFadeTasks
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	bl stopPalFade
	mov r0, sp
	mov r4, #0
	strh r4, [r0]
	mov r1, #0xa0
	lsl r1, r1, #0x13
	ldr r2, =0x01000001
	bl Bios_memcpy
	mov r0, sp
	add r0, #2
	strh r4, [r0]
	ldr r4, =0x03000580
	ldr r2, =0x0100001E
	add r1, r4, #0
	bl Bios_memcpy
	ldr r0, =dword_822F7B8
	ldr r0, [r0]
	str r0, [r4, #0x38]
	mov r0, #0
	strh r0, [r4]
	mov r0, #0x84
	bl allocateSpriteTiles
	strh r0, [r4, #8]
	ldr r1, =credits_fonts
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldrh r1, [r4, #8]
	lsl r1, r1, #5
	ldr r2, =0x06010000
	add r1, r1, r2
	bl Bios_LZ77_16_2
	ldr r0, =word_82241EC
	mov r1, #1
	bl LoadSpritePalette
	strh r0, [r4, #0xa]
	ldr r0, =0x030005C0
	mov r1, #1
	bl allocFont
	ldr r0, =(sub_8030EEC+1)
	bl setCurrentTaskFunc
	add sp, #4
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool


	
thumb_func_global sub_8030EEC
sub_8030EEC: @ 0x08030EEC
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	sub sp, #4
	ldr r0, =0x03002080
	ldrh r1, [r0, #0xc]
	mov r0, #8
	and r0, r1
	ldr r6, =0x03000580
	cmp r0, #0
	beq _08030F08
	mov r0, #7
	strh r0, [r6, #2]
_08030F08:
	ldrh r0, [r6, #2]
	cmp r0, #0xb
	bls _08030F10
	b def_8030F18
_08030F10:
	lsl r0, r0, #2
	ldr r1, =_08030F28
	add r0, r0, r1
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
.pool
_08030F28: @ jump table
	.4byte _08030F58 @ case 0
	.4byte _08031010 @ case 1
	.4byte _08031124 @ case 2
	.4byte _08031144 @ case 3
	.4byte _08031164 @ case 4
	.4byte _08031254 @ case 5
	.4byte _08031268 @ case 6
	.4byte _08031288 @ case 7
	.4byte _080312A4 @ case 8
	.4byte _080312C4 @ case 9
	.4byte _080312D8 @ case 10
	.4byte _080312F8 @ case 11
_08030F58:
	bl clearSprites
	ldr r3, =0x03000580
	mov r0, #0
	str r0, [r3, #4]
	mov r5, #0
	ldr r0, [r3, #0x38]
	ldrh r0, [r0]
	cmp r5, r0
	blo _08030F6E
	b _08031230
_08030F6E:
	mov r8, r3
	ldr r0, =0x030020AC
	mov sb, r0
_08030F74:
	mov r1, r8
	ldr r2, [r1, #0x38]
	lsl r6, r5, #2
	mov r3, sb
	ldrb r1, [r3]
	lsl r0, r1, #2
	add r0, r0, r1
	lsl r0, r0, #2
	add r0, r6, r0
	add r2, #4
	add r2, r2, r0
	ldr r0, [r2]
	ldr r1, [r0, #8]
	lsl r7, r5, #3
	cmp r1, #0
	beq _08030FF0
	mov r0, r8
	ldrh r2, [r0, #8]
	mov r0, #0
	str r0, [sp]
	add r0, r1, #0
	mov r1, #0
	mov r3, #0
	bl allocSprite
	mov r3, r8
	add r3, #0x14
	add r3, r7, r3
	str r0, [r3]
	add r0, #0x23
	ldrb r1, [r0]
	mov r2, #0x10
	orr r1, r2
	strb r1, [r0]
	ldr r4, [r3]
	mov r1, r8
	ldr r2, [r1, #0x38]
	mov r0, sb
	ldrb r1, [r0]
	lsl r0, r1, #2
	add r0, r0, r1
	lsl r0, r0, #2
	add r0, r6, r0
	add r2, #4
	add r2, r2, r0
	ldr r0, [r2]
	ldrh r0, [r0]
	strh r0, [r4, #0x10]
	ldr r3, [r3]
	mov r1, r8
	ldr r2, [r1, #0x38]
	mov r0, sb
	ldrb r1, [r0]
	lsl r0, r1, #2
	add r0, r0, r1
	lsl r0, r0, #2
	add r0, r6, r0
	add r2, #4
	add r2, r2, r0
	ldr r0, [r2]
	ldrh r0, [r0, #2]
	strh r0, [r3, #0x12]
_08030FF0:
	mov r3, r8
	add r1, r7, r3
	mov r0, #0
	strh r0, [r1, #0x10]
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldr r0, [r3, #0x38]
	ldrh r0, [r0]
	cmp r5, r0
	blo _08030F74
	b _08031230
	.align 2, 0
.pool
_08031010:
	mov r5, #0
	ldr r0, [r6, #0x38]
	ldrh r0, [r0]
	cmp r5, r0
	bhs _08031054
	add r4, r6, #0
	ldr r7, =0x030020AC
_0803101E:
	ldr r2, [r4, #0x38]
	lsl r3, r5, #2
	ldrb r1, [r7]
	lsl r0, r1, #2
	add r0, r0, r1
	lsl r0, r0, #2
	add r0, r3, r0
	add r2, #4
	add r2, r2, r0
	ldr r1, [r2]
	ldr r0, [r4, #4]
	ldrh r1, [r1, #4]
	cmp r0, r1
	bne _08031046
	ldr r0, =off_822F8A4
	add r0, r3, r0
	ldr r0, [r0]
	mov r1, #3
	bl createTask
_08031046:
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldr r0, [r4, #0x38]
	ldrh r0, [r0]
	cmp r5, r0
	blo _0803101E
_08031054:
	ldr r0, [r6, #4]
	add r0, #1
	str r0, [r6, #4]
	ldr r1, [r6, #0x38]
	ldrh r1, [r1, #2]
	cmp r0, r1
	bhi _08031064
	b def_8030F18
_08031064:
	mov r0, #0
	str r0, [r6, #4]
	mov r0, #2
	strh r0, [r6, #2]
	ldrh r0, [r6]
	add r0, #1
	strh r0, [r6]
	ldr r1, =dword_822F7B8
	ldrh r0, [r6]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r1, [r0]
	str r1, [r6, #0x38]
	cmp r1, #0
	bne _080310B8
	ldr r0, =0x03003D40
	ldrb r0, [r0, #0x11]
	cmp r0, #2
	beq _0803108C
	b _08031306
_0803108C:
	ldr r0, =0x03003C00
	ldrb r0, [r0, #3]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1d
	mov r1, #4
	and r0, r1
	cmp r0, #0
	bne _0803109E
	b _08031306
_0803109E:
	mov r0, #8
	b _08031308
	.align 2, 0
.pool
_080310B8:
	ldr r0, =unk_822F5F8
	cmp r1, r0
	bne _08031104
	ldr r1, =0x03003D90
	mov r0, #0xf0
	strh r0, [r1]
	ldr r1, =0x03003D88
	ldr r0, =0x00003CA0
	strh r0, [r1]
	ldr r1, =0x03003D7C
	mov r0, #0x30
	strh r0, [r1]
	ldr r1, =0x03003D8C
	mov r0, #0x10
	strh r0, [r1]
	ldr r2, =0x03003CD4
	ldrh r0, [r2]
	mov r3, #0x80
	lsl r3, r3, #6
	add r1, r3, #0
	orr r0, r1
	strh r0, [r2]
	mov r0, #3
	b _08031308
	.align 2, 0
.pool
_08031104:
	ldr r0, =unk_822F638
	cmp r1, r0
	beq _0803110C
	b def_8030F18
_0803110C:
	ldr r2, =0x03003CD4
	ldrh r1, [r2]
	ldr r0, =0x0000DFFF
	and r0, r1
	strh r0, [r2]
	b def_8030F18
	.align 2, 0
.pool
_08031124:
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x3c
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _08031138
	b def_8030F18
_08031138:
	ldr r1, =0x03000580
	mov r0, #0
	strh r0, [r1, #2]
	b def_8030F18
	.align 2, 0
.pool
_08031144:
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x3c
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _08031158
	b def_8030F18
_08031158:
	ldr r1, =0x03000580
	mov r0, #4
	strh r0, [r1, #2]
	b def_8030F18
	.align 2, 0
.pool
_08031164:
	mov r5, #1
	ldr r0, =word_822F5B8
	ldrh r0, [r0]
	cmp r5, r0
	bhs _08031188
	ldr r4, =0x03000594
_08031170:
	lsl r0, r5, #3
	add r0, r0, r4
	ldr r0, [r0]
	bl spriteFreeHeader
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldr r0, =word_822F5B8
	ldrh r0, [r0]
	cmp r5, r0
	blo _08031170
_08031188:
	ldr r3, =0x03000580
	mov r0, #0
	str r0, [r3, #4]
	mov r5, #0
	ldr r0, [r3, #0x38]
	ldrh r0, [r0]
	cmp r5, r0
	bhs _08031230
	mov r8, r3
	ldr r0, =0x030020AC
	mov sb, r0
_0803119E:
	mov r1, r8
	ldr r2, [r1, #0x38]
	lsl r6, r5, #2
	mov r3, sb
	ldrb r1, [r3]
	lsl r0, r1, #2
	add r0, r0, r1
	lsl r0, r0, #2
	add r0, r6, r0
	add r2, #4
	add r2, r2, r0
	ldr r0, [r2]
	ldr r1, [r0, #8]
	lsl r7, r5, #3
	cmp r1, #0
	beq _0803121A
	mov r0, r8
	ldrh r2, [r0, #8]
	mov r0, #0
	str r0, [sp]
	add r0, r1, #0
	mov r1, #0
	mov r3, #0
	bl allocSprite
	mov r3, r8
	add r3, #0x14
	add r3, r7, r3
	str r0, [r3]
	add r0, #0x23
	ldrb r1, [r0]
	mov r2, #0x10
	orr r1, r2
	strb r1, [r0]
	ldr r4, [r3]
	mov r1, r8
	ldr r2, [r1, #0x38]
	mov r0, sb
	ldrb r1, [r0]
	lsl r0, r1, #2
	add r0, r0, r1
	lsl r0, r0, #2
	add r0, r6, r0
	add r2, #4
	add r2, r2, r0
	ldr r0, [r2]
	ldrh r0, [r0]
	strh r0, [r4, #0x10]
	ldr r3, [r3]
	mov r1, r8
	ldr r2, [r1, #0x38]
	mov r0, sb
	ldrb r1, [r0]
	lsl r0, r1, #2
	add r0, r0, r1
	lsl r0, r0, #2
	add r0, r6, r0
	add r2, #4
	add r2, r2, r0
	ldr r0, [r2]
	ldrh r0, [r0, #2]
	strh r0, [r3, #0x12]
_0803121A:
	mov r3, r8
	add r1, r7, r3
	mov r0, #0
	strh r0, [r1, #0x10]
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldr r0, [r3, #0x38]
	ldrh r0, [r0]
	cmp r5, r0
	blo _0803119E
_08031230:
	bl gfx_related_sub_800F674
	ldr r0, =0x03000580
	mov r2, #0
	mov r1, #1
	strh r1, [r0, #2]
	strh r2, [r0, #0xc]
	strh r2, [r0, #0xe]
	b def_8030F18
	.align 2, 0
.pool
_08031254:
	mov r0, #0
	mov r1, #0xa
	bl fadeoutSong
	ldr r1, =0x03000580
	mov r0, #6
	strh r0, [r1, #2]
	b def_8030F18
	.align 2, 0
.pool
_08031268:
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0xb4
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	beq def_8030F18
	ldr r1, =0x03000580
	mov r0, #7
	strh r0, [r1, #2]
	b def_8030F18
	.align 2, 0
.pool
_08031288:
	bl clearSprites
	bl clearPalBufs
	mov r0, #2
	bl endPriorityOrHigherTask
	ldr r0, =(cb_loadTitleScreen+1)
	bl setCurrentTaskFunc
	b def_8030F18
	.align 2, 0
.pool
_080312A4:
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x3c
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	beq def_8030F18
	ldr r1, =0x03000580
	mov r0, #9
	strh r0, [r1, #2]
	b def_8030F18
	.align 2, 0
.pool
_080312C4:
	bl clearSprites
	bl drawSTR_ninjamaster
	ldr r1, =0x03000580
	mov r0, #0xa
	strh r0, [r1, #2]
	b def_8030F18
	.align 2, 0
.pool
_080312D8:
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x3c
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	beq def_8030F18
	ldr r0, =0x03000580
	mov r1, #0
	str r1, [r0, #4]
	mov r1, #0xb
	strh r1, [r0, #2]
	b def_8030F18
	.align 2, 0
.pool
_080312F8:
	ldr r0, [r6, #4]
	add r0, #1
	str r0, [r6, #4]
	cmp r0, #0xf0
	bls def_8030F18
	mov r0, #0
	str r0, [r6, #4]
_08031306:
	mov r0, #5
_08031308:
	strh r0, [r6, #2]
def_8030F18:
	add sp, #4
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	
	
	
	thumb_func_global sub_8031318
sub_8031318: @ 0x08031318
	push {lr}
	mov r0, #0
	bl sub_8031390
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0803132A
	bl endCurrentTask
_0803132A:
	pop {r0}
	bx r0
	.align 2, 0
	
	
	
	thumb_func_global sub_8031330
sub_8031330: @ 0x08031330
	push {lr}
	mov r0, #1
	bl sub_8031390
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08031342
	bl endCurrentTask
_08031342:
	pop {r0}
	bx r0
	.align 2, 0
	
	
	
	thumb_func_global sub_8031348
sub_8031348: @ 0x08031348
	push {lr}
	mov r0, #2
	bl sub_8031390
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0803135A
	bl endCurrentTask
_0803135A:
	pop {r0}
	bx r0
	.align 2, 0
	
	
	
	thumb_func_global sub_8031360
sub_8031360: @ 0x08031360
	push {lr}
	mov r0, #3
	bl sub_8031390
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08031372
	bl endCurrentTask
_08031372:
	pop {r0}
	bx r0
	.align 2, 0
	
	
	
	thumb_func_global sub_8031378
sub_8031378: @ 0x08031378
	push {lr}
	mov r0, #4
	bl sub_8031390
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0803138A
	bl endCurrentTask
_0803138A:
	pop {r0}
	bx r0
	.align 2, 0
	
	
	
	
	thumb_func_global sub_8031390
sub_8031390: @ 0x08031390
	push {r4, r5, r6, lr}
	lsl r0, r0, #0x10
	ldr r6, =0x03000580
	lsr r4, r0, #0xd
	add r3, r4, r6
	ldrh r5, [r3, #0x10]
	cmp r5, #0
	beq _080313AC
	cmp r5, #1
	beq _080313FC
	b _08031446
	.align 2, 0
.pool
_080313AC:
	add r0, r6, #0
	add r0, #0x14
	add r4, r4, r0
	ldr r2, [r4]
	cmp r2, #0
	beq _080313D6
	add r2, #0x23
	ldrb r1, [r2]
	mov r0, #0x11
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	ldr r2, [r4]
	add r2, #0x25
	ldrb r1, [r2]
	mov r0, #4
	neg r0, r0
	and r0, r1
	mov r1, #1
	orr r0, r1
	strb r0, [r2]
_080313D6:
	strh r5, [r3, #0x12]
	ldr r2, =0x03003D74
	ldrh r1, [r2]
	cmp r1, #0
	bne _080313EC
	mov r4, #0x81
	lsl r4, r4, #6
	add r0, r4, #0
	strh r0, [r2]
	ldr r0, =0x03003D94
	strh r1, [r0]
_080313EC:
	ldrh r0, [r3, #0x10]
	add r0, #1
	strh r0, [r3, #0x10]
	b _08031446
	.align 2, 0
.pool
_080313FC:
	ldrh r2, [r3, #0x12]
	lsl r1, r2, #0x10
	lsr r0, r1, #0x10
	cmp r0, #0x3a
	bls _0803143C
	mov r1, #0
	strh r1, [r3, #0x12]
	ldr r2, =0x03003D94
	mov r0, #0x10
	strh r0, [r2]
	ldr r0, =0x03003D74
	strh r1, [r0]
	strh r1, [r3, #0x10]
	add r0, r6, #0
	add r0, #0x14
	add r0, r4, r0
	ldr r0, [r0]
	cmp r0, #0
	beq _08031430
	add r2, r0, #0
	add r2, #0x25
	ldrb r1, [r2]
	mov r0, #4
	neg r0, r0
	and r0, r1
	strb r0, [r2]
_08031430:
	mov r0, #1
	b _08031448
	.align 2, 0
.pool
_0803143C:
	ldr r0, =0x03003D94
	lsr r1, r1, #0x11
	strh r1, [r0]
	add r0, r2, #1
	strh r0, [r3, #0x12]
_08031446:
	mov r0, #0
_08031448:
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool




	thumb_func_global sub_8031454
sub_8031454: @ 0x08031454
	push {r4, r5, r6, lr}
	sub sp, #8
	ldr r6, =0x03003CD4
	mov r1, #0x82
	lsl r1, r1, #5
	add r0, r1, #0
	strh r0, [r6]
	bl FreeAllPalFadeTasks
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	bl stopPalFade
	add r0, sp, #4
	mov r4, #0
	strh r4, [r0]
	mov r5, #0xa0
	lsl r5, r5, #0x13
	ldr r2, =0x01000001
	add r1, r5, #0
	bl Bios_memcpy
	mov r0, sp
	add r0, #6
	strh r4, [r0]
	ldr r1, =0x03000580
	ldr r2, =0x0100001E
	bl Bios_memcpy
	ldr r0, =word_82241EC
	mov r1, #1
	add r2, r5, #0
	bl memcpy_pal
	ldr r1, =off_822F8B8
	ldr r4, =0x03002080
	add r4, #0x2c
	ldrb r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0xc0
	lsl r1, r1, #0x13
	bl Bios_LZ77_16_2
	ldr r1, =unk_822F8C4
	ldrb r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x06008000
	mov r2, #0
	str r2, [sp]
	mov r3, #0
	bl LZ77_mapCpy
	ldr r1, =0x03003D00
	mov r0, #0x80
	lsl r0, r0, #5
	strh r0, [r1]
	ldrh r0, [r6]
	mov r2, #0x80
	lsl r2, r2, #1
	add r1, r2, #0
	orr r0, r1
	strh r0, [r6]
	ldr r0, =(sub_803151C+1)
	bl setCurrentTaskFunc
	add sp, #8
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool




	thumb_func_global sub_803151C
sub_803151C: @ 0x0803151C
	push {r4, r5, lr}
	ldr r5, =0x03000580
	ldrh r4, [r5, #2]
	cmp r4, #1
	beq _08031552
	cmp r4, #1
	bgt _08031534
	cmp r4, #0
	beq _0803153E
	b _080315A0
	.align 2, 0
.pool
_08031534:
	cmp r4, #2
	beq _08031578
	cmp r4, #3
	beq _08031592
	b _080315A0
_0803153E:
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	beq _080315A0
	str r4, [r5, #4]
	b _0803158A
_08031552:
	ldr r0, =0x03002080
	ldrh r1, [r0, #0xc]
	mov r0, #8
	and r0, r1
	cmp r0, #0
	bne _0803156C
	ldr r0, [r5, #4]
	add r0, #1
	str r0, [r5, #4]
	mov r1, #0xe1
	lsl r1, r1, #3
	cmp r0, r1
	bls _080315A0
_0803156C:
	mov r0, #0
	str r0, [r5, #4]
	b _0803158A
	.align 2, 0
.pool
_08031578:
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	beq _080315A0
_0803158A:
	ldrh r0, [r5, #2]
	add r0, #1
	strh r0, [r5, #2]
	b _080315A0
_08031592:
	bl clearSprites
	bl clearPalBufs
	ldr r0, =(cb_loadTitleScreen+1)
	bl setCurrentTaskFunc
_080315A0:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global drawSTR_ninjamaster
drawSTR_ninjamaster: @ 0x080315AC
	push {r4, r5, lr}
	sub sp, #4
	ldr r1, =ninjaMasterStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r4, [r0]
	ldr r5, =0x030005C0
	mov r0, #0
	str r0, [sp]
	add r0, r4, #0
	add r1, r5, #0
	mov r2, #0
	mov r3, #0
	bl bufferString
	add r0, r4, #0
	mov r1, #0
	bl str_get_last_width_OamX_sum
	mov r1, #0xf0
	sub r1, r1, r0
	lsl r1, r1, #0xf
	lsr r1, r1, #0x10
	str r5, [sp]
	add r0, r4, #0
	mov r2, #0x4b
	mov r3, #0
	bl setStringPos
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global cb_easymodeCredits
cb_easymodeCredits: @ 0x08031600
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl FreeAllPalFadeTasks
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	bl stopPalFade
	mov r0, #2
	bl endPriorityOrHigherTask
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	bl stopSoundTracks
	ldr r0, =(easyModeCreditsSwitch+1)
	bl setCurrentTaskFunc
	ldr r0, =0x03001F00
	mov r1, #0
	strh r1, [r0]
	ldr r0, =0x03001F02
	strh r1, [r0]
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global easyModeCreditsSwitch
easyModeCreditsSwitch: @ 0x08031658
	push {r4, lr}
	ldr r1, =easymodeEndFuncs
	ldr r4, =0x03001F00
	ldrh r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl call_r0
	lsl r0, r0, #0x10
	asr r1, r0, #0x10
	mov r0, #1
	neg r0, r0
	cmp r1, r0
	beq _0803168C
	cmp r1, #1
	bne _08031692
	ldrh r0, [r4]
	add r0, #1
	strh r0, [r4]
	b _08031692
	.align 2, 0
.pool
_0803168C:
	ldr r0, =(sub_8031454+1)
	bl setCurrentTaskFunc
_08031692:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global loadSTR_toBeContinued
loadSTR_toBeContinued: @ 0x0803169C
	push {lr}
	ldr r0, =0x03001EF0
	mov r1, #0
	bl allocFont
	ldr r0, =0x03001EF8
	mov r1, #1
	bl allocFont
	bl drawSTRING_ToBeContinued
	mov r0, #1
	pop {r1}
	bx r1
	.align 2, 0
.pool



	thumb_func_global sub_80316C0
sub_80316C0: @ 0x080316C0
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _080316D6
	mov r0, #0
	b _080316DE
_080316D6:
	ldr r1, =0x03001F02
	mov r0, #0
	strh r0, [r1]
	mov r0, #1
_080316DE:
	pop {r1}
	bx r1
	.align 2, 0
.pool



	thumb_func_global sub_80316E8
sub_80316E8: @ 0x080316E8
	ldr r1, =0x03001F02
	ldrh r0, [r1]
	add r0, #1
	strh r0, [r1]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0x3c
	bhi _08031700
	mov r0, #0
	b locret_8031706
	.align 2, 0
.pool
_08031700:
	mov r0, #0
	strh r0, [r1]
	mov r0, #1
locret_8031706:
	bx lr
	
	
	
	thumb_func_global state_draw_TryNormalMode
state_draw_TryNormalMode: @ 0x08031708
	push {lr}
	bl drawSTR_tryNormalMode
	mov r0, #0xdf
	bl playSong
	mov r0, #1
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	thumb_func_global sub_803171C
sub_803171C: @ 0x0803171C
	push {r4, r5, lr}
	sub sp, #4
	ldr r1, =TryNormalStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r5, [r0]
	add r0, r5, #0
	mov r1, #0xe
	bl str_getX_ofFirstChar
	add r4, r0, #0
	add r0, r5, #0
	mov r1, #0xe
	bl str_get_last_width_OamX_sum
	add r2, r4, #0
	sub r2, #0xc
	sub r0, r0, r4
	mov r1, #0xf0
	sub r1, r1, r0
	asr r1, r1, #1
	cmp r2, r1
	blt _08031774
	lsl r1, r2, #0x10
	lsr r1, r1, #0x10
	ldr r0, =0x03001EF8
	str r0, [sp]
	add r0, r5, #0
	mov r2, #0x4b
	mov r3, #0xe
	bl setStringPos
	mov r0, #0
	b _0803178E
	.align 2, 0
.pool
_08031774:
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r0, =0x03001EF8
	str r0, [sp]
	add r0, r5, #0
	mov r2, #0x4b
	mov r3, #0xe
	bl setStringPos
	ldr r1, =0x03001F02
	mov r0, #0
	strh r0, [r1]
	mov r0, #1
_0803178E:
	add sp, #4
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
.pool
	thumb_func_global sub_80317A0
sub_80317A0: @ 0x080317A0
	ldr r0, =0x03002080
	ldrh r1, [r0, #0xc]
	mov r0, #8
	and r0, r1
	cmp r0, #0
	bne _080317B4
	mov r0, #0
	b locret_80317B6
	.align 2, 0
.pool
_080317B4:
	mov r0, #1
locret_80317B6:
	bx lr
	thumb_func_global state_fadeToBlack
state_fadeToBlack: @ 0x080317B8
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	mov r3, #0x10
	bl fadeScreen
	add r1, r0, #0
	lsl r1, r1, #0x18
	cmp r1, #0
	beq _080317D0
	mov r0, #1
_080317D0:
	pop {r1}
	bx r1
	thumb_func_global sub_80317D4
sub_80317D4: @ 0x080317D4
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl FreeAllPalFadeTasks
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	bl stopPalFade
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	mov r0, #1
	neg r0, r0
	pop {r1}
	bx r1
	.align 2, 0
.pool
	thumb_func_global drawSTRING_ToBeContinued
drawSTRING_ToBeContinued: @ 0x08031808
	push {r4, r5, lr}
	sub sp, #4
	ldr r1, =ToBeContinuedStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r4, [r0]
	ldr r5, =0x03001EF0
	mov r0, #0
	str r0, [sp]
	add r0, r4, #0
	add r1, r5, #0
	mov r2, #0
	mov r3, #0
	bl bufferString
	add r0, r4, #0
	mov r1, #0
	bl str_get_last_width_OamX_sum
	mov r1, #0xe8
	sub r1, r1, r0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	str r5, [sp]
	add r0, r4, #0
	mov r2, #0x8c
	mov r3, #0
	bl setStringPos
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global drawSTR_tryNormalMode
drawSTR_tryNormalMode: @ 0x0803185C
	push {r4, r5, lr}
	sub sp, #4
	ldr r1, =TryNormalStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r5, [r0]
	ldr r4, =0x03001EF8
	mov r0, #0
	str r0, [sp]
	add r0, r5, #0
	add r1, r4, #0
	mov r2, #0xe
	mov r3, #0
	bl bufferString
	str r4, [sp]
	add r0, r5, #0
	mov r1, #0xf0
	mov r2, #0x4b
	mov r3, #0xe
	bl setStringPos
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_80318A4
sub_80318A4: @ 0x080318A4
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl FreeAllPalFadeTasks
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	bl stopPalFade
	mov r0, #2
	bl endPriorityOrHigherTask
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	bl stopSoundTracks
	ldr r0, =(sub_80318FC+1)
	bl setCurrentTaskFunc
	ldr r0, =0x03001F00
	mov r1, #0
	strh r1, [r0]
	ldr r0, =0x03001F02
	strh r1, [r0]
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_80318FC
sub_80318FC: @ 0x080318FC
	push {r4, lr}
	ldr r1, =off_822FAF8
	ldr r4, =0x03001F00
	ldrh r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl call_r0
	lsl r0, r0, #0x10
	asr r1, r0, #0x10
	mov r0, #1
	neg r0, r0
	cmp r1, r0
	beq _08031930
	cmp r1, #1
	bne _08031936
	ldrh r0, [r4]
	add r0, #1
	strh r0, [r4]
	b _08031936
	.align 2, 0
.pool
_08031930:
	ldr r0, =(sub_8031454+1)
	bl setCurrentTaskFunc
_08031936:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_8031940
sub_8031940: @ 0x08031940
	push {lr}
	ldr r0, =0x03001EF0
	mov r1, #0
	bl allocFont
	ldr r0, =0x03001EF8
	mov r1, #1
	bl allocFont
	bl sub_8031A40
	bl sub_8031AC0
	mov r0, #5
	bl playSong
	mov r0, #1
	pop {r1}
	bx r1
	.align 2, 0
.pool
	thumb_func_global sub_8031970
sub_8031970: @ 0x08031970
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0803198A
	bl sub_8031A94
	mov r0, #0
	b _08031992
_0803198A:
	ldr r1, =0x03001F02
	mov r0, #0
	strh r0, [r1]
	mov r0, #1
_08031992:
	pop {r1}
	bx r1
	.align 2, 0
.pool
	thumb_func_global sub_803199C
sub_803199C: @ 0x0803199C
	push {r4, lr}
	ldr r4, =0x03001F02
	ldrh r0, [r4]
	add r0, #1
	strh r0, [r4]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0x78
	bhi _080319BC
	bl sub_8031A94
	mov r0, #0
	b _080319C6
	.align 2, 0
.pool
_080319BC:
	bl sub_8031A40
	mov r0, #0
	strh r0, [r4]
	mov r0, #1
_080319C6:
	pop {r4}
	pop {r1}
	bx r1
	thumb_func_global sub_80319CC
sub_80319CC: @ 0x080319CC
	ldr r0, =0x03002080
	ldrh r1, [r0, #0xc]
	mov r0, #8
	and r0, r1
	cmp r0, #0
	bne _080319E0
	mov r0, #0
	b locret_80319E8
	.align 2, 0
.pool
_080319E0:
	ldr r1, =0x03001F02
	mov r0, #0
	strh r0, [r1]
	mov r0, #1
locret_80319E8:
	bx lr
	.align 2, 0
.pool



	thumb_func_global sub_80319F0
sub_80319F0: @ 0x080319F0
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	mov r3, #0x10
	bl fadeScreen
	add r1, r0, #0
	lsl r1, r1, #0x18
	cmp r1, #0
	beq _08031A08
	mov r0, #1
_08031A08:
	pop {r1}
	bx r1
	
	
	
	thumb_func_global sub_8031A0C
sub_8031A0C: @ 0x08031A0C
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl FreeAllPalFadeTasks
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	bl stopPalFade
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	mov r0, #1
	neg r0, r0
	pop {r1}
	bx r1
	.align 2, 0
.pool



	thumb_func_global sub_8031A40
sub_8031A40: @ 0x08031A40
	push {r4, r5, lr}
	sub sp, #4
	ldr r1, =congratzStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r4, [r0]
	ldr r5, =0x03001EF0
	mov r0, #0
	str r0, [sp]
	add r0, r4, #0
	add r1, r5, #0
	mov r2, #0
	mov r3, #0
	bl bufferString
	add r0, r4, #0
	mov r1, #0
	bl str_get_last_width_OamX_sum
	mov r1, #0xf0
	sub r1, r1, r0
	lsl r1, r1, #0xf
	lsr r1, r1, #0x10
	str r5, [sp]
	add r0, r4, #0
	mov r2, #0x46
	mov r3, #0
	bl setStringPos
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_8031A94
sub_8031A94: @ 0x08031A94
	push {lr}
	ldr r2, =congratzStrings
	ldr r1, =0x03002080
	add r0, r1, #0
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r0, [r0]
	ldr r2, [r1]
	mov r1, #8
	and r2, r1
	lsr r2, r2, #3
	mov r1, #0
	bl setStringVisibility
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_8031AC0
sub_8031AC0: @ 0x08031AC0
	push {r4, r5, lr}
	sub sp, #4
	ldr r1, =ThanksForPlayingStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r4, [r0]
	ldr r5, =0x03001EF8
	mov r0, #0
	str r0, [sp]
	add r0, r4, #0
	add r1, r5, #0
	mov r2, #0x10
	mov r3, #0
	bl bufferString
	add r0, r4, #0
	mov r1, #0x10
	bl str_get_last_width_OamX_sum
	mov r1, #0xf0
	sub r1, r1, r0
	lsl r1, r1, #0xf
	lsr r1, r1, #0x10
	str r5, [sp]
	add r0, r4, #0
	mov r2, #0x5f
	mov r3, #0x10
	bl setStringPos
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

