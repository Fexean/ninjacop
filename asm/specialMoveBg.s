.include "asm/macros.inc"


/* void loadSpecialMoveGfx(void) */
thumb_func_global loadSpecialMoveGfx
loadSpecialMoveGfx: @ 0x08012E24
	push {lr}
	sub sp, #4
	ldr r0, =ninjutsu_pal
	ldr r2, =0x050001C0
	mov r1, #1
	bl memcpy_pal
	ldr r0, =ninjutsu_tiles
	ldr r1, =0x06008000
	bl Bios_LZ77_16_2
	ldr r0, =ninjutsu_map
	ldr r1, =0x0600D000
	mov r2, #0xe
	str r2, [sp]
	mov r2, #0
	mov r3, #0
	bl LZ77_mapCpy
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool


/* void setBG0_killSpecial(void) 
	-sets BG0CNT to 0x1A08
*/
thumb_func_global setBG0_killSpecial
setBG0_killSpecial: @ 0x08012E68
	ldr r1, =0x03003D00
	ldr r0, =0x00001A08
	strh r0, [r1]
	bx lr
	.align 2, 0
.pool


/* void setBG0_toConst(void)
	-sets BG0CNT to 0x1F0C
*/
thumb_func_global setBG0_toConst
setBG0_toConst: @ 0x08012E78
	ldr r1, =0x03003D00
	ldr r0, =0x00001F0C
	strh r0, [r1]
	bx lr
	.align 2, 0
.pool
