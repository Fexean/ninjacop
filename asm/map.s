.include "asm/macros.inc"


@completely disassembled


thumb_func_global maybe_map_load
maybe_map_load: @ 0x08011B44
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #8
	add r6, r0, #0
	ldrb r0, [r6]
	lsl r0, r0, #0x1e
	lsr r0, r0, #0x1e
	cmp r0, #3
	bgt _08011BF0
	ldr r1, [r6, #8]
	mov r8, r1
	cmp r1, #0
	beq _08011B6A
	add r0, #1
	cmp r0, #3
	bgt _08011BF0
_08011B6A:
	ldrb r1, [r6]
	lsl r0, r1, #0x1c
	lsr r0, r0, #0x1e
	add r5, r1, #0
	cmp r0, #3
	bgt _08011BF0
	ldr r2, [r6, #4]
	ldrb r7, [r2]
	ldrb r3, [r2, #1]
	mov sb, r3
	mov r0, sb
	mul r0, r7, r0
	cmp r0, #0x10
	bgt _08011BF0
	ldrb r0, [r2, #0x10]
	cmp r0, #0x20
	bhi _08011BF0
	ldrb r0, [r2, #0x11]
	cmp r0, #0x20
	bhi _08011BF0
	ldrb r1, [r6, #1]
	lsl r0, r1, #0x1c
	lsr r0, r0, #0x1c
	cmp r0, #0xf
	bgt _08011BF0
	lsl r0, r1, #0x1a
	lsr r4, r0, #0x1e
	cmp r4, #3
	bgt _08011BF0
	ldr r1, [r6]
	lsl r0, r1, #0xd
	lsr r3, r0, #0x1b
	mov ip, r1
	cmp r3, #0x1f
	bgt _08011BF0
	lsl r0, r4, #0xe
	ldrh r1, [r6, #2]
	lsr r1, r1, #3
	lsl r1, r1, #5
	add r0, r0, r1
	lsl r1, r3, #0xb
	cmp r0, r1
	bgt _08011BF0
	ldrb r1, [r2, #0x10]
	ldrb r0, [r2, #0x11]
	mul r1, r0, r1
	lsl r0, r7, #1
	mul r0, r1, r0
	mov r1, sb
	mul r1, r0, r1
	mov sb, r1
	mov r0, #0xc0
	lsl r0, r0, #8
	cmp sb, r0
	bhi _08011BF0
	ldr r0, [r2, #0x74]
	cmp r0, #0
	beq _08011BF4
	cmp r3, #0x13
	ble _08011BF0
	lsl r0, r5, #0x1e
	cmp r0, #0
	beq _08011BF0
	lsl r0, r5, #0x1c
	lsr r0, r0, #0x1e
	cmp r0, #0
	bgt _08011BF4
_08011BF0:
	mov r0, #1
	b _08012140
_08011BF4:
	ldr r2, =0x030046F0
	lsl r0, r5, #0x1e
	lsr r0, r0, #0x1e
	mov r5, #0
	strb r0, [r2, #1]
	ldrh r0, [r6, #2]
	lsr r0, r0, #3
	strh r0, [r2, #2]
	ldrb r0, [r6, #1]
	lsl r1, r0, #0x1c
	lsr r7, r1, #0x1c
	strb r7, [r2, #4]
	lsl r0, r0, #0x1a
	lsr r0, r0, #0x1e
	strb r0, [r2, #5]
	mov r3, ip
	lsl r0, r3, #0xd
	lsr r0, r0, #0x1b
	strb r0, [r2, #6]
	ldr r4, [r6, #4]
	str r4, [r2, #8]
	ldrh r1, [r6, #0xc]
	strh r1, [r2, #0x14]
	ldrh r3, [r6, #0xe]
	strh r3, [r2, #0x16]
	mov r0, r8
	str r0, [r2, #0x24]
	strh r5, [r2, #0x1a]
	strh r5, [r2, #0x18]
	sub r0, r1, #1
	strh r0, [r2, #0x1c]
	sub r3, #1
	strh r3, [r2, #0x1e]
	lsl r1, r1, #0x10
	lsr r1, r1, #0x13
	strh r1, [r2, #0x20]
	ldr r1, [r4, #4]
	cmp r1, #0
	beq _08011C52
	lsl r2, r7, #5
	mov r0, #0xa0
	lsl r0, r0, #0x13
	add r2, r2, r0
	add r0, r1, #0
	mov r1, #0xb
	bl memcpy_pal
_08011C52:
	ldr r1, [r6, #4]
	ldr r4, [r1, #0xc]
	cmp r4, #0
	beq _08011CA4
	ldrb r0, [r1, #8]
	cmp r0, #0
	bne _08011C88
	ldrh r1, [r1, #0xa]
	lsl r1, r1, #0x15
	lsr r1, r1, #0x10
	ldrb r2, [r6, #1]
	lsl r2, r2, #0x1a
	lsr r2, r2, #0x1e
	lsl r2, r2, #0xe
	ldrh r0, [r6, #2]
	lsr r0, r0, #3
	lsl r0, r0, #5
	mov r3, #0xc0
	lsl r3, r3, #0x13
	add r0, r0, r3
	add r2, r2, r0
	add r0, r4, #0
	bl Bios__memcpy_bytecount
	b _08011CA4
	.align 2, 0
.pool
_08011C88:
	ldrb r1, [r6, #1]
	lsl r1, r1, #0x1a
	lsr r1, r1, #0x1e
	lsl r1, r1, #0xe
	ldrh r0, [r6, #2]
	lsr r0, r0, #3
	lsl r0, r0, #5
	mov r2, #0xc0
	lsl r2, r2, #0x13
	add r0, r0, r2
	add r1, r1, r0
	add r0, r4, #0
	bl Bios_LZ77_16_2
_08011CA4:
	add r1, sp, #4
	mov r0, #0
	strh r0, [r1]
	ldrb r1, [r6, #1]
	lsl r1, r1, #0x1a
	lsr r1, r1, #0x1e
	lsl r1, r1, #0xe
	ldrh r0, [r6, #2]
	lsr r0, r0, #3
	mov r2, #0xbe
	lsl r2, r2, #2
	add r0, r0, r2
	lsl r0, r0, #5
	mov r2, #0xc0
	lsl r2, r2, #0x13
	add r0, r0, r2
	add r1, r1, r0
	ldr r2, =0x01000080
	add r0, sp, #4
	bl Bios_memcpy
	ldr r0, =unk_8039e68
	ldr r2, =0x05000160
	mov r1, #1
	bl memcpy_pal
	ldr r0, =off_803A1B8
	mov r1, #0xa0
	lsl r1, r1, #0x13
	mov r2, #0
	bl startPalAnimation
	mov r7, #0
	ldr r1, [r6, #4]
	ldrb r3, [r1, #1]
	cmp r7, r3
	blo _08011CF0
	b _08011FB0
_08011CF0:
	mov r0, #0
	mov r8, r0
	ldrb r2, [r1]
	cmp r8, r2
	blo _08011CFC
	b _08011FA0
_08011CFC:
	ldrb r0, [r1, #0x12]
	cmp r0, #0
	bne _08011D3C
	ldrb r0, [r1]
	mul r0, r7, r0
	add r0, r8
	lsl r0, r0, #2
	add r1, #0x14
	add r1, r1, r0
	ldr r0, [r1]
	ldrh r3, [r6, #2]
	lsr r3, r3, #3
	ldrb r1, [r6, #1]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	str r1, [sp]
	ldr r1, =0x02001920
	mov r2, #0
	bl tilemapCpy
	b _08011D5E
	.align 2, 0
.pool
_08011D3C:
	ldrb r0, [r1]
	mul r0, r7, r0
	add r0, r8
	lsl r0, r0, #2
	add r1, #0x14
	add r1, r1, r0
	ldr r0, [r1]
	ldrh r3, [r6, #2]
	lsr r3, r3, #3
	ldrb r1, [r6, #1]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	str r1, [sp]
	ldr r1, =0x02001920
	mov r2, #0
	bl LZ77_mapCpy
_08011D5E:
	ldr r3, [r6, #4]
	ldrb r1, [r3, #0x10]
	lsl r1, r1, #1
	ldrb r0, [r3]
	mul r0, r7, r0
	ldrb r2, [r3, #0x11]
	mul r0, r2, r0
	add r0, r8
	mul r1, r0, r1
	ldr r0, =0x0200E630
	add r5, r1, r0
	mov r4, #0
	mov r0, #1
	add r0, r8
	mov sl, r0
	cmp r4, r2
	bhs _08011DA2
_08011D80:
	lsl r0, r4, #6
	ldr r1, =0x02001920
	add r0, r0, r1
	ldrb r2, [r3, #0x10]
	add r1, r5, #0
	bl Bios_memcpy
	ldrh r0, [r6, #0xc]
	lsr r0, r0, #2
	add r5, r5, r0
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	ldr r3, [r6, #4]
	ldrb r1, [r3, #0x11]
	cmp r4, r1
	blo _08011D80
_08011DA2:
	ldr r2, [r6, #4]
	ldr r0, [r2, #0x74]
	cmp r0, #0
	beq _08011E50
	mov r3, sb
	lsl r1, r3, #1
	ldr r0, =0x0000BFFF
	cmp r1, r0
	bhi _08011E50
	ldrb r0, [r2, #0x12]
	cmp r0, #0
	bne _08011DEC
	ldrb r0, [r2]
	mul r0, r7, r0
	add r0, r8
	lsl r0, r0, #2
	add r1, r2, #0
	add r1, #0x74
	add r1, r1, r0
	ldr r0, [r1]
	ldrh r3, [r6, #2]
	lsr r3, r3, #3
	ldrb r1, [r6, #1]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	str r1, [sp]
	ldr r1, =0x02001920
	mov r2, #0
	bl tilemapCpy
	b _08011E10
	.align 2, 0
.pool
_08011DEC:
	ldrb r0, [r2]
	mul r0, r7, r0
	add r0, r8
	lsl r0, r0, #2
	add r1, r2, #0
	add r1, #0x74
	add r1, r1, r0
	ldr r0, [r1]
	ldrh r3, [r6, #2]
	lsr r3, r3, #3
	ldrb r1, [r6, #1]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	str r1, [sp]
	ldr r1, =0x02001920
	mov r2, #0
	bl LZ77_mapCpy
_08011E10:
	ldr r3, [r6, #4]
	ldrb r1, [r3, #0x10]
	lsl r1, r1, #1
	ldrb r0, [r3]
	mul r0, r7, r0
	ldrb r2, [r3, #0x11]
	mul r0, r2, r0
	add r0, r8
	mul r0, r1, r0
	add r0, sb
	ldr r1, =0x0200E630
	add r5, r0, r1
	mov r4, #0
	cmp r4, r2
	bhs _08011E50
_08011E2E:
	lsl r0, r4, #6
	ldr r1, =0x02001920
	add r0, r0, r1
	ldrb r2, [r3, #0x10]
	add r1, r5, #0
	bl Bios_memcpy
	ldrh r0, [r6, #0xc]
	lsr r0, r0, #2
	add r5, r5, r0
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	ldr r3, [r6, #4]
	ldrb r0, [r3, #0x11]
	cmp r4, r0
	blo _08011E2E
_08011E50:
	ldr r3, [r6, #4]
	mov r1, #0xa4
	lsl r1, r1, #1
	add r4, r3, r1
	ldr r0, [r4]
	cmp r0, #0
	beq _08011F00
	mov r2, sb
	lsl r0, r2, #1
	add r2, r0, r2
	ldr r1, =0x0000BFFF
	add r5, r0, #0
	cmp r2, r1
	bhi _08011F00
	ldrb r0, [r3, #0x12]
	cmp r0, #0
	bne _08011EA0
	ldrb r0, [r3]
	mul r0, r7, r0
	add r0, r8
	lsl r0, r0, #2
	add r0, r4, r0
	ldr r0, [r0]
	ldrh r3, [r6, #2]
	lsr r3, r3, #3
	ldrb r1, [r6, #1]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	str r1, [sp]
	ldr r1, =0x02001920
	mov r2, #0
	bl tilemapCpy
	b _08011EC0
	.align 2, 0
.pool
_08011EA0:
	ldrb r0, [r3]
	mul r0, r7, r0
	add r0, r8
	lsl r0, r0, #2
	add r0, r4, r0
	ldr r0, [r0]
	ldrh r3, [r6, #2]
	lsr r3, r3, #3
	ldrb r1, [r6, #1]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	str r1, [sp]
	ldr r1, =0x02001920
	mov r2, #0
	bl LZ77_mapCpy
_08011EC0:
	ldr r3, [r6, #4]
	ldrb r1, [r3, #0x10]
	lsl r1, r1, #1
	ldrb r0, [r3]
	mul r0, r7, r0
	ldrb r2, [r3, #0x11]
	mul r0, r2, r0
	add r0, r8
	mul r0, r1, r0
	add r0, r0, r5
	ldr r1, =0x0200E630
	add r5, r0, r1
	mov r4, #0
	cmp r4, r2
	bhs _08011F00
_08011EDE:
	lsl r0, r4, #6
	ldr r1, =0x02001920
	add r0, r0, r1
	ldrb r2, [r3, #0x10]
	add r1, r5, #0
	bl Bios_memcpy
	ldrh r0, [r6, #0xc]
	lsr r0, r0, #2
	add r5, r5, r0
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	ldr r3, [r6, #4]
	ldrb r0, [r3, #0x11]
	cmp r4, r0
	blo _08011EDE
_08011F00:
	ldr r3, [r6, #4]
	add r0, r3, #0
	add r0, #0xd4
	ldrb r0, [r0]
	cmp r0, #0
	bne _08011F34
	ldrb r0, [r3]
	mul r0, r7, r0
	add r0, r8
	lsl r0, r0, #2
	add r1, r3, #0
	add r1, #0xd8
	add r1, r1, r0
	ldr r0, [r1]
	ldrb r2, [r3, #0x10]
	ldrb r1, [r3, #0x11]
	mul r2, r1, r2
	ldr r1, =0x02001920
	bl Bios_memcpy
	b _08011F4A
	.align 2, 0
.pool
_08011F34:
	ldrb r0, [r3]
	mul r0, r7, r0
	add r0, r8
	lsl r0, r0, #2
	add r1, r3, #0
	add r1, #0xd8
	add r1, r1, r0
	ldr r0, [r1]
	ldr r1, =0x02001920
	bl Bios_LZ77_8bit
_08011F4A:
	ldr r3, [r6, #4]
	ldrb r1, [r3, #0x10]
	ldrb r0, [r3]
	mul r0, r7, r0
	ldrb r2, [r3, #0x11]
	mul r0, r2, r0
	add r0, r8
	mul r1, r0, r1
	ldr r0, =0x0200A630
	add r5, r1, r0
	mov r4, #0
	cmp r4, r2
	bhs _08011F8E
_08011F64:
	ldr r0, =0x030046F0
	ldr r0, [r0, #8]
	ldrb r0, [r0, #0x10]
	mul r0, r4, r0
	ldr r1, =0x02001920
	add r0, r0, r1
	ldrb r2, [r3, #0x10]
	lsr r2, r2, #1
	add r1, r5, #0
	bl Bios_memcpy
	ldrh r0, [r6, #0xc]
	lsr r0, r0, #3
	add r5, r5, r0
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	ldr r3, [r6, #4]
	ldrb r1, [r3, #0x11]
	cmp r4, r1
	blo _08011F64
_08011F8E:
	mov r2, sl
	lsl r0, r2, #0x10
	lsr r0, r0, #0x10
	mov r8, r0
	ldr r1, [r6, #4]
	ldrb r3, [r1]
	cmp r8, r3
	bhs _08011FA0
	b _08011CFC
_08011FA0:
	add r0, r7, #1
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	ldr r1, [r6, #4]
	ldrb r0, [r1, #1]
	cmp r7, r0
	bhs _08011FB0
	b _08011CF0
_08011FB0:
	ldr r4, =0x03003D00
	ldrb r2, [r6]
	lsl r3, r2, #0x1e
	lsr r3, r3, #0x1d
	add r3, r3, r4
	ldrb r1, [r6, #1]
	lsl r1, r1, #0x1a
	lsr r1, r1, #0x1e
	lsl r1, r1, #2
	ldr r0, [r6]
	lsl r0, r0, #0xd
	lsr r0, r0, #0x1b
	lsl r0, r0, #8
	orr r1, r0
	lsl r2, r2, #0x1c
	lsr r2, r2, #0x1e
	orr r2, r1
	strh r2, [r3]
	ldr r0, [r6, #4]
	ldr r0, [r0, #0x74]
	cmp r0, #0
	beq _08012002
	ldrb r2, [r6]
	lsl r3, r2, #0x1e
	lsr r3, r3, #0x1d
	sub r3, #2
	add r3, r3, r4
	ldrb r1, [r6, #1]
	lsl r1, r1, #0x1a
	lsr r1, r1, #0x1e
	lsl r1, r1, #2
	ldr r0, [r6]
	lsl r0, r0, #0xd
	lsr r0, r0, #0x1b
	add r0, #1
	lsl r0, r0, #8
	orr r1, r0
	lsl r2, r2, #0x1a
	lsr r2, r2, #0x1e
	orr r2, r1
	strh r2, [r3]
_08012002:
	ldr r0, [r6, #4]
	mov r1, #0xa4
	lsl r1, r1, #1
	add r0, r0, r1
	ldr r0, [r0]
	cmp r0, #0
	beq _08012034
	ldrb r3, [r6]
	lsl r2, r3, #0x1e
	lsr r2, r2, #0x1d
	sub r2, #4
	add r2, r2, r4
	ldrb r1, [r6, #1]
	lsl r1, r1, #0x1a
	lsr r1, r1, #0x1e
	lsl r1, r1, #2
	ldr r0, [r6]
	lsl r0, r0, #0xd
	lsr r0, r0, #0x1b
	add r0, #2
	lsl r0, r0, #8
	orr r1, r0
	lsr r3, r3, #6
	orr r3, r1
	strh r3, [r2]
_08012034:
	ldr r2, [r6, #8]
	mov r8, r2
	cmp r2, #0
	beq _080120FE
	ldr r0, [r6]
	lsl r0, r0, #0xd
	lsr r0, r0, #0x1b
	add r0, #2
	lsl r0, r0, #0xb
	mov r1, #0xc0
	lsl r1, r1, #0x13
	add r5, r0, r1
	mov r7, #0
	ldr r1, =parallaxChunkCounts
	ldrb r0, [r2, #6]
	lsl r0, r0, #1
	add r0, r0, r1
	b _080120CA
	.align 2, 0
.pool
_0801206C:
	ldr r0, [r6, #4]
	ldrb r0, [r0, #0x12]
	cmp r0, #0
	bne _08012094
	lsl r1, r7, #2
	add r0, r2, #0
	add r0, #0x10
	add r0, r0, r1
	ldr r0, [r0]
	ldrh r3, [r6, #2]
	lsr r3, r3, #3
	ldrb r1, [r6, #1]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	str r1, [sp]
	add r1, r5, #0
	mov r2, #0
	bl tilemapCpy
	b _080120B2
_08012094:
	ldr r0, [r6, #8]
	lsl r1, r7, #2
	add r0, #0x10
	add r0, r0, r1
	ldr r0, [r0]
	ldrh r3, [r6, #2]
	lsr r3, r3, #3
	ldrb r1, [r6, #1]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	str r1, [sp]
	add r1, r5, #0
	mov r2, #0
	bl LZ77_mapCpy
_080120B2:
	mov r2, #0x80
	lsl r2, r2, #4
	add r5, r5, r2
	add r0, r7, #1
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	ldr r1, =parallaxChunkCounts
	ldr r2, [r6, #8]
	ldrb r0, [r2, #6]
	lsl r0, r0, #1
	add r0, r0, r1
	mov r8, r2
_080120CA:
	ldrh r0, [r0]
	cmp r7, r0
	blo _0801206C
	ldr r0, =0x03003D00
	ldrb r2, [r6]
	lsl r2, r2, #0x1e
	lsr r2, r2, #0x1d
	add r2, #2
	add r2, r2, r0
	mov r3, r8
	ldrb r1, [r3, #6]
	lsl r1, r1, #0xe
	ldrb r0, [r6, #1]
	lsl r0, r0, #0x1a
	lsr r0, r0, #0x1e
	lsl r0, r0, #2
	orr r1, r0
	ldr r0, [r6]
	lsl r0, r0, #0xd
	lsr r0, r0, #0x1b
	add r0, #2
	lsl r0, r0, #8
	orr r1, r0
	mov r0, #3
	orr r1, r0
	strh r1, [r2]
_080120FE:
	mov r7, #0
_08012100:
	ldr r0, [r6, #4]
	lsl r1, r7, #2
	mov r2, #0x9c
	lsl r2, r2, #1
	add r0, r0, r2
	add r0, r0, r1
	ldr r0, [r0]
	cmp r0, #0
	beq _08012116
	bl call_r0
_08012116:
	add r0, r7, #1
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	cmp r7, #3
	bls _08012100
	ldr r1, =0x030046F0
	mov r2, #0
	mov r0, #1
	strb r0, [r1]
	strb r2, [r1, #0x12]
	ldr r1, =0x03000D48
	ldr r3, =0x0000FFFF
	add r0, r3, #0
	strh r0, [r1]
	bl loadSpecialMoveGfx
	ldr r0, =(cb_handleLvlBackgrounds+1)
	mov r1, #0
	bl addCallback
	mov r0, #0
_08012140:
	add sp, #8
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global handleParallaxScroll
handleParallaxScroll: @ 0x08012168
	push {r4, r5, r6, r7, lr}
	add r6, r0, #0
	add r7, r1, #0
	ldr r4, =0x030046F0
	mvn r1, r7
	ldrh r0, [r4, #0x16]
	add r1, r1, r0
	mov r2, #7
	add r0, r6, #0
	and r0, r2
	strb r0, [r4, #0x10]
	add r0, r1, #0
	and r0, r2
	strb r0, [r4, #0x11]
	ldrb r2, [r4, #0x10]
	sub r0, r6, r2
	lsr r5, r0, #3
	ldrb r0, [r4, #0x11]
	sub r1, r1, r0
	lsr r3, r1, #3
	cmp r5, #0
	beq _0801219C
	sub r5, #1
	add r0, r2, #0
	add r0, #8
	strb r0, [r4, #0x10]
_0801219C:
	cmp r3, #0
	beq _080121A8
	sub r3, #1
	ldrb r0, [r4, #0x11]
	add r0, #8
	strb r0, [r4, #0x11]
_080121A8:
	ldrh r0, [r4, #0x14]
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	mov ip, r1
	lsr r0, r0, #0x12
	mul r0, r3, r0
	lsl r1, r5, #1
	add r0, r0, r1
	ldr r1, =0x0200E630
	add r0, r0, r1
	str r0, [r4, #0xc]
	ldr r2, [r4, #0x24]
	cmp r2, #0
	bne _080121C6
	b _08012310
_080121C6:
	ldrb r0, [r2]
	cmp r0, #1
	beq _08012240
	cmp r0, #1
	bgt _080121E0
	cmp r0, #0
	beq _080121E6
	b _080122F2
	.align 2, 0
.pool
_080121E0:
	cmp r0, #2
	beq _0801228A
	b _080122F2
_080121E6:
	mov r0, ip
	cmp r0, #0xf0
	bls _080121FE
	ldrh r0, [r2, #0xa]
	sub r0, #0xf0
	mul r0, r6, r0
	ldrh r1, [r4, #0x14]
	sub r1, #0xf0
	bl Bios_Div
	add r5, r0, #0
	b _08012200
_080121FE:
	mov r5, #0
_08012200:
	ldr r4, =0x030046F0
	ldrh r0, [r4, #0x16]
	cmp r0, #0xa0
	bls _0801223C
	add r1, r7, #0
	sub r1, #0x9f
	ldr r0, [r4, #0x24]
	ldrh r0, [r0, #0xc]
	sub r0, #0xa0
	mul r0, r1, r0
	ldrh r1, [r4, #0x16]
	sub r1, #0xa0
	bl Bios_Div
	add r3, r0, #0
	ldr r1, =byte_803A1F0
	ldr r0, [r4, #0x24]
	ldrb r0, [r0, #6]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r1, [r0]
	add r0, r3, #0
	add r0, #0xa0
	sub r3, r1, r0
	b _080122F2
	.align 2, 0
.pool
_0801223C:
	mov r3, #0
	b _080122F2
_08012240:
	ldrh r1, [r2, #2]
	mov r3, #0x80
	lsl r3, r3, #8
	add r0, r3, #0
	and r0, r1
	cmp r0, #0
	beq _08012254
	ldrh r0, [r2, #2]
	sub r0, r3, r0
	b _08012256
_08012254:
	ldrh r0, [r2, #2]
_08012256:
	mul r0, r6, r0
	lsr r5, r0, #8
	ldr r3, =0x030046F0
	ldr r2, [r3, #0x24]
	ldrh r1, [r2, #4]
	mov r4, #0x80
	lsl r4, r4, #8
	add r0, r4, #0
	and r0, r1
	cmp r0, #0
	beq _0801227C
	mvn r0, r7
	ldrh r3, [r3, #0x16]
	add r0, r0, r3
	ldrh r1, [r2, #4]
	sub r1, r4, r1
	b _08012284
	.align 2, 0
.pool
_0801227C:
	mvn r0, r7
	ldrh r3, [r3, #0x16]
	add r0, r0, r3
	ldrh r1, [r2, #4]
_08012284:
	mul r0, r1, r0
	lsr r3, r0, #8
	b _080122F2
_0801228A:
	ldrh r1, [r2, #2]
	mov r3, #0x80
	lsl r3, r3, #8
	add r0, r3, #0
	and r0, r1
	cmp r0, #0
	beq _080122A8
	ldrh r0, [r2, #2]
	sub r0, r3, r0
	ldr r1, =0x03002080
	ldr r1, [r1]
	b _080122AE
	.align 2, 0
.pool
_080122A8:
	ldrh r1, [r2, #2]
	ldr r0, =0x03002080
	ldr r0, [r0]
_080122AE:
	mul r0, r1, r0
	lsr r0, r0, #8
	ldrh r1, [r2, #0xa]
	bl Bios_modulo
	add r5, r0, #0
	ldr r0, =0x030046F0
	ldr r2, [r0, #0x24]
	ldrh r1, [r2, #4]
	mov r3, #0x80
	lsl r3, r3, #8
	add r0, r3, #0
	and r0, r1
	cmp r0, #0
	beq _080122E0
	ldrh r0, [r2, #4]
	sub r0, r3, r0
	ldr r1, =0x03002080
	ldr r1, [r1]
	b _080122E6
	.align 2, 0
.pool
_080122E0:
	ldrh r1, [r2, #4]
	ldr r0, =0x03002080
	ldr r0, [r0]
_080122E6:
	mul r0, r1, r0
	lsr r0, r0, #8
	ldrh r1, [r2, #0xc]
	bl Bios_modulo
	add r3, r0, #0
_080122F2:
	ldr r1, =0x03002308
	ldr r2, =0x030046F0
	ldrb r0, [r2, #1]
	add r0, #1
	lsl r0, r0, #1
	add r0, r0, r1
	neg r1, r5
	strh r1, [r0]
	ldr r1, =0x030022F8
	ldrb r0, [r2, #1]
	add r0, #1
	lsl r0, r0, #1
	add r0, r0, r1
	neg r1, r3
	strh r1, [r0]
_08012310:
	ldr r0, =(cb_handleLvlBackgrounds+1)
	mov r1, #0
	bl addCallback
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global cb_handleLvlBackgrounds
cb_handleLvlBackgrounds: @ 0x08012334
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	ldr r0, =0x030046F0
	ldrb r1, [r0]
	add r6, r0, #0
	cmp r1, #0
	bne _08012358
	ldr r0, =(cb_handleLvlBackgrounds+1)
	bl deleteCallback
	b _080124B2
	.align 2, 0
.pool
_08012358:
	ldr r3, [r6, #0xc]
	ldrb r0, [r6, #6]
	lsl r0, r0, #0xb
	mov r1, #0xc0
	lsl r1, r1, #0x13
	add r1, r0, r1
	mov r5, #0
	ldr r0, =0x03002308
	mov ip, r0
	ldr r0, =0x030022F8
	mov r8, r0
	ldr r0, =0x03003D38
	mov sb, r0
	ldr r0, =0x03003D68
	mov sl, r0
	ldr r2, =0x040000D4
	ldr r7, =0x80000020
	ldrh r0, [r6, #0x14]
	lsr r4, r0, #2
_0801237E:
	str r3, [r2]
	str r1, [r2, #4]
	str r7, [r2, #8]
	ldr r0, [r2, #8]
	add r3, r3, r4
	add r1, #0x40
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, #0x15
	bls _0801237E
	ldrb r1, [r6, #1]
	lsl r1, r1, #1
	add r1, ip
	ldrb r0, [r6, #0x10]
	neg r0, r0
	strh r0, [r1]
	ldrb r1, [r6, #1]
	lsl r1, r1, #1
	add r1, r8
	ldrb r0, [r6, #0x11]
	neg r0, r0
	strh r0, [r1]
	ldrb r0, [r6, #1]
	lsl r0, r0, #1
	add r0, sb
	ldrb r1, [r6, #0x10]
	strh r1, [r0]
	ldrb r0, [r6, #1]
	lsl r0, r0, #1
	add r0, sl
	ldrb r1, [r6, #0x11]
	strh r1, [r0]
	ldr r0, [r6, #8]
	ldr r0, [r0, #0x74]
	cmp r0, #0
	beq _08012432
	ldrh r2, [r6, #0x14]
	ldrh r0, [r6, #0x16]
	mul r0, r2, r0
	asr r0, r0, #5
	ldr r1, [r6, #0xc]
	add r3, r1, r0
	ldrb r0, [r6, #6]
	add r0, #1
	lsl r0, r0, #0xb
	mov r1, #0xc0
	lsl r1, r1, #0x13
	add r1, r0, r1
	mov r5, #0
	ldr r4, =0x040000D4
	ldr r7, =0x80000020
	lsr r2, r2, #2
_080123E8:
	str r3, [r4]
	str r1, [r4, #4]
	str r7, [r4, #8]
	ldr r0, [r4, #8]
	add r3, r3, r2
	add r1, #0x40
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, #0x15
	bls _080123E8
	ldrb r0, [r6, #1]
	sub r0, #1
	lsl r0, r0, #1
	add r0, ip
	ldrb r1, [r6, #0x10]
	neg r1, r1
	strh r1, [r0]
	ldrb r0, [r6, #1]
	sub r0, #1
	lsl r0, r0, #1
	add r0, r8
	ldrb r1, [r6, #0x11]
	neg r1, r1
	strh r1, [r0]
	ldrb r0, [r6, #1]
	sub r0, #1
	lsl r0, r0, #1
	add r0, sb
	ldrb r1, [r6, #0x10]
	strh r1, [r0]
	ldrb r0, [r6, #1]
	sub r0, #1
	lsl r0, r0, #1
	add r0, sl
	ldrb r1, [r6, #0x11]
	strh r1, [r0]
_08012432:
	ldr r0, [r6, #8]
	mov r1, #0xa4
	lsl r1, r1, #1
	add r0, r0, r1
	ldr r0, [r0]
	cmp r0, #0
	beq _080124AC
	ldrh r2, [r6, #0x14]
	ldrh r0, [r6, #0x16]
	mul r0, r2, r0
	asr r0, r0, #5
	lsl r0, r0, #1
	ldr r1, [r6, #0xc]
	add r3, r1, r0
	ldrb r0, [r6, #6]
	add r0, #2
	lsl r0, r0, #0xb
	mov r1, #0xc0
	lsl r1, r1, #0x13
	add r1, r0, r1
	mov r5, #0
	ldr r4, =0x040000D4
	ldr r7, =0x80000020
	lsr r2, r2, #2
_08012462:
	str r3, [r4]
	str r1, [r4, #4]
	str r7, [r4, #8]
	ldr r0, [r4, #8]
	add r3, r3, r2
	add r1, #0x40
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, #0x15
	bls _08012462
	ldrb r0, [r6, #1]
	sub r0, #2
	lsl r0, r0, #1
	add r0, ip
	ldrb r1, [r6, #0x10]
	neg r1, r1
	strh r1, [r0]
	ldrb r0, [r6, #1]
	sub r0, #2
	lsl r0, r0, #1
	add r0, r8
	ldrb r1, [r6, #0x11]
	neg r1, r1
	strh r1, [r0]
	ldrb r0, [r6, #1]
	sub r0, #2
	lsl r0, r0, #1
	add r0, sb
	ldrb r1, [r6, #0x10]
	strh r1, [r0]
	ldrb r0, [r6, #1]
	sub r0, #2
	lsl r0, r0, #1
	add r0, sl
	ldrb r1, [r6, #0x11]
	strh r1, [r0]
_080124AC:
	ldr r0, =(cb_handleLvlBackgrounds+1)
	bl deleteCallback
_080124B2:
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_080124DC
sub_080124DC: @ 0x080124DC
	push {r4, r5, r6, lr}
	ldr r3, =0x030046F0
	mov r0, #1
	strb r0, [r3, #0x12]
	ldr r4, =0x03003CD4
	mov r6, #0x80
	lsl r6, r6, #1
	add r2, r6, #0
	ldrb r0, [r3, #1]
	lsl r2, r0
	ldrh r0, [r4]
	orr r2, r0
	strh r2, [r4]
	ldr r5, [r3, #8]
	ldr r0, [r5, #0x74]
	cmp r0, #0
	beq _0801250A
	ldrb r1, [r3, #1]
	sub r1, #1
	add r0, r6, #0
	lsl r0, r1
	orr r2, r0
	strh r2, [r4]
_0801250A:
	mov r1, #0xa4
	lsl r1, r1, #1
	add r0, r5, r1
	ldr r0, [r0]
	cmp r0, #0
	beq _08012524
	ldrb r0, [r3, #1]
	sub r0, #2
	add r1, r6, #0
	lsl r1, r0
	ldrh r0, [r4]
	orr r1, r0
	strh r1, [r4]
_08012524:
	ldr r0, [r3, #0x24]
	cmp r0, #0
	beq _08012538
	ldrb r0, [r3, #1]
	add r0, #1
	add r1, r6, #0
	lsl r1, r0
	ldrh r0, [r4]
	orr r1, r0
	strh r1, [r4]
_08012538:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global sub_8012548
sub_8012548: @ 0x08012548
	push {r4, r5, r6, lr}
	ldr r3, =0x030046F0
	mov r0, #0
	strb r0, [r3, #0x12]
	ldr r4, =0x03003CD4
	mov r6, #0x80
	lsl r6, r6, #1
	add r0, r6, #0
	ldrb r1, [r3, #1]
	lsl r0, r1
	ldrh r1, [r4]
	add r2, r1, #0
	bic r2, r0
	strh r2, [r4]
	ldr r5, [r3, #8]
	ldr r0, [r5, #0x74]
	cmp r0, #0
	beq _08012578
	ldrb r1, [r3, #1]
	sub r1, #1
	add r0, r6, #0
	lsl r0, r1
	bic r2, r0
	strh r2, [r4]
_08012578:
	mov r1, #0xa4
	lsl r1, r1, #1
	add r0, r5, r1
	ldr r0, [r0]
	cmp r0, #0
	beq _08012592
	ldrb r0, [r3, #1]
	sub r0, #2
	add r1, r6, #0
	lsl r1, r0
	ldrh r0, [r4]
	bic r0, r1
	strh r0, [r4]
_08012592:
	ldr r0, [r3, #0x24]
	cmp r0, #0
	beq _080125AC
	ldrb r1, [r3, #1]
	add r2, r6, #0
	lsl r2, r1
	add r1, #1
	add r0, r6, #0
	lsl r0, r1
	orr r2, r0
	ldrh r0, [r4]
	bic r0, r2
	strh r0, [r4]
_080125AC:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
	
	thumb_func_global sub_80125BC
sub_80125BC: @ 0x080125BC
	push {r4, r5, r6, lr}
	ldr r3, =0x030046F0
	mov r0, #0
	strb r0, [r3]
	ldr r4, =0x03003CD4
	mov r6, #0x80
	lsl r6, r6, #1
	add r0, r6, #0
	ldrb r1, [r3, #1]
	lsl r0, r1
	ldrh r1, [r4]
	add r2, r1, #0
	bic r2, r0
	strh r2, [r4]
	ldr r5, [r3, #8]
	ldr r0, [r5, #0x74]
	cmp r0, #0
	beq _080125EC
	ldrb r1, [r3, #1]
	sub r1, #1
	add r0, r6, #0
	lsl r0, r1
	bic r2, r0
	strh r2, [r4]
_080125EC:
	mov r1, #0xa4
	lsl r1, r1, #1
	add r0, r5, r1
	ldr r0, [r0]
	cmp r0, #0
	beq _08012606
	ldrb r0, [r3, #1]
	sub r0, #2
	add r1, r6, #0
	lsl r1, r0
	ldrh r0, [r4]
	bic r0, r1
	strh r0, [r4]
_08012606:
	ldr r0, [r3, #0x24]
	cmp r0, #0
	beq _08012620
	ldrb r1, [r3, #1]
	add r2, r6, #0
	lsl r2, r1
	add r1, #1
	add r0, r6, #0
	lsl r0, r1
	orr r2, r0
	ldrh r0, [r4]
	bic r0, r2
	strh r0, [r4]
_08012620:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global getCollision
getCollision: @ 0x08012630
	push {r4, lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	add r2, r0, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r3, =0x030046F0
	ldrh r4, [r3, #0x14]
	sub r0, r4, #1
	cmp r2, r0
	bgt _08012670
	ldrh r3, [r3, #0x16]
	sub r0, r3, #1
	cmp r1, r0
	bgt _08012670
	add r0, r1, #1
	sub r0, r3, r0
	lsl r0, r0, #0x10
	lsr r2, r2, #3
	lsr r0, r0, #0x13
	ldr r1, =0x0200A630
	mul r0, r4, r0
	asr r0, r0, #3
	add r0, r0, r2
	add r0, r0, r1
	ldrb r0, [r0]
	b _08012672
	.align 2, 0
.pool
_08012670:
	ldr r0, =0x0000FFFF
_08012672:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool
	
	
	
	
	thumb_func_global sub_801267C
sub_801267C: @ 0x0801267C
	push {r4, lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	add r3, r0, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r2, =0x030046F0
	ldrh r4, [r2, #0x14]
	sub r0, r4, #1
	cmp r3, r0
	bgt _080126B8
	ldrh r2, [r2, #0x16]
	sub r0, r2, #1
	cmp r1, r0
	bgt _080126B8
	add r0, r1, #1
	sub r0, r2, r0
	lsl r0, r0, #0x10
	lsr r1, r3, #3
	lsr r0, r0, #0x13
	mul r0, r4, r0
	asr r0, r0, #3
	add r0, r0, r1
	ldr r1, =0x0200A630
	add r0, r0, r1
	b _080126BA
	.align 2, 0
.pool
_080126B8:
	mov r0, #0
_080126BA:
	pop {r4}
	pop {r1}
	bx r1
	
	

thumb_func_global isCollisionSolid
isCollisionSolid: @ 0x080126C0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x14
	cmp r0, #1
	beq _080126CC
	mov r0, #0
	b _080126CE
_080126CC:
	mov r0, #1
_080126CE:
	bx lr

thumb_func_global isCollisionDoor
isCollisionDoor: @ 0x080126D0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x14
	cmp r0, #4
	beq _080126DC
	mov r0, #0
	b _080126DE
_080126DC:
	mov r0, #1
_080126DE:
	bx lr

thumb_func_global isCollisionLockedDoor
isCollisionLockedDoor: @ 0x080126E0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x14
	cmp r0, #6
	beq _080126EC
	mov r0, #0
	b _080126EE
_080126EC:
	mov r0, #1
_080126EE:
	bx lr
	
	
thumb_func_global isCollisionHookSpot
isCollisionHookSpot: @ 0x080126F0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x14
	cmp r0, #2
	beq _080126FC
	mov r0, #0
	b locret_80126FE
_080126FC:
	mov r0, #1
locret_80126FE:
	bx lr
	
	
thumb_func_global isCollisionSpike
isCollisionSpike: @ 0x08012700
	lsl r0, r0, #0x10
	lsr r0, r0, #0x14
	cmp r0, #0xc
	beq _0801270C
	mov r0, #0
	b locret_801270E
_0801270C:
	mov r0, #1
locret_801270E:
	bx lr
	

thumb_func_global isCollisionConveyer
isCollisionConveyer: @ 0x08012710
	lsl r0, r0, #0x10
	lsr r0, r0, #0x14
	cmp r0, #7
	beq _0801271C
	mov r0, #0
	b _0801271E
_0801271C:
	mov r0, #1
_0801271E:
	bx lr
	
	
	
	
	
	thumb_func_global isCollisionSolid2
isCollisionSolid2: @ 0x08012720
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x10
	and r0, r1
	cmp r0, #0
	bne _08012730
	mov r0, #0
	b locret_8012732
_08012730:
	mov r0, #1
locret_8012732:
	bx lr
	
	
	
	thumb_func_global collisionUnk
collisionUnk: @ 0x08012734
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0
	beq _08012740
	cmp r0, #0x3f
	bls _08012744
_08012740:
	mov r0, #0
	b locret_8012746
_08012744:
	mov r0, #1
locret_8012746:
	bx lr
	
	
	
	thumb_func_global sub_8012748
sub_8012748: @ 0x08012748
	push {lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl getCollision
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	lsr r0, r0, #0x14
	cmp r0, #1
	beq _08012766
	mov r0, #1
	neg r0, r0
	b _0801276A
_08012766:
	lsl r0, r1, #0x10
	asr r0, r0, #0x10
_0801276A:
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	thumb_func_global sub_8012770
sub_8012770: @ 0x08012770
	push {lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl getCollision
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	lsr r0, r0, #0x14
	cmp r0, #4
	beq _0801278E
	mov r0, #1
	neg r0, r0
	b _08012792
_0801278E:
	lsl r0, r1, #0x10
	asr r0, r0, #0x10
_08012792:
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	thumb_func_global sub_8012798
sub_8012798: @ 0x08012798
	push {lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl getCollision
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	lsr r0, r0, #0x14
	cmp r0, #6
	beq _080127B6
	mov r0, #1
	neg r0, r0
	b _080127BA
_080127B6:
	lsl r0, r1, #0x10
	asr r0, r0, #0x10
_080127BA:
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	thumb_func_global sub_80127C0
sub_80127C0: @ 0x080127C0
	push {lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl getCollision
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	lsr r0, r0, #0x14
	cmp r0, #2
	beq _080127DE
	mov r0, #1
	neg r0, r0
	b _080127E2
_080127DE:
	lsl r0, r1, #0x10
	asr r0, r0, #0x10
_080127E2:
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	thumb_func_global sub_80127E8
sub_80127E8: @ 0x080127E8
	push {lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl getCollision
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	lsr r0, r0, #0x14
	cmp r0, #0xc
	beq _08012806
	mov r0, #1
	neg r0, r0
	b _0801280A
_08012806:
	lsl r0, r1, #0x10
	asr r0, r0, #0x10
_0801280A:
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	thumb_func_global sub_8012810
sub_8012810: @ 0x08012810
	push {lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl getCollision
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	lsr r0, r0, #0x14
	cmp r0, #7
	beq _0801282E
	mov r0, #1
	neg r0, r0
	b _08012832
_0801282E:
	lsl r0, r1, #0x10
	asr r0, r0, #0x10
_08012832:
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	thumb_func_global sub_8012838
sub_8012838: @ 0x08012838
	push {lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl getCollision
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	bne _08012858
	mov r0, #1
	neg r0, r0
	b _0801285C
_08012858:
	lsl r0, r1, #0x10
	asr r0, r0, #0x10
_0801285C:
	pop {r1}
	bx r1
	
	
	
	thumb_func_global sub_8012860
sub_8012860: @ 0x08012860
	push {lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl getCollision
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	add r1, r0, #0
	cmp r0, #0
	beq _0801287C
	cmp r0, #0xaf
	bls _08012882
_0801287C:
	mov r0, #1
	neg r0, r0
	b _08012886
_08012882:
	lsl r0, r1, #0x10
	asr r0, r0, #0x10
_08012886:
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	
	
	

thumb_func_global getDoorWarpDest
getDoorWarpDest: @ 0x0801288C
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	ldr r2, =doorWarpDestinations
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r2, [r0]
	ldrb r0, [r1, #2]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r2, [r0]
	ldrb r0, [r1, #3]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r2, [r0]
	b _080128CE
	.align 2, 0
.pool
_080128B8:
	ldrb r0, [r2]
	cmp r0, r3
	bne _080128CC
	ldrb r1, [r2, #2]
	lsl r1, r1, #8
	ldrb r0, [r2, #1]
	orr r0, r1
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	b _080128D8
_080128CC:
	add r2, #4
_080128CE:
	ldrb r0, [r2]
	cmp r0, #0xff
	bne _080128B8
	mov r0, #1
	neg r0, r0
_080128D8:
	bx lr
	.align 2, 0

thumb_func_global loadMap
loadMap: @ 0x080128DC
	push {lr}
	lsl r0, r0, #0x10
	lsl r1, r1, #0x10
	lsl r2, r2, #0x10
	ldr r3, =maps_DD828
	lsr r0, r0, #0xe
	add r0, r0, r3
	ldr r0, [r0]
	lsr r1, r1, #0xe
	add r1, r1, r0
	ldr r0, [r1]
	lsr r2, r2, #0xe
	add r2, r2, r0
	ldr r0, [r2]
	bl maybe_map_load
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global loadCameraBorder
loadCameraBorder: @ 0x08012908
	push {lr}
	ldr r0, =cameraBorderThingy
	ldr r2, =0x030046F0
	ldrb r1, [r2, #5]
	lsl r1, r1, #0xe
	ldrh r2, [r2, #2]
	mov r3, #0xbe
	lsl r3, r3, #2
	add r2, r2, r3
	lsl r2, r2, #5
	mov r3, #0xc0
	lsl r3, r3, #0x13
	add r2, r2, r3
	add r1, r1, r2
	mov r2, #0x80
	bl Bios_memcpy
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08012938
sub_08012938: @ 0x08012938
	push {lr}
	sub sp, #4
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r0, =0x030046F0
	ldrb r1, [r0, #5]
	lsl r1, r1, #0xe
	ldrh r0, [r0, #2]
	mov r2, #0xbe
	lsl r2, r2, #2
	add r0, r0, r2
	lsl r0, r0, #5
	mov r2, #0xc0
	lsl r2, r2, #0x13
	add r0, r0, r2
	add r1, r1, r0
	ldr r2, =0x01000080
	mov r0, sp
	bl Bios_memcpy
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08012970
sub_08012970: @ 0x08012970
	push {r4, lr}
	lsl r4, r0, #0x10
	lsr r0, r4, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r3, =0x030046F0
	ldrh r2, [r3, #0x1c]
	cmp r0, r2
	bhi _080129B0
	ldrh r0, [r3, #0x1e]
	cmp r1, r0
	bhi _080129B0
	ldr r2, =0x0200A630
	ldrh r0, [r3, #0x1e]
	sub r0, r0, r1
	asr r0, r0, #3
	ldrh r1, [r3, #0x20]
	mul r0, r1, r0
	lsr r1, r4, #0x13
	add r0, r0, r1
	add r0, r0, r2
	ldrb r1, [r0]
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	bne _080129B0
	mov r0, #0
	b _080129B2
	.align 2, 0
.pool
_080129B0:
	mov r0, #1
_080129B2:
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global sub_080129B8
sub_080129B8: @ 0x080129B8
	push {r4, lr}
	lsl r4, r0, #0x10
	lsr r0, r4, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r3, =0x030046F0
	ldrh r2, [r3, #0x1c]
	cmp r0, r2
	bhi _080129F2
	ldrh r0, [r3, #0x1e]
	cmp r1, r0
	bhi _080129F2
	ldr r2, =0x0200A630
	ldrh r0, [r3, #0x1e]
	sub r0, r0, r1
	asr r0, r0, #3
	ldrh r1, [r3, #0x20]
	mul r0, r1, r0
	lsr r1, r4, #0x13
	add r0, r0, r1
	add r0, r0, r2
	ldrb r0, [r0]
	mov r1, #0xf
	and r0, r1
	sub r0, #1
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #1
	bls _08012A00
_080129F2:
	mov r0, #0
	b _08012A02
	.align 2, 0
.pool
_08012A00:
	mov r0, #1
_08012A02:
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global sub_08012A08
sub_08012A08: @ 0x08012A08
	push {r4, r5, r6, r7, lr}
	lsl r6, r0, #0x10
	lsr r0, r6, #0x10
	add r3, r0, #0
	lsl r1, r1, #0x10
	lsr r4, r1, #0x10
	mov ip, r4
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	add r5, r2, #0
	ldr r1, =0x030046F0
	ldrh r7, [r1, #0x1c]
	cmp r0, r7
	bhi _08012A32
	add r0, r0, r2
	add r2, r7, #0
	cmp r0, r2
	bgt _08012A32
	ldrh r7, [r1, #0x1e]
	cmp r4, r7
	bls _08012A3C
_08012A32:
	mov r0, #1
	b _08012A78
	.align 2, 0
.pool
_08012A3C:
	mov r0, #7
	and r3, r0
	add r0, r3, r5
	add r0, #8
	asr r4, r0, #3
	lsr r2, r6, #0x13
	ldrh r0, [r1, #0x1e]
	mov r3, ip
	sub r0, r0, r3
	lsl r0, r0, #0xd
	lsr r0, r0, #0x10
	ldrh r1, [r1, #0x20]
	mul r0, r1, r0
	add r0, r0, r2
	ldr r1, =0x0200A630
	add r2, r0, r1
	mov r3, #0
	cmp r3, r4
	bge _08012A76
	mov r5, #0x10
_08012A64:
	ldrb r1, [r2]
	add r0, r5, #0
	and r0, r1
	cmp r0, #0
	bne _08012A32
	add r2, #1
	add r3, #1
	cmp r3, r4
	blt _08012A64
_08012A76:
	mov r0, #0
_08012A78:
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_08012A84
sub_08012A84: @ 0x08012A84
	push {r4, r5, r6, r7, lr}
	lsl r7, r0, #0x10
	lsr r0, r7, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	add r6, r1, #0
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	add r4, r2, #0
	ldr r5, =0x030046F0
	ldrh r3, [r5, #0x1c]
	cmp r0, r3
	bhi _08012AAA
	sub r0, r1, r2
	ldrh r3, [r5, #0x1e]
	cmp r0, r3
	bgt _08012AAA
	cmp r1, r3
	bls _08012AB4
_08012AAA:
	mov r0, #1
	b _08012AF0
	.align 2, 0
.pool
_08012AB4:
	add r1, r4, #0
	add r1, #0xf
	mov r0, #7
	and r0, r6
	sub r1, r1, r0
	asr r4, r1, #3
	lsr r2, r7, #0x13
	sub r0, r3, r6
	lsl r0, r0, #0xd
	lsr r0, r0, #0x10
	ldrh r1, [r5, #0x20]
	mul r0, r1, r0
	add r0, r0, r2
	ldr r1, =0x0200A630
	add r2, r0, r1
	mov r3, #0
	cmp r3, r4
	bge _08012AEE
	mov r6, #0x10
_08012ADA:
	ldrb r1, [r2]
	add r0, r6, #0
	and r0, r1
	cmp r0, #0
	bne _08012AAA
	ldrh r0, [r5, #0x20]
	add r2, r2, r0
	add r3, #1
	cmp r3, r4
	blt _08012ADA
_08012AEE:
	mov r0, #0
_08012AF0:
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool



thumb_func_global sub_8012AFC
sub_8012AFC: @ 0x08012AFC
	push {r4, r5, r6, lr}
	lsl r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	mov r3, #0xe0
	lsl r3, r3, #0xb
	and r3, r0
	lsr r3, r3, #0x10
	add r3, r3, r2
	add r3, #8
	lsr r5, r3, #3
	lsr r0, r0, #0x13
	ldr r3, =0x030046F0
	ldrh r2, [r3, #0x1e]
	sub r2, r2, r1
	lsl r2, r2, #0xd
	lsr r2, r2, #0x10
	ldrh r1, [r3, #0x20]
	mul r1, r2, r1
	add r1, r1, r0
	ldr r0, =0x0200A630
	add r2, r1, r0
	mov r4, #0
	mov r3, #0
	cmp r4, r5
	bhs _08012B5C
	ldrb r1, [r2]
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	bne _08012B5C
	mov r6, #0x10
_08012B40:
	add r2, #1
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, r5
	bhs _08012B5C
	ldrb r1, [r2]
	add r0, r6, #0
	and r0, r1
	cmp r0, #0
	beq _08012B40
_08012B5C:
	add r0, r4, #0
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool



	thumb_func_global sub_8012B6C
sub_8012B6C: @ 0x08012B6C
	push {r4, r5, r6, lr}
	lsl r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	add r2, #0xf
	mov r3, #0xe0
	lsl r3, r3, #0xb
	and r3, r0
	lsr r3, r3, #0x10
	sub r2, r2, r3
	lsl r2, r2, #0xd
	lsr r5, r2, #0x10
	lsr r0, r0, #0x13
	ldr r3, =0x030046F0
	ldrh r2, [r3, #0x1e]
	sub r2, r2, r1
	lsl r2, r2, #0xd
	lsr r2, r2, #0x10
	ldrh r1, [r3, #0x20]
	mul r1, r2, r1
	add r1, r1, r0
	ldr r0, =0x0200A630
	add r2, r1, r0
	mov r4, #0
	mov r3, #0
	cmp r4, r5
	bhs _08012BCE
	ldrb r1, [r2]
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	bne _08012BCE
	mov r6, #0x10
_08012BB2:
	sub r2, #1
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, r5
	bhs _08012BCE
	ldrb r1, [r2]
	add r0, r6, #0
	and r0, r1
	cmp r0, #0
	beq _08012BB2
_08012BCE:
	add r0, r4, #0
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool



	thumb_func_global sub_8012BE0
sub_8012BE0: @ 0x08012BE0
	push {r4, r5, r6, r7, lr}
	lsl r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	mov r3, #7
	and r3, r1
	add r3, r3, r2
	add r3, #8
	lsr r5, r3, #3
	lsr r0, r0, #0x13
	ldr r3, =0x030046F0
	ldrh r2, [r3, #0x1e]
	sub r2, r2, r1
	lsl r2, r2, #0xd
	lsr r2, r2, #0x10
	ldrh r6, [r3, #0x20]
	add r1, r2, #0
	mul r1, r6, r1
	add r1, r1, r0
	ldr r0, =0x0200A630
	add r2, r1, r0
	mov r4, #0
	mov r3, #0
	cmp r4, r5
	bhs _08012C3E
	ldrb r1, [r2]
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	bne _08012C3E
	mov r7, #0x10
_08012C22:
	sub r2, r2, r6
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, r5
	bhs _08012C3E
	ldrb r1, [r2]
	add r0, r7, #0
	and r0, r1
	cmp r0, #0
	beq _08012C22
_08012C3E:
	add r0, r4, #0
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool



	thumb_func_global sub_8012C50
sub_8012C50: @ 0x08012C50
	push {r4, r5, r6, r7, lr}
	lsl r0, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	mov r3, #7
	and r3, r1
	add r3, r3, r2
	add r3, #8
	lsr r5, r3, #3
	lsr r0, r0, #0x13
	ldr r3, =0x030046F0
	ldrh r2, [r3, #0x1e]
	sub r2, r2, r1
	lsl r2, r2, #0xd
	lsr r2, r2, #0x10
	ldrh r6, [r3, #0x20]
	add r1, r2, #0
	mul r1, r6, r1
	add r1, r1, r0
	ldr r0, =0x0200A630
	add r2, r1, r0
	mov r4, #0
	mov r3, #0
	cmp r4, r5
	bhs _08012CAE
	ldrb r1, [r2]
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	bne _08012CAE
	mov r7, #0x10
_08012C92:
	add r2, r2, r6
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, r5
	bhs _08012CAE
	ldrb r1, [r2]
	add r0, r7, #0
	and r0, r1
	cmp r0, #0
	beq _08012C92
_08012CAE:
	add r0, r4, #0
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool







thumb_func_global sub_08012CC0
sub_08012CC0: @ 0x08012CC0
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	mov ip, r3
	lsl r3, r0, #0x10
	lsr r0, r3, #0x10
	add r4, r0, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r8, r1
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	add r6, r2, #0
	ldr r5, =0x030046F0
	ldrh r7, [r5, #0x1c]
	cmp r0, r7
	bhi _08012CF0
	add r0, r0, r2
	add r2, r7, #0
	cmp r0, r2
	bgt _08012CF0
	ldrh r7, [r5, #0x1e]
	cmp r1, r7
	bls _08012CF8
_08012CF0:
	mov r0, #1
	b _08012D7A
	.align 2, 0
.pool
_08012CF8:
	mov r0, #7
	and r4, r0
	add r0, r4, r6
	add r0, #8
	asr r4, r0, #3
	lsr r2, r3, #0x13
	ldrh r0, [r5, #0x1e]
	mov r1, r8
	sub r0, r0, r1
	lsl r0, r0, #0xd
	lsr r0, r0, #0x10
	ldrh r1, [r5, #0x20]
	mul r0, r1, r0
	add r0, r0, r2
	ldr r1, =0x0200A630
	add r3, r0, r1
	mov r2, #0
	cmp r2, r4
	bge _08012D78
	mov r5, #0xf
	ldr r7, =dword_803A624
	ldr r6, =0x03003D40
_08012D24:
	ldrb r1, [r3]
	lsr r0, r1, #4
	cmp r0, #0xc
	bne _08012D70
	add r2, r5, #0
	and r2, r1
	mov r0, ip
	strb r2, [r0]
	ldrb r0, [r6, #0x11]
	lsl r4, r0, #2
	ldrb r0, [r3]
	add r1, r5, #0
	and r1, r0
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #2
	add r0, r4, r0
	add r0, r0, r7
	ldr r1, [r0]
	ldrb r0, [r6, #1]
	cmp r0, #2
	bls _08012D5A
	cmp r2, #2
	bhi _08012D5A
	ldr r0, =dword_803A66C
	add r0, r4, r0
	ldr r1, [r0]
_08012D5A:
	add r0, r1, #0
	b _08012D7A
	.align 2, 0
.pool
_08012D70:
	add r3, #1
	add r2, #1
	cmp r2, r4
	blt _08012D24
_08012D78:
	mov r0, #0
_08012D7A:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1

thumb_func_global sub_08012D84
sub_08012D84: @ 0x08012D84
	push {r4, r5, r6, r7, lr}
	mov ip, r3
	lsl r7, r0, #0x10
	lsr r0, r7, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	add r6, r1, #0
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	add r3, r2, #0
	ldr r5, =0x030046F0
	ldrh r4, [r5, #0x1c]
	cmp r0, r4
	bhi _08012DAC
	sub r0, r1, r2
	ldrh r4, [r5, #0x1e]
	cmp r0, r4
	bgt _08012DAC
	cmp r1, r4
	bls _08012DE0
_08012DAC:
	mov r0, #1
	b _08012E18
	.align 2, 0
.pool
_08012DB4:
	mov r1, #0xf
	add r0, r1, #0
	and r0, r2
	mov r2, ip
	strb r0, [r2]
	ldr r3, =dword_803A624
	ldr r2, =0x03003D40
	ldrb r0, [r4]
	and r1, r0
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r2, [r2, #0x11]
	add r0, r0, r2
	lsl r0, r0, #2
	add r0, r0, r3
	ldr r0, [r0]
	b _08012E18
	.align 2, 0
.pool
_08012DE0:
	add r1, r3, #0
	add r1, #0xf
	mov r0, #7
	and r0, r6
	sub r1, r1, r0
	asr r3, r1, #3
	lsr r2, r7, #0x13
	sub r0, r4, r6
	lsl r0, r0, #0xd
	lsr r0, r0, #0x10
	ldrh r1, [r5, #0x20]
	mul r0, r1, r0
	add r0, r0, r2
	ldr r1, =0x0200A630
	add r4, r0, r1
	mov r1, #0
	cmp r1, r3
	bge _08012E16
_08012E04:
	ldrb r2, [r4]
	lsr r0, r2, #4
	cmp r0, #0xc
	beq _08012DB4
	ldrh r0, [r5, #0x20]
	add r4, r4, r0
	add r1, #1
	cmp r1, r3
	blt _08012E04
_08012E16:
	mov r0, #0
_08012E18:
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool


