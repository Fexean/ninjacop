.include "asm/macros.inc"



thumb_func_global sub_08016AC0
sub_08016AC0: @ 0x08016AC0
	push {lr}
	sub sp, #4
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r1, =0x03000DB0
	ldr r2, =0x010007E0
	mov r0, sp
	bl Bios_memcpy
	ldr r0, =(task_unknown+1)
	mov r1, #0xfe
	bl createTask
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08016AF0
sub_08016AF0: @ 0x08016AF0
	ldr r2, [r0]
	asr r2, r2, #8
	strh r2, [r1]
	ldr r0, [r0, #4]
	asr r0, r0, #8
	strh r0, [r1, #2]
	bx lr
	.align 2, 0

thumb_func_global sub_08016B00
sub_08016B00: @ 0x08016B00
	mov r3, #0
	ldrsh r2, [r0, r3]
	lsl r2, r2, #8
	str r2, [r1]
	mov r2, #2
	ldrsh r0, [r0, r2]
	lsl r0, r0, #8
	str r0, [r1, #4]
	bx lr
	.align 2, 0

thumb_func_global copyModifiedByCamera
copyModifiedByCamera: @ 0x08016B14
	push {r4, lr}
	ldr r4, =0x03004720
	ldrh r3, [r4]
	lsl r3, r3, #8
	ldr r2, [r0]
	sub r2, r2, r3
	asr r2, r2, #8
	strh r2, [r1]
	ldrh r2, [r4, #2]
	lsl r2, r2, #8
	ldr r0, [r0, #4]
	sub r2, r2, r0
	asr r2, r2, #8
	strh r2, [r1, #2]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global unused_sub_8016B3C
unused_sub_8016B3C: @ 0x08016B3C
	push {r4, lr}
	mov r3, #0
	ldrsh r2, [r0, r3]
	ldr r3, =0x03004720
	lsl r2, r2, #8
	ldrh r4, [r3]
	add r2, r2, r4
	str r2, [r1]
	ldrh r2, [r3, #2]
	mov r3, #2
	ldrsh r0, [r0, r3]
	lsl r0, r0, #8
	sub r2, r2, r0
	str r2, [r1, #4]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global unused_sub_8016B64
unused_sub_8016B64: @ 0x08016B64
	push {r4, lr}
	sub sp, #8
	add r4, r1, #0
	mov r1, sp
	bl sub_08016B00
	mov r0, sp
	add r1, r4, #0
	bl copyModifiedByCamera
	add sp, #8
	pop {r4}
	pop {r0}
	bx r0

thumb_func_global unused_sub_8016B80
unused_sub_8016B80: @ 0x08016B80
	push {r4, lr}
	sub sp, #8
	add r4, r1, #0
	mov r1, sp
	bl unused_sub_8016B3C
	mov r0, sp
	add r1, r4, #0
	bl sub_08016AF0
	add sp, #8
	pop {r4}
	pop {r0}
	bx r0
	
	
	
thumb_func_global sub_8016B9C
sub_8016B9C:
	push {lr}
	sub sp, #4
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r1, =0x03000DB0
	ldr r2, =0x010007E0
	mov r0, sp
	bl Bios_memcpy
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global allocObj
allocObj: @ 0x08016BC0
	push {r4, r5, lr}
	sub sp, #4
	ldr r0, =0x03000DB0
	ldr r2, =0x040000D4
	mov r4, sp
	add r3, r0, #0
	add r3, #0x50
	add r1, r0, #0
	ldr r0, =0x00000F6C
	add r5, r1, r0
_08016BD4:
	ldrb r0, [r3]
	cmp r0, #0
	bne _08016C00
	strh r0, [r4]
	mov r0, sp
	str r0, [r2]
	str r1, [r2, #4]
	ldr r0, =0x8100002A
	str r0, [r2, #8]
	ldr r0, [r2, #8]
	mov r0, #1
	strb r0, [r3]
	add r0, r1, #0
	b _08016C0A
	.align 2, 0
.pool
_08016C00:
	add r3, #0x54
	add r1, #0x54
	cmp r1, r5
	ble _08016BD4
	mov r0, #0
_08016C0A:
	add sp, #4
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global clearObjField50
clearObjField50: @ 0x08016C14
	add r0, #0x50
	mov r1, #0
	strb r1, [r0]
	bx lr

thumb_func_global initPlayerInit
initPlayerInit: @ 0x08016C1C
	push {r4, r5, lr}
	sub sp, #4
	add r5, r0, #0
	bl allocObj
	add r4, r0, #0
	cmp r4, #0
	beq _08016C68
	str r5, [r4]
	mov r1, #0x20
	mov r2, #8
	bl cameraRelativeXY
	mov r1, #8
	neg r1, r1
	mov r0, #0x20
	str r0, [sp]
	add r0, r4, #0
	mov r2, #0x1f
	mov r3, #0x10
	bl setObjSize
	add r0, r4, #0
	mov r1, #0
	bl setObjCollisionCallback
	add r0, r4, #0
	mov r1, #0
	bl setObjPhysicsCallback
	add r0, r4, #0
	mov r1, #0
	bl setObjDrawCallback
	ldr r0, [r4, #4]
	ldr r1, [r4, #8]
	str r0, [r4, #0xc]
	str r1, [r4, #0x10]
_08016C68:
	add r0, r4, #0
	add sp, #4
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global cameraRelativeXY
cameraRelativeXY: @ 0x08016C74
	push {lr}
	add r3, r0, #0
	lsl r1, r1, #0x10
	asr r1, r1, #8
	str r1, [r3, #4]
	lsl r2, r2, #0x10
	asr r2, r2, #8
	str r2, [r3, #8]
	add r0, r3, #4
	add r3, #0x14
	add r1, r3, #0
	bl copyModifiedByCamera
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global setObjSize
setObjSize: @ 0x08016C94
	push {r4, lr}
	ldr r4, [sp, #8]
	strh r1, [r0, #0x38]
	strh r2, [r0, #0x3a]
	strh r3, [r0, #0x3c]
	strh r4, [r0, #0x3e]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global setObjCollisionCallback
setObjCollisionCallback: @ 0x08016CA8
	add r2, r0, #0
	str r1, [r2, #0x40]
	cmp r1, #0
	bne _08016CB4
	ldr r0, =(init40_defaultFunc+1)
	str r0, [r2, #0x40]
_08016CB4:
	bx lr
	.align 2, 0
.pool

thumb_func_global setObjPhysicsCallback
setObjPhysicsCallback: @ 0x08016CBC
	add r2, r0, #0
	str r1, [r2, #0x44]
	cmp r1, #0
	bne _08016CC8
	ldr r0, =(init44_sub_8016E98+1)
	str r0, [r2, #0x44]
_08016CC8:
	bx lr
	.align 2, 0
.pool

thumb_func_global setObjDrawCallback
setObjDrawCallback: @ 0x08016CD0
	add r2, r0, #0
	str r1, [r2, #0x48]
	cmp r1, #0
	bne _08016CDC
	ldr r0, =(updateSpriteScreenPos+1)
	str r0, [r2, #0x48]
_08016CDC:
	bx lr
	.align 2, 0
.pool

thumb_func_global setObjVelocity_X
setObjVelocity_X: @ 0x08016CE4
	str r1, [r0, #0x18]
	bx lr

thumb_func_global setObjVelocity_Y
setObjVelocity_Y: @ 0x08016CE8
	str r1, [r0, #0x1c]
	bx lr

thumb_func_global setObjVelocity
setObjVelocity: @ 0x08016CEC
	str r1, [r0, #0x18]
	str r2, [r0, #0x1c]
	bx lr
	.align 2, 0

thumb_func_global setObjMaxVelocityX
setObjMaxVelocityX: @ 0x08016CF4
	str r1, [r0, #0x28]
	bx lr

thumb_func_global setObjMaxVelocityY
setObjMaxVelocityY: @ 0x08016CF8
	str r1, [r0, #0x2c]
	bx lr

thumb_func_global setObjAccelerationX
setObjAccelerationX: @ 0x08016CFC
	str r1, [r0, #0x30]
	bx lr

thumb_func_global setObjAccelerationY
setObjAccelerationY: @ 0x08016D00
	str r1, [r0, #0x34]
	bx lr

thumb_func_global setObjAcceleration
setObjAcceleration: @ 0x08016D04
	str r1, [r0, #0x30]
	str r2, [r0, #0x34]
	bx lr
	.align 2, 0

thumb_func_global clearObjForces
clearObjForces: @ 0x08016D0C
	mov r1, #0
	str r1, [r0, #0x34]
	str r1, [r0, #0x1c]
	str r1, [r0, #0x30]
	str r1, [r0, #0x18]
	bx lr

thumb_func_global clearObjVel_Accel
clearObjVel_Accel: @ 0x08016D18
	mov r1, #0
	str r1, [r0, #0x30]
	str r1, [r0, #0x18]
	bx lr
	
	
	


	@unused

	thumb_func_global clearObjVel_Accel_Y
clearObjVel_Accel_Y: @ 0x08016D20
	mov r1, #0
	str r1, [r0, #0x34]
	str r1, [r0, #0x1c]
	bx lr
	thumb_func_global clearObjVelocity
clearObjVelocity: @ 0x08016D28
	mov r1, #0
	str r1, [r0, #0x1c]
	str r1, [r0, #0x18]
	bx lr
	thumb_func_global clearObjAcceleration
clearObjAcceleration: @ 0x08016D30
	mov r1, #0
	str r1, [r0, #0x34]
	str r1, [r0, #0x30]
	bx lr

	
	
	
	
	
thumb_func_global updateVelocity
updateVelocity: @ 0x08016D38
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x18]
	ldr r1, [r4, #0x30]
	add r0, r0, r1
	str r0, [r4, #0x18]
	ldr r1, [r4, #0x1c]
	ldr r0, [r4, #0x34]
	add r1, r1, r0
	str r1, [r4, #0x1c]
	add r0, r4, #0
	add r0, #0x4e
	ldrb r0, [r0]
	cmp r0, #0
	bne _08016D5C
	add r0, r1, #0
	sub r0, #0x62
	str r0, [r4, #0x1c]
_08016D5C:
	ldr r1, [r4, #0x18]
	ldr r0, [r4, #0x28]
	cmp r1, r0
	bgt _08016D6A
	neg r0, r0
	cmp r1, r0
	bge _08016D6C
_08016D6A:
	str r0, [r4, #0x18]
_08016D6C:
	ldr r1, [r4, #0x1c]
	ldr r0, [r4, #0x2c]
	cmp r1, r0
	bgt _08016D7A
	neg r0, r0
	cmp r1, r0
	bge _08016D7C
_08016D7A:
	str r0, [r4, #0x1c]
_08016D7C:
	add r0, r4, #0
	add r0, #0x18
	bl getVelocityDirection
	add r1, r4, #0
	add r1, #0x4c
	strb r0, [r1]
	pop {r4}
	pop {r0}
	bx r0

thumb_func_global applyVelocity
applyVelocity: @ 0x08016D90
	push {r4, lr}
	sub sp, #8
	add r4, r0, #0
	ldr r0, [r4, #4]
	ldr r1, [r4, #8]
	str r0, [r4, #0xc]
	str r1, [r4, #0x10]
	add r1, r4, #0
	add r1, #0x51
	mov r0, #0
	strb r0, [r1]
	sub r1, #2
	strb r0, [r1]
	add r0, r4, #0
	add r0, #0x52
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _08016DC6
	ldr r0, [r4, #4]
	ldr r1, [r4, #0x18]
	add r0, r0, r1
	str r0, [r4, #4]
	ldr r0, [r4, #8]
	ldr r1, [r4, #0x1c]
	b _08016DE4
_08016DC6:
	ldr r0, [r4, #0x18]
	ldr r1, [r4, #0x1c]
	str r0, [sp]
	str r1, [sp, #4]
	ldr r2, [r4, #0x40]
	add r0, r4, #0
	mov r1, sp
	bl call_r2
	ldr r0, [r4, #4]
	ldr r1, [sp]
	add r0, r0, r1
	str r0, [r4, #4]
	ldr r0, [r4, #8]
	ldr r1, [sp, #4]
_08016DE4:
	add r0, r0, r1
	str r0, [r4, #8]
	add sp, #8
	pop {r4}
	pop {r0}
	bx r0
	
	
	
thumb_func_global sub_8016DF0
sub_8016DF0: @ 0x08016DF0
	ldr r3, =0x03004720
	ldrh r2, [r3]
	lsl r2, r2, #8
	ldr r1, [r0, #4]
	sub r1, r1, r2
	asr r1, r1, #8
	strh r1, [r0, #0x14]
	ldrh r1, [r3, #2]
	lsl r1, r1, #8
	ldr r2, [r0, #8]
	sub r1, r1, r2
	asr r1, r1, #8
	strh r1, [r0, #0x16]
	bx lr
	.align 2, 0
.pool
	thumb_func_global sub_8016E10
sub_8016E10: @ 0x08016E10
	add r2, r0, #0
	ldr r1, [r2]
	cmp r1, #0
	beq locret_8016E22
	ldrh r0, [r2, #0x14]
	strh r0, [r1, #0x10]
	ldr r1, [r2]
	ldrh r0, [r2, #0x16]
	strh r0, [r1, #0x12]
locret_8016E22:
	bx lr
	
	

thumb_func_global runObjPhysics
runObjPhysics: @ 0x08016E24
	push {lr}
	ldr r1, [r0, #0x44]
	bl call_r1
	pop {r0}
	bx r0

thumb_func_global task_unknown
task_unknown: @ 0x08016E30
	push {r4, r5, r6, lr}
	ldr r0, =0x03004750
	ldr r1, [r0]
	ldr r0, [r1, #4]
	ldr r1, [r1, #8]
	mov r2, #0x80
	lsl r2, r2, #5
	add r1, r1, r2
	bl runCameraFunc
	mov r4, #0
	ldr r5, =0x03000DB0
	add r6, r5, #0
	add r6, #0x48
_08016E4C:
	mov r0, #0x54
	add r1, r4, #0
	mul r1, r0, r1
	add r2, r1, r5
	add r0, r2, #0
	add r0, #0x50
	ldrb r0, [r0]
	cmp r0, #0
	beq _08016E68
	add r0, r1, r6
	ldr r1, [r0]
	add r0, r2, #0
	bl call_r1
_08016E68:
	add r4, #1
	cmp r4, #0x2f
	ble _08016E4C
	ldr r4, =0x03004720
	ldrh r0, [r4]
	ldrh r1, [r4, #2]
	bl handleParallaxScroll
	bl sub_8032150
	ldrh r0, [r4]
	ldrh r1, [r4, #2]
	bl animateDoorArrows
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global init44_sub_8016E98
init44_sub_8016E98: @ 0x08016E98
	push {r4, lr}
	add r4, r0, #0
	bl updateVelocity
	add r0, r4, #0
	bl applyVelocity
	pop {r4}
	pop {r0}
	bx r0
	
	
	
	
thumb_func_global init40_defaultFunc
init40_defaultFunc: @ 0x08016EAC
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #0xc
	add r6, r0, #0
	add r4, r1, #0
	add r0, r4, #0
	bl getVelocityDirection
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	mov r8, r0
	mov r0, #0x38
	ldrsh r1, [r6, r0]
	lsl r1, r1, #8
	ldr r0, [r6, #4]
	add r0, r0, r1
	str r0, [sp]
	mov r0, #0x3a
	ldrsh r1, [r6, r0]
	lsl r1, r1, #8
	ldr r0, [r6, #8]
	add r0, r0, r1
	str r0, [sp, #4]
	ldr r0, [r6, #0x3c]
	str r0, [sp, #8]
	mov r1, sp
	mov r0, sp
	ldrh r0, [r0, #8]
	sub r0, #1
	strh r0, [r1, #8]
	mov r0, sp
	ldrh r0, [r0, #0xa]
	sub r0, #1
	strh r0, [r1, #0xa]
	mov r7, #1
	mov r0, r8
	and r0, r7
	cmp r0, #0
	bne _08016F30
	mov r0, #2
	mov r1, r8
	and r0, r1
	add r5, r6, #0
	add r5, #0x51
	cmp r0, #0
	beq _08016F56
	ldr r1, [r4, #4]
	mov r0, sp
	bl sub_80170A4
	add r1, r0, #0
	cmp r1, #0
	beq _08016F50
	ldr r0, [r4, #4]
	add r0, r0, r1
	str r0, [r4, #4]
	ldrb r1, [r5]
	mov r0, #2
	orr r0, r1
	strb r0, [r5]
	add r1, r6, #0
	add r1, #0x4d
	mov r0, #0
	strb r0, [r1]
	b _08016F56
_08016F30:
	ldr r1, [r4, #4]
	mov r0, sp
	bl sub_8017084
	add r1, r0, #0
	add r5, r6, #0
	add r5, #0x51
	cmp r1, #0
	beq _08016F50
	ldr r0, [r4, #4]
	add r0, r0, r1
	str r0, [r4, #4]
	ldrb r1, [r5]
	mov r0, #1
	orr r0, r1
	strb r0, [r5]
_08016F50:
	add r0, r6, #0
	add r0, #0x4d
	strb r7, [r0]
_08016F56:
	ldr r0, [sp, #4]
	ldr r1, [r4, #4]
	add r0, r0, r1
	str r0, [sp, #4]
	mov r0, #4
	mov r1, r8
	and r0, r1
	cmp r0, #0
	beq _08016F82
	ldr r1, [r4]
	mov r0, sp
	bl sub_80170C4
	add r1, r0, #0
	cmp r1, #0
	beq _08016FA8
	ldr r0, [r4]
	add r0, r0, r1
	str r0, [r4]
	ldrb r1, [r5]
	mov r0, #4
	b _08016FA4
_08016F82:
	mov r0, #8
	mov r1, r8
	and r0, r1
	cmp r0, #0
	beq _08016FA8
	ldr r1, [r4]
	mov r0, sp
	bl sub_80170E4
	add r1, r0, #0
	cmp r1, #0
	beq _08016FA8
	ldr r0, [r4]
	add r0, r0, r1
	str r0, [r4]
	ldrb r1, [r5]
	mov r0, #8
_08016FA4:
	orr r0, r1
	strb r0, [r5]
_08016FA8:
	add r0, r6, #0
	add r0, #0x4f
	ldrb r1, [r0]
	ldrb r2, [r5]
	orr r1, r2
	strb r1, [r0]
	mov r0, r8
	cmp r0, #0
	beq _08016FC6
	ldr r0, [r4]
	cmp r0, #0
	bne _08016FCA
	ldr r0, [r4, #4]
	cmp r0, #0
	bne _08016FCA
_08016FC6:
	mov r0, #1
	b _08016FCC
_08016FCA:
	mov r0, #0
_08016FCC:
	add sp, #0xc
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	
	
	

thumb_func_global updateSpriteScreenPos
updateSpriteScreenPos: @ 0x08016FD8
	push {r4, lr}
	add r3, r0, #0
	ldr r2, =0x03004720
	ldrh r1, [r2]
	lsl r1, r1, #8
	ldr r0, [r3, #4]
	sub r0, r0, r1
	asr r4, r0, #8
	strh r4, [r3, #0x14]
	ldrh r0, [r2, #2]
	lsl r0, r0, #8
	ldr r1, [r3, #8]
	sub r0, r0, r1
	asr r0, r0, #8
	strh r0, [r3, #0x16]
	ldr r0, [r3]
	cmp r0, #0
	beq _08017004
	strh r4, [r0, #0x10]
	ldr r1, [r3]
	ldrh r0, [r3, #0x16]
	strh r0, [r1, #0x12]
_08017004:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global getVelocityDirection
getVelocityDirection: @ 0x08017010
	mov r2, #0
	ldr r1, [r0]
	cmp r1, #0
	ble _0801701C
	mov r2, #8
	b _08017022
_0801701C:
	cmp r1, #0
	bge _08017022
	mov r2, #4
_08017022:
	ldr r0, [r0, #4]
	cmp r0, #0
	ble _0801702E
	mov r0, #1
	orr r2, r0
	b _0801703A
_0801702E:
	cmp r0, #0
	bge _0801703A
	mov r0, #2
	orr r2, r0
	lsl r0, r2, #0x18
	lsr r2, r0, #0x18
_0801703A:
	add r0, r2, #0
	bx lr
.align 2, 0	
	
	
	

	thumb_func_global sub_8017040
sub_8017040: @ 0x08017040
	add r3, r0, #0
	ldr r1, [r3]
	add r2, r1, #0
	cmp r1, #0
	bge _0801704C
	neg r2, r1
_0801704C:
	ldr r0, =0x000007FF
	cmp r2, r0
	bgt _0801705C
	mov r0, #0
	b _08017078
	.align 2, 0
.pool
_0801705C:
	add r0, r1, #0
	cmp r0, #0
	ble _08017070
	mov r1, #0x80
	lsl r1, r1, #4
	ldr r2, =0xFFFFF800
	b _08017076
	.align 2, 0
.pool
_08017070:
	ldr r1, =0xFFFFF800
	mov r2, #0x80
	lsl r2, r2, #4
_08017076:
	add r0, r0, r2
_08017078:
	str r0, [r3]
	add r0, r1, #0
	bx lr
	.align 2, 0
.pool





	thumb_func_global sub_8017084
sub_8017084: @ 0x08017084
	push {r4, r5, lr}
	sub sp, #0xc
	mov r2, sp
	ldm r0!, {r3, r4, r5}
	stm r2!, {r3, r4, r5}
	ldr r0, [sp, #4]
	add r0, r0, r1
	str r0, [sp, #4]
	mov r0, sp
	bl sub_08017184
	add sp, #0xc
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	
	thumb_func_global sub_80170A4
sub_80170A4: @ 0x080170A4
	push {r4, r5, lr}
	sub sp, #0xc
	mov r2, sp
	ldm r0!, {r3, r4, r5}
	stm r2!, {r3, r4, r5}
	ldr r0, [sp, #4]
	add r0, r0, r1
	str r0, [sp, #4]
	mov r0, sp
	bl sub_80171B4
	add sp, #0xc
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	
	thumb_func_global sub_80170C4
sub_80170C4: @ 0x080170C4
	push {r4, r5, lr}
	sub sp, #0xc
	mov r2, sp
	ldm r0!, {r3, r4, r5}
	stm r2!, {r3, r4, r5}
	ldr r0, [sp]
	add r0, r0, r1
	str r0, [sp]
	mov r0, sp
	bl sub_80171F4
	add sp, #0xc
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	
	thumb_func_global sub_80170E4
sub_80170E4: @ 0x080170E4
	push {r4, r5, lr}
	sub sp, #0xc
	mov r2, sp
	ldm r0!, {r3, r4, r5}
	stm r2!, {r3, r4, r5}
	ldr r0, [sp]
	add r0, r0, r1
	str r0, [sp]
	mov r0, sp
	bl sub_8017228
	add sp, #0xc
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0

