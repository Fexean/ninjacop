.include "asm/macros.inc"

@complete

thumb_func_global cb_languageSelect
cb_languageSelect: @ 0x080273F8
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	sub sp, #4
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl clearSprites
	bl resetAffineTransformations
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r0, =0x03000588
	mov r2, #0
	strh r2, [r0]
	ldr r1, =0x03002080
	ldr r0, =0x03003C00
	ldrb r0, [r0, #4]
	lsr r0, r0, #6
	add r4, r1, #0
	add r4, #0x2c
	strb r0, [r4]
	cmp r0, #2
	bls _08027436
	strb r2, [r4]
_08027436:
	ldr r0, =0x03000580
	mov r1, #1
	bl allocFont
	bl sub_08027690
	bl sub_08027704
	bl unused_sub_8027778
	ldr r0, =gScrollCursorSpritePal
	mov r1, #2
	bl LoadSpritePalette
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	mov r0, #0xda
	bl allocateSpriteTiles
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldr r0, =gScrollCursorSpriteTiles
	lsl r1, r5, #5
	ldr r7, =0x06010000
	add r1, r1, r7
	bl Bios_LZ77_16_2
	ldr r0, =menu_scroll_highlight_anim
	mov r1, #0
	str r1, [sp]
	add r2, r5, #0
	mov r3, #1
	bl allocSprite
	ldr r3, =0x0300058C
	str r0, [r3]
	add r0, #0x23
	ldrb r2, [r0]
	mov r1, #0x11
	neg r1, r1
	and r1, r2
	strb r1, [r0]
	ldr r0, [r3]
	add r0, #0x22
	strb r6, [r0]
	ldr r3, [r3]
	ldr r1, =dword_81C3180
	ldrb r0, [r4]
	lsl r0, r0, #3
	add r0, r0, r1
	ldr r0, [r0]
	sub r0, #2
	strh r0, [r3, #0x10]
	ldrb r0, [r4]
	lsl r0, r0, #3
	add r2, r1, #4
	add r0, r0, r2
	ldr r0, [r0]
	sub r0, #2
	strh r0, [r3, #0x12]
	mov r4, #0
	mov r8, r1
	mov sb, r2
_080274B4:
	mov r0, #0
	str r0, [sp]
	ldr r0, =off_820CB24
	mov r1, #0
	add r2, r5, #0
	mov r3, #1
	bl allocSprite
	ldr r1, =0x03000590
	lsl r3, r4, #2
	add r3, r3, r1
	str r0, [r3]
	add r0, #0x23
	ldrb r1, [r0]
	mov r7, #0x11
	neg r7, r7
	add r2, r7, #0
	and r1, r2
	strb r1, [r0]
	ldr r0, [r3]
	add r0, #0x22
	strb r6, [r0]
	ldr r2, [r3]
	lsl r1, r4, #3
	mov r7, r8
	add r0, r1, r7
	ldr r0, [r0]
	strh r0, [r2, #0x10]
	ldr r2, [r3]
	add r1, sb
	ldr r0, [r1]
	strh r0, [r2, #0x12]
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #2
	bls _080274B4
	ldr r0, =(cb_languageSelect_switch+1)
	bl setCurrentTaskFunc
	add sp, #4
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global cb_languageSelect_switch
cb_languageSelect_switch: @ 0x0802754C
	push {r4, lr}
	ldr r1, =languageSelectStates
	ldr r4, =0x03000588
	ldrh r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl call_r0
	lsl r0, r0, #0x10
	asr r1, r0, #0x10
	mov r0, #1
	neg r0, r0
	cmp r1, r0
	beq _08027580
	cmp r1, #1
	bne _08027586
	ldrh r0, [r4]
	add r0, #1
	strh r0, [r4]
	b _08027586
	.align 2, 0
.pool
_08027580:
	ldr r0, =(cb_konamiLogoLoad+1)
	bl setCurrentTaskFunc
_08027586:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global langSelectState1
langSelectState1: @ 0x08027590
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _080275A6
	mov r0, #0
	b _080275A8
_080275A6:
	mov r0, #1
_080275A8:
	pop {r1}
	bx r1

thumb_func_global langSelectState2
langSelectState2: @ 0x080275AC
	push {r4, lr}
	ldr r4, =0x03002080
	ldrh r1, [r4, #0xc]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _080275C4
	mov r0, #1
	b _08027634
	.align 2, 0
.pool
_080275C4:
	ldrh r1, [r4, #0x14]
	mov r0, #0xc0
	and r0, r1
	cmp r0, #0
	beq _0802762A
	bl sub_080277E4
	bl sub_08027808
	bl sub_0802782C
	ldrh r1, [r4, #0x14]
	mov r0, #0x40
	and r0, r1
	cmp r0, #0
	beq _080275EC
	add r4, #0x2c
	ldrb r0, [r4]
	add r0, #2
	b _080275F2
_080275EC:
	add r4, #0x2c
	ldrb r0, [r4]
	add r0, #1
_080275F2:
	mov r1, #3
	bl Bios_modulo
	strb r0, [r4]
	bl sub_08027690
	bl sub_08027704
	bl unused_sub_8027778
	ldr r0, =0x0300058C
	ldr r3, [r0]
	ldr r2, =dword_81C3180
	ldr r1, =0x03002080
	add r1, #0x2c
	ldrb r0, [r1]
	lsl r0, r0, #3
	add r0, r0, r2
	ldr r0, [r0]
	sub r0, #2
	strh r0, [r3, #0x10]
	ldrb r0, [r1]
	lsl r0, r0, #3
	add r2, #4
	add r0, r0, r2
	ldr r0, [r0]
	sub r0, #2
	strh r0, [r3, #0x12]
_0802762A:
	ldr r0, =0x0300058C
	ldr r0, [r0]
	bl updateSpriteAnimation
	mov r0, #0
_08027634:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global langSelectState3
langSelectState3: @ 0x08027648
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _08027660
	mov r0, #0
	b _08027662
_08027660:
	mov r0, #1
_08027662:
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global langSelectState4
langSelectState4: @ 0x08027668
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl clearSprites
	bl resetAffineTransformations
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	mov r0, #1
	neg r0, r0
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_08027690
sub_08027690: @ 0x08027690
	push {r4, r5, r6, lr}
	sub sp, #4
	ldr r1, =off_81C31C8
	ldr r4, =0x03002080
	add r4, #0x2c
	ldrb r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r5, [r0]
	add r0, r5, #0
	mov r1, #0
	bl hideString
	ldr r6, =0x03000580
	mov r0, #0
	str r0, [sp]
	add r0, r5, #0
	add r1, r6, #0
	mov r2, #0
	mov r3, #0
	bl bufferString
	ldr r0, =dword_81C3180
	ldr r1, [r0]
	add r1, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r2, [r0, #4]
	add r2, #9
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	str r6, [sp]
	add r0, r5, #0
	mov r3, #0
	bl setStringPos
	mov r3, #1
	ldrb r0, [r4]
	cmp r0, #0
	bne _080276E2
	mov r3, #4
_080276E2:
	add r0, r5, #0
	add r1, r6, #0
	mov r2, #0
	bl setStringColor
	add sp, #4
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08027704
sub_08027704: @ 0x08027704
	push {r4, r5, r6, lr}
	sub sp, #4
	ldr r1, =off_81C3208
	ldr r4, =0x03002080
	add r4, #0x2c
	ldrb r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r5, [r0]
	add r0, r5, #0
	mov r1, #8
	bl hideString
	ldr r6, =0x03000580
	mov r0, #0
	str r0, [sp]
	add r0, r5, #0
	add r1, r6, #0
	mov r2, #8
	mov r3, #0
	bl bufferString
	ldr r0, =dword_81C3180
	ldr r1, [r0, #8]
	add r1, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r2, [r0, #0xc]
	add r2, #9
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	str r6, [sp]
	add r0, r5, #0
	mov r3, #8
	bl setStringPos
	mov r3, #1
	ldrb r0, [r4]
	cmp r0, #1
	bne _08027756
	mov r3, #4
_08027756:
	add r0, r5, #0
	add r1, r6, #0
	mov r2, #8
	bl setStringColor
	add sp, #4
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global unused_sub_8027778
unused_sub_8027778: @ 0x08027778
	push {r4, r5, r6, lr}
	sub sp, #4
	ldr r1, =off_81C3244
	ldr r4, =0x03002080
	add r4, #0x2c
	ldrb r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r5, [r0]
	ldr r6, =0x03000580
	mov r0, #0
	str r0, [sp]
	add r0, r5, #0
	add r1, r6, #0
	mov r2, #0x10
	mov r3, #0
	bl bufferString
	ldr r0, =dword_81C3180
	ldr r1, [r0, #0x10]
	add r1, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r2, [r0, #0x14]
	add r2, #9
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	str r6, [sp]
	add r0, r5, #0
	mov r3, #0x10
	bl setStringPos
	mov r3, #1
	ldrb r0, [r4]
	cmp r0, #2
	bne _080277C2
	mov r3, #4
_080277C2:
	add r0, r5, #0
	add r1, r6, #0
	mov r2, #0x10
	bl setStringColor
	add sp, #4
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_080277E4
sub_080277E4: @ 0x080277E4
	push {lr}
	ldr r1, =off_81C31C8
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0
	bl hideString
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08027808
sub_08027808: @ 0x08027808
	push {lr}
	ldr r1, =off_81C3208
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #8
	bl hideString
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802782C
sub_0802782C: @ 0x0802782C
	push {lr}
	ldr r1, =off_81C3244
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0x10
	bl hideString
	pop {r0}
	bx r0
	.align 2, 0
.pool
