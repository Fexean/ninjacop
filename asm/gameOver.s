.include "asm/macros.inc"

@completely disassembled
@needs a split	


thumb_func_global loadGameOverScreen
loadGameOverScreen: @ 0x08029CC0
	push {r4, r5, r6, lr}
	sub sp, #4
	ldr r4, =0x03003CD4
	mov r1, #0x82
	lsl r1, r1, #5
	add r0, r1, #0
	strh r0, [r4]
	bl FreeAllPalFadeTasks
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	mov r0, #2
	bl endPriorityOrHigherTask
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r0, =0x03000590
	mov r1, #3
	bl allocFont
	ldr r0, =0x03000598
	mov r1, #1
	bl allocFont
	ldr r0, =gameOverPal
	mov r2, #0xa0
	lsl r2, r2, #0x13
	mov r1, #1
	bl memcpy_pal
	ldr r0, =gameOverTiles
	mov r1, #0xc0
	lsl r1, r1, #0x13
	bl Bios_LZ77_16_2
	ldr r0, =gameOverMap
	ldr r1, =0x06008000
	mov r6, #0
	str r6, [sp]
	mov r2, #0
	mov r3, #0
	bl LZ77_mapCpy
	ldr r1, =0x03003D00
	ldr r0, =0x00001003
	strh r0, [r1, #6]
	ldrh r0, [r4]
	mov r2, #0x80
	lsl r2, r2, #4
	add r1, r2, #0
	orr r0, r1
	strh r0, [r4]
	mov r0, #0x25
	bl allocateSpriteTiles
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	ldr r0, =gameOverYesNoSprPal
	mov r1, #1
	bl LoadSpritePalette
	add r5, r0, #0
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	ldr r0, =gameOverYesNoSprTiles
	lsl r1, r4, #5
	ldr r2, =0x06010000
	add r1, r1, r2
	bl Bios_LZ77_16_2
	ldr r0, =0x0300058A
	strh r6, [r0]
	ldr r2, =gameOverAnimations
	ldrh r1, [r0]
	lsl r1, r1, #2
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #3
	add r1, r1, r0
	add r1, r1, r2
	ldr r0, [r1]
	str r6, [sp]
	mov r1, #0
	add r2, r4, #0
	mov r3, #3
	bl allocSprite
	ldr r1, =0x03000584
	str r0, [r1]
	add r0, #0x22
	strb r5, [r0]
	ldr r1, [r1]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	ldr r0, =0x03000580
	strh r6, [r0]
	ldr r0, =0x0300058E
	strh r6, [r0]
	ldr r5, =0x03003D40
	ldr r0, =0x03003C00
	add r4, r0, #0
	add r4, #0xce
	ldrh r1, [r4]
	mov r0, #0x64
	mul r0, r1, r0
	ldr r1, [r5, #0x1c]
	cmp r1, r0
	bls _08029DB8
	add r0, r1, #0
	mov r1, #0x64
	bl Bios_Div
	strh r0, [r4]
_08029DB8:
	bl loadHidden_scoreStrings
	bl loadScoreValueStrings
	add r2, r5, #0
	add r2, #0x20
	ldrb r1, [r2]
	mov r0, #2
	neg r0, r0
	and r0, r1
	mov r1, #2
	orr r0, r1
	strb r0, [r2]
	ldr r0, =(cb_gameOver+1)
	bl setCurrentTaskFunc
	add sp, #4
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global cb_gameOver
cb_gameOver: @ 0x08029E34
	push {r4, r5, lr}
	ldr r1, =gameoverStates
	ldr r0, =0x03000580
	ldrh r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl call_r0
	lsl r0, r0, #0x10
	mov r1, #0x80
	lsl r1, r1, #9
	add r0, r0, r1
	asr r0, r0, #0x10
	cmp r0, #5
	bls _08029E56
	b _08029FBE
_08029E56:
	lsl r0, r0, #2
	ldr r1, =_08029E6C
	add r0, r0, r1
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
.pool
_08029E6C: @ jump table
	.4byte _08029FB8 @ case 0
	.4byte _08029FBE @ case 1
	.4byte _08029E84 @ case 2
	.4byte _08029E94 @ case 3
	.4byte _08029EF0 @ case 4
	.4byte _08029EFC @ case 5
_08029E84:
	ldr r1, =0x03000580
	ldrh r0, [r1]
	add r0, #1
	strh r0, [r1]
	b _08029FBE
	.align 2, 0
.pool
_08029E94:
	ldr r5, =0x03003D40
	mov r0, #0
	strb r0, [r5, #0x10]
	strb r0, [r5, #4]
	mov r1, #0
	strh r0, [r5, #6]
	strb r1, [r5, #3]
	bl ClearIWRAMRegions
	bl resetKeys
	bl setPlayerStats_andtuff_8031B14
	ldrb r0, [r5, #2]
	cmp r0, #0
	beq _08029ECA
	ldr r4, =0x03003C00
	ldr r0, [r4]
	lsl r0, r0, #0xd
	lsr r0, r0, #0x19
	bl sub_0803242C
	ldrh r0, [r4, #2]
	lsl r0, r0, #0x16
	lsr r0, r0, #0x19
	bl sub_08032444
_08029ECA:
	ldrb r0, [r5]
	cmp r0, #0
	beq _08029EDA
	ldr r0, =0x03003C00
	add r0, #0xcc
	ldrh r1, [r0]
	mov r0, #0x64
	mul r0, r1, r0
_08029EDA:
	str r0, [r5, #0x1c]
	ldr r0, =(cb_startLevelLoad_2+1)
	bl setCurrentTaskFunc
	b _08029FBE
	.align 2, 0
.pool
_08029EF0:
	ldr r1, =0x03000580
	mov r0, #6
	strh r0, [r1]
	b _08029FBE
	.align 2, 0
.pool
_08029EFC:
	ldr r0, =0x03003D40
	ldrb r1, [r0]
	add r2, r0, #0
	cmp r1, #2
	beq _08029FBE
	ldrb r0, [r2, #1]
	add r1, r0, #0
	cmp r1, #2
	bhi _08029F44
	ldrb r1, [r2, #8]
	cmp r1, #0
	beq _08029F34
	mov r1, #0
	ldrb r3, [r2, #8]
	mov r4, #1
_08029F1A:
	add r0, r3, #0
	asr r0, r1
	and r0, r4
	cmp r0, #0
	bne _08029F4E
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	cmp r1, #2
	bls _08029F1A
	b _08029FA6
	.align 2, 0
.pool
_08029F34:
	strb r1, [r2, #9]
	str r1, [r2, #0x1c]
	ldr r0, =0x03003C00
	add r0, #0xcc
	strh r1, [r0]
	b _08029F6A
	.align 2, 0
.pool
_08029F44:
	cmp r1, #5
	bne _08029F80
	mov r0, #0
	strb r0, [r2, #2]
	b _08029F94
_08029F4E:
	strb r1, [r2, #1]
	ldr r1, =stageCount
	ldrb r0, [r2, #1]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrb r0, [r0]
	sub r0, #1
	strb r0, [r2, #2]
	strb r4, [r2, #0x10]
	ldr r0, =0x03003C00
	ldrb r0, [r0]
	lsl r0, r0, #0x1a
	lsr r0, r0, #0x1a
	strb r0, [r2, #9]
_08029F6A:
	ldr r0, =(cb_saving+1)
	bl setCurrentTaskFunc
	b _08029FA6
	.align 2, 0
.pool
_08029F80:
	sub r0, #1
	strb r0, [r2, #1]
	ldr r1, =stageCount
	ldrb r0, [r2, #1]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrb r0, [r0]
	sub r0, #1
	strb r0, [r2, #2]
	mov r0, #1
_08029F94:
	strb r0, [r2, #0x10]
	ldr r0, =0x03003C00
	ldrb r0, [r0]
	lsl r0, r0, #0x1a
	lsr r0, r0, #0x1a
	strb r0, [r2, #9]
	ldr r0, =(cb_saving+1)
	bl setCurrentTaskFunc
_08029FA6:
	bl setPlayerStats_andtuff_8031B14
	b _08029FBE
	.align 2, 0
.pool
_08029FB8:
	ldr r0, =(cb_loadTitleScreen+1)
	bl setCurrentTaskFunc
_08029FBE:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global gameover_missionfailed
gameover_missionfailed: @ 0x08029FC8
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _08029FDE
	mov r0, #0
	b _0802A016
_08029FDE:
	ldr r1, =0x03003D74
	ldr r2, =0x00003F50
	add r0, r2, #0
	strh r0, [r1]
	ldr r1, =0x03003D94
	mov r2, #0x80
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl bufferStr_mission
	mov r0, #0x18
	mov r1, #0x28
	bl positionStr_mission
	bl bufferStr_failed
	mov r0, #0x50
	mov r1, #0x40
	bl positionStr_failed
	ldr r1, =0x0300058C
	mov r0, #0
	strh r0, [r1]
	mov r0, #0xe
	bl playSong
	mov r0, #1
_0802A016:
	pop {r1}
	bx r1
	.align 2, 0
.pool
	thumb_func_global gameover_fadein
gameover_fadein: @ 0x0802A02C
	push {r4, lr}
	ldr r2, =0x0300058C
	ldrh r0, [r2]
	add r0, #1
	strh r0, [r2]
	mov r1, #0x10
	and r0, r1
	cmp r0, #0
	beq _0802A05C
	ldr r3, =0x03003D94
	ldrb r1, [r3]
	add r1, #1
	cmp r1, #0x10
	bls _0802A04A
	mov r1, #0x10
_0802A04A:
	mov r4, #0x80
	lsl r4, r4, #5
	add r0, r4, #0
	orr r0, r1
	strh r0, [r3]
	lsl r0, r0, #0x10
	ldr r1, =0x10100000
	cmp r0, r1
	beq _0802A06C
_0802A05C:
	mov r0, #0
	b _0802A07E
	.align 2, 0
.pool
_0802A06C:
	mov r1, #0
	strh r1, [r2]
	ldr r0, =0x03003D74
	strh r1, [r0]
	bl showSTR_score
	bl gameover_showScore
	mov r0, #1
_0802A07E:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool
	thumb_func_global sub_802A088
sub_802A088: @ 0x0802A088
	ldr r1, =0x0300058C
	ldrh r0, [r1]
	add r0, #1
	strh r0, [r1]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0x1e
	bhi _0802A0A0
	mov r0, #0
	b locret_802A0A6
	.align 2, 0
.pool
_0802A0A0:
	mov r0, #0
	strh r0, [r1]
	mov r0, #1
locret_802A0A6:
	bx lr



thumb_func_global gameOver_loadContinue
gameOver_loadContinue: @ 0x0802A0A8
	push {r4, r5, lr}
	ldr r0, =0x03002080
	ldrh r1, [r0, #0xc]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _0802A114
	ldr r0, =0x0300058E
	ldrh r0, [r0]
	cmp r0, #0
	bne _0802A104
	bl hideSTR_mission
	bl hideStr_failed
	bl sub_0802A3A4
	mov r0, #0xf0
	mov r1, #0x28
	bl positionSTR_continue
	ldr r5, =0x030005A0
	bl sub_0802A438
	add r4, r0, #0
	bl getContinueStrX
	sub r4, r4, r0
	mov r0, #0xf0
	sub r0, r0, r4
	mov r1, #2
	bl Bios_Div
	strh r0, [r5]
	mov r0, #0xdf
	bl playSong
	mov r0, #1
	b _0802A116
	.align 2, 0
.pool
_0802A104:
	ldr r1, =0x03000582
	mov r0, #4
	strh r0, [r1]
	mov r0, #3
	b _0802A116
	.align 2, 0
.pool
_0802A114:
	mov r0, #0
_0802A116:
	pop {r4, r5}
	pop {r1}
	bx r1
	
thumb_func_global gameover_positionContinueStr
gameover_positionContinueStr: @ 0x0802A11C
	push {r4, lr}
	bl getContinueStrX
	add r1, r0, #0
	sub r1, #0x10
	ldr r0, =0x030005A0
	ldrh r0, [r0]
	cmp r1, r0
	blt _0802A140
	lsl r0, r1, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x28
	bl positionSTR_continue
	mov r0, #0
	b _0802A174
	.align 2, 0
.pool
_0802A140:
	mov r1, #0x28
	bl positionSTR_continue
	ldr r0, =off_81C6978
	ldr r4, =0x03000584
	ldr r1, [r4]
	add r1, #0x22
	ldrb r1, [r1]
	lsl r1, r1, #0x18
	asr r1, r1, #0x18
	lsl r1, r1, #5
	ldr r2, =0x05000200
	add r1, r1, r2
	mov r2, #0
	bl startPalAnimation
	ldr r1, =0x03000588
	strh r0, [r1]
	ldr r1, [r4]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	mov r0, #1
_0802A174:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool
	thumb_func_global gameover_waitYesNo
gameover_waitYesNo: @ 0x0802A18C
	push {r4, lr}
	ldr r4, =0x03002080
	ldrh r1, [r4, #0xc]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _0802A1D4
	ldr r0, =0x0300058A
	ldrh r0, [r0]
	cmp r0, #0
	beq _0802A1C0
	mov r0, #0xcd
	bl playSong
	ldr r1, =0x03000582
	mov r2, #1
	neg r2, r2
	add r0, r2, #0
	strh r0, [r1]
	b _0802A1CC
	.align 2, 0
.pool
_0802A1C0:
	ldr r1, =0x03000582
	mov r0, #2
	strh r0, [r1]
	mov r0, #0xcc
	bl playSong
_0802A1CC:
	mov r0, #1
	b _0802A20E
	.align 2, 0
.pool
_0802A1D4:
	mov r0, #0x30
	and r0, r1
	cmp r0, #0
	beq _0802A20C
	ldr r2, =0x0300058A
	ldrh r0, [r2]
	mov r1, #1
	eor r0, r1
	strh r0, [r2]
	ldr r0, =0x03000584
	ldr r0, [r0]
	ldr r3, =gameOverAnimations
	ldrh r2, [r2]
	lsl r2, r2, #2
	add r1, r4, #0
	add r1, #0x2c
	ldrb r1, [r1]
	lsl r1, r1, #3
	add r2, r2, r1
	add r2, r2, r3
	ldr r1, [r2]
	ldrh r2, [r0, #0xc]
	mov r3, #0
	bl setSpriteAnimation
	mov r0, #0xcb
	bl playSong
_0802A20C:
	mov r0, #0
_0802A20E:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool
	thumb_func_global sub_802A220
sub_802A220: @ 0x0802A220
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0802A238
	mov r0, #0
	b _0802A23A
_0802A238:
	mov r0, #1
_0802A23A:
	pop {r1}
	bx r1
	.align 2, 0
	thumb_func_global ganeover_clear
ganeover_clear: @ 0x0802A240
	push {lr}
	bl FreeAllPalFadeTasks
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	bl stopSoundTracks
	ldr r0, =0x03000582
	mov r1, #0
	ldrsh r0, [r0, r1]
	pop {r1}
	bx r1
	.align 2, 0
.pool
	thumb_func_global bufferStr_mission
bufferStr_mission: @ 0x0802A27C
	push {lr}
	sub sp, #4
	ldr r1, =MissionStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000590
	mov r2, #1
	str r2, [sp]
	mov r2, #0
	mov r3, #3
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global positionStr_mission
positionStr_mission: @ 0x0802A2B0
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =MissionStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000590
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global hideSTR_mission
hideSTR_mission: @ 0x0802A2EC
	push {lr}
	ldr r1, =MissionStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0
	bl hideString
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
	thumb_func_global bufferStr_failed
bufferStr_failed: @ 0x0802A310
	push {lr}
	sub sp, #4
	ldr r1, =FailedStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000590
	mov r2, #1
	str r2, [sp]
	mov r2, #7
	mov r3, #3
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
	
	thumb_func_global positionStr_failed
positionStr_failed: @ 0x0802A344
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =FailedStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000590
	str r1, [sp]
	add r1, r3, #0
	mov r3, #7
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
	
thumb_func_global hideStr_failed
hideStr_failed: @ 0x0802A380
	push {lr}
	ldr r1, =FailedStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #7
	bl hideString
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802A3A4
sub_0802A3A4: @ 0x0802A3A4
	push {lr}
	sub sp, #4
	ldr r1, =ContinueStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000590
	mov r2, #0
	str r2, [sp]
	mov r2, #0xf
	mov r3, #3
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global positionSTR_continue
positionSTR_continue: @ 0x0802A3D8
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =ContinueStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000590
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0xf
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global getContinueStrX
getContinueStrX: @ 0x0802A414
	push {lr}
	ldr r1, =ContinueStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0xf
	bl str_getX_ofFirstChar
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_0802A438
sub_0802A438: @ 0x0802A438
	push {lr}
	ldr r1, =ContinueStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0xf
	bl str_get_last_width_OamX_sum
	pop {r1}
	bx r1
	.align 2, 0
.pool



thumb_func_global sub_802A45C
sub_802A45C: @ 0x0802A45C
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl FreeAllPalFadeTasks
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	mov r0, #2
	bl endPriorityOrHigherTask
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r1, =0x03000580
	mov r0, #7
	strh r0, [r1]
	ldr r1, =0x0300058E
	mov r0, #1
	strh r0, [r1]
	ldr r2, =0x03003D40
	add r2, #0x20
	ldrb r0, [r2]
	mov r1, #1
	orr r0, r1
	mov r1, #3
	neg r1, r1
	and r0, r1
	strb r0, [r2]
	ldr r1, =0x03000582
	mov r0, #4
	strh r0, [r1]
	ldr r0, =(cb_gameOver+1)
	bl setCurrentTaskFunc
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global loadHidden_scoreStrings
loadHidden_scoreStrings: @ 0x0802A4CC
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #4
	ldr r1, =HighScorePtsStrings
	ldr r5, =0x03002080
	add r5, #0x2c
	ldrb r0, [r5]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r7, [r0]
	ldr r6, =0x03000598
	mov r0, #0
	mov r8, r0
	str r0, [sp]
	add r0, r7, #0
	add r1, r6, #0
	mov r2, #0x23
	mov r3, #3
	bl bufferString
	add r0, r7, #0
	mov r1, #0x23
	bl str_get_last_width_OamX_sum
	mov r4, #0xec
	sub r4, r4, r0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	str r6, [sp]
	add r0, r7, #0
	add r1, r4, #0
	mov r2, #0x94
	mov r3, #0x23
	bl setStringPos
	add r0, r7, #0
	mov r1, #0x23
	bl hideString
	ldr r1, =scoreStrings
	ldrb r0, [r5]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r7, [r0]
	mov r0, r8
	str r0, [sp]
	add r0, r7, #0
	add r1, r6, #0
	mov r2, #0x19
	mov r3, #3
	bl bufferString
	str r6, [sp]
	add r0, r7, #0
	add r1, r4, #0
	mov r2, #0x88
	mov r3, #0x19
	bl setStringPos
	add r0, r7, #0
	mov r1, #0x19
	bl hideString
	add sp, #4
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
	thumb_func_global showSTR_score
showSTR_score: @ 0x0802A568
	push {r4, lr}
	ldr r1, =HighScorePtsStrings
	ldr r4, =0x03002080
	add r4, #0x2c
	ldrb r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0x23
	bl showString
	ldr r1, =scoreStrings
	ldrb r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0x19
	bl showString
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
thumb_func_global loadScoreValueStrings
loadScoreValueStrings: @ 0x0802A5A0
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #4
	mov r0, #0xb
	bl allocateSpriteTiles
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	ldr r0, =dword_8208DA0
	mov r1, #1
	bl LoadSpritePalette
	mov r8, r0
	mov r0, r8
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r8, r0
	ldr r0, =score_numberTiles
	lsl r4, r4, #0x10
	asr r1, r4, #0xb
	ldr r2, =0x06010000
	add r1, r1, r2
	bl Bios_LZ77_16_2
	ldr r1, =HighScorePtsStrings
	ldr r6, =0x03002080
	add r6, #0x2c
	ldrb r0, [r6]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0x23
	bl str_getX_ofFirstChar
	add r5, r0, #0
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	ldr r0, =0x03003D40
	ldr r0, [r0, #0x1c]
	ldr r1, =0x030005B0
	mov r2, #0
	bl createScoreString
	ldr r0, =scoreAnimation
	lsr r4, r4, #0x10
	mov r1, #0
	mov sl, r1
	str r1, [sp]
	add r2, r4, #0
	mov r3, #3
	bl allocSprite
	ldr r2, =0x030005A4
	str r0, [r2]
	ldr r7, =gameover_score_x_lut
	ldrb r1, [r6]
	lsl r1, r1, #1
	add r1, r1, r7
	ldrh r1, [r1]
	add r1, r5, r1
	strh r1, [r0, #0x10]
	mov r1, #0x88
	strh r1, [r0, #0x12]
	add r0, #0x22
	mov r1, r8
	strb r1, [r0]
	ldr r1, [r2]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	mov sb, r2
	mov r2, sb
	orr r0, r2
	strb r0, [r1]
	ldr r0, =0x03003C00
	add r0, #0xce
	ldrh r1, [r0]
	mov r0, #0x64
	mul r0, r1, r0
	ldr r1, =0x03000610
	mov r2, #0
	bl createScoreString
	ldr r0, =highscoreAnimation
	mov r1, sl
	str r1, [sp]
	mov r1, #0
	add r2, r4, #0
	mov r3, #3
	bl allocSprite
	ldr r2, =0x030005A8
	str r0, [r2]
	ldrb r1, [r6]
	lsl r1, r1, #1
	add r1, r1, r7
	ldrh r1, [r1]
	add r5, r5, r1
	strh r5, [r0, #0x10]
	mov r1, #0x94
	strh r1, [r0, #0x12]
	add r0, #0x22
	mov r1, r8
	strb r1, [r0]
	ldr r1, [r2]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, sb
	orr r0, r2
	strb r0, [r1]
	add sp, #4
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool


thumb_func_global gameover_showScore
gameover_showScore: @ 0x0802A6CC
	ldr r0, =0x030005A4
	ldr r1, [r0]
	add r1, #0x23
	ldrb r3, [r1]
	mov r2, #0x11
	neg r2, r2
	add r0, r2, #0
	and r0, r3
	strb r0, [r1]
	ldr r0, =0x030005A8
	ldr r0, [r0]
	add r0, #0x23
	ldrb r1, [r0]
	and r2, r1
	strb r2, [r0]
	bx lr
	.align 2, 0
.pool














@ More intro scroll functions, not sure how to split these


thumb_func_global getLevelIntroId
getLevelIntroId: @ 0x0802A6F4
	ldr r0, =0x03003D40
	ldrb r1, [r0, #1]
	add r2, r0, #0
	cmp r1, #2
	bne _0802A70C
	ldrb r0, [r2, #2]
	cmp r0, #1
	bls _0802A70C
	mov r0, #6
	b _0802A71E
	.align 2, 0
.pool
_0802A70C:
	ldrb r0, [r2, #1]
	cmp r0, #5
	bne _0802A71C
	ldrb r0, [r2, #0x11]
	cmp r0, #2
	bne _0802A71C
	mov r0, #7
	b _0802A71E
_0802A71C:
	ldrb r0, [r2, #1]
_0802A71E:
	bx lr

thumb_func_global startLevelIntro
startLevelIntro: @ 0x0802A720
	push {r4, lr}
	ldr r4, =scrollIntroStructs
	bl getLevelIntroId
	lsl r0, r0, #0x10
	lsr r0, r0, #0xe
	add r0, r0, r4
	ldr r0, [r0]
	bl start_intro_scroll
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global start_intro_scroll
start_intro_scroll: @ 0x0802A740
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x10
	add r4, r0, #0
	ldr r0, [r4, #4]
	mov r2, #0xa0
	lsl r2, r2, #0x13
	mov r1, #1
	bl memcpy_pal
	ldr r0, [r4, #0x20]
	ldr r2, =0x05000020
	mov r1, #4
	bl memcpy_pal
	ldr r0, [r4]
	mov r1, #0xc0
	lsl r1, r1, #0x13
	bl Bios_LZ77_16_2
	ldr r7, =0x06000160
	mov r5, #0
	add r0, r4, #0
	add r0, #0x18
	str r0, [sp, #8]
_0802A778:
	lsl r1, r5, #2
	add r0, r4, #0
	add r0, #8
	add r0, r0, r1
	ldr r0, [r0]
	add r1, r7, #0
	bl Bios_LZ77_16_2
	lsl r1, r5, #1
	add r0, r4, #0
	add r0, #0x18
	add r0, r0, r1
	ldrh r0, [r0]
	add r7, r7, r0
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, #3
	bls _0802A778
	add r1, sp, #4
	mov r0, #0
	strh r0, [r1]
	ldr r1, =0x0200E630
	ldr r2, =0x01006000
	add r0, sp, #4
	bl Bios_memcpy
	ldr r0, [r4, #0x24]
	ldr r1, =0x02001920
	mov r2, #0
	str r2, [sp]
	mov r3, #0
	bl LZ77_mapCpy
	mov r5, #0
	add r4, #0x28
	str r4, [sp, #0xc]
_0802A7C2:
	ldr r6, =0x02001920
	lsl r1, r5, #6
	ldr r0, =0x0200E66C
	add r7, r1, r0
	mov r4, #0
_0802A7CC:
	add r0, r6, #0
	add r1, r7, #0
	mov r2, #0x20
	bl Bios_memcpy
	add r6, #0x40
	mov r1, #0x80
	lsl r1, r1, #1
	add r7, r7, r1
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0x13
	bls _0802A7CC
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, #3
	bls _0802A7C2
	mov r0, #0xb0
	lsl r0, r0, #1
	mov r1, #0x20
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sb, r0
	mov r5, #0
	ldr r3, =0x02001920
	mov sl, r3
_0802A808:
	lsl r0, r5, #2
	ldr r1, [sp, #0xc]
	add r0, r1, r0
	ldr r0, [r0]
	mov r1, #1
	str r1, [sp]
	mov r1, sl
	mov r2, #0
	mov r3, sb
	bl LZ77_mapCpy
	mov r6, sl
	lsl r2, r5, #1
	add r0, r2, r5
	lsl r0, r0, #4
	ldr r1, =0x0200EA78
	add r7, r0, r1
	mov r4, #0
	mov r8, r2
	add r5, #1
_0802A830:
	add r0, r6, #0
	add r1, r7, #0
	mov r2, #0x12
	bl Bios_memcpy
	add r6, #0x40
	mov r3, #0x80
	lsl r3, r3, #1
	add r7, r7, r3
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0xb
	bls _0802A830
	ldr r0, [sp, #8]
	add r0, r8
	ldrh r0, [r0]
	mov r1, #0x20
	bl Bios_Div
	add r0, sb
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sb, r0
	lsl r0, r5, #0x10
	lsr r5, r0, #0x10
	cmp r5, #3
	bls _0802A808
	ldr r1, =0x03003D00
	ldr r0, =0x00001F03
	strh r0, [r1, #6]
	ldr r2, =0x03003CD4
	ldrh r0, [r2]
	mov r3, #0x80
	lsl r3, r3, #4
	add r1, r3, #0
	orr r0, r1
	strh r0, [r2]
	mov r0, #0
	bl updateScrollTilemap
	add sp, #0x10
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global updateScrollTilemap
updateScrollTilemap: @ 0x0802A8BC
	push {lr}
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	ldr r3, =0x03001EB0
	mov r2, #7
	and r1, r2
	strb r1, [r3, #4]
	lsr r0, r0, #0x13
	lsl r0, r0, #1
	ldr r1, =0x0200E630
	add r0, r0, r1
	str r0, [r3]
	ldr r0, =(CB_updateScrollingMap+1)
	mov r1, #0
	bl addCallback
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global CB_updateScrollingMap
CB_updateScrollingMap: @ 0x0802A8EC
	push {r4, r5, r6, lr}
	ldr r2, =0x03003D38
	ldr r1, =0x03001EB0
	ldrb r0, [r1, #4]
	strh r0, [r2, #6]
	ldr r4, [r1]
	ldr r6, =0x0600F800
	mov r5, #0
_0802A8FC:
	add r0, r4, #0
	add r1, r6, #0
	mov r2, #0x20
	bl Bios_memcpy
	mov r0, #0x80
	lsl r0, r0, #1
	add r4, r4, r0
	add r6, #0x40
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, #0x14
	bls _0802A8FC
	ldr r0, =(CB_updateScrollingMap+1)
	bl deleteCallback
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global unused_loadFace
unused_loadFace: @ 0x0802A934
	push {r4, r5, r6, lr}
	sub sp, #4
	add r6, r0, #0
	ldr r4, =unusedFaces
	bl getLevelIntroId
	lsl r0, r0, #0x10
	lsr r0, r0, #0xe
	add r0, r0, r4
	ldr r5, [r0]
	cmp r5, #0
	beq _0802A9E8
	ldr r0, [r5, #8]
	ldrb r1, [r5, #0xe]
	ldrb r2, [r6, #2]
	lsl r2, r2, #5
	mov r3, #0xa0
	lsl r3, r3, #0x13
	add r2, r2, r3
	bl memcpy_pal
	ldr r0, [r5]
	ldrb r1, [r6, #3]
	lsl r1, r1, #0xe
	ldrb r2, [r6, #1]
	lsl r2, r2, #5
	mov r4, #0xc0
	lsl r4, r4, #0x13
	add r2, r2, r4
	add r1, r1, r2
	bl Bios_LZ77_16_2
	ldr r0, [r5, #4]
	ldrb r1, [r6, #4]
	lsl r1, r1, #0xb
	add r1, r1, r4
	ldrb r3, [r6, #1]
	mov r2, #2
	ldrsb r2, [r6, r2]
	str r2, [sp]
	mov r2, #0
	bl LZ77_mapCpy
	ldr r0, =0x03003D00
	ldrb r2, [r6]
	lsl r2, r2, #1
	add r2, r2, r0
	ldrb r0, [r5, #0x10]
	ldrb r1, [r5, #0xf]
	orr r1, r0
	ldrb r0, [r6, #3]
	lsl r0, r0, #2
	orr r1, r0
	ldrb r0, [r6, #4]
	lsl r0, r0, #8
	orr r1, r0
	ldrb r0, [r6, #5]
	orr r0, r1
	mov r3, #0
	strh r0, [r2]
	ldr r1, =0x03002308
	ldrb r0, [r6]
	lsl r0, r0, #1
	add r0, r0, r1
	strh r3, [r0]
	ldr r1, =0x030022F8
	ldrb r0, [r6]
	lsl r0, r0, #1
	add r0, r0, r1
	strh r3, [r0]
	ldr r2, =0x03003CD4
	mov r0, #0x80
	lsl r0, r0, #1
	ldrb r6, [r6]
	lsl r0, r6
	ldrh r1, [r2]
	orr r0, r1
	strh r0, [r2]
	mov r0, #1
	b _0802A9EA
	.align 2, 0
.pool
_0802A9E8:
	mov r0, #0
_0802A9EA:
	add sp, #4
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
	

	thumb_func_global loadLevelCompletionBG
loadLevelCompletionBG: @ 0x0802A9F4
	push {r4, r5, r6, lr}
	sub sp, #4
	add r5, r0, #0
	ldr r1, =levelCompletionImages
	ldr r0, =0x03003D40
	ldrb r0, [r0, #1]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r6, [r0]
	cmp r6, #0
	beq _0802AAAC
	ldr r0, [r6, #8]
	ldrb r1, [r6, #0xe]
	ldrb r2, [r5, #2]
	lsl r2, r2, #5
	mov r3, #0xa0
	lsl r3, r3, #0x13
	add r2, r2, r3
	bl memcpy_pal
	ldr r0, [r6]
	ldrb r1, [r5, #3]
	lsl r1, r1, #0xe
	ldrb r2, [r5, #1]
	lsl r2, r2, #5
	mov r4, #0xc0
	lsl r4, r4, #0x13
	add r2, r2, r4
	add r1, r1, r2
	bl Bios_LZ77_16_2
	ldr r0, [r6, #4]
	ldrb r1, [r5, #4]
	lsl r1, r1, #0xb
	add r1, r1, r4
	ldrb r3, [r5, #1]
	mov r2, #2
	ldrsb r2, [r5, r2]
	str r2, [sp]
	mov r2, #0
	bl LZ77_mapCpy
	ldr r0, =0x03003D00
	ldrb r2, [r5]
	lsl r2, r2, #1
	add r2, r2, r0
	ldrb r0, [r6, #0x10]
	ldrb r1, [r6, #0xf]
	orr r1, r0
	ldrb r0, [r5, #3]
	lsl r0, r0, #2
	orr r1, r0
	ldrb r0, [r5, #4]
	lsl r0, r0, #8
	orr r1, r0
	ldrb r0, [r5, #5]
	orr r0, r1
	mov r3, #0
	strh r0, [r2]
	ldr r1, =0x03002308
	ldrb r0, [r5]
	lsl r0, r0, #1
	add r0, r0, r1
	strh r3, [r0]
	ldr r1, =0x030022F8
	ldrb r0, [r5]
	lsl r0, r0, #1
	add r0, r0, r1
	strh r3, [r0]
	ldr r2, =0x03003CD4
	mov r0, #0x80
	lsl r0, r0, #1
	ldrb r5, [r5]
	lsl r0, r5
	ldrh r1, [r2]
	orr r0, r1
	strh r0, [r2]
	mov r0, #1
	b _0802AAAE
	.align 2, 0
.pool
_0802AAAC:
	mov r0, #0
_0802AAAE:
	add sp, #4
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
	
	

thumb_func_global sub_0802AAB8
sub_0802AAB8: @ 0x0802AAB8
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #4
	lsl r0, r0, #0x18
	lsr r7, r0, #0x18
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	mov sb, r1
	lsl r2, r2, #0x18
	lsr r2, r2, #0x18
	mov sl, r2
	lsl r3, r3, #0x18
	lsr r3, r3, #0x18
	mov r8, r3
	ldr r0, =0x03003CD4
	ldrh r4, [r0]
	mov r0, #0x80
	lsl r0, r0, #1
	lsl r0, r1
	and r4, r0
	cmp r4, #0
	bne _0802AB64
	lsl r5, r7, #4
	ldr r6, =0x03001E48
	add r1, r5, r6
	add r0, r7, #0
	bl getBgInfo
	mov r0, sp
	strh r4, [r0]
	mov r0, sl
	lsl r4, r0, #0xb
	mov r1, #0xc0
	lsl r1, r1, #0x13
	add r1, r4, r1
	ldr r2, =0x01000400
	mov r0, sp
	bl Bios_memcpy
	mov r1, r8
	cmp r1, #0
	bne _0802AB3C
	add r0, r6, #4
	add r0, r5, r0
	ldr r1, [r0]
	mov r2, #0xa0
	lsl r2, r2, #2
	add r1, r1, r2
	ldr r0, =0x03001EA0
	str r1, [r0]
	ldr r2, =0x06000280
	b _0802AB4A
	.align 2, 0
.pool
_0802AB3C:
	add r0, r6, #4
	add r0, r5, r0
	ldr r1, [r0]
	add r1, #0x1e
	ldr r0, =0x03001EA0
	str r1, [r0]
	ldr r2, =0x0600001E
_0802AB4A:
	add r1, r4, r2
	str r1, [r0, #4]
	mov r1, r8
	strb r1, [r0, #8]
	mov r2, sb
	strb r2, [r0, #0xa]
	strb r7, [r0, #9]
	mov r1, sl
	strb r1, [r0, #0xb]
	ldr r0, =(sub_802AB80+1)
	mov r1, #0
	bl addCallback
_0802AB64:
	add sp, #4
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global sub_802AB80
sub_802AB80: @ 0x0802AB80
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #8
	ldr r3, =0x03001EA0
	ldrb r1, [r3, #8]
	lsl r0, r1, #2
	add r0, r0, r1
	lsl r0, r0, #0x11
	mov r2, #0xa0
	lsl r2, r2, #0xc
	add r0, r0, r2
	lsr r7, r0, #0x10
	mov r0, #0x1e
	cmp r1, #0
	beq _0802ABA6
	mov r0, #0xf
_0802ABA6:
	lsl r2, r0, #0x11
	ldr r0, [r3]
	mov sl, r0
	mov r5, #0
	mov r1, sp
	add r1, #2
	str r1, [sp, #4]
	cmp r5, r7
	bhs _0802ABF6
	add r4, r3, #0
	lsr r6, r2, #0x11
	mov r8, sp
	mov r2, #0x80
	lsl r2, r2, #0x11
	mov sb, r2
_0802ABC4:
	ldr r0, [r4]
	ldr r1, [r4, #4]
	add r2, r6, #0
	bl Bios_memcpy
	mov r0, #0
	mov r1, r8
	strh r0, [r1]
	ldr r1, [r4]
	mov r0, sp
	mov r2, sb
	orr r2, r6
	bl Bios_memcpy
	ldr r0, [r4]
	add r0, #0x40
	str r0, [r4]
	ldr r0, [r4, #4]
	add r0, #0x40
	str r0, [r4, #4]
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, r7
	blo _0802ABC4
_0802ABF6:
	mov r5, #0
	ldr r4, [sp, #4]
_0802ABFA:
	mov r0, #0
	strh r0, [r4]
	lsl r1, r5, #6
	add r1, sl
	mov r2, #0xa0
	lsl r2, r2, #2
	add r1, r1, r2
	add r0, r4, #0
	ldr r2, =0x01000020
	bl Bios_memcpy
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, #0xb
	bls _0802ABFA
	ldr r1, =0x03003D00
	ldr r3, =0x03001EA0
	ldrb r2, [r3, #0xa]
	lsl r2, r2, #1
	add r2, r2, r1
	ldrb r0, [r3, #9]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r4, [r0]
	mov r1, #0xc
	and r1, r4
	ldrb r0, [r3, #0xb]
	lsl r0, r0, #8
	orr r1, r0
	mov r0, #3
	and r0, r4
	orr r1, r0
	strh r1, [r2]
	ldr r2, =0x03003CD4
	mov r0, #0x80
	lsl r0, r0, #1
	ldrb r3, [r3, #0xa]
	lsl r0, r3
	ldrh r1, [r2]
	orr r0, r1
	strh r0, [r2]
	ldr r0, =(sub_802AB80+1)
	bl deleteCallback
	add sp, #8
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global getBgInfo
getBgInfo: @ 0x0802AC78
	push {r4, r5, lr}
	add r4, r1, #0
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	lsl r5, r0, #1
	ldr r2, =0x04000008
	add r1, r5, r2
	ldrh r2, [r1]
	strb r0, [r4]
	mov r1, #3
	add r0, r2, #0
	and r0, r1
	strb r0, [r4, #1]
	mov r0, #0xf8
	lsl r0, r0, #5
	and r0, r2
	lsl r0, r0, #3
	mov r1, #0xc0
	lsl r1, r1, #0x13
	add r0, r0, r1
	str r0, [r4, #4]
	lsr r2, r2, #0xe
	ldr r1, =screenSizeLUT
	lsl r2, r2, #2
	add r0, r2, r1
	ldrh r0, [r0]
	strh r0, [r4, #8]
	add r1, #2
	add r2, r2, r1
	ldrh r0, [r2]
	strh r0, [r4, #0xa]
	ldr r1, =0x04000010
	add r0, r5, r1
	ldrh r0, [r0]
	ldrh r1, [r4, #8]
	bl Bios_modulo
	strh r0, [r4, #0xc]
	ldr r2, =0x04000012
	add r5, r5, r2
	ldrh r0, [r5]
	ldrh r1, [r4, #0xa]
	bl Bios_modulo
	strh r0, [r4, #0xe]
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool



