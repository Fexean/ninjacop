.include "asm/macros.inc"
	
@complete

/*	void clearGfx_and_Wram()	*/
thumb_func_global clearGfx_and_Wram
clearGfx_and_Wram: @ 0x0800F0B0
	push {lr}
	bl clearRAM
	bl clearGraphics
	pop {r0}
	bx r0
	.align 2, 0

/*	void clearRAM()
	-clears EWRAM and first 0x7A00 bytes of IWRAM
*/
thumb_func_global clearRAM
clearRAM: @ 0x0800F0C0
	sub sp, #4
	mov r2, #0
	str r2, [sp]
	ldr r0, =REG_DMA3
	mov r1, sp
	str r1, [r0]
	mov r1, #0x80
	lsl r1, r1, #0x12
	str r1, [r0, #4]
	ldr r1, =0x85010000
	str r1, [r0, #8]
	ldr r1, [r0, #8]
	str r2, [sp]
	mov r1, sp
	str r1, [r0]
	mov r1, #0xc0
	lsl r1, r1, #0x12
	str r1, [r0, #4]
	ldr r1, =0x85001E80
	str r1, [r0, #8]
	ldr r0, [r0, #8]
	add sp, #4
	bx lr
	.align 2, 0
.pool


/*	void clearGraphics()	*/
thumb_func_global clearGraphics
clearGraphics: @ 0x0800F0FC
	push {lr}
	bl clearTiles
	bl clearOAM_HW
	bl clearPalettes
	pop {r0}
	bx r0
	.align 2, 0


/*	void clearTiles()
	-clears entire vram
*/
thumb_func_global clearTiles
clearTiles: @ 0x0800F110
	push {lr}
	sub sp, #4
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	mov r1, #0xc0
	lsl r1, r1, #0x13
	ldr r2, =0x0100C000
	mov r0, sp
	bl Bios_memcpy
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool


/*	void clearOAM_HW()
	- clears hardware OAM
*/
thumb_func_global clearOAM_HW
clearOAM_HW: @ 0x0800F130
	push {r4, lr}
	mov r0, #0xe0
	lsl r0, r0, #0x13
	mov r1, #0x80
	lsl r1, r1, #2
	add r2, r1, #0
	mov r1, #0
	mov r3, #0x80
	lsl r3, r3, #1
	add r4, r3, #0
	mov r3, #0x1f
_0800F146:
	strh r2, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r4, [r0]
	add r0, #2
	strh r2, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r2, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r2, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r4, [r0]
	add r0, #2
	sub r3, #1
	cmp r3, #0
	bge _0800F146
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0


/*	void ClearOAM(void* oam)
	-clears OAM structure starting at oam
*/
thumb_func_global ClearOAM
ClearOAM: @ 0x0800F194
	mov r1, #0x80
	lsl r1, r1, #2
	add r2, r1, #0
	mov r1, #0
	mov r3, #0x1f
_0800F19E:
	strh r2, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #4
	strh r2, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #4
	strh r2, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #4
	strh r2, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #4
	sub r3, #1
	cmp r3, #0
	bge _0800F19E
	bx lr
	.align 2, 0

thumb_func_global clearOAM_Unused
clearOAM_Unused: @ 0x0800F1D8
	push {r4, lr}
	mov r1, #0x80
	lsl r1, r1, #2
	add r2, r1, #0
	mov r1, #0
	mov r3, #0x80
	lsl r3, r3, #1
	add r4, r3, #0
	mov r3, #0x1f
_0800F1EA:
	strh r2, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r4, [r0]
	add r0, #2
	strh r2, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r2, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r2, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r1, [r0]
	add r0, #2
	strh r4, [r0]
	add r0, #2
	sub r3, #1
	cmp r3, #0
	bge _0800F1EA
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0

/*void clearPalettes()
	-fills palram with 0x00
*/
thumb_func_global clearPalettes
clearPalettes: @ 0x0800F238
	sub sp, #4
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r1, =0x040000D4
	mov r0, sp
	str r0, [r1]
	mov r0, #0xa0
	lsl r0, r0, #0x13
	str r0, [r1, #4]
	ldr r0, =0x81000200
	str r0, [r1, #8]
	ldr r0, [r1, #8]
	add sp, #4
	bx lr
	.align 2, 0
.pool
