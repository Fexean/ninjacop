.include "asm/macros.inc"





thumb_func_global unused_sub_801F254
unused_sub_801F254: @ 0x0801F254
	mov ip, r0
	mov r3, ip
	add r3, #0x44
	ldrb r1, [r3]
	lsl r0, r1, #0x1f
	cmp r0, #0
	beq _0801F27E
	mov r0, ip
	ldr r2, [r0, #0x3c]
	ldr r1, [r2, #0x18]
	cmp r1, #0
	bge _0801F26E
	neg r1, r1
_0801F26E:
	ldr r0, [r2, #0x28]
	cmp r1, r0
	blt _0801F29A
	mov r0, #0
	str r0, [r2, #0x30]
	ldrb r1, [r3]
	sub r0, #2
	b _0801F296
_0801F27E:
	lsl r0, r1, #0x1d
	cmp r0, #0
	bge _0801F29A
	mov r0, ip
	ldr r1, [r0, #0x3c]
	ldr r0, [r1, #0x18]
	cmp r0, #0
	bne _0801F29A
	str r0, [r1, #0x30]
	ldrb r1, [r3]
	mov r0, #5
	neg r0, r0
_0801F296:
	and r0, r1
	strb r0, [r3]
_0801F29A:
	mov r3, ip
	add r3, #0x44
	ldrb r1, [r3]
	lsl r0, r1, #0x1e
	cmp r0, #0
	bge _0801F2C2
	mov r1, ip
	ldr r2, [r1, #0x3c]
	ldr r1, [r2, #0x1c]
	cmp r1, #0
	bge _0801F2B2
	neg r1, r1
_0801F2B2:
	ldr r0, [r2, #0x2c]
	cmp r1, r0
	blt _0801F2DE
	mov r0, #0
	str r0, [r2, #0x34]
	ldrb r1, [r3]
	sub r0, #3
	b _0801F2DA
_0801F2C2:
	lsl r0, r1, #0x1c
	cmp r0, #0
	bge _0801F2DE
	mov r1, ip
	ldr r0, [r1, #0x3c]
	ldr r1, [r0, #0x1c]
	cmp r1, #0
	bne _0801F2DE
	str r1, [r0, #0x34]
	ldrb r1, [r3]
	mov r0, #9
	neg r0, r0
_0801F2DA:
	and r0, r1
	strb r0, [r3]
_0801F2DE:
	bx lr

thumb_func_global getPlayerPos
getPlayerPos: @ 0x0801F2E0
	ldr r1, _0801F2F0
	ldr r2, [r1]
	ldr r1, [r2, #4]
	str r1, [r0]
	ldr r1, [r2, #8]
	str r1, [r0, #4]
	bx lr
	.align 2, 0
_0801F2F0: .4byte 0x03004750

thumb_func_global getPlayerObj
getPlayerObj: @ 0x0801F2F4
	ldr r0, _0801F2FC
	ldr r0, [r0]
	bx lr
	.align 2, 0
_0801F2FC: .4byte 0x03004750

thumb_func_global sub_0801F300
sub_0801F300: @ 0x0801F300
	push {r4, r5, r6, lr}
	add r6, r0, #0
	ldr r0, _0801F334
	ldr r4, [r0, #4]
	add r0, #0x74
	cmp r4, r0
	beq _0801F340
_0801F30E:
	add r5, r4, #0
	add r5, #0x44
	ldrb r0, [r5]
	lsl r0, r0, #0x1b
	cmp r0, #0
	blt _0801F338
	ldr r1, [r4, #0x3c]
	add r0, r6, #0
	bl collide
	cmp r0, #0
	beq _0801F338
	ldrb r0, [r5]
	mov r1, #0x40
	orr r0, r1
	strb r0, [r5]
	add r0, r4, #0
	b _0801F342
	.align 2, 0
_0801F334: .4byte 0x03004EE0
_0801F338:
	ldr r4, [r4, #4]
	ldr r0, _0801F348
	cmp r4, r0
	bne _0801F30E
_0801F340:
	mov r0, #0
_0801F342:
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
_0801F348: .4byte 0x03004F54

thumb_func_global sub_0801F34C
sub_0801F34C: @ 0x0801F34C
	push {r4, r5, r6, lr}
	add r6, r0, #0
	ldr r0, _0801F384
	ldr r4, [r0, #4]
	add r0, #0x74
	cmp r4, r0
	beq _0801F390
_0801F35A:
	add r5, r4, #0
	add r5, #0x44
	ldrb r0, [r5]
	lsl r0, r0, #0x1b
	cmp r0, #0
	blt _0801F388
	ldr r1, [r4, #0x3c]
	add r0, r6, #0
	bl collide
	cmp r0, #0
	beq _0801F388
	ldrb r0, [r5]
	mov r1, #0x10
	orr r0, r1
	mov r1, #0x40
	orr r0, r1
	strb r0, [r5]
	add r0, r4, #0
	b _0801F392
	.align 2, 0
_0801F384: .4byte 0x03005A40
_0801F388:
	ldr r4, [r4, #4]
	ldr r0, _0801F398
	cmp r4, r0
	bne _0801F35A
_0801F390:
	mov r0, #0
_0801F392:
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
_0801F398: .4byte 0x03005AB4

thumb_func_global getCollidingPlayerAttackEnt
getCollidingPlayerAttackEnt: @ 0x0801F39C
	push {r4, r5, lr}
	add r5, r0, #0
	ldr r0, _0801F3F4
	ldr r4, [r0, #4]
	add r0, #0x74
	cmp r4, r0
	beq _0801F400
_0801F3AA:
	add r0, r4, #0
	add r0, #0x63
	add r1, r5, #0
	add r1, #0x64
	ldrb r0, [r0]
	ldrb r1, [r1]
	cmp r0, r1
	beq _0801F3F8
	add r0, r4, #0
	add r0, #0x44
	ldrb r0, [r0]
	lsl r0, r0, #0x1b
	cmp r0, #0
	blt _0801F3F8
	ldr r0, [r5, #0x3c]
	ldr r1, [r4, #0x3c]
	bl collide
	cmp r0, #0
	beq _0801F3F8
	ldr r2, [r4, #0x3c]
	add r0, r2, #0
	add r0, #0x52
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	bne _0801F3EE
	ldr r0, [r5, #0x3c]
	add r1, r2, #0
	bl sub_0801F40C
	cmp r0, #0
	bne _0801F3F8
_0801F3EE:
	str r5, [r4, #0x48]
	add r0, r4, #0
	b _0801F402
	.align 2, 0
_0801F3F4: .4byte 0x03006770
_0801F3F8:
	ldr r4, [r4, #4]
	ldr r0, _0801F408
	cmp r4, r0
	bne _0801F3AA
_0801F400:
	mov r0, #0
_0801F402:
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
_0801F408: .4byte 0x030067E4

thumb_func_global sub_0801F40C
sub_0801F40C: @ 0x0801F40C
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #4
	add r2, r0, #0
	add r5, r1, #0
	mov r1, #0x3c
	ldrsh r0, [r5, r1]
	cmp r0, #8
	bgt _0801F42C
	mov r1, #0x3e
	ldrsh r0, [r5, r1]
	cmp r0, #8
	ble _0801F4F6
_0801F42C:
	ldr r0, [r2, #4]
	asr r0, r0, #8
	ldrh r1, [r2, #0x38]
	add r0, r0, r1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	ldrh r0, [r2, #0x3c]
	add r0, r3, r0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov ip, r0
	ldr r0, [r2, #8]
	asr r0, r0, #8
	ldrh r1, [r2, #0x3a]
	add r0, r0, r1
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	ldrh r0, [r2, #0x3e]
	sub r0, r7, r0
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	mov r1, ip
	sub r0, r1, r3
	lsr r1, r0, #0x1f
	add r0, r0, r1
	asr r0, r0, #1
	add r0, r3, r0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	str r0, [sp]
	sub r0, r7, r2
	lsr r1, r0, #0x1f
	add r0, r0, r1
	asr r0, r0, #1
	add r0, r2, r0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sl, r0
	ldr r0, [r5, #4]
	lsl r0, r0, #8
	lsr r4, r0, #0x10
	ldr r0, [r5, #8]
	lsl r0, r0, #8
	lsr r1, r0, #0x10
	add r5, r4, #0
	add r6, r1, #0
	mov r0, #0
	mov sb, r0
	mov r8, r0
	cmp r4, r3
	bhs _0801F496
	sub r0, r3, r4
	b _0801F49E
_0801F496:
	cmp ip, r4
	bhs _0801F4A4
	mov r5, ip
	sub r0, r4, r5
_0801F49E:
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r8, r0
_0801F4A4:
	cmp r1, r2
	bhs _0801F4AE
	add r6, r2, #0
	sub r0, r6, r1
	b _0801F4B6
_0801F4AE:
	cmp r7, r1
	bhs _0801F4BC
	add r6, r1, #0
	sub r0, r6, r7
_0801F4B6:
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sb, r0
_0801F4BC:
	add r0, r5, #0
	mov r2, r8
	bl sub_08012A08
	cmp r0, #0
	beq _0801F4D6
	add r0, r5, #0
	mov r1, sl
	mov r2, r8
	bl sub_08012A08
	cmp r0, #0
	bne _0801F4F2
_0801F4D6:
	add r0, r4, #0
	add r1, r6, #0
	mov r2, sb
	bl sub_08012A84
	cmp r0, #0
	beq _0801F4F6
	ldr r0, [sp]
	add r1, r6, #0
	mov r2, sb
	bl sub_08012A84
	cmp r0, #0
	beq _0801F4F6
_0801F4F2:
	mov r0, #1
	b _0801F4F8
_0801F4F6:
	mov r0, #0
_0801F4F8:
	add sp, #4
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1



thumb_func_global sub_801F508
sub_801F508: @ 0x0801F508
	push {r4, lr}
	sub sp, #0xc
	mov r2, sp
	ldr r3, [r1, #0x3c]
	ldrh r1, [r3, #0x3c]
	strh r1, [r2, #8]
	ldrh r1, [r3, #0x3e]
	strh r1, [r2, #0xa]
	ldr r1, [r3, #4]
	lsl r1, r1, #8
	asr r1, r1, #0x10
	mov r4, #0x38
	ldrsh r2, [r3, r4]
	add r1, r1, r2
	str r1, [sp]
	ldr r1, [r3, #8]
	lsl r1, r1, #8
	asr r1, r1, #0x10
	mov r4, #0x3a
	ldrsh r2, [r3, r4]
	add r1, r1, r2
	mov r2, sp
	ldrh r2, [r2, #0xa]
	sub r1, r1, r2
	str r1, [sp, #4]
	mov r1, sp
	bl sub_8011540
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	add sp, #0xc
	pop {r4}
	pop {r1}
	bx r1
	thumb_func_global sub_801F54C
sub_801F54C: @ 0x0801F54C
	push {r4, lr}
	add r4, r0, #0
	add r0, #0x5c
	ldrh r0, [r0]
	cmp r0, #0
	beq _0801F55C
	cmp r0, #2
	bne _0801F580
_0801F55C:
	ldr r3, _0801F588
	ldr r2, _0801F58C
	ldr r0, [r4, #8]
	ldrh r1, [r0, #0x24]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r2, [r2, #0x11]
	add r0, r0, r2
	add r0, r0, r3
	ldrb r0, [r0]
	cmp r0, #0
	beq _0801F580
	add r0, r4, #0
	add r0, #0x44
	ldrb r1, [r0]
	mov r2, #0x10
	orr r1, r2
	strb r1, [r0]
_0801F580:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801F588: .4byte byte_81429F2
_0801F58C: .4byte 0x03003D40




thumb_func_global sub_0801F590
sub_0801F590: @ 0x0801F590
	push {r4, r5, r6, lr}
	add r5, r0, #0
	ldr r0, _0801F5B0
	ldr r4, [r0, #4]
	add r0, #0x74
	cmp r4, r0
	beq _0801F5BA
	add r6, r0, #0
_0801F5A0:
	ldr r1, [r4, #0x3c]
	add r0, r5, #0
	bl collide
	cmp r0, #0
	beq _0801F5B4
	add r0, r4, #0
	b _0801F5BC
	.align 2, 0
_0801F5B0: .4byte 0x030063D0
_0801F5B4:
	ldr r4, [r4, #4]
	cmp r4, r6
	bne _0801F5A0
_0801F5BA:
	mov r0, #0
_0801F5BC:
	pop {r4, r5, r6}
	pop {r1}
	bx r1
.align 2, 0

sub_801F5C4: @ 0x0801F5C4
	push {r4, r5, lr}
	add r5, r0, #0
	ldr r0, _0801F5F0
	ldr r4, [r0, #4]
	add r0, #0x74
	cmp r4, r0
	beq _0801F5FC
_0801F5D2:
	add r0, r4, #0
	add r0, #0x44
	ldrb r0, [r0]
	lsl r0, r0, #0x1b
	cmp r0, #0
	blt _0801F5F4
	ldr r1, [r4, #0x3c]
	add r0, r5, #0
	bl collide
	cmp r0, #0
	beq _0801F5F4
	add r0, r4, #0
	b _0801F5FE
	.align 2, 0
_0801F5F0: .4byte 0x03004880
_0801F5F4:
	ldr r4, [r4, #4]
	ldr r0, _0801F604
	cmp r4, r0
	bne _0801F5D2
_0801F5FC:
	mov r0, #0
_0801F5FE:
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
_0801F604: .4byte 0x030048F4
	thumb_func_global sub_801F608
sub_801F608: @ 0x0801F608
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	add r5, r0, #0
	ldr r0, [r5, #4]
	asr r0, r0, #8
	ldrh r1, [r5, #0x38]
	add r0, r0, r1
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	ldrh r0, [r5, #0x3c]
	add r0, r7, r0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r8, r0
	ldr r0, [r5, #8]
	lsl r0, r0, #8
	lsr r6, r0, #0x10
	ldr r0, [r5, #0x1c]
	asr r0, r0, #8
	add r0, r6, r0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov ip, r0
	ldr r0, _0801F678
	ldr r2, [r0, #4]
	add r0, #0x74
	cmp r2, r0
	beq _0801F684
_0801F642:
	ldr r1, [r2, #0x3c]
	ldr r0, [r1, #4]
	asr r0, r0, #8
	ldrh r3, [r1, #0x38]
	add r0, r0, r3
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	ldrh r0, [r1, #0x3c]
	add r0, r3, r0
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	ldr r0, [r1, #8]
	asr r0, r0, #8
	ldrh r1, [r1, #0x3a]
	add r0, r0, r1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp ip, r0
	bhi _0801F67C
	cmp r7, r3
	blo _0801F67C
	cmp r8, r4
	bhi _0801F67C
	sub r0, r0, r6
	lsl r0, r0, #8
	b _0801F686
	.align 2, 0
_0801F678: .4byte 0x03004880
_0801F67C:
	ldr r2, [r2, #4]
	ldr r0, _0801F690
	cmp r2, r0
	bne _0801F642
_0801F684:
	ldr r0, [r5, #0x1c]
_0801F686:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
_0801F690: .4byte 0x030048F4





thumb_func_global weirdObjCollidesPlayer
weirdObjCollidesPlayer: @ 0x0801F694
	push {r4, r5, lr}
	sub sp, #0x54
	add r4, r0, #0
	bl getPlayerObj
	add r5, r0, #0
	ldr r1, [r4, #0x3c]
	mov r0, sp
	mov r2, #0x54
	bl memcpy
	mov r0, sp
	mov r2, #0x3a
	ldrsh r1, [r0, r2]
	ldr r0, [sp, #8]
	add r0, r0, r1
	str r0, [sp, #8]
	mov r1, sp
	mov r0, #2
	strh r0, [r1, #0x3e]
	add r0, r5, #0
	bl collide
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	add sp, #0x54
	pop {r4, r5}
	pop {r1}
	bx r1
.align 2, 0




	thumb_func_global sub_801F6D0
sub_801F6D0: @ 0x0801F6D0
	push {r4, lr}
	ldr r0, _0801F720
	ldr r3, [r0, #4]
	add r0, #0x74
	cmp r3, r0
	beq _0801F71A
	add r4, r0, #0
_0801F6DE:
	ldr r0, [r3, #0x3c]
	ldrh r1, [r0, #0x16]
	ldrh r0, [r0, #0x14]
	cmp r0, #0xf0
	bhi _0801F714
	lsl r0, r1, #0x10
	asr r0, r0, #0x10
	cmp r0, #0
	blt _0801F714
	cmp r0, #0xa0
	bgt _0801F714
	add r2, r3, #0
	add r2, #0x44
	ldrb r0, [r2]
	mov r1, #0x80
	orr r0, r1
	strb r0, [r2]
	add r2, #1
	ldrb r0, [r2]
	mov r1, #1
	orr r0, r1
	strb r0, [r2]
	add r2, #0x22
	ldrb r0, [r2]
	mov r1, #0x20
	orr r0, r1
	strb r0, [r2]
_0801F714:
	ldr r3, [r3, #4]
	cmp r3, r4
	bne _0801F6DE
_0801F71A:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0801F720: .4byte 0x03004EE0






thumb_func_global collide
collide: @ 0x0801F724
	push {r4, r5, r6, r7, lr}
	ldr r2, [r0, #4]
	asr r2, r2, #8
	ldrh r3, [r0, #0x38]
	add r2, r2, r3
	ldrh r3, [r0, #0x3c]
	lsl r2, r2, #0x10
	asr r7, r2, #0x10
	add r3, r7, r3
	ldr r2, [r0, #8]
	asr r2, r2, #8
	ldrh r4, [r0, #0x3a]
	add r2, r2, r4
	ldrh r0, [r0, #0x3e]
	lsl r2, r2, #0x10
	asr r6, r2, #0x10
	sub r0, r6, r0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov ip, r0
	ldr r2, [r1, #4]
	asr r2, r2, #8
	ldrh r0, [r1, #0x38]
	add r2, r2, r0
	ldrh r0, [r1, #0x3c]
	lsl r2, r2, #0x10
	asr r2, r2, #0x10
	add r0, r2, r0
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldr r0, [r1, #8]
	asr r0, r0, #8
	ldrh r4, [r1, #0x3a]
	add r0, r0, r4
	ldrh r1, [r1, #0x3e]
	lsl r0, r0, #0x10
	asr r4, r0, #0x10
	sub r1, r4, r1
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	lsl r3, r3, #0x10
	asr r3, r3, #0x10
	cmp r3, r2
	ble _0801F79A
	lsl r0, r5, #0x10
	asr r0, r0, #0x10
	cmp r7, r0
	bge _0801F79A
	lsl r0, r1, #0x10
	asr r0, r0, #0x10
	cmp r6, r0
	ble _0801F79A
	mov r1, ip
	lsl r0, r1, #0x10
	asr r0, r0, #0x10
	cmp r0, r4
	bge _0801F79A
	mov r0, #1
	b _0801F79C
_0801F79A:
	mov r0, #0
_0801F79C:
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1



.align 2, 0
	thumb_func_global sub_801F7A4
sub_801F7A4: @ 0x0801F7A4
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	add r7, r1, #0
	mov r1, #0
	str r1, [r7, #0xc]
	str r1, [r7, #8]
	str r1, [r7, #4]
	str r1, [r7]
	ldr r1, [r0]
	mov ip, r1
	ldr r2, [r0, #4]
	mov r3, #8
	ldrsh r1, [r0, r3]
	lsl r1, r1, #8
	add r1, ip
	add r1, #0xff
	mov sl, r1
	mov r4, #0xa
	ldrsh r0, [r0, r4]
	lsl r0, r0, #8
	mov r1, #0xff
	add r1, r1, r2
	mov sb, r1
	sub r2, r2, r0
	mov r8, r2
	ldr r0, _0801F878
	ldr r4, [r0, #4]
	add r0, #0x74
	cmp r4, r0
	beq _0801F884
_0801F7E6:
	add r0, r4, #0
	add r0, #0x44
	ldrb r0, [r0]
	lsl r0, r0, #0x1b
	cmp r0, #0
	blt _0801F87C
	add r0, r4, #0
	add r0, #0x5a
	ldrh r0, [r0]
	cmp r0, #0x85
	beq _0801F87C
	cmp r0, #0x86
	beq _0801F87C
	ldr r3, [r4, #0x3c]
	mov r2, #0x38
	ldrsh r0, [r3, r2]
	lsl r0, r0, #8
	ldr r1, [r3, #4]
	add r6, r1, r0
	mov r1, #0x3a
	ldrsh r0, [r3, r1]
	lsl r0, r0, #8
	ldr r1, [r3, #8]
	add r1, r1, r0
	mov r2, #0x3c
	ldrsh r0, [r3, r2]
	sub r0, #1
	lsl r0, r0, #8
	add r0, r6, r0
	add r5, r0, #0
	add r5, #0xff
	mov r2, #0x3e
	ldrsh r0, [r3, r2]
	sub r0, #1
	lsl r0, r0, #8
	add r2, r1, #0
	add r2, #0xff
	sub r1, r1, r0
	cmp ip, r5
	bgt _0801F87C
	cmp sb, r1
	blt _0801F87C
	cmp sl, r6
	blt _0801F87C
	cmp r8, r2
	bgt _0801F87C
	cmp r8, r1
	blt _0801F84E
	mov r4, r8
	sub r0, r2, r4
	add r0, #1
	str r0, [r7, #4]
_0801F84E:
	cmp sb, r2
	bgt _0801F85A
	mov r2, sb
	sub r0, r2, r1
	mvn r0, r0
	str r0, [r7]
_0801F85A:
	cmp ip, r6
	blt _0801F866
	mov r4, ip
	sub r0, r5, r4
	add r0, #1
	str r0, [r7, #8]
_0801F866:
	cmp sl, r5
	bgt _0801F872
	mov r1, sl
	sub r0, r1, r6
	mvn r0, r0
	str r0, [r7, #0xc]
_0801F872:
	add r0, r3, #0
	b _0801F886
	.align 2, 0
_0801F878: .4byte 0x03004880
_0801F87C:
	ldr r4, [r4, #4]
	ldr r0, _0801F894
	cmp r4, r0
	bne _0801F7E6
_0801F884:
	mov r0, #0
_0801F886:
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
_0801F894: .4byte 0x030048F4





thumb_func_global sub_0801F898
sub_0801F898: @ 0x0801F898
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	add r2, r0, #0
	mov r1, #0x38
	ldrsh r0, [r2, r1]
	lsl r0, r0, #8
	ldr r1, [r2, #4]
	add r1, r1, r0
	mov sb, r1
	mov r7, #0x3a
	ldrsh r0, [r2, r7]
	add r0, #0xf
	lsl r0, r0, #8
	ldr r1, [r2, #8]
	add r6, r1, r0
	mov r1, #0x3c
	ldrsh r0, [r2, r1]
	sub r0, #1
	lsl r0, r0, #8
	add r0, sb
	mov r8, r0
	ldr r7, _0801F920
	add r7, r7, r6
	mov ip, r7
	ldr r0, _0801F924
	ldr r3, [r0, #4]
	add r0, #0x74
	cmp r3, r0
	beq _0801F930
_0801F8D6:
	add r0, r3, #0
	add r0, #0x44
	ldrb r0, [r0]
	lsl r0, r0, #0x1a
	cmp r0, #0
	blt _0801F928
	ldr r2, [r3, #0x3c]
	mov r1, #0x38
	ldrsh r0, [r2, r1]
	lsl r0, r0, #8
	ldr r1, [r2, #4]
	add r5, r1, r0
	mov r7, #0x3a
	ldrsh r0, [r2, r7]
	lsl r0, r0, #8
	ldr r1, [r2, #8]
	add r4, r1, r0
	mov r0, #0x3c
	ldrsh r1, [r2, r0]
	sub r1, #1
	lsl r1, r1, #8
	add r1, r5, r1
	mov r7, #0x3e
	ldrsh r0, [r2, r7]
	sub r0, #1
	lsl r0, r0, #8
	sub r0, r4, r0
	cmp sb, r1
	bgt _0801F928
	cmp r6, r0
	blt _0801F928
	cmp r8, r5
	blt _0801F928
	cmp ip, r4
	bgt _0801F928
	mov r0, #1
	b _0801F932
	.align 2, 0
_0801F920: .4byte 0xFFFFF100
_0801F924: .4byte 0x03004880
_0801F928:
	ldr r3, [r3, #4]
	ldr r0, _0801F940
	cmp r3, r0
	bne _0801F8D6
_0801F930:
	mov r0, #0
_0801F932:
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
_0801F940: .4byte 0x030048F4

thumb_func_global script_call
script_call: @ 0x0801F944
	push {r4, lr}
	add r3, r0, #0
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	ldr r2, [r3, #0x20]
	add r1, r2, r1
	ldr r4, [r1]
	mov r0, #0x62
	add r0, r0, r3
	mov ip, r0
	ldrb r0, [r0]
	cmp r0, #5
	bls _0801F966
	ldrb r0, [r2, #2]
	add r0, r2, r0
	str r0, [r3, #0x20]
	b _0801F984
_0801F966:
	ldrb r0, [r2, #2]
	add r0, r2, r0
	str r0, [r3, #0x20]
	mov r2, ip
	ldrb r1, [r2]
	add r0, r1, #1
	strb r0, [r2]
	lsl r1, r1, #0x18
	lsr r1, r1, #0x16
	add r0, r3, #0
	add r0, #0x24
	add r0, r0, r1
	ldr r1, [r3, #0x20]
	str r1, [r0]
	str r4, [r3, #0x20]
_0801F984:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global angle_sub_801F98C
angle_sub_801F98C: @ 0x0801F98C
	push {lr}
	asr r2, r2, #8
	asr r0, r0, #8
	sub r2, r2, r0
	asr r3, r3, #8
	asr r1, r1, #8
	sub r3, r3, r1
	lsl r2, r2, #0x10
	asr r2, r2, #0x10
	lsl r3, r3, #0x10
	asr r3, r3, #0x10
	add r0, r2, #0
	add r1, r3, #0
	bl Bios_arctan
	lsl r0, r0, #0x10
	lsr r0, r0, #8
	ldr r1, _0801F9BC
	bl divide
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	pop {r1}
	bx r1
	.align 2, 0
_0801F9BC: .4byte 0x0000FFFF

thumb_func_global sub_801F9C0
sub_801F9C0: @ 0x0801F9C0
	push {r4, r5, r6, lr}
	mov r6, r8
	push {r6}
	add r4, r0, #0
	mov r8, r1
	add r6, r3, #0
	ldr r0, [sp, #0x14]
	mov r5, #0x62
	neg r5, r5
	lsl r4, r4, #1
	sub r2, r2, r0
	add r0, r4, #0
	mul r0, r4, r0
	lsl r2, r2, #3
	add r1, r2, #0
	mul r1, r5, r1
	sub r0, r0, r1
	bl Bios_Sqrt
	neg r4, r4
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	sub r4, r4, r0
	lsl r5, r5, #1
	add r0, r4, #0
	add r1, r5, #0
	bl divide
	add r1, r0, #0
	mov r0, r8
	sub r6, r6, r0
	add r0, r6, #0
	bl divide
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
	
thumb_func_global sub_801FA10
sub_801FA10: @ 0x0801FA10
	push {r4, lr}
	add r4, r0, #0
	lsl r2, r2, #0x18
	lsr r0, r2, #0x18
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	ldrh r3, [r4]
	cmp r3, #0
	beq _0801FA28
	cmp r3, #1
	beq _0801FA36
	b _0801FA44
_0801FA28:
	sub r0, r1, r0
	lsl r0, r0, #0x18
	lsr r1, r0, #0x18
	cmp r1, #0xd0
	bne _0801FA44
	mov r0, #1
	b _0801FA42
_0801FA36:
	add r0, r1, r0
	lsl r0, r0, #0x18
	lsr r1, r0, #0x18
	cmp r1, #0x30
	bne _0801FA44
	mov r0, #0
_0801FA42:
	strh r0, [r4]
_0801FA44:
	add r0, r1, #0
	pop {r4}
	pop {r1}
	bx r1
	
	
	
	thumb_func_global sub_801FA4C
sub_801FA4C: @ 0x0801FA4C
	push {r4, lr}
	add r4, r0, #0
	lsl r2, r2, #0x18
	lsr r0, r2, #0x18
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	ldrh r3, [r4]
	cmp r3, #0
	beq _0801FA64
	cmp r3, #1
	beq _0801FA72
	b _0801FA80
_0801FA64:
	add r0, r1, r0
	lsl r0, r0, #0x18
	lsr r1, r0, #0x18
	cmp r1, #0xba
	bne _0801FA80
	mov r0, #1
	b _0801FA7E
_0801FA72:
	sub r0, r1, r0
	lsl r0, r0, #0x18
	lsr r1, r0, #0x18
	cmp r1, #0x50
	bne _0801FA80
	mov r0, #0
_0801FA7E:
	strh r0, [r4]
_0801FA80:
	add r0, r1, #0
	pop {r4}
	pop {r1}
	bx r1
	
	
	
	thumb_func_global sub_801FA88
sub_801FA88: @ 0x0801FA88
	push {r4, r5, r6, r7, lr}
	ldr r4, [sp, #0x14]
	ldr r5, [sp, #0x18]
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	lsl r5, r5, #0x18
	lsr r6, r5, #0x18
	add r7, r6, #0
	sub r2, r2, r0
	lsl r2, r2, #8
	sub r3, r3, r1
	lsl r3, r3, #8
	asr r2, r2, #0x10
	asr r3, r3, #0x10
	add r0, r2, #0
	add r1, r3, #0
	bl Bios_arctan
	lsl r0, r0, #0x10
	lsr r0, r0, #8
	ldr r1, _0801FAC8
	bl divide
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	mov r5, #0
	cmp r4, r1
	bls _0801FACC
	neg r0, r6
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	b _0801FAD2
	.align 2, 0
_0801FAC8: .4byte 0x0000FFFF
_0801FACC:
	cmp r4, r1
	bhs _0801FAD2
	add r5, r7, #0
_0801FAD2:
	sub r0, r4, r1
	cmp r0, #0
	bge _0801FADA
	neg r0, r0
_0801FADA:
	cmp r0, #0x80
	ble _0801FAE4
	lsl r0, r5, #0x10
	neg r0, r0
	lsr r5, r0, #0x10
_0801FAE4:
	add r0, r5, r4
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	
	
	

thumb_func_global getChargeToGive
getChargeToGive: @ 0x0801FAF0
	push {r4, lr}
	add r2, r0, #0
	ldr r0, [r2, #0x48]
	ldr r0, [r0, #0x44]
	lsl r0, r0, #5
	lsr r1, r0, #0x10
	mov r0, #0x80
	lsl r0, r0, #5
	and r0, r1
	cmp r0, #0
	beq _0801FB0A
	mov r0, #0
	b _0801FB30
_0801FB0A:
	mov r4, #0
	mov r0, #0x80
	lsl r0, r0, #4
	and r1, r0
	cmp r1, #0
	beq _0801FB18
	mov r4, #3
_0801FB18:
	ldr r3, _0801FB38
	ldr r0, _0801FB3C
	ldrb r1, [r0, #0x11]
	add r1, r4, r1
	ldr r0, [r2, #8]
	ldrh r2, [r0, #0x20]
	lsl r0, r2, #1
	add r0, r0, r2
	lsl r0, r0, #1
	add r1, r1, r0
	add r1, r1, r3
	ldrb r0, [r1]
_0801FB30:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
_0801FB38: .4byte ChargeRewardTable
_0801FB3C: .4byte 0x03003D40

thumb_func_global unused_sub_801FB40
unused_sub_801FB40: @ 0x0801FB40
	push {lr}
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r0, [r0, #0x48]
	add r0, #0x5a
	ldrh r0, [r0]
	cmp r0, #0x43
	bgt _0801FB5C
	cmp r0, #0x41
	blt _0801FB5C
	lsl r0, r1, #0x10
	asr r0, r0, #0x10
	bl ModifySpecialCharge
_0801FB5C:
	pop {r0}
	bx r0

thumb_func_global setObjAnimation
setObjAnimation: @ 0x0801FB60
	push {r4, r5, r6, lr}
	add r6, r0, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r2, _0801FBA4
	add r0, #0x5a
	ldrh r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r2, [r0]
	ldr r0, [r6, #0x3c]
	ldr r5, [r0]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #2
	add r4, r0, r2
	ldr r0, [r5, #4]
	ldr r1, [r4]
	cmp r0, r1
	beq _0801FBBA
	ldrh r2, [r5, #0xc]
	ldr r3, [r4, #4]
	add r0, r5, #0
	bl setSpriteAnimation
	ldr r2, [r6, #0x3c]
	ldrb r0, [r4, #8]
	cmp r0, #0
	bne _0801FBA8
	ldr r0, [r6, #8]
	ldr r1, [r0, #0xc]
	ldr r0, [r0, #8]
	b _0801FBB6
	.align 2, 0
_0801FBA4: .4byte entityAnimations
_0801FBA8:
	ldr r0, _0801FBD8
	ldrb r1, [r4, #8]
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r0, [r1]
	ldr r1, [r0, #4]
	ldr r0, [r0]
_0801FBB6:
	str r0, [r2, #0x38]
	str r1, [r2, #0x3c]
_0801FBBA:
	add r0, r6, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #1
	bne _0801FBDC
	add r2, r5, #0
	add r2, #0x24
	ldrb r1, [r2]
	mov r0, #3
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	b _0801FBE8
	.align 2, 0
_0801FBD8: .4byte objSizeTable
_0801FBDC:
	add r0, r5, #0
	add r0, #0x24
	ldrb r1, [r0]
	mov r2, #2
	orr r1, r2
	strb r1, [r0]
_0801FBE8:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global setObjDir_animFrame
setObjDir_animFrame: @ 0x0801FBF0
	push {lr}
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	mov r2, #0x67
	add r2, r2, r0
	mov ip, r2
	mov r2, #0xf
	and r1, r2
	mov r2, ip
	ldrb r3, [r2]
	mov r2, #0x10
	neg r2, r2
	and r2, r3
	orr r2, r1
	mov r1, ip
	strb r2, [r1]
	add r1, r0, #0
	add r1, #0x65
	ldrb r1, [r1]
	bl setObjAnimation
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global setObjAnimation_
setObjAnimation_: @ 0x0801FC20
	push {lr}
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	add r2, r0, #0
	add r2, #0x65
	strb r1, [r2]
	bl setObjAnimation
	pop {r0}
	bx r0

thumb_func_global sub_0801FC34
sub_0801FC34: @ 0x0801FC34
	push {r4, r5, r6, lr}
	add r5, r1, #0
	ldr r3, [r0, #0x3c]
	ldrh r2, [r3, #0x38]
	ldrh r0, [r3, #0x14]
	add r2, r2, r0
	ldrh r4, [r3, #0x16]
	ldrh r0, [r3, #0x3a]
	sub r4, r4, r0
	ldrh r1, [r3, #0x3c]
	lsl r2, r2, #0x10
	asr r6, r2, #0x10
	add r1, r6, r1
	ldrh r0, [r3, #0x3e]
	lsl r4, r4, #0x10
	asr r4, r4, #0x10
	add r0, r4, r0
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	neg r3, r5
	cmp r1, r3
	blt _0801FC80
	add r0, r5, #0
	add r0, #0xf0
	cmp r6, r0
	bgt _0801FC80
	lsl r0, r2, #0x10
	asr r0, r0, #0x10
	cmp r0, r3
	blt _0801FC80
	add r0, r5, #0
	add r0, #0xa0
	cmp r4, r0
	bgt _0801FC80
	mov r0, #1
	b _0801FC82
_0801FC80:
	mov r0, #0
_0801FC82:
	pop {r4, r5, r6}
	pop {r1}
	bx r1

thumb_func_global ObjPlayerDiff_X
ObjPlayerDiff_X: @ 0x0801FC88
	push {r4, lr}
	sub sp, #8
	add r4, r0, #0
	mov r0, sp
	bl getPlayerPos
	ldr r0, [sp]
	asr r0, r0, #8
	ldr r1, [r4, #0x3c]
	ldr r1, [r1, #4]
	asr r1, r1, #8
	sub r0, r0, r1
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	add sp, #8
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global ObjPlayerDiff_Y
ObjPlayerDiff_Y: @ 0x0801FCAC
	push {r4, lr}
	sub sp, #8
	add r4, r0, #0
	mov r0, sp
	bl getPlayerPos
	ldr r0, [sp, #4]
	asr r0, r0, #8
	ldr r1, [r4, #0x3c]
	ldr r1, [r1, #8]
	asr r1, r1, #8
	sub r0, r0, r1
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	add sp, #8
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global sub_0801FCD0
sub_0801FCD0: @ 0x0801FCD0
	push {r4, lr}
	add r4, r0, #0
	bl ObjPlayerDiff_X
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #0
	ble _0801FCE4
	mov r0, #1
	b _0801FCF6
_0801FCE4:
	cmp r0, #0
	blt _0801FCF4
	add r0, r4, #0
	add r0, #0x67
	ldrb r0, [r0]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1c
	b _0801FCF6
_0801FCF4:
	mov r0, #0
_0801FCF6:
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global objHorizontalDistToPlayer
objHorizontalDistToPlayer: @ 0x0801FCFC
	push {r4, lr}
	sub sp, #8
	add r4, r0, #0
	mov r0, sp
	bl getPlayerPos
	ldr r1, [r4, #0x3c]
	ldr r0, [sp]
	ldr r1, [r1, #4]
	sub r0, r0, r1
	lsl r0, r0, #8
	asr r0, r0, #0x10
	cmp r0, #0
	bge _0801FD1A
	neg r0, r0
_0801FD1A:
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	add sp, #8
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global objVerticalDistToPlayer
objVerticalDistToPlayer: @ 0x0801FD28
	push {r4, lr}
	sub sp, #8
	add r4, r0, #0
	mov r0, sp
	bl getPlayerPos
	ldr r1, [r4, #0x3c]
	ldr r0, [sp, #4]
	ldr r1, [r1, #8]
	sub r0, r0, r1
	lsl r0, r0, #8
	asr r0, r0, #0x10
	cmp r0, #0
	bge _0801FD46
	neg r0, r0
_0801FD46:
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	add sp, #8
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global sub_0801FD54
sub_0801FD54: @ 0x0801FD54
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	add r5, r0, #0
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	mov r8, r1
	bl objHorizontalDistToPlayer
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	asr r0, r0, #0x10
	ldr r4, _0801FD88
	cmp r0, r4
	bge _0801FD82
	add r0, r5, #0
	bl objVerticalDistToPlayer
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, r4
	blt _0801FD8C
_0801FD82:
	mov r0, #0
	b _0801FDFA
	.align 2, 0
_0801FD88: .4byte 0x000000A0
_0801FD8C:
	ldr r0, _0801FDA4
	ldr r1, [r0]
	ldr r2, [r5, #0x3c]
	ldr r3, [r1, #4]
	ldr r2, [r2, #4]
	add r1, r0, #0
	cmp r3, r2
	blt _0801FDA8
	lsl r0, r2, #8
	lsr r4, r0, #0x10
	sub r0, r3, r2
	b _0801FDAE
	.align 2, 0
_0801FDA4: .4byte 0x03004750
_0801FDA8:
	lsl r0, r3, #8
	lsr r4, r0, #0x10
	sub r0, r2, r3
_0801FDAE:
	lsl r0, r0, #8
	lsr r2, r0, #0x10
	lsl r0, r6, #0x10
	asr r0, r0, #0x10
	cmp r0, #0x10
	bgt _0801FDE2
	lsl r0, r7, #0x10
	asr r0, r0, #0x10
	cmp r0, #0x40
	bgt _0801FDE2
	add r0, r1, #0
	add r0, #0x21
	ldrb r0, [r0]
	cmp r0, #0
	beq _0801FDE2
	add r0, r4, #0
	ldr r1, [r1]
	ldr r1, [r1, #8]
	lsl r1, r1, #8
	lsr r1, r1, #0x10
	bl sub_08012A08
	cmp r0, #0
	bne _0801FDE2
	mov r0, #1
	b _0801FDFA
_0801FDE2:
	ldr r1, _0801FE04
	mov r2, r8
	lsl r0, r2, #2
	add r0, r0, r1
	ldr r1, _0801FE08
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r3, [r0]
	add r0, r5, #0
	mov r2, #0x30
	bl call_r3
_0801FDFA:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
_0801FE04: .4byte off_8142670
_0801FE08: .4byte 0x000000A0

	



thumb_func_global sub_801FE0C
sub_801FE0C: @ 0x0801FE0C
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #0x18
	add r5, r0, #0
	add r4, r2, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r8, r1
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	bl getPlayerObj
	ldr r2, [r5, #0x3c]
	ldr r1, [r2, #4]
	lsl r1, r1, #8
	lsr r6, r1, #0x10
	ldr r3, [r2, #8]
	lsl r3, r3, #8
	lsr r3, r3, #0x10
	add r4, r3, r4
	lsl r4, r4, #0x10
	lsr r7, r4, #0x10
	ldr r1, [r0, #4]
	lsl r1, r1, #8
	asr r1, r1, #0x10
	mov r4, #0x38
	ldrsh r2, [r0, r4]
	add r1, r1, r2
	str r1, [sp]
	ldr r1, [r0, #8]
	lsl r1, r1, #8
	asr r1, r1, #0x10
	str r1, [sp, #4]
	mov r2, sp
	ldrh r1, [r0, #0x3c]
	strh r1, [r2, #8]
	mov r1, sp
	ldrh r0, [r0, #0x3e]
	strh r0, [r1, #0xa]
	add r5, r3, #0
	cmp r5, r7
	bhs _0801FE98
	add r4, sp, #0xc
_0801FE64:
	add r1, r5, #0
	add r1, #8
	add r0, r6, #0
	mov r2, r8
	bl sub_8012AFC
	str r6, [sp, #0xc]
	str r5, [r4, #4]
	lsl r0, r0, #3
	strh r0, [r4, #8]
	mov r0, #0x10
	strh r0, [r4, #0xa]
	mov r0, sp
	add r1, r4, #0
	bl sub_8011540
	cmp r0, #0
	beq _0801FE8C
	mov r0, #1
	b _0801FE9A
_0801FE8C:
	add r0, r5, #0
	add r0, #0x10
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, r7
	blo _0801FE64
_0801FE98:
	mov r0, #0
_0801FE9A:
	add sp, #0x18
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
	thumb_func_global sub_801FEA8
sub_801FEA8: @ 0x0801FEA8
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #0x18
	add r5, r0, #0
	add r4, r2, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r8, r1
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	bl getPlayerObj
	ldr r2, [r5, #0x3c]
	ldr r1, [r2, #4]
	lsl r1, r1, #8
	lsr r5, r1, #0x10
	ldr r3, [r2, #8]
	lsl r3, r3, #8
	lsr r3, r3, #0x10
	add r4, r4, r3
	ldr r1, [r0, #4]
	lsl r1, r1, #8
	asr r1, r1, #0x10
	mov r6, #0x38
	ldrsh r2, [r0, r6]
	add r1, r1, r2
	str r1, [sp]
	ldr r1, [r0, #8]
	lsl r1, r1, #8
	asr r1, r1, #0x10
	str r1, [sp, #4]
	mov r2, sp
	ldrh r1, [r0, #0x3c]
	strh r1, [r2, #8]
	mov r1, sp
	ldrh r0, [r0, #0x3e]
	strh r0, [r1, #0xa]
	lsl r1, r3, #0x10
	lsl r4, r4, #0x10
	asr r2, r4, #0x10
	cmp r1, r4
	bge _0801FF40
	lsl r0, r5, #0x10
	asr r6, r0, #0x10
	add r4, sp, #0xc
	add r7, r2, #0
_0801FF06:
	asr r5, r1, #0x10
	add r1, r5, #0
	add r1, #8
	add r0, r6, #0
	mov r2, r8
	bl sub_8012B6C
	lsl r0, r0, #0x10
	asr r0, r0, #0xd
	sub r1, r6, r0
	str r1, [sp, #0xc]
	str r5, [r4, #4]
	strh r0, [r4, #8]
	mov r0, #0x10
	strh r0, [r4, #0xa]
	mov r0, sp
	add r1, r4, #0
	bl sub_8011540
	cmp r0, #0
	beq _0801FF34
	mov r0, #1
	b _0801FF42
_0801FF34:
	add r0, r5, #0
	add r0, #0x10
	lsl r1, r0, #0x10
	asr r0, r1, #0x10
	cmp r0, r7
	blt _0801FF06
_0801FF40:
	mov r0, #0
_0801FF42:
	add sp, #0x18
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
	thumb_func_global sub_801FF50
sub_801FF50: @ 0x0801FF50
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #0x18
	add r6, r0, #0
	add r4, r2, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r8, r1
	lsl r4, r4, #0x10
	lsr r5, r4, #0x10
	bl getPlayerObj
	ldr r2, [r6, #0x3c]
	ldr r3, [r2, #4]
	asr r3, r3, #8
	lsr r4, r4, #0x11
	sub r3, r3, r4
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	ldr r1, [r2, #8]
	asr r1, r1, #8
	ldrh r2, [r2, #0x3a]
	add r1, r1, r2
	lsl r1, r1, #0x10
	lsr r7, r1, #0x10
	add r5, r3, r5
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	ldr r1, [r0, #4]
	lsl r1, r1, #8
	asr r1, r1, #0x10
	mov r4, #0x38
	ldrsh r2, [r0, r4]
	add r1, r1, r2
	str r1, [sp]
	ldr r1, [r0, #8]
	lsl r1, r1, #8
	asr r1, r1, #0x10
	str r1, [sp, #4]
	mov r2, sp
	ldrh r1, [r0, #0x3c]
	strh r1, [r2, #8]
	mov r1, sp
	ldrh r0, [r0, #0x3e]
	strh r0, [r1, #0xa]
	add r4, r3, #0
	cmp r4, r5
	bhs _0801FFE6
	add r6, sp, #0xc
_0801FFB4:
	add r0, r4, #0
	add r1, r7, #0
	mov r2, r8
	bl sub_8012BE0
	str r4, [sp, #0xc]
	str r7, [r6, #4]
	mov r1, #8
	strh r1, [r6, #8]
	lsl r0, r0, #3
	strh r0, [r6, #0xa]
	mov r0, sp
	add r1, r6, #0
	bl sub_8011540
	cmp r0, #0
	beq _0801FFDA
	mov r0, #1
	b _0801FFE8
_0801FFDA:
	add r0, r4, #0
	add r0, #8
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, r5
	blo _0801FFB4
_0801FFE6:
	mov r0, #0
_0801FFE8:
	add sp, #0x18
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	thumb_func_global sub_801FFF4
sub_801FFF4: @ 0x0801FFF4
	push {lr}
	add r2, r0, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #1
	beq _08020010
	add r0, r2, #0
	mov r1, #0x38
	mov r2, #0x38
	bl sub_801FEA8
	b _0802001A
_08020010:
	add r0, r2, #0
	mov r1, #0x38
	mov r2, #0x38
	bl sub_801FE0C
_0802001A:
	pop {r1}
	bx r1
	.align 2, 0
	thumb_func_global sub_8020020
sub_8020020: @ 0x08020020
	push {lr}
	mov r1, #0x40
	mov r2, #0x20
	bl sub_801FF50
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	
	thumb_func_global sub_8020030
sub_8020030: @ 0x08020030
	push {r4, lr}
	add r4, r0, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #1
	bne _0802004E
	add r0, r4, #0
	mov r1, #0xa0
	mov r2, #0x30
	bl sub_801FE0C
	cmp r0, #0
	bne _0802006A
_0802004E:
	add r0, r4, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #0
	bne _0802006E
	add r0, r4, #0
	mov r1, #0xa0
	mov r2, #0x30
	bl sub_801FEA8
	cmp r0, #0
	beq _0802006E
_0802006A:
	mov r0, #1
	b _08020070
_0802006E:
	mov r0, #0
_08020070:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0





thumb_func_global setObjCodeBlink
setObjCodeBlink: @ 0x08020078
	add r2, r0, #0
	add r2, #0x68
	mov r1, #0
	strb r1, [r2]
	ldr r1, _08020088
	str r1, [r0, #0x10]
	bx lr
	.align 2, 0
_08020088: .4byte (blinkObj+1)

thumb_func_global blinkObj
blinkObj: @ 0x0802008C
	push {r4, lr}
	add r4, r0, #0
	add r1, r4, #0
	add r1, #0x68
	ldrb r0, [r1]
	add r0, #1
	strb r0, [r1]
	ldrb r0, [r1]
	cmp r0, #0x1d
	bls _080200B6
	ldr r0, [r4, #0x3c]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	mov r0, #0
	str r0, [r4, #0x10]
	b _080200E2
_080200B6:
	mov r1, #6
	bl modulo
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #2
	bls _080200D2
	ldr r0, [r4, #0x3c]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	b _080200E0
_080200D2:
	ldr r0, [r4, #0x3c]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
_080200E0:
	strb r0, [r1]
_080200E2:
	pop {r4}
	pop {r0}
	bx r0




thumb_func_global sub_80200E8
sub_80200E8: @ 0x080200E8
	push {r4, r5, r6, lr}
	add r4, r0, #0
	add r5, r4, #0
	add r5, #0x68
	ldrb r0, [r5]
	add r0, #1
	mov r6, #0
	strb r0, [r5]
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0x1d
	bls _08020120
	ldr r1, _08020128
	ldr r0, _0802012C
	str r0, [r1]
	ldr r0, _08020130
	ldr r0, [r0]
	str r0, [r1, #4]
	ldr r0, _08020134
	str r0, [r1, #8]
	ldr r0, [r1, #8]
	add r0, r4, #0
	add r0, #0x69
	ldrb r0, [r0]
	bl freePalFadeTaskAtIndex
	str r6, [r4, #0x10]
	strb r6, [r5]
_08020120:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_08020128: .4byte 0x040000D4
_0802012C: .4byte 0x0200A610
_08020130: .4byte 0x02020150
_08020134: .4byte 0x80000010




thumb_func_global sub_08020138
sub_08020138: @ 0x08020138
	push {r4, r5, r6, lr}
	add r5, r0, #0
	add r6, r1, #0
	ldr r0, [r5, #0x10]
	cmp r0, #0
	bne _08020186
	ldr r1, _08020194
	ldr r0, [r5, #0x3c]
	ldr r3, [r0]
	add r3, #0x22
	mov r2, #0
	ldrsb r2, [r3, r2]
	lsl r2, r2, #5
	ldr r0, [r6, #4]
	lsl r0, r0, #1
	ldr r4, _08020198
	add r0, r0, r4
	add r2, r2, r0
	str r2, [r1]
	ldr r1, _0802019C
	str r2, [r1]
	ldr r0, _080201A0
	str r0, [r1, #4]
	ldr r0, _080201A4
	str r0, [r1, #8]
	ldr r0, [r1, #8]
	mov r1, #0
	ldrsb r1, [r3, r1]
	lsl r1, r1, #5
	add r1, r1, r4
	add r0, r6, #0
	mov r2, #0
	bl startPalAnimation
	add r1, r5, #0
	add r1, #0x69
	strb r0, [r1]
	ldr r0, _080201A8
	str r0, [r5, #0x10]
_08020186:
	add r1, r5, #0
	add r1, #0x68
	mov r0, #0
	strb r0, [r1]
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_08020194: .4byte 0x02020150
_08020198: .4byte 0x05000200
_0802019C: .4byte 0x040000D4
_080201A0: .4byte 0x0200A610
_080201A4: .4byte 0x80000010
_080201A8: .4byte (sub_80200E8+1)

thumb_func_global unused_sub_80201AC
unused_sub_80201AC: @ 0x080201AC
	push {lr}
	add r2, r0, #0
	ldr r0, [r2, #8]
	ldrh r0, [r0, #0x1c]
	cmp r0, #0
	beq _080201DE
	add r0, r2, #0
	add r0, #0x62
	ldrb r0, [r0]
	lsl r0, r0, #1
	add r1, r2, #0
	add r1, #0x4c
	add r1, r1, r0
	ldrh r0, [r1]
	add r0, #1
	strh r0, [r1]
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	ldr r1, [r2, #8]
	ldrh r1, [r1, #0x1c]
	cmp r0, r1
	bne _080201DE
	add r0, r2, #0
	bl destroyObject
_080201DE:
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global sub_080201E4
sub_080201E4: @ 0x080201E4
	add r3, r0, #0
	ldr r0, _080201FC
	lsl r1, r1, #0x10
	lsl r0, r0, #0x10
	lsr r2, r1, #0x10
	cmp r1, r0
	bne _08020204
	ldr r0, _08020200
	ldr r0, [r0]
	ldr r0, [r0, #4]
	b _0802021C
	.align 2, 0
_080201FC: .4byte 0x00008001
_08020200: .4byte 0x03004750
_08020204:
	ldr r0, _08020214
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r2, r0
	beq _08020218
	asr r0, r1, #8
	b _0802021C
	.align 2, 0
_08020214: .4byte 0x00008002
_08020218:
	ldrh r0, [r3, #0x14]
	lsl r0, r0, #8
_0802021C:
	bx lr
	.align 2, 0

thumb_func_global sub_08020220
sub_08020220: @ 0x08020220
	add r3, r0, #0
	ldr r0, _08020238
	lsl r1, r1, #0x10
	lsl r0, r0, #0x10
	lsr r2, r1, #0x10
	cmp r1, r0
	bne _08020240
	ldr r0, _0802023C
	ldr r0, [r0]
	ldr r0, [r0, #8]
	b _08020258
	.align 2, 0
_08020238: .4byte 0x00008001
_0802023C: .4byte 0x03004750
_08020240:
	ldr r0, _08020250
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r2, r0
	beq _08020254
	asr r0, r1, #8
	b _08020258
	.align 2, 0
_08020250: .4byte 0x00008002
_08020254:
	ldrh r0, [r3, #0x16]
	lsl r0, r0, #8
_08020258:
	bx lr
	.align 2, 0

thumb_func_global sub_0802025C
sub_0802025C: @ 0x0802025C
	push {r4, lr}
	mov ip, r0
	mov r4, #1
	ldr r0, [r0, #0x48]
	ldr r0, [r0, #0x3c]
	mov r2, ip
	ldr r1, [r2, #0x3c]
	ldr r0, [r0, #4]
	ldr r1, [r1, #4]
	sub r0, r0, r1
	lsl r0, r0, #8
	lsr r1, r0, #0x10
	mov r0, ip
	add r0, #0x5a
	ldrh r0, [r0]
	cmp r0, #0x30
	bhi _080202A8
	lsl r0, r1, #0x10
	asr r2, r0, #0x10
	cmp r2, #0
	ble _08020294
	mov r0, ip
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #0
	beq _080202A6
_08020294:
	cmp r2, #0
	bge _080202A8
	mov r0, ip
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #1
	bne _080202A8
_080202A6:
	mov r4, #2
_080202A8:
	mov r1, ip
	ldr r0, [r1, #0x48]
	ldr r0, [r0, #0x44]
	lsl r0, r0, #5
	lsr r0, r0, #0x10
	mov r1, #0x80
	lsl r1, r1, #5
	and r0, r1
	cmp r0, #0
	beq _080202BE
	mov r4, #2
_080202BE:
	mov r2, ip
	ldr r0, [r2, #0x44]
	lsl r0, r0, #5
	lsr r0, r0, #0x10
	ldr r1, _080202F8
	asr r0, r1
	mov r1, #1
	and r0, r1
	cmp r0, #0
	beq _080202D4
	mov r4, #2
_080202D4:
	ldr r3, _080202FC
	ldr r2, _08020300
	mov r1, ip
	ldr r0, [r1, #8]
	ldrh r1, [r0, #0x22]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r2, [r2, #0x11]
	add r0, r0, r2
	lsl r0, r0, #1
	add r0, r0, r3
	ldrh r0, [r0]
	mul r0, r4, r0
	bl sub_8035044
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_080202F8: .4byte 0x0000000F
_080202FC: .4byte unk_8142986
_08020300: .4byte 0x03003D40

thumb_func_global sub_8020304
sub_8020304: @ 0x08020304
	push {r4, r5, r6, r7, lr}
	add r7, r0, #0
	bl getPlayerObj
	ldr r1, [r7, #0x3c]
	ldr r4, [r1, #4]
	ldr r1, [r1, #8]
	ldr r2, [r0, #4]
	ldr r3, [r0, #8]
	add r0, r4, #0
	bl angle_sub_801F98C
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	add r1, r7, #0
	add r1, #0x6e
	strh r0, [r1]
	lsl r1, r0, #2
	add r1, r1, r0
	lsl r1, r1, #1
	add r0, r1, #0
	mov r1, #0x15
	bl divide
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	add r0, r4, #0
	mov r1, #0xa
	bl sub_08037FA8
	add r6, r0, #0
	add r5, r7, #0
	add r5, #0x70
	strh r6, [r5]
	add r0, r4, #0
	mov r1, #0xa
	bl modulo
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #4
	bls _0802035E
	add r0, r6, #1
	strh r0, [r5]
_0802035E:
	ldrh r1, [r5]
	mov r2, #0
	ldrsh r0, [r5, r2]
	cmp r0, #0xb
	ble _0802036E
	add r0, r1, #0
	sub r0, #0xc
	strh r0, [r5]
_0802036E:
	ldr r0, _08020388
	mov r2, #0
	ldrsh r1, [r5, r2]
	lsl r1, r1, #1
	add r1, r1, r0
	ldrh r1, [r1]
	add r0, r7, #0
	bl setObjAnimation_
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_08020388: .4byte a3771519A9_262_

thumb_func_global sub_802038C
sub_802038C: @ 0x0802038C
	push {lr}
	ldr r2, _080203A8
	add r1, r0, #0
	add r1, #0x70
	mov r3, #0
	ldrsh r1, [r1, r3]
	lsl r1, r1, #1
	add r1, r1, r2
	ldrh r1, [r1]
	bl setObjAnimation_
	pop {r0}
	bx r0
	.align 2, 0
_080203A8: .4byte unk_8142694



thumb_func_global sub_80203AC
sub_80203AC: @ 0x080203AC
	push {lr}
	ldr r2, _080203C8
	add r1, r0, #0
	add r1, #0x70
	mov r3, #0
	ldrsh r1, [r1, r3]
	lsl r1, r1, #1
	add r1, r1, r2
	ldrh r1, [r1]
	bl setObjAnimation_
	pop {r0}
	bx r0
	.align 2, 0
_080203C8: .4byte unk_81426ac



	thumb_func_global sub_80203CC
sub_80203CC: @ 0x080203CC
	push {r4, r5, r6, lr}
	sub sp, #8
	add r5, r0, #0
	ldr r3, _08020440
	add r0, #0x6e
	mov r1, #0
	ldrsh r2, [r0, r1]
	add r0, r2, #0
	add r0, #0x40
	lsl r0, r0, #1
	add r0, r0, r3
	mov r4, #0
	ldrsh r1, [r0, r4]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #7
	cmp r0, #0
	bge _080203F2
	add r0, #0xff
_080203F2:
	lsl r0, r0, #8
	lsr r6, r0, #0x10
	lsl r0, r2, #1
	add r0, r0, r3
	mov r2, #0
	ldrsh r1, [r0, r2]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #7
	cmp r0, #0
	bge _0802040A
	add r0, #0xff
_0802040A:
	lsl r4, r0, #8
	ldr r0, _08020444
	add r1, r5, #0
	add r1, #0x70
	mov r2, #0
	ldrsh r1, [r1, r2]
	add r0, r1, r0
	mov r2, #0
	ldrsb r2, [r0, r2]
	ldr r0, _08020448
	add r1, r1, r0
	mov r3, #0
	ldrsb r3, [r1, r3]
	lsl r0, r6, #0x10
	asr r0, r0, #0x10
	str r0, [sp]
	asr r4, r4, #0x10
	str r4, [sp, #4]
	add r0, r5, #0
	mov r1, #0x4f
	bl createChildEntity
	add sp, #8
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_08020440: .4byte dword_80382C8
_08020444: .4byte dword_81426C4
_08020448: .4byte dword_81426D0
	thumb_func_global sub_802044C
sub_802044C: @ 0x0802044C
	push {r4, r5, r6, r7, lr}
	mov ip, r0
	mov r6, ip
	add r6, #0x62
	ldrb r0, [r6]
	lsl r0, r0, #1
	mov r5, ip
	add r5, #0x4c
	add r0, r5, r0
	ldrh r1, [r0]
	add r1, #1
	mov r7, #0
	strh r1, [r0]
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	ldr r4, _08020498
	mov r2, ip
	add r2, #0x6a
	ldrh r3, [r2]
	add r0, r3, r4
	ldrb r0, [r0]
	cmp r1, r0
	bne _08020500
	add r0, r3, #1
	strh r0, [r2]
	ldrh r3, [r2]
	add r0, r3, r4
	ldrb r0, [r0]
	cmp r0, #0xff
	bne _0802049C
	ldrb r0, [r6]
	lsl r0, r0, #1
	add r0, r5, r0
	strh r7, [r0]
	strh r7, [r2]
	mov r0, #1
	b _08020502
	.align 2, 0
_08020498: .4byte off_81426EC
_0802049C:
	mov r0, ip
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #1
	bne _080204C4
	mov r0, ip
	ldr r2, [r0, #0x3c]
	ldr r0, _080204C0
	add r0, r3, r0
	mov r1, #0
	ldrsb r1, [r0, r1]
	lsl r1, r1, #8
	ldr r0, [r2, #4]
	add r0, r0, r1
	b _080204D6
	.align 2, 0
_080204C0: .4byte dword_81426DC
_080204C4:
	mov r0, ip
	ldr r2, [r0, #0x3c]
	ldr r0, _08020508
	add r0, r3, r0
	mov r1, #0
	ldrsb r1, [r0, r1]
	lsl r1, r1, #8
	ldr r0, [r2, #4]
	sub r0, r0, r1
_080204D6:
	str r0, [r2, #4]
	ldr r1, _0802050C
	mov r0, ip
	add r0, #0x6a
	ldrh r0, [r0]
	add r0, r0, r1
	mov r1, #0
	ldrsb r1, [r0, r1]
	lsl r1, r1, #8
	ldr r0, [r2, #8]
	add r0, r0, r1
	str r0, [r2, #8]
	mov r0, ip
	add r0, #0x62
	ldrb r0, [r0]
	lsl r0, r0, #1
	mov r1, ip
	add r1, #0x4c
	add r1, r1, r0
	mov r0, #0
	strh r0, [r1]
_08020500:
	mov r0, #0
_08020502:
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
_08020508: .4byte dword_81426DC
_0802050C: .4byte dword_81426E4




thumb_func_global sub_8020510
sub_8020510: @ 0x08020510
	push {r4, r5, r6, r7, lr}
	mov ip, r0
	mov r6, ip
	add r6, #0x62
	ldrb r0, [r6]
	lsl r0, r0, #1
	mov r5, ip
	add r5, #0x4c
	add r0, r5, r0
	ldrh r1, [r0]
	add r1, #1
	mov r7, #0
	strh r1, [r0]
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	ldr r4, _0802055C
	mov r2, ip
	add r2, #0x6a
	ldrh r3, [r2]
	add r0, r3, r4
	ldrb r0, [r0]
	cmp r1, r0
	bne _080205C4
	add r0, r3, #1
	strh r0, [r2]
	ldrh r3, [r2]
	add r0, r3, r4
	ldrb r0, [r0]
	cmp r0, #0xff
	bne _08020560
	ldrb r0, [r6]
	lsl r0, r0, #1
	add r0, r5, r0
	strh r7, [r0]
	strh r7, [r2]
	mov r0, #1
	b _080205C6
	.align 2, 0
_0802055C: .4byte unk_8142703
_08020560:
	mov r0, ip
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #1
	bne _08020588
	mov r0, ip
	ldr r2, [r0, #0x3c]
	ldr r0, _08020584
	add r0, r3, r0
	mov r1, #0
	ldrsb r1, [r0, r1]
	lsl r1, r1, #8
	ldr r0, [r2, #4]
	add r0, r0, r1
	b _0802059A
	.align 2, 0
_08020584: .4byte unk_81426f5
_08020588:
	mov r0, ip
	ldr r2, [r0, #0x3c]
	ldr r0, _080205CC
	add r0, r3, r0
	mov r1, #0
	ldrsb r1, [r0, r1]
	lsl r1, r1, #8
	ldr r0, [r2, #4]
	sub r0, r0, r1
_0802059A:
	str r0, [r2, #4]
	ldr r1, _080205D0
	mov r0, ip
	add r0, #0x6a
	ldrh r0, [r0]
	add r0, r0, r1
	mov r1, #0
	ldrsb r1, [r0, r1]
	lsl r1, r1, #8
	ldr r0, [r2, #8]
	add r0, r0, r1
	str r0, [r2, #8]
	mov r0, ip
	add r0, #0x62
	ldrb r0, [r0]
	lsl r0, r0, #1
	mov r1, ip
	add r1, #0x4c
	add r1, r1, r0
	mov r0, #0
	strh r0, [r1]
_080205C4:
	mov r0, #0
_080205C6:
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
_080205CC: .4byte unk_81426f5
_080205D0: .4byte unk_81426FC




thumb_func_global sub_80205D4
sub_80205D4: @ 0x080205D4
	push {r4, r5, lr}
	add r2, r0, #0
	ldr r4, [r2, #0x3c]
	ldr r0, [r4]
	mov r1, #0
	str r1, [r0, #4]
	add r0, r2, #0
	add r0, #0x67
	ldrb r0, [r0]
	mov r3, #0xf
	and r3, r0
	cmp r3, #1
	bne _080205FA
	ldr r0, [r2, #0x48]
	ldr r0, [r0, #0x3c]
	ldr r1, [r4, #4]
	ldr r0, [r0, #4]
	cmp r1, r0
	bgt _0802060A
_080205FA:
	ldr r5, [r2, #0x48]
	cmp r3, #0
	bne _08020614
	ldr r0, [r5, #0x3c]
	ldr r1, [r4, #4]
	ldr r0, [r0, #4]
	cmp r1, r0
	bge _08020614
_0802060A:
	add r0, r2, #0
	mov r1, #9
	bl setObjAnimation_
	b _0802063E
_08020614:
	ldr r1, [r5, #0x3c]
	ldr r0, [r2, #0x3c]
	ldr r0, [r0, #8]
	mov r3, #0x80
	lsl r3, r3, #5
	add r0, r0, r3
	ldr r1, [r1, #8]
	cmp r1, r0
	ble _08020630
	add r0, r2, #0
	mov r1, #7
	bl setObjAnimation_
	b _0802063E
_08020630:
	add r0, r2, #0
	mov r1, #8
	bl setObjAnimation_
	ldr r0, _08020644
	bl playSong
_0802063E:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_08020644: .4byte 0x00000129
	thumb_func_global sub_8020648
sub_8020648: @ 0x08020648
	push {r4, r5, lr}
	add r2, r0, #0
	ldr r4, [r2, #0x3c]
	ldr r0, [r4]
	mov r1, #0
	str r1, [r0, #4]
	add r0, r2, #0
	add r0, #0x67
	ldrb r0, [r0]
	mov r3, #0xf
	and r3, r0
	cmp r3, #1
	bne _0802066E
	ldr r0, [r2, #0x48]
	ldr r0, [r0, #0x3c]
	ldr r1, [r4, #4]
	ldr r0, [r0, #4]
	cmp r1, r0
	bgt _0802067E
_0802066E:
	ldr r5, [r2, #0x48]
	cmp r3, #0
	bne _08020688
	ldr r0, [r5, #0x3c]
	ldr r1, [r4, #4]
	ldr r0, [r0, #4]
	cmp r1, r0
	bge _08020688
_0802067E:
	add r0, r2, #0
	mov r1, #0xc
	bl setObjAnimation_
	b _080206AC
_08020688:
	ldr r1, [r5, #0x3c]
	ldr r0, [r2, #0x3c]
	ldr r0, [r0, #8]
	mov r3, #0x80
	lsl r3, r3, #5
	add r0, r0, r3
	ldr r1, [r1, #8]
	cmp r1, r0
	ble _080206A4
	add r0, r2, #0
	mov r1, #0xa
	bl setObjAnimation_
	b _080206AC
_080206A4:
	add r0, r2, #0
	mov r1, #0xb
	bl setObjAnimation_
_080206AC:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
	thumb_func_global sub_80206B4
sub_80206B4: @ 0x080206B4
	push {r4, r5, r6, r7, lr}
	add r3, r0, #0
	mov r7, #0
	ldr r6, [r3, #0x3c]
	ldr r0, [r6]
	str r7, [r0, #4]
	add r0, r3, #0
	add r0, #0x67
	ldrb r0, [r0]
	mov r5, #0xf
	and r5, r0
	cmp r5, #1
	bne _080206DC
	ldr r1, [r3, #0x48]
	ldr r0, [r1, #0x3c]
	ldr r2, [r6, #4]
	ldr r0, [r0, #4]
	add r4, r1, #0
	cmp r2, r0
	bgt _080206EC
_080206DC:
	ldr r4, [r3, #0x48]
	cmp r5, #0
	bne _080206EE
	ldr r0, [r4, #0x3c]
	ldr r1, [r6, #4]
	ldr r0, [r0, #4]
	cmp r1, r0
	bge _080206EE
_080206EC:
	mov r7, #1
_080206EE:
	add r0, r4, #0
	add r0, #0x5a
	ldrh r0, [r0]
	cmp r0, #0x3e
	bne _0802072C
	cmp r7, #0
	bne _08020722
	ldr r1, [r4, #0x3c]
	ldr r0, [r3, #0x3c]
	ldr r0, [r0, #8]
	mov r2, #0x80
	lsl r2, r2, #5
	add r0, r0, r2
	ldr r1, [r1, #8]
	cmp r1, r0
	ble _08020718
	add r0, r3, #0
	mov r1, #7
	bl setObjAnimation_
	b _08020742
_08020718:
	add r0, r3, #0
	mov r1, #8
	bl setObjAnimation_
	b _08020742
_08020722:
	add r0, r3, #0
	mov r1, #9
	bl setObjAnimation_
	b _08020742
_0802072C:
	cmp r7, #0
	bne _0802073A
	add r0, r3, #0
	mov r1, #0xa
	bl setObjAnimation_
	b _08020742
_0802073A:
	add r0, r3, #0
	mov r1, #0xb
	bl setObjAnimation_
_08020742:
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	thumb_func_global sub_8020748
sub_8020748: @ 0x08020748
	push {r4, r5, lr}
	add r3, r0, #0
	ldr r5, [r3, #0x48]
	ldr r0, [r5, #0x44]
	lsl r0, r0, #5
	lsr r1, r0, #0x10
	mov r0, #0x80
	lsl r0, r0, #5
	and r0, r1
	cmp r0, #0
	beq _08020768
	add r0, r3, #0
	mov r1, #0x10
	bl setObjAnimation_
	b _080207B8
_08020768:
	mov r0, #0x80
	lsl r0, r0, #4
	and r1, r0
	cmp r1, #0
	beq _0802077C
	add r0, r3, #0
	mov r1, #0xf
	bl setObjAnimation_
	b _080207B8
_0802077C:
	add r0, r3, #0
	add r0, #0x67
	ldrb r0, [r0]
	mov r4, #0xf
	and r4, r0
	cmp r4, #1
	bne _08020796
	ldr r0, [r3, #0x3c]
	ldr r1, [r5, #0x3c]
	ldr r2, [r0, #4]
	ldr r0, [r1, #4]
	cmp r2, r0
	bgt _080207A6
_08020796:
	cmp r4, #0
	bne _080207B0
	ldr r0, [r3, #0x3c]
	ldr r1, [r5, #0x3c]
	ldr r2, [r0, #4]
	ldr r0, [r1, #4]
	cmp r2, r0
	bge _080207B0
_080207A6:
	add r0, r3, #0
	mov r1, #0xd
	bl setObjAnimation_
	b _080207B8
_080207B0:
	add r0, r3, #0
	mov r1, #0xe
	bl setObjAnimation_
_080207B8:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
	thumb_func_global sub_80207C0
sub_80207C0: @ 0x080207C0
	push {r4, lr}
	add r2, r0, #0
	mov r4, #0
	add r0, #0x67
	ldrb r0, [r0]
	mov r3, #0xf
	and r3, r0
	cmp r3, #1
	bne _080207E0
	ldr r1, [r2, #0x3c]
	ldr r0, [r2, #0x48]
	ldr r0, [r0, #0x3c]
	ldr r1, [r1, #4]
	ldr r0, [r0, #4]
	cmp r1, r0
	bgt _080207F2
_080207E0:
	cmp r3, #0
	bne _080207F4
	ldr r1, [r2, #0x3c]
	ldr r0, [r2, #0x48]
	ldr r0, [r0, #0x3c]
	ldr r1, [r1, #4]
	ldr r0, [r0, #4]
	cmp r1, r0
	bge _080207F4
_080207F2:
	mov r4, #1
_080207F4:
	cmp r4, #0
	bne _08020802
	add r0, r2, #0
	mov r1, #0xe
	bl setObjAnimation_
	b _0802080A
_08020802:
	add r0, r2, #0
	mov r1, #0xd
	bl setObjAnimation_
_0802080A:
	pop {r4}
	pop {r0}
	bx r0
	thumb_func_global sub_8020810
sub_8020810: @ 0x08020810
	push {r4, lr}
	add r4, r0, #0
	bl getPlayerObj
	ldr r1, [r0, #4]
	asr r1, r1, #8
	add r2, r4, #0
	add r2, #0x6e
	strh r1, [r2]
	ldr r0, [r0, #8]
	asr r0, r0, #8
	add r4, #0x70
	strh r0, [r4]
	pop {r4}
	pop {r0}
	bx r0







thumb_func_global sub_08020830
sub_08020830: @ 0x08020830
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #8
	add r7, r0, #0
	lsl r1, r1, #0x10
	lsl r2, r2, #0x10
	lsr r4, r2, #0x10
	ldr r2, [r7, #0x3c]
	ldr r0, _080208A8
	mov ip, r0
	add r0, r7, #0
	add r0, #0x67
	ldrb r0, [r0]
	lsl r6, r0, #0x1c
	lsr r0, r6, #0x1c
	add r0, ip
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	lsr r3, r1, #0x10
	mov r8, r3
	asr r1, r1, #8
	mul r1, r0, r1
	ldr r0, [r2, #4]
	add r5, r0, r1
	add r0, r7, #0
	add r0, #0x6e
	mov r3, #0
	ldrsh r1, [r0, r3]
	lsl r1, r1, #8
	lsr r0, r6, #0x1c
	add r0, ip
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	lsl r0, r0, #0xb
	sub r3, r1, r0
	sub r1, r3, r5
	cmp r5, r3
	ble _08020884
	sub r1, r5, r3
_08020884:
	mov r0, #0xb0
	lsl r0, r0, #7
	cmp r1, r0
	bgt _080208AC
	mov r0, #0xc0
	lsl r0, r0, #3
	lsl r4, r4, #0x10
	asr r1, r4, #8
	ldr r2, [r2, #8]
	add r2, r2, r1
	add r1, r7, #0
	add r1, #0x70
	mov r6, #0
	ldrsh r1, [r1, r6]
	lsl r1, r1, #8
	str r1, [sp]
	add r1, r5, #0
	b _080208DA
	.align 2, 0
_080208A8: .4byte dirLutMaybe
_080208AC:
	mov r0, #0xc0
	lsl r0, r0, #3
	lsl r4, r4, #0x10
	asr r1, r4, #8
	ldr r2, [r2, #8]
	add r2, r2, r1
	lsr r1, r6, #0x1c
	add r1, ip
	ldrb r1, [r1]
	lsl r1, r1, #0x18
	asr r1, r1, #0x18
	lsl r3, r1, #1
	add r3, r3, r1
	lsl r3, r3, #2
	sub r3, r3, r1
	lsl r3, r3, #0xb
	add r1, r7, #0
	add r1, #0x70
	mov r5, #0
	ldrsh r1, [r1, r5]
	lsl r1, r1, #8
	str r1, [sp]
	mov r1, #0
_080208DA:
	bl sub_801F9C0
	add r5, r0, #0
	add r1, r7, #0
	add r1, #0x67
	ldrb r0, [r1]
	mov r2, #0xf
	and r2, r0
	add r0, r1, #0
	cmp r2, #1
	bne _080208F4
	cmp r5, #0
	blt _080208FC
_080208F4:
	cmp r2, #0
	bne _080208FE
	cmp r5, #0
	ble _080208FE
_080208FC:
	mov r5, #0
_080208FE:
	ldr r1, _08020938
	ldrb r0, [r0]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1c
	add r0, r0, r1
	mov r1, #0
	ldrsb r1, [r0, r1]
	mov r6, r8
	lsl r0, r6, #0x10
	asr r0, r0, #0x10
	add r2, r0, #0
	mul r2, r1, r2
	lsl r2, r2, #0x10
	asr r2, r2, #0x10
	asr r3, r4, #0x10
	str r5, [sp]
	mov r0, #0xc0
	lsl r0, r0, #3
	str r0, [sp, #4]
	add r0, r7, #0
	mov r1, #0x4a
	bl createChildEntity
	add sp, #8
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_08020938: .4byte dirLutMaybe
	
	
	
	
	
	
	
	
	
	
	
	
	
	thumb_func_global negateEntityXVelocity
negateEntityXVelocity: @ 0x0802093C
	push {lr}
	ldr r0, [r0, #0x3c]
	ldr r1, [r0, #0x18]
	neg r1, r1
	bl setObjVelocity_X
	pop {r0}
	bx r0
	thumb_func_global sub_802094C
sub_802094C: @ 0x0802094C
	push {r4, lr}
	sub sp, #8
	add r4, r0, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	ldr r2, _08020980
	cmp r0, #1
	bne _08020962
	mov r2, #0xc
_08020962:
	lsl r2, r2, #0x10
	asr r2, r2, #0x10
	mov r0, #0
	str r0, [sp]
	str r0, [sp, #4]
	add r0, r4, #0
	mov r1, #0x55
	mov r3, #0xc
	bl createChildEntity
	str r0, [r4, #0x40]
	add sp, #8
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_08020980: .4byte 0x0000FFF4
	thumb_func_global sub_8020984
sub_8020984: @ 0x08020984
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x40]
	cmp r0, #0
	beq _08020992
	bl destroyObject
_08020992:
	mov r0, #0
	str r0, [r4, #0x40]
	pop {r4}
	pop {r0}
	bx r0
	thumb_func_global sub_802099C
sub_802099C: @ 0x0802099C
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	mov r8, r0
	bl getPlayerObj
	ldr r1, [r0, #4]
	lsl r1, r1, #8
	lsr r6, r1, #0x10
	ldr r1, [r0, #8]
	asr r1, r1, #8
	ldrh r2, [r0, #0x3a]
	add r1, r1, r2
	ldrh r0, [r0, #0x3e]
	sub r1, r1, r0
	lsl r1, r1, #0x10
	lsr r7, r1, #0x10
	mov r0, r8
	ldrh r1, [r0, #0x18]
	ldr r0, _080209F0
	cmp r1, r0
	beq _080209D4
	cmp r6, r1
	bls _08020A28
	mov r1, r8
	ldrh r1, [r1, #0x1a]
	cmp r6, r1
	bhs _08020A28
_080209D4:
	mov r2, r8
	ldr r0, [r2, #0x3c]
	ldr r0, [r0, #8]
	lsl r0, r0, #8
	asr r0, r0, #0x10
	sub r0, r7, r0
	cmp r0, #0
	bge _080209E6
	neg r0, r0
_080209E6:
	cmp r0, #0x3f
	bgt _08020A28
	mov r5, #0
	b _080209FC
	.align 2, 0
_080209F0: .4byte 0x0000FFFF
_080209F4:
	add r0, r5, #0
	add r0, #0x10
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
_080209FC:
	sub r4, r7, r5
	lsl r1, r4, #0x10
	lsr r1, r1, #0x10
	add r0, r6, #0
	bl getCollision
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _080209F4
	mov r1, r8
	ldr r0, [r1, #0x3c]
	ldr r0, [r0, #8]
	lsl r0, r0, #8
	asr r0, r0, #0x10
	sub r0, r4, r0
	cmp r0, #0
	bge _08020A20
	neg r0, r0
_08020A20:
	cmp r0, #0x3f
	bgt _08020A28
	mov r0, #1
	b _08020A2A
_08020A28:
	mov r0, #0
_08020A2A:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	thumb_func_global sub_8020A34
sub_8020A34: @ 0x08020A34
	push {r4, lr}
	sub sp, #8
	add r4, r0, #0
	mov r0, #0
	str r0, [sp]
	str r0, [sp, #4]
	add r0, r4, #0
	mov r1, #0x56
	mov r2, #0
	mov r3, #8
	bl createChildEntity
	str r0, [r4, #0x40]
	add sp, #8
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
	thumb_func_global sub_8020A58
sub_8020A58: @ 0x08020A58
	push {r4, r5, r6, lr}
	sub sp, #4
	add r5, r0, #0
	bl getPlayerObj
	add r4, r0, #0
	add r0, r5, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #1
	bne _08020A88
	mov r0, #0xe0
	lsl r0, r0, #3
	ldr r2, [r5, #0x3c]
	ldr r1, [r2, #4]
	ldr r2, [r2, #8]
	ldr r3, [r4, #4]
	ldr r6, _08020A84
	b _08020A98
	.align 2, 0
_08020A84: .4byte 0xFFFFF800
_08020A88:
	mov r0, #0xe0
	lsl r0, r0, #3
	ldr r2, [r5, #0x3c]
	ldr r1, [r2, #4]
	ldr r2, [r2, #8]
	ldr r3, [r4, #4]
	mov r6, #0x80
	lsl r6, r6, #4
_08020A98:
	add r3, r3, r6
	ldr r4, [r4, #8]
	str r4, [sp]
	bl sub_801F9C0
	add r1, r0, #0
	ldr r0, [r5, #0x3c]
	mov r2, #0xe0
	lsl r2, r2, #3
	bl setObjVelocity
	add sp, #4
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
	thumb_func_global sub_8020AB8
sub_8020AB8: @ 0x08020AB8
	push {r4, r5, r6, r7, lr}
	add r1, r0, #0
	ldr r0, _08020AF8
	add r7, r1, #0
	add r7, #0x6a
	ldrh r6, [r7]
	add r2, r6, r0
	ldrb r0, [r2]
	cmp r0, #0
	beq _08020AF4
	add r0, r1, #0
	add r0, #0x66
	ldrb r5, [r0]
	ldrb r4, [r2]
	ldr r3, _08020AFC
	ldr r2, _08020B00
	ldr r0, [r1, #8]
	ldrh r1, [r0, #0x1e]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r2, [r2, #0x11]
	add r0, r0, r2
	add r0, r0, r3
	ldrb r0, [r0]
	mul r0, r4, r0
	mov r1, #0x64
	bl divide
	cmp r5, r0
	ble _08020B04
_08020AF4:
	mov r0, #0
	b _08020B0A
	.align 2, 0
_08020AF8: .4byte unk_814270b
_08020AFC: .4byte byte_814295B
_08020B00: .4byte 0x03003D40
_08020B04:
	add r0, r6, #1
	strh r0, [r7]
	mov r0, #1
_08020B0A:
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	thumb_func_global sub_8020B10
sub_8020B10: @ 0x08020B10
	push {r4, lr}
	sub sp, #8
	add r4, r0, #0
	mov r0, #0x40
	bl random
	add r0, #0x20
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r2, _08020B58
	add r1, r0, #0
	add r1, #0x40
	lsl r1, r1, #1
	add r1, r1, r2
	mov r3, #0
	ldrsh r1, [r1, r3]
	lsl r0, r0, #1
	add r0, r0, r2
	mov r2, #0
	ldrsh r0, [r0, r2]
	lsl r1, r1, #0x12
	asr r1, r1, #0x10
	str r1, [sp]
	lsl r0, r0, #0x12
	asr r0, r0, #0x10
	str r0, [sp, #4]
	add r0, r4, #0
	mov r1, #0x5b
	mov r2, #0
	mov r3, #0x38
	bl createChildEntity
	add sp, #8
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_08020B58: .4byte dword_80382C8
	thumb_func_global sub_8020B5C
sub_8020B5C: @ 0x08020B5C
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #8
	add r5, r0, #0
	ldr r6, _08020C00
	mov r0, #0x67
	add r0, r0, r5
	mov r8, r0
	ldrb r0, [r0]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1c
	add r0, r0, r6
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	lsl r2, r0, #4
	sub r2, r2, r0
	lsl r2, r2, #1
	mov r4, #0
	str r4, [sp]
	str r4, [sp, #4]
	add r0, r5, #0
	mov r1, #0x53
	mov r3, #0x47
	bl createChildEntity
	add r2, r0, #0
	mov r1, r8
	ldrb r0, [r1]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x16
	mov r1, #0x60
	sub r1, r1, r0
	add r0, r2, #0
	add r0, #0x6e
	strh r1, [r0]
	str r5, [r2, #0x40]
	add r7, r5, #0
	add r7, #0x6e
	ldrh r0, [r7]
	add r0, #1
	strh r0, [r7]
	mov r1, r8
	ldrb r0, [r1]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1c
	add r0, r0, r6
	mov r1, #0
	ldrsb r1, [r0, r1]
	mov r0, #0x2b
	add r2, r1, #0
	mul r2, r0, r2
	str r4, [sp]
	str r4, [sp, #4]
	add r0, r5, #0
	mov r1, #0x53
	mov r3, #0x11
	bl createChildEntity
	add r2, r0, #0
	mov r1, r8
	ldrb r0, [r1]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x16
	add r0, #0xa0
	add r1, r2, #0
	add r1, #0x6e
	strh r0, [r1]
	sub r1, #2
	mov r0, #1
	strh r0, [r1]
	str r5, [r2, #0x40]
	ldrh r0, [r7]
	add r0, #1
	strh r0, [r7]
	add sp, #8
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_08020C00: .4byte dirLutMaybe
	thumb_func_global sub_8020C04
sub_8020C04: @ 0x08020C04
	push {r4, r5, r6, r7, lr}
	add r5, r0, #0
	bl getPlayerObj
	add r4, r0, #0
	bl sub_80102F4
	mov r1, #1
	and r1, r0
	cmp r1, #0
	beq _08020C26
	ldr r1, [r5, #0x3c]
	ldr r0, [r1, #4]
	ldr r1, [r1, #8]
	ldr r2, [r4, #4]
	ldr r3, [r4, #8]
	b _08020C36
_08020C26:
	ldr r1, [r5, #0x3c]
	ldr r0, [r1, #4]
	ldr r1, [r1, #8]
	ldr r2, [r4, #4]
	ldr r3, [r4, #8]
	mov r4, #0xa0
	lsl r4, r4, #5
	add r3, r3, r4
_08020C36:
	bl angle_sub_801F98C
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	ldr r2, [r5, #0x3c]
	ldr r7, _08020C80
	add r0, r4, #0
	add r0, #0x40
	lsl r0, r0, #1
	add r0, r0, r7
	mov r1, #0
	ldrsh r0, [r0, r1]
	mov r6, #0xd0
	mul r0, r6, r0
	cmp r0, #0
	bge _08020C58
	add r0, #0xff
_08020C58:
	asr r1, r0, #8
	add r0, r2, #0
	bl setObjVelocity_X
	ldr r2, [r5, #0x3c]
	lsl r0, r4, #1
	add r0, r0, r7
	mov r4, #0
	ldrsh r0, [r0, r4]
	mul r0, r6, r0
	cmp r0, #0
	bge _08020C72
	add r0, #0xff
_08020C72:
	asr r1, r0, #8
	add r0, r2, #0
	bl setObjVelocity_Y
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_08020C80: .4byte dword_80382C8
	thumb_func_global sub_8020C84
sub_8020C84: @ 0x08020C84
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #8
	add r6, r0, #0
	bl getPlayerObj
	mov r8, r0
	mov r0, #0x6a
	add r0, r0, r6
	mov sb, r0
	ldrh r7, [r0]
	cmp r7, #0
	beq _08020CAA
	cmp r7, #1
	beq _08020D64
	b _08020E20
_08020CAA:
	ldr r0, _08020D54
	add r4, r6, #0
	add r4, #0x67
	ldrb r2, [r4]
	lsl r2, r2, #0x1c
	lsr r2, r2, #0x1a
	add r2, r2, r0
	add r0, r6, #0
	add r0, #0x6c
	add r5, r6, #0
	add r5, #0x6e
	ldrh r1, [r5]
	ldr r3, [r2]
	mov r2, #2
	bl call_r3
	strh r0, [r5]
	ldr r1, _08020D58
	ldrb r3, [r4]
	lsl r0, r3, #0x1c
	lsr r0, r0, #0x1c
	add r0, r0, r1
	mov r1, #0
	ldrsb r1, [r0, r1]
	mov r0, #0xd0
	mul r0, r1, r0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sl, r0
	ldr r1, _08020D5C
	mov r2, #0
	ldrsh r0, [r5, r2]
	lsl r0, r0, #1
	add r0, r0, r1
	mov r2, #0
	ldrsh r1, [r0, r2]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #2
	add r0, r0, r1
	lsl r0, r0, #5
	cmp r0, #0
	bge _08020D02
	add r0, #0xff
_08020D02:
	lsl r0, r0, #8
	lsr r5, r0, #0x10
	mov r2, #0xf
	and r2, r3
	cmp r2, #1
	bne _08020D20
	ldr r0, [r6, #0x3c]
	mov r3, r8
	ldr r1, [r3, #4]
	mov r3, #0x80
	lsl r3, r3, #7
	add r1, r1, r3
	ldr r0, [r0, #4]
	cmp r0, r1
	bgt _08020D36
_08020D20:
	cmp r2, #0
	beq _08020D26
	b _08020E20
_08020D26:
	ldr r0, [r6, #0x3c]
	mov r2, r8
	ldr r1, [r2, #4]
	ldr r3, _08020D60
	add r1, r1, r3
	ldr r0, [r0, #4]
	cmp r0, r1
	bge _08020E20
_08020D36:
	add r1, r6, #0
	add r1, #0x6a
	mov r0, #1
	strh r0, [r1]
	ldrb r2, [r4]
	lsl r1, r2, #0x1c
	lsr r1, r1, #0x1c
	add r1, #1
	and r1, r0
	sub r0, #0x11
	and r0, r2
	orr r0, r1
	strb r0, [r4]
	b _08020E20
	.align 2, 0
_08020D54: .4byte off_8142408
_08020D58: .4byte dirLutMaybe
_08020D5C: .4byte dword_80382C8
_08020D60: .4byte 0xFFFFC000
_08020D64:
	ldr r1, [r6, #0x3c]
	ldr r0, [r1, #4]
	ldr r1, [r1, #8]
	mov r4, r8
	ldr r2, [r4, #4]
	ldr r3, [r4, #8]
	mov r4, #0x80
	lsl r4, r4, #5
	add r3, r3, r4
	add r5, r6, #0
	add r5, #0x6e
	ldrh r4, [r5]
	str r4, [sp]
	mov r4, #2
	str r4, [sp, #4]
	bl sub_801FA88
	mov r4, #0
	strh r0, [r5]
	ldr r3, _08020DEC
	mov r0, #0
	ldrsh r1, [r5, r0]
	add r0, r1, #0
	add r0, #0x40
	lsl r0, r0, #1
	add r0, r0, r3
	mov r2, #0
	ldrsh r0, [r0, r2]
	mov r2, #0xd0
	mul r0, r2, r0
	cmp r0, #0
	bge _08020DA6
	add r0, #0xff
_08020DA6:
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	mov sl, r0
	lsl r0, r1, #1
	add r0, r0, r3
	mov r3, #0
	ldrsh r0, [r0, r3]
	mul r0, r2, r0
	cmp r0, #0
	bge _08020DBC
	add r0, #0xff
_08020DBC:
	lsl r0, r0, #8
	lsr r5, r0, #0x10
	ldr r0, [r6, #0x3c]
	ldr r0, [r0, #8]
	mov r2, r8
	ldr r1, [r2, #8]
	sub r0, r0, r1
	lsl r0, r0, #8
	asr r0, r0, #0x10
	cmp r0, #0
	bge _08020DD4
	neg r0, r0
_08020DD4:
	cmp r0, #0xf
	bgt _08020E20
	mov r3, sb
	strh r4, [r3]
	ldr r0, [r6, #0x3c]
	ldr r0, [r0, #0x1c]
	cmp r0, #0
	blt _08020DF0
	add r0, r6, #0
	add r0, #0x6c
	strh r4, [r0]
	b _08020DF6
	.align 2, 0
_08020DEC: .4byte dword_80382C8
_08020DF0:
	add r0, r6, #0
	add r0, #0x6c
	strh r7, [r0]
_08020DF6:
	ldr r0, [r6, #0x3c]
	ldr r0, [r0, #0x18]
	cmp r0, #0
	blt _08020E12
	add r2, r6, #0
	add r2, #0x67
	ldrb r0, [r2]
	mov r1, #0x10
	neg r1, r1
	and r1, r0
	mov r0, #1
	orr r1, r0
	strb r1, [r2]
	b _08020E20
_08020E12:
	add r2, r6, #0
	add r2, #0x67
	ldrb r1, [r2]
	mov r0, #0x10
	neg r0, r0
	and r0, r1
	strb r0, [r2]
_08020E20:
	ldr r0, [r6, #0x3c]
	mov r1, sl
	lsl r4, r1, #0x10
	asr r4, r4, #0x10
	lsl r2, r5, #0x10
	asr r2, r2, #0x10
	add r1, r4, #0
	bl setObjVelocity
	cmp r4, #0
	blt _08020E40
	add r0, r6, #0
	mov r1, #1
	bl setObjDir_animFrame
	b _08020E48
_08020E40:
	add r0, r6, #0
	mov r1, #0
	bl setObjDir_animFrame
_08020E48:
	add sp, #8
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	thumb_func_global sub_8020E58
sub_8020E58: @ 0x08020E58
	add r0, #0x6e
	mov r1, #0
	ldrsh r2, [r0, r1]
	ldr r1, _08020E74
	ldr r0, _08020E78
	ldrb r0, [r0, #0x11]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	cmp r2, r0
	blt _08020E7C
	mov r0, #0
	b locret_8020E7E
	.align 2, 0
_08020E74: .4byte unk_8142710
_08020E78: .4byte 0x03003D40
_08020E7C:
	mov r0, #1
locret_8020E7E:
	bx lr
	thumb_func_global entity_dec_parent_field6e
entity_dec_parent_field6e: @ 0x08020E80
	ldr r1, [r0, #0x40]
	add r1, #0x6e
	ldrh r0, [r1]
	sub r0, #1
	strh r0, [r1]
	bx lr
	thumb_func_global sub_8020E8C
sub_8020E8C: @ 0x08020E8C
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	add r6, r0, #0
	bl getPlayerObj
	mov r8, r0
	mov r0, #0xe0
	lsl r0, r0, #2
	mov sb, r0
	ldr r2, [r6, #0x3c]
	ldr r1, _08020F34
	add r0, r6, #0
	add r0, #0x67
	ldrb r0, [r0]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1c
	add r0, r0, r1
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	lsl r0, r0, #0xc
	ldr r1, [r2, #4]
	add r1, r1, r0
	ldr r7, [r2, #8]
	mov r2, r8
	ldr r0, [r2, #4]
	sub r4, r1, r0
	cmp r4, #0
	bge _08020ECC
	neg r4, r4
_08020ECC:
	add r0, r4, #0
	mov r1, sb
	bl divide
	add r5, r0, #0
	cmp r5, #0x1d
	bgt _08020EE6
	mov r5, #0x1e
	add r0, r4, #0
	mov r1, #0x1e
	bl divide
	mov sb, r0
_08020EE6:
	lsl r0, r5, #1
	add r0, r0, r5
	cmp r0, #0
	bge _08020EF0
	add r0, #3
_08020EF0:
	asr r2, r0, #2
	mov r1, r8
	ldr r0, [r1, #8]
	sub r0, r0, r7
	ldr r1, _08020F38
	cmp r0, r1
	ble _08020F00
	add r2, r5, #0
_08020F00:
	lsr r4, r2, #0x1f
	add r4, r2, r4
	asr r4, r4, #1
	mov r0, #0xd0
	lsl r0, r0, #5
	add r1, r4, #0
	bl divide
	add r5, r0, #0
	add r1, r4, #0
	bl divide
	add r1, r6, #0
	add r1, #0x6a
	mov r2, sb
	strh r2, [r1]
	add r1, #2
	strh r5, [r1]
	add r1, #2
	strh r0, [r1]
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_08020F34: .4byte dirLutMaybe
_08020F38: .4byte 0x00001AFF
	thumb_func_global sub_8020F3C
sub_8020F3C: @ 0x08020F3C
	push {r4, r5, r6, lr}
	sub sp, #8
	add r5, r0, #0
	ldr r4, _08020F9C
	add r0, #0x67
	ldrb r1, [r0]
	lsl r1, r1, #0x1c
	lsr r0, r1, #0x1c
	add r0, r0, r4
	mov r2, #0
	ldrsb r2, [r0, r2]
	lsl r2, r2, #4
	add r0, r5, #0
	add r0, #0x6a
	ldrh r3, [r0]
	lsr r1, r1, #0x1c
	add r1, r1, r4
	mov r0, #0
	ldrsb r0, [r1, r0]
	mul r0, r3, r0
	str r0, [sp]
	add r4, r5, #0
	add r4, #0x6c
	ldrh r0, [r4]
	str r0, [sp, #4]
	add r0, r5, #0
	mov r1, #0x50
	mov r3, #0x1b
	bl createChildEntity
	add r6, r0, #0
	ldr r0, [r6, #0x3c]
	ldrh r1, [r4]
	bl setObjMaxVelocityY
	ldr r0, [r6, #0x3c]
	add r1, r5, #0
	add r1, #0x6e
	mov r2, #0
	ldrsh r1, [r1, r2]
	neg r1, r1
	bl setObjAccelerationY
	add sp, #8
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_08020F9C: .4byte dirLutMaybe
	thumb_func_global sub_8020FA0
sub_8020FA0: @ 0x08020FA0
	push {r4, r5, r6, lr}
	add r5, r0, #0
	ldr r0, _08021000
	add r6, r5, #0
	add r6, #0x67
	ldrb r2, [r6]
	lsl r2, r2, #0x1c
	lsr r2, r2, #0x1a
	add r2, r2, r0
	add r0, r5, #0
	add r0, #0x6a
	add r4, r5, #0
	add r4, #0x6e
	ldrh r1, [r4]
	ldr r3, [r2]
	mov r2, #3
	bl call_r3
	strh r0, [r4]
	ldr r1, _08021004
	ldrb r0, [r6]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1c
	add r0, r0, r1
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	lsl r1, r0, #1
	add r1, r1, r0
	ldr r2, _08021008
	mov r3, #0
	ldrsh r0, [r4, r3]
	lsl r0, r0, #1
	add r0, r0, r2
	mov r2, #0
	ldrsh r0, [r0, r2]
	lsl r2, r0, #1
	add r2, r2, r0
	ldr r0, [r5, #0x3c]
	lsl r1, r1, #0x17
	asr r1, r1, #0x10
	lsl r2, r2, #0x10
	asr r2, r2, #0x10
	bl setObjVelocity
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_08021000: .4byte off_8142408
_08021004: .4byte dirLutMaybe
_08021008: .4byte dword_80382C8
	thumb_func_global sub_802100C
sub_802100C: @ 0x0802100C
	add r2, r0, #0
	add r0, #0x67
	ldrb r0, [r0]
	mov r1, #0xf
	and r1, r0
	cmp r1, #1
	bne _08021022
	ldr r0, [r2, #0x3c]
	ldr r0, [r0, #0x18]
	cmp r0, #0
	ble _0802102E
_08021022:
	cmp r1, #0
	bne _08021032
	ldr r0, [r2, #0x3c]
	ldr r0, [r0, #0x18]
	cmp r0, #0
	blt _08021032
_0802102E:
	mov r0, #1
	b locret_8021034
_08021032:
	mov r0, #0
locret_8021034:
	bx lr
	.align 2, 0
	thumb_func_global sub_8021038
sub_8021038: @ 0x08021038
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	add r6, r0, #0
	bl getPlayerObj
	mov r5, #0xcc
	lsl r5, r5, #2
	ldr r1, [r6, #0x3c]
	mov r8, r1
	ldr r1, [r0, #4]
	mov r2, r8
	ldr r0, [r2, #4]
	sub r0, r1, r0
	cmp r0, #0
	bge _0802105C
	neg r0, r0
_0802105C:
	mov r1, #0xf0
	lsl r1, r1, #6
	add r4, r0, r1
	ldr r0, _080210E0
	cmp r4, r0
	bgt _0802106C
	mov r4, #0xfc
	lsl r4, r4, #7
_0802106C:
	add r0, r4, #0
	mul r0, r5, r0
	mov r1, #0xfc
	lsl r1, r1, #7
	bl divide
	add r5, r0, #0
	lsl r4, r4, #1
	add r0, r4, #0
	add r1, r5, #0
	bl divide
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	add r1, r0, #0
	mul r1, r0, r1
	add r0, r4, #0
	bl divide
	add r4, r0, #0
	ldr r2, _080210E4
	mov sb, r2
	add r7, r6, #0
	add r7, #0x67
	ldrb r0, [r7]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1c
	add r0, sb
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	add r1, r5, #0
	mul r1, r0, r1
	mov r0, r8
	bl setObjVelocity_X
	ldr r0, [r6, #0x3c]
	add r1, r5, #0
	bl setObjMaxVelocityX
	ldr r0, [r6, #0x3c]
	neg r4, r4
	ldrb r1, [r7]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	add r1, sb
	ldrb r1, [r1]
	lsl r1, r1, #0x18
	asr r1, r1, #0x18
	mul r1, r4, r1
	bl setObjAccelerationX
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_080210E0: .4byte 0x00007DFF
_080210E4: .4byte dirLutMaybe
	thumb_func_global sub_80210E8
sub_80210E8: @ 0x080210E8
	push {r4, r5, r6, lr}
	add r5, r0, #0
	ldr r4, [r5, #0x3c]
	ldr r1, [r4, #8]
	ldrh r0, [r5, #0x16]
	add r0, #0x20
	lsl r0, r0, #8
	sub r0, r0, r1
	ldr r1, _08021138
	add r0, r0, r1
	mov r1, #0x1e
	bl divide
	add r6, r0, #0
	ldrh r0, [r5, #0x14]
	lsl r0, r0, #8
	ldr r1, [r4, #4]
	sub r0, r0, r1
	mov r1, #0x1e
	bl divide
	add r1, r0, #0
	add r0, r4, #0
	add r2, r6, #0
	bl setObjVelocity
	ldr r0, [r5, #0x3c]
	mov r4, #0x80
	lsl r4, r4, #5
	add r1, r4, #0
	bl setObjMaxVelocityX
	ldr r0, [r5, #0x3c]
	add r1, r4, #0
	bl setObjMaxVelocityY
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_08021138: .4byte 0x0000AC44
	thumb_func_global sub_802113C
sub_802113C: @ 0x0802113C
	push {lr}
	ldr r2, _0802115C
	ldr r0, [r0, #0x3c]
	ldr r0, [r0]
	add r0, #0x22
	mov r1, #0
	ldrsb r1, [r0, r1]
	lsl r1, r1, #5
	ldr r0, _08021160
	add r1, r1, r0
	add r0, r2, #0
	mov r2, #0
	bl startPalAnimation
	pop {r0}
	bx r0
	.align 2, 0
_0802115C: .4byte a_
_08021160: .4byte 0x05000200
	thumb_func_global sub_8021164
sub_8021164: @ 0x08021164
	ldr r2, _08021188
	ldr r1, _0802118C
	str r1, [r2]
	ldr r0, [r0, #0x3c]
	ldr r0, [r0]
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	lsl r0, r0, #5
	ldr r1, _08021190
	add r0, r0, r1
	str r0, [r2, #4]
	ldr r0, _08021194
	str r0, [r2, #8]
	ldr r0, [r2, #8]
	bx lr
	.align 2, 0
_08021188: .4byte 0x040000D4
_0802118C: .4byte unk_82DFD48
_08021190: .4byte 0x05000200
_08021194: .4byte 0x80000010
	thumb_func_global sub_8021198
sub_8021198: @ 0x08021198
	push {lr}
	ldr r2, _080211B8
	ldr r0, [r0, #0x3c]
	ldr r0, [r0]
	add r0, #0x22
	mov r1, #0
	ldrsb r1, [r0, r1]
	lsl r1, r1, #5
	ldr r0, _080211BC
	add r1, r1, r0
	add r0, r2, #0
	mov r2, #0
	bl startPalAnimation
	pop {r0}
	bx r0
	.align 2, 0
_080211B8: .4byte a__0
_080211BC: .4byte 0x05000200
	thumb_func_global sub_80211C0
sub_80211C0: @ 0x080211C0
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x3c]
	ldr r0, [r0, #0x18]
	cmp r0, #0
	bne _08021214
	add r2, r4, #0
	add r2, #0x67
	ldrb r1, [r2]
	mov r0, #0xf
	and r0, r1
	cmp r0, #1
	bne _080211EC
	mov r0, #0x10
	neg r0, r0
	and r0, r1
	strb r0, [r2]
	ldr r0, [r4, #0x3c]
	mov r1, #0xe
	bl setObjAccelerationX
	b _08021202
_080211EC:
	mov r0, #0x10
	neg r0, r0
	and r0, r1
	mov r1, #1
	orr r0, r1
	strb r0, [r2]
	ldr r0, [r4, #0x3c]
	mov r1, #0xe
	neg r1, r1
	bl setObjAccelerationX
_08021202:
	ldr r0, [r4, #0x3c]
	mov r1, #0x6e
	neg r1, r1
	bl setObjVelocity_Y
	ldr r0, [r4, #0x3c]
	mov r1, #1
	bl setObjAccelerationY
_08021214:
	ldr r2, [r4, #0x3c]
	ldr r1, [r2, #0x18]
	cmp r1, #0
	bge _0802121E
	neg r1, r1
_0802121E:
	mov r0, #0xaf
	lsl r0, r0, #1
	cmp r1, r0
	bne _08021248
	add r0, r4, #0
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #1
	bne _0802123E
	add r0, r2, #0
	mov r1, #0xe
	bl setObjAccelerationX
	b _08021248
_0802123E:
	mov r1, #0xe
	neg r1, r1
	add r0, r2, #0
	bl setObjAccelerationX
_08021248:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
	
	
	
	
	
	
	
	
	
	
	
	
thumb_func_global sub_8021250
sub_8021250: @ 0x08021250
	push {r4, r5, r6, r7, lr}
	sub sp, #8
	add r5, r0, #0
	ldr r0, [r5, #0x3c]
	ldr r1, [r0, #4]
	asr r1, r1, #8
	strh r1, [r5, #0x14]
	ldr r0, [r0, #8]
	asr r0, r0, #8
	strh r0, [r5, #0x16]
	mov r7, #0x80
	lsl r7, r7, #2
	ldr r4, _08021294
	ldrb r0, [r4, #0x11]
	cmp r0, #1
	bne _0802129C
	add r3, r5, #0
	add r3, #0x66
	ldr r2, _08021298
	ldr r0, [r5, #8]
	ldrh r1, [r0, #0x1e]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r4, [r4, #0x11]
	add r0, r0, r4
	add r0, r0, r2
	ldrb r0, [r0]
	ldrb r1, [r3]
	lsr r0, r0, #1
	add r7, #0x40
	add r6, r3, #0
	cmp r1, r0
	bhi _080212A8
	b _080212A4
	.align 2, 0
_08021294: .4byte 0x03003D40
_08021298: .4byte byte_814295B
_0802129C:
	add r6, r5, #0
	add r6, #0x66
	cmp r0, #2
	bne _080212A8
_080212A4:
	mov r7, #0xa0
	lsl r7, r7, #2
_080212A8:
	ldr r0, [r5, #0x3c]
	ldr r1, [r0, #4]
	lsl r1, r1, #8
	lsr r1, r1, #0x10
	ldr r2, [r0, #8]
	asr r2, r2, #8
	add r2, #0x16
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	mov r0, #0x8a
	mov r3, #0
	bl createEntWithDirection
	add r4, r0, #0
	ldrb r0, [r6]
	add r1, r4, #0
	add r1, #0x66
	strb r0, [r1]
	str r5, [r4, #0x40]
	add r0, r4, #0
	add r0, #0x6a
	strh r7, [r0]
	mov r5, #0
	mov r6, #0
_080212D8:
	str r6, [sp]
	str r6, [sp, #4]
	add r0, r4, #0
	mov r1, #0x8b
	mov r2, #0
	mov r3, #0
	bl createChildEntity
	add r1, r0, #0
	add r0, #0x6a
	strh r7, [r0]
	str r4, [r1, #0x40]
	add r4, r1, #0
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, #0xb
	bls _080212D8
	mov r0, #0
	str r0, [sp]
	str r0, [sp, #4]
	add r0, r1, #0
	mov r1, #0x8c
	mov r2, #0
	mov r3, #0
	bl createChildEntity
	add r1, r0, #0
	str r4, [r1, #0x40]
	add sp, #8
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
	
	



thumb_func_global sub_802131C
sub_802131C: @ 0x0802131C
	push {r4, r5, r6, lr}
	sub sp, #8
	add r5, r0, #0
	ldr r1, _08021384
	mov r0, sp
	mov r2, #7
	bl memcpy
	mov r0, #7
	bl random
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	add r0, sp
	ldrb r4, [r0]
	ldr r2, [r5, #0x3c]
	ldr r6, _08021388
	add r0, r4, #0
	add r0, #0x40
	lsl r0, r0, #1
	add r0, r0, r6
	mov r3, #0
	ldrsh r1, [r0, r3]
	lsl r0, r1, #3
	add r0, r0, r1
	lsl r0, r0, #6
	cmp r0, #0
	bge _08021356
	add r0, #0xff
_08021356:
	asr r1, r0, #8
	add r0, r2, #0
	bl setObjVelocity_X
	ldr r2, [r5, #0x3c]
	lsl r0, r4, #1
	add r0, r0, r6
	mov r3, #0
	ldrsh r1, [r0, r3]
	lsl r0, r1, #3
	add r0, r0, r1
	lsl r0, r0, #6
	cmp r0, #0
	bge _08021374
	add r0, #0xff
_08021374:
	asr r1, r0, #8
	add r0, r2, #0
	bl setObjVelocity_Y
	add sp, #8
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_08021384: .4byte unk_814271C
_08021388: .4byte dword_80382C8






thumb_func_global sub_802138C
sub_802138C: @ 0x0802138C
	push {r4, r5, r6, lr}
	sub sp, #4
	add r6, r0, #0
	ldr r1, _08021424
	mov r0, sp
	mov r2, #3
	bl memcpy
	ldr r0, [r6, #0x3c]
	add r0, #0x51
	ldrb r0, [r0]
	cmp r0, #0
	bne _080213A8
	b _0802153A
_080213A8:
	add r4, r6, #0
	add r4, #0x66
	ldr r2, _08021428
	ldr r0, _0802142C
	ldrb r3, [r0, #0x11]
	ldr r0, [r6, #8]
	ldrh r1, [r0, #0x1e]
	lsl r0, r1, #1
	add r0, r0, r1
	add r0, r3, r0
	add r0, r0, r2
	ldrb r0, [r0]
	ldrb r1, [r4]
	lsr r0, r0, #1
	cmp r1, r0
	bhs _080213D8
	mov r1, sp
	add r0, r1, r3
	ldrb r0, [r0]
	bl random
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _080214A2
_080213D8:
	ldr r1, [r6, #0x3c]
	ldr r0, [r1, #0x18]
	lsl r0, r0, #8
	asr r0, r0, #0x10
	ldr r1, [r1, #0x1c]
	lsl r1, r1, #8
	asr r1, r1, #0x10
	bl Bios_arctan
	lsl r0, r0, #0x10
	lsr r0, r0, #8
	ldr r1, _08021430
	bl divide
	lsl r0, r0, #0x18
	lsr r5, r0, #0x18
	mov r0, #4
	bl random
	lsl r0, r0, #0x1b
	mov r3, #0x80
	lsl r3, r3, #0x14
	add r0, r0, r3
	lsr r3, r0, #0x18
	ldr r2, [r6, #0x3c]
	add r0, r2, #0
	add r0, #0x51
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _0802143C
	ldr r0, [r2, #0x18]
	cmp r0, #0
	ble _08021434
	lsl r0, r3, #0x18
	neg r0, r0
	b _0802143A
	.align 2, 0
_08021424: .4byte unk_8142723
_08021428: .4byte byte_814295B
_0802142C: .4byte 0x03003D40
_08021430: .4byte 0x0000FFFF
_08021434:
	add r0, r3, #0
	add r0, #0x80
	lsl r0, r0, #0x18
_0802143A:
	lsr r5, r0, #0x18
_0802143C:
	ldr r2, [r6, #0x3c]
	add r0, r2, #0
	add r0, #0x51
	ldrb r1, [r0]
	mov r0, #2
	and r0, r1
	cmp r0, #0
	beq _0802145E
	ldr r0, [r2, #0x18]
	cmp r0, #0
	ble _08021456
	add r5, r3, #0
	b _0802145E
_08021456:
	mov r0, #0x80
	sub r0, r0, r3
	lsl r0, r0, #0x18
	lsr r5, r0, #0x18
_0802145E:
	add r0, r2, #0
	add r0, #0x51
	ldrb r1, [r0]
	mov r0, #8
	and r0, r1
	cmp r0, #0
	beq _08021480
	ldr r0, [r2, #0x1c]
	cmp r0, #0
	ble _08021478
	add r0, r3, #0
	add r0, #0x40
	b _0802147C
_08021478:
	mov r0, #0xc0
	sub r0, r0, r3
_0802147C:
	lsl r0, r0, #0x18
	lsr r5, r0, #0x18
_08021480:
	ldr r2, [r6, #0x3c]
	add r0, r2, #0
	add r0, #0x51
	ldrb r1, [r0]
	mov r0, #4
	and r0, r1
	cmp r0, #0
	beq _080214B8
	ldr r0, [r2, #0x1c]
	cmp r0, #0
	ble _0802149C
	mov r0, #0x40
	sub r0, r0, r3
	b _080214B4
_0802149C:
	add r0, r3, #0
	add r0, #0xc0
	b _080214B4
_080214A2:
	ldr r1, [r6, #0x3c]
	ldr r0, [r1, #4]
	ldr r1, [r1, #8]
	ldr r2, _08021514
	ldr r3, [r2]
	ldr r2, [r3, #4]
	ldr r3, [r3, #8]
	bl angle_sub_801F98C
_080214B4:
	lsl r0, r0, #0x18
	lsr r5, r0, #0x18
_080214B8:
	add r0, r6, #0
	add r0, #0x6e
	strh r5, [r0]
	sub r0, #4
	ldrh r2, [r0]
	ldr r1, _08021518
	add r0, r5, #0
	add r0, #0x40
	lsl r0, r0, #1
	add r0, r0, r1
	mov r3, #0
	ldrsh r0, [r0, r3]
	mul r0, r2, r0
	cmp r0, #0
	bge _080214D8
	add r0, #0xff
_080214D8:
	lsl r0, r0, #8
	lsr r4, r0, #0x10
	lsl r0, r5, #1
	add r0, r0, r1
	mov r1, #0
	ldrsh r0, [r0, r1]
	mul r2, r0, r2
	cmp r2, #0
	bge _080214EC
	add r2, #0xff
_080214EC:
	lsl r2, r2, #8
	ldr r0, [r6, #0x3c]
	lsl r4, r4, #0x10
	asr r4, r4, #0x10
	asr r2, r2, #0x10
	add r1, r4, #0
	bl setObjVelocity
	cmp r4, #0
	blt _0802151C
	add r2, r6, #0
	add r2, #0x67
	ldrb r0, [r2]
	mov r1, #0x10
	neg r1, r1
	and r1, r0
	mov r0, #1
	orr r1, r0
	strb r1, [r2]
	b _0802152A
	.align 2, 0
_08021514: .4byte 0x03004750
_08021518: .4byte dword_80382C8
_0802151C:
	add r2, r6, #0
	add r2, #0x67
	ldrb r1, [r2]
	mov r0, #0x10
	neg r0, r0
	and r0, r1
	strb r0, [r2]
_0802152A:
	lsr r1, r5, #3
	add r0, r6, #0
	bl setObjAnimation_
	mov r0, #0xbf
	lsl r0, r0, #1
	bl playSong
_0802153A:
	add sp, #4
	pop {r4, r5, r6}
	pop {r0}
	bx r0




.align 2, 0
	thumb_func_global sub_8021544
sub_8021544: @ 0x08021544
	push {r4, lr}
	add r4, r0, #0
	ldr r1, [r4, #0x3c]
	ldr r0, [r1, #0x18]
	lsl r0, r0, #8
	asr r0, r0, #0x10
	ldr r1, [r1, #0x1c]
	lsl r1, r1, #8
	asr r1, r1, #0x10
	bl Bios_arctan
	lsl r0, r0, #0x10
	lsr r0, r0, #8
	ldr r1, _08021584
	bl divide
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	ldr r0, [r4, #0x3c]
	ldr r0, [r0, #0x18]
	cmp r0, #0
	blt _08021588
	add r2, r4, #0
	add r2, #0x67
	ldrb r0, [r2]
	mov r1, #0x10
	neg r1, r1
	and r1, r0
	mov r0, #1
	orr r1, r0
	strb r1, [r2]
	b _08021596
	.align 2, 0
_08021584: .4byte 0x0000FFFF
_08021588:
	add r2, r4, #0
	add r2, #0x67
	ldrb r1, [r2]
	mov r0, #0x10
	neg r0, r0
	and r0, r1
	strb r0, [r2]
_08021596:
	lsr r1, r3, #3
	add r0, r4, #0
	bl setObjAnimation_
	pop {r4}
	pop {r0}
	bx r0
	
	
	
	thumb_func_global sub_80215A4
sub_80215A4: @ 0x080215A4
	push {r4, r5, r6, lr}
	mov r6, sl
	mov r5, sb
	mov r4, r8
	push {r4, r5, r6}
	add r5, r0, #0
	ldr r1, [r5, #0x3c]
	ldr r0, [r1, #0x18]
	lsl r0, r0, #8
	asr r0, r0, #0x10
	ldr r1, [r1, #0x1c]
	lsl r1, r1, #8
	asr r1, r1, #0x10
	bl Bios_arctan
	lsl r0, r0, #0x10
	lsr r0, r0, #8
	ldr r1, _08021638
	mov sl, r1
	bl divide
	mov sb, r0
	lsl r0, r0, #0x10
	mov sb, r0
	ldr r6, [r5, #0x40]
	ldr r4, [r5, #0x3c]
	ldr r1, [r6, #0x3c]
	mov r8, r1
	ldr r0, [r1, #0xc]
	ldr r1, [r4, #4]
	sub r0, r0, r1
	mov r1, #6
	bl divide
	str r0, [r4, #0x18]
	mov r1, r8
	ldr r0, [r1, #0x10]
	ldr r1, [r4, #8]
	sub r0, r0, r1
	mov r1, #6
	bl divide
	str r0, [r4, #0x1c]
	add r0, r6, #0
	add r0, #0x66
	ldrb r0, [r0]
	add r1, r5, #0
	add r1, #0x66
	strb r0, [r1]
	ldr r0, [r6, #0x44]
	lsl r0, r0, #5
	lsr r0, r0, #0x10
	mov r1, sl
	and r0, r1
	lsl r0, r0, #0xb
	ldr r1, [r5, #0x44]
	ldr r2, _0802163C
	and r1, r2
	orr r1, r0
	str r1, [r5, #0x44]
	mov r0, sb
	lsr r0, r0, #0x13
	mov sb, r0
	add r0, r5, #0
	mov r1, sb
	bl setObjAnimation
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_08021638: .4byte 0x0000FFFF
_0802163C: .4byte 0xF80007FF



	thumb_func_global sub_8021640
sub_8021640: @ 0x08021640
	push {lr}
	add r1, r0, #0
	ldr r0, [r1, #0x3c]
	ldr r0, [r0, #0x1c]
	mov r2, #0xc0
	cmp r0, #0
	ble _08021650
	mov r2, #0x40
_08021650:
	add r0, r1, #0
	add r0, #0x6e
	strh r2, [r0]
	add r0, r1, #0
	bl sub_8021660
	pop {r0}
	bx r0
	
	
	
	thumb_func_global sub_8021660
sub_8021660: @ 0x08021660
	push {r4, r5, r6, lr}
	add r4, r0, #0
	add r5, r4, #0
	add r5, #0x6e
	ldrb r0, [r5]
	sub r0, #3
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	strh r0, [r5]
	add r0, r4, #0
	add r0, #0x6a
	ldrh r2, [r0]
	ldr r6, _080216E4
	mov r0, #0
	ldrsh r3, [r5, r0]
	add r0, r3, #0
	add r0, #0x40
	lsl r0, r0, #1
	add r0, r0, r6
	mov r1, #0
	ldrsh r0, [r0, r1]
	mul r0, r2, r0
	cmp r0, #0
	bge _08021692
	add r0, #0xff
_08021692:
	lsl r0, r0, #8
	lsr r1, r0, #0x10
	lsl r0, r3, #1
	add r0, r0, r6
	mov r3, #0
	ldrsh r0, [r0, r3]
	mul r0, r2, r0
	cmp r0, #0
	bge _080216A6
	add r0, #0xff
_080216A6:
	lsl r2, r0, #8
	ldr r0, [r4, #0x3c]
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	asr r2, r2, #0x10
	bl setObjVelocity
	add r2, r4, #0
	add r2, #0x67
	ldrb r1, [r2]
	mov r0, #0x10
	neg r0, r0
	and r0, r1
	mov r1, #1
	orr r0, r1
	strb r0, [r2]
	mov r1, #0
	ldrsh r0, [r5, r1]
	cmp r0, #0
	bge _080216D0
	add r0, #7
_080216D0:
	asr r1, r0, #3
	add r1, #0x20
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	add r0, r4, #0
	bl setObjAnimation
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_080216E4: .4byte dword_80382C8



	thumb_func_global sub_80216E8
sub_80216E8: @ 0x080216E8
	push {r4, r5, r6, r7, lr}
	add r5, r0, #0
	ldr r6, [r5, #0x40]
	ldr r1, [r5, #0x3c]
	ldr r0, [r1, #4]
	ldr r1, [r1, #8]
	ldr r3, [r6, #0x3c]
	ldr r2, [r3, #4]
	ldr r3, [r3, #8]
	mov r4, #0xb0
	lsl r4, r4, #5
	add r3, r3, r4
	bl angle_sub_801F98C
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	add r0, r5, #0
	add r0, #0x6a
	ldrh r2, [r0]
	ldr r1, _08021774
	add r0, r7, #0
	add r0, #0x40
	lsl r0, r0, #1
	add r0, r0, r1
	mov r3, #0
	ldrsh r0, [r0, r3]
	mul r0, r2, r0
	cmp r0, #0
	bge _08021724
	add r0, #0xff
_08021724:
	lsl r0, r0, #8
	lsr r4, r0, #0x10
	lsl r0, r7, #1
	add r0, r0, r1
	mov r1, #0
	ldrsh r0, [r0, r1]
	mul r2, r0, r2
	cmp r2, #0
	bge _08021738
	add r2, #0xff
_08021738:
	lsl r2, r2, #8
	ldr r0, [r5, #0x3c]
	lsl r4, r4, #0x10
	asr r4, r4, #0x10
	asr r2, r2, #0x10
	add r1, r4, #0
	bl setObjVelocity
	ldr r0, [r6, #0x3c]
	ldr r0, [r0, #4]
	asr r0, r0, #8
	strh r0, [r5, #0x14]
	ldr r0, [r6, #0x3c]
	ldr r0, [r0, #8]
	asr r0, r0, #8
	add r0, #0x16
	strh r0, [r5, #0x16]
	cmp r4, #0
	blt _08021778
	add r2, r5, #0
	add r2, #0x67
	ldrb r0, [r2]
	mov r1, #0x10
	neg r1, r1
	and r1, r0
	mov r0, #1
	orr r1, r0
	strb r1, [r2]
	b _08021786
	.align 2, 0
_08021774: .4byte dword_80382C8
_08021778:
	add r2, r5, #0
	add r2, #0x67
	ldrb r1, [r2]
	mov r0, #0x10
	neg r0, r0
	and r0, r1
	strb r0, [r2]
_08021786:
	lsr r1, r7, #3
	add r0, r5, #0
	bl setObjAnimation_
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	
	
	
	thumb_func_global sub_8021794
sub_8021794: @ 0x08021794
	ldr r1, [r0, #0x40]
	add r0, #0x66
	ldrb r0, [r0]
	add r1, #0x66
	strb r0, [r1]
	bx lr
	
	
	
	thumb_func_global sub_80217A0
sub_80217A0: @ 0x080217A0
	push {r4, r5, lr}
	add r5, r0, #0
	mov r0, #0x80
	lsl r0, r0, #1
	bl random
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	add r2, r0, #0
	ldr r4, _0802180C
	add r0, #0x40
	lsl r0, r0, #1
	add r0, r0, r4
	mov r3, #0
	ldrsh r1, [r0, r3]
	lsl r0, r1, #3
	add r0, r0, r1
	lsl r0, r0, #6
	cmp r0, #0
	bge _080217CA
	add r0, #0xff
_080217CA:
	lsl r0, r0, #8
	lsr r3, r0, #0x10
	lsl r0, r2, #1
	add r0, r0, r4
	mov r2, #0
	ldrsh r1, [r0, r2]
	lsl r0, r1, #3
	add r0, r0, r1
	lsl r0, r0, #6
	cmp r0, #0
	bge _080217E2
	add r0, #0xff
_080217E2:
	lsl r2, r0, #8
	ldr r0, [r5, #0x3c]
	lsl r1, r3, #0x10
	asr r1, r1, #0x10
	asr r2, r2, #0x10
	bl setObjVelocity
	ldr r3, [r5, #0x40]
	ldr r2, [r3, #0x44]
	lsl r1, r2, #5
	lsr r1, r1, #0x10
	mov r0, #0x10
	orr r1, r0
	lsl r1, r1, #0xb
	ldr r0, _08021810
	and r0, r2
	orr r0, r1
	str r0, [r3, #0x44]
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_0802180C: .4byte dword_80382C8
_08021810: .4byte 0xF80007FF



	thumb_func_global sub_8021814
sub_8021814: @ 0x08021814
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x10]
	cmp r0, #0
	bne _08021842
	ldr r0, _08021850
	ldr r1, [r4, #0x3c]
	ldr r1, [r1]
	add r1, #0x22
	ldrb r1, [r1]
	lsl r1, r1, #0x18
	asr r1, r1, #0x18
	lsl r1, r1, #5
	ldr r2, _08021854
	add r1, r1, r2
	mov r2, #0
	bl startPalAnimation
	add r1, r4, #0
	add r1, #0x69
	strb r0, [r1]
	ldr r0, _08021858
	str r0, [r4, #0x10]
_08021842:
	add r1, r4, #0
	add r1, #0x68
	mov r0, #0
	strb r0, [r1]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_08021850: .4byte aTy
_08021854: .4byte 0x05000200
_08021858: .4byte (sub_802185C+1)



	thumb_func_global sub_802185C
sub_802185C: @ 0x0802185C
	push {r4, r5, r6, lr}
	add r4, r0, #0
	add r5, r4, #0
	add r5, #0x68
	ldrb r0, [r5]
	add r0, #1
	mov r6, #0
	strb r0, [r5]
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0x3b
	bls _080218A2
	ldr r1, _080218A8
	ldr r0, _080218AC
	str r0, [r1]
	ldr r0, [r4, #0x3c]
	ldr r0, [r0]
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	lsl r0, r0, #5
	ldr r2, _080218B0
	add r0, r0, r2
	str r0, [r1, #4]
	ldr r0, _080218B4
	str r0, [r1, #8]
	ldr r0, [r1, #8]
	add r0, r4, #0
	add r0, #0x69
	ldrb r0, [r0]
	bl freePalFadeTaskAtIndex
	str r6, [r4, #0x10]
	strb r6, [r5]
_080218A2:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_080218A8: .4byte 0x040000D4
_080218AC: .4byte unk_82D21B8
_080218B0: .4byte 0x05000200
_080218B4: .4byte 0x80000010



	thumb_func_global sub_80218B8
sub_80218B8: @ 0x080218B8
	push {r4, lr}
	add r4, r0, #0
	bl ObjPlayerDiff_Y
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #0
	ble _080218D2
	ldr r0, [r4, #0x3c]
	mov r1, #0x80
	bl setObjVelocity_Y
	b _080218EA
_080218D2:
	cmp r0, #0
	bge _080218E2
	ldr r0, [r4, #0x3c]
	mov r1, #0x80
	neg r1, r1
	bl setObjVelocity_Y
	b _080218EA
_080218E2:
	ldr r0, [r4, #0x3c]
	mov r1, #0
	bl setObjVelocity_Y
_080218EA:
	pop {r4}
	pop {r0}
	bx r0
	
	
	
	thumb_func_global sub_80218F0
sub_80218F0: @ 0x080218F0
	push {r4, r5, r6, r7, lr}
	mov ip, r0
	ldr r0, _08021944
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	cmp r0, #0
	blt _0802193E
	ldr r1, _08021948
	mov r7, ip
	add r7, #0x6a
	ldrh r6, [r7]
	ldr r0, _0802194C
	ldrb r5, [r0, #0x11]
	lsl r0, r5, #3
	add r0, r6, r0
	add r1, r0, r1
	ldrb r0, [r1]
	cmp r0, #0
	beq _0802193E
	mov r0, ip
	add r0, #0x66
	ldrb r4, [r0]
	ldrb r3, [r1]
	ldr r2, _08021950
	mov r1, ip
	ldr r0, [r1, #8]
	ldrh r1, [r0, #0x1e]
	lsl r0, r1, #1
	add r0, r0, r1
	add r0, r5, r0
	add r0, r0, r2
	ldrb r0, [r0]
	mul r0, r3, r0
	mov r1, #0x64
	bl divide
	cmp r4, r0
	ble _08021954
_0802193E:
	mov r0, #0
	b _0802195A
	.align 2, 0
_08021944: .4byte 0x03004750
_08021948: .4byte unk_8142726
_0802194C: .4byte 0x03003D40
_08021950: .4byte byte_814295B
_08021954:
	add r0, r6, #1
	strh r0, [r7]
	mov r0, #1
_0802195A:
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	
	
	
	thumb_func_global sub_8021960
sub_8021960: @ 0x08021960
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	add r7, r0, #0
	ldr r0, _080219D0
	mov sb, r0
	ldr r1, _080219D4
	mov r8, r1
	ldr r0, _080219D8
	mov sl, r0
	add r6, r7, #0
	add r6, #0x6a
_0802197C:
	add r0, r7, #0
	add r0, #0x66
	ldrb r4, [r0]
	ldrh r5, [r6]
	mov r1, r8
	ldrb r2, [r1, #0x11]
	lsl r0, r2, #3
	add r0, r5, r0
	add r0, sb
	ldrb r3, [r0]
	ldr r0, [r7, #8]
	ldrh r1, [r0, #0x1e]
	lsl r0, r1, #1
	add r0, r0, r1
	add r2, r2, r0
	add r2, sl
	ldrb r0, [r2]
	mul r0, r3, r0
	mov r1, #0x64
	bl divide
	cmp r4, r0
	bgt _080219C0
	add r0, r5, #1
	strh r0, [r6]
	mov r1, r8
	ldrb r0, [r1, #0x11]
	lsl r0, r0, #3
	ldrh r1, [r6]
	add r0, r0, r1
	add r0, sb
	ldrb r0, [r0]
	cmp r0, #0
	bne _0802197C
_080219C0:
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_080219D0: .4byte unk_8142726
_080219D4: .4byte 0x03003D40
_080219D8: .4byte byte_814295B



	thumb_func_global sub_80219DC
sub_80219DC: @ 0x080219DC
	push {r4, lr}
	add r4, r0, #0
	ldr r0, _08021A04
	ldr r1, [r4, #0x3c]
	ldr r1, [r1]
	add r1, #0x22
	ldrb r1, [r1]
	lsl r1, r1, #0x18
	asr r1, r1, #0x18
	lsl r1, r1, #5
	ldr r2, _08021A08
	add r1, r1, r2
	mov r2, #0
	bl startPalAnimation
	add r4, #0x70
	strh r0, [r4]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_08021A04: .4byte unk_82EE2A4
_08021A08: .4byte 0x05000200



	thumb_func_global sub_8021A0C
sub_8021A0C: @ 0x08021A0C
	push {r4, lr}
	add r4, r0, #0
	add r0, #0x70
	ldrh r0, [r0]
	bl freePalFadeTaskAtIndex
	ldr r1, _08021A40
	ldr r0, _08021A44
	str r0, [r1]
	ldr r0, [r4, #0x3c]
	ldr r0, [r0]
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	lsl r0, r0, #5
	ldr r2, _08021A48
	add r0, r0, r2
	str r0, [r1, #4]
	ldr r0, _08021A4C
	str r0, [r1, #8]
	ldr r0, [r1, #8]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_08021A40: .4byte 0x040000D4
_08021A44: .4byte unk_82E97DC
_08021A48: .4byte 0x05000200
_08021A4C: .4byte 0x80000020



	thumb_func_global sub_8021A50
sub_8021A50: @ 0x08021A50
	push {r4, lr}
	add r4, r0, #0
	add r4, #0x6e
	mov r1, #0
	ldrsh r0, [r4, r1]
	cmp r0, #0
	bne _08021A68
	mov r0, #3
	bl random
	add r0, #4
	strh r0, [r4]
_08021A68:
	ldrh r0, [r4]
	sub r0, #1
	strh r0, [r4]
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08021A78
	mov r0, #0
	b _08021A7A
_08021A78:
	mov r0, #1
_08021A7A:
	pop {r4}
	pop {r1}
	bx r1
	
	
	
	thumb_func_global sub_8021A80
sub_8021A80: @ 0x08021A80
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	add r6, r0, #0
	ldr r0, _08021AE0
	ldrb r0, [r0, #0x11]
	cmp r0, #2
	beq sub_8021AE8
	mov r0, #3
	bl random
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldr r4, [r6, #0x3c]
	ldr r2, _08021AE4
	lsl r1, r5, #2
	add r0, r1, r2
	ldrh r0, [r0]
	lsl r0, r0, #8
	str r0, [r4, #4]
	add r3, r2, #2
	add r1, r1, r3
	ldrh r0, [r1]
	lsl r0, r0, #8
	str r0, [r4, #8]
	mov r8, r2
	add r7, r3, #0
	mov r4, #1
_08021AB8:
	add r0, r5, #1
	mov r1, #3
	bl idivmod
	add r5, r0, #0
	lsl r2, r5, #2
	mov r1, r8
	add r0, r2, r1
	ldrh r1, [r0]
	add r2, r2, r7
	ldrh r2, [r2]
	mov r0, #0x90
	mov r3, #1
	bl createEntWithDirection
	str r6, [r0, #0x40]
	sub r4, #1
	cmp r4, #0
	bge _08021AB8
	b _08021B36
	.align 2, 0
_08021AE0: .4byte 0x03003D40
_08021AE4: .4byte unk_814274e


thumb_func_global sub_8021AE8
sub_8021AE8:
	mov r0, #5
	bl random
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldr r4, [r6, #0x3c]
	ldr r2, _08021B40
	lsl r1, r5, #2
	add r0, r1, r2
	ldrh r0, [r0]
	lsl r0, r0, #8
	str r0, [r4, #4]
	add r3, r2, #2
	add r1, r1, r3
	ldrh r0, [r1]
	lsl r0, r0, #8
	str r0, [r4, #8]
	mov r8, r2
	add r7, r3, #0
	mov r4, #3
_08021B10:
	add r0, r5, #1
	mov r1, #5
	bl idivmod
	add r5, r0, #0
	lsl r2, r5, #2
	mov r1, r8
	add r0, r2, r1
	ldrh r1, [r0]
	add r2, r2, r7
	ldrh r2, [r2]
	mov r0, #0x90
	mov r3, #1
	bl createEntWithDirection
	str r6, [r0, #0x40]
	sub r4, #1
	cmp r4, #0
	bge _08021B10
_08021B36:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_08021B40: .4byte unk_814274e



	thumb_func_global sub_8021B44
sub_8021B44: @ 0x08021B44
	push {r4, r5, lr}
	add r5, r0, #0
	bl getPlayerObj
	ldr r1, [r5, #0x3c]
	ldr r4, [r1, #4]
	ldr r1, [r1, #8]
	ldr r2, [r0, #4]
	ldr r3, [r0, #8]
	mov r0, #0x80
	lsl r0, r0, #5
	add r3, r3, r0
	add r0, r4, #0
	bl angle_sub_801F98C
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	add r2, r0, #0
	ldr r4, _08021BAC
	add r0, #0x40
	lsl r0, r0, #1
	add r0, r0, r4
	mov r3, #0
	ldrsh r1, [r0, r3]
	lsl r0, r1, #2
	add r0, r0, r1
	lsl r0, r0, #6
	cmp r0, #0
	bge _08021B80
	add r0, #0xff
_08021B80:
	lsl r0, r0, #8
	lsr r3, r0, #0x10
	lsl r0, r2, #1
	add r0, r0, r4
	mov r2, #0
	ldrsh r1, [r0, r2]
	lsl r0, r1, #2
	add r0, r0, r1
	lsl r0, r0, #6
	cmp r0, #0
	bge _08021B98
	add r0, #0xff
_08021B98:
	lsl r2, r0, #8
	ldr r0, [r5, #0x3c]
	lsl r1, r3, #0x10
	asr r1, r1, #0x10
	asr r2, r2, #0x10
	bl setObjVelocity
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_08021BAC: .4byte dword_80382C8



	thumb_func_global sub_8021BB0
sub_8021BB0: @ 0x08021BB0
	add r2, r0, #0
	add r2, #0x68
	mov r1, #0
	strb r1, [r2]
	ldr r1, _08021BC0
	str r1, [r0, #0x10]
	bx lr
	.align 2, 0
_08021BC0: .4byte (sub_8021BC4+1)



	thumb_func_global sub_8021BC4
sub_8021BC4: @ 0x08021BC4
	push {r4, r5, lr}
	add r4, r0, #0
	add r1, r4, #0
	add r1, #0x68
	ldrb r0, [r1]
	add r0, #1
	strb r0, [r1]
	ldrb r3, [r1]
	add r5, r3, #0
	ldr r0, [r4, #0x3c]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	cmp r3, #9
	bhi _08021BFA
	add r0, r3, #0
	mov r1, #3
	bl modulo
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _08021C3C
	b _08021C1A
_08021BFA:
	cmp r3, #0x13
	bhi _08021C08
	mov r0, #1
	and r3, r0
	cmp r3, #0
	beq _08021C3C
	b _08021C1A
_08021C08:
	cmp r5, #0x1d
	bhi _08021C2A
	add r0, r5, #0
	mov r1, #3
	bl modulo
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08021C3C
_08021C1A:
	ldr r0, [r4, #0x3c]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	b _08021C3C
_08021C2A:
	ldr r0, [r4, #0x3c]
	ldr r1, [r0]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	mov r0, #0
	str r0, [r4, #0x10]
_08021C3C:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
	
	
	
	thumb_func_global sub_8021C44
sub_8021C44: @ 0x08021C44
	push {r4, lr}
	sub sp, #4
	add r4, r0, #0
	ldr r3, [r4, #0x3c]
	ldr r1, [r3, #4]
	asr r1, r1, #8
	ldr r2, _08021C90
	add r0, #0x67
	ldrb r0, [r0]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1c
	add r0, r0, r2
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	lsl r0, r0, #3
	add r1, r1, r0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r2, [r3, #8]
	lsl r2, r2, #8
	lsr r2, r2, #0x10
	mov r0, #0
	str r0, [sp]
	mov r0, #0x8d
	mov r3, #0
	bl createEntWithVel
	add r2, r0, #0
	add r2, #0x5c
	mov r1, #1
	strh r1, [r2]
	str r4, [r0, #0x40]
	add sp, #4
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_08021C90: .4byte dirLutMaybe



	thumb_func_global sub_8021C94
sub_8021C94: @ 0x08021C94
	ldr r1, [r0, #0x40]
	ldr r3, [r0, #0x3c]
	ldr r2, [r1, #0x3c]
	ldr r1, _08021CB4
	add r0, #0x67
	ldrb r0, [r0]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1c
	add r0, r0, r1
	mov r1, #0
	ldrsb r1, [r0, r1]
	lsl r1, r1, #0xb
	ldr r0, [r2, #4]
	add r0, r0, r1
	str r0, [r3, #4]
	bx lr
	.align 2, 0
_08021CB4: .4byte dirLutMaybe



	thumb_func_global sub_8021CB8
sub_8021CB8: @ 0x08021CB8
	push {r4, lr}
	ldr r1, [r0, #0x40]
	ldr r1, [r1, #0x3c]
	ldr r4, [r0, #0x3c]
	ldr r3, [r1, #4]
	ldr r1, [r4, #4]
	sub r2, r3, r1
	add r0, #0x67
	ldrb r1, [r0]
	mov r0, #0xf
	and r0, r1
	cmp r0, #1
	bne _08021CDE
	add r0, r2, #0
	cmp r2, #0
	bge _08021CDA
	neg r0, r2
_08021CDA:
	add r0, r3, r0
	b _08021CE8
_08021CDE:
	add r0, r2, #0
	cmp r0, #0
	bge _08021CE6
	neg r0, r0
_08021CE6:
	sub r0, r3, r0
_08021CE8:
	str r0, [r4, #4]
	pop {r4}
	pop {r0}
	bx r0
	
	
	
	thumb_func_global sub_8021CF0
sub_8021CF0: @ 0x08021CF0
	push {r4, lr}
	mov ip, r0
	mov r3, ip
	add r3, #0x6e
	ldrh r0, [r3]
	add r0, #1
	strh r0, [r3]
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #0x1e
	ble _08021D0A
	mov r0, #0x1e
	strh r0, [r3]
_08021D0A:
	mov r0, ip
	add r0, #0x5a
	ldrh r0, [r0]
	cmp r0, #0x4d
	beq _08021D1A
	cmp r0, #0x4e
	beq _08021D3E
	b _08021D58
_08021D1A:
	mov r0, ip
	ldr r2, [r0, #0x3c]
	mov r4, #0
	ldrsh r1, [r3, r4]
	lsl r1, r1, #2
	mov r4, #4
	neg r4, r4
	add r0, r4, #0
	sub r0, r0, r1
	strh r0, [r2, #0x38]
	mov r0, ip
	ldr r1, [r0, #0x3c]
	mov r2, #0
	ldrsh r0, [r3, r2]
	lsl r0, r0, #3
	add r0, #8
	strh r0, [r1, #0x3c]
	b _08021D58
_08021D3E:
	mov r4, ip
	ldr r1, [r4, #0x3c]
	mov r2, #0
	ldrsh r0, [r3, r2]
	lsl r0, r0, #2
	add r0, #4
	strh r0, [r1, #0x3a]
	ldr r1, [r4, #0x3c]
	mov r4, #0
	ldrsh r0, [r3, r4]
	lsl r0, r0, #3
	add r0, #8
	strh r0, [r1, #0x3e]
_08021D58:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
	
	
	
	thumb_func_global sub_8021D60
sub_8021D60: @ 0x08021D60
	push {r4, lr}
	add r4, r0, #0
	mov r1, #1
	bl setObjAnimation
	add r0, r4, #0
	add r0, #0x6e
	ldrh r2, [r0]
	mov r1, #0
	ldrsh r0, [r0, r1]
	cmp r0, #0x1e
	beq _08021D8A
	ldr r0, [r4, #0x3c]
	ldr r1, [r0]
	mov r0, #0x1d
	sub r0, r0, r2
	strh r0, [r1, #0xe]
	ldr r0, [r4, #0x3c]
	ldr r0, [r0]
	bl advanceSpriteAnimation
_08021D8A:
	pop {r4}
	pop {r0}
	bx r0
	
	
	
	thumb_func_global sub_8021D90
sub_8021D90: @ 0x08021D90
	push {r4, lr}
	add r3, r0, #0
	mov r0, #0x6e
	add r0, r0, r3
	mov ip, r0
	mov r1, #0
	ldrsh r0, [r0, r1]
	sub r0, #1
	cmp r0, #1
	bge _08021DA6
	mov r0, #1
_08021DA6:
	mov r2, ip
	strh r0, [r2]
	add r0, r3, #0
	add r0, #0x5a
	ldrh r0, [r0]
	cmp r0, #0x4d
	beq _08021DBA
	cmp r0, #0x4e
	beq _08021DDE
	b _08021DF8
_08021DBA:
	ldr r2, [r3, #0x3c]
	mov r4, ip
	mov r0, #0
	ldrsh r1, [r4, r0]
	sub r1, #1
	lsl r1, r1, #2
	mov r4, #4
	neg r4, r4
	add r0, r4, #0
	sub r0, r0, r1
	strh r0, [r2, #0x38]
	ldr r1, [r3, #0x3c]
	mov r2, ip
	mov r3, #0
	ldrsh r0, [r2, r3]
	lsl r0, r0, #3
	strh r0, [r1, #0x3c]
	b _08021DF8
_08021DDE:
	ldr r1, [r3, #0x3c]
	mov r4, ip
	mov r2, #0
	ldrsh r0, [r4, r2]
	lsl r0, r0, #2
	add r0, #4
	strh r0, [r1, #0x3a]
	ldr r1, [r3, #0x3c]
	mov r3, #0
	ldrsh r0, [r4, r3]
	lsl r0, r0, #3
	add r0, #8
	strh r0, [r1, #0x3e]
_08021DF8:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
	
	
	
	thumb_func_global sub_8021E00
sub_8021E00: @ 0x08021E00
	push {r4, lr}
	add r4, r0, #0
	bl ObjPlayerDiff_Y
	lsl r0, r0, #0x10
	cmp r0, #0
	bgt _08021E2E
	add r0, r4, #0
	bl objVerticalDistToPlayer
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #0x80
	bgt _08021E2E
	add r0, r4, #0
	bl objHorizontalDistToPlayer
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #0x60
	bgt _08021E2E
	mov r0, #1
	b _08021E30
_08021E2E:
	mov r0, #0
_08021E30:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	thumb_func_global sub_8021E38
sub_8021E38: @ 0x08021E38
	add r0, #0x6e
	mov r1, #0xc0
	strh r1, [r0]
	bx lr
	thumb_func_global sub_8021E40
sub_8021E40: @ 0x08021E40
	push {r4, r5, r6, lr}
	add r5, r0, #0
	ldr r0, _08021EAC
	add r6, r5, #0
	add r6, #0x67
	ldrb r2, [r6]
	lsl r2, r2, #0x1c
	lsr r2, r2, #0x1a
	add r2, r2, r0
	add r0, r5, #0
	add r0, #0x6a
	add r4, r5, #0
	add r4, #0x6e
	ldrh r1, [r4]
	ldr r3, [r2]
	mov r2, #3
	bl call_r3
	strh r0, [r4]
	ldr r1, _08021EB0
	ldrb r0, [r6]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1c
	add r0, r0, r1
	mov r1, #0
	ldrsb r1, [r0, r1]
	lsl r0, r1, #3
	add r0, r0, r1
	lsl r0, r0, #0x15
	lsr r3, r0, #0x10
	ldr r1, _08021EB4
	mov r2, #0
	ldrsh r0, [r4, r2]
	lsl r0, r0, #1
	add r0, r0, r1
	mov r2, #0
	ldrsh r1, [r0, r2]
	lsl r0, r1, #3
	add r0, r0, r1
	lsl r0, r0, #6
	cmp r0, #0
	bge _08021E96
	add r0, #0xff
_08021E96:
	lsl r2, r0, #8
	ldr r0, [r5, #0x3c]
	lsl r1, r3, #0x10
	asr r1, r1, #0x10
	asr r2, r2, #0x10
	bl setObjVelocity
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_08021EAC: .4byte off_8142408
_08021EB0: .4byte dirLutMaybe
_08021EB4: .4byte dword_80382C8
	thumb_func_global entity_is_falling
entity_is_falling: @ 0x08021EB8
	ldr r0, [r0, #0x3c]
	ldr r0, [r0, #0x1c]
	cmp r0, #0
	ble _08021EC4
	mov r0, #0
	b locret_8021EC6
_08021EC4:
	mov r0, #1
locret_8021EC6:
	bx lr
	thumb_func_global sub_8021EC8
sub_8021EC8: @ 0x08021EC8
	push {r4, r5, lr}
	add r4, r0, #0
	bl getPlayerObj
	add r5, r0, #0
	ldr r3, [r4, #0x3c]
	ldr r1, [r3, #4]
	ldr r2, [r5, #4]
	cmp r1, r2
	bgt _08021EFC
	ldrh r0, [r4, #0x18]
	lsl r0, r0, #8
	add r0, r1, r0
	cmp r2, r0
	bgt _08021EFC
	ldr r1, [r3, #8]
	ldr r2, [r5, #8]
	cmp r1, r2
	bgt _08021EFC
	ldrh r0, [r4, #0x1a]
	lsl r0, r0, #8
	add r0, r1, r0
	cmp r2, r0
	bgt _08021EFC
	mov r0, #1
	b _08021EFE
_08021EFC:
	mov r0, #0
_08021EFE:
	pop {r4, r5}
	pop {r1}
	bx r1
	
	
	






thumb_func_global sub_8021F04
sub_8021F04: @ 0x08021F04
	push {r4, r5, lr}
	add r4, r0, #0
	bl getPlayerObj
	add r5, r0, #0
	mov r0, #0x6a
	add r0, r0, r4
	mov ip, r0
	ldrh r0, [r0]
	cmp r0, #0
	bne _08021F46
	ldr r3, [r4, #0x3c]
	ldr r1, [r3, #4]
	ldr r2, [r5, #4]
	cmp r1, r2
	bgt _08021F46
	ldrh r0, [r4, #0x18]
	lsl r0, r0, #8
	add r0, r1, r0
	cmp r2, r0
	bgt _08021F46
	ldr r1, [r3, #8]
	ldr r2, [r5, #8]
	cmp r1, r2
	bgt _08021F46
	ldrh r0, [r4, #0x1a]
	lsl r0, r0, #8
	add r0, r1, r0
	cmp r2, r0
	bgt _08021F46
	mov r0, #1
	mov r1, ip
	strh r0, [r1]
_08021F46:
	add r0, r4, #0
	add r0, #0x6a
	ldrh r0, [r0]
	cmp r0, #1
	bne _08021F6A
	add r1, r4, #0
	add r1, #0x6e
	ldrh r0, [r1]
	add r0, #1
	strh r0, [r1]
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	sub r1, #0x10
	ldrh r1, [r1]
	cmp r0, r1
	blt _08021F6A
	mov r0, #1
	b _08021F6C
_08021F6A:
	mov r0, #0
_08021F6C:
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0	
	
	
	
	
	thumb_func_global sub_8021F74
sub_8021F74: @ 0x08021F74
	push {r4, r5, lr}
	add r3, r0, #0
	mov r5, #0
	mov r0, #0x66
	add r0, r0, r3
	mov ip, r0
	ldr r2, _08021FBC
	ldr r0, _08021FC0
	ldrb r4, [r0, #0x11]
	ldr r0, [r3, #8]
	ldrh r1, [r0, #0x1e]
	lsl r0, r1, #1
	add r0, r0, r1
	add r0, r4, r0
	add r0, r0, r2
	ldrb r0, [r0]
	mov r2, ip
	ldrb r1, [r2]
	lsr r0, r0, #1
	cmp r1, r0
	bhs _08021FA0
	mov r5, #1
_08021FA0:
	add r0, r3, #0
	add r0, #0x70
	mov r1, #0
	ldrsh r2, [r0, r1]
	ldr r1, _08021FC4
	lsl r0, r4, #1
	add r0, r0, r5
	add r0, r0, r1
	ldrb r0, [r0]
	cmp r2, r0
	bge _08021FC8
	mov r0, #0
	b _08021FCA
	.align 2, 0
_08021FBC: .4byte byte_814295B
_08021FC0: .4byte 0x03003D40
_08021FC4: .4byte dword_814279C
_08021FC8:
	mov r0, #1
_08021FCA:
	pop {r4, r5}
	pop {r1}
	bx r1
	
	
	
	
	thumb_func_global sub_8021FD0
sub_8021FD0: @ 0x08021FD0
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #8
	add r6, r0, #0
	bl getPlayerObj
	mov r8, r0
	mov r0, #0x6a
	add r0, r0, r6
	mov sb, r0
	ldrh r7, [r0]
	cmp r7, #0
	beq _08021FF6
	cmp r7, #1
	beq _080220B0
	b _0802216C
_08021FF6:
	ldr r0, _080220A0
	add r4, r6, #0
	add r4, #0x67
	ldrb r2, [r4]
	lsl r2, r2, #0x1c
	lsr r2, r2, #0x1a
	add r2, r2, r0
	add r0, r6, #0
	add r0, #0x6c
	add r5, r6, #0
	add r5, #0x6e
	ldrh r1, [r5]
	ldr r3, [r2]
	mov r2, #2
	bl call_r3
	strh r0, [r5]
	ldr r1, _080220A4
	ldrb r3, [r4]
	lsl r0, r3, #0x1c
	lsr r0, r0, #0x1c
	add r0, r0, r1
	mov r1, #0
	ldrsb r1, [r0, r1]
	mov r0, #0xd0
	mul r0, r1, r0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sl, r0
	ldr r1, _080220A8
	mov r2, #0
	ldrsh r0, [r5, r2]
	lsl r0, r0, #1
	add r0, r0, r1
	mov r2, #0
	ldrsh r1, [r0, r2]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #2
	add r0, r0, r1
	lsl r0, r0, #5
	cmp r0, #0
	bge _0802204E
	add r0, #0xff
_0802204E:
	lsl r0, r0, #8
	lsr r5, r0, #0x10
	mov r2, #0xf
	and r2, r3
	cmp r2, #1
	bne _0802206C
	ldr r0, [r6, #0x3c]
	mov r3, r8
	ldr r1, [r3, #4]
	mov r3, #0x80
	lsl r3, r3, #7
	add r1, r1, r3
	ldr r0, [r0, #4]
	cmp r0, r1
	bgt _08022082
_0802206C:
	cmp r2, #0
	beq _08022072
	b _0802216C
_08022072:
	ldr r0, [r6, #0x3c]
	mov r2, r8
	ldr r1, [r2, #4]
	ldr r3, _080220AC
	add r1, r1, r3
	ldr r0, [r0, #4]
	cmp r0, r1
	bge _0802216C
_08022082:
	add r1, r6, #0
	add r1, #0x6a
	mov r0, #1
	strh r0, [r1]
	ldrb r2, [r4]
	lsl r1, r2, #0x1c
	lsr r1, r1, #0x1c
	add r1, #1
	and r1, r0
	sub r0, #0x11
	and r0, r2
	orr r0, r1
	strb r0, [r4]
	b _0802216C
	.align 2, 0
_080220A0: .4byte off_8142408
_080220A4: .4byte dirLutMaybe
_080220A8: .4byte dword_80382C8
_080220AC: .4byte 0xFFFFC000
_080220B0:
	ldr r1, [r6, #0x3c]
	ldr r0, [r1, #4]
	ldr r1, [r1, #8]
	mov r4, r8
	ldr r2, [r4, #4]
	ldr r3, [r4, #8]
	mov r4, #0x80
	lsl r4, r4, #5
	add r3, r3, r4
	add r5, r6, #0
	add r5, #0x6e
	ldrh r4, [r5]
	str r4, [sp]
	mov r4, #2
	str r4, [sp, #4]
	bl sub_801FA88
	mov r4, #0
	strh r0, [r5]
	ldr r3, _08022138
	mov r0, #0
	ldrsh r1, [r5, r0]
	add r0, r1, #0
	add r0, #0x40
	lsl r0, r0, #1
	add r0, r0, r3
	mov r2, #0
	ldrsh r0, [r0, r2]
	mov r2, #0xd0
	mul r0, r2, r0
	cmp r0, #0
	bge _080220F2
	add r0, #0xff
_080220F2:
	lsl r0, r0, #8
	lsr r0, r0, #0x10
	mov sl, r0
	lsl r0, r1, #1
	add r0, r0, r3
	mov r3, #0
	ldrsh r0, [r0, r3]
	mul r0, r2, r0
	cmp r0, #0
	bge _08022108
	add r0, #0xff
_08022108:
	lsl r0, r0, #8
	lsr r5, r0, #0x10
	ldr r0, [r6, #0x3c]
	ldr r0, [r0, #8]
	mov r2, r8
	ldr r1, [r2, #8]
	sub r0, r0, r1
	lsl r0, r0, #8
	asr r0, r0, #0x10
	cmp r0, #0
	bge _08022120
	neg r0, r0
_08022120:
	cmp r0, #0xf
	bgt _0802216C
	mov r3, sb
	strh r4, [r3]
	ldr r0, [r6, #0x3c]
	ldr r0, [r0, #0x1c]
	cmp r0, #0
	blt _0802213C
	add r0, r6, #0
	add r0, #0x6c
	strh r4, [r0]
	b _08022142
	.align 2, 0
_08022138: .4byte dword_80382C8
_0802213C:
	add r0, r6, #0
	add r0, #0x6c
	strh r7, [r0]
_08022142:
	ldr r0, [r6, #0x3c]
	ldr r0, [r0, #0x18]
	cmp r0, #0
	blt _0802215E
	add r2, r6, #0
	add r2, #0x67
	ldrb r0, [r2]
	mov r1, #0x10
	neg r1, r1
	and r1, r0
	mov r0, #1
	orr r1, r0
	strb r1, [r2]
	b _0802216C
_0802215E:
	add r2, r6, #0
	add r2, #0x67
	ldrb r1, [r2]
	mov r0, #0x10
	neg r0, r0
	and r0, r1
	strb r0, [r2]
_0802216C:
	ldr r0, [r6, #0x3c]
	mov r1, sl
	lsl r4, r1, #0x10
	asr r4, r4, #0x10
	lsl r2, r5, #0x10
	asr r2, r2, #0x10
	add r1, r4, #0
	bl setObjVelocity
	cmp r4, #0
	blt _0802218C
	add r0, r6, #0
	mov r1, #1
	bl setObjDir_animFrame
	b _08022194
_0802218C:
	add r0, r6, #0
	mov r1, #0
	bl setObjDir_animFrame
_08022194:
	add sp, #8
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	thumb_func_global sub_80221A4
sub_80221A4: @ 0x080221A4
	ldr r1, [r0, #0x40]
	add r1, #0x70
	ldrh r0, [r1]
	sub r0, #1
	strh r0, [r1]
	bx lr
	
	
	
	
	thumb_func_global sub_80221B0
sub_80221B0: @ 0x080221B0
	push {r4, r5, r6, r7, lr}
	sub sp, #8
	add r6, r0, #0
	mov r5, #0
	add r7, r6, #0
	add r7, #0x70
	b _080221E4
_080221BE:
	ldrh r0, [r7]
	add r0, #1
	strh r0, [r7]
	ldr r0, _080221FC
	lsl r1, r5, #1
	add r0, r1, r0
	ldrh r2, [r0]
	ldr r0, _08022200
	add r1, r1, r0
	ldrh r3, [r1]
	str r4, [sp]
	str r4, [sp, #4]
	add r0, r6, #0
	mov r1, #0x65
	bl createChildEnt_absPos
	bl sub_8021FD0
	add r5, #1
_080221E4:
	cmp r5, #5
	bgt _080221F4
	add r0, r6, #0
	bl sub_8021F74
	add r4, r0, #0
	cmp r4, #0
	beq _080221BE
_080221F4:
	add sp, #8
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_080221FC: .4byte unk_814276C
_08022200: .4byte dword_8142778




	thumb_func_global sub_8022204
sub_8022204: @ 0x08022204
	push {r4, r5, r6, r7, lr}
	sub sp, #8
	add r6, r0, #0
	mov r5, #0
	add r7, r6, #0
	add r7, #0x70
	b _08022238
_08022212:
	ldrh r0, [r7]
	add r0, #1
	strh r0, [r7]
	ldr r0, _08022250
	lsl r1, r5, #1
	add r0, r1, r0
	ldrh r2, [r0]
	ldr r0, _08022254
	add r1, r1, r0
	ldrh r3, [r1]
	str r4, [sp]
	str r4, [sp, #4]
	add r0, r6, #0
	mov r1, #0x65
	bl createChildEnt_absPos
	bl sub_8021FD0
	add r5, #1
_08022238:
	cmp r5, #5
	bgt _08022248
	add r0, r6, #0
	bl sub_8021F74
	add r4, r0, #0
	cmp r4, #0
	beq _08022212
_08022248:
	add sp, #8
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_08022250: .4byte dword_8142784
_08022254: .4byte dword_8142790




	thumb_func_global sub_8022258
sub_8022258: @ 0x08022258
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #8
	add r5, r0, #0
	add r7, r5, #0
	add r7, #0x67
	ldrb r1, [r7]
	lsl r6, r1, #0x1c
	lsr r6, r6, #0x1c
	mov r4, #0x10
	neg r4, r4
	add r0, r4, #0
	and r0, r1
	mov r1, #1
	orr r0, r1
	strb r0, [r7]
	mov r0, #0x1d
	neg r0, r0
	mov sl, r0
	ldr r0, _08022308
	str r0, [sp]
	mov r3, #0
	mov sb, r3
	str r3, [sp, #4]
	add r0, r5, #0
	mov r1, #0x66
	mov r2, #0x14
	mov r3, sl
	bl createChildEntity
	add r2, r0, #0
	ldr r1, [r5, #0x44]
	ldr r0, _0802230C
	and r1, r0
	ldr r0, [r2, #0x44]
	ldr r3, _08022310
	mov r8, r3
	and r0, r3
	orr r0, r1
	str r0, [r2, #0x44]
	ldr r0, [r2, #0x3c]
	mov r1, #0x12
	neg r1, r1
	bl setObjAccelerationX
	ldrb r1, [r7]
	add r0, r4, #0
	and r0, r1
	strb r0, [r7]
	mov r2, #0x16
	neg r2, r2
	ldr r0, _08022314
	str r0, [sp]
	mov r0, sb
	str r0, [sp, #4]
	add r0, r5, #0
	mov r1, #0x66
	mov r3, sl
	bl createChildEntity
	add r2, r0, #0
	ldr r1, [r5, #0x44]
	ldr r0, _0802230C
	and r1, r0
	ldr r0, [r2, #0x44]
	mov r3, r8
	and r0, r3
	orr r0, r1
	str r0, [r2, #0x44]
	ldr r0, [r2, #0x3c]
	mov r1, #0x12
	bl setObjAccelerationX
	ldrb r0, [r7]
	and r4, r0
	orr r4, r6
	strb r4, [r7]
	add sp, #8
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_08022308: .4byte 0x000002AA
_0802230C: .4byte 0x07FFF800
_08022310: .4byte 0xF80007FF
_08022314: .4byte 0xFFFFFD56




	thumb_func_global sub_8022318
sub_8022318: @ 0x08022318
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #8
	add r5, r0, #0
	add r7, r5, #0
	add r7, #0x67
	ldrb r1, [r7]
	lsl r6, r1, #0x1c
	lsr r6, r6, #0x1c
	mov r4, #0x10
	neg r4, r4
	add r0, r4, #0
	and r0, r1
	mov r1, #1
	orr r0, r1
	strb r0, [r7]
	mov r0, #0x1d
	neg r0, r0
	mov sl, r0
	mov r0, #0x80
	lsl r0, r0, #3
	str r0, [sp]
	mov r3, #0
	mov sb, r3
	str r3, [sp, #4]
	add r0, r5, #0
	mov r1, #0x66
	mov r2, #0x14
	mov r3, sl
	bl createChildEntity
	add r2, r0, #0
	ldr r1, [r5, #0x44]
	ldr r0, _080223CC
	and r1, r0
	ldr r0, [r2, #0x44]
	ldr r3, _080223D0
	mov r8, r3
	and r0, r3
	orr r0, r1
	str r0, [r2, #0x44]
	ldr r0, [r2, #0x3c]
	mov r1, #0x1c
	neg r1, r1
	bl setObjAccelerationX
	ldrb r1, [r7]
	add r0, r4, #0
	and r0, r1
	strb r0, [r7]
	mov r2, #0x16
	neg r2, r2
	ldr r0, _080223D4
	str r0, [sp]
	mov r0, sb
	str r0, [sp, #4]
	add r0, r5, #0
	mov r1, #0x66
	mov r3, sl
	bl createChildEntity
	add r2, r0, #0
	ldr r1, [r5, #0x44]
	ldr r0, _080223CC
	and r1, r0
	ldr r0, [r2, #0x44]
	mov r3, r8
	and r0, r3
	orr r0, r1
	str r0, [r2, #0x44]
	ldr r0, [r2, #0x3c]
	mov r1, #0x1c
	bl setObjAccelerationX
	ldrb r0, [r7]
	and r4, r0
	orr r4, r6
	strb r4, [r7]
	add sp, #8
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_080223CC: .4byte 0x07FFF800
_080223D0: .4byte 0xF80007FF
_080223D4: .4byte 0xFFFFFC00



	thumb_func_global sub_80223D8
sub_80223D8: @ 0x080223D8
	push {r4, r5, lr}
	add r2, r0, #0
	ldr r5, [r2, #0x40]
	add r0, #0x67
	ldrb r0, [r0]
	mov r4, #0xf
	and r4, r0
	cmp r4, #1
	bne _080223F4
	ldr r0, [r2, #0x3c]
	ldr r1, [r0, #0x18]
	add r3, r0, #0
	cmp r1, #0
	ble _08022402
_080223F4:
	cmp r4, #0
	bne _08022424
	ldr r0, [r2, #0x3c]
	ldr r1, [r0, #0x18]
	add r3, r0, #0
	cmp r1, #0
	blt _08022424
_08022402:
	ldr r0, [r5, #0x3c]
	ldr r0, [r0, #8]
	ldr r1, [r3, #8]
	sub r0, r0, r1
	ldr r1, _08022420
	cmp r0, r1
	ble _0802241A
	mov r1, #0x8e
	lsl r1, r1, #1
	add r0, r3, #0
	bl setObjVelocity_Y
_0802241A:
	mov r0, #1
	b _08022426
	.align 2, 0
_08022420: .4byte 0x00001DFF
_08022424:
	mov r0, #0
_08022426:
	pop {r4, r5}
	pop {r1}
	bx r1
	
	
	
	
	thumb_func_global sub_802242C
sub_802242C: @ 0x0802242C
	push {r4, r5, lr}
	add r2, r0, #0
	ldr r4, [r2, #0x40]
	ldr r5, [r4, #0x3c]
	ldr r0, [r5]
	ldrh r3, [r0, #0xe]
	ldr r0, [r2, #0x44]
	lsl r0, r0, #5
	lsr r0, r0, #0x10
	mov r1, #0x40
	and r0, r1
	cmp r0, #0
	beq _08022478
	add r0, r4, #0
	add r0, #0x65
	ldrb r0, [r0]
	cmp r0, #9
	bne _08022456
	add r0, r3, #5
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
_08022456:
	ldr r4, [r2, #0x3c]
	ldr r1, _08022470
	add r0, r2, #0
	add r0, #0x67
	ldrb r0, [r0]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1c
	add r0, r0, r1
	mov r1, #0
	ldrsb r1, [r0, r1]
	ldr r2, _08022474
	b _0802248E
	.align 2, 0
_08022470: .4byte dirLutMaybe
_08022474: .4byte dword_81427AC
_08022478:
	ldr r4, [r2, #0x3c]
	ldr r1, _080224B8
	add r0, r2, #0
	add r0, #0x67
	ldrb r0, [r0]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1c
	add r0, r0, r1
	mov r1, #0
	ldrsb r1, [r0, r1]
	ldr r2, _080224BC
_0802248E:
	lsl r3, r3, #1
	add r0, r3, r2
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	mul r1, r0, r1
	lsl r1, r1, #8
	ldr r0, [r5, #4]
	add r0, r0, r1
	str r0, [r4, #4]
	add r2, #1
	add r3, r3, r2
	mov r1, #0
	ldrsb r1, [r3, r1]
	lsl r1, r1, #8
	ldr r0, [r5, #8]
	sub r0, r0, r1
	str r0, [r4, #8]
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_080224B8: .4byte dirLutMaybe
_080224BC: .4byte unk_81427a2




	thumb_func_global sub_80224C0
sub_80224C0: @ 0x080224C0
	push {r4, r5, lr}
	ldr r1, [r0, #0x40]
	ldr r4, [r1, #0x3c]
	ldr r1, [r4]
	ldrh r3, [r1, #0xe]
	ldr r5, [r0, #0x3c]
	ldr r1, _08022508
	add r0, #0x67
	ldrb r0, [r0]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1c
	add r0, r0, r1
	mov r1, #0
	ldrsb r1, [r0, r1]
	ldr r2, _0802250C
	lsl r3, r3, #1
	add r0, r3, r2
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	mul r1, r0, r1
	lsl r1, r1, #8
	ldr r0, [r4, #4]
	add r0, r0, r1
	str r0, [r5, #4]
	add r2, #1
	add r3, r3, r2
	mov r1, #0
	ldrsb r1, [r3, r1]
	lsl r1, r1, #8
	ldr r0, [r4, #8]
	sub r0, r0, r1
	str r0, [r5, #8]
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_08022508: .4byte dirLutMaybe
_0802250C: .4byte dword_81427C0



	thumb_func_global sub_8022510
sub_8022510: @ 0x08022510
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x10]
	cmp r0, #0
	bne _0802253E
	ldr r0, _0802254C
	ldr r1, [r4, #0x3c]
	ldr r1, [r1]
	add r1, #0x22
	ldrb r1, [r1]
	lsl r1, r1, #0x18
	asr r1, r1, #0x18
	lsl r1, r1, #5
	ldr r2, _08022550
	add r1, r1, r2
	mov r2, #0
	bl startPalAnimation
	add r1, r4, #0
	add r1, #0x6c
	strh r0, [r1]
	ldr r0, _08022554
	str r0, [r4, #0x10]
_0802253E:
	add r1, r4, #0
	add r1, #0x68
	mov r0, #0
	strb r0, [r1]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
_0802254C: .4byte unk_832458C
_08022550: .4byte 0x05000200
_08022554: .4byte (sub_8022558+1)



	thumb_func_global sub_8022558
sub_8022558: @ 0x08022558
	push {r4, r5, r6, lr}
	add r4, r0, #0
	add r5, r4, #0
	add r5, #0x68
	ldrb r0, [r5]
	add r0, #1
	mov r6, #0
	strb r0, [r5]
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0x27
	bls _0802259E
	ldr r1, _080225B8
	ldr r0, _080225BC
	str r0, [r1]
	ldr r0, [r4, #0x3c]
	ldr r0, [r0]
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	lsl r0, r0, #5
	ldr r2, _080225C0
	add r0, r0, r2
	str r0, [r1, #4]
	ldr r0, _080225C4
	str r0, [r1, #8]
	ldr r0, [r1, #8]
	add r0, r4, #0
	add r0, #0x6c
	ldrh r0, [r0]
	bl freePalFadeTaskAtIndex
	str r6, [r4, #0x10]
	strb r6, [r5]
_0802259E:
	add r1, r4, #0
	add r1, #0x6e
	mov r2, #0
	ldrsh r0, [r1, r2]
	sub r0, #2
	cmp r0, #1
	bge _080225AE
	mov r0, #1
_080225AE:
	strh r0, [r1]
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_080225B8: .4byte 0x040000D4
_080225BC: .4byte unk_8316E00
_080225C0: .4byte 0x05000200
_080225C4: .4byte 0x80000010




	thumb_func_global sub_80225C8
sub_80225C8: @ 0x080225C8
	push {r4, r5, r6, lr}
	mov ip, r0
	ldr r0, _08022628
	ldrb r3, [r0, #0x11]
	lsl r0, r3, #1
	add r0, r0, r3
	lsl r0, r0, #1
	ldr r1, _0802262C
	add r6, r0, r1
	mov r4, #0
	mov r0, ip
	add r0, #0x6e
	mov r1, #0
	ldrsh r0, [r0, r1]
	cmp r0, #0
	bne _08022620
	mov r0, ip
	add r0, #0x66
	ldrb r5, [r0]
	ldr r2, _08022630
	mov r1, ip
	ldr r0, [r1, #8]
	ldrh r1, [r0, #0x1e]
	lsl r0, r1, #1
	add r0, r0, r1
	add r0, r3, r0
	add r0, r0, r2
	ldrb r1, [r0]
	lsr r0, r1, #1
	add r0, #1
	cmp r5, r0
	bge _08022614
	lsr r0, r1, #2
	add r0, #1
	mov r4, #2
	cmp r5, r0
	blt _08022614
	mov r4, #1
_08022614:
	lsl r0, r4, #1
	add r0, r0, r6
	ldrh r1, [r0]
	mov r0, ip
	add r0, #0x6e
	strh r1, [r0]
_08022620:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_08022628: .4byte 0x03003D40
_0802262C: .4byte dword_81427CC
_08022630: .4byte byte_814295B




	thumb_func_global sub_8022634
sub_8022634: @ 0x08022634
	add r1, r0, #0
	add r1, #0x6e
	ldrh r2, [r1]
	mov r3, #0
	ldrsh r0, [r1, r3]
	cmp r0, #0
	ble _08022646
	sub r0, r2, #1
	strh r0, [r1]
_08022646:
	mov r2, #0
	ldrsh r0, [r1, r2]
	cmp r0, #0
	beq _08022652
	mov r0, #0
	b locret_8022654
_08022652:
	mov r0, #1
locret_8022654:
	bx lr
	.align 2, 0
	
	
	
	
	thumb_func_global sub_8022658
sub_8022658: @ 0x08022658
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #8
	mov r8, r0
	ldr r4, _080226CC
	ldr r6, _080226D0
	mov r5, #0x10
_08022668:
	mov r1, #0
	ldrsh r0, [r4, r1]
	lsl r0, r0, #8
	mov r2, #2
	ldrsh r1, [r4, r2]
	lsl r1, r1, #8
	mov r3, #4
	ldrsh r2, [r4, r3]
	lsl r2, r2, #8
	mov r7, #6
	ldrsh r3, [r4, r7]
	lsl r3, r3, #8
	bl angle_sub_801F98C
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	add r1, r0, #0
	add r1, #0x40
	lsl r1, r1, #1
	add r1, r1, r6
	mov r2, #0
	ldrsh r1, [r1, r2]
	lsl r1, r1, #1
	lsl r0, r0, #1
	add r0, r0, r6
	mov r3, #0
	ldrsh r0, [r0, r3]
	lsl r0, r0, #1
	ldrh r2, [r4]
	ldrh r3, [r4, #2]
	str r1, [sp]
	str r0, [sp, #4]
	mov r0, r8
	mov r1, #0x65
	bl createChildEnt_absPos
	ldrh r1, [r4, #4]
	strh r1, [r0, #0x14]
	ldrh r1, [r4, #6]
	strh r1, [r0, #0x16]
	add r4, #8
	sub r5, #1
	cmp r5, #0
	bge _08022668
	add sp, #8
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_080226CC: .4byte word_81427E0
_080226D0: .4byte dword_80382C8




	thumb_func_global sub_80226D4
sub_80226D4: @ 0x080226D4
	ldr r2, [r0, #0x3c]
	ldrh r1, [r0, #0x14]
	lsl r1, r1, #8
	str r1, [r2, #4]
	ldrh r0, [r0, #0x16]
	lsl r0, r0, #8
	str r0, [r2, #8]
	bx lr
	
	
	
	
	thumb_func_global sub_80226E4
sub_80226E4: @ 0x080226E4
	push {r4, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x3c]
	ldr r1, [r0, #4]
	asr r1, r1, #8
	sub r1, #0x14
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r2, [r0, #8]
	asr r2, r2, #8
	add r2, #0x38
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	mov r0, #0x8f
	mov r3, #0
	bl createEntWithDirection
	str r4, [r0, #0x40]
	str r0, [r4, #0x40]
	pop {r4}
	pop {r0}
	bx r0
	
	
	
	
	thumb_func_global sub_8022710
sub_8022710: @ 0x08022710
	push {r4, r5, r6, r7, lr}
	add r1, r0, #0
	ldr r0, _08022750
	add r7, r1, #0
	add r7, #0x6a
	ldrh r6, [r7]
	add r2, r6, r0
	ldrb r0, [r2]
	cmp r0, #0
	beq _0802274C
	add r0, r1, #0
	add r0, #0x66
	ldrb r5, [r0]
	ldrb r4, [r2]
	ldr r3, _08022754
	ldr r2, _08022758
	ldr r0, [r1, #8]
	ldrh r1, [r0, #0x1e]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r2, [r2, #0x11]
	add r0, r0, r2
	add r0, r0, r3
	ldrb r0, [r0]
	mul r0, r4, r0
	mov r1, #0x64
	bl divide
	cmp r5, r0
	ble _0802275C
_0802274C:
	mov r0, #0
	b _08022762
	.align 2, 0
_08022750: .4byte unk_8142868
_08022754: .4byte byte_814295B
_08022758: .4byte 0x03003D40
_0802275C:
	add r0, r6, #1
	strh r0, [r7]
	mov r0, #1
_08022762:
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	
	
	
	
	thumb_func_global sub_8022768
sub_8022768: @ 0x08022768
	push {r4, r5, r6, lr}
	mov r6, r8
	push {r6}
	ldr r4, [r0, #0x40]
	ldr r1, [r0, #0x3c]
	mov r8, r1
	ldr r6, [r4, #0x3c]
	ldr r3, _080227D4
	mov r2, #0x65
	add r2, r2, r4
	mov ip, r2
	ldrb r1, [r2]
	lsl r1, r1, #2
	add r1, r1, r3
	mov r5, #0
	ldrsh r2, [r1, r5]
	lsl r2, r2, #8
	ldr r5, _080227D8
	add r4, #0x67
	ldrb r1, [r4]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	add r1, r1, r5
	ldrb r1, [r1]
	lsl r1, r1, #0x18
	asr r1, r1, #0x18
	mul r2, r1, r2
	ldr r1, [r6, #4]
	add r1, r1, r2
	mov r2, r8
	str r1, [r2, #4]
	mov r5, ip
	ldrb r1, [r5]
	lsl r1, r1, #2
	add r3, #2
	add r1, r1, r3
	mov r3, #0
	ldrsh r2, [r1, r3]
	lsl r2, r2, #8
	ldr r1, [r6, #8]
	add r1, r1, r2
	mov r5, r8
	str r1, [r5, #8]
	ldrb r1, [r4]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	bl setObjDir_animFrame
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_080227D4: .4byte dword_814286C
_080227D8: .4byte dirLutMaybe



	thumb_func_global sub_80227DC
sub_80227DC: @ 0x080227DC
	push {r4, r5, r6, lr}
	ldr r4, [r0, #0x40]
	ldr r1, [r4, #0x3c]
	ldr r3, [r1, #4]
	asr r3, r3, #8
	ldr r5, _08022818
	ldr r2, _0802281C
	add r1, r4, #0
	add r1, #0x67
	ldrb r1, [r1]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	add r1, r1, r2
	mov r2, #0
	ldrsb r2, [r1, r2]
	ldrh r1, [r5, #0x38]
	add r6, r1, #0
	mul r6, r2, r6
	add r1, r6, #0
	add r3, r3, r1
	strh r3, [r0, #0x14]
	ldr r1, [r4, #0x3c]
	ldr r1, [r1, #8]
	asr r1, r1, #8
	ldrh r5, [r5, #0x3a]
	add r1, r1, r5
	strh r1, [r0, #0x16]
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
_08022818: .4byte dword_814286C
_0802281C: .4byte dirLutMaybe




	thumb_func_global sub_8022820
sub_8022820: @ 0x08022820
	push {r4, r5, lr}
	add r4, r0, #0
	ldr r0, [r4, #0x40]
	add r1, r0, #0
	add r1, #0x67
	ldrb r1, [r1]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	add r1, #1
	mov r5, #1
	and r1, r5
	bl setObjDir_animFrame
	add r0, r4, #0
	add r0, #0x67
	ldrb r1, [r0]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	add r1, #1
	and r1, r5
	add r0, r4, #0
	bl setObjDir_animFrame
	pop {r4, r5}
	pop {r0}
	bx r0
	
	
	
	
	thumb_func_global sub_8022854
sub_8022854: @ 0x08022854
	push {r4, r5, lr}
	add r4, r0, #0
	bl getPlayerObj
	add r3, r0, #0
	ldr r2, [r4, #0x3c]
	ldr r1, _080228A0
	add r0, r4, #0
	add r0, #0x67
	ldrb r0, [r0]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1c
	add r0, r0, r1
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	lsl r1, r0, #2
	add r1, r1, r0
	lsl r1, r1, #0xb
	ldr r0, [r2, #4]
	add r0, r0, r1
	ldr r1, [r2, #8]
	mov r2, #0xa0
	lsl r2, r2, #6
	add r1, r1, r2
	ldr r2, [r3, #4]
	ldr r3, [r3, #8]
	mov r5, #0x80
	lsl r5, r5, #5
	add r3, r3, r5
	bl angle_sub_801F98C
	add r4, #0x6e
	strh r0, [r4]
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
_080228A0: .4byte dirLutMaybe




	thumb_func_global sub_80228A4
sub_80228A4: @ 0x080228A4
	push {r4, r5, r6, r7, lr}
	sub sp, #8
	add r5, r0, #0
	ldr r0, _08022930
	ldrb r0, [r0, #0x11]
	cmp r0, #0
	bne _080228B4
	b _080229B8
_080228B4:
	mov r6, #4
	ldr r2, [r5, #0x3c]
	ldr r1, _08022934
	add r4, r5, #0
	add r4, #0x67
	ldrb r0, [r4]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1c
	add r0, r0, r1
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	lsl r1, r0, #2
	add r1, r1, r0
	lsl r1, r1, #0xb
	ldr r0, [r2, #4]
	add r0, r0, r1
	ldr r1, [r2, #8]
	mov r2, #0x94
	lsl r2, r2, #6
	add r1, r1, r2
	ldr r2, _08022938
	ldr r3, [r2]
	ldr r2, [r3, #4]
	ldr r3, [r3, #8]
	mov r7, #0x80
	lsl r7, r7, #5
	add r3, r3, r7
	bl angle_sub_801F98C
	lsl r0, r0, #0x18
	lsr r2, r0, #0x18
	ldrb r1, [r4]
	mov r0, #0xf
	and r0, r1
	cmp r0, #1
	bne _080229A0
	mov r0, #0x6e
	add r0, r0, r5
	mov ip, r0
	mov r1, #0
	ldrsh r3, [r0, r1]
	cmp r2, r3
	bge _08022952
	cmp r2, #0x40
	bhi _08022940
	add r0, r3, #0
	cmp r0, #0xbf
	ble _08022940
	add r1, r3, #4
	add r0, r1, #0
	cmp r1, #0
	bge _08022922
	ldr r4, _0802293C
	add r0, r3, r4
_08022922:
	asr r0, r0, #8
	lsl r0, r0, #8
	sub r0, r1, r0
	mov r7, ip
	strh r0, [r7]
	b _080229B8
	.align 2, 0
_08022930: .4byte 0x03003D40
_08022934: .4byte dirLutMaybe
_08022938: .4byte 0x03004750
_0802293C: .4byte 0x00000103
_08022940:
	add r2, r5, #0
	add r2, #0x6e
	mov r1, #0
	ldrsh r0, [r2, r1]
	sub r0, r0, r6
	mov r3, #0x80
	lsl r3, r3, #1
	add r1, r0, r3
	b _0802298E
_08022952:
	cmp r2, r3
	ble _080229B8
	cmp r2, #0xbf
	bls _08022984
	mov r4, ip
	mov r7, #0
	ldrsh r0, [r4, r7]
	cmp r0, #0x40
	bgt _08022984
	add r1, r3, #0
	add r1, #0xfc
	add r0, r1, #0
	cmp r1, #0
	bge _08022972
	ldr r2, _08022980
	add r0, r3, r2
_08022972:
	asr r0, r0, #8
	lsl r0, r0, #8
	sub r0, r1, r0
	mov r3, ip
	strh r0, [r3]
	b _080229B8
	.align 2, 0
_08022980: .4byte 0x000001FB
_08022984:
	add r2, r5, #0
	add r2, #0x6e
	mov r4, #0
	ldrsh r0, [r2, r4]
	add r1, r0, r6
_0802298E:
	add r0, r1, #0
	cmp r1, #0
	bge _08022996
	add r0, #0xff
_08022996:
	asr r0, r0, #8
	lsl r0, r0, #8
	sub r0, r1, r0
	strh r0, [r2]
	b _080229B8
_080229A0:
	add r1, r5, #0
	add r1, #0x6e
	mov r7, #0
	ldrsh r0, [r1, r7]
	cmp r2, r0
	bge _080229B2
	ldrh r0, [r1]
	sub r0, #4
	b _080229B6
_080229B2:
	ldrh r0, [r1]
	add r0, #4
_080229B6:
	strh r0, [r1]
_080229B8:
	ldr r3, _08022A70
	add r0, r5, #0
	add r0, #0x6e
	mov r1, #0
	ldrsh r2, [r0, r1]
	add r0, r2, #0
	add r0, #0x40
	lsl r0, r0, #1
	add r0, r0, r3
	mov r4, #0
	ldrsh r1, [r0, r4]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #7
	cmp r0, #0
	bge _080229DA
	add r0, #0xff
_080229DA:
	lsl r0, r0, #8
	lsr r4, r0, #0x10
	lsl r0, r2, #1
	add r0, r0, r3
	mov r7, #0
	ldrsh r1, [r0, r7]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #7
	cmp r0, #0
	bge _080229F2
	add r0, #0xff
_080229F2:
	lsl r0, r0, #8
	lsr r6, r0, #0x10
	lsl r0, r4, #0x10
	asr r2, r0, #0x10
	add r3, r5, #0
	add r3, #0x67
	cmp r2, #0
	ble _08022A0C
	ldrb r1, [r3]
	mov r0, #0xf
	and r0, r1
	cmp r0, #0
	beq _08022A28
_08022A0C:
	cmp r2, #0
	bge _08022A1A
	ldrb r1, [r3]
	mov r0, #0xf
	and r0, r1
	cmp r0, #1
	beq _08022A28
_08022A1A:
	add r0, r2, #0
	cmp r0, #0
	bge _08022A22
	neg r0, r0
_08022A22:
	ldr r1, _08022A74
	cmp r0, #0x7f
	bgt _08022A3E
_08022A28:
	ldr r0, _08022A74
	ldrb r1, [r3]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1c
	add r1, r1, r0
	ldrb r1, [r1]
	lsl r1, r1, #0x18
	asr r1, r1, #0x18
	lsl r1, r1, #0x17
	lsr r4, r1, #0x10
	add r1, r0, #0
_08022A3E:
	ldrb r0, [r3]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1c
	add r0, r0, r1
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
	lsl r2, r0, #2
	add r2, r2, r0
	lsl r2, r2, #3
	lsl r0, r4, #0x10
	asr r0, r0, #0x10
	str r0, [sp]
	lsl r0, r6, #0x10
	asr r0, r0, #0x10
	str r0, [sp, #4]
	add r0, r5, #0
	mov r1, #0x4f
	mov r3, #0x25
	bl createChildEntity
	add sp, #8
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
_08022A70: .4byte dword_80382C8
_08022A74: .4byte dirLutMaybe



	thumb_func_global sub_8022A78
sub_8022A78: @ 0x08022A78
	push {lr}
	mov r0, #0
	bl sub_8013A18
	pop {r0}
	bx r0
	
	
	
	
	thumb_func_global sub_8022A84
sub_8022A84: @ 0x08022A84
	push {lr}
	mov r0, #1
	bl sub_8013A18
	pop {r0}
	bx r0
	
	
	
	
	thumb_func_global sub_8022A90
sub_8022A90: @ 0x08022A90
	push {r4, lr}
	add r4, r0, #0
	bl getPlayerObj
	ldrh r2, [r4, #0x18]
	ldr r0, [r0, #4]
	lsl r0, r0, #8
	asr r1, r0, #0x10
	cmp r2, r1
	bgt _08022AB0
	add r0, r2, #0
	add r0, #8
	cmp r1, r0
	bgt _08022AB0
	mov r0, #1
	b _08022AB2
_08022AB0:
	mov r0, #0
_08022AB2:
	pop {r4}
	pop {r1}
	bx r1
