.include "asm/macros.inc"



/*	bool8 allocFont(struct Font* result, u8 font_id)
	- allocates & loads sprite tiles and palette for font
	- writes the following values to result:
	
	struct Font{
		u16 tileOffset;
		u8 palNum;
		u8 font_id;
		void* font;	//pointer to the data returned by getFontData
	};
*/
thumb_func_global allocFont
allocFont: @ 0x0802D44C
	push {r4, r5, lr}
	add r4, r0, #0
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	add r5, r1, #0
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	bl getFontData
	str r0, [r4, #4]
	ldrh r0, [r0, #8]
	bl allocateSpriteTiles
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #0
	blt _0802D4A0
	strh r0, [r4]
	ldr r1, [r4, #4]
	ldr r0, [r1, #0xc]
	ldrb r1, [r1, #0x10]
	bl LoadSpritePalette
	cmp r0, #0
	blt _0802D4A0
	strb r0, [r4, #2]
	ldr r0, [r4, #4]
	ldr r0, [r0, #4]
	ldrh r1, [r4]
	lsl r1, r1, #5
	ldr r2, =0x06010000
	add r1, r1, r2
	bl Bios_LZ77_16_2
	strb r5, [r4, #3]
	mov r0, #1
	b _0802D4A2
	.align 2, 0
.pool
_0802D4A0:
	mov r0, #0
_0802D4A2:
	pop {r4, r5}
	pop {r1}
	bx r1


/* void* getFontData(u8 language, u8 font_id)
	-returns pointer to the specified font
*/
thumb_func_global getFontData
getFontData: @ 0x0802D4A8
	add r2, r0, #0
	lsl r2, r2, #0x18
	lsr r2, r2, #0x18
	lsl r1, r1, #0x18
	ldr r0, =fontData
	lsr r1, r1, #0x16
	add r1, r1, r0
	ldr r0, [r1]
	lsl r1, r2, #2
	add r1, r1, r2
	lsl r1, r1, #2
	add r0, r0, r1
	bx lr
	.align 2, 0
.pool


/*	void bufferString(u16* string, void* unk, u8 charBufOffset, u8 unk1, u8 unk2)
	- creates OAMs for each character and displays them
*/
thumb_func_global bufferString
bufferString: @ 0x0802D4C8
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x1c
	add r4, r0, #0
	mov r8, r1
	ldr r0, [sp, #0x3c]
	lsl r2, r2, #0x18
	lsr r5, r2, #0x18
	lsl r3, r3, #0x18
	lsr r3, r3, #0x18
	str r3, [sp, #0x10]
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	str r0, [sp, #0x14]
	add r7, r4, #0
	mov r2, #0
	b _0802D500
_0802D4F0:
	cmp r1, #0xfe
	bne _0802D4F8
	add r7, #2
	b _0802D500
_0802D4F8:
	add r7, #2
	add r0, r2, #1
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
_0802D500:
	ldrb r1, [r7]
	add r0, r1, #0
	cmp r0, #0xff
	bne _0802D4F0
	add r7, r4, #0
	mov r0, #0
	str r0, [sp, #0x18]
	mov sb, r0
	ldrb r1, [r7]
	add r0, r1, #0
	cmp r0, #0xff
	beq _0802D5C4
	add r5, r5, r2
	mov sl, r5
_0802D51C:
	cmp r1, #0xfe
	bne _0802D52E
	ldrb r0, [r7, #1]
	add r0, sb
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sb, r0
	add r7, #2
	b _0802D5BC
_0802D52E:
	mov r1, r8
	ldr r0, [r1, #4]
	ldr r2, [r0]
	ldrb r1, [r7]
	lsl r0, r1, #2
	add r2, r2, r0
	ldr r6, [r2]
	mov r0, r8
	bl getCharYOffset
	add r2, r0, #0
	lsl r2, r2, #0x10
	asr r2, r2, #0x10
	mov r0, sl
	sub r0, #1
	ldr r3, [sp, #0x18]
	sub r0, r0, r3
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	mov r4, sb
	lsl r1, r4, #0x10
	asr r1, r1, #0x10
	ldrh r3, [r6]
	mov r5, r8
	ldrh r5, [r5]
	add r3, r3, r5
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	mov r5, r8
	ldrb r4, [r5, #2]
	str r4, [sp]
	ldr r5, [sp, #0x10]
	lsl r4, r5, #8
	str r4, [sp, #4]
	ldr r5, =byte_8209AC8
	ldrb r4, [r6, #2]
	lsl r4, r4, #1
	add r4, r4, r5
	ldrb r4, [r4]
	str r4, [sp, #8]
	mov r4, #0
	str r4, [sp, #0xc]
	bl createSimpleSprite
	ldr r0, =0x030037D0
	ldr r4, [sp, #0x18]
	add r4, #1
	mov r1, sl
	sub r3, r1, r4
	lsl r3, r3, #4
	add r3, r3, r0
	mov r0, #3
	ldr r2, [sp, #0x14]
	and r2, r0
	lsl r2, r2, #2
	ldrb r0, [r3, #1]
	mov r5, #0xd
	neg r5, r5
	add r1, r5, #0
	and r0, r1
	orr r0, r2
	strb r0, [r3, #1]
	ldrb r0, [r7, #1]
	add r0, sb
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sb, r0
	add r7, #2
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	str r4, [sp, #0x18]
_0802D5BC:
	ldrb r1, [r7]
	add r0, r1, #0
	cmp r0, #0xff
	bne _0802D51C
_0802D5C4:
	add sp, #0x1c
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

/*	void setStringPos(u16* string, u16 x, u16 y, u8 charBufOffset, void* unknown)	*/
thumb_func_global setStringPos
setStringPos: @ 0x0802D5DC
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	add r5, r0, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	mov r8, r2
	lsl r3, r3, #0x18
	lsr r6, r3, #0x18
	add r4, r5, #0
	mov r3, #0
	ldrb r2, [r4]
	add r0, r2, #0
	ldr r7, =0x030037C0
	cmp r0, #0xff
	beq _0802D61C
_0802D600:
	cmp r2, #0xfe
	bne _0802D60C
	add r4, #2
	b _0802D614
	.align 2, 0
.pool
_0802D60C:
	add r4, #2
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
_0802D614:
	ldrb r2, [r4]
	add r0, r2, #0
	cmp r0, #0xff
	bne _0802D600
_0802D61C:
	add r4, r5, #0
	add r0, r6, r3
	lsl r0, r0, #4
	add r6, r0, r7
	add r5, r1, #0
	b _0802D664
_0802D628:
	cmp r1, #0xfe
	bne _0802D638
	ldrb r0, [r4, #1]
	add r0, r5, r0
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	add r4, #2
	b _0802D664
_0802D638:
	ldr r1, =0x000001FF
	add r0, r1, #0
	add r2, r5, #0
	and r2, r0
	ldrh r0, [r6, #2]
	ldr r3, =0xFFFFFE00
	add r1, r3, #0
	and r0, r1
	orr r0, r2
	strh r0, [r6, #2]
	ldrb r1, [r4]
	ldr r0, [sp, #0x18]
	bl getCharYOffset
	add r0, r8
	strb r0, [r6]
	ldrb r0, [r4, #1]
	add r0, r5, r0
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	add r4, #2
	sub r6, #0x10
_0802D664:
	ldrb r1, [r4]
	add r0, r1, #0
	cmp r0, #0xff
	bne _0802D628
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

/*void showString(u16* string, u8 charBufOffset)*/
thumb_func_global showString
showString: @ 0x0802D680
	push {lr}
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	mov r2, #1
	bl setStringVisibility
	pop {r0}
	bx r0

/*void hideString(u16* string, u8 charBufOffset)*/
thumb_func_global hideString
hideString: @ 0x0802D690
	push {lr}
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	mov r2, #0
	bl setStringVisibility
	pop {r0}
	bx r0

/*void setStringVisibility(u16* string, u8 charBufOffset, u8 isVisible)*/
thumb_func_global setStringVisibility
setStringVisibility: @ 0x0802D6A0
	push {r4, lr}
	lsl r1, r1, #0x18
	lsl r2, r2, #0x18
	lsr r4, r2, #0x18
	add r2, r0, #0
	lsr r1, r1, #0x14
	ldr r0, =0x030037D0
	add r1, r1, r0
	b _0802D6C6
	.align 2, 0
.pool
_0802D6B8:
	cmp r3, #0xfe
	bne _0802D6C0
	add r2, #2
	b _0802D6C6
_0802D6C0:
	strb r4, [r1, #0xc]
	add r2, #2
	add r1, #0x10
_0802D6C6:
	ldrb r3, [r2]
	add r0, r3, #0
	cmp r0, #0xff
	bne _0802D6B8
	pop {r4}
	pop {r0}
	bx r0


/*void setStringColor(u16* string, void* unknown, u8 charBufOffset, u8 palNum)
	-sets all characters of a string to use the same palette number
*/
thumb_func_global setStringColor
setStringColor: @ 0x0802D6D4
	push {r4, r5, r6, r7, lr}
	add r6, r1, #0
	lsl r2, r2, #0x18
	lsl r3, r3, #0x18
	lsr r5, r3, #0x18
	add r3, r0, #0
	lsr r2, r2, #0x14
	ldr r0, =0x030037D0
	add r4, r2, r0
	ldrb r1, [r3]
	add r0, r1, #0
	cmp r0, #0xff
	beq _0802D718
	mov r7, #0xf
_0802D6F0:
	cmp r1, #0xfe
	bne _0802D6FC
	add r3, #2
	b _0802D710
	.align 2, 0
.pool
_0802D6FC:
	ldrb r0, [r6, #2]
	add r0, r0, r5
	lsl r0, r0, #4
	ldrb r2, [r4, #5]
	add r1, r7, #0
	and r1, r2
	orr r1, r0
	strb r1, [r4, #5]
	add r3, #2
	add r4, #0x10
_0802D710:
	ldrb r1, [r3]
	add r0, r1, #0
	cmp r0, #0xff
	bne _0802D6F0
_0802D718:
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0


/*u16 str_getX_ofFirstChar(u16 *string, u8 charBufOffset)*/
thumb_func_global str_getX_ofFirstChar
str_getX_ofFirstChar: @ 0x0802D720
	push {r4, r5, lr}
	lsl r1, r1, #0x18
	lsr r4, r1, #0x18
	add r3, r0, #0
	mov r1, #0
	ldrb r2, [r3]
	add r0, r2, #0
	ldr r5, =0x030037C0
	cmp r0, #0xff
	beq _0802D750
_0802D734:
	cmp r2, #0xfe
	bne _0802D740
	add r3, #2
	b _0802D748
	.align 2, 0
.pool
_0802D740:
	add r3, #2
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
_0802D748:
	ldrb r2, [r3]
	add r0, r2, #0
	cmp r0, #0xff
	bne _0802D734
_0802D750:
	add r0, r4, r1
	lsl r0, r0, #4
	add r0, r0, r5
	ldrh r0, [r0, #2]
	lsl r0, r0, #0x17
	lsr r0, r0, #0x17
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0


/*int str_get_last_width_OamX_sum(u16* string, u8 charBufOffset)
	- (240-result) seems to be the x-position where the string must be moved to center it on the screen.
*/
thumb_func_global str_get_last_width_OamX_sum
str_get_last_width_OamX_sum: @ 0x0802D764
	push {r4, lr}
	lsl r1, r1, #0x18
	lsr r3, r1, #0x18
	add r1, r0, #0
	ldrb r2, [r1]
	add r0, r2, #0
	ldr r4, =0x030037D0
	cmp r0, #0xff
	beq _0802D780
_0802D776:
	add r1, #2
	ldrb r2, [r1]
	add r0, r2, #0
	cmp r0, #0xff
	bne _0802D776
_0802D780:
	lsl r0, r3, #4
	add r0, r0, r4
	ldrh r0, [r0, #2]
	lsl r0, r0, #0x17
	sub r1, #1
	lsr r0, r0, #0x17
	ldrb r1, [r1]
	add r0, r0, r1
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool


/*int strlen_noWhitespaces(u16* string)*/
thumb_func_global strlen_noWhitespaces
strlen_noWhitespaces: @ 0x0802D79C
	add r2, r0, #0
	mov r3, #0
	b _0802D7B2
_0802D7A2:
	cmp r1, #0xfe
	bne _0802D7AA
	add r2, #2
	b _0802D7B2
_0802D7AA:
	add r2, #2
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
_0802D7B2:
	ldrb r1, [r2]
	add r0, r1, #0
	cmp r0, #0xff
	bne _0802D7A2
	add r0, r3, #0
	bx lr
	.align 2, 0


/*s16 getCharYOffset(void* unk, u8 character)
	-returns character specific yOffset, used for special characters that are taller than usual
*/
thumb_func_global getCharYOffset
getCharYOffset: @ 0x0802D7C0
	add r2, r0, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	cmp r1, #0x20
	bls _0802D7E0
	cmp r1, #0x2d
	bhi _0802D7E0
	ldr r0, =charYLUT
	ldrb r1, [r2, #3]
	lsl r1, r1, #1
	add r1, r1, r0
	mov r2, #0
	ldrsh r0, [r1, r2]
	b _0802D7E2
	.align 2, 0
.pool
_0802D7E0:
	mov r0, #0
_0802D7E2:
	bx lr
	
