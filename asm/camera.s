.include "asm/macros.inc"


@complete


thumb_func_global setCameraState
setCameraState: @ 0x08012E88
	push {r4, r5, r6, lr}
	sub sp, #8
	add r5, r0, #0
	add r6, r1, #0
	add r3, r2, #0
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	lsl r6, r6, #0x10
	lsr r6, r6, #0x10
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r4, =0x03004720
	ldr r2, =0x01000018
	mov r0, sp
	add r1, r4, #0
	str r3, [sp, #4]
	bl Bios_memcpy
	strh r5, [r4]
	strh r6, [r4, #2]
	ldr r3, [sp, #4]
	strh r3, [r4, #8]
	strh r5, [r4, #0x1c]
	strh r6, [r4, #0x1e]
	add sp, #8
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global runCameraFunc
runCameraFunc: @ 0x08012ED0
	push {lr}
	ldr r2, =0x03004720
	ldrh r3, [r2]
	strh r3, [r2, #0x1c]
	ldrh r3, [r2, #2]
	strh r3, [r2, #0x1e]
	ldr r3, =cameraFuncTable
	ldrh r2, [r2, #8]
	lsl r2, r2, #2
	add r2, r2, r3
	ldr r2, [r2]
	bl call_r2
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	pop {r1}
	bx r1
	.align 2, 0
.pool



	thumb_func_global cameraFunc2
cameraFunc2: @ 0x08012EFC
	ldr r2, =0x03004720
	ldrh r1, [r2, #0x2c]
	mov r0, #0x80
	lsl r0, r0, #8
	and r0, r1
	cmp r0, #0
	beq _08012F10
	ldrh r0, [r2, #0x10]
	add r0, #1
	strh r0, [r2, #0x10]
_08012F10:
	mov r0, #1
	bx lr
	.align 2, 0
.pool
	
	
	
	thumb_func_global camera_playerControlled
camera_playerControlled: @ 0x08012F18
	push {r4, r5, r6, lr}
	add r5, r0, #0
	add r6, r1, #0
	ldr r0, =0x03004720
	ldrh r3, [r0]
	ldrh r4, [r0, #2]
	ldr r0, =0x03003D40
	ldrb r0, [r0, #0x10]
	cmp r0, #0
	beq _08012F3C
	bl removeCameraPlayerControl
	b _08012FBE
	.align 2, 0
.pool
_08012F3C:
	ldr r1, =0x03002080
	ldrh r2, [r1, #0xe]
	mov r0, #0xf0
	and r0, r2
	cmp r0, #0
	bne _08012F64
	ldr r0, =cameraFuncTable
	ldr r2, [r0]
	add r0, r5, #0
	add r1, r6, #0
	bl call_r2
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	b _08012FC0
	.align 2, 0
.pool
_08012F64:
	mov r0, #0xc0
	and r0, r2
	cmp r0, #0
	beq _08012F7A
	mov r0, #0x40
	and r0, r2
	cmp r0, #0
	beq _08012F78
	add r4, #2
	b _08012F7A
_08012F78:
	sub r4, #2
_08012F7A:
	ldrh r1, [r1, #0xe]
	mov r0, #0x30
	and r0, r1
	cmp r0, #0
	beq _08012F92
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	beq _08012F90
	add r3, #3
	b _08012F92
_08012F90:
	sub r3, #3
_08012F92:
	asr r0, r5, #8
	add r1, r0, #0
	sub r1, #0xe8
	cmp r3, r1
	bge _08012F9E
	add r3, r1, #0
_08012F9E:
	sub r0, #8
	cmp r3, r0
	ble _08012FA6
	add r3, r0, #0
_08012FA6:
	asr r1, r6, #8
	cmp r4, r1
	bge _08012FAE
	add r4, r1, #0
_08012FAE:
	add r1, #0x9f
	cmp r4, r1
	ble _08012FB6
	add r4, r1, #0
_08012FB6:
	add r0, r3, #0
	add r1, r4, #0
	bl setCameraPos
_08012FBE:
	mov r0, #1
_08012FC0:
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	thumb_func_global cameraFunc1
cameraFunc1: @ 0x08012FC8
	push {r4, r5, r6, lr}
	ldr r0, =0x03003D40
	ldrb r0, [r0, #1]
	lsl r0, r0, #3
	ldr r1, =dword_803CDA0
	add r4, r0, r1
	ldr r0, =0x03004720
	ldrh r1, [r4]
	add r2, r0, #0
	ldrh r0, [r2]
	cmp r1, r0
	bhi _08012FF2
	ldrh r3, [r2]
	mov r1, #4
	ldrsh r0, [r2, r1]
	add r0, r3, r0
	ldrh r1, [r4]
	cmp r0, r1
	ble _08013000
	cmp r1, r3
	blo _08013010
_08012FF2:
	mov r3, #4
	ldrsh r0, [r2, r3]
	ldrh r6, [r2]
	add r0, r0, r6
	ldrh r1, [r4]
	cmp r0, r1
	blt _08013010
_08013000:
	ldrh r0, [r4]
	b _08013016
	.align 2, 0
.pool
_08013010:
	ldrh r0, [r2, #4]
	ldrh r3, [r2]
	add r0, r0, r3
_08013016:
	strh r0, [r2]
	ldrh r0, [r4, #2]
	add r5, r0, #0
	ldrh r1, [r4, #2]
	ldrh r6, [r2, #2]
	cmp r5, r6
	bhi _08013034
	ldrh r3, [r2, #2]
	mov r6, #6
	ldrsh r0, [r2, r6]
	add r0, r3, r0
	cmp r0, r1
	ble _08013040
	cmp r1, r3
	blo _08013044
_08013034:
	mov r3, #6
	ldrsh r0, [r2, r3]
	ldrh r6, [r2, #2]
	add r0, r0, r6
	cmp r0, r1
	blt _08013044
_08013040:
	strh r5, [r2, #2]
	b _0801304C
_08013044:
	ldrh r0, [r2, #6]
	ldrh r1, [r2, #2]
	add r0, r0, r1
	strh r0, [r2, #2]
_0801304C:
	ldr r1, [r2]
	ldr r0, [r4]
	cmp r1, r0
	bne _0801305C
	ldrh r0, [r4, #4]
	strh r0, [r2, #8]
	bl sub_8010DB8
_0801305C:
	mov r0, #1
	pop {r4, r5, r6}
	pop {r1}
	bx r1



thumb_func_global sub_08013064
sub_08013064: @ 0x08013064
	push {r4, r5, r6, lr}
	ldr r0, =0x03003D40
	ldrb r0, [r0, #1]
	lsl r0, r0, #3
	ldr r1, =dword_803CDA0
	add r5, r0, r1
	ldr r4, =0x03004720
	mov r0, #0
	mov r6, #1
	strh r6, [r4, #8]
	strh r0, [r4, #0x10]
	ldrh r0, [r5]
	ldrh r1, [r4]
	sub r0, r0, r1
	mov r1, #0x3c
	bl Bios_Div
	strh r0, [r4, #4]
	ldrh r0, [r5, #2]
	ldrh r1, [r4, #2]
	sub r0, r0, r1
	mov r1, #0x3c
	bl Bios_Div
	strh r0, [r4, #6]
	ldrh r1, [r4, #4]
	mov r2, #4
	ldrsh r0, [r4, r2]
	cmp r0, #0
	bne _080130C2
	ldrh r0, [r5]
	ldrh r3, [r4]
	cmp r0, r3
	bhs _080130C0
	ldr r0, =0x0000FFFF
	strh r0, [r4, #4]
	b _080130C2
	.align 2, 0
.pool
_080130C0:
	strh r6, [r4, #4]
_080130C2:
	ldr r1, =0x03004720
	ldrh r2, [r1, #6]
	mov r3, #6
	ldrsh r0, [r1, r3]
	cmp r0, #0
	bne _080130E8
	ldrh r0, [r5, #2]
	ldrh r3, [r1, #2]
	cmp r0, r3
	bhs _080130E4
	ldr r0, =0x0000FFFF
	b _080130E6
	.align 2, 0
.pool
_080130E4:
	mov r0, #1
_080130E6:
	strh r0, [r1, #6]
_080130E8:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
	
	
	

thumb_func_global camera_normal
camera_normal: @ 0x080130F0
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	add r5, r0, #0
	add r7, r1, #0
	ldr r1, =0x03004720
	ldr r0, [r1, #0x14]
	sub r2, r5, r0
	mov r8, r1
	cmp r2, #0
	bge _08013114
	neg r0, r2
	asr r0, r0, #8
	neg r2, r0
	b _08013116
	.align 2, 0
.pool
_08013114:
	asr r2, r2, #8
_08013116:
	asr r3, r5, #8
	cmp r2, #0
	beq _0801312C
	ldr r0, =0x03004750
	ldrh r0, [r0, #4]
	cmp r0, #0
	beq _0801312C
	ldr r0, =0x03003D40
	ldrb r0, [r0, #0x10]
	cmp r0, #0
	beq _08013180
_0801312C:
	mov r0, r8
	ldrh r1, [r0]
	add r0, r1, #0
	add r0, #0xf0
	cmp r3, r0
	bge _0801313C
	cmp r3, r1
	bge _08013150
_0801313C:
	mov r2, #2
	cmp r3, r1
	bge _080131E6
	sub r2, #4
	b _080131E6
	.align 2, 0
.pool
_08013150:
	sub r0, r3, r1
	sub r0, #0x78
	lsl r0, r0, #7
	mov r1, #0xf0
	bl Bios_Div
	mov r1, #0xff
	and r1, r0
	ldr r0, =dword_80382C8
	lsl r1, r1, #1
	add r1, r1, r0
	mov r0, #0
	ldrsh r2, [r1, r0]
	lsl r2, r2, #1
	cmp r2, #0
	bge _0801317C
	neg r0, r2
	asr r0, r0, #8
	neg r2, r0
	b _080131E6
	.align 2, 0
.pool
_0801317C:
	asr r2, r2, #8
	b _080131E6
_08013180:
	ldr r1, =0x0000FFFF
	cmp r2, #0
	ble _08013188
	mov r1, #1
_08013188:
	ldr r0, =0x03004720
	ldrh r2, [r0]
	lsl r2, r2, #8
	sub r2, r5, r2
	lsl r0, r1, #0x10
	asr r0, r0, #0x10
	mov r1, #0x32
	mul r1, r0, r1
	mov r0, #0x78
	sub r0, r0, r1
	lsl r0, r0, #8
	sub r4, r2, r0
	cmp r4, #0
	bge _080131B0
	neg r0, r4
	b _080131B2
	.align 2, 0
.pool
_080131B0:
	add r0, r4, #0
_080131B2:
	bl Bios_Sqrt
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	lsl r0, r2, #1
	add r0, r0, r2
	lsl r2, r0, #1
	asr r2, r2, #8
	cmp r4, #0
	bge _080131C8
	neg r2, r2
_080131C8:
	ldr r1, =0x03004720
	mov r8, r1
	cmp r2, #0
	beq _080131E6
	ldr r0, [r1, #0x20]
	cmp r0, r2
	beq _080131E6
	cmp r0, r2
	ble _080131E4
	sub r2, r0, #1
	b _080131E6
	.align 2, 0
.pool
_080131E4:
	add r2, r0, #1
_080131E6:
	mov r1, r8
	ldrh r0, [r1]
	add r6, r0, r2
	str r2, [r1, #0x20]
	asr r3, r7, #8
	ldrh r1, [r1, #2]
	cmp r3, r1
	bge _080131FE
	add r0, r1, #0
	sub r0, #0xa0
	cmp r3, r0
	bgt _08013208
_080131FE:
	mov r2, #7
	cmp r3, r1
	bge _0801323A
	sub r2, #0xe
	b _0801323A
_08013208:
	sub r0, r1, r3
	add r0, #0x50
	lsl r0, r0, #7
	mov r1, #0xa0
	bl Bios_Div
	mov r1, #0xff
	and r1, r0
	ldr r0, =dword_80382C8
	lsl r1, r1, #1
	add r1, r1, r0
	mov r0, #0
	ldrsh r2, [r1, r0]
	lsl r0, r2, #1
	add r0, r0, r2
	lsl r2, r0, #1
	cmp r2, #0
	bge _08013238
	neg r0, r2
	asr r0, r0, #8
	neg r2, r0
	b _0801323A
	.align 2, 0
.pool
_08013238:
	asr r2, r2, #8
_0801323A:
	mov r4, r8
	ldrh r1, [r4, #2]
	add r1, r1, r2
	str r2, [r4, #0x24]
	str r5, [r4, #0x14]
	str r7, [r4, #0x18]
	add r0, r6, #0
	bl setCameraPos
	ldrh r3, [r4, #0x2c]
	mov r0, #0x80
	lsl r0, r0, #8
	and r0, r3
	cmp r0, #0
	beq _0801328C
	ldrh r0, [r4, #0x10]
	add r0, #1
	mov r1, #0
	strh r0, [r4, #0x10]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0x1f
	bls _0801326E
	strh r1, [r4, #0x10]
	strh r1, [r4, #0x2c]
	b _0801328C
_0801326E:
	ldr r2, =dword_803CDD0
	mov r0, r8
	ldrh r1, [r0, #0x10]
	lsl r1, r1, #1
	mov r0, #1
	and r0, r3
	lsl r0, r0, #6
	add r1, r1, r0
	add r1, r1, r2
	ldrh r0, [r1]
	mov r1, r8
	ldrh r1, [r1, #2]
	add r0, r0, r1
	mov r2, r8
	strh r0, [r2, #2]
_0801328C:
	mov r0, #1
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool
	thumb_func_global cameraFunc4
cameraFunc4: @ 0x0801329C
	push {r4, r5, r6, lr}
	add r6, r0, #0
	add r5, r1, #0
	ldr r1, =0x03004720
	ldr r0, [r1, #0x14]
	sub r2, r6, r0
	asr r3, r6, #8
	add r4, r1, #0
	cmp r2, #0
	beq _080132B8
	ldr r0, =0x03004750
	ldrh r0, [r0, #4]
	cmp r0, #0
	bne _08013308
_080132B8:
	ldrh r2, [r4]
	add r0, r2, #0
	add r0, #0xf0
	cmp r3, r0
	bge _080132C6
	cmp r3, r2
	bge _080132D8
_080132C6:
	mov r1, #2
	cmp r3, r2
	bge _08013366
	sub r1, #4
	b _08013366
	.align 2, 0
.pool
_080132D8:
	sub r0, r3, r2
	sub r0, #0x78
	lsl r0, r0, #7
	mov r1, #0xf0
	bl Bios_Div
	mov r1, #0xff
	and r1, r0
	ldr r0, =dword_80382C8
	lsl r1, r1, #1
	add r1, r1, r0
	mov r0, #0
	ldrsh r1, [r1, r0]
	lsl r1, r1, #1
	cmp r1, #0
	bge _08013304
	neg r0, r1
	asr r0, r0, #8
	neg r1, r0
	b _08013366
	.align 2, 0
.pool
_08013304:
	asr r1, r1, #8
	b _08013366
_08013308:
	ldr r0, =0x0000FFFF
	cmp r2, #0
	ble _08013310
	mov r0, #1
_08013310:
	ldrh r2, [r4]
	lsl r2, r2, #8
	sub r2, r6, r2
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	mov r1, #0x32
	mul r1, r0, r1
	mov r0, #0x78
	sub r0, r0, r1
	lsl r0, r0, #8
	sub r4, r2, r0
	cmp r4, #0
	bge _08013334
	neg r0, r4
	b _08013336
	.align 2, 0
.pool
_08013334:
	add r0, r4, #0
_08013336:
	bl Bios_Sqrt
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r1, r0, #1
	asr r1, r1, #8
	cmp r4, #0
	bge _0801334C
	neg r1, r1
_0801334C:
	ldr r4, =0x03004720
	cmp r1, #0
	beq _08013366
	ldr r0, [r4, #0x20]
	cmp r0, r1
	beq _08013366
	cmp r0, r1
	ble _08013364
	sub r1, r0, #1
	b _08013366
	.align 2, 0
.pool
_08013364:
	add r1, r0, #1
_08013366:
	ldrh r0, [r4]
	add r3, r0, r1
	str r1, [r4, #0x20]
	ldr r0, =0x03004EE0
	mov r1, #0x92
	lsl r1, r1, #1
	add r0, r0, r1
	ldr r0, [r0]
	ldr r0, [r0, #8]
	cmp r5, r0
	ble _08013388
	add r0, r5, r0
	asr r0, r0, #9
	add r0, #0x5c
	b _0801338E
	.align 2, 0
.pool
_08013388:
	add r0, r5, r0
	asr r0, r0, #9
	add r0, #0x4e
_0801338E:
	ldrh r2, [r4, #2]
	add r1, r0, #0
	cmp r1, r2
	beq _0801339E
	sub r1, r2, #1
	cmp r0, r2
	ble _0801339E
	add r1, r2, #1
_0801339E:
	asr r2, r5, #8
	add r0, r2, #0
	add r0, #0x9c
	cmp r1, r0
	ble _080133AA
	sub r1, #1
_080133AA:
	add r0, r2, #0
	add r0, #0x24
	cmp r1, r0
	bge _080133B4
	add r1, #1
_080133B4:
	str r6, [r4, #0x14]
	str r5, [r4, #0x18]
	add r0, r3, #0
	bl setCameraPos
	mov r0, #1
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0




thumb_func_global unused_sub_80133C8
unused_sub_80133C8: @ 0x080133C8
	push {r4, r5, r6, lr}
	asr r3, r0, #8
	asr r4, r1, #8
	ldr r0, =0x03004720
	ldrh r2, [r0]
	add r1, r2, #0
	add r1, #0xf0
	add r6, r0, #0
	cmp r3, r1
	bge _080133E0
	cmp r3, r2
	bge _080133F0
_080133E0:
	mov r1, #6
	cmp r3, r2
	bge _08013422
	sub r1, #0xc
	b _08013422
	.align 2, 0
.pool
_080133F0:
	sub r0, r3, r2
	sub r0, #0x78
	lsl r0, r0, #7
	mov r1, #0xf0
	bl Bios_Div
	mov r1, #0xff
	and r1, r0
	ldr r0, =dword_80382C8
	lsl r1, r1, #1
	add r1, r1, r0
	mov r0, #0
	ldrsh r1, [r1, r0]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r1, r0, #1
	cmp r1, #0
	bge _08013420
	neg r0,r1
	asr r0, r0, #8
	neg r1, r0
	b _08013422
	.align 2, 0
.pool
_08013420:
	asr r1, r1, #8
_08013422:
	ldrh r0, [r6]
	add r5, r0, r1
	ldrh r1, [r6, #2]
	cmp r4, r1
	bge _08013434
	add r0, r1, #0
	sub r0, #0xa0
	cmp r4, r0
	bgt _0801343E
_08013434:
	mov r2, #6
	cmp r4, r1
	bge _08013472
	sub r2, #0xc
	b _08013472
_0801343E:
	sub r0, r1, r4
	add r0, #0x50
	lsl r0, r0, #7
	mov r1, #0xa0
	bl Bios_Div
	mov r1, #0xff
	and r1, r0
	ldr r0, =dword_80382C8
	lsl r1, r1, #1
	add r1, r1, r0
	mov r0, #0
	ldrsh r2, [r1, r0]
	lsl r0, r2, #1
	add r0, r0, r2
	lsl r2, r0, #1
	cmp r2, #0
	bge _08013470
	neg r0, r2
	asr r0, r0, #8
	neg r2, r0
	b _08013472
	.align 2, 0
.pool
_08013470:
	asr r2, r2, #8
_08013472:
	ldrh r1, [r6, #2]
	add r1, r1, r2
	add r0, r5, #0
	bl setCameraPos
	mov r0, #1
	pop {r4, r5, r6}
	pop {r1}
	bx r1

thumb_func_global unused_sub_8013484
unused_sub_8013484: @ 0x08013484
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	add r5, r0, #0
	add r6, r1, #0
	ldr r1, =0x03004720
	ldr r0, [r1, #0x14]
	sub r4, r5, r0
	ldr r0, [r1, #0x18]
	sub r0, r6, r0
	mov sb, r0
	asr r3, r5, #8
	ldrh r2, [r1]
	add r0, r2, #0
	add r0, #0xf0
	mov r8, r1
	cmp r3, r0
	bge _080134AE
	cmp r3, r2
	bge _080134BC
_080134AE:
	mov r1, #6
	cmp r3, r2
	bge _08013512
	sub r1, #0xc
	b _08013512
	.align 2, 0
.pool
_080134BC:
	cmp r4, #0
	bne _080134C6
	sub r0, r3, r2
	sub r0, #0x78
	b _080134D4
_080134C6:
	cmp r4, #0
	ble _080134D0
	sub r0, r3, r2
	sub r0, #0x5a
	b _080134D4
_080134D0:
	sub r0, r3, r2
	sub r0, #0x96
_080134D4:
	lsl r0, r0, #7
	mov r1, #0xf0
	bl Bios_Div
	add r1, r0, #0
	mov r0, #0xff
	and r1, r0
	ldr r0, =dword_80382C8
	lsl r1, r1, #1
	add r1, r1, r0
	mov r0, #0
	ldrsh r1, [r1, r0]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r1, r0, #1
	cmp r1, #0
	bge _0801350C
	neg r0,r1
	asr r0, r0, #8
	neg r1, r0
	ldr r2, =0x03004720
	mov r8, r2
	b _08013512
	.align 2, 0
.pool
_0801350C:
	asr r1, r1, #8
	ldr r0, =0x03004720
	mov r8, r0
_08013512:
	mov r2, r8
	ldrh r0, [r2]
	add r7, r0, r1
	asr r2, r6, #8
	mov r0, r8
	ldrh r1, [r0, #2]
	cmp r2, r1
	bge _0801352A
	add r0, r1, #0
	sub r0, #0xa0
	cmp r2, r0
	bgt _08013538
_0801352A:
	mov r3, #6
	cmp r2, r1
	bge _0801356A
	sub r3, #0xc
	b _0801356A
	.align 2, 0
.pool
_08013538:
	sub r0, r1, r2
	add r0, #0x50
	lsl r0, r0, #7
	mov r1, #0xa0
	bl Bios_Div
	mov r1, #0xff
	and r1, r0
	ldr r0, =dword_80382C8
	lsl r1, r1, #1
	add r1, r1, r0
	mov r2, #0
	ldrsh r3, [r1, r2]
	lsl r0, r3, #1
	add r0, r0, r3
	lsl r3, r0, #1
	cmp r3, #0
	bge _08013568
	neg r0, r3
	asr r0, r0, #8
	neg r3, r0
	b _0801356A
	.align 2, 0
.pool
_08013568:
	asr r3, r3, #8
_0801356A:
	mov r0, r8
	ldrh r1, [r0, #2]
	add r1, r1, r3
	str r5, [r0, #0x14]
	str r6, [r0, #0x18]
	str r4, [r0, #0x20]
	mov r2, sb
	str r2, [r0, #0x24]
	add r0, r7, #0
	bl setCameraPos
	mov r0, #1
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	
	thumb_func_global cameraFunc5
cameraFunc5: @ 0x08013590
	push {r4, r5, r6, r7, lr}
	add r7, r0, #0
	add r4, r1, #0
	ldr r2, =dword_803CE50
	ldr r0, =0x03002080
	ldr r0, [r0]
	mov r1, #2
	and r0, r1
	lsr r0, r0, #1
	lsl r0, r0, #1
	add r0, r0, r2
	mov r1, #0
	ldrsh r0, [r0, r1]
	add r6, r0, #0
	add r6, #8
	ldr r0, =0x03004EE0
	mov r1, #0x92
	lsl r1, r1, #1
	add r0, r0, r1
	ldr r0, [r0]
	ldr r0, [r0, #8]
	cmp r4, r0
	ble _080135D4
	add r0, r4, r0
	asr r0, r0, #9
	add r3, r0, #0
	add r3, #0x5c
	b _080135DC
	.align 2, 0
.pool
_080135D4:
	add r0, r4, r0
	asr r0, r0, #9
	add r3, r0, #0
	add r3, #0x4e
_080135DC:
	ldr r0, =0x03004720
	ldrh r2, [r0, #2]
	add r1, r3, #0
	add r5, r0, #0
	cmp r1, r2
	beq _080135F0
	sub r1, r2, #1
	cmp r3, r2
	ble _080135F0
	add r1, r2, #1
_080135F0:
	asr r2, r4, #8
	add r0, r2, #0
	add r0, #0x9c
	cmp r1, r0
	ble _080135FC
	sub r1, #1
_080135FC:
	add r0, r2, #0
	add r0, #0x24
	cmp r1, r0
	bge _08013606
	add r1, #1
_08013606:
	str r7, [r5, #0x14]
	str r4, [r5, #0x18]
	add r0, r6, #0
	bl setCameraPos
	mov r0, #1
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool




	thumb_func_global cameraFunc6
cameraFunc6: @ 0x0801361C
	push {r4, r5, lr}
	add r4, r0, #0
	add r5, r1, #0
	ldr r2, =0x03004720
	ldr r3, =dword_803CE50
	ldr r0, =0x03002080
	ldr r0, [r0]
	mov r1, #2
	and r0, r1
	lsr r0, r0, #1
	lsl r0, r0, #1
	add r0, r0, r3
	mov r1, #0
	ldrsh r0, [r0, r1]
	add r0, #8
	ldrh r1, [r2, #2]
	str r4, [r2, #0x14]
	str r5, [r2, #0x18]
	bl setCameraPos
	mov r0, #1
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
.pool



	thumb_func_global ret_1
ret_1: @ 0x08013658
	mov r0, #1
	bx lr
	
	
	
	
	thumb_func_global cameraFunc7
cameraFunc7: @ 0x0801365C
	push {r4, r5, lr}
	add r4, r0, #0
	add r5, r1, #0
	bl getPlayerHealth
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	cmp r1, #0
	bne _08013688
	ldr r0, =0x03004720
	strh r1, [r0, #8]
	str r1, [r0, #0x28]
	add r0, r4, #0
	add r1, r5, #0
	bl runCameraFunc
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	b _08013750
	.align 2, 0
.pool
_08013688:
	ldr r1, =0x03004720
	ldr r0, [r1, #0x28]
	ldr r0, [r0, #0x3c]
	ldr r0, [r0, #4]
	asr r4, r0, #8
	ldrh r3, [r1]
	add r0, r3, #0
	add r0, #0xf0
	add r5, r1, #0
	cmp r4, r0
	bge _080136A2
	cmp r4, r3
	bge _080136B0
_080136A2:
	mov r2, #2
	cmp r4, r3
	bge _080136DE
	sub r2, #4
	b _080136DE
	.align 2, 0
.pool
_080136B0:
	sub r0, r4, r3
	sub r0, #0x78
	lsl r0, r0, #7
	mov r1, #0xf0
	bl Bios_Div
	mov r1, #0xff
	and r1, r0
	ldr r0, =dword_80382C8
	lsl r1, r1, #1
	add r1, r1, r0
	mov r0, #0
	ldrsh r2, [r1, r0]
	lsl r2, r2, #1
	cmp r2, #0
	bge _080136DC
	neg r0, r2
	asr r0, r0, #8
	neg r2, r0
	b _080136DE
	.align 2, 0
.pool
_080136DC:
	asr r2, r2, #8
_080136DE:
	ldrh r0, [r5]
	add r4, r0, r2
	str r2, [r5, #0x20]
	ldr r0, [r5, #0x28]
	ldr r0, [r0, #0x3c]
	ldr r0, [r0, #8]
	asr r3, r0, #8
	ldrh r1, [r5, #2]
	cmp r3, r1
	bge _080136FA
	add r0, r1, #0
	sub r0, #0xa0
	cmp r3, r0
	bgt _08013704
_080136FA:
	mov r2, #7
	cmp r3, r1
	bge _08013736
	sub r2, #0xe
	b _08013736
_08013704:
	sub r0, r1, r3
	add r0, #0x50
	lsl r0, r0, #7
	mov r1, #0xa0
	bl Bios_Div
	mov r1, #0xff
	and r1, r0
	ldr r0, =dword_80382C8
	lsl r1, r1, #1
	add r1, r1, r0
	mov r0, #0
	ldrsh r2, [r1, r0]
	lsl r0, r2, #1
	add r0, r0, r2
	lsl r2, r0, #1
	cmp r2, #0
	bge _08013734
	neg r0, r2
	asr r0, r0, #8
	neg r2, r0
	b _08013736
	.align 2, 0
.pool
_08013734:
	asr r2, r2, #8
_08013736:
	ldrh r1, [r5, #2]
	add r1, r1, r2
	str r2, [r5, #0x24]
	ldr r0, [r5, #0x28]
	ldr r2, [r0, #0x3c]
	ldr r0, [r2, #4]
	str r0, [r5, #0x14]
	ldr r0, [r2, #8]
	str r0, [r5, #0x18]
	add r0, r4, #0
	bl setCameraPos
	mov r0, #1
_08013750:
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	
thumb_func_global setCameraPos
setCameraPos: @ 0x08013758
	push {r4, r5, lr}
	add r2, r0, #0
	add r4, r1, #0
	ldr r1, =0x030046F0
	ldrh r0, [r1, #0x14]
	add r3, r0, #0
	sub r3, #0xf0
	add r5, r1, #0
	cmp r2, r3
	ble _0801377C
	ldr r0, =0x03004720
	strh r3, [r0]
	b _08013790
	.align 2, 0
.pool
_0801377C:
	cmp r2, #0
	bge _0801378C
	ldr r1, =0x03004720
	mov r0, #0
	strh r0, [r1]
	b _08013792
	.align 2, 0
.pool
_0801378C:
	ldr r0, =0x03004720
	strh r2, [r0]
_08013790:
	add r1, r0, #0
_08013792:
	ldrh r0, [r5, #0x16]
	sub r0, #1
	cmp r4, r0
	ble _080137A4
	strh r0, [r1, #2]
	b _080137B0
	.align 2, 0
.pool
_080137A4:
	cmp r4, #0x9e
	bgt _080137AE
	mov r0, #0x9f
	strh r0, [r1, #2]
	b _080137B0
_080137AE:
	strh r4, [r1, #2]
_080137B0:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global setCameraStruct
setCameraStruct: @ 0x080137B8
	push {r4, r5, r6, lr}
	mov r6, r8
	push {r6}
	sub sp, #8
	add r5, r0, #0
	add r6, r1, #0
	mov r8, r2
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	lsl r6, r6, #0x10
	lsr r6, r6, #0x10
	mov r0, r8
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r8, r0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r4, =0x03004720
	ldr r2, =0x01000018
	mov r0, sp
	add r1, r4, #0
	str r3, [sp, #4]
	bl Bios_memcpy
	add r0, r5, #0
	add r1, r6, #0
	mov r2, r8
	ldr r3, [sp, #4]
	bl getLevelInitialCameraPosition
	ldrh r1, [r0]
	mov r2, #0
	strh r1, [r4]
	ldrh r1, [r0, #2]
	strh r1, [r4, #2]
	ldrh r0, [r0, #4]
	strh r0, [r4, #8]
	strh r2, [r4, #4]
	strh r2, [r4, #6]
	strh r2, [r4, #0xe]
	strh r2, [r4, #0xc]
	strh r2, [r4, #0x10]
	strh r2, [r4, #0x2c]
	add sp, #8
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global getLevelInitialCameraPosition
getLevelInitialCameraPosition: @ 0x08013828
	push {r4, lr}
	lsl r0, r0, #0x10
	lsl r1, r1, #0x10
	lsl r2, r2, #0x10
	lsl r3, r3, #0x10
	ldr r4, =initialCameraStates
	lsr r0, r0, #0xe
	add r0, r0, r4
	ldr r0, [r0]
	lsr r1, r1, #0xe
	add r1, r1, r0
	ldr r0, [r1]
	lsr r2, r2, #0xe
	add r2, r2, r0
	ldr r0, [r2]
	lsr r3, r3, #0xd
	add r3, r0, r3
	add r0, r3, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global getDoorCoords
getDoorCoords: @ 0x08013858
	push {r4, lr}
	lsl r0, r0, #0x10
	lsl r1, r1, #0x10
	lsl r2, r2, #0x10
	lsl r3, r3, #0x10
	ldr r4, =doorPositions
	lsr r0, r0, #0xe
	add r0, r0, r4
	ldr r0, [r0]
	lsr r1, r1, #0xe
	add r1, r1, r0
	ldr r0, [r1]
	lsr r2, r2, #0xe
	add r2, r2, r0
	ldr r0, [r2]
	lsr r3, r3, #0xe
	add r3, r0, r3
	add r0, r3, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_08013888
sub_08013888: @ 0x08013888
	ldr r0, =0x03004720
	ldrh r1, [r0, #8]
	strh r1, [r0, #0xa]
	mov r1, #2
	strh r1, [r0, #8]
	bx lr
	.align 2, 0
.pool

thumb_func_global sub_08013898
sub_08013898: @ 0x08013898
	ldr r1, =0x03004720
	ldrh r0, [r1, #0xa]
	strh r0, [r1, #8]
	bx lr
	.align 2, 0
.pool

thumb_func_global sub_080138A4
sub_080138A4: @ 0x080138A4
	push {r4, lr}
	ldr r4, =0x03004720
	ldrh r0, [r4, #8]
	cmp r0, #5
	beq _080138E8
	cmp r0, #9
	beq _080138E8
	cmp r0, #8
	beq _080138E8
	ldr r1, =0x03003D40
	ldrb r0, [r1, #0x10]
	cmp r0, #0
	bne _080138E8
	ldr r0, [r1]
	ldr r1, =0x00FFFF00
	and r0, r1
	mov r1, #0x81
	lsl r1, r1, #0xa
	cmp r0, r1
	bne _080138D8
	ldr r2, =0x03000D4C
	ldr r1, =0x03003D94
	ldrh r0, [r1]
	strh r0, [r2]
	mov r0, #0x10
	strh r0, [r1]
_080138D8:
	bl sub_080329D4
	ldrh r0, [r4, #8]
	strh r0, [r4, #0xa]
	mov r0, #3
	strh r0, [r4, #8]
	bl loadCameraBorder
_080138E8:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global removeCameraPlayerControl
removeCameraPlayerControl: @ 0x08013904
	push {r4, lr}
	ldr r4, =0x03004720
	ldrh r0, [r4, #8]
	cmp r0, #3
	bne _08013932
	ldr r0, =0x03003D40
	ldr r0, [r0]
	ldr r1, =0x00FFFF00
	and r0, r1
	mov r1, #0x81
	lsl r1, r1, #0xa
	cmp r0, r1
	bne _08013926
	ldr r0, =0x03003D94
	ldr r1, =0x03000D4C
	ldrh r1, [r1]
	strh r1, [r0]
_08013926:
	bl sub_08032978
	ldrh r0, [r4, #0xa]
	strh r0, [r4, #8]
	bl sub_08012938
_08013932:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0801394C
sub_0801394C: @ 0x0801394C
	ldr r0, =0x03004720
	ldrh r0, [r0, #8]
	cmp r0, #3
	beq _0801395C
	mov r0, #0
	b _0801395E
	.align 2, 0
.pool
_0801395C:
	mov r0, #1
_0801395E:
	bx lr
	
	
	
	thumb_func_global isCameraPosCloneDifferent
isCameraPosCloneDifferent:
	ldr r1, =0x03004720
	ldrh r0, [r1]
	ldrh r2, [r1, #0x1c]
	cmp r0, r2
	bne _0801397C
	ldrh r0, [r1, #2]
	ldrh r1, [r1, #0x1e]
	cmp r0, r1
	bne _0801397C
	mov r0, #0
	b locret_801397E
	.align 2, 0
.pool
_0801397C:
	mov r0, #1
locret_801397E:
	bx lr
	
	
	
thumb_func_global sub_08013980
sub_08013980: @ 0x08013980
	push {r4, r5, lr}
	add r5, r0, #0
	ldr r0, =0x03003D40
	ldrb r2, [r0, #2]
	ldr r1, =stageCount
	ldrb r0, [r0, #1]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	sub r0, #1
	cmp r2, r0
	blt _080139B8
	bl getPlayerHealth
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _080139B8
	ldr r4, =0x03004720
	ldrh r0, [r4, #8]
	cmp r0, #2
	bne _080139AE
	mov r0, #8
	strh r0, [r4, #0xa]
_080139AE:
	bl removeCameraPlayerControl
	str r5, [r4, #0x28]
	mov r0, #8
	strh r0, [r4, #8]
_080139B8:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global sub_080139CC
sub_080139CC: @ 0x080139CC
	ldr r0, =0x03003D40
	ldrb r2, [r0, #2]
	ldr r1, =stageCount
	ldrb r0, [r0, #1]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	sub r0, #1
	cmp r2, r0
	blt _080139FE
	ldr r1, =0x03004720
	mov r2, #0
	str r2, [r1, #0x28]
	ldrh r0, [r1, #8]
	cmp r0, #2
	bne _080139FC
	strh r2, [r1, #0xa]
	b _080139FE
	.align 2, 0
.pool
_080139FC:
	strh r2, [r1, #8]
_080139FE:
	bx lr

thumb_func_global sub_08013A00
sub_08013A00: @ 0x08013A00
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	ldr r1, =0x03004720
	ldrh r0, [r1, #8]
	cmp r0, #2
	bne _08013A14
	strh r2, [r1, #0xa]
	b _08013A16
	.align 2, 0
.pool
_08013A14:
	strh r2, [r1, #8]
_08013A16:
	bx lr
	
	
	thumb_func_global sub_8013A18
sub_8013A18: @ 0x08013A18
	push {r4, lr}
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #1
	bhi _08013A4C
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	cmp r0, #1
	bne _08013A4C
	ldrb r0, [r1, #2]
	cmp r0, #2
	bls _08013A4C
	ldr r2, =0x03004720
	ldrh r1, [r2, #0x2c]
	mov r3, #0x80
	lsl r3, r3, #8
	add r0, r3, #0
	and r0, r1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	cmp r1, #0
	bne _08013A4C
	add r0, r4, #0
	orr r0, r3
	strh r0, [r2, #0x2c]
	strh r1, [r2, #0x10]
_08013A4C:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
thumb_func_global getCameraField2C_bit0
getCameraField2C_bit0: @ 0x08013A5C
	ldr r0, =0x03003D40
	ldrb r1, [r0, #1]
	cmp r1, #1
	bne _08013A8C
	ldrb r0, [r0, #2]
	cmp r0, #2
	bls _08013A8C
	ldr r3, =0x03004720
	ldrh r2, [r3, #0x2c]
	mov r0, #0x80
	lsl r0, r0, #8
	and r0, r2
	cmp r0, #0
	beq _08013A8C
	ldrh r0, [r3, #0x10]
	cmp r0, #1
	bhi _08013A8C
	and r1, r2
	add r0, r1, #0
	b _08013A90
	.align 2, 0
.pool
_08013A8C:
	mov r0, #1
	neg r0, r0
_08013A90:
	bx lr
	.align 2, 0
