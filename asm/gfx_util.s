.include "asm/macros.inc"
	


/*
void tilemapCpy(void* src, void* dst, u16 mode, u16 tileoffset, u8 paloffset)
- Copies a tilemap from src to dst
- src points to width and height followed by the tilemap data

- tileoffset is added to the tile number of each tilemap entry
- paloffset is added to the palette number of each tilemap entry

*/
thumb_func_global tilemapCpy
tilemapCpy: @ 0x0800DC14
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x58
	mov sl, r0
	str r1, [sp, #0x40]
	ldr r0, [sp, #0x78]
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	str r2, [sp, #0x44]
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	str r3, [sp, #0x48]
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	str r0, [sp, #0x4c]
	cmp r2, #0
	bne _0800DCC8
	mov r0, sl
	str r0, [sp, #0x50]
	ldrh r5, [r0]
	ldrh r1, [r0, #2]
	mov sb, r1
	mov r4, #0
	cmp r4, sb
	bhs _0800DD2A
_0800DC4C:
	add r0, r4, #0
	mul r0, r5, r0
	lsl r0, r0, #1
	add r0, #4
	ldr r2, [sp, #0x50]
	add r0, r2, r0
	mov r1, sp
	add r2, r5, #0
	bl Bios_memcpy
	mov r3, #0
	lsl r7, r4, #6
	str r7, [sp, #0x54]
	add r6, r4, #1
	cmp r3, r5
	bhs _0800DCAC
	ldr r1, [sp, #0x4c]
	lsl r0, r1, #0x18
	asr r4, r0, #0x18
	ldr r2, =0x00000FFF
	mov sl, r2
	mov r7, #0xf0
	lsl r7, r7, #8
	mov ip, r7
	mov r0, #0xf
	mov r8, r0
_0800DC80:
	lsl r0, r3, #1
	mov r1, sp
	add r2, r1, r0
	ldrh r1, [r2]
	lsr r0, r1, #0xc
	add r0, r4, r0
	mov r7, r8
	and r0, r7
	lsl r0, r0, #0xc
	mov r7, sl
	and r1, r7
	mov r7, ip
	and r0, r7
	orr r1, r0
	ldr r0, [sp, #0x48]
	add r1, r0, r1
	strh r1, [r2]
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, r5
	blo _0800DC80
_0800DCAC:
	ldr r2, [sp, #0x40]
	ldr r3, [sp, #0x54]
	add r1, r2, r3
	mov r0, sp
	add r2, r5, #0
	bl Bios_memcpy
	lsl r0, r6, #0x10
	lsr r4, r0, #0x10
	cmp r4, sb
	blo _0800DC4C
	b _0800DD2A
	.align 2, 0
.pool
_0800DCC8:
	mov r4, sl
	ldrb r5, [r4]
	ldrb r7, [r4, #1]
	mov sb, r7
	mov r4, #0
	cmp r4, sb
	bhs _0800DD2A
	lsr r0, r5, #1
	lsl r0, r0, #0x10
	mov r8, r0
	mov r7, sp
_0800DCDE:
	add r0, r4, #0
	mul r0, r5, r0
	add r0, #2
	add r0, sl
	mov r1, sp
	mov r3, r8
	lsr r2, r3, #0x10
	bl Bios_memcpy
	mov r3, #0
	add r6, r4, #1
	ldr r0, [sp, #0x44]
	add r2, r4, #0
	mul r2, r0, r2
	cmp r3, r5
	bhs _0800DD12
_0800DCFE:
	add r0, r7, r3
	ldrb r1, [r0]
	ldr r4, [sp, #0x48]
	add r1, r1, r4
	strb r1, [r0]
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, r5
	blo _0800DCFE
_0800DD12:
	asr r1, r2, #3
	ldr r0, [sp, #0x40]
	add r1, r0, r1
	add r0, r7, #0
	mov r3, r8
	lsr r2, r3, #0x10
	bl Bios_memcpy
	lsl r0, r6, #0x10
	lsr r4, r0, #0x10
	cmp r4, sb
	blo _0800DCDE
_0800DD2A:
	add sp, #0x58
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global unused_sub_800DD3C_mapcpy
unused_sub_800DD3C_mapcpy: @ 0x0800DD3C
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x5c
	str r1, [sp, #0x40]
	str r2, [sp, #0x44]
	ldr r1, [sp, #0x7c]
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	str r3, [sp, #0x48]
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	str r1, [sp, #0x4c]
	add r5, r0, #0
	ldrh r6, [r5]
	ldrh r0, [r5, #2]
	str r0, [sp, #0x50]
	add r5, #4
	mov r0, #0
	ldr r1, [sp, #0x50]
	cmp r0, r1
	bhs _0800DDF6
	lsl r3, r6, #1
	str r3, [sp, #0x54]
_0800DD70:
	mov r4, #0
	lsl r7, r0, #6
	mov ip, r7
	add r0, #1
	mov r8, r0
	cmp r4, r6
	bhs _0800DDDA
	ldr r1, [sp, #0x4c]
	lsl r0, r1, #0x18
	asr r0, r0, #0x18
	str r0, [sp, #0x58]
	mov r3, #0xf0
	lsl r3, r3, #8
	mov sl, r3
	ldr r7, =0x00000FFF
	mov sb, r7
_0800DD90:
	lsl r3, r4, #1
	add r0, r3, r5
	ldrh r2, [r0]
	add r0, r2, #0
	ldr r1, =0x000003FF
	and r0, r1
	lsl r0, r0, #1
	ldr r7, [sp, #0x40]
	add r0, r0, r7
	ldrh r1, [r0]
	ldr r0, [sp, #0x48]
	add r1, r0, r1
	add r0, r2, #0
	mov r7, #0xc0
	lsl r7, r7, #4
	and r0, r7
	orr r1, r0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	lsr r2, r2, #0xc
	ldr r0, [sp, #0x58]
	add r2, r0, r2
	lsl r2, r2, #0x1c
	lsr r2, r2, #0x10
	mov r7, sp
	add r0, r7, r3
	mov r3, sl
	and r2, r3
	mov r7, sb
	and r1, r7
	orr r2, r1
	strh r2, [r0]
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, r6
	blo _0800DD90
_0800DDDA:
	ldr r1, [sp, #0x44]
	add r1, ip
	mov r0, sp
	add r2, r6, #0
	bl Bios_memcpy
	ldr r0, [sp, #0x54]
	add r5, r5, r0
	mov r1, r8
	lsl r0, r1, #0x10
	lsr r0, r0, #0x10
	ldr r3, [sp, #0x50]
	cmp r0, r3
	blo _0800DD70
_0800DDF6:
	add sp, #0x5c
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool



/*
	void LZ77_mapCpy(void* src, void* dst, u16 mode, u16 tileoffset, u8 paloffset)
	- decompresses data from src to the buffer at 0x02009480, then calls tilemapCpy with that buffer as src
*/
thumb_func_global LZ77_mapCpy
LZ77_mapCpy: @ 0x0800DE10
	push {r4, r5, r6, lr}
	mov r6, sb
	mov r5, r8
	push {r5, r6}
	sub sp, #4
	mov sb, r1
	add r5, r2, #0
	add r6, r3, #0
	ldr r4, [sp, #0x1c]
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	lsl r6, r6, #0x10
	lsr r6, r6, #0x10
	lsl r4, r4, #0x18
	lsr r4, r4, #0x18
	ldr r1, =sMapDecompressionBuf
	mov r8, r1
	bl Bios_LZ77_8bit
	lsl r4, r4, #0x18
	asr r4, r4, #0x18
	str r4, [sp]
	mov r0, r8
	mov r1, sb
	add r2, r5, #0
	add r3, r6, #0
	bl tilemapCpy
	add sp, #4
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global unused_lz_mapCpy
unused_lz_mapCpy: @ 0x0800DE5C
	push {r4, r5, r6, lr}
	mov r6, sb
	mov r5, r8
	push {r5, r6}
	sub sp, #4
	mov r8, r1
	mov sb, r2
	add r5, r3, #0
	ldr r4, [sp, #0x1c]
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	lsl r4, r4, #0x18
	lsr r4, r4, #0x18
	ldr r6, =sMapDecompressionBuf
	add r1, r6, #0
	bl Bios_LZ77_8bit
	lsl r4, r4, #0x18
	asr r4, r4, #0x18
	str r4, [sp]
	add r0, r6, #0
	mov r1, r8
	mov r2, sb
	add r3, r5, #0
	bl unused_sub_800DD3C_mapcpy
	add sp, #4
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global Bios__memcpy_bytecount
Bios__memcpy_bytecount: @ 0x0800DEA4
	push {lr}
	add r3, r1, #0
	add r1, r2, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x11
	add r2, r3, #0
	bl Bios_memcpy
	pop {r0}
	bx r0

thumb_func_global Bios_LZ77_16_2
Bios_LZ77_16_2: @ 0x0800DEB8
	push {lr}
	bl Bios_LZ77_16bit
	pop {r0}
	bx r0
	.align 2, 0



/*
	void unused_structuredMapCpy(struct unused* arg)
	- calls tilemapCpy or LZ77_mapCpy with arguments taken from a structure.
	
	the structure is like this: 
	struct unused{
		u8 isCompressed;
		u16 mode;
		void* src;
		void* dst;
		u16 tileOffset;
		u16 palOffset;
	};
*/
thumb_func_global unused_structuredMapCpy
unused_structuredMapCpy: @ 0x0800DEC4
	push {r4, r5, lr}
	sub sp, #4
	add r5, r0, #0
	b _0800DF00
_0800DECC:
	ldrb r0, [r5]
	cmp r0, #0
	beq _0800DED8
	cmp r0, #1
	beq _0800DEEC
	b _0800DEFE
_0800DED8:
	ldr r0, [r5, #4]
	ldr r1, [r5, #8]
	ldrh r2, [r5, #2]
	ldrh r3, [r5, #0xc]
	mov r4, #0xe
	ldrsb r4, [r5, r4]
	str r4, [sp]
	bl tilemapCpy
	b _0800DEFE
_0800DEEC:
	ldr r0, [r5, #4]
	ldr r1, [r5, #8]
	ldrh r2, [r5, #2]
	ldrh r3, [r5, #0xc]
	mov r4, #0xe
	ldrsb r4, [r5, r4]
	str r4, [sp]
	bl LZ77_mapCpy
_0800DEFE:
	add r5, #0x10
_0800DF00:
	ldrb r0, [r5]
	cmp r0, #0xff
	bne _0800DECC
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global unused_structuredCpy
unused_structuredCpy: @ 0x0800DF10
	push {r4, lr}
	add r4, r0, #0
	b _0800DF30
_0800DF16:
	cmp r1, #0
	bne _0800DF26
	ldr r0, [r4, #4]
	ldrh r1, [r4, #2]
	ldr r2, [r4, #8]
	bl Bios__memcpy_bytecount
	b _0800DF2E
_0800DF26:
	ldr r0, [r4, #4]
	ldr r1, [r4, #8]
	bl Bios_LZ77_16_2
_0800DF2E:
	add r4, #0xc
_0800DF30:
	ldrb r1, [r4]
	add r0, r1, #0
	cmp r0, #0xff
	bne _0800DF16
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0


/*
	void memcpy_pal(void* src, int palCount, void* dst)
	- copies palCount*32 bytes from src to dst
*/
thumb_func_global memcpy_pal
memcpy_pal: @ 0x0800DF40
	push {lr}
	add r3, r1, #0
	add r1, r2, #0
	lsl r3, r3, #0x18
	lsr r3, r3, #0x14
	add r2, r3, #0
	bl Bios_memcpy
	pop {r0}
	bx r0

thumb_func_global updateGFXRegs
updateGFXRegs: @ 0x0800DF54
	push {r4, r5, lr}
	mov r1, #0x80
	lsl r1, r1, #0x13
	ldr r0, =0x03003CD4
	ldrh r0, [r0]
	strh r0, [r1]
	add r1, #4
	ldr r0, =0x03003D08
	ldrh r0, [r0]
	strh r0, [r1]
	ldr r2, =0x04000008
	ldr r1, =0x03003D00
	ldrh r0, [r1]
	strh r0, [r2]
	add r2, #2
	ldrh r0, [r1, #2]
	strh r0, [r2]
	add r2, #2
	ldrh r0, [r1, #4]
	strh r0, [r2]
	add r2, #2
	ldrh r0, [r1, #6]
	strh r0, [r2]
	add r2, #2
	ldr r1, =0x03003D38
	ldrh r0, [r1]
	strh r0, [r2]
	add r2, #4
	ldrh r0, [r1, #2]
	strh r0, [r2]
	add r2, #4
	ldrh r0, [r1, #4]
	strh r0, [r2]
	add r2, #4
	ldrh r0, [r1, #6]
	strh r0, [r2]
	sub r2, #0xa
	ldr r1, =0x03003D68
	ldrh r0, [r1]
	strh r0, [r2]
	add r2, #4
	ldrh r0, [r1, #2]
	strh r0, [r2]
	add r2, #4
	ldrh r0, [r1, #4]
	strh r0, [r2]
	add r2, #4
	ldrh r0, [r1, #6]
	strh r0, [r2]
	ldr r1, =0x04000020
	ldr r5, =0x03003CE0
	ldr r0, [r5]
	strh r0, [r1]
	add r1, #2
	ldr r4, =0x03003D10
	ldr r0, [r4]
	strh r0, [r1]
	add r1, #2
	ldr r3, =0x03003D80
	ldr r0, [r3]
	strh r0, [r1]
	add r1, #2
	ldr r2, =0x03003D28
	ldr r0, [r2]
	strh r0, [r1]
	add r1, #0xa
	ldr r0, [r5, #4]
	strh r0, [r1]
	add r1, #2
	ldr r0, [r4, #4]
	strh r0, [r1]
	add r1, #2
	ldr r0, [r3, #4]
	strh r0, [r1]
	add r1, #2
	ldr r0, [r2, #4]
	strh r0, [r1]
	sub r1, #0xe
	ldr r2, =0x03003D30
	ldr r0, [r2]
	str r0, [r1]
	add r1, #0x10
	ldr r0, [r2, #4]
	str r0, [r1]
	sub r1, #0xc
	ldr r2, =0x03003BF0
	ldr r0, [r2]
	str r0, [r1]
	add r1, #0x10
	ldr r0, [r2, #4]
	str r0, [r1]
	add r1, #4
	ldr r2, =0x03003D90
	ldrh r0, [r2]
	strh r0, [r1]
	add r1, #2
	ldrh r0, [r2, #2]
	strh r0, [r1]
	add r1, #2
	ldr r2, =0x03003D88
	ldrh r0, [r2]
	strh r0, [r1]
	add r1, #2
	ldrh r0, [r2, #2]
	strh r0, [r1]
	add r1, #2
	ldr r0, =0x03003D7C
	ldrh r0, [r0]
	strh r0, [r1]
	add r1, #2
	ldr r0, =0x03003D8C
	ldrh r0, [r0]
	strh r0, [r1]
	add r1, #2
	ldr r0, =0x03003D20
	ldrh r0, [r0]
	strh r0, [r1]
	add r1, #4
	ldr r0, =0x03003D74
	ldrh r0, [r0]
	strh r0, [r1]
	add r1, #2
	ldr r0, =0x03003D94
	ldrh r0, [r0]
	strh r0, [r1]
	add r1, #2
	ldr r0, =0x03003D70
	ldrh r0, [r0]
	strh r0, [r1]
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
	thumb_func_global sub_800E0B0
sub_800E0B0: @ 0x0800E0B0
	lsl r0, r0, #0x18
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	lsl r2, r2, #0x18
	lsr r2, r2, #0x18
	ldr r3, =0x03002308
	lsr r0, r0, #0x17
	add r3, r0, r3
	strh r1, [r3]
	ldr r1, =0x030022F8
	add r0, r0, r1
	strh r2, [r0]
	bx lr
	.align 2, 0
.pool


thumb_func_global unused_setAffineBG
unused_setAffineBG: @ 0x0800E0D4
	push {r4, r5, lr}
	lsl r0, r0, #0x18
	lsr r4, r0, #0x18
	lsl r1, r1, #0x10
	lsr r5, r1, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	cmp r4, #1
	bls _0800E100
	ldr r0, =0x030022F0
	sub r1, r4, #2
	lsl r1, r1, #1
	add r0, r1, r0
	strh r5, [r0]
	ldr r0, =0x03002300
	add r0, r1, r0
	strh r2, [r0]
	ldr r0, =0x03002310
	add r1, r1, r0
	strh r3, [r1]
_0800E100:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global handleBgMode
handleBgMode: @ 0x0800E114
	push {lr}
	bl copy_negative_ram
	bl calculateBgTransforms
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global copy_negative_ram
copy_negative_ram: @ 0x0800E124
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	mov r0, #0x80
	lsl r0, r0, #0x13
	ldrh r0, [r0]
	mov r4, #7
	and r4, r0
	cmp r4, #1
	bhi _0800E174
	mov r3, #0
	ldr r0, =0x030022F8
	mov ip, r0
	ldr r7, =0x03003D38
	mov r8, r7
	ldr r6, =0x03002308
	ldr r5, =0x03003D68
_0800E146:
	lsl r1, r3, #1
	mov r0, r8
	add r2, r1, r0
	add r0, r1, r6
	mov r7, #0
	ldrsh r0, [r0, r7]
	neg r0, r0
	strh r0, [r2]
	add r2, r1, r5
	add r1, ip
	mov r7, #0
	ldrsh r0, [r1, r7]
	neg r0, r0
	strh r0, [r2]
	add r0, r3, #1
	lsl r0, r0, #0x18
	lsr r3, r0, #0x18
	cmp r3, #3
	bhi _0800E174
	cmp r3, #1
	bls _0800E146
	cmp r4, #1
	bne _0800E146
_0800E174:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global calculateBgTransforms
calculateBgTransforms: @ 0x0800E190
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #8
	mov r0, #0x80
	lsl r0, r0, #0x13
	ldrh r1, [r0]
	mov r0, #7
	and r0, r1
	cmp r0, #0
	bne _0800E1AC
	b _0800E39C
_0800E1AC:
	mov r0, #0
	str r0, [sp]
_0800E1B0:
	ldr r6, =0x030022F0
	ldr r1, [sp]
	lsl r1, r1, #1
	mov r8, r1
	add r6, r8
	ldrh r0, [r6]
	add r0, #0x40
	lsl r0, r0, #1
	ldr r2, =dword_80382C8
	add r0, r0, r2
	mov r4, #0
	ldrsh r5, [r0, r4]
	ldr r4, =0x03002300
	add r4, r8
	mov r1, #0
	ldrsh r0, [r4, r1]
	bl invert_16bit
	add r1, r0, #0
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	add r0, r5, #0
	bl multiply_fixed8_unk
	ldr r1, =0x03003CE0
	ldr r2, [sp]
	lsl r7, r2, #2
	add r1, r7, r1
	str r1, [sp, #4]
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	str r0, [r1]
	ldrh r0, [r6]
	lsl r0, r0, #1
	ldr r1, =dword_80382C8
	add r0, r0, r1
	mov r2, #0
	ldrsh r5, [r0, r2]
	mov r1, #0
	ldrsh r0, [r4, r1]
	bl invert_16bit
	add r1, r0, #0
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	add r0, r5, #0
	bl multiply_fixed8_unk
	ldr r1, =0x03003D10
	add r1, r1, r7
	mov sb, r1
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	str r0, [r1]
	ldrh r0, [r6]
	lsl r0, r0, #1
	ldr r2, =dword_80382C8
	add r0, r0, r2
	ldrh r4, [r0]
	neg r4, r4
	lsl r4, r4, #0x10
	asr r4, r4, #0x10
	ldr r0, =0x03002310
	add r8, r0
	mov r1, r8
	mov r2, #0
	ldrsh r0, [r1, r2]
	bl invert_16bit
	add r1, r0, #0
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	add r0, r4, #0
	bl multiply_fixed8_unk
	ldr r1, =0x03003D80
	add r1, r1, r7
	mov sl, r1
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	str r0, [r1]
	ldrh r0, [r6]
	add r0, #0x40
	lsl r0, r0, #1
	ldr r4, =dword_80382C8
	add r0, r0, r4
	mov r1, #0
	ldrsh r4, [r0, r1]
	mov r2, r8
	mov r1, #0
	ldrsh r0, [r2, r1]
	bl invert_16bit
	add r1, r0, #0
	lsl r1, r1, #0x10
	asr r1, r1, #0x10
	add r0, r4, #0
	bl multiply_fixed8_unk
	ldr r1, =0x03003D28
	add r6, r7, r1
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	str r0, [r6]
	ldr r0, =0x03003D64
	ldr r2, [sp]
	add r0, r2, r0
	ldrb r0, [r0]
	cmp r0, #0
	bne _0800E324
	ldr r4, =0x03003D30
	add r4, r7, r4
	ldr r1, [sp, #4]
	ldr r0, [r1]
	lsl r2, r0, #4
	sub r2, r2, r0
	lsl r2, r2, #3
	neg r2, r2
	mov r0, sb
	ldr r1, [r0]
	lsl r0, r1, #2
	add r0, r0, r1
	lsl r0, r0, #4
	sub r2, r2, r0
	ldr r0, =0x03002308
	ldr r3, [sp]
	add r3, #2
	lsl r3, r3, #1
	add r0, r3, r0
	mov r1, #0
	ldrsh r0, [r0, r1]
	neg r0, r0
	add r0, #0x78
	lsl r0, r0, #8
	add r2, r2, r0
	str r2, [r4]
	ldr r4, =0x03003BF0
	add r4, r7, r4
	mov r2, sl
	ldr r0, [r2]
	lsl r1, r0, #4
	sub r1, r1, r0
	lsl r1, r1, #3
	neg r1, r1
	ldr r2, [r6]
	lsl r0, r2, #2
	add r0, r0, r2
	lsl r0, r0, #4
	sub r1, r1, r0
	ldr r0, =0x030022F8
	add r3, r3, r0
	mov r2, #0
	ldrsh r0, [r3, r2]
	neg r0, r0
	add r0, #0x50
	lsl r0, r0, #8
	add r1, r1, r0
	str r1, [r4]
	b _0800E38C
	.align 2, 0
.pool
_0800E324:
	ldr r4, =0x03003D30
	add r4, r4, r7
	mov r8, r4
	ldr r5, =0x03002308
	ldr r3, [sp]
	add r3, #2
	lsl r3, r3, #1
	add r5, r3, r5
	mov r4, #0
	ldrsh r0, [r5, r4]
	neg r0, r0
	sub r0, #0x78
	ldr r2, [sp, #4]
	ldr r1, [r2]
	add r2, r1, #0
	mul r2, r0, r2
	ldr r0, =0x030022F8
	add r3, r3, r0
	mov r4, #0
	ldrsh r0, [r3, r4]
	neg r0, r0
	sub r0, #0x50
	mov r4, sb
	ldr r1, [r4]
	mul r0, r1, r0
	add r2, r2, r0
	mov r0, #0xf0
	lsl r0, r0, #7
	add r2, r2, r0
	mov r1, r8
	str r2, [r1]
	ldr r4, =0x03003BF0
	add r4, r7, r4
	mov r2, #0
	ldrsh r0, [r5, r2]
	neg r0, r0
	sub r0, #0x78
	mov r2, sl
	ldr r1, [r2]
	add r2, r1, #0
	mul r2, r0, r2
	mov r1, #0
	ldrsh r0, [r3, r1]
	neg r0, r0
	sub r0, #0x50
	ldr r1, [r6]
	mul r0, r1, r0
	add r2, r2, r0
	mov r0, #0xa0
	lsl r0, r0, #7
	add r2, r2, r0
	str r2, [r4]
_0800E38C:
	ldr r0, [sp]
	add r0, #1
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	str r0, [sp]
	cmp r0, #1
	bhi _0800E39C
	b _0800E1B0
_0800E39C:
	add sp, #8
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global resetAffineTransformations
resetAffineTransformations: @ 0x0800E3BC
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	mov r3, #0
	ldr r0, =0x030022F8
	mov r8, r0
	ldr r7, =0x03003D68
	mov ip, r7
	ldr r0, =0x03003D38
	mov sb, r0
	mov r6, #0
	mov r2, #0
	mov r4, #0x80
	lsl r4, r4, #1
	ldr r5, =0x03002308
_0800E3DC:
	lsl r1, r3, #1
	add r0, r1, r5
	strh r2, [r0]
	mov r7, r8
	add r0, r1, r7
	strh r2, [r0]
	mov r7, ip
	add r0, r1, r7
	strh r2, [r0]
	mov r7, sb
	add r0, r1, r7
	strh r2, [r0]
	cmp r3, #1
	bhi _0800E436
	ldr r0, =0x030022F0
	add r0, r1, r0
	strh r2, [r0]
	ldr r0, =0x03002300
	add r0, r1, r0
	strh r4, [r0]
	ldr r0, =0x03002310
	add r0, r1, r0
	strh r4, [r0]
	ldr r0, =0x03003D30
	lsl r1, r3, #2
	add r0, r1, r0
	str r2, [r0]
	ldr r0, =0x03003BF0
	add r0, r1, r0
	str r2, [r0]
	ldr r0, =0x03003CE0
	add r0, r1, r0
	str r4, [r0]
	ldr r0, =0x03003D10
	add r0, r1, r0
	str r2, [r0]
	ldr r0, =0x03003D80
	add r0, r1, r0
	str r2, [r0]
	ldr r0, =0x03003D28
	add r1, r1, r0
	str r4, [r1]
	ldr r0, =0x03003D64
	add r0, r3, r0
	strb r6, [r0]
_0800E436:
	add r0, r3, #1
	lsl r0, r0, #0x18
	lsr r3, r0, #0x18
	cmp r3, #3
	bls _0800E3DC
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global loadOAM_rotsca
loadOAM_rotsca: @ 0x0800E484
	push {r4, r5, r6, r7, lr}
	ldr r1, =0x03002820
	ldrb r0, [r1]
	cmp r0, #0
	beq _0800E4A8
	ldr r2, =0x040000D4
	ldr r0, =0x03002420
	str r0, [r2]
	mov r0, #0xe0
	lsl r0, r0, #0x13
	str r0, [r2, #4]
	ldrb r0, [r1]
	lsl r0, r0, #1
	mov r1, #0x84
	lsl r1, r1, #0x18
	orr r0, r1
	str r0, [r2, #8]
	ldr r0, [r2, #8]
_0800E4A8:
	mov r4, #0
	ldr r0, =0x07000006
	mov ip, r0
	ldr r7, =0x03002320
	ldr r6, =0x0700000E
	ldr r5, =0x07000016
_0800E4B4:
	lsl r3, r4, #5
	mov r0, ip
	add r1, r3, r0
	lsl r2, r4, #3
	add r2, r2, r7
	ldrh r0, [r2]
	strh r0, [r1]
	add r1, r3, r6
	ldrh r0, [r2, #2]
	strh r0, [r1]
	add r1, r3, r5
	ldrh r0, [r2, #4]
	strh r0, [r1]
	ldr r0, =0x0700001E
	add r3, r3, r0
	ldrh r0, [r2, #6]
	strh r0, [r3]
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0x1f
	bls _0800E4B4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool
