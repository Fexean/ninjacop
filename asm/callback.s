.include "asm/macros.inc"

@ see docs/callback.md


/*	void initCBList(void)
	- clears callbacks and links the first and second callbacks together

*/
thumb_func_global initCBList
initCBList: @ 0x0800DA50
	push {r4, r5, r6, lr}
	sub sp, #4
	ldr r4, =0x03002110
	add r6, r4, #0
	add r6, #0x10
	mov r5, #0
	str r5, [sp]
	ldr r2, =0x05000030
	mov r0, sp
	add r1, r4, #0
	bl Bios_memcpy
	mov r0, #1
	strb r0, [r4, #0xe]
	strh r5, [r4, #0xc]
	str r5, [r4, #4]
	str r6, [r4, #8]
	str r5, [r4]
	strb r0, [r6, #0xe]
	add r0, #0xff
	strh r0, [r6, #0xc]
	str r4, [r6, #4]
	str r5, [r6, #8]
	str r5, [r4, #0x10]
	add sp, #4
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool


/*	void InitCallbacks(void)	*/
thumb_func_global InitCallbacks
InitCallbacks: @ 0x0800DA90
	push {lr}
	bl initCBList
	ldr r0, =updateGFXRegs+1
	mov r1, #0x80
	bl addCallback
	ldr r0, =CB_UpdateOAM+1
	mov r1, #0x80
	bl addCallback
	pop {r0}
	bx r0
	.align 2, 0
.pool


/*	void run_callbacks(void)
	-executes all callback functions, called in vblank
*/
thumb_func_global run_callbacks
run_callbacks: @ 0x0800DAB4
	push {r4, r5, lr}
	ldr r0, =0x03002110
	ldr r4, [r0, #8]
	add r0, #0x10
	cmp r4, r0
	beq _0800DAD2
	add r5, r0, #0
_0800DAC2:
	ldr r0, [r4]
	cmp r0, #0
	beq _0800DACC
	bl call_r0
_0800DACC:
	ldr r4, [r4, #8]
	cmp r4, r5
	bne _0800DAC2
_0800DAD2:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

/*	void addCallback(void* callbackFunc, u8 priority)	
	-adds a callback function
*/
thumb_func_global addCallback
addCallback: @ 0x0800DADC
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	add r4, r0, #0
	lsl r1, r1, #0x18
	lsr r7, r1, #0x18
	bl findCallback
	add r5, r0, #0
	add r6, r5, #0
	cmp r6, #0
	bne _0800DB20
	bl findFirstFreeCB
	add r5, r0, #0
	cmp r5, #0
	beq _0800DB20
	ldr r0, =0x04000208
	mov r8, r0
	strh r6, [r0]
	add r0, r7, #0
	bl find_C_StructList
	ldr r1, [r0, #4]
	str r5, [r1, #8]
	str r5, [r0, #4]
	str r1, [r5, #4]
	str r0, [r5, #8]
	str r4, [r5]
	strh r7, [r5, #0xc]
	mov r0, #1
	strb r0, [r5, #0xe]
	mov r1, r8
	strh r0, [r1]
_0800DB20:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool


/*	void deleteCallback(void* callbackFunc)
-deletes a callback that uses callbackFunc
*/
thumb_func_global deleteCallback
deleteCallback: @ 0x0800DB30
	push {r4, lr}
	bl findCallback
	add r4, r0, #0
	cmp r4, #0
	beq _0800DB54
	ldr r3, =0x04000208
	mov r2, #0
	strh r2, [r3]
	ldr r1, [r4, #4]
	ldr r0, [r4, #8]
	str r0, [r1, #8]
	ldr r1, [r4, #8]
	ldr r0, [r4, #4]
	str r0, [r1, #4]
	strb r2, [r4, #0xe]
	mov r0, #1
	strh r0, [r3]
_0800DB54:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global unused_sub_800DB60
unused_sub_800DB60: @ 0x0800DB60
	lsl r0, r0, #0x18
	lsr r3, r0, #0x18
	ldr r2, =0x03002120
	ldr r1, [r2, #4]
	add r0, r2, #0
	sub r0, #0x10
	cmp r1, r0
	beq _0800DB8C
	ldrh r0, [r1, #0xc]
	cmp r0, r3
	bls _0800DB8C
	ldr r0, [r1, #4]
	str r2, [r0, #8]
	ldr r0, [r1, #4]
	str r0, [r2, #4]
	mov r0, #0
	strb r0, [r1, #0xe]
	mov r0, #1
	b _0800DB8E
	.align 2, 0
.pool
_0800DB8C:
	mov r0, #0
_0800DB8E:
	bx lr


/*	struct callback* findFirstFreeCB(void)
-returns first unused callback structure
*/

thumb_func_global findFirstFreeCB
findFirstFreeCB: @ 0x0800DB90
	mov r2, #2
	ldr r0, =0x03002110
	add r1, r0, #0
	add r1, #0x20
_0800DB98:
	ldrb r0, [r1, #0xe]
	cmp r0, #0
	bne _0800DBA8
	add r0, r1, #0
	b _0800DBB2
	.align 2, 0
.pool
_0800DBA8:
	add r1, #0x10
	add r2, #1
	cmp r2, #0xb
	ble _0800DB98
	mov r0, #0
_0800DBB2:
	bx lr


/*	struct callback* find_C_StructList(u8 priority)
-returns first callback with a higher priority than the argument
*/
thumb_func_global find_C_StructList
find_C_StructList: @ 0x0800DBB4
	lsl r0, r0, #0x18
	lsr r2, r0, #0x18
	ldr r0, =0x03002110
	ldr r1, [r0, #8]
	add r0, #0x10
	cmp r1, r0
	beq _0800DBDA
	add r3, r0, #0
_0800DBC4:
	ldrh r0, [r1, #0xc]
	cmp r0, r2
	bls _0800DBD4
	add r0, r1, #0
	b _0800DBDC
	.align 2, 0
.pool
_0800DBD4:
	ldr r1, [r1, #8]
	cmp r1, r3
	bne _0800DBC4
_0800DBDA:
	ldr r0, =0x03002120
_0800DBDC:
	bx lr
	.align 2, 0
.pool


/*	struct callback* findCallback(void* func)
-returns pointer to a callback that has a specific function or 0 if one doesn't exist
*/
thumb_func_global findCallback
findCallback: @ 0x0800DBE4
	add r2, r0, #0
	ldr r0, =0x03002110
	ldr r1, [r0, #8]
	add r0, #0x10
	cmp r1, r0
	beq _0800DC0E
	add r3, r0, #0
_0800DBF2:
	ldr r0, [r1]
	cmp r0, r2
	bne _0800DC08
	ldrb r0, [r1, #0xe]
	cmp r0, #0
	beq _0800DC08
	add r0, r1, #0
	b _0800DC10
	.align 2, 0
.pool
_0800DC08:
	ldr r1, [r1, #8]
	cmp r1, r3
	bne _0800DBF2
_0800DC0E:
	mov r0, #0
_0800DC10:
	bx lr
	.align 2, 0
