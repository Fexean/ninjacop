.include "asm/macros.inc"

@See docs/task.md


/*	void initTasks(void)	*/
thumb_func_global initTasks
initTasks: @ 0x080165FC
	push {lr}
	sub sp, #4
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r2, =0x040000D4
	str r1, [r2]
	ldr r0, =0x0201AF00
	str r0, [r2, #4]
	ldr r1, =0x810027F0
	str r1, [r2, #8]
	ldr r1, [r2, #8]
	mov r2, #0
	str r2, [r0]
	add r1, r0, #0
	add r1, #0x50
	str r1, [r0, #4]
	str r0, [r0, #0x50]
	str r2, [r1, #4]
	mov r1, #0
	ldr r2, =0x00004FA8
	add r3, r0, r2
	add r2, r0, #0
_0801662A:
	lsl r0, r1, #2
	add r0, r0, r1
	lsl r0, r0, #4
	add r0, r0, r2
	strb r1, [r0, #0x10]
	add r0, r1, #1
	lsl r0, r0, #0x18
	lsr r1, r0, #0x18
	cmp r1, #0x31
	bls _0801662A
	add r1, r3, #4
	add r0, r3, #0
	bl weirdAlloc
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool


/*	void runTasks(void)
	-runs tasks that have clear kill & pause bits
*/
thumb_func_global runTasks
runTasks: @ 0x0801665C
	push {r4, r5, r6, r7, lr}
	ldr r1, =0x0201AF00
	mov r0, #0xfa
	lsl r0, r0, #4
	add r3, r1, r0
	ldr r0, [r1, #4]
	str r0, [r3]
	add r2, r1, #0
	add r2, #0x50
	cmp r0, r2
	beq _080166F6
	add r6, r1, #0
	add r4, r3, #0
	add r7, r2, #0
_08016678:
	ldr r2, [r4]
	ldrb r1, [r2, #0xc]
	mov r0, #0xa
	and r0, r1
	cmp r0, #0
	bne _080166EC
	ldrh r0, [r2, #0x12]
	cmp r0, #0
	beq _08016694
	sub r0, #1
	strh r0, [r2, #0x12]
	b _080166EC
	.align 2, 0
.pool
_08016694:
	ldr r1, =0x00004FB4
	add r5, r6, r1
	add r0, r5, #0
	bl setjmp
	cmp r0, #0
	bne _080166D0
	ldr r1, [r4]
	ldr r0, [r1, #8]
	cmp r0, #0
	bne _080166B2
	ldr r2, =0x00004FAC
	add r0, r6, r2
	ldr r0, [r0]
	str r0, [r1, #0x4c]
_080166B2:
	ldr r0, [r4]
	add r0, #0x24
	mov r1, #1
	bl runTask
	add r0, r5, #0
	mov r1, #1
	neg r1, r1
	bl longjmp
	b _080166EC
	.align 2, 0
.pool
_080166D0:
	cmp r0, #0
	ble _080166EC
	ldr r2, [r4]
	ldr r0, [r2, #8]
	cmp r0, #0
	bne _080166EC
	ldr r1, =0x00004FA8
	add r0, r6, r1
	ldr r1, [r0]
	str r1, [r2, #8]
	ldr r2, =0x00004FAC
	add r1, r6, r2
	bl weirdAlloc
_080166EC:
	ldr r0, [r4]
	ldr r0, [r0, #4]
	str r0, [r4]
	cmp r0, r7
	bne _08016678
_080166F6:
	bl manageTasks
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

/*	
	struct task* createTask(void* taskFunction, u8 priority)
	- creates a new task with a function
	- inUse bit is set and data, linked_task, delay & stack set to 0
*/
thumb_func_global createTask
createTask: @ 0x08016708
	push {r4, r5, r6, lr}
	add r6, r0, #0
	lsl r1, r1, #0x18
	lsr r5, r1, #0x18
	add r0, r5, #0
	bl findFreeTask
	add r4, r0, #0
	cmp r4, #0
	beq _0801675C
	mov r0, #0
	mov r1, #1
	strb r1, [r4, #0xc]
	strb r5, [r4, #0xe]
	strb r0, [r4, #0xd]
	strh r0, [r4, #0x12]
	str r0, [r4, #8]
	str r0, [r4, #0x14]
	str r0, [r4, #0x18]
	str r0, [r4, #0x1c]
	str r0, [r4, #0x20]
	add r0, r4, #0
	bl addTask
	add r0, r4, #0
	add r0, #0x24
	bl setjmp
	str r6, [r4, #0x24]
	ldr r1, =0x0201AF00
	ldr r0, =0x00004FB0
	add r1, r1, r0
	ldrb r0, [r1]
	add r0, #1
	strb r0, [r1]
	add r0, r4, #0
	b _0801675E
	.align 2, 0
.pool
_0801675C:
	mov r0, #0
_0801675E:
	pop {r4, r5, r6}
	pop {r1}
	bx r1

/*	void endCurrentTask()
	-ends the task that is being executed
*/
thumb_func_global endCurrentTask
endCurrentTask: @ 0x08016764
	push {lr}
	ldr r0, =0x0201AF00
	mov r1, #0xfa
	lsl r1, r1, #4
	add r0, r0, r1
	ldr r0, [r0]
	bl endTask
	pop {r0}
	bx r0
	.align 2, 0
.pool

/*	void endTask(struct task* t)
	-sets the task's kill bit
*/
thumb_func_global endTask
endTask: @ 0x0801677C
	ldrb r2, [r0, #0xc]
	mov r1, #8
	orr r1, r2
	strb r1, [r0, #0xc]
	bx lr
	.align 2, 0

/*	void endPriorityOrHigherTask(u8 priority)
	-calls endTask with tasks that have a priority larger or equal to the argument
*/
thumb_func_global endPriorityOrHigherTask
endPriorityOrHigherTask: @ 0x08016788
	push {r4, r5, r6, lr}
	lsl r0, r0, #0x18
	lsr r5, r0, #0x18
	ldr r0, =0x0201AF50
	ldr r4, [r0]
	add r1, r0, #0
	sub r1, #0x50
	cmp r4, r1
	beq _080167B4
	ldrb r0, [r4, #0xe]
	cmp r0, r5
	blo _080167B4
	add r6, r1, #0
_080167A2:
	add r0, r4, #0
	bl endTask
	ldr r4, [r4]
	cmp r4, r6
	beq _080167B4
	ldrb r0, [r4, #0xe]
	cmp r0, r5
	bhs _080167A2
_080167B4:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool


/*	void unused_sub_80167C0(void* taskFunction)
	-creates a task with currentTask's priority and sets its linked_task to currentTasks field_10 and sets pause bit on currentTask
*/
thumb_func_global unused_sub_80167C0
unused_sub_80167C0: @ 0x080167C0
	push {r4, lr}
	ldr r1, =0x0201AF00
	mov r2, #0xfa
	lsl r2, r2, #4
	add r4, r1, r2
	ldr r1, [r4]
	ldrb r1, [r1, #0xe]
	bl createTask
	add r3, r0, #0
	cmp r3, #0
	beq _080167E8
	ldr r0, [r4]
	ldrb r0, [r0, #0x10]
	strb r0, [r3, #0xd]
	ldr r2, [r4]
	ldrb r1, [r2, #0xc]
	mov r0, #2
	orr r0, r1
	strb r0, [r2, #0xc]
_080167E8:
	add r0, r3, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

/*	void setCurrentTaskDelay(u16 delay)	*/
thumb_func_global setCurrentTaskDelay
setCurrentTaskDelay: @ 0x080167F4
	push {lr}
	add r1, r0, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r0, =0x0201AF00
	mov r2, #0xfa
	lsl r2, r2, #4
	add r0, r0, r2
	ldr r0, [r0]
	bl setTaskDelay
	pop {r0}
	bx r0
	.align 2, 0
.pool


/*	void setTaskDelay(struct task* task, u16 delay)	*/
thumb_func_global setTaskDelay
setTaskDelay: @ 0x08016814
	strh r1, [r0, #0x12]
	bx lr

/*	void setCurrentTaskFunc(void* taskFunction)	*/
thumb_func_global setCurrentTaskFunc
setCurrentTaskFunc: @ 0x08016818
	push {lr}
	add r1, r0, #0
	ldr r0, =0x0201AF00
	mov r2, #0xfa
	lsl r2, r2, #4
	add r0, r0, r2
	ldr r0, [r0]
	bl setTaskFunc
	pop {r0}
	bx r0
	.align 2, 0
.pool

/*	void setTaskFunc(struct task* t, void* taskFunction)	*/
thumb_func_global setTaskFunc
setTaskFunc: @ 0x08016834
	push {lr}
	str r1, [r0, #0x24]
	bl taskTrySetFree4K
	pop {r0}
	bx r0

/*	void unused_setCurrentTaskPriority(u8 priority)	*/
thumb_func_global unused_setCurrentTaskPriority
unused_setCurrentTaskPriority: @ 0x08016840
	push {lr}
	add r1, r0, #0
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	ldr r0, =0x0201AF00
	mov r2, #0xfa
	lsl r2, r2, #4
	add r0, r0, r2
	ldr r0, [r0]
	bl setTaskPriority
	pop {r0}
	bx r0
	.align 2, 0
.pool


/*	void setTaskPriority(struct task* task, u8 priority)	
	-actually sets newPriority and changePriority flag, manageTasks handles the rest
*/
thumb_func_global setTaskPriority
setTaskPriority: @ 0x08016860
	add r2, r0, #0
	lsl r1, r1, #0x18
	lsr r1, r1, #0x18
	ldrb r0, [r2, #0xe]
	cmp r0, r1
	beq _08016876
	strb r1, [r2, #0xf]
	ldrb r1, [r2, #0xc]
	mov r0, #0x10
	orr r0, r1
	strb r0, [r2, #0xc]
_08016876:
	bx lr


/*	void manageTasks(void)
	-runs at the end of runTasks, goes through all tasks and:
		-calls freeTask if kill bit is set
		-calls free4kBlock and sets _4kBlock to 0 if free4k is set
		-moves task to a new place in the task list if changePriority is set
*/
thumb_func_global manageTasks
manageTasks: @ 0x08016878
	push {r4, r5, lr}
	ldr r0, =0x0201AF00
	ldr r4, [r0, #4]
	add r0, #0x50
	cmp r4, r0
	beq _080168EA
_08016884:
	ldrb r1, [r4, #0xc]
	mov r0, #8
	and r0, r1
	cmp r0, #0
	beq _08016894
	add r0, r4, #0
	bl freeTask
_08016894:
	ldrb r1, [r4, #0xc]
	mov r0, #4
	and r0, r1
	cmp r0, #0
	beq _080168B2
	mov r0, #0xfb
	and r0, r1
	mov r5, #0
	strb r0, [r4, #0xc]
	ldr r0, [r4, #8]
	cmp r0, #0
	beq _080168B2
	bl free4kBlock
	str r5, [r4, #8]
_080168B2:
	ldr r5, [r4, #4]
	ldrb r1, [r4, #0xc]
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	beq _080168E2
	ldrb r0, [r4, #0xe]
	ldrb r1, [r4, #0xf]
	cmp r0, r1
	beq _080168DA
	ldr r0, [r4]
	str r5, [r0, #4]
	ldr r1, [r4, #4]
	ldr r0, [r4]
	str r0, [r1]
	ldrb r0, [r4, #0xf]
	strb r0, [r4, #0xe]
	add r0, r4, #0
	bl addTask
_080168DA:
	ldrb r1, [r4, #0xc]
	mov r0, #0xef
	and r0, r1
	strb r0, [r4, #0xc]
_080168E2:
	add r4, r5, #0
	ldr r0, =0x0201AF50
	cmp r4, r0
	bne _08016884
_080168EA:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool


/*	void freeTask(struct task* task)
- clears inUse, changePriority and kill bits, and unlinks the task from the task list
- if linked_task is non-zero, tasks[linked_task].pause = 0
*/
thumb_func_global freeTask
freeTask: @ 0x080168F8
	push {r4, lr}
	add r4, r0, #0
	ldrb r0, [r4, #0xc]
	mov r1, #0xe6
	and r1, r0
	strb r1, [r4, #0xc]
	ldrb r0, [r4, #0xd]
	cmp r0, #0
	beq _0801691C
	ldr r2, =0x0201AF00
	lsl r1, r0, #2
	add r1, r1, r0
	lsl r1, r1, #4
	add r1, r1, r2
	ldrb r2, [r1, #0xc]
	mov r0, #0xfd
	and r0, r2
	strb r0, [r1, #0xc]
_0801691C:
	add r0, r4, #0
	bl taskTrySetFree4K
	ldr r1, [r4]
	ldr r0, [r4, #4]
	str r0, [r1, #4]
	ldr r1, [r4, #4]
	ldr r0, [r4]
	str r0, [r1]
	ldr r1, =0x0201AF00
	ldr r0, =0x00004FB0
	add r1, r1, r0
	ldrb r0, [r1]
	sub r0, #1
	strb r0, [r1]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool


/*	u8* weirdAlloc(u8** start, u8** end)
- calls alloc4kRam, writes its result at start and (result+4092) at end
- returns result
*/
thumb_func_global weirdAlloc
weirdAlloc: @ 0x08016948
	push {r4, r5, lr}
	add r4, r0, #0
	add r5, r1, #0
	bl alloc4kRam
	str r0, [r4]
	ldr r2, =0x00000FFC
	add r1, r0, r2
	str r1, [r5]
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
.pool


/*	void taskTrySetFree4K(struct task* task)
- sets task->free4k bit if task->_4kBlock is non-zero
*/
thumb_func_global taskTrySetFree4K
taskTrySetFree4K: @ 0x08016964
	add r2, r0, #0
	ldr r0, [r2, #8]
	cmp r0, #0
	beq _08016974
	ldrb r1, [r2, #0xc]
	mov r0, #4
	orr r0, r1
	strb r0, [r2, #0xc]
_08016974:
	bx lr
	.align 2, 0


/*	struct task* findFreeTask(void)
- finds and returns a task with inUse and free4k bits cleared
- returns 0 if one doesn't exist
*/
thumb_func_global findFreeTask
findFreeTask: @ 0x08016978
	push {r4, r5, lr}
	mov r3, #2
	ldr r4, =0x0201AF00
	mov r5, #5
_08016980:
	lsl r0, r3, #2
	add r0, r0, r3
	lsl r0, r0, #4
	add r2, r0, r4
	ldrb r1, [r2, #0xc]
	add r0, r5, #0
	and r0, r1
	cmp r0, #0
	bne _0801699C
	add r0, r2, #0
	b _080169A8
	.align 2, 0
.pool
_0801699C:
	add r0, r3, #1
	lsl r0, r0, #0x18
	lsr r3, r0, #0x18
	cmp r3, #0x31
	bls _08016980
	mov r0, #0
_080169A8:
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0


/*
	void addTask(struct task* t)
	-links the task to the doubly linked task list at the correct location based on its priority
*/
thumb_func_global addTask
addTask: @ 0x080169B0
	push {r4, lr}
	add r3, r0, #0
	ldr r0, =0x0201AF00
	ldr r1, [r0, #4]
	add r4, r0, #0
	add r4, #0x50
	cmp r1, r4
	beq _080169D4
	ldrb r0, [r1, #0xe]
	ldrb r2, [r3, #0xe]
	cmp r0, r2
	bhi _080169D4
_080169C8:
	ldr r1, [r1, #4]
	cmp r1, r4
	beq _080169D4
	ldrb r0, [r1, #0xe]
	cmp r0, r2
	bls _080169C8
_080169D4:
	str r1, [r3, #4]
	ldr r0, [r1]
	str r0, [r3]
	str r3, [r0, #4]
	ldr r0, [r3, #4]
	str r3, [r0]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool


/*	u8* alloc4kRam(void)	
	-returns pointer to a 4 kilobyte block of ram, there are 4 blocks available which can be allocated
	-returns 0 if no blocks are available
*/
thumb_func_global alloc4kRam
alloc4kRam: @ 0x080169EC
	push {r4, r5, lr}
	mov r2, #0
	ldr r3, =0x0201BEA4
	mov r4, #1
	add r5, r3, #4
_080169F6:
	add r1, r2, r3
	ldrb r0, [r1]
	cmp r0, #0
	bne _08016A0C
	strb r4, [r1]
	lsl r0, r2, #0xc
	add r0, r0, r5
	b _08016A18
	.align 2, 0
.pool
_08016A0C:
	add r0, r2, #1
	lsl r0, r0, #0x18
	lsr r2, r0, #0x18
	cmp r2, #3
	bls _080169F6
	mov r0, #0
_08016A18:
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0


/*	void free4kBlock(u8* blockAddr) 
	-frees a memory block allocated by the above function
*/
thumb_func_global free4kBlock
free4kBlock: @ 0x08016A20
	push {r4, r5, lr}
	add r3, r0, #0
	mov r1, #0
	ldr r2, =0x0201BEA8
	sub r5, r2, #4
	mov r4, #0
_08016A2C:
	lsl r0, r1, #0xc
	add r0, r0, r2
	cmp r3, r0
	bne _08016A38
	add r0, r5, r1
	strb r4, [r0]
_08016A38:
	add r0, r1, #1
	lsl r0, r0, #0x18
	lsr r1, r0, #0x18
	cmp r1, #3
	bls _08016A2C
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool


/*	void sub_8016A4C(u8 priority)
	-sets task.delay to 1 for each task where: task.priority >= priority
*/
thumb_func_global sub_8016A4C
sub_8016A4C: @ 0x08016A4C
	push {r4, r5, r6, lr}
	lsl r0, r0, #0x18
	lsr r5, r0, #0x18
	ldr r0, =0x0201AF50
	ldr r4, [r0]
	add r1, r0, #0
	sub r1, #0x50
	cmp r4, r1
	beq _08016A7A
	ldrb r0, [r4, #0xe]
	cmp r0, r5
	blo _08016A7A
	add r6, r1, #0
_08016A66:
	add r0, r4, #0
	mov r1, #1
	bl setTaskDelay
	ldr r4, [r4]
	cmp r4, r6
	beq _08016A7A
	ldrb r0, [r4, #0xe]
	cmp r0, r5
	bhs _08016A66
_08016A7A:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool


/*	void sub_08016A84(u8 min, u8 max)
	-sets task.delay to 1 for each task where: min <= task.priority <= max
*/
thumb_func_global sub_08016A84
sub_08016A84: @ 0x08016A84
	push {r4, r5, r6, lr}
	lsl r0, r0, #0x18
	lsr r5, r0, #0x18
	lsl r1, r1, #0x18
	lsr r6, r1, #0x18
	ldr r0, =0x0201AF50
	ldr r4, [r0]
	sub r0, #0x50
	b _08016AAC
	.align 2, 0
.pool
_08016A9C:
	cmp r0, r6
	bhi _08016AA8
	add r0, r4, #0
	mov r1, #1
	bl setTaskDelay
_08016AA8:
	ldr r4, [r4]
	ldr r0, =0x0201AF00
_08016AAC:
	cmp r4, r0
	beq _08016AB6
	ldrb r0, [r4, #0xe]
	cmp r0, r5
	bhs _08016A9C
_08016AB6:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

