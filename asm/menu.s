.include "asm/macros.inc"

@completely disassembled

@ Menu after title screen


thumb_func_global cb_loadMenu
cb_loadMenu: @ 0x0802D7E4
	push {r4, r5, r6, r7, lr}
	sub sp, #4
	ldr r4, =0x03003CD4
	mov r1, #0x82
	lsl r1, r1, #5
	add r0, r1, #0
	strh r0, [r4]
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r0, =menuNinjaPal
	mov r2, #0xa0
	lsl r2, r2, #0x13
	mov r1, #1
	bl memcpy_pal
	ldr r0, =menuNinjaTilesCompressed
	mov r1, #0xc0
	lsl r1, r1, #0x13
	bl Bios_LZ77_16_2
	ldr r0, =menuNinjaMapCompressed
	ldr r1, =0x0600F000
	mov r6, #0
	str r6, [sp]
	mov r2, #0
	mov r3, #0
	bl LZ77_mapCpy
	ldr r5, =0x03003D00
	ldr r0, =0x00001E02
	strh r0, [r5, #4]
	ldrh r0, [r4]
	mov r2, #0x80
	lsl r2, r2, #3
	add r1, r2, #0
	orr r0, r1
	strh r0, [r4]
	ldr r0, =menuBgPal
	ldr r2, =0x05000020
	mov r1, #1
	bl memcpy_pal
	ldr r0, =menuBgTilesCompressed
	ldr r1, =0x06000C00
	bl Bios_LZ77_16_2
	ldr r0, =menuBgMapCompressed
	ldr r1, =0x0600F800
	mov r7, #1
	str r7, [sp]
	mov r2, #0
	mov r3, #0x60
	bl LZ77_mapCpy
	ldr r0, =0x00001F02
	strh r0, [r5, #6]
	ldrh r0, [r4]
	mov r2, #0x80
	lsl r2, r2, #4
	add r1, r2, #0
	orr r0, r1
	strh r0, [r4]
	ldr r1, =0x03000580
	ldr r0, =0x03003D40
	ldrb r0, [r0]
	strh r0, [r1]
	ldr r1, =0x0300058E
	strh r6, [r1]
	ldr r0, =0x03003C00
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	cmp r0, #0
	bge _0802D8D4
	mov r0, #3
	strh r0, [r1]
	b _0802D8D6
	.align 2, 0
.pool
_0802D8D4:
	strh r7, [r1]
_0802D8D6:
	bl sub_0802DB58
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0802D8EA
	ldr r0, =0x0300058E
	ldrh r1, [r0]
	mov r2, #4
	orr r1, r2
	strh r1, [r0]
_0802D8EA:
	ldr r0, =0x03000584
	mov r1, #0
	strh r1, [r0]
	ldr r0, =0x03000590
	strh r1, [r0]
	ldr r0, =0x03000598
	mov r1, #0
	bl allocFont
	ldr r0, =0x030005A0
	mov r1, #1
	bl allocFont
	ldr r0, =0x030005A8
	mov r1, #2
	bl allocFont
	bl sub_0802E128
	bl sub_0802EE4C
	bl sub_0802DF68
	mov r0, #0xf0
	mov r1, #0x12
	bl setSelectModeStringPos
	bl sub_0802DFD8
	ldr r0, =0x03003C00
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	cmp r0, #0
	bge _0802D984
	ldr r4, =unk_820E330
	ldr r1, [r4, #4]
	add r1, #9
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r0, #0xf0
	bl sub_0802E00C
	bl sub_0802E048
	ldr r1, [r4, #0xc]
	add r1, #9
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r0, #0xf0
	bl sub_0802E07C
	bl sub_0802E0B8
	ldr r1, [r4, #0x14]
	add r1, #9
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r0, #0xf0
	bl sub_0802E0EC
	b _0802D9B8
	.align 2, 0
.pool
_0802D984:
	ldr r4, =unk_820E318
	ldr r1, [r4, #4]
	add r1, #9
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r0, #0xf0
	bl sub_0802E00C
	bl sub_0802E048
	ldr r1, [r4, #0xc]
	add r1, #9
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r0, #0xf0
	bl sub_0802E07C
	bl sub_0802E0B8
	ldr r1, [r4, #0x14]
	add r1, #9
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r0, #0xf0
	bl sub_0802E0EC
_0802D9B8:
	ldr r0, =0x03003D40
	ldrb r0, [r0, #0x10]
	cmp r0, #0
	beq _0802D9C6
	ldr r1, =0x03000584
	mov r0, #4
	strh r0, [r1]
_0802D9C6:
	ldr r0, =0x03003C00
	add r0, #0xce
	ldrh r0, [r0]
	cmp r0, #0
	beq _0802D9D4
	bl sub_0802EEE0
_0802D9D4:
	ldr r0, =(cb_menu_switchcase+1)
	bl setCurrentTaskFunc
	add sp, #4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global cb_menu_switchcase
cb_menu_switchcase: @ 0x0802D9F8
	push {lr}
	ldr r1, =menuFunctions
	ldr r0, =0x03000584
	ldrh r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl call_r0
	lsl r0, r0, #0x10
	mov r1, #0x80
	lsl r1, r1, #9
	add r0, r0, r1
	asr r0, r0, #0x10
	cmp r0, #7
	bls _0802DA1A
	b _0802DB48
_0802DA1A:
	lsl r0, r0, #2
	ldr r1, =_0802DA30
	add r0, r0, r1
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
.pool
_0802DA30: @ jump table
	.4byte _0802DB30 @ case 0
	.4byte _0802DB48 @ case 1
	.4byte _0802DA50 @ case 2
	.4byte _0802DA6C @ case 3
	.4byte _0802DAE8 @ case 4
	.4byte _0802DB00 @ case 5
	.4byte _0802DB18 @ case 6
	.4byte _0802DB24 @ case 7
_0802DA50:
	ldr r0, =0x03000590
	ldrh r0, [r0]
	bl sub_0802E314
	ldr r1, =0x03000584
	ldrh r0, [r1]
	add r0, #1
	strh r0, [r1]
	b _0802DB50
	.align 2, 0
.pool
_0802DA6C:
	ldr r2, =0x03003D40
	ldr r0, =0x03000580
	ldrh r0, [r0]
	mov r1, #0
	strb r0, [r2]
	ldr r0, =0x03000584
	strh r1, [r0]
	ldrb r0, [r2]
	cmp r0, #1
	beq _0802DAB0
	cmp r0, #1
	ble _0802DA88
	cmp r0, #2
	beq _0802DAD4
_0802DA88:
	ldr r0, =0x03000582
	ldrh r0, [r0]
	strb r0, [r2, #0x11]
	mov r0, #0
	strb r0, [r2, #9]
	str r1, [r2, #0x1c]
	ldr r0, =(cb_loadLevelSelect+1)
	bl setCurrentTaskFunc
	b _0802DB50
	.align 2, 0
.pool
_0802DAB0:
	ldr r1, =0x03003C00
	ldrb r0, [r1, #3]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1e
	strb r0, [r2, #0x11]
	add r1, #0xcc
	ldrh r1, [r1]
	mov r0, #0x64
	mul r0, r1, r0
	str r0, [r2, #0x1c]
	ldr r0, =(cb_loadContinueScreen+1)
	bl setCurrentTaskFunc
	b _0802DB50
	.align 2, 0
.pool
_0802DAD4:
	mov r0, #1
	strb r0, [r2, #0x11]
	bl sub_0802CC7C
	ldr r0, =(cb_loadTimeTrialMenu+1)
	bl setCurrentTaskFunc
	b _0802DB50
	.align 2, 0
.pool
_0802DAE8:
	ldr r0, =0x03000590
	ldrh r0, [r0]
	bl sub_0802E314
	ldr r1, =0x03000584
	mov r0, #8
	strh r0, [r1]
	b _0802DB50
	.align 2, 0
.pool
_0802DB00:
	ldr r0, =0x03000590
	ldrh r0, [r0]
	bl sub_0802E314
	ldr r1, =0x03000584
	mov r0, #2
	strh r0, [r1]
	b _0802DB50
	.align 2, 0
.pool
_0802DB18:
	ldr r1, =0x03000584
	mov r0, #0xa
	strh r0, [r1]
	b _0802DB50
	.align 2, 0
.pool
_0802DB24:
	ldr r1, =0x03000584
	mov r0, #5
	strh r0, [r1]
	b _0802DB50
	.align 2, 0
.pool
_0802DB30:
	ldr r0, =0x03000584
	mov r1, #0
	strh r1, [r0]
	ldr r0, =(cb_loadTitleScreen+1)
	bl setCurrentTaskFunc
	b _0802DB50
	.align 2, 0
.pool
_0802DB48:
	ldr r0, =0x03000590
	ldrh r0, [r0]
	bl sub_0802E314
_0802DB50:
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802DB58
sub_0802DB58: @ 0x0802DB58
	mov r1, #0
	ldr r2, =0x03003C05
_0802DB5C:
	add r0, r1, r2
	ldrb r0, [r0]
	cmp r0, #0
	beq _0802DB6C
	mov r0, #1
	b _0802DB78
	.align 2, 0
.pool
_0802DB6C:
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	cmp r1, #5
	bls _0802DB5C
	mov r0, #0
_0802DB78:
	bx lr
	.align 2, 0

thumb_func_global return_1
return_1: @ 0x0802DB7C
	mov r0, #1
	bx lr

thumb_func_global menuState1
menuState1: @ 0x0802DB80
	push {lr}
	bl menuState2
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0802DB9A
	mov r0, #0
	b _0802DBAE
_0802DB9A:
	ldr r1, =0x03003D40
	ldr r0, =0x03003C00
	ldrb r0, [r0, #3]
	lsl r0, r0, #0x1c
	lsr r0, r0, #0x1e
	strb r0, [r1, #0x11]
	mov r0, #2
	bl playSong
	mov r0, #1
_0802DBAE:
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global menuState2
menuState2: @ 0x0802DBBC
	push {r4, lr}
	bl slideModeSelectString
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	neg r1, r0
	orr r1, r0
	lsr r4, r1, #0x1f
	bl sub_0802DC78
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0802DBDA
	mov r0, #2
	orr r4, r0
_0802DBDA:
	bl sub_0802DCD8
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0802DBEC
	mov r0, #4
	orr r4, r0
	lsl r0, r4, #0x10
	lsr r4, r0, #0x10
_0802DBEC:
	bl sub_0802DD38
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0802DBFE
	mov r0, #8
	orr r4, r0
	lsl r0, r4, #0x10
	lsr r4, r0, #0x10
_0802DBFE:
	cmp r4, #0xf
	bne _0802DC2C
	ldr r0, =0x030005B0
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	ldr r0, =0x03003C00
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	cmp r0, #0
	bge _0802DC20
	bl sub_0802EDBC
_0802DC20:
	mov r0, #1
	b _0802DC2E
	.align 2, 0
.pool
_0802DC2C:
	mov r0, #0
_0802DC2E:
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global slideModeSelectString
slideModeSelectString: @ 0x0802DC34
	push {r4, lr}
	ldr r1, =SelectModeStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0xd
	bl str_getX_ofFirstChar
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	sub r0, #0xc
	cmp r0, #0x57
	bgt _0802DC64
	mov r0, #0x58
	mov r4, #1
	b _0802DC6A
	.align 2, 0
.pool
_0802DC64:
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r4, #0
_0802DC6A:
	mov r1, #0x12
	bl setSelectModeStringPos
	add r0, r4, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global sub_0802DC78
sub_0802DC78: @ 0x0802DC78
	push {r4, lr}
	ldr r0, =0x03003C00
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	cmp r0, #0
	bge _0802DC90
	ldr r0, =unk_820E330
	b _0802DC92
	.align 2, 0
.pool
_0802DC90:
	ldr r0, =unk_820E318
_0802DC92:
	ldrh r3, [r0]
	ldr r1, =0x030005B8
	ldr r0, [r1]
	ldrh r2, [r0, #0x10]
	add r0, r2, #0
	sub r0, #0xc
	cmp r0, r3
	bge _0802DCB0
	add r2, r3, #0
	mov r4, #1
	b _0802DCB6
	.align 2, 0
.pool
_0802DCB0:
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	mov r4, #0
_0802DCB6:
	ldr r0, [r1]
	strh r2, [r0, #0x10]
	add r0, r2, #0
	add r0, #0x10
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r1, [r1]
	ldrh r1, [r1, #0x12]
	add r1, #9
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl sub_0802E00C
	add r0, r4, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global sub_0802DCD8
sub_0802DCD8: @ 0x0802DCD8
	push {r4, lr}
	ldr r0, =0x03003C00
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	cmp r0, #0
	bge _0802DCF0
	ldr r0, =unk_820E330
	b _0802DCF2
	.align 2, 0
.pool
_0802DCF0:
	ldr r0, =unk_820E318
_0802DCF2:
	ldrh r3, [r0, #8]
	ldr r1, =0x030005B8
	ldr r0, [r1, #4]
	ldrh r2, [r0, #0x10]
	add r0, r2, #0
	sub r0, #0xc
	cmp r0, r3
	bge _0802DD10
	add r2, r3, #0
	mov r4, #1
	b _0802DD16
	.align 2, 0
.pool
_0802DD10:
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	mov r4, #0
_0802DD16:
	ldr r0, [r1, #4]
	strh r2, [r0, #0x10]
	add r0, r2, #0
	add r0, #0x10
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r1, [r1, #4]
	ldrh r1, [r1, #0x12]
	add r1, #9
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl sub_0802E07C
	add r0, r4, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global sub_0802DD38
sub_0802DD38: @ 0x0802DD38
	push {r4, lr}
	ldr r0, =0x03003C00
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	cmp r0, #0
	bge _0802DD50
	ldr r0, =unk_820E330
	b _0802DD52
	.align 2, 0
.pool
_0802DD50:
	ldr r0, =unk_820E318
_0802DD52:
	ldrh r3, [r0, #0x10]
	ldr r1, =0x030005B8
	ldr r0, [r1, #8]
	ldrh r2, [r0, #0x10]
	add r0, r2, #0
	sub r0, #0xc
	cmp r0, r3
	bge _0802DD70
	add r2, r3, #0
	mov r4, #1
	b _0802DD76
	.align 2, 0
.pool
_0802DD70:
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	mov r4, #0
_0802DD76:
	ldr r0, [r1, #8]
	strh r2, [r0, #0x10]
	add r0, r2, #0
	add r0, #0x10
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r1, [r1, #8]
	ldrh r1, [r1, #0x12]
	add r1, #9
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl sub_0802E0EC
	add r0, r4, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global sub_0802DD98
sub_0802DD98: @ 0x0802DD98
	push {r4, r5, r6, lr}
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	lsl r1, r1, #0x10
	lsr r5, r1, #0x10
	mov r4, #0
_0802DDA4:
	cmp r6, #0
	beq _0802DDAC
	add r0, r5, r4
	b _0802DDB0
_0802DDAC:
	sub r0, r4, #3
	sub r0, r5, r0
_0802DDB0:
	mov r1, #3
	bl Bios_modulo
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	ldr r0, =0x0300058E
	ldrh r0, [r0]
	asr r0, r2
	mov r1, #1
	and r0, r1
	cmp r0, #0
	bne _0802DDD2
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #2
	bls _0802DDA4
_0802DDD2:
	add r0, r2, #0
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global menuState3
menuState3: @ 0x0802DDE0
	push {r4, r5, r6, lr}
	ldr r2, =0x03002080
	ldrh r1, [r2, #0xc]
	mov r4, #2
	add r0, r4, #0
	and r0, r1
	cmp r0, #0
	beq _0802DE0C
	mov r0, #0xcd
	bl playSong
	ldr r1, =0x0300058C
	mov r2, #1
	neg r2, r2
	add r0, r2, #0
	strh r0, [r1]
	mov r0, #3
	b _0802DF06
	.align 2, 0
.pool
_0802DE0C:
	mov r0, #9
	and r0, r1
	cmp r0, #0
	beq _0802DEB4
	mov r0, #0xcc
	bl playSong
	ldr r0, =0x03000580
	ldrh r0, [r0]
	cmp r0, #0
	bne _0802DEA8
	bl sub_0802EE04
	mov r4, #0xd
	ldr r5, =0x030037D0
_0802DE2A:
	lsl r0, r4, #4
	add r0, r0, r5
	bl freeSimpleSpriteHeader
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0x3f
	bls _0802DE2A
	ldr r0, =0x030005B0
	ldr r1, [r0]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	mov r4, #0
	ldr r6, =0x03003C00
	ldr r3, =0x03003D40
	ldr r5, =0x030005B8
_0802DE52:
	lsl r0, r4, #2
	add r0, r0, r5
	ldr r1, [r0]
	add r1, #0x23
	ldrb r0, [r1]
	orr r0, r2
	strb r0, [r1]
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #2
	bls _0802DE52
	ldrb r0, [r6, #3]
	lsl r0, r0, #0x19
	lsr r1, r0, #0x1d
	mov r0, #6
	and r0, r1
	cmp r0, #0
	bne _0802DEA0
	mov r2, #1
	add r0, r2, #0
	and r0, r1
	cmp r0, #0
	beq _0802DEA2
	strb r2, [r3, #0x11]
	b _0802DEA4
	.align 2, 0
.pool
_0802DEA0:
	mov r0, #2
_0802DEA2:
	strb r0, [r3, #0x11]
_0802DEA4:
	mov r0, #1
	b _0802DF06
_0802DEA8:
	ldr r0, =0x0300058C
	strh r4, [r0]
	mov r0, #3
	b _0802DF06
	.align 2, 0
.pool
_0802DEB4:
	ldrh r1, [r2, #0x14]
	mov r0, #0xc0
	and r0, r1
	cmp r0, #0
	beq _0802DF04
	ldr r5, =0x03000580
	ldrh r4, [r5]
	mov r0, #0x40
	and r0, r1
	cmp r0, #0
	beq _0802DEE0
	add r0, r4, #2
	mov r1, #3
	bl Bios_modulo
	add r1, r0, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r0, #0
	b _0802DEF0
	.align 2, 0
.pool
_0802DEE0:
	add r0, r4, #1
	mov r1, #3
	bl Bios_modulo
	add r1, r0, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r0, #1
_0802DEF0:
	bl sub_0802DD98
	strh r0, [r5]
	ldr r0, =0x03000580
	ldrh r0, [r0]
	cmp r4, r0
	beq _0802DF04
	mov r0, #0xcb
	bl playSong
_0802DF04:
	mov r0, #0
_0802DF06:
	pop {r4, r5, r6}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global menuState8
menuState8: @ 0x0802DF10
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0802DF28
	mov r0, #0
	b _0802DF2A
_0802DF28:
	mov r0, #1
_0802DF2A:
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global menuState9
menuState9: @ 0x0802DF30
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl resetAffineTransformations
	bl clearSprites
	bl clearPalBufs
	bl FreeAllPalFadeTasks
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r0, =0x0300058C
	mov r1, #0
	ldrsh r0, [r0, r1]
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_0802DF68
sub_0802DF68: @ 0x0802DF68
	push {lr}
	sub sp, #4
	ldr r1, =SelectModeStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000598
	mov r2, #0
	str r2, [sp]
	mov r2, #0xd
	mov r3, #1
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global setSelectModeStringPos
setSelectModeStringPos: @ 0x0802DF9C
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =SelectModeStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000598
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0xd
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802DFD8
sub_0802DFD8: @ 0x0802DFD8
	push {lr}
	sub sp, #4
	ldr r1, =NewGameStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005A0
	mov r2, #0
	str r2, [sp]
	mov r2, #0x17
	mov r3, #1
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802E00C
sub_0802E00C: @ 0x0802E00C
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =NewGameStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005A0
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0x17
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802E048
sub_0802E048: @ 0x0802E048
	push {lr}
	sub sp, #4
	ldr r1, =LoadGameStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005A0
	mov r2, #0
	str r2, [sp]
	mov r2, #0x23
	mov r3, #1
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802E07C
sub_0802E07C: @ 0x0802E07C
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =LoadGameStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005A0
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0x23
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802E0B8
sub_0802E0B8: @ 0x0802E0B8
	push {lr}
	sub sp, #4
	ldr r1, =TimeTrialStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005A0
	mov r2, #0
	str r2, [sp]
	mov r2, #0x2d
	mov r3, #1
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802E0EC
sub_0802E0EC: @ 0x0802E0EC
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =TimeTrialStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005A0
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0x2d
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802E128
sub_0802E128: @ 0x0802E128
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #4
	ldr r4, =0x030005D8
	ldr r0, =gScrollCursorSpritePal
	mov r1, #2
	bl LoadSpritePalette
	lsl r1, r0, #0x10
	lsr r1, r1, #0x10
	mov sb, r1
	strh r0, [r4]
	mov r0, #0xda
	bl allocateSpriteTiles
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r8, r0
	ldr r0, =gScrollCursorSpriteTiles
	mov r2, r8
	lsl r1, r2, #5
	ldr r3, =0x06010000
	add r1, r1, r3
	bl Bios_LZ77_16_2
	ldr r0, =menu_scroll_highlight_anim
	mov r1, #0
	str r1, [sp]
	mov r2, r8
	mov r3, #2
	bl allocSprite
	ldr r3, =0x030005B0
	str r0, [r3]
	add r0, #0x23
	ldrb r1, [r0]
	mov r2, #0x10
	orr r1, r2
	strb r1, [r0]
	ldr r0, [r3]
	add r0, #0x22
	mov r5, sb
	strb r5, [r0]
	ldr r0, =0x03003C00
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	cmp r0, #0
	bge _0802E1B4
	ldr r3, [r3]
	ldr r1, =unk_820E330
	b _0802E1B8
	.align 2, 0
.pool
_0802E1B4:
	ldr r3, [r3]
	ldr r1, =unk_820E318
_0802E1B8:
	ldr r2, =0x03000580
	ldrh r0, [r2]
	lsl r0, r0, #3
	add r0, r0, r1
	ldr r0, [r0]
	strh r0, [r3, #0x10]
	ldrh r0, [r2]
	lsl r0, r0, #3
	add r1, #4
	add r0, r0, r1
	ldr r0, [r0]
	strh r0, [r3, #0x12]
	mov r6, #0
	ldr r0, =dword_820E31C
	mov sl, r0
_0802E1D6:
	mov r0, #0
	str r0, [sp]
	ldr r0, =off_820CB24
	mov r1, #0
	mov r2, r8
	mov r3, #2
	bl allocSprite
	ldr r1, =0x030005B8
	lsl r3, r6, #2
	add r4, r3, r1
	str r0, [r4]
	add r0, #0x23
	ldrb r1, [r0]
	mov r5, #0x11
	neg r5, r5
	add r2, r5, #0
	and r1, r2
	strb r1, [r0]
	ldr r0, [r4]
	add r0, #0x22
	mov r1, sb
	strb r1, [r0]
	ldr r1, [r4]
	lsl r2, r6, #4
	mov r5, #0x98
	lsl r5, r5, #1
	add r0, r2, r5
	strh r0, [r1, #0x10]
	ldr r0, =0x03003C00
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	add r5, r3, #0
	add r7, r2, #0
	cmp r0, #0
	bge _0802E248
	ldr r2, [r4]
	ldr r0, =unk_820E330
	lsl r1, r6, #3
	add r0, #4
	add r0, r1, r0
	b _0802E250
	.align 2, 0
.pool
_0802E248:
	ldr r2, [r4]
	lsl r1, r6, #3
	mov r3, sl
	add r0, r1, r3
_0802E250:
	ldr r0, [r0]
	strh r0, [r2, #0x12]
	add r4, r1, #0
	mov r0, #0
	str r0, [sp]
	ldr r0, =off_820CB60
	mov r1, #0
	mov r2, r8
	mov r3, #2
	bl allocSprite
	ldr r1, =0x030005C8
	add r3, r5, r1
	str r0, [r3]
	add r0, #0x23
	ldrb r1, [r0]
	mov r2, #0x10
	orr r1, r2
	strb r1, [r0]
	ldr r0, [r3]
	add r0, #0x22
	mov r5, sb
	strb r5, [r0]
	ldr r1, [r3]
	mov r2, #0x98
	lsl r2, r2, #1
	add r0, r7, r2
	strh r0, [r1, #0x10]
	ldr r0, =0x03003C00
	ldrb r0, [r0, #3]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1d
	mov r1, #2
	and r0, r1
	cmp r0, #0
	beq _0802E2AC
	ldr r0, [r3]
	mov r3, sl
	add r1, r4, r3
	b _0802E2B4
	.align 2, 0
.pool
_0802E2AC:
	ldr r0, [r3]
	ldr r1, =unk_820E348
	add r1, #4
	add r1, r4, r1
_0802E2B4:
	ldr r1, [r1]
	strh r1, [r0, #0x12]
	add r0, r6, #1
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	cmp r6, #2
	bls _0802E1D6
	ldr r0, =off_820CB84
	mov r1, #0
	str r1, [sp]
	mov r2, r8
	mov r3, #2
	bl allocSprite
	ldr r3, =0x030005D4
	str r0, [r3]
	add r0, #0x23
	ldrb r1, [r0]
	mov r2, #0x10
	orr r1, r2
	strb r1, [r0]
	ldr r0, [r3]
	add r0, #0x22
	mov r5, sb
	strb r5, [r0]
	ldr r2, [r3]
	ldr r1, =unk_820E330
	ldr r0, [r1, #8]
	strh r0, [r2, #0x10]
	ldr r0, [r1, #0xc]
	strh r0, [r2, #0x12]
	add sp, #4
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802E314
sub_0802E314: @ 0x0802E314
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	mov r5, #0
	ldr r0, =0x030020AC
	mov r8, r0
	ldr r7, =byte_820E38C
	ldr r1, =0x030005D8
	mov sb, r1
_0802E32E:
	cmp r5, #1
	beq _0802E364
	cmp r5, #1
	bgt _0802E348
	cmp r5, #0
	beq _0802E34E
	b _0802E384
	.align 2, 0
.pool
_0802E348:
	cmp r5, #2
	beq _0802E378
	b _0802E384
_0802E34E:
	cmp r6, #0
	beq _0802E35C
	ldr r1, =Easy_strings
	b _0802E37A
	.align 2, 0
.pool
_0802E35C:
	ldr r1, =NewGameStrings
	b _0802E37A
	.align 2, 0
.pool
_0802E364:
	cmp r6, #0
	beq _0802E370
	ldr r1, =LoadGameStrings
	b _0802E37A
	.align 2, 0
.pool
_0802E370:
	ldr r1, =LoadGameStrings
	b _0802E37A
	.align 2, 0
.pool
_0802E378:
	ldr r1, =TimeTrialStrings
_0802E37A:
	mov r2, r8
	ldrb r0, [r2]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r4, [r0]
_0802E384:
	ldr r0, =0x030005B8
	lsl r1, r5, #2
	add r0, r1, r0
	ldr r2, [r0]
	mov sl, r1
	cmp r6, #0
	beq _0802E404
	cmp r5, #2
	bne _0802E3C0
	ldr r0, =0x03003C00
	ldrb r0, [r0, #3]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1d
	and r0, r5
	cmp r0, #0
	bne _0802E3C0
	mov r1, sb
	ldrb r0, [r1]
	add r0, #1
	add r1, r2, #0
	add r1, #0x22
	strb r0, [r1]
	b _0802E3CA
	.align 2, 0
.pool
_0802E3C0:
	mov r0, sb
	ldrh r1, [r0]
	add r0, r2, #0
	add r0, #0x22
	strb r1, [r0]
_0802E3CA:
	ldr r0, =0x03000582
	ldrh r0, [r0]
	cmp r0, r5
	beq _0802E44A
	cmp r5, #2
	bne _0802E3F0
	ldr r0, =0x03003C00
	ldrb r0, [r0, #3]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1d
	and r0, r5
	cmp r0, #0
	bne _0802E3F0
	ldrb r2, [r7, #2]
	b _0802E422
	.align 2, 0
.pool
_0802E3F0:
	add r0, r5, r7
	ldrb r2, [r0]
	add r0, r4, #0
	ldr r1, =0x030005A0
	mov r3, #1
	bl setStringColor
	b _0802E472
	.align 2, 0
.pool
_0802E404:
	ldr r0, =0x0300058E
	ldrh r0, [r0]
	asr r0, r5
	mov r1, #1
	and r0, r1
	cmp r0, #0
	bne _0802E438
	mov r1, sb
	ldrb r0, [r1]
	add r0, #1
	add r1, r2, #0
	add r1, #0x22
	strb r0, [r1]
	add r0, r5, r7
	ldrb r2, [r0]
_0802E422:
	add r0, r4, #0
	ldr r1, =0x030005A0
	mov r3, #3
	bl setStringColor
	b _0802E472
	.align 2, 0
.pool
_0802E438:
	mov r0, sb
	ldrh r1, [r0]
	add r0, r2, #0
	add r0, #0x22
	strb r1, [r0]
	ldr r0, =0x03000580
	ldrh r0, [r0]
	cmp r0, r5
	bne _0802E464
_0802E44A:
	add r0, r5, r7
	ldrb r2, [r0]
	add r0, r4, #0
	ldr r1, =0x030005A0
	mov r3, #4
	bl setStringColor
	b _0802E472
	.align 2, 0
.pool
_0802E464:
	add r0, r5, r7
	ldrb r2, [r0]
	add r0, r4, #0
	ldr r1, =0x030005A0
	mov r3, #1
	bl setStringColor
_0802E472:
	ldr r0, =0x030005B8
	add r0, sl
	ldr r0, [r0]
	bl updateSpriteAnimation
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, #2
	bhi _0802E488
	b _0802E32E
_0802E488:
	cmp r6, #0
	beq _0802E4D8
	ldr r0, =0x03003C00
	ldrb r0, [r0, #3]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1d
	mov r1, #2
	and r0, r1
	cmp r0, #0
	beq _0802E4C0
	ldr r4, =0x030005B0
	ldr r3, [r4]
	ldr r1, =unk_820E318
	ldr r2, =0x03000582
	b _0802E500
	.align 2, 0
.pool
_0802E4C0:
	ldr r4, =0x030005B0
	ldr r3, [r4]
	ldr r1, =unk_820E348
	ldr r2, =0x03000582
	b _0802E500
	.align 2, 0
.pool
_0802E4D8:
	ldr r0, =0x03003C00
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	cmp r0, #0
	bge _0802E4F8
	ldr r4, =0x030005B0
	ldr r3, [r4]
	ldr r1, =unk_820E330
	b _0802E4FE
	.align 2, 0
.pool
_0802E4F8:
	ldr r4, =0x030005B0
	ldr r3, [r4]
	ldr r1, =unk_820E318
_0802E4FE:
	ldr r2, =0x03000580
_0802E500:
	ldrh r0, [r2]
	lsl r0, r0, #3
	add r0, r0, r1
	ldr r0, [r0]
	sub r0, #2
	strh r0, [r3, #0x10]
	ldrh r0, [r2]
	lsl r0, r0, #3
	add r1, #4
	add r0, r0, r1
	ldr r0, [r0]
	sub r0, #2
	strh r0, [r3, #0x12]
	ldr r0, [r4]
	bl updateSpriteAnimation
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global loadDifficultyMenu_mayb
loadDifficultyMenu_mayb: @ 0x0802E53C
	push {r4, r5, lr}
	mov r4, #0xd
	ldr r5, =0x030037D0
_0802E542:
	lsl r0, r4, #4
	add r0, r0, r5
	bl freeSimpleSpriteHeader
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0x3f
	bls _0802E542
	ldr r1, =0x03000582
	ldr r0, =0x03003D40
	ldrb r0, [r0, #0x11]
	strh r0, [r1]
	bl sub_0802E874
	bl loadDifficultyString
	mov r0, #0xf0
	mov r1, #0x12
	bl loadDifficultyStringAtPos
	bl sub_0802EAF0
	ldr r0, =0x03003C00
	ldrb r0, [r0, #3]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1d
	mov r1, #2
	and r0, r1
	cmp r0, #0
	beq _0802E598
	ldr r0, =unk_820E318
	b _0802E59A
	.align 2, 0
.pool
_0802E598:
	ldr r0, =unk_820E348
_0802E59A:
	ldrh r1, [r0, #4]
	add r1, #9
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r0, #0xf0
	bl sub_0802EB24
	bl sub_0802EBD4
	ldr r0, =0x03003C00
	ldrb r0, [r0, #3]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1d
	mov r1, #2
	and r0, r1
	cmp r0, #0
	beq _0802E5CC
	ldr r0, =unk_820E318
	b _0802E5CE
	.align 2, 0
.pool
_0802E5CC:
	ldr r0, =unk_820E348
_0802E5CE:
	ldrh r1, [r0, #0xc]
	add r1, #9
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r0, #0xf0
	bl sub_0802EC08
	bl sub_0802ECB8
	ldr r0, =unk_820E318
	ldr r1, [r0, #0x14]
	add r1, #9
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r0, #0xf0
	bl sub_0802ED08
	ldr r1, =0x03000590
	mov r0, #1
	strh r0, [r1]
	ldr r1, =0x03003D40
	ldrb r0, [r1, #0x10]
	cmp r0, #0
	bne _0802E614
	mov r0, #1
	b _0802E61A
	.align 2, 0
.pool
_0802E614:
	mov r0, #0
	strb r0, [r1, #0x10]
	mov r0, #5
_0802E61A:
	pop {r4, r5}
	pop {r1}
	bx r1

thumb_func_global menuState10
menuState10: @ 0x0802E620
	push {lr}
	bl menuState5
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0802E63A
	mov r0, #0
	b _0802E642
_0802E63A:
	mov r0, #2
	bl playSong
	mov r0, #6
_0802E642:
	pop {r1}
	bx r1
	.align 2, 0

thumb_func_global menuState5
menuState5: @ 0x0802E648
	push {r4, r5, lr}
	bl isDifficultyStringAnimDone
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	neg r1, r0
	orr r1, r0
	lsr r4, r1, #0x1f
	bl sub_0802EB60
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0802E666
	mov r0, #2
	orr r4, r0
_0802E666:
	bl sub_0802EC44
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0802E678
	mov r0, #4
	orr r4, r0
	lsl r0, r4, #0x10
	lsr r4, r0, #0x10
_0802E678:
	bl sub_0802ED60
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0802E68A
	mov r0, #8
	orr r4, r0
	lsl r0, r4, #0x10
	lsr r4, r0, #0x10
_0802E68A:
	cmp r4, #0xf
	bne _0802E6A8
	ldr r0, =0x030005B0
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	mov r0, #1
	b _0802E6D2
	.align 2, 0
.pool
_0802E6A8:
	mov r4, #0
	ldr r5, =0x030005C8
_0802E6AC:
	lsl r0, r4, #2
	add r0, r0, r5
	ldr r2, [r0]
	add r0, r2, #0
	add r0, #0x23
	ldrb r1, [r0]
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	bne _0802E6C6
	add r0, r2, #0
	bl updateSpriteAnimation
_0802E6C6:
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #2
	bls _0802E6AC
	mov r0, #0
_0802E6D2:
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global menuState6
menuState6: @ 0x0802E6DC
	push {r4, r5, lr}
	ldr r2, =0x03002080
	ldrh r1, [r2, #0xc]
	mov r4, #2
	add r0, r4, #0
	and r0, r1
	cmp r0, #0
	beq _0802E750
	mov r4, #0xd
	ldr r5, =0x030037D0
_0802E6F0:
	lsl r0, r4, #4
	add r0, r0, r5
	bl freeSimpleSpriteHeader
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0x3f
	bls _0802E6F0
	ldr r0, =0x030005B0
	ldr r1, [r0]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	mov r4, #0
	ldr r3, =0x030005B8
_0802E714:
	lsl r0, r4, #2
	add r0, r0, r3
	ldr r1, [r0]
	add r1, #0x23
	ldrb r0, [r1]
	orr r0, r2
	strb r0, [r1]
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #2
	bls _0802E714
	mov r0, #0xcd
	bl playSong
	ldr r1, =0x03000590
	mov r0, #0
	strh r0, [r1]
	mov r0, #1
	b _0802E7F6
	.align 2, 0
.pool
_0802E750:
	mov r0, #9
	and r0, r1
	cmp r0, #0
	beq _0802E76C
	mov r0, #0xcc
	bl playSong
	ldr r0, =0x0300058C
	strh r4, [r0]
	mov r0, #3
	b _0802E7F6
	.align 2, 0
.pool
_0802E76C:
	ldrh r1, [r2, #0x14]
	mov r0, #0xc0
	and r0, r1
	cmp r0, #0
	beq _0802E7CC
	ldr r0, =0x03003C00
	ldrb r0, [r0, #3]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1d
	and r0, r4
	cmp r0, #0
	beq _0802E7B0
	mov r0, #0x40
	and r0, r1
	cmp r0, #0
	beq _0802E7A0
	ldr r4, =0x03000582
	ldrh r0, [r4]
	add r0, #2
	mov r1, #3
	b _0802E7B8
	.align 2, 0
.pool
_0802E7A0:
	ldr r4, =0x03000582
	ldrh r0, [r4]
	add r0, #1
	mov r1, #3
	b _0802E7B8
	.align 2, 0
.pool
_0802E7B0:
	ldr r4, =0x03000582
	ldrh r0, [r4]
	add r0, #1
	mov r1, #2
_0802E7B8:
	bl Bios_modulo
	strh r0, [r4]
	mov r0, #0xcb
	bl playSong
	b _0802E7F4
	.align 2, 0
.pool
_0802E7CC:
	mov r4, #0
	ldr r5, =0x030005C8
_0802E7D0:
	lsl r0, r4, #2
	add r0, r0, r5
	ldr r2, [r0]
	add r0, r2, #0
	add r0, #0x23
	ldrb r1, [r0]
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	bne _0802E7EA
	add r0, r2, #0
	bl updateSpriteAnimation
_0802E7EA:
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #2
	bls _0802E7D0
_0802E7F4:
	mov r0, #0
_0802E7F6:
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global menuState7
menuState7: @ 0x0802E800
	push {r4, r5, lr}
	mov r4, #0xd
	ldr r5, =0x030037D0
_0802E806:
	lsl r0, r4, #4
	add r0, r0, r5
	bl freeSimpleSpriteHeader
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0x3f
	bls _0802E806
	bl sub_0802E96C
	bl sub_0802EE4C
	bl sub_0802DF68
	mov r0, #0xf0
	mov r1, #0x12
	bl setSelectModeStringPos
	bl sub_0802DFD8
	ldr r4, =unk_820E318
	ldr r1, [r4, #4]
	add r1, #9
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r0, #0xf0
	bl sub_0802E00C
	bl sub_0802E048
	ldr r1, [r4, #0xc]
	add r1, #9
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r0, #0xf0
	bl sub_0802E07C
	bl sub_0802E0B8
	ldr r1, [r4, #0x14]
	add r1, #9
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r0, #0xf0
	bl sub_0802E0EC
	mov r0, #4
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_0802E874
sub_0802E874: @ 0x0802E874
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	ldr r3, =0x030005B0
	ldr r1, [r3]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	ldr r0, =0x03003C00
	ldrb r0, [r0, #3]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1d
	mov r1, #2
	and r0, r1
	cmp r0, #0
	beq _0802E8AC
	ldr r3, [r3]
	ldr r1, =unk_820E318
	b _0802E8B0
	.align 2, 0
.pool
_0802E8AC:
	ldr r3, [r3]
	ldr r1, =unk_820E348
_0802E8B0:
	ldr r2, =0x03000582
	ldrh r0, [r2]
	lsl r0, r0, #3
	add r0, r0, r1
	ldr r0, [r0]
	strh r0, [r3, #0x10]
	ldrh r0, [r2]
	lsl r0, r0, #3
	add r1, #4
	add r0, r0, r1
	ldr r0, [r0]
	strh r0, [r3, #0x12]
	mov r5, #0
	ldr r0, =0x030005B8
	mov sb, r0
	mov r7, #0x11
	neg r7, r7
	ldr r6, =0x03003C00
	ldr r2, =dword_820E31C
	mov r8, r2
	ldr r0, =unk_820E34C
	mov ip, r0
_0802E8DC:
	lsl r3, r5, #2
	mov r2, sb
	add r4, r3, r2
	ldr r1, [r4]
	add r1, #0x23
	ldrb r2, [r1]
	add r0, r7, #0
	and r0, r2
	strb r0, [r1]
	ldr r1, [r4]
	lsl r0, r5, #4
	mov r2, #0x98
	lsl r2, r2, #1
	add r0, r0, r2
	strh r0, [r1, #0x10]
	ldrb r0, [r6, #3]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1d
	mov r1, #2
	and r0, r1
	cmp r0, #0
	beq _0802E928
	ldr r0, [r4]
	lsl r1, r5, #3
	add r1, r8
	b _0802E92E
	.align 2, 0
.pool
_0802E928:
	ldr r0, [r4]
	lsl r1, r5, #3
	add r1, ip
_0802E92E:
	ldr r1, [r1]
	strh r1, [r0, #0x12]
	ldrb r0, [r6, #3]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1d
	asr r0, r5
	mov r1, #1
	and r0, r1
	cmp r0, #0
	beq _0802E952
	ldr r0, =0x030005C8
	add r0, r3, r0
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	add r0, r7, #0
	and r0, r2
	strb r0, [r1]
_0802E952:
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, #2
	bls _0802E8DC
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802E96C
sub_0802E96C: @ 0x0802E96C
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	ldr r3, =0x030005B0
	ldr r1, [r3]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	ldr r3, [r3]
	ldr r1, =unk_820E318
	ldr r2, =0x03000580
	ldrh r0, [r2]
	lsl r0, r0, #3
	add r0, r0, r1
	ldr r0, [r0]
	strh r0, [r3, #0x10]
	ldrh r0, [r2]
	lsl r0, r0, #3
	add r1, #4
	add r0, r0, r1
	ldr r0, [r0]
	strh r0, [r3, #0x12]
	mov r5, #0
	mov r0, #0x11
	neg r0, r0
	mov sl, r0
	mov r0, #0x98
	lsl r0, r0, #1
	add r7, r0, #0
	ldr r0, =dword_820E334
	mov sb, r0
	mov r8, r1
	ldr r1, =0x030005B8
	mov ip, r1
_0802E9B8:
	lsl r3, r5, #2
	mov r0, ip
	add r4, r3, r0
	ldr r1, [r4]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, sl
	and r0, r2
	strb r0, [r1]
	ldr r1, [r4]
	lsl r2, r5, #4
	add r0, r2, r7
	strh r0, [r1, #0x10]
	ldr r1, =0x03003C00
	ldrb r0, [r1]
	lsl r0, r0, #0x19
	add r6, r2, #0
	cmp r0, #0
	bge _0802EA00
	ldr r0, [r4]
	lsl r1, r5, #3
	add r1, sb
	b _0802EA06
	.align 2, 0
.pool
_0802EA00:
	ldr r0, [r4]
	lsl r1, r5, #3
	add r1, r8
_0802EA06:
	ldr r1, [r1]
	strh r1, [r0, #0x12]
	ldr r0, =0x030005C8
	add r3, r3, r0
	ldr r2, [r3]
	add r2, #0x23
	ldrb r0, [r2]
	mov r1, #0x10
	orr r0, r1
	strb r0, [r2]
	ldr r1, [r3]
	add r0, r6, r7
	strh r0, [r1, #0x10]
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, #2
	bls _0802E9B8
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global loadDifficultyString
loadDifficultyString: @ 0x0802EA3C
	push {lr}
	sub sp, #4
	ldr r1, =difficultyMenuStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000598
	mov r2, #0
	str r2, [sp]
	mov r2, #0xd
	mov r3, #1
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global loadDifficultyStringAtPos
loadDifficultyStringAtPos: @ 0x0802EA70
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =difficultyMenuStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x03000598
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0xd
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global isDifficultyStringAnimDone
isDifficultyStringAnimDone: @ 0x0802EAAC
	push {r4, lr}
	ldr r1, =difficultyMenuStrings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0xd
	bl str_getX_ofFirstChar
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	sub r0, #0xc
	cmp r0, #0x57
	bgt _0802EADC
	mov r0, #0x58
	mov r4, #1
	b _0802EAE2
	.align 2, 0
.pool
_0802EADC:
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r4, #0
_0802EAE2:
	mov r1, #0x12
	bl loadDifficultyStringAtPos
	add r0, r4, #0
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global sub_0802EAF0
sub_0802EAF0: @ 0x0802EAF0
	push {lr}
	sub sp, #4
	ldr r1, =Easy_strings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005A0
	mov r2, #0
	str r2, [sp]
	mov r2, #0x17
	mov r3, #1
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802EB24
sub_0802EB24: @ 0x0802EB24
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =Easy_strings
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005A0
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0x17
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802EB60
sub_0802EB60: @ 0x0802EB60
	push {r4, r5, lr}
	ldr r2, =0x030005B8
	ldr r0, [r2]
	ldrh r4, [r0, #0x10]
	ldr r0, =0x03003C00
	ldrb r0, [r0, #3]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1d
	mov r1, #2
	and r0, r1
	cmp r0, #0
	beq _0802EB88
	ldr r0, =unk_820E318
	b _0802EB8A
	.align 2, 0
.pool
_0802EB88:
	ldr r0, =unk_820E348
_0802EB8A:
	ldrh r3, [r0]
	add r0, r4, #0
	sub r0, #0xc
	cmp r0, r3
	bge _0802EBA0
	add r4, r3, #0
	mov r5, #1
	b _0802EBA6
	.align 2, 0
.pool
_0802EBA0:
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	mov r5, #0
_0802EBA6:
	ldr r0, [r2]
	strh r4, [r0, #0x10]
	ldr r0, =0x030005C8
	ldr r0, [r0]
	strh r4, [r0, #0x10]
	add r0, r4, #0
	add r0, #0x10
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r1, [r2]
	ldrh r1, [r1, #0x12]
	add r1, #9
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl sub_0802EB24
	add r0, r5, #0
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_0802EBD4
sub_0802EBD4: @ 0x0802EBD4
	push {lr}
	sub sp, #4
	ldr r1, =off_820CD88
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005A0
	mov r2, #0
	str r2, [sp]
	mov r2, #0x23
	mov r3, #1
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802EC08
sub_0802EC08: @ 0x0802EC08
	push {lr}
	sub sp, #4
	add r3, r0, #0
	add r2, r1, #0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	ldr r1, =off_820CD88
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =0x030005A0
	str r1, [sp]
	add r1, r3, #0
	mov r3, #0x23
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802EC44
sub_0802EC44: @ 0x0802EC44
	push {r4, lr}
	ldr r2, =0x030005B8
	ldr r0, [r2, #4]
	ldrh r3, [r0, #0x10]
	ldr r0, =0x03003C00
	ldrb r0, [r0, #3]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1d
	mov r1, #2
	and r0, r1
	cmp r0, #0
	beq _0802EC6C
	ldr r0, =unk_820E318
	b _0802EC6E
	.align 2, 0
.pool
_0802EC6C:
	ldr r0, =unk_820E348
_0802EC6E:
	ldrh r1, [r0, #8]
	add r0, r3, #0
	sub r0, #0xc
	cmp r0, r1
	bge _0802EC84
	add r3, r1, #0
	mov r4, #1
	b _0802EC8A
	.align 2, 0
.pool
_0802EC84:
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	mov r4, #0
_0802EC8A:
	ldr r0, [r2, #4]
	strh r3, [r0, #0x10]
	ldr r0, =0x030005C8
	ldr r0, [r0, #4]
	strh r3, [r0, #0x10]
	add r0, r3, #0
	add r0, #0x10
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r1, [r2, #4]
	ldrh r1, [r1, #0x12]
	add r1, #9
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl sub_0802EC08
	add r0, r4, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_0802ECB8
sub_0802ECB8: @ 0x0802ECB8
	push {lr}
	sub sp, #4
	ldr r0, =0x03003C00
	ldrb r0, [r0, #3]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1d
	mov r1, #2
	and r0, r1
	cmp r0, #0
	beq _0802ECD8
	ldr r0, =off_820CDB4
	b _0802ECDA
	.align 2, 0
.pool
_0802ECD8:
	ldr r0, =_3StarsStrings
_0802ECDA:
	ldr r1, =0x03002080
	add r1, #0x2c
	ldrb r1, [r1]
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r2, [r1]
	ldr r1, =0x030005A0
	mov r0, #0
	str r0, [sp]
	add r0, r2, #0
	mov r2, #0x2d
	mov r3, #1
	bl bufferString
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802ED08
sub_0802ED08: @ 0x0802ED08
	push {lr}
	sub sp, #4
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	lsl r1, r1, #0x10
	lsr r2, r1, #0x10
	ldr r0, =0x03003C00
	ldrb r0, [r0, #3]
	lsl r0, r0, #0x19
	lsr r0, r0, #0x1d
	mov r1, #2
	and r0, r1
	cmp r0, #0
	beq _0802ED30
	ldr r0, =off_820CDB4
	b _0802ED32
	.align 2, 0
.pool
_0802ED30:
	ldr r0, =_3StarsStrings
_0802ED32:
	ldr r1, =0x03002080
	add r1, #0x2c
	ldrb r1, [r1]
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r1, [r1]
	ldr r0, =0x030005A0
	str r0, [sp]
	add r0, r1, #0
	add r1, r3, #0
	mov r3, #0x2d
	bl setStringPos
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802ED60
sub_0802ED60: @ 0x0802ED60
	push {r4, lr}
	ldr r0, =0x030005B8
	ldr r1, [r0, #8]
	ldrh r1, [r1, #0x10]
	add r2, r1, #0
	sub r2, #0xc
	ldr r1, =unk_820E318
	ldr r1, [r1, #0x10]
	add r3, r0, #0
	cmp r2, r1
	bge _0802ED88
	lsl r0, r1, #0x10
	lsr r1, r0, #0x10
	mov r4, #1
	b _0802ED8E
	.align 2, 0
.pool
_0802ED88:
	lsl r0, r2, #0x10
	lsr r1, r0, #0x10
	mov r4, #0
_0802ED8E:
	ldr r0, [r3, #8]
	strh r1, [r0, #0x10]
	ldr r0, =0x030005C8
	ldr r0, [r0, #8]
	strh r1, [r0, #0x10]
	add r0, r1, #0
	add r0, #0x10
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r1, [r3, #8]
	ldrh r1, [r1, #0x12]
	add r1, #9
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl sub_0802ED08
	add r0, r4, #0
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global sub_0802EDBC
sub_0802EDBC: @ 0x0802EDBC
	push {lr}
	ldr r0, =0x030005D4
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	ldr r3, =DifficultiesStrings
	ldr r2, =0x03002080
	add r2, #0x2c
	ldr r0, =0x03003C00
	ldrb r1, [r0, #3]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1e
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r2, [r2]
	add r0, r0, r2
	lsl r0, r0, #2
	add r0, r0, r3
	ldr r0, [r0]
	mov r1, #0x37
	bl showString
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802EE04
sub_0802EE04: @ 0x0802EE04
	push {lr}
	ldr r0, =0x030005D4
	ldr r1, [r0]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	ldr r3, =DifficultiesStrings
	ldr r2, =0x03002080
	add r2, #0x2c
	ldr r0, =0x03003C00
	ldrb r1, [r0, #3]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1e
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r2, [r2]
	add r0, r0, r2
	lsl r0, r0, #2
	add r0, r0, r3
	ldr r0, [r0]
	mov r1, #0x37
	bl hideString
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802EE4C
sub_0802EE4C: @ 0x0802EE4C
	push {r4, r5, lr}
	sub sp, #4
	ldr r3, =DifficultiesStrings
	ldr r2, =0x03002080
	add r2, #0x2c
	ldr r0, =0x03003C00
	ldrb r1, [r0, #3]
	lsl r1, r1, #0x1c
	lsr r1, r1, #0x1e
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r2, [r2]
	add r0, r0, r2
	lsl r0, r0, #2
	add r0, r0, r3
	ldr r4, [r0]
	ldr r5, =0x030005A8
	mov r0, #0
	str r0, [sp]
	add r0, r4, #0
	add r1, r5, #0
	mov r2, #0x37
	mov r3, #1
	bl bufferString
	add r0, r4, #0
	add r1, r5, #0
	mov r2, #0x37
	mov r3, #3
	bl setStringColor
	add r0, r4, #0
	mov r1, #0x37
	bl str_get_last_width_OamX_sum
	add r1, r0, #0
	mov r0, #0x38
	sub r0, r0, r1
	mov r1, #2
	bl Bios_Div
	ldr r2, =unk_820E330
	ldr r1, [r2, #8]
	add r1, #0x42
	add r1, r1, r0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r2, [r2, #0xc]
	sub r2, #0xc
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	str r5, [sp]
	add r0, r4, #0
	mov r3, #0x37
	bl setStringPos
	add r0, r4, #0
	mov r1, #0x37
	bl hideString
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0802EEE0
sub_0802EEE0: @ 0x0802EEE0
	push {r4, r5, r6, lr}
	mov r6, sb
	mov r5, r8
	push {r5, r6}
	sub sp, #4
	ldr r1, =HighScorePtsStrings
	ldr r0, =0x03002080
	mov r8, r0
	mov r2, #0x2c
	add r8, r2
	mov r3, r8
	ldrb r0, [r3]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r5, [r0]
	ldr r4, =0x030005A0
	mov r0, #0
	mov sb, r0
	str r0, [sp]
	add r0, r5, #0
	add r1, r4, #0
	mov r2, #0
	mov r3, #1
	bl bufferString
	add r0, r5, #0
	mov r1, #0
	bl str_get_last_width_OamX_sum
	mov r1, #0xec
	sub r1, r1, r0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	str r4, [sp]
	add r0, r5, #0
	mov r2, #0x94
	mov r3, #0
	bl setStringPos
	mov r0, #0xb
	bl allocateSpriteTiles
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	ldr r0, =dword_8208DA0
	mov r1, #1
	bl LoadSpritePalette
	add r6, r0, #0
	lsl r6, r6, #0x10
	lsr r6, r6, #0x10
	ldr r0, =score_numberTiles
	lsl r4, r4, #0x10
	asr r1, r4, #0xb
	ldr r2, =0x06010000
	add r1, r1, r2
	bl Bios_LZ77_16_2
	add r0, r5, #0
	mov r1, #0
	bl str_getX_ofFirstChar
	add r5, r0, #0
	lsl r5, r5, #0x10
	lsr r5, r5, #0x10
	ldr r0, =0x03003C00
	add r0, #0xce
	ldrh r1, [r0]
	mov r0, #0x64
	mul r0, r1, r0
	ldr r1, =0x030005E0
	mov r2, #0
	bl createScoreString
	ldr r0, =unk_820E39C
	lsr r4, r4, #0x10
	mov r3, sb
	str r3, [sp]
	mov r1, #0
	add r2, r4, #0
	mov r3, #1
	bl allocSprite
	ldr r1, =0x030005DC
	str r0, [r1]
	ldr r2, =gameover_score_x_lut
	mov r3, r8
	ldrb r1, [r3]
	lsl r1, r1, #1
	add r1, r1, r2
	ldrh r1, [r1]
	add r5, r5, r1
	strh r5, [r0, #0x10]
	mov r1, #0x94
	strh r1, [r0, #0x12]
	add r0, #0x22
	strb r6, [r0]
	add sp, #4
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool
