.include "asm/macros.inc"

@complete

thumb_func_global Bios_arctan
Bios_arctan: @ 0x080353D8
	svc #0xa
	bx lr

thumb_func_global Bios_memcpy_32b
Bios_memcpy_32b: @ 0x080353DC
	svc #0xc
	bx lr

thumb_func_global Bios_memcpy
Bios_memcpy: @ 0x080353E0
	svc #0xb
	bx lr

thumb_func_global Bios_Div
Bios_Div: @ 0x080353E4
	svc #6
	bx lr

thumb_func_global Bios_modulo
Bios_modulo: @ 0x080353E8
	svc #6
	add r0, r1, #0
	bx lr
	.align 2, 0

thumb_func_global Bios_LZ77_16bit
Bios_LZ77_16bit: @ 0x080353F0
	svc #0x12
	bx lr

thumb_func_global Bios_LZ77_8bit
Bios_LZ77_8bit: @ 0x080353F4
	svc #0x11
	bx lr

thumb_func_global Bios_SoundDriverVSyncOff
Bios_SoundDriverVSyncOff: @ 0x080353F8
	svc #0x28
	bx lr

thumb_func_global Bios_SoundDriverVSyncOn
Bios_SoundDriverVSyncOn: @ 0x080353FC
	svc #0x29
	bx lr

thumb_func_global Bios_Sqrt
Bios_Sqrt: @ 0x08035400
	svc #8
	bx lr

thumb_func_global Bios_waitVBlank
Bios_waitVBlank: @ 0x08035404
	mov r2, #0
	svc #5
	bx lr
	.align 2, 0
	
