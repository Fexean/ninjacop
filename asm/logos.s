.include "asm/macros.inc"

@completely disassembled

thumb_func_global cb_licensedByNintendo
cb_licensedByNintendo: @ 0x08027088
	push {r4, r5, r6, lr}
	sub sp, #8
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl clearSprites
	bl resetAffineTransformations
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	add r1, sp, #4
	mov r0, #0
	strh r0, [r1]
	mov r1, #0xa0
	lsl r1, r1, #0x13
	ldr r2, =0x01000001
	add r0, sp, #4
	bl Bios_memcpy
	ldr r5, =0x03000580
	add r0, r5, #0
	mov r1, #1
	bl allocFont
	ldr r4, =licenseStr
	mov r6, #0
	str r6, [sp]
	add r0, r4, #0
	add r1, r5, #0
	mov r2, #0
	mov r3, #0
	bl bufferString
	add r0, r4, #0
	mov r1, #0
	bl str_get_last_width_OamX_sum
	mov r1, #0xf0
	sub r1, r1, r0
	lsl r1, r1, #0xf
	lsr r1, r1, #0x10
	str r5, [sp]
	add r0, r4, #0
	mov r2, #0x4c
	mov r3, #0
	bl setStringPos
	ldr r0, =0x0300058A
	strh r6, [r0]
	ldr r0, =(licensedByNin_screen+1)
	bl setCurrentTaskFunc
	add sp, #8
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global licensedByNin_screen
licensedByNin_screen: @ 0x0802711C
	push {r4, lr}
	ldr r1, =licensedByNin_states
	ldr r4, =0x0300058A
	ldrh r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl call_r0
	lsl r0, r0, #0x10
	asr r1, r0, #0x10
	mov r0, #1
	neg r0, r0
	cmp r1, r0
	beq _08027150
	cmp r1, #1
	bne _0802715E
	ldrh r0, [r4]
	add r0, #1
	strh r0, [r4]
	b _0802715E
	.align 2, 0
.pool
_08027150:
	ldr r0, =0x03002080
	add r0, #0x2c
	mov r1, #0
	strb r1, [r0]
	ldr r0, =(cb_konamiLogoLoad+1)
	bl setCurrentTaskFunc
_0802715E:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global licensedState0
licensedState0: @ 0x0802716C
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _08027182
	mov r0, #0
	b _0802718A
_08027182:
	ldr r1, =0x03000588
	mov r0, #0
	strh r0, [r1]
	mov r0, #1
_0802718A:
	pop {r1}
	bx r1
	.align 2, 0
.pool



thumb_func_global licensedState1
licensedState1: @ 0x08027194
	ldr r1, =0x03000588
	ldrh r0, [r1]
	add r0, #1
	strh r0, [r1]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0xb4
	bhi _080271AC
	mov r0, #0
	b _080271B2
	.align 2, 0
.pool
_080271AC:
	mov r0, #0
	strh r0, [r1]
	mov r0, #1
_080271B2:
	bx lr



thumb_func_global licensedState2
licensedState2: @ 0x080271B4
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _080271CC
	mov r0, #0
	b _080271CE
_080271CC:
	mov r0, #1
_080271CE:
	pop {r1}
	bx r1
	.align 2, 0



thumb_func_global licensedState3
licensedState3: @ 0x080271D4
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl clearSprites
	bl resetAffineTransformations
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	mov r0, #1
	neg r0, r0
	pop {r1}
	bx r1
	.align 2, 0
.pool



thumb_func_global cb_konamiLogoLoad
cb_konamiLogoLoad: @ 0x080271FC
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl clearSprites
	bl resetAffineTransformations
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r1, =0x0300058A
	mov r0, #0
	strh r0, [r1]
	ldr r0, =(cb_konamiLogo+1)
	bl setCurrentTaskFunc
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global cb_konamiLogo
cb_konamiLogo: @ 0x08027234
	push {lr}
	ldr r0, =0x0300058A
	bl konamiLogoSwitch
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08027250
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r0, =(cb_introStart+1)
	bl setCurrentTaskFunc
_08027250:
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global cb_hudsonLogoUnused
cb_hudsonLogoUnused: @ 0x0802725C
	push {r4, lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl clearSprites
	bl resetAffineTransformations
	mov r0, #0x3f
	mov r1, #0
	bl setBldCnt
	ldr r0, =hudson_pal
	mov r1, #0xa0
	lsl r1, r1, #0x13
	mov r2, #0xfd
	bl Bios_memcpy
	ldr r0, =hudson_tiles
	mov r1, #0xc0
	lsl r1, r1, #0x13
	mov r2, #0xf8
	lsl r2, r2, #4
	bl Bios_memcpy
	mov r4, #0
_08027294:
	lsl r0, r4, #4
	sub r0, r0, r4
	lsl r0, r0, #2
	ldr r1, =hudson_map
	add r0, r0, r1
	lsl r1, r4, #6
	ldr r2, =0x0600F800
	add r1, r1, r2
	mov r2, #0x1e
	bl Bios_memcpy
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0x13
	bls _08027294
	ldr r1, =0x03003D00
	mov r3, #0
	mov r0, #0xfc
	lsl r0, r0, #5
	strh r0, [r1]
	ldr r2, =0x03003CD4
	ldrh r0, [r2]
	mov r4, #0x80
	lsl r4, r4, #1
	add r1, r4, #0
	orr r0, r1
	strh r0, [r2]
	ldr r0, =0x0300058A
	strh r3, [r0]
	ldr r0, =0x03000588
	strh r3, [r0]
	ldr r0, =(unused_hudsonLogo+1)
	bl setCurrentTaskFunc
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global unused_hudsonLogo
unused_hudsonLogo: @ 0x08027304
	push {r4, lr}
	ldr r1, =hudsonStates
	ldr r4, =0x0300058A
	ldrh r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl call_r0
	lsl r0, r0, #0x10
	asr r1, r0, #0x10
	mov r0, #1
	neg r0, r0
	cmp r1, r0
	beq _08027338
	cmp r1, #1
	bne _0802733E
	ldrh r0, [r4]
	add r0, #1
	strh r0, [r4]
	b _0802733E
	.align 2, 0
.pool
_08027338:
	ldr r0, =(cb_introStart+1)
	bl setCurrentTaskFunc
_0802733E:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global hudsonState0
hudsonState0: @ 0x08027348
	ldr r1, =0x03000588
	ldrh r0, [r1]
	add r0, #1
	strh r0, [r1]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0x1e
	bhi _08027360
	mov r0, #0
	b _08027366
	.align 2, 0
.pool
_08027360:
	mov r0, #0
	strh r0, [r1]
	mov r0, #1
_08027366:
	bx lr



thumb_func_global hudsonState1
hudsonState1: @ 0x08027368
	push {lr}
	mov r0, #0x3f
	mov r1, #0
	mov r2, #0x3c
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0802737E
	mov r0, #0
	b _08027386
_0802737E:
	ldr r1, =0x03000588
	mov r0, #0
	strh r0, [r1]
	mov r0, #1
_08027386:
	pop {r1}
	bx r1
	.align 2, 0
.pool



thumb_func_global hudsonState2
hudsonState2: @ 0x08027390
	ldr r1, =0x03000588
	ldrh r0, [r1]
	add r0, #1
	strh r0, [r1]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0x78
	bhi _080273A8
	mov r0, #0
	b _080273AA
	.align 2, 0
.pool
_080273A8:
	mov r0, #1
_080273AA:
	bx lr



thumb_func_global hudsonState3
hudsonState3: @ 0x080273AC
	push {lr}
	mov r0, #0x3f
	mov r1, #0
	mov r2, #0x3c
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _080273C4
	mov r0, #0
	b _080273CC
_080273C4:
	ldr r1, =0x03000588
	mov r0, #0
	strh r0, [r1]
	mov r0, #1
_080273CC:
	pop {r1}
	bx r1
	.align 2, 0
.pool



thumb_func_global hudsonState4
hudsonState4: @ 0x080273D4
	push {lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl resetAffineTransformations
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	mov r0, #1
	neg r0, r0
	pop {r1}
	bx r1
	.align 2, 0
.pool
