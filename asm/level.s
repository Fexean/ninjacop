.include "asm/macros.inc"

@ This should be divided further


@ only manual disasm needed

thumb_func_global setPlayerStats_andtuff_8031B14
setPlayerStats_andtuff_8031B14: @ 0x08031B14
	push {r4, r5, lr}
	sub sp, #4
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r5, =0x03001F08
	ldr r2, =0x01000054
	mov r0, sp
	add r1, r5, #0
	bl Bios_memcpy
	add r4, r5, #0
	mov r1, #0x50
	str r1, [r4, #0xc]
	str r1, [r4, #8]
	str r1, [r4]
	add r4, #0x1c
	ldr r3, =0x03003D40
	ldrb r0, [r3, #0x12]
	cmp r0, #0
	beq _08031B42
	str r1, [r4, #8]
	str r1, [r4]
_08031B42:
	str r1, [r4, #0xc]
	add r4, r5, #0
	add r4, #0x38
	ldr r2, =unk_82329F8
	ldrb r1, [r3, #1]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r1, [r3, #0x11]
	add r0, r0, r1
	add r0, r0, r2
	ldrb r0, [r0]
	str r0, [r4, #0xc]
	str r0, [r4, #8]
	str r0, [r5, #0x38]
	mov r1, #1
	strb r1, [r4, #0x14]
	add r4, #0x1c
	mov r0, #0x18
	str r0, [r4, #0xc]
	str r0, [r4, #8]
	str r0, [r5, #0x54]
	strb r1, [r4, #0x14]
	ldrb r0, [r3, #0x11]
	cmp r0, #2
	bne _08031B90
	add r1, r5, #0
	add r1, #0x80
	mov r0, #0xe1
	lsl r0, r0, #4
	b _08031B96
	.align 2, 0
.pool
_08031B90:
	add r1, r5, #0
	add r1, #0x80
	ldr r0, =0x00001518
_08031B96:
	str r0, [r1]
	ldr r4, =0x03001F90
	ldr r0, =0x0016E360
	mov r1, #0xa
	bl Bios_Div
	str r0, [r4, #0xc]
	mov r0, #1
	strb r0, [r4, #0x14]
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global resetKeys
resetKeys: @ 0x08031BBC
	ldr r0, =0x03001F08
	add r0, #0x70
	mov r1, #0
	strh r1, [r0]
	bx lr
	.align 2, 0
.pool

thumb_func_global loadHUD
loadHUD: @ 0x08031BCC
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #8
	ldr r4, =0x03003D40
	ldrb r0, [r4, #0x12]
	cmp r0, #0
	beq _08031BEC
	bl setPlayerStats_andtuff_8031B14
	ldr r0, [r4, #0x14]
	ldrb r0, [r0, #0xc]
	bl setPowerupLevel
_08031BEC:
	ldrb r0, [r4, #1]
	cmp r0, #5
	bne _08031C22
	ldr r1, =0x03001F08
	mov r0, #0x50
	str r0, [r1, #8]
	str r0, [r1, #4]
	str r0, [r1]
	mov r0, #0
	str r0, [r1, #0x24]
	str r0, [r1, #0x20]
	str r0, [r1, #0x1c]
	ldrb r0, [r4, #0x11]
	cmp r0, #2
	bne _08031C1C
	add r1, #0x80
	mov r0, #0xe1
	lsl r0, r0, #4
	b _08031C20
	.align 2, 0
.pool
_08031C1C:
	add r1, #0x80
	ldr r0, =0x00001518
_08031C20:
	str r0, [r1]
_08031C22:
	ldr r0, =HUDPalette
	ldr r4, =0x05000180
	mov r1, #1
	add r2, r4, #0
	bl memcpy_pal
	ldr r0, =off_8231184
	add r1, r4, #0
	mov r2, #0
	bl startPalAnimation
	ldr r0, =HUDTiles
	ldr r4, =0x0600E000
	add r1, r4, #0
	bl Bios_LZ77_16_2
	add r1, sp, #4
	mov r2, #0xc1
	lsl r2, r2, #8
	add r0, r2, #0
	strh r0, [r1]
	ldr r1, =0x0600F800
	ldr r2, =0x01000400
	add r0, sp, #4
	bl Bios_memcpy
	ldr r0, =0x03003D40
	ldrb r0, [r0, #1]
	cmp r0, #4
	bhi _08031C94
	ldr r1, =0x03003D00
	ldr r0, =0x00001F0C
	strh r0, [r1]
	b _08031CAC
	.align 2, 0
.pool
_08031C94:
	ldr r1, =0x03003D00
	ldr r0, =0x00001F0D
	strh r0, [r1]
	mov r0, sp
	add r0, #6
	ldr r3, =0x0000FFFF
	add r1, r3, #0
	strh r1, [r0]
	ldr r2, =0x01000010
	add r1, r4, #0
	bl Bios_memcpy
_08031CAC:
	ldr r4, =0x03001F08
	mov r7, #0
	strb r7, [r4, #0x14]
	add r0, r4, #0
	add r0, #0x30
	strb r7, [r0]
	ldr r1, =0x03003D40
	ldrb r3, [r1, #2]
	ldr r2, =stageCount
	ldrb r5, [r1, #1]
	lsl r0, r5, #1
	add r0, r0, r2
	ldrh r0, [r0]
	sub r0, #1
	add r6, r1, #0
	cmp r3, r0
	bne _08031D26
	cmp r5, #5
	bne _08031CF8
	add r1, r4, #0
	add r1, #0x4c
	mov r0, #1
	strb r0, [r1]
	b _08031D26
	.align 2, 0
.pool
_08031CF8:
	add r0, r4, #0
	add r0, #0x4c
	strb r7, [r0]
	add r5, r4, #0
	add r5, #0x38
	ldr r2, =unk_82329F8
	ldrb r1, [r6, #1]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r1, [r6, #0x11]
	add r0, r0, r1
	add r0, r0, r2
	ldrb r0, [r0]
	str r0, [r5, #0xc]
	str r0, [r5, #8]
	str r0, [r4, #0x38]
	str r7, [r5, #0x10]
	ldrb r0, [r6, #1]
	cmp r0, #2
	bne _08031D26
	add r0, r4, #0
	add r0, #0x68
	strb r7, [r0]
_08031D26:
	ldrb r0, [r6, #3]
	cmp r0, #0
	bne _08031D36
	ldrb r0, [r6, #4]
	cmp r0, #0
	bne _08031D36
	bl resetKeys
_08031D36:
	mov r4, #0
_08031D38:
	add r0, r4, #0
	bl hasKey
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08031D4A
	add r0, r4, #0
	bl drawKeyToHUD
_08031D4A:
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #3
	bls _08031D38
	ldr r2, =0x03003D40
	ldrb r0, [r2, #3]
	cmp r0, #0
	bne _08031D8A
	ldrb r0, [r2, #4]
	cmp r0, #0
	bne _08031D8A
	ldr r3, =0x03001F08
	ldr r4, =hostageCounts
	ldrb r1, [r2, #2]
	lsl r1, r1, #1
	ldrb r0, [r2, #1]
	lsl r0, r0, #3
	add r1, r1, r0
	ldrb r2, [r2, #0x11]
	lsl r0, r2, #1
	add r0, r0, r2
	lsl r0, r0, #4
	add r1, r1, r0
	add r1, r1, r4
	ldrh r0, [r1]
	add r2, r3, #0
	add r2, #0x72
	mov r1, #0
	strh r0, [r2]
	add r3, #0x86
	strb r1, [r3]
_08031D8A:
	ldr r5, =0x03001F90
	ldr r4, =0x03003D40
	ldr r0, [r4, #0x1c]
	mov r1, #0xa
	bl Bios_Div
	str r0, [r5, #8]
	str r0, [r5, #4]
	str r0, [r5]
	mov r2, #0
	str r2, [r5, #0x10]
	strb r2, [r5, #0x14]
	ldr r0, =0x03003C00
	add r0, #0xce
	ldrh r1, [r0]
	mov r0, #0x64
	mul r1, r0, r1
	ldr r0, [r4, #0x1c]
	cmp r0, r1
	blo _08031DD4
	ldr r0, =0x03001FB2
	strh r2, [r0]
	b _08031DDA
	.align 2, 0
.pool
_08031DD4:
	ldr r1, =0x03001FB2
	mov r0, #0x30
	strh r0, [r1]
_08031DDA:
	bl drawRescue
	ldr r0, =0x03001FB0
	ldr r2, =0x0000FFFF
	add r1, r2, #0
	strh r1, [r0]
	ldr r3, =0x03003D40
	ldrb r0, [r3, #1]
	cmp r0, #5
	bne _08031E5E
	mov r0, #8
	bl allocateSpriteTiles
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldr r0, =hudTimerSprPal
	mov r1, #1
	bl LoadSpritePalette
	ldr r4, =0x03001FB8
	mov r1, #0
	mov sb, r1
	strb r0, [r4, #0x18]
	ldr r0, =hudTimerAnims
	ldr r6, [r0]
	ldr r2, =hudTimerSprTiles
	mov r8, r2
	add r2, r5, #0
	str r1, [sp]
	add r0, r6, #0
	mov r1, r8
	mov r3, #1
	bl allocSprite
	str r0, [r4]
	ldrb r1, [r4, #0x18]
	add r0, #0x22
	strb r1, [r0]
	ldr r1, [r4]
	mov r0, #0x68
	strh r0, [r1, #0x10]
	ldr r0, [r4]
	mov r5, #0xc
	strh r5, [r0, #0x12]
	mov r0, #8
	bl allocateSpriteTiles
	add r2, r0, #0
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	mov r3, sb
	str r3, [sp]
	add r0, r6, #0
	mov r1, r8
	mov r3, #1
	bl allocSprite
	str r0, [r4, #4]
	ldrb r1, [r4, #0x18]
	add r0, #0x22
	strb r1, [r0]
	ldr r1, [r4, #4]
	mov r0, #0x78
	strh r0, [r1, #0x10]
	ldr r0, [r4, #4]
	strh r5, [r0, #0x12]
_08031E5E:
	mov r0, #0x20
	bl allocateSpriteTiles
	add r4, r0, #0
	ldr r0, =hudBarSprTiles
	lsl r4, r4, #0x10
	asr r1, r4, #0xb
	ldr r2, =0x06010000
	add r1, r1, r2
	bl Bios_LZ77_16_2
	ldr r3, =doorSymbolSprPal
	mov sl, r3
	mov r0, sl
	mov r1, #1
	bl LoadSpritePalette
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	ldr r0, =unk_8232878
	lsr r7, r4, #0x10
	mov r1, #0
	mov r8, r1
	str r1, [sp]
	add r2, r7, #0
	mov r3, #1
	bl allocSprite
	ldr r5, =0x03001F08
	str r0, [r5, #0x18]
	mov r4, #6
	strh r4, [r0, #0x10]
	ldr r0, [r5, #0x18]
	mov r2, #4
	mov sb, r2
	mov r3, sb
	strh r3, [r0, #0x12]
	ldr r0, [r5, #0x18]
	add r0, #0x22
	strb r6, [r0]
	ldr r0, =unk_8232890
	mov r1, r8
	str r1, [sp]
	mov r1, #0
	add r2, r7, #0
	mov r3, #1
	bl allocSprite
	str r0, [r5, #0x34]
	strh r4, [r0, #0x10]
	ldr r1, [r5, #0x34]
	mov r0, #0xe
	strh r0, [r1, #0x12]
	ldr r0, [r5, #0x34]
	add r0, #0x22
	strb r6, [r0]
	ldr r3, =0x03003D40
	ldrb r2, [r3, #2]
	ldr r1, =stageCount
	ldrb r0, [r3, #1]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	sub r0, #1
	cmp r2, r0
	bne _08031EF0
	mov r0, sl
	add r0, #0x20
	mov r1, #1
	bl LoadSpritePalette
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
_08031EF0:
	ldr r0, =unk_82328A8
	mov r1, r8
	str r1, [sp]
	mov r1, #0
	add r2, r7, #0
	mov r3, #1
	bl allocSprite
	str r0, [r5, #0x50]
	mov r1, #0x9a
	strh r1, [r0, #0x10]
	ldr r0, [r5, #0x50]
	mov r2, sb
	strh r2, [r0, #0x12]
	ldr r0, [r5, #0x50]
	add r0, #0x22
	strb r6, [r0]
	ldr r1, [r5, #0x50]
	add r1, #0x24
	ldrb r0, [r1]
	mov r4, #2
	orr r0, r4
	strb r0, [r1]
	ldr r1, [r5, #0x50]
	mov r0, #0x50
	strh r0, [r1, #0x18]
	ldr r0, =unk_82328C0
	mov r3, r8
	str r3, [sp]
	mov r1, #0
	add r2, r7, #0
	mov r3, #1
	bl allocSprite
	str r0, [r5, #0x6c]
	mov r1, #0xd2
	strh r1, [r0, #0x10]
	ldr r1, [r5, #0x6c]
	mov r0, #0x10
	strh r0, [r1, #0x12]
	ldr r0, [r5, #0x6c]
	add r0, #0x22
	strb r6, [r0]
	ldr r1, [r5, #0x6c]
	add r1, #0x24
	ldrb r0, [r1]
	orr r0, r4
	strb r0, [r1]
	ldr r1, [r5, #0x6c]
	mov r0, #0x18
	strh r0, [r1, #0x18]
	mov r0, #0x14
	bl allocateSpriteTiles
	add r4, r0, #0
	ldr r0, =doorSymbolSprTiles
	lsl r4, r4, #0x10
	asr r1, r4, #0xb
	ldr r2, =0x06010000
	add r1, r1, r2
	bl Bios_LZ77_16_2
	lsr r4, r4, #0x10
	add r1, r6, #0
	add r0, r4, #0
	bl sub_08034790
	mov r0, #0
	bl hasKey
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08031FAC
	add r0, r5, #0
	add r0, #0x72
	ldrh r0, [r0]
	cmp r0, #0
	bne _08031FAC
	ldr r0, =off_8232908
	ldr r3, =0x03003D40
	ldrb r1, [r3, #2]
	lsl r1, r1, #2
	ldrb r2, [r3, #1]
	lsl r2, r2, #4
	add r1, r1, r2
	add r1, r1, r0
	ldr r0, [r1]
	cmp r0, #0
	beq _08031FAC
	mov r1, #0xa0
	lsl r1, r1, #0x13
	mov r2, #0
	bl startPalAnimation
_08031FAC:
	ldr r0, =pauseScreenPal
	mov r1, #1
	bl LoadSpritePalette
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	mov r0, #0x12
	bl allocateSpriteTiles
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldr r0, =unk_822FC9C
	lsl r4, r5, #0x10
	asr r1, r4, #0xb
	ldr r2, =0x06010000
	add r1, r1, r2
	bl Bios_LZ77_16_2
	ldr r0, =0x0201FEE0
	bl sub_080331E0
	ldr r0, =dword_82328D8
	lsr r4, r4, #0x10
	mov r3, #0
	mov r8, r3
	str r3, [sp]
	mov r1, #0
	add r2, r4, #0
	mov r3, #1
	bl allocSprite
	ldr r2, =0x03001F08
	str r0, [r2, #0x7c]
	mov r1, #0x58
	strh r1, [r0, #0x10]
	ldr r1, [r2, #0x7c]
	mov r0, #4
	strh r0, [r1, #0x12]
	ldr r1, [r2, #0x7c]
	add r1, #0x23
	ldrb r0, [r1]
	mov r7, #0x10
	orr r0, r7
	strb r0, [r1]
	ldr r0, [r2, #0x7c]
	add r0, #0x22
	strb r6, [r0]
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	cmp r0, #5
	beq _08032020
	ldr r0, [r1]
	ldr r1, =0x00FFFF00
	and r0, r1
	mov r1, #0x81
	lsl r1, r1, #0xa
	cmp r0, r1
	bne _0803204A
_08032020:
	ldr r0, =off_822FFE0
	mov r1, r8
	str r1, [sp]
	mov r1, #0
	add r2, r4, #0
	mov r3, #1
	bl allocSprite
	ldr r2, =0x03002014
	str r0, [r2]
	mov r1, #0x78
	strh r1, [r0, #0x10]
	mov r1, #0x50
	strh r1, [r0, #0x12]
	add r0, #0x23
	ldrb r1, [r0]
	orr r1, r7
	strb r1, [r0]
	ldr r0, [r2]
	add r0, #0x22
	strb r6, [r0]
_0803204A:
	ldr r4, =0x03003D40
	ldr r0, [r4, #0x1c]
	ldr r1, =0x0201A630
	mov r2, #1
	bl createScoreString
	ldr r0, =unk_82328F0
	add r2, r5, #0
	mov r1, #0
	str r1, [sp]
	mov r3, #1
	bl allocSprite
	ldr r5, =0x03001F08
	add r3, r5, #0
	add r3, #0xa0
	str r0, [r3]
	mov r1, #0x57
	strh r1, [r0, #0x10]
	ldr r1, [r3]
	mov r0, #4
	strh r0, [r1, #0x12]
	ldr r1, [r3]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	ldr r0, [r3]
	add r0, #0x22
	strb r6, [r0]
	ldrb r0, [r4]
	cmp r0, #2
	beq _08032096
	add r1, r5, #0
	add r1, #0x9c
	mov r0, #0
	strb r0, [r1]
_08032096:
	ldrb r0, [r4, #1]
	cmp r0, #4
	bhi _080320A6
	ldrb r0, [r4, #0x12]
	cmp r0, #0
	bne _080320A6
	bl DrawPowerLevel
_080320A6:
	add sp, #8
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08032128
sub_08032128: @ 0x08032128
	ldr r0, =0x03003CD4
	ldrh r1, [r0]
	mov r3, #0x80
	lsl r3, r3, #1
	add r2, r3, #0
	orr r1, r2
	strh r1, [r0]
	bx lr
	.align 2, 0
.pool
	
	
	
		thumb_func_global sub_803213C
sub_803213C: @ 0x0803213C
	ldr r2, =0x03003CD4
	ldrh r1, [r2]
	ldr r0, =0x0000FEFF
	and r0, r1
	strh r0, [r2]
	bx lr
	.align 2, 0
.pool
	
	
	
thumb_func_global sub_8032150
sub_8032150: @ 0x08032150
	push {lr}
	bl sub_08032170
	bl sub_08032468
	bl sub_080324C0
	bl sub_08032578
	bl sub_080325A8
	bl sub_08033458
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global sub_08032170
sub_08032170: @ 0x08032170
	push {r4, lr}
	ldr r4, =0x03001F08
	add r0, r4, #0
	bl sub_080321B0
	add r0, r4, #0
	add r0, #0x1c
	bl sub_080321B0
	add r0, r4, #0
	add r0, #0x38
	bl sub_080321B0
	add r0, r4, #0
	add r0, #0x54
	bl sub_080321B0
	add r0, r4, #0
	add r0, #0x88
	bl sub_080321B0
	bl sub_08032FA8
	bl sub_08033310
	bl invulnSpecial_handleCounter_pal
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_080321B0
sub_080321B0: @ 0x080321B0
	push {r4, r5, lr}
	add r3, r0, #0
	ldr r0, [r3]
	ldr r4, [r3, #0x10]
	add r2, r0, r4
	cmp r2, #0
	bge _080321C0
	mov r2, #0
_080321C0:
	ldr r0, [r3, #0xc]
	cmp r2, r0
	bls _080321C8
	add r2, r0, #0
_080321C8:
	cmp r4, #0
	bge _080321D6
	ldr r0, [r3, #8]
	add r1, r0, #1
	add r5, r0, #0
	cmp r2, r1
	blo _080321E4
_080321D6:
	cmp r4, #0
	ble _080321EA
	ldr r0, [r3, #8]
	sub r1, r0, #1
	add r5, r0, #0
	cmp r2, r1
	bls _080321EA
_080321E4:
	mov r0, #0
	str r0, [r3, #0x10]
	add r2, r5, #0
_080321EA:
	str r2, [r3]
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global modifyPlayerHealth
modifyPlayerHealth: @ 0x080321F4
	push {r4, lr}
	add r1, r0, #0
	ldr r4, =0x03001F08
	add r0, r4, #0
	bl sub_08032294
	add r4, #0x85
	ldrb r1, [r4]
	mov r0, #2
	neg r0, r0
	and r0, r1
	strb r0, [r4]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08032218
sub_08032218: @ 0x08032218
	ldr r0, =0x03001F08
	add r0, #0x85
	ldrb r0, [r0]
	lsl r0, r0, #0x1f
	lsr r0, r0, #0x1f
	bx lr
	.align 2, 0
.pool

thumb_func_global dealHostageDamage
dealHostageDamage: @ 0x08032228
	push {lr}
	bl modifyPlayerHealth
	ldr r1, =0x03001F08
	add r1, #0x85
	ldrb r0, [r1]
	mov r2, #1
	orr r0, r2
	strb r0, [r1]
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global ModifySpecialCharge
ModifySpecialCharge: @ 0x08032244
	push {r4, lr}
	add r4, r0, #0
	bl isUsingInvulnSpecial
	cmp r0, #0
	beq _08032258
	cmp r4, #0
	ble _08032258
	cmp r4, #0x4f
	ble _08032260
_08032258:
	ldr r0, =0x03001F24
	add r1, r4, #0
	bl sub_08032294
_08032260:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0803226C
sub_0803226C: @ 0x0803226C
	push {lr}
	add r1, r0, #0
	ldr r0, =0x03001F40
	bl sub_08032294
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08032280
sub_08032280: @ 0x08032280
	push {lr}
	add r1, r0, #0
	ldr r0, =0x03001F5C
	bl sub_08032294
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08032294
sub_08032294: @ 0x08032294
	push {r4, lr}
	add r4, r0, #0
	cmp r1, #0
	beq _0803230C
	ldr r0, [r4, #0x10]
	cmp r0, #0
	ble _080322A6
	cmp r1, #0
	bgt _080322AE
_080322A6:
	cmp r0, #0
	bge _080322B2
	cmp r1, #0
	bge _080322B2
_080322AE:
	ldr r0, [r4, #8]
	b _080322B6
_080322B2:
	ldr r0, [r4, #8]
	str r0, [r4]
_080322B6:
	add r2, r0, r1
	ldr r0, =0x03001F24
	cmp r4, r0
	bne _080322C8
	cmp r2, #0x4f
	bne _080322C8
	cmp r1, #0
	ble _080322C8
	mov r2, #0x50
_080322C8:
	cmp r2, #0
	bgt _080322D8
	mov r0, #0
	str r0, [r4, #8]
	b _080322E4
	.align 2, 0
.pool
_080322D8:
	ldr r0, [r4, #0xc]
	cmp r2, r0
	bls _080322E2
	str r0, [r4, #8]
	b _080322E4
_080322E2:
	str r2, [r4, #8]
_080322E4:
	ldr r1, [r4, #8]
	ldr r0, [r4]
	cmp r1, r0
	bhs _080322F8
	sub r0, r1, r0
	mov r1, #0x1e
	bl Bios_Div
	sub r0, #1
	b _0803230A
_080322F8:
	cmp r1, r0
	bls _08032308
	sub r0, r1, r0
	mov r1, #0x1e
	bl Bios_Div
	add r0, #1
	b _0803230A
_08032308:
	mov r0, #0
_0803230A:
	str r0, [r4, #0x10]
_0803230C:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
	
	
	
	thumb_func_global sub_8032314
sub_8032314: @ 0x08032314
	push {lr}
	ldr r0, =0x03001F08
	mov r1, #1
	bl sub_80323B4
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_8032328
sub_8032328: @ 0x08032328
	push {lr}
	ldr r0, =0x03001F24
	mov r1, #1
	bl sub_80323B4
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_803233C
sub_803233C: @ 0x0803233C
	push {lr}
	ldr r0, =0x03001F40
	mov r1, #1
	bl sub_80323B4
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_8032350
sub_8032350: @ 0x08032350
	push {lr}
	ldr r0, =0x03001F5C
	mov r1, #1
	bl sub_80323B4
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_8032364
sub_8032364: @ 0x08032364
	push {lr}
	ldr r0, =0x03001F08
	mov r1, #0
	bl sub_80323B4
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_8032378
sub_8032378: @ 0x08032378
	push {lr}
	ldr r0, =0x03001F24
	mov r1, #0
	bl sub_80323B4
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_803238C
sub_803238C: @ 0x0803238C
	push {lr}
	ldr r0, =0x03001F40
	mov r1, #0
	bl sub_80323B4
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_80323A0
sub_80323A0: @ 0x080323A0
	push {lr}
	ldr r0, =0x03001F5C
	mov r1, #0
	bl sub_80323B4
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_80323B4
sub_80323B4: @ 0x080323B4
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	cmp r1, #1
	bhi locret_80323BE
	strb r1, [r0, #0x14]
locret_80323BE:
	bx lr
	
	

thumb_func_global getHealthBarValue
getHealthBarValue: @ 0x080323C0
	push {lr}
	ldr r0, =0x03001F08
	bl readHalfWord
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global getPlayerSpecialBar
getPlayerSpecialBar: @ 0x080323D4
	push {lr}
	ldr r0, =0x03001F24
	bl readHalfWord
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global ldrh_3001F40
ldrh_3001F40: @ 0x080323E8
	push {lr}
	ldr r0, =0x03001F40
	bl readHalfWord
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global ldrh_3001F5C
ldrh_3001F5C: @ 0x080323FC
	push {lr}
	ldr r0, =0x03001F5C
	bl readHalfWord
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global readHalfWord
readHalfWord: @ 0x08032410
	ldrh r0, [r0]
	bx lr

thumb_func_global getPlayerHealth
getPlayerHealth: @ 0x08032414
	ldr r0, =0x03001F08
	ldrh r0, [r0, #8]
	bx lr
	.align 2, 0
.pool

thumb_func_global getPlayerSpecial
getPlayerSpecial: @ 0x08032420
	ldr r0, =0x03001F08
	ldrh r0, [r0, #0x24]
	bx lr
	.align 2, 0
.pool

thumb_func_global sub_0803242C
sub_0803242C: @ 0x0803242C
	push {lr}
	add r1, r0, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r0, =0x03001F08
	bl sub_0803245C
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08032444
sub_08032444: @ 0x08032444
	push {lr}
	add r1, r0, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r0, =0x03001F24
	bl sub_0803245C
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0803245C
sub_0803245C: @ 0x0803245C
	str r1, [r0, #8]
	str r1, [r0, #4]
	str r1, [r0]
	mov r1, #0
	str r1, [r0, #0x10]
	bx lr

thumb_func_global sub_08032468
sub_08032468: @ 0x08032468
	push {r4, lr}
	ldr r4, =0x03001F08
	ldr r2, =unk_8230B9C
	ldr r3, =0x02002120
	add r0, r4, #0
	mov r1, #0
	bl sub_080325D8
	add r0, r4, #0
	add r0, #0xa5
	ldrb r0, [r0]
	cmp r0, #0
	beq _0803249C
	ldr r0, =off_82323C8
	ldr r1, [r4, #0x18]
	add r1, #0x22
	ldrb r1, [r1]
	lsl r1, r1, #0x18
	asr r1, r1, #0x18
	lsl r1, r1, #5
	ldr r2, =0x05000200
	add r1, r1, r2
	mov r2, #0
	mov r3, #1
	bl palFadeMaybe
_0803249C:
	mov r0, #0x80
	strb r0, [r4, #0x14]
	ldr r0, [r4]
	str r0, [r4, #4]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_080324C0
sub_080324C0: @ 0x080324C0
	push {r4, r5, lr}
	ldr r4, =0x03001F24
	ldr r2, =unk_8230BE4
	ldr r3, =0x02009310
	add r0, r4, #0
	mov r1, #1
	bl sub_080325D8
	add r2, r4, #0
	sub r2, #0x1c
	mov r0, #0x80
	strb r0, [r4, #0x14]
	ldr r4, =0x03001FB0
	ldrh r1, [r4]
	ldr r0, =0x0000FFFF
	cmp r1, r0
	bne _08032524
	ldr r1, [r2, #0x1c]
	ldr r0, [r2, #0x28]
	cmp r1, r0
	bne _0803252C
	ldr r0, =off_8232308
	ldr r1, [r2, #0x34]
	add r1, #0x22
	ldrb r1, [r1]
	lsl r1, r1, #0x18
	asr r1, r1, #0x18
	lsl r1, r1, #5
	ldr r2, =0x05000200
	add r1, r1, r2
	mov r2, #0
	bl startPalAnimation
	strh r0, [r4]
	b _08032556
	.align 2, 0
.pool
_08032524:
	ldr r1, [r2, #0x1c]
	ldr r0, [r2, #0x28]
	cmp r1, r0
	beq _08032556
_0803252C:
	ldr r4, =0x03001FB0
	ldrh r0, [r4]
	ldr r5, =0x0000FFFF
	cmp r0, r5
	beq _08032556
	ldr r0, =doorSymbolSprPal
	ldr r1, =0x03001F08
	ldr r1, [r1, #0x34]
	add r1, #0x22
	mov r2, #0
	ldrsb r2, [r1, r2]
	lsl r2, r2, #5
	ldr r1, =0x05000200
	add r2, r2, r1
	mov r1, #1
	bl memcpy_pal
	ldrh r0, [r4]
	bl freePalFadeTaskAtIndex
	strh r5, [r4]
_08032556:
	ldr r1, =0x03001F08
	ldr r0, [r1, #0x1c]
	str r0, [r1, #0x20]
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08032578
sub_08032578: @ 0x08032578
	push {r4, lr}
	ldr r4, =0x03001F40
	ldr r2, =unk_8230C18
	ldr r3, =0x020093B0
	add r0, r4, #0
	mov r1, #0
	bl sub_080325D8
	add r1, r4, #0
	sub r1, #0x38
	mov r0, #0x80
	strb r0, [r4, #0x14]
	ldr r0, [r1, #0x38]
	str r0, [r1, #0x3c]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_080325A8
sub_080325A8: @ 0x080325A8
	push {r4, lr}
	ldr r4, =0x03001F5C
	ldr r2, =unk_8230C4C
	ldr r3, =0x0201AEB0
	add r0, r4, #0
	mov r1, #0
	bl sub_080325D8
	add r1, r4, #0
	sub r1, #0x54
	mov r0, #0x80
	strb r0, [r4, #0x14]
	ldr r0, [r1, #0x54]
	str r0, [r1, #0x58]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_080325D8
sub_080325D8: @ 0x080325D8
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #4
	add r5, r0, #0
	mov r8, r2
	add r4, r3, #0
	lsl r1, r1, #0x10
	lsr r6, r1, #0x10
	ldr r1, [r5]
	ldr r0, [r5, #4]
	cmp r1, r0
	bne _080325FE
	ldrb r0, [r5, #0x14]
	cmp r0, #0x80
	bne _080325FE
	b _080326FA
_080325FE:
	mov r0, r8
	ldrb r7, [r0]
	mov r0, sp
	mov r1, #0
	mov sb, r1
	strh r1, [r0]
	add r0, r7, #3
	lsl r2, r0, #1
	add r2, r2, r0
	lsl r2, r2, #1
	mov r0, #0x80
	lsl r0, r0, #0x11
	orr r2, r0
	mov r0, sp
	add r1, r4, #0
	bl Bios_memcpy
	ldrb r0, [r5, #0x14]
	cmp r0, #1
	beq _0803262E
	ldr r0, =0x03003D40
	ldrb r0, [r0, #0x12]
	cmp r0, #0
	beq _08032640
_0803262E:
	ldr r0, [r5, #0x18]
	add r0, #0x23
	ldrb r1, [r0]
	mov r2, #0x10
	orr r1, r2
	strb r1, [r0]
	b _080326FA
	.align 2, 0
.pool
_08032640:
	ldr r1, [r5, #0x18]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	add r0, r7, #2
	strh r0, [r4]
	add r4, #0xc
	ldr r0, =unk_8232a16
	lsl r2, r6, #2
	add r0, r2, r0
	ldrh r0, [r0]
	strh r0, [r4]
	ldr r0, =0x0000FFF8
	strh r0, [r4, #2]
	ldrb r0, [r4, #8]
	mov r1, #0x1f
	orr r0, r1
	strb r0, [r4, #8]
	add r4, #0xc
	ldr r0, [r5]
	ldr r1, [r5, #0xc]
	mov sl, r2
	cmp r0, r1
	bhs _08032690
	mul r0, r7, r0
	lsl r0, r0, #3
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	lsl r3, r7, #3
	mov sb, r3
	b _08032696
	.align 2, 0
.pool
_08032690:
	lsl r6, r7, #3
	add r0, r6, #0
	mov sb, r0
_08032696:
	mov r3, #0
	cmp r3, r7
	bhs _080326E4
	mov r0, #7
	and r0, r6
	lsl r0, r0, #1
	mov ip, r0
_080326A4:
	lsl r1, r3, #2
	mov r0, r8
	add r0, #4
	add r0, r0, r1
	ldr r2, [r0]
	add r0, r3, #1
	lsl r1, r0, #3
	add r5, r0, #0
	cmp r6, r1
	bge _080326CA
	lsl r1, r3, #3
	cmp r6, r1
	bge _080326C2
	ldrh r0, [r2]
	b _080326CE
_080326C2:
	mov r3, ip
	add r0, r2, r3
	ldrh r0, [r0]
	b _080326CE
_080326CA:
	ldrh r0, [r2, #0x10]
	lsl r1, r3, #3
_080326CE:
	strh r0, [r4]
	strh r1, [r4, #2]
	ldrb r0, [r4, #8]
	mov r1, #0x1f
	orr r0, r1
	strb r0, [r4, #8]
	add r4, #0xc
	lsl r0, r5, #0x10
	lsr r3, r0, #0x10
	cmp r3, r7
	blo _080326A4
_080326E4:
	ldr r0, =unk_8232a16
	add r0, #2
	add r0, sl
	ldrh r0, [r0]
	strh r0, [r4]
	mov r0, sb
	strh r0, [r4, #2]
	ldrb r0, [r4, #8]
	mov r1, #0x1f
	orr r0, r1
	strb r0, [r4, #8]
_080326FA:
	add sp, #4
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global giveKey
giveKey: @ 0x08032710
	push {r4, r5, lr}
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	ldr r5, =0x03001F08
	add r2, r5, #0
	add r2, #0x70
	mov r0, #1
	lsl r0, r4
	ldrh r1, [r2]
	orr r0, r1
	strh r0, [r2]
	add r0, r4, #0
	bl drawKeyToHUD
	cmp r4, #0
	bne _0803275A
	add r0, r5, #0
	add r0, #0x72
	ldrh r0, [r0]
	cmp r0, #0
	bne _0803275A
	ldr r2, =off_8232908
	ldr r1, =0x03003D40
	ldrb r0, [r1, #2]
	lsl r0, r0, #2
	ldrb r1, [r1, #1]
	lsl r1, r1, #4
	add r0, r0, r1
	add r0, r0, r2
	ldr r0, [r0]
	cmp r0, #0
	beq _0803275A
	mov r1, #0xa0
	lsl r1, r1, #0x13
	mov r2, #0
	bl startPalAnimation
_0803275A:
	ldr r1, =0x03002018
	lsl r0, r4, #1
	add r0, r0, r1
	mov r1, #0
	strh r1, [r0]
	ldr r1, =keyBlinkTasks
	lsl r0, r4, #2
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0xff
	bl createTask
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
.pool



thumb_func_global task_blinkKeyHUD_red
task_blinkKeyHUD_red: @ 0x0803278C
	push {lr}
	mov r0, #0
	bl blinkKey
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _080327AA
	mov r0, #0
	bl drawKeyToHUD
	ldr r1, =0x03002018
	mov r0, #0
	strh r0, [r1]
	bl endCurrentTask
_080327AA:
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global task_blinkKeyHUD_blue
task_blinkKeyHUD_blue: @ 0x080327B4
	push {lr}
	mov r0, #1
	bl blinkKey
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _080327D2
	mov r0, #1
	bl drawKeyToHUD
	ldr r1, =0x03002018
	mov r0, #0
	strh r0, [r1, #2]
	bl endCurrentTask
_080327D2:
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global task_blinkKeyHUD_yellow
task_blinkKeyHUD_yellow: @ 0x080327DC
	push {lr}
	mov r0, #2
	bl blinkKey
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _080327FA
	mov r0, #2
	bl drawKeyToHUD
	ldr r1, =0x03002018
	mov r0, #0
	strh r0, [r1, #4]
	bl endCurrentTask
_080327FA:
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global task_blinkKeyHUD_green
task_blinkKeyHUD_green: @ 0x08032804
	push {lr}
	mov r0, #3
	bl blinkKey
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08032822
	mov r0, #3
	bl drawKeyToHUD
	ldr r1, =0x03002018
	mov r0, #0
	strh r0, [r1, #6]
	bl endCurrentTask
_08032822:
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global blinkKey
blinkKey: @ 0x0803282C
	push {r4, lr}
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	ldr r1, =0x03003D40
	ldrb r0, [r1, #0x10]
	cmp r0, #0
	bne _08032884
	ldrb r0, [r1, #6]
	cmp r0, #8
	beq _08032884
	ldr r0, =0x03002018
	lsl r2, r3, #1
	add r0, r2, r0
	ldrh r1, [r0]
	mov r0, #4
	and r0, r1
	add r4, r2, #0
	cmp r0, #0
	beq _08032864
	add r0, r3, #0
	bl hideKeyHUD
	b _0803286A
	.align 2, 0
.pool
_08032864:
	add r0, r3, #0
	bl drawKeyToHUD
_0803286A:
	ldr r1, =0x03002018
	add r1, r4, r1
	ldrh r0, [r1]
	add r0, #1
	strh r0, [r1]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0x20
	bhi _08032884
	mov r0, #0
	b _08032886
	.align 2, 0
.pool
_08032884:
	mov r0, #1
_08032886:
	pop {r4}
	pop {r1}
	bx r1
	
	
	
	

thumb_func_global hasKey
hasKey: @ 0x0803288C
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r1, =0x03001F08
	add r1, #0x70
	ldrh r1, [r1]
	asr r1, r0
	mov r0, #1
	and r1, r0
	cmp r1, #0
	bne _080328A8
	mov r0, #0
	b _080328AA
	.align 2, 0
.pool
_080328A8:
	mov r0, #1
_080328AA:
	bx lr

thumb_func_global sub_080328AC
sub_080328AC: @ 0x080328AC
	push {r4, r5, r6, lr}
	lsl r5, r0, #0x10
	lsr r4, r5, #0x10
	add r6, r4, #0
	add r0, r4, #0
	bl findOpenedDoorPos
	add r0, r4, #0
	bl isCollisionLockedDoor
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _0803293C
	mov r5, #0xf
	and r5, r4
	cmp r5, #0
	bne _080328DC
	ldr r0, =0x03001F08
	add r0, #0x72
	ldrh r0, [r0]
	cmp r0, #0
	beq _080328DC
	bl sub_08034B40
_080328DC:
	add r0, r5, #0
	bl hasKey
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08032928
	cmp r5, #0
	bne _08032910
	ldr r0, =0x03001F08
	add r0, #0x72
	ldrh r0, [r0]
	cmp r0, #0
	beq _0803290C
	mov r0, #0
	bl sub_080349A8
	ldr r0, =0x00000105
	bl playSong
	b _08032970
	.align 2, 0
.pool
_0803290C:
	bl sub_8033184
_08032910:
	add r0, r6, #0
	bl getDoorWarpDest
	ldr r1, =0x03003D40
	strh r0, [r1, #6]
	add r0, r6, #0
	bl sub_08034D70
	mov r0, #1
	b _08032972
	.align 2, 0
.pool
_08032928:
	add r0, r5, #0
	bl sub_080349A8
	ldr r0, =0x00000105
	bl playSong
	b _08032970
	.align 2, 0
.pool
_0803293C:
	add r0, r4, #0
	bl isCollisionDoor
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08032960
	add r0, r4, #0
	bl sub_08034D70
	add r0, r4, #0
	bl getDoorWarpDest
	ldr r1, =0x03003D40
	strh r0, [r1, #6]
	mov r0, #1
	b _08032972
	.align 2, 0
.pool
_08032960:
	lsr r0, r5, #0x14
	cmp r0, #0xa
	bne _08032970
	mov r0, #0xf
	and r6, r0
	add r0, r6, #0
	bl sub_08014EB0
_08032970:
	mov r0, #0
_08032972:
	pop {r4, r5, r6}
	pop {r1}
	bx r1

thumb_func_global sub_08032978
sub_08032978: @ 0x08032978
	push {lr}
	sub sp, #4
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	cmp r0, #5
	beq _08032992
	ldr r0, [r1]
	ldr r1, =0x00FFFF00
	and r0, r1
	mov r1, #0x81
	lsl r1, r1, #0xa
	cmp r0, r1
	bne _080329B0
_08032992:
	ldr r0, =0x03002014
	ldr r1, [r0]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	b _080329C4
	.align 2, 0
.pool
_080329B0:
	mov r1, sp
	mov r2, #0xc1
	lsl r2, r2, #8
	add r0, r2, #0
	strh r0, [r1]
	ldr r1, =0x0600FA9C
	ldr r2, =0x01000002
	mov r0, sp
	bl Bios_memcpy
_080329C4:
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_080329D4
sub_080329D4: @ 0x080329D4
	push {r4, lr}
	sub sp, #4
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	cmp r0, #5
	beq _080329EE
	ldr r0, [r1]
	ldr r1, =0x00FFFF00
	and r0, r1
	mov r1, #0x81
	lsl r1, r1, #0xa
	cmp r0, r1
	bne _08032A0C
_080329EE:
	ldr r0, =0x03002014
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	b _08032A32
	.align 2, 0
.pool
_08032A0C:
	mov r1, sp
	ldr r2, =0x0000C117
	add r0, r2, #0
	strh r0, [r1]
	ldr r1, =0x0600FA9C
	ldr r4, =0x01000001
	mov r0, sp
	add r2, r4, #0
	bl Bios_memcpy
	mov r0, sp
	add r0, #2
	ldr r2, =0x0000C118
	add r1, r2, #0
	strh r1, [r0]
	ldr r1, =0x0600FA9E
	add r2, r4, #0
	bl Bios_memcpy
_08032A32:
	add sp, #4
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global hideKeyHUD
hideKeyHUD: @ 0x08032A50
	push {lr}
	sub sp, #4
	add r1, r0, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r2, =dword_8232A10
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #1
	add r0, r0, r2
	add r1, #3
	ldrh r0, [r0]
	add r1, r1, r0
	lsl r1, r1, #1
	ldr r0, =0x0600FCC4
	add r1, r1, r0
	mov r2, sp
	mov r3, #0xc1
	lsl r3, r3, #8
	add r0, r3, #0
	strh r0, [r2]
	ldr r2, =0x01000001
	mov r0, sp
	bl Bios_memcpy
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global drawKeyToHUD
drawKeyToHUD: @ 0x08032A9C
	push {r4, lr}
	sub sp, #4
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	ldr r0, =0x03003D40
	ldrb r0, [r0, #0x12]
	cmp r0, #0
	bne _08032AD8
	ldr r1, =dword_8232A10
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #1
	add r0, r0, r1
	add r1, r4, #3
	ldrh r0, [r0]
	add r1, r1, r0
	lsl r1, r1, #1
	ldr r0, =0x0600FCC4
	add r1, r1, r0
	mov r3, sp
	ldr r2, =keyTileMaps
	lsl r0, r4, #1
	add r0, r0, r2
	ldrh r0, [r0]
	strh r0, [r3]
	ldr r2, =0x01000001
	mov r0, sp
	bl Bios_memcpy
_08032AD8:
	add sp, #4
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global saveHostage
saveHostage: @ 0x08032AF8
	push {r4, lr}
	ldr r0, =0x03001F08
	add r4, r0, #0
	add r4, #0x72
	ldrh r0, [r4]
	cmp r0, #0
	beq _08032B40
	sub r0, #1
	strh r0, [r4]
	bl clearHostage_drawDone
	ldrh r0, [r4]
	cmp r0, #0
	bne _08032B40
	mov r0, #0
	bl hasKey
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08032B40
	ldr r2, =off_8232908
	ldr r1, =0x03003D40
	ldrb r0, [r1, #2]
	lsl r0, r0, #2
	ldrb r1, [r1, #1]
	lsl r1, r1, #4
	add r0, r0, r1
	add r0, r0, r2
	ldr r0, [r0]
	cmp r0, #0
	beq _08032B40
	mov r1, #0xa0
	lsl r1, r1, #0x13
	mov r2, #0
	bl startPalAnimation
_08032B40:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global clearHostage_drawDone
clearHostage_drawDone: @ 0x08032B54
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #4
	mov r1, sp
	mov r2, #0xc1
	lsl r2, r2, #8
	add r0, r2, #0
	strh r0, [r1]
	ldr r4, =0x03001F08
	add r4, #0x72
	ldrh r1, [r4]
	lsl r1, r1, #1
	ldr r0, =0x0600FCEA
	add r1, r1, r0
	ldr r2, =0x01000001
	mov r0, sp
	bl Bios_memcpy
	ldrh r0, [r4]
	cmp r0, #0
	bne _08032BCE
	mov r4, #0
	ldr r3, =doneMapLookup
	ldr r0, =0x03002080
	add r2, r0, #0
	add r2, #0x2c
	ldrb r0, [r2]
	lsl r0, r0, #2
	add r1, r3, #2
	add r0, r0, r1
	ldrh r0, [r0]
	cmp r4, r0
	bhs _08032BCE
	mov r5, sp
	add r5, #2
	mov r8, r3
	add r6, r2, #0
	add r7, r1, #0
_08032BA2:
	ldrb r0, [r6]
	lsl r0, r0, #2
	add r0, r8
	ldrh r0, [r0]
	add r0, r4, r0
	strh r0, [r5]
	lsl r1, r4, #1
	ldr r2, =0x0600FCEA
	add r1, r1, r2
	add r0, r5, #0
	ldr r2, =0x01000001
	bl Bios_memcpy
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	ldrb r0, [r6]
	lsl r0, r0, #2
	add r0, r0, r7
	ldrh r0, [r0]
	cmp r4, r0
	blo _08032BA2
_08032BCE:
	add sp, #4
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global drawRescue
drawRescue: @ 0x08032BF0
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #4
	ldr r2, =0x03003D40
	ldrb r0, [r2, #0x12]
	cmp r0, #0
	bne _08032CF4
	ldr r0, =0x03001F08
	add r0, #0x72
	ldrh r0, [r0]
	cmp r0, #0
	beq _08032C68
	mov r4, #0
	mov r5, sp
_08032C0E:
	ldr r1, =0x0000C105
	add r0, r4, r1
	strh r0, [r5]
	lsl r1, r4, #1
	ldr r2, =0x0600FCE2
	add r1, r1, r2
	mov r0, sp
	ldr r2, =0x01000001
	bl Bios_memcpy
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #3
	bls _08032C0E
	mov r1, sp
	ldr r2, =0x0000C109
	add r0, r2, #0
	strh r0, [r1]
	ldr r1, =0x0600FCEA
	ldr r0, =0x03001F08
	add r0, #0x72
	ldrh r2, [r0]
	mov r0, #0x80
	lsl r0, r0, #0x11
	orr r2, r0
	mov r0, sp
	bl Bios_memcpy
	b _08032CF4
	.align 2, 0
.pool
_08032C68:
	ldr r3, =hostageCounts
	ldrb r1, [r2, #2]
	lsl r1, r1, #1
	ldrb r0, [r2, #1]
	lsl r0, r0, #3
	add r1, r1, r0
	ldrb r2, [r2, #0x11]
	lsl r0, r2, #1
	add r0, r0, r2
	lsl r0, r0, #4
	add r1, r1, r0
	add r1, r1, r3
	ldrh r0, [r1]
	cmp r0, #0
	beq _08032CF4
	mov r4, #0
	mov r5, sp
_08032C8A:
	ldr r1, =0x0000C105
	add r0, r4, r1
	strh r0, [r5]
	lsl r1, r4, #1
	ldr r2, =0x0600FCE2
	add r1, r1, r2
	mov r0, sp
	ldr r2, =0x01000001
	bl Bios_memcpy
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #3
	bls _08032C8A
	mov r4, #0
	ldr r3, =doneMapLookup
	ldr r0, =0x03002080
	add r2, r0, #0
	add r2, #0x2c
	ldrb r0, [r2]
	lsl r0, r0, #2
	add r1, r3, #2
	add r0, r0, r1
	ldrh r0, [r0]
	cmp r4, r0
	bhs _08032CF4
	mov r6, sp
	mov r8, r3
	add r5, r2, #0
	add r7, r1, #0
_08032CC8:
	ldrb r0, [r5]
	lsl r0, r0, #2
	add r0, r8
	ldrh r0, [r0]
	add r0, r4, r0
	strh r0, [r6]
	lsl r1, r4, #1
	ldr r0, =0x0600FCEA
	add r1, r1, r0
	mov r0, sp
	ldr r2, =0x01000001
	bl Bios_memcpy
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	ldrb r0, [r5]
	lsl r0, r0, #2
	add r0, r0, r7
	ldrh r0, [r0]
	cmp r4, r0
	blo _08032CC8
_08032CF4:
	add sp, #4
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08032D1C
sub_08032D1C: @ 0x08032D1C
	push {lr}
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	cmp r0, #5
	bne _08032D3C
	ldrb r0, [r1]
	cmp r0, #2
	beq _08032D3C
	ldr r0, =0x03001F08
	add r0, #0x84
	mov r1, #1
	strb r1, [r0]
	ldr r0, =(sub_8032D4C+1)
	mov r1, #0xff
	bl createTask
_08032D3C:
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
	
	
	
	thumb_func_global sub_8032D4C
sub_8032D4C: @ 0x08032D4C
	push {r4, lr}
	ldr r2, =0x03001F08
	add r0, r2, #0
	add r0, #0x84
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _08032D98
	add r4, r2, #0
	add r4, #0x80
	ldr r0, [r4]
	sub r0, #1
	str r0, [r4]
	cmp r0, #0
	bgt _08032D70
	mov r0, #0
	str r0, [r4]
_08032D70:
	ldr r1, [r4]
	ldr r0, =0x00000E4C
	cmp r1, r0
	bne _08032D7C
	bl sub_08032DDC
_08032D7C:
	ldr r1, [r4]
	ldr r0, =0x00000744
	cmp r1, r0
	bne _08032D98
	ldr r0, =0x03002012
	mov r1, #0
	strh r1, [r0]
	mov r0, #0xd0
	bl playSong
	ldr r0, =(sub_8032E40+1)
	mov r1, #0xff
	bl createTask
_08032D98:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
	
	
	

thumb_func_global sub_08032DB4
sub_08032DB4: @ 0x08032DB4
	ldr r0, =0x03001F08
	add r0, #0x84
	ldrb r2, [r0]
	mov r1, #1
	orr r1, r2
	strb r1, [r0]
	bx lr
	.align 2, 0
.pool
	
	
	thumb_func_global sub_8032DC8
sub_8032DC8: @ 0x08032DC8
	ldr r0, =0x03001F08
	add r0, #0x84
	ldrb r2, [r0]
	mov r1, #0xfe
	and r1, r2
	strb r1, [r0]
	bx lr
	.align 2, 0
.pool
	
	
	
thumb_func_global sub_08032DDC
sub_08032DDC: @ 0x08032DDC
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	cmp r0, #5
	bne _08032DF6
	ldrb r0, [r1]
	cmp r0, #2
	beq _08032DF6
	ldr r0, =0x03001F08
	add r0, #0x84
	ldrb r2, [r0]
	mov r1, #2
	orr r1, r2
	strb r1, [r0]
_08032DF6:
	bx lr
	.align 2, 0
.pool

thumb_func_global nullsub_11
nullsub_11: @ 0x08032E00
	bx lr
	.align 2, 0

thumb_func_global sub_08032E04
sub_08032E04: @ 0x08032E04
	push {r4, lr}
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	cmp r0, #5
	bne _08032E30
	ldrb r0, [r1]
	cmp r0, #2
	beq _08032E30
	ldr r4, =0x03001FB8
	ldr r1, [r4]
	add r1, #0x23
	ldrb r3, [r1]
	mov r2, #0x11
	neg r2, r2
	add r0, r2, #0
	and r0, r3
	strb r0, [r1]
	ldr r0, [r4, #4]
	add r0, #0x23
	ldrb r1, [r0]
	and r2, r1
	strb r2, [r0]
_08032E30:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool


thumb_func_global sub_8032E40
sub_8032E40:
	push {r4, lr}
	ldr r0, =0x03004750
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	cmp r0, #0
	blt _08032EA2
	ldr r0, =0x03002012
	ldrh r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	bne _08032E9A
	ldr r4, =dword_8232A44
	lsr r0, r1, #1
	mov r1, #0xc
	bl Bios_modulo
	lsl r0, r0, #1
	add r0, r0, r4
	ldrh r2, [r0]
	ldr r4, =0x03003D94
	ldrh r1, [r4]
	mov r0, #0xff
	lsl r0, r0, #8
	and r0, r1
	orr r0, r2
	strh r0, [r4]
	cmp r2, #1
	bne _08032E9A
	bl sub_8032F80
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08032E9A
	mov r1, #0x80
	lsl r1, r1, #5
	add r0, r1, #0
	strh r0, [r4]
	mov r0, #0xd0
	mov r1, #4
	bl sub_8010550
	bl endCurrentTask
_08032E9A:
	ldr r1, =0x03002012
	ldrh r0, [r1]
	add r0, #1
	strh r0, [r1]
_08032EA2:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
	thumb_func_global sub_8032EB8
	sub_8032EB8: @ 0x08032EB8
	push {r4, r5, lr}
	add r4, r0, #0
	add r5, r1, #0
	ldrb r0, [r4]
	cmp r0, #1
	beq _08032EF0
	cmp r0, #1
	bgt _08032ECE
	cmp r0, #0
	beq _08032ED4
	b _08032F46
_08032ECE:
	cmp r0, #2
	beq _08032F42
	b _08032F46
_08032ED4:
	strh r0, [r5]
	ldr r2, =0x03003D00
	ldrh r1, [r2]
	ldr r0, =0x0000FFFC
	and r0, r1
	strh r0, [r2]
	ldrb r0, [r4]
	add r0, #1
	strb r0, [r4]
	b _08032F46
	.align 2, 0
.pool
_08032EF0:
	ldrh r1, [r5]
	mov r0, #3
	and r0, r1
	cmp r0, #0
	bne _08032F3A
	ldr r1, =0x03003D94
	ldrh r0, [r1]
	lsr r3, r0, #8
	lsl r0, r0, #0x18
	lsr r2, r0, #0x18
	cmp r3, #0
	bne _08032F24
	cmp r2, #0x10
	bne _08032F2A
	strh r3, [r5]
	ldrb r0, [r4]
	add r0, #1
	strb r0, [r4]
	ldr r0, =0x03003D74
	strh r3, [r0]
	b _08032F46
	.align 2, 0
.pool
_08032F24:
	sub r0, r3, #1
	lsl r0, r0, #0x18
	lsr r3, r0, #0x18
_08032F2A:
	cmp r2, #0xf
	bhi _08032F34
	add r0, r2, #1
	lsl r0, r0, #0x18
	lsr r2, r0, #0x18
_08032F34:
	lsl r0, r3, #8
	orr r0, r2
	strh r0, [r1]
_08032F3A:
	ldrh r0, [r5]
	add r0, #1
	strh r0, [r5]
	b _08032F46
_08032F42:
	mov r0, #1
	b _08032F48
_08032F46:
	mov r0, #0
_08032F48:
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0



thumb_func_global getLaunchSiteTime
getLaunchSiteTime: @ 0x08032F50
	ldr r0, =0x03001F08
	add r0, #0x80
	ldrh r0, [r0]
	bx lr
	.align 2, 0
.pool

thumb_func_global sub_08032F5C
sub_08032F5C: @ 0x08032F5C
	ldr r0, =0x03003D40
	ldrb r0, [r0, #1]
	cmp r0, #4
	bls _08032F6E
	ldr r0, =0x03001F08
	add r0, #0x80
	ldr r0, [r0]
	cmp r0, #0x3b
	ble _08032F7C
_08032F6E:
	mov r0, #0
	b _08032F7E
	.align 2, 0
.pool
_08032F7C:
	mov r0, #1
_08032F7E:
	bx lr
	

thumb_func_global sub_8032F80
sub_8032F80:
	ldr r0, =0x03003D40
	ldrb r0, [r0, #1]
	cmp r0, #4
	bls _08032F96
	ldr r0, =0x03001F08
	add r0, #0x84
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _08032FA4
_08032F96:
	mov r0, #0
	b locret_8032FA6
	.align 2, 0
.pool
_08032FA4:
	mov r0, #1
locret_8032FA6:
	bx lr
	
	
thumb_func_global sub_08032FA8
sub_08032FA8: @ 0x08032FA8
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	ldr r0, =0x03003D40
	ldrb r0, [r0, #1]
	cmp r0, #5
	beq _08032FBA
	b _080330F6
_08032FBA:
	ldr r4, =0x03001F08
	mov r0, #0x80
	add r0, r0, r4
	mov sb, r0
	ldr r0, [r0]
	mov r1, #0x3c
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r8, r0
	mov r1, #0xa
	bl Bios_Div
	lsl r0, r0, #0x10
	ldr r7, =0x03001FB8
	ldr r3, [r7]
	ldr r5, =hudTimerAnims
	lsr r0, r0, #0xe
	add r0, r0, r5
	ldr r1, [r0]
	ldrh r2, [r3, #0xc]
	ldr r6, =hudTimerSprTiles
	add r0, r3, #0
	add r3, r6, #0
	bl setSpriteAnimation
	mov r0, r8
	mov r1, #0xa
	bl Bios_modulo
	lsl r0, r0, #0x10
	ldr r3, [r7, #4]
	lsr r0, r0, #0xe
	add r0, r0, r5
	ldr r1, [r0]
	ldrh r2, [r3, #0xc]
	add r0, r3, #0
	add r3, r6, #0
	bl setSpriteAnimation
	add r4, #0x84
	ldrb r4, [r4]
	mov r0, #2
	and r0, r4
	cmp r0, #0
	beq _080330B0
	mov r1, #1
	mov r0, #1
	and r0, r4
	cmp r0, #0
	beq _08033094
	mov r0, sb
	ldr r4, [r0]
	cmp r4, #0
	beq _08033094
	cmp r4, #0x3b
	bgt _08033058
	ldr r5, [r7]
	ldr r3, [r7, #4]
	ldr r0, =0x03002080
	ldr r2, [r0]
	mov r0, #4
	and r2, r0
	lsr r2, r2, #2
	b _0803306C
	.align 2, 0
.pool
_08033058:
	ldr r0, =0x00000257
	cmp r4, r0
	bgt _08033094
	ldr r5, [r7]
	ldr r3, [r7, #4]
	ldr r0, =0x03002080
	ldr r2, [r0]
	mov r0, #8
	and r2, r0
	lsr r2, r2, #3
_0803306C:
	add r3, #0x23
	and r2, r1
	lsl r2, r2, #4
	ldrb r4, [r3]
	mov r1, #0x11
	neg r1, r1
	add r0, r1, #0
	and r0, r4
	orr r0, r2
	strb r0, [r3]
	add r5, #0x23
	ldrb r0, [r5]
	and r1, r0
	orr r1, r2
	strb r1, [r5]
	b _080330C6
	.align 2, 0
.pool
_08033094:
	ldr r3, [r7]
	ldr r2, [r7, #4]
	add r2, #0x23
	ldrb r4, [r2]
	mov r1, #0x11
	neg r1, r1
	add r0, r1, #0
	and r0, r4
	strb r0, [r2]
	add r3, #0x23
	ldrb r0, [r3]
	and r1, r0
	strb r1, [r3]
	b _080330C6
_080330B0:
	ldr r1, [r7]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	ldr r1, [r7, #4]
	add r1, #0x23
	ldrb r0, [r1]
	orr r0, r2
	strb r0, [r1]
_080330C6:
	ldr r2, =0x03001F08
	add r0, r2, #0
	add r0, #0x84
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq _080330F6
	add r0, r2, #0
	add r0, #0x80
	ldr r0, [r0]
	cmp r0, #0
	beq _080330F6
	mov r1, #0x3c
	bl Bios_modulo
	cmp r0, #0
	bne _080330F6
	mov r1, r8
	cmp r1, #0xa
	bhi _080330F6
	ldr r0, =0x000001BD
	bl playSong
_080330F6:
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_0803310C
sub_0803310C: @ 0x0803310C
	ldr r1, =0x03003D40
	ldrb r0, [r1, #3]
	cmp r0, #0
	bne _0803311A
	ldrb r0, [r1, #4]
	cmp r0, #0
	beq _08033120
_0803311A:
	ldrb r0, [r1, #0x12]
	cmp r0, #0
	beq _0803312A
_08033120:
	ldr r0, =0x03001F08
	mov r1, #0
	str r1, [r0, #0x74]
	add r0, #0x78
	strb r1, [r0]
_0803312A:
	bx lr
	.align 2, 0
.pool




thumb_func_global sub_8033134
sub_8033134: @ 0x08033134
	ldr r2, =0x03001F08
	add r0, r2, #0
	add r0, #0x78
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	beq locret_8033154
	ldr r0, [r2, #0x74]
	add r0, #1
	str r0, [r2, #0x74]
	ldr r1, =0x0005879E
	cmp r0, r1
	bls locret_8033154
	ldr r0, =0x0005879F
	str r0, [r2, #0x74]
locret_8033154:
	bx lr
	.align 2, 0
.pool



thumb_func_global sub_08033164
sub_08033164: @ 0x08033164
	push {lr}
	ldr r0, =0x03003D40
	ldrb r0, [r0]
	cmp r0, #2
	bne _08033176
	ldr r0, =(sub_8033134+1)
	mov r1, #0xf0
	bl createTask
_08033176:
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global sub_8033184
sub_8033184: @ 0x08033184
	ldr r0, =0x03001F08
	add r0, #0x78
	ldrb r2, [r0]
	mov r1, #0xfe
	and r1, r2
	strb r1, [r0]
	bx lr
	.align 2, 0
.pool



thumb_func_global sub_8033198
sub_8033198:
	ldr r0, =0x03001F08
	add r0, #0x78
	ldrb r2, [r0]
	mov r1, #1
	orr r1, r2
	strb r1, [r0]
	bx lr
	.align 2, 0
.pool



	.thumb_func @ sub_80331AC
sub_80331AC: @ 0x080331AC
	ldr r0, =0x03001F08
	add r0, #0x78
	ldrb r2, [r0]
	mov r1, #0xfd
	and r1, r2
	strb r1, [r0]
	bx lr
	.align 2, 0
.pool


thumb_func_global sub_080331C0
sub_080331C0: @ 0x080331C0
	ldr r0, =0x03001F08
	add r0, #0x78
	ldrb r2, [r0]
	mov r1, #2
	orr r1, r2
	strb r1, [r0]
	bx lr
	.align 2, 0
.pool

thumb_func_global getDword3001F7C
getDword3001F7C: @ 0x080331D4
	ldr r0, =0x03001F08
	ldr r0, [r0, #0x74]
	bx lr
	.align 2, 0
.pool

thumb_func_global sub_080331E0
sub_080331E0: @ 0x080331E0
	add r1, r0, #0
	mov r0, #8
	strh r0, [r1]
	add r1, #0xc
	mov r2, #0
	ldr r3, =_080331F8
_80331EC:
	lsl r0, r2, #2
	add r0, r0, r3
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
.pool
_080331F8: .4byte _8033218
.4byte _8033218
.4byte _803321C
.4byte _8033218
.4byte _8033218
.4byte _8033220
.4byte _8033218
.4byte _8033218
_8033218:
	mov r0,#0
	b _8033222
_803321C:
	mov r0, #0xA
	b _8033222
_8033220:
	mov r0, #0xB
_8033222:
	strh r0, [r1]
	lsl r0, r2, #3
	strh r0, [r1, #2]
	add r1, #0xC
	add r0, r2, #1
	lsl r0, #0x10
	lsr r2, r0, #0x10
	cmp r2, #7
	bls _80331EC
	bx lr
	.align 2, 0




thumb_func_global sub_08033238
sub_08033238: @ 0x08033238
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	add r6, r1, #0
	add r6, #0xc
	mov r1, #0
	mov r8, r1
	add r7, r0, #0
	mov r2, #0xe1
	lsl r2, r2, #4
	mov sb, r2
	mov r1, sb
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0x63
	bls _08033264
	sub r4, #0x63
	mov r8, r4
	mov r4, #0x63
_08033264:
	add r0, r4, #0
	mov r1, #0xa
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	add r0, r4, #0
	mov r1, #0xa
	bl Bios_modulo
	strh r5, [r6]
	add r6, #0xc
	strh r0, [r6]
	add r6, #0x18
	add r0, r7, #0
	mov r1, sb
	bl Bios_modulo
	add r7, r0, #0
	mov r1, #0x3c
	bl Bios_Div
	mov r2, r8
	lsl r1, r2, #4
	sub r1, r1, r2
	lsl r1, r1, #2
	add r0, r0, r1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	add r0, r4, #0
	mov r1, #0xa
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	add r0, r4, #0
	mov r1, #0xa
	bl Bios_modulo
	strh r5, [r6]
	add r6, #0xc
	strh r0, [r6]
	add r6, #0x18
	add r0, r7, #0
	mov r1, #0x3c
	bl Bios_modulo
	add r7, r0, #0
	ldr r0, =0x03001F08
	ldr r1, [r0, #0x74]
	ldr r0, =0x0005879E
	cmp r1, r0
	bls _080332DC
	mov r4, #0x63
	b _080332EA
	.align 2, 0
.pool
_080332DC:
	mov r0, #0x64
	mul r0, r7, r0
	mov r1, #0x3c
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
_080332EA:
	add r0, r4, #0
	mov r1, #0xa
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	add r0, r4, #0
	mov r1, #0xa
	bl Bios_modulo
	strh r5, [r6]
	strh r0, [r6, #0xc]
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global sub_08033310
sub_08033310: @ 0x08033310
	push {lr}
	ldr r0, =0x03003D40
	ldrb r1, [r0]
	cmp r1, #2
	bne _08033356
	ldr r3, =0x03001F08
	add r0, r3, #0
	add r0, #0x78
	ldrb r0, [r0]
	and r1, r0
	cmp r1, #0
	bne _08033340
	ldr r0, [r3, #0x7c]
	add r0, #0x23
	ldrb r1, [r0]
	mov r2, #0x10
	orr r1, r2
	strb r1, [r0]
	b _08033356
	.align 2, 0
.pool
_08033340:
	ldr r1, [r3, #0x7c]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	ldr r0, [r3, #0x74]
	ldr r1, =0x0201FEE0
	bl sub_08033238
_08033356:
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global createScoreString
createScoreString: @ 0x08033360
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #0x14
	add r4, r0, #0
	add r7, r1, #0
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	mov r8, r2
	add r0, sp, #8
	mov r1, #0
	strh r1, [r0]
	mov r1, r8
	add r1, #4
	lsl r2, r1, #1
	add r2, r2, r1
	lsl r2, r2, #2
	mov r1, #0x80
	lsl r1, r1, #0x11
	orr r2, r1
	add r1, r7, #0
	bl Bios_memcpy
	add r2, r4, #0
	mov r3, #1
	mov r6, #0
_08033394:
	ldr r1, =dword_8232A5C
	lsl r0, r6, #2
	add r0, r0, r1
	ldr r5, [r0]
	add r0, r2, #0
	add r1, r5, #0
	str r2, [sp, #0xc]
	str r3, [sp, #0x10]
	bl Bios_Div
	mov r1, sp
	add r4, r1, r6
	strb r0, [r4]
	ldr r2, [sp, #0xc]
	add r0, r2, #0
	add r1, r5, #0
	bl Bios_modulo
	add r2, r0, #0
	ldrb r0, [r4]
	ldr r3, [sp, #0x10]
	cmp r0, #0
	beq _080333CE
	cmp r3, #1
	bhi _080333CE
	mov r0, #7
	sub r0, r0, r6
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
_080333CE:
	add r0, r6, #1
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	cmp r6, #6
	bls _08033394
	mov r0, r8
	cmp r0, #0
	beq _08033418
	add r0, r3, #2
	strh r0, [r7]
	add r7, #0xc
	ldr r2, =pointsLangLookup
	ldr r1, =0x03002080
	add r1, #0x2c
	ldrb r0, [r1]
	lsl r0, r0, #1
	add r0, r0, r2
	ldrh r0, [r0]
	add r0, #1
	strh r0, [r7]
	mov r0, #0x31
	strh r0, [r7, #2]
	add r7, #0xc
	ldrb r0, [r1]
	lsl r0, r0, #1
	add r0, r0, r2
	ldrh r0, [r0]
	strh r0, [r7]
	mov r0, #0x39
	strh r0, [r7, #2]
	b _0803341A
	.align 2, 0
.pool
_08033418:
	strh r3, [r7]
_0803341A:
	add r7, #0xc
	mov r6, #0
	cmp r6, r3
	bhs _0803344C
	mov r2, #6
_08033424:
	mov r1, r8
	cmp r1, #0
	beq _08033432
	sub r1, r2, r6
	lsl r0, r1, #3
	sub r0, r0, r1
	b _08033436
_08033432:
	sub r0, r2, r6
	lsl r0, r0, #3
_08033436:
	strh r0, [r7, #2]
	sub r0, r2, r6
	add r0, sp
	ldrb r0, [r0]
	strh r0, [r7]
	add r7, #0xc
	add r0, r6, #1
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	cmp r6, r3
	blo _08033424
_0803344C:
	add sp, #0x14
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0

thumb_func_global sub_08033458
sub_08033458: @ 0x08033458
	push {r4, r5, lr}
	ldr r2, =0x03003D40
	ldrb r0, [r2]
	cmp r0, #2
	beq _08033542
	ldr r4, =0x03001F90
	ldr r0, =0x03003C00
	add r1, r0, #0
	add r1, #0xce
	ldrh r0, [r1]
	cmp r0, #0
	beq _080334BC
	add r1, r0, #0
	mov r0, #0x64
	mul r1, r0, r1
	ldr r0, [r2, #0x1c]
	cmp r0, r1
	blo _080334BC
	ldr r5, =0x03001FB2
	ldrh r0, [r5]
	cmp r0, #0
	beq _080334BC
	ldr r3, [r4, #0x18]
	ldr r0, =0x03002080
	ldr r1, [r0]
	mov r0, #4
	and r1, r0
	add r3, #0x23
	lsr r1, r1, #2
	lsl r1, r1, #4
	ldrb r2, [r3]
	sub r0, #0x15
	and r0, r2
	orr r0, r1
	strb r0, [r3]
	ldrh r0, [r5]
	sub r0, #1
	strh r0, [r5]
	b _080334CA
	.align 2, 0
.pool
_080334BC:
	ldr r0, [r4, #0x18]
	add r0, #0x23
	ldrb r2, [r0]
	mov r1, #0x11
	neg r1, r1
	and r1, r2
	strb r1, [r0]
_080334CA:
	ldr r0, =0x03001F08
	add r5, r0, #0
	add r5, #0xa5
	ldrb r0, [r5]
	cmp r0, #0
	beq _080334F4
	ldr r0, =off_8230B78
	ldr r1, [r4, #0x18]
	add r1, #0x22
	ldrb r1, [r1]
	lsl r1, r1, #0x18
	asr r1, r1, #0x18
	lsl r1, r1, #5
	ldr r2, =0x05000200
	add r1, r1, r2
	mov r2, #0
	mov r3, #1
	bl palFadeMaybe
	mov r0, #0
	strb r0, [r5]
_080334F4:
	ldr r1, [r4]
	ldr r0, [r4, #4]
	ldrb r2, [r4, #0x14]
	cmp r1, r0
	bne _08033502
	cmp r2, #0x80
	beq _08033542
_08033502:
	cmp r2, #1
	beq _0803350E
	ldr r0, =0x03003D40
	ldrb r0, [r0, #0x12]
	cmp r0, #0
	beq _0803352C
_0803350E:
	ldr r0, [r4, #0x18]
	add r0, #0x23
	ldrb r1, [r0]
	mov r2, #0x10
	orr r1, r2
	strb r1, [r0]
	b _08033542
	.align 2, 0
.pool
_0803352C:
	lsl r0, r1, #2
	add r0, r0, r1
	lsl r0, r0, #1
	ldr r1, =0x0201A630
	mov r2, #1
	bl createScoreString
	mov r0, #0x80
	strb r0, [r4, #0x14]
	ldr r0, [r4]
	str r0, [r4, #4]
_08033542:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global openPauseMenu
openPauseMenu: @ 0x0803354C
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #4
	ldr r0, =0x03003D74
	ldrh r0, [r0]
	mov r1, #0xc0
	and r1, r0
	cmp r1, #0x80
	bne _08033562
	b _080336BE
_08033562:
	cmp r1, #0xc0
	bne _08033568
	b _080336BE
_08033568:
	ldr r0, =0x03003BD0
	ldrb r0, [r0, #0x1c]
	cmp r0, #2
	bne _08033572
	b _080336BE
_08033572:
	ldr r0, =0x03004720
	ldrh r0, [r0, #8]
	cmp r0, #6
	bne _0803357C
	b _080336BE
_0803357C:
	cmp r0, #7
	bne _08033582
	b _080336BE
_08033582:
	ldr r4, =0x03003D40
	ldrb r0, [r4, #0x10]
	cmp r0, #0
	beq _0803358C
	b _080336BE
_0803358C:
	ldrb r0, [r4, #6]
	cmp r0, #8
	bne _08033594
	b _080336BE
_08033594:
	bl getHealthBarValue
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _080335A0
	b _080336BE
_080335A0:
	ldrb r0, [r4]
	cmp r0, #2
	bne _080335B6
	ldr r0, =0x03001F08
	add r0, #0x78
	ldrb r1, [r0]
	mov r0, #1
	and r0, r1
	cmp r0, #0
	bne _080335B6
	b _080336BE
_080335B6:
	mov r0, #0x2c
	bl allocateSpriteTiles
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	lsl r4, r6, #0x10
	asr r5, r4, #0x10
	cmp r5, #0
	bge _080335CA
	b _080336BE
_080335CA:
	ldr r0, =pauseScreenPal
	mov r1, #1
	bl LoadSpritePalette
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	cmp r0, #0
	bge _080335F8
	lsr r0, r4, #0x10
	b _080336B8
	.align 2, 0
.pool
_080335F8:
	ldr r0, =pauseScreenTiles
	lsl r1, r5, #5
	ldr r2, =0x06010000
	add r1, r1, r2
	bl Bios_LZ77_16_2
	ldr r0, =0x03003D40
	ldrb r0, [r0]
	cmp r0, #2
	beq _08033620
	ldr r1, =pauseScreenSprites
	b _08033622
	.align 2, 0
.pool
_08033620:
	ldr r1, =timeTrialPauseSprites
_08033622:
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	lsr r2, r4, #0x10
	mov r1, #0
	str r1, [sp]
	mov r3, #0
	bl allocSprite
	ldr r1, =0x03001FB8
	str r0, [r1, #8]
	ldr r2, =0x03001FB8
	ldr r0, [r2, #8]
	cmp r0, #0
	bne _08033658
	add r0, r6, #0
	b _080336B8
	.align 2, 0
.pool
_08033658:
	add r0, #0x22
	mov r1, #0
	strb r7, [r0]
	ldr r0, [r2, #8]
	strh r1, [r0, #0x10]
	ldr r0, [r2, #8]
	strh r1, [r0, #0x12]
	ldr r2, [r2, #8]
	add r2, #0x25
	ldrb r1, [r2]
	mov r0, #4
	neg r0, r0
	and r0, r1
	mov r1, #1
	orr r0, r1
	strb r0, [r2]
	ldr r0, =0x03003D40
	ldrb r0, [r0]
	cmp r0, #2
	bne _08033690
	ldr r0, =0x03001FD8
	add r0, #0x37
	mov r1, #1
	b _08033696
	.align 2, 0
.pool
_08033690:
	ldr r0, =0x03001FD8
	add r0, #0x37
	mov r1, #2
_08033696:
	strb r1, [r0]
	ldr r0, =off_82309F4
	add r4, r6, #0
	mov r1, #0
	str r1, [sp]
	add r2, r4, #0
	mov r3, #0
	bl allocSprite
	ldr r6, =0x03001FB8
	str r0, [r6, #0x14]
	cmp r0, #0
	bne _080336D0
	ldr r0, [r6, #8]
	bl spriteFreeHeader
	add r0, r4, #0
_080336B8:
	mov r1, #0x2c
	bl freeSpriteTiles
_080336BE:
	mov r0, #0
	b _080337D8
	.align 2, 0
.pool
_080336D0:
	add r0, #0x22
	strb r7, [r0]
	ldr r3, [r6, #0x14]
	ldr r2, =aPppPjpppPjl
	ldr r4, =0x03001FD8
	mov r7, #0x37
	ldr r5, =0x03003D40
	ldrb r1, [r5]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r1, [r7, r4]
	add r0, r0, r1
	lsl r0, r0, #2
	add r0, r0, r2
	ldrh r0, [r0]
	strh r0, [r3, #0x10]
	ldr r3, [r6, #0x14]
	ldrb r1, [r5]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r7, [r7, r4]
	add r0, r0, r7
	lsl r0, r0, #2
	add r2, #2
	add r0, r0, r2
	ldrh r0, [r0]
	strh r0, [r3, #0x12]
	ldr r2, [r6, #0x14]
	add r2, #0x25
	ldrb r1, [r2]
	mov r0, #4
	neg r0, r0
	and r0, r1
	mov r1, #1
	orr r0, r1
	strb r0, [r2]
	bl sub_08032E04
	bl sub_0801B498
	ldr r0, =0x03003D74
	mov r8, r0
	ldrh r0, [r0]
	strh r0, [r4]
	ldr r6, =0x03003D94
	ldrh r0, [r6]
	strh r0, [r4, #2]
	ldr r3, =0x03003D70
	ldrh r0, [r3]
	strh r0, [r4, #4]
	ldr r7, =0x03004750
	ldr r1, [r7]
	ldr r0, [r1]
	add r0, #0x23
	ldrb r0, [r0]
	lsl r0, r0, #0x1b
	lsr r0, r0, #0x1f
	add r4, #0x36
	strb r0, [r4]
	ldr r1, [r1]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	ldr r1, =0x00003FFF
	add r0, r1, #0
	mov r2, r8
	strh r0, [r2]
	mov r0, #0x10
	strh r0, [r6]
	mov r0, #4
	strh r0, [r3]
	ldrb r0, [r5, #1]
	cmp r0, #5
	bne _08033780
	add r0, r7, #0
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	cmp r0, #0
	blt _08033780
	ldr r2, =0x03003CD4
	ldrh r1, [r2]
	ldr r0, =0x0000FEFF
	and r0, r1
	strh r0, [r2]
_08033780:
	ldr r4, =0x03001FB8
	ldr r1, [r4, #8]
	add r1, #0x23
	ldrb r2, [r1]
	mov r3, #0x11
	neg r3, r3
	add r0, r3, #0
	and r0, r2
	strb r0, [r1]
	ldr r0, =0x03003D40
	ldrb r0, [r0]
	cmp r0, #2
	beq _080337A6
	ldr r0, [r4, #0xc]
	add r0, #0x23
	ldrb r2, [r0]
	add r1, r3, #0
	and r1, r2
	strb r1, [r0]
_080337A6:
	bl sub_08033CE8
	mov r0, #2
	bl sub_8016A4C
	ldr r0, =(cb_pauseMenu+1)
	bl setCurrentTaskFunc
	mov r4, #0
_080337B8:
	add r0, r4, #0
	bl sub_08010710
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #1
	bne _080337CC
	add r0, r4, #0
	bl sub_08010658
_080337CC:
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #4
	bls _080337B8
	mov r0, #1
_080337D8:
	add sp, #4
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
.pool

thumb_func_global cb_pauseMenu
cb_pauseMenu: @ 0x08033814
	push {r4, r5, r6, lr}
	ldr r0, =0x03004750
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x19
	ldr r4, =0x03002080
	cmp r0, #0
	bge _08033856
	ldr r0, [r4]
	mov r1, #1
	and r0, r1
	cmp r0, #0
	beq _0803384C
	ldr r0, =0x03003CD4
	ldrh r1, [r0]
	mov r3, #0x80
	lsl r3, r3, #1
	add r2, r3, #0
	orr r1, r2
	strh r1, [r0]
	b _08033856
	.align 2, 0
.pool
_0803384C:
	ldr r2, =0x03003CD4
	ldrh r1, [r2]
	ldr r0, =0x0000FEFF
	and r0, r1
	strh r0, [r2]
_08033856:
	ldr r0, =0x03003D40
	ldrb r1, [r0, #1]
	add r3, r0, #0
	cmp r1, #3
	bne _08033892
	ldr r0, [r4]
	mov r1, #1
	and r0, r1
	cmp r0, #0
	bne _08033888
	ldr r0, =0x03003CD4
	ldrh r1, [r0]
	mov r5, #0x80
	lsl r5, r5, #2
	add r2, r5, #0
	orr r1, r2
	strh r1, [r0]
	b _08033892
	.align 2, 0
.pool
_08033888:
	ldr r2, =0x03003CD4
	ldrh r1, [r2]
	ldr r0, =0x0000FDFF
	and r0, r1
	strh r0, [r2]
_08033892:
	ldrb r2, [r3, #1]
	cmp r2, #1
	bne _080338A4
	ldrb r1, [r3, #2]
	ldr r0, =stageCount
	ldrh r0, [r0, #2]
	sub r0, #1
	cmp r1, r0
	beq _080338B4
_080338A4:
	cmp r2, #4
	bne _08033910
	ldrb r1, [r3, #2]
	ldr r0, =stageCount
	ldrh r0, [r0, #8]
	sub r0, #1
	cmp r1, r0
	bne _08033910
_080338B4:
	ldr r0, [r4]
	mov r1, #1
	and r0, r1
	cmp r0, #0
	bne _080338DC
	ldr r0, =0x03003CD4
	ldrh r1, [r0]
	mov r4, #0x80
	lsl r4, r4, #3
	add r2, r4, #0
	orr r1, r2
	strh r1, [r0]
	b _080338E6
	.align 2, 0
.pool
_080338DC:
	ldr r2, =0x03003CD4
	ldrh r1, [r2]
	ldr r0, =0x0000FBFF
	and r0, r1
	strh r0, [r2]
_080338E6:
	ldrb r0, [r3, #1]
	cmp r0, #4
	bne _08033910
	ldr r0, =0x03002080
	ldr r0, [r0]
	mov r1, #1
	and r0, r1
	cmp r0, #0
	bne _0803390C
	bl nullsub_9
	b _08033910
	.align 2, 0
.pool
_0803390C:
	bl nullsub_10
_08033910:
	ldr r0, =0x03002080
	ldrh r1, [r0, #0xc]
	mov r0, #8
	and r0, r1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #0
	bne _08033944
	mov r2, #1
	and r2, r1
	cmp r2, #0
	bne _0803392A
	b _08033B00
_0803392A:
	ldr r0, =0x03001FD8
	add r6, r0, #0
	add r6, #0x37
	ldr r1, =unk_8232ABC
	ldr r5, =0x03003D40
	ldrb r0, [r5]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrb r1, [r6]
	ldrh r0, [r0]
	cmp r1, r0
	beq _08033944
	b _08033A44
_08033944:
	ldr r0, =0x03001FB8
	ldr r1, [r0, #8]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	bl nullsub_11
	bl sub_0801B468
	ldr r2, =0x03003D74
	ldr r1, =0x03001FD8
	ldrh r0, [r1]
	strh r0, [r2]
	ldr r2, =0x03003D70
	ldrh r0, [r1, #4]
	strh r0, [r2]
	ldr r2, =0x03003D94
	ldrh r0, [r1, #2]
	strh r0, [r2]
	ldr r0, =0x03004750
	ldr r0, [r0]
	ldr r3, [r0]
	add r1, #0x36
	ldrb r0, [r1]
	add r3, #0x23
	mov r1, #1
	and r1, r0
	lsl r1, r1, #4
	ldrb r2, [r3]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	orr r0, r1
	strb r0, [r3]
	ldr r3, =0x03003CD4
	ldrh r1, [r3]
	mov r5, #0x80
	lsl r5, r5, #1
	add r0, r5, #0
	orr r1, r0
	strh r1, [r3]
	ldr r4, =0x03003D40
	ldrb r2, [r4, #1]
	cmp r2, #3
	bne _080339AC
	mov r5, #0x80
	lsl r5, r5, #2
	add r0, r5, #0
	orr r1, r0
	strh r1, [r3]
_080339AC:
	cmp r2, #1
	bne _080339C8
	ldrb r1, [r4, #2]
	ldr r0, =stageCount
	ldrh r0, [r0, #2]
	sub r0, #1
	cmp r1, r0
	bne _080339C8
	ldrh r0, [r3]
	mov r2, #0x80
	lsl r2, r2, #3
	add r1, r2, #0
	orr r0, r1
	strh r0, [r3]
_080339C8:
	ldr r4, =0x03001FB8
	ldr r0, [r4, #8]
	add r0, #0x22
	ldrb r0, [r0]
	mov r1, #1
	bl clearSpritePalettes
	ldr r0, [r4, #8]
	ldrh r0, [r0, #0xc]
	mov r1, #0x2c
	bl freeSpriteTiles
	ldr r0, [r4, #8]
	bl spriteFreeHeader
	ldr r0, [r4, #0x14]
	bl spriteFreeHeader
	mov r4, #0
_080339EE:
	add r0, r4, #0
	bl sub_08010710
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #3
	bne _08033A02
	add r0, r4, #0
	bl sub_08010688
_08033A02:
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #4
	bls _080339EE
	ldr r0, =(task_inGame+1)
	bl setCurrentTaskFunc
	b _08033BD0
	.align 2, 0
.pool
_08033A44:
	bl sub_801CAC4
	bl sub_801CB9C
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldrb r0, [r5]
	cmp r0, #2
	beq _08033AE4
	ldrb r0, [r6]
	cmp r0, #0
	bne _08033A70
	bl sub_8010950
	ldr r0, =(sub_802A45C+1)
	bl setCurrentTaskFunc
	b _08033BD0
	.align 2, 0
.pool
_08033A70:
	bl setPlayerStats_andtuff_8031B14
	strb r4, [r5, #0x10]
	strb r4, [r5, #4]
	mov r0, #0
	strh r4, [r5, #6]
	strb r0, [r5, #3]
	ldrb r0, [r5]
	cmp r0, #0
	bne _08033A94
	str r4, [r5, #0x1c]
	mov r0, #0x50
	bl sub_0803242C
	mov r0, #0
	bl sub_08032444
	b _08033AF2
_08033A94:
	ldr r2, =0x03003C00
	add r0, r2, #0
	add r0, #0xcc
	ldrh r1, [r0]
	mov r0, #0x64
	mul r0, r1, r0
	str r0, [r5, #0x1c]
	ldrb r0, [r5, #2]
	cmp r0, #0
	beq _08033AB8
	ldr r0, [r2]
	lsl r0, r0, #0xd
	lsr r0, r0, #0x19
	bl sub_0803242C
	b _08033ABE
	.align 2, 0
.pool
_08033AB8:
	mov r0, #0x50
	bl sub_0803242C
_08033ABE:
	ldr r0, =0x03003D40
	ldrb r0, [r0, #2]
	cmp r0, #0
	beq _08033ADC
	ldr r0, =0x03003C00
	ldrh r0, [r0, #2]
	lsl r0, r0, #0x16
	lsr r0, r0, #0x19
	bl sub_08032444
	b _08033AF2
	.align 2, 0
.pool
_08033ADC:
	mov r0, #0
	bl sub_08032444
	b _08033AF2
_08033AE4:
	bl setPlayerStats_andtuff_8031B14
	strb r4, [r5, #0x10]
	strb r4, [r5, #4]
	mov r0, #0
	strh r4, [r5, #6]
	strb r0, [r5, #3]
_08033AF2:
	ldr r0, =(cb_startLevelLoad_2+1)
	bl setCurrentTaskFunc
	b _08033BD0
	.align 2, 0
.pool
_08033B00:
	ldr r0, =0x03003D40
	ldrb r0, [r0]
	cmp r0, #2
	bne _08033B54
	mov r0, #0x30
	and r0, r1
	cmp r0, #0
	beq _08033BD0
	mov r0, #0x20
	and r0, r1
	cmp r0, #0
	beq _08033B2C
	ldr r0, =0x03001FD8
	add r1, r0, #0
	add r1, #0x37
	ldrb r0, [r1]
	cmp r0, #0
	beq _08033B2C
	strb r2, [r1]
	mov r0, #0xcb
	bl playSong
_08033B2C:
	ldr r0, =0x03002080
	ldrh r1, [r0, #0xc]
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	beq _08033B9C
	ldr r0, =0x03001FD8
	add r1, r0, #0
	add r1, #0x37
	ldrb r0, [r1]
	cmp r0, #0
	bne _08033B9C
	mov r0, #1
	b _08033B94
	.align 2, 0
.pool
_08033B54:
	mov r0, #0xc0
	and r0, r1
	cmp r0, #0
	beq _08033BD0
	mov r0, #0x40
	and r0, r1
	cmp r0, #0
	beq _08033B7A
	ldr r0, =0x03001FD8
	add r1, r0, #0
	add r1, #0x37
	ldrb r0, [r1]
	cmp r0, #0
	beq _08033B7A
	sub r0, #1
	strb r0, [r1]
	mov r0, #0xcb
	bl playSong
_08033B7A:
	ldr r0, =0x03002080
	ldrh r1, [r0, #0xc]
	mov r0, #0x80
	and r0, r1
	cmp r0, #0
	beq _08033B9C
	ldr r0, =0x03001FD8
	add r1, r0, #0
	add r1, #0x37
	ldrb r0, [r1]
	cmp r0, #1
	bhi _08033B9C
	add r0, #1
_08033B94:
	strb r0, [r1]
	mov r0, #0xcb
	bl playSong
_08033B9C:
	ldr r5, =0x03001FB8
	ldr r6, [r5, #0x14]
	ldr r3, =aPppPjpppPjl
	ldr r2, =0x03001FD8
	add r2, #0x37
	ldr r4, =0x03003D40
	ldrb r1, [r4]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r1, [r2]
	add r0, r0, r1
	lsl r0, r0, #2
	add r0, r0, r3
	ldrh r0, [r0]
	strh r0, [r6, #0x10]
	ldr r5, [r5, #0x14]
	ldrb r1, [r4]
	lsl r0, r1, #1
	add r0, r0, r1
	ldrb r2, [r2]
	add r0, r0, r2
	lsl r0, r0, #2
	add r3, #2
	add r0, r0, r3
	ldrh r0, [r0]
	strh r0, [r5, #0x12]
_08033BD0:
	ldr r0, =0x03001FB8
	ldr r0, [r0, #0x14]
	bl updateSpriteAnimation
	bl blinkPausePal
	mov r0, #2
	bl sub_8016A4C
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global unused_sub_8033BFC
unused_sub_8033BFC: @ 0x08033BFC
	push {r4, lr}
	ldr r0, =0x03003D40
	ldrb r1, [r0]
	add r3, r0, #0
	cmp r1, #2
	beq _08033CD8
	ldrb r0, [r3, #1]
	add r1, r0, #0
	cmp r1, #2
	bhi _08033C48
	ldrb r0, [r3, #8]
	cmp r0, #0
	beq _08033C38
	mov r1, #0
	add r2, r0, #0
	mov r4, #1
_08033C1C:
	add r0, r2, #0
	asr r0, r1
	and r0, r4
	cmp r0, #0
	bne _08033C52
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	cmp r1, #2
	bls _08033C1C
	b _08033CBA
	.align 2, 0
.pool
_08033C38:
	strb r0, [r3, #9]
	str r0, [r3, #0x1c]
	ldr r0, =(cb_loadLevelSelect+1)
	bl setCurrentTaskFunc
	b _08033CBA
	.align 2, 0
.pool
_08033C48:
	cmp r1, #5
	bne _08033C8C
	mov r0, #0
	strb r0, [r3, #2]
	b _08033CA0
_08033C52:
	mov r2, #0
	strb r1, [r3, #1]
	ldr r1, =stageCount
	ldrb r0, [r3, #1]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrb r0, [r0]
	sub r0, #1
	strb r0, [r3, #2]
	strb r4, [r3, #0x10]
	ldr r1, =0x03003C00
	ldrb r0, [r1]
	lsl r0, r0, #0x1a
	lsr r0, r0, #0x1a
	strb r0, [r3, #9]
	str r2, [r3, #0x1c]
	add r1, #0xcc
	strh r2, [r1]
	ldr r0, =(cb_saving+1)
	bl setCurrentTaskFunc
	b _08033CBA
	.align 2, 0
.pool
_08033C8C:
	sub r0, #1
	strb r0, [r3, #1]
	ldr r1, =stageCount
	ldrb r0, [r3, #1]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrb r0, [r0]
	sub r0, #1
	strb r0, [r3, #2]
	mov r0, #1
_08033CA0:
	strb r0, [r3, #0x10]
	ldr r1, =0x03003C00
	ldrb r0, [r1]
	lsl r0, r0, #0x1a
	lsr r0, r0, #0x1a
	mov r2, #0
	strb r0, [r3, #9]
	str r2, [r3, #0x1c]
	add r1, #0xcc
	strh r2, [r1]
	ldr r0, =(cb_saving+1)
	bl setCurrentTaskFunc
_08033CBA:
	bl setPlayerStats_andtuff_8031B14
	mov r0, #2
	bl endPriorityOrHigherTask
	bl FreeAllPalFadeTasks
	b _08033CE2
	.align 2, 0
.pool
_08033CD8:
	bl blinkPausePal
	mov r0, #2
	bl sub_8016A4C
_08033CE2:
	pop {r4}
	pop {r0}
	bx r0

thumb_func_global sub_08033CE8
sub_08033CE8: @ 0x08033CE8
	push {r4, r5, r6, lr}
	mov r1, #0
	ldr r3, =dword_8230B10
	ldr r0, [r3]
	cmp r0, #0
	beq _08033D18
	ldr r2, =0x03001FDE
	mov r6, #0
	add r5, r2, #0
	add r5, #0x20
	mov r4, #0
_08033CFE:
	lsl r0, r1, #1
	add r0, r0, r2
	strh r4, [r0]
	add r0, r1, r5
	strb r6, [r0]
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	lsl r0, r1, #3
	add r0, r0, r3
	ldr r0, [r0]
	cmp r0, #0
	bne _08033CFE
_08033D18:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global blinkPausePal
blinkPausePal: @ 0x08033D28
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #4
	mov r6, #0
	ldr r2, =dword_8230B10
	ldr r4, [r2]
	cmp r4, #0
	beq _08033DBE
	ldr r7, =0x03001FD8
	add r0, r7, #6
	mov sl, r0
	mov r0, #0
	mov sb, r0
	mov r0, #0
	mov r8, r0
_08033D4C:
	add r0, r7, #0
	add r0, #0x26
	add r5, r6, r0
	ldrb r0, [r5]
	lsl r0, r0, #2
	add r4, r4, r0
	ldr r0, =0x03001FB8
	ldr r0, [r0, #8]
	add r0, #0x22
	mov r1, #0
	ldrsb r1, [r0, r1]
	lsl r1, r1, #5
	ldr r0, [r2, #4]
	lsl r0, r0, #1
	ldr r2, =0x05000200
	add r0, r0, r2
	add r1, r1, r0
	mov r2, sp
	ldrh r0, [r4, #2]
	strh r0, [r2]
	mov r0, sp
	ldr r2, =0x01000001
	bl Bios_memcpy
	lsl r0, r6, #1
	mov r2, sl
	add r1, r0, r2
	ldrh r0, [r1]
	add r0, #1
	strh r0, [r1]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldrh r2, [r4]
	cmp r0, r2
	bls _08033DAC
	mov r0, r8
	strh r0, [r1]
	ldrb r0, [r5]
	add r0, #1
	strb r0, [r5]
	ldrh r1, [r4, #4]
	cmp r1, #0
	beq _08033DA8
	ldr r0, =0x0000FFFE
	cmp r1, r0
	bne _08033DAC
_08033DA8:
	mov r2, sb
	strb r2, [r5]
_08033DAC:
	add r0, r6, #1
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	lsl r1, r6, #3
	ldr r0, =dword_8230B10
	add r2, r1, r0
	ldr r4, [r2]
	cmp r4, #0
	bne _08033D4C
_08033DBE:
	add sp, #4
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool




	thumb_func_global sub_8033DE8
	sub_8033DE8:
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #4
	add r7, r0, #0
	add r5, r1, #0
	ldrb r0, [r7]
	cmp r0, #4
	bls _08033E00
	b def_8033E08
_08033E00:
	lsl r0, r0, #2
	ldr r1, =_08033E10
	add r0, r0, r1
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
.pool
_08033E10: @ jump table
	.4byte _08033E24 @ case 0
	.4byte _0803401C @ case 1
	.4byte _08034058 @ case 2
	.4byte _0803406E @ case 3
	.4byte _080340E8 @ case 4
_08033E24:
	ldr r0, =0x03003D40
	mov sb, r0
	ldrb r0, [r0]
	cmp r0, #2
	beq _08033E30
	b _08033F7C
_08033E30:
	mov r0, #0x44
	bl allocateSpriteTiles
	lsl r5, r0, #0x10
	cmp r5, #0
	bge _08033E3E
	b _08033F98
_08033E3E:
	ldr r0, =stageTextPal
	mov r1, #1
	bl LoadSpritePalette
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	cmp r0, #0
	bge _08033E50
	b _08033F98
_08033E50:
	mov r1, sb
	ldrb r0, [r1, #2]
	lsl r3, r0, #3
	ldrb r2, [r1, #1]
	lsl r1, r2, #5
	add r3, r3, r1
	ldr r1, =0x03003C10
	add r3, r3, r1
	ldr r1, =dword_82065EC
	mov sl, r1
	lsl r0, r0, #2
	lsl r2, r2, #4
	add r0, r0, r2
	add r0, sl
	ldr r1, [r3]
	ldr r0, [r0]
	cmp r1, r0
	bhs _08033EE0
	ldr r1, =bestTime
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =timeTileset
	lsr r2, r5, #0x10
	mov r3, #0
	mov r8, r3
	str r3, [sp]
	bl allocSprite
	ldr r5, =0x03002030
	str r0, [r5]
	add r0, #0x22
	strb r6, [r0]
	ldr r1, [r5]
	mov r0, #0xf0
	strh r0, [r1, #0x10]
	mov r6, #0x54
	strh r6, [r1, #0x12]
	ldr r4, =0x02009250
	add r0, r4, #0
	bl sub_080331E0
	mov r1, sb
	ldrb r0, [r1, #2]
	lsl r0, r0, #3
	ldrb r1, [r1, #1]
	lsl r1, r1, #5
	add r0, r0, r1
	ldr r2, =0x03003C10
	add r0, r0, r2
	b _08033F24
	.align 2, 0
.pool
_08033EE0:
	ldr r1, =timeForQualify
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =timeTileset
	lsr r2, r5, #0x10
	mov r3, #0
	mov r8, r3
	str r3, [sp]
	bl allocSprite
	ldr r5, =0x03002030
	str r0, [r5]
	add r0, #0x22
	strb r6, [r0]
	ldr r1, [r5]
	mov r0, #0xf0
	strh r0, [r1, #0x10]
	mov r6, #0x54
	strh r6, [r1, #0x12]
	ldr r4, =0x02009250
	add r0, r4, #0
	bl sub_080331E0
	mov r1, sb
	ldrb r0, [r1, #2]
	lsl r0, r0, #2
	ldrb r1, [r1, #1]
	lsl r1, r1, #4
	add r0, r0, r1
	add r0, sl
_08033F24:
	ldr r0, [r0]
	add r1, r4, #0
	bl sub_08033238
	ldr r0, =unk_82399F4
	ldr r4, =0x03001F08
	ldr r1, [r4, #0x7c]
	ldrh r2, [r1, #0xc]
	mov r3, r8
	str r3, [sp]
	mov r1, #0
	mov r3, #0
	bl allocSprite
	ldr r1, =0x03002034
	str r0, [r1]
	ldr r1, [r5]
	ldrh r1, [r1, #0x10]
	sub r1, #0x20
	strh r1, [r0, #0x10]
	strh r6, [r0, #0x12]
	ldr r1, [r4, #0x7c]
	add r1, #0x22
	ldrb r1, [r1]
	add r0, #0x22
	strb r1, [r0]
	b _08034002
	.align 2, 0
.pool
_08033F7C:
	mov r0, #0x44
	bl allocateSpriteTiles
	lsl r4, r0, #0x10
	cmp r4, #0
	blt _08033F98
	ldr r0, =stageTextPal
	mov r1, #1
	bl LoadSpritePalette
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	cmp r0, #0
	bge _08033FA4
_08033F98:
	mov r0, #4
	strb r0, [r7]
	b def_8033E08
	.align 2, 0
.pool
_08033FA4:
	mov r1, sb
	ldrb r0, [r1, #1]
	cmp r0, #4
	bhi _08033FCC
	ldr r2, =stageText
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r1, [r0]
	lsl r0, r1, #2
	add r0, r0, r1
	mov r3, sb
	ldrb r3, [r3, #2]
	add r0, r0, r3
	lsl r0, r0, #2
	b _08033FDC
	.align 2, 0
.pool
_08033FCC:
	ldr r2, =stageText
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r1, [r0]
	lsl r0, r1, #2
	add r0, r0, r1
	lsl r0, r0, #2
	add r2, #0x10
_08033FDC:
	add r0, r0, r2
	ldr r0, [r0]
	ldr r1, =stageTextTiles
	lsr r2, r4, #0x10
	mov r3, #0
	str r3, [sp]
	bl allocSprite
	ldr r1, =0x03002030
	str r0, [r1]
	ldr r1, =0x03002030
	ldr r0, [r1]
	add r0, #0x22
	strb r6, [r0]
	ldr r1, [r1]
	mov r0, #0xf0
	strh r0, [r1, #0x10]
	mov r0, #0x50
	strh r0, [r1, #0x12]
_08034002:
	mov r0, #0xde
	bl playSong
	b _080340B0
	.align 2, 0
.pool
_0803401C:
	ldr r0, =0x03002030
	ldr r1, [r0]
	mov r2, #0x10
	ldrsh r0, [r1, r2]
	sub r0, #8
	cmp r0, #0x77
	bgt _08034050
	mov r0, #0x78
	strh r0, [r1, #0x10]
	ldr r0, =0x03003D40
	ldrb r0, [r0]
	cmp r0, #2
	bne _0803403E
	ldr r0, =0x03002034
	ldr r1, [r0]
	mov r0, #0x58
	strh r0, [r1, #0x10]
_0803403E:
	mov r0, #0
	b _080340AE
	.align 2, 0
.pool
_08034050:
	ldrh r0, [r1, #0x10]
	sub r0, #8
	strh r0, [r1, #0x10]
	b _080340CA
_08034058:
	ldrh r0, [r5]
	add r0, #1
	strh r0, [r5]
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #0x3c
	ble def_8033E08
	mov r0, #0xe7
	bl playSong
	b _080340AC
_0803406E:
	ldr r4, =0x03002030
	ldr r2, [r4]
	mov r3, #0x10
	ldrsh r0, [r2, r3]
	sub r0, #8
	mov r1, #0x78
	neg r1, r1
	cmp r0, r1
	bge _080340C4
	add r0, r2, #0
	add r0, #0x22
	ldrb r0, [r0]
	mov r1, #1
	bl clearSpritePalettes
	ldr r0, [r4]
	ldrh r0, [r0, #0xc]
	mov r1, #0x44
	bl freeSpriteTiles
	ldr r0, [r4]
	bl spriteFreeHeader
	ldr r0, =0x03003D40
	ldrb r0, [r0]
	cmp r0, #2
	bne _080340AC
	ldr r0, =0x03002034
	ldr r0, [r0]
	bl spriteFreeHeader
_080340AC:
	mov r0, #0
_080340AE:
	strh r0, [r5]
_080340B0:
	ldrb r0, [r7]
	add r0, #1
	strb r0, [r7]
	b def_8033E08
	.align 2, 0
.pool
_080340C4:
	ldrh r0, [r2, #0x10]
	sub r0, #8
	strh r0, [r2, #0x10]
_080340CA:
	ldr r0, =0x03003D40
	ldrb r0, [r0]
	cmp r0, #2
	bne def_8033E08
	ldr r0, =0x03002034
	ldr r1, [r0]
	ldrh r0, [r1, #0x10]
	sub r0, #8
	strh r0, [r1, #0x10]
	b def_8033E08
	.align 2, 0
.pool
_080340E8:
	mov r0, #1
	b _080340EE
def_8033E08:
	mov r0, #0
_080340EE:
	add sp, #4
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	
	
	thumb_func_global sub_8034100
sub_8034100:	
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #4
	mov sb, r0
	ldrb r4, [r0]
	cmp r4, #2
	bne _08034116
	b _08034268
_08034116:
	cmp r4, #2
	bgt _08034126
	cmp r4, #0
	beq _0803412E
	cmp r4, #1
	bne _08034124
	b _0803422C
_08034124:
	b _08034660
_08034126:
	cmp r4, #3
	bne _0803412C
	b _08034640
_0803412C:
	b _08034660
_0803412E:
	ldr r5, =0x03003D40
	ldrb r2, [r5, #2]
	ldr r1, =stageCount
	ldrb r0, [r5, #1]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	sub r0, #1
	cmp r2, r0
	bne _08034154
	ldrb r0, [r5]
	cmp r0, #2
	beq _08034174
	mov r0, #2
	b _0803425A
	.align 2, 0
.pool
_08034154:
	mov r0, #0x5b
	bl allocateSpriteTiles
	lsl r6, r0, #0x10
	cmp r6, #0
	bge _08034162
	b _08034654
_08034162:
	ldr r0, =stageTextPal
	mov r1, #1
	bl LoadSpritePalette
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	mov r8, r1
	cmp r0, #0
	bge _08034180
_08034174:
	mov r0, #0xff
	mov r2, sb
	strb r0, [r2]
	b _08034664
	.align 2, 0
.pool
_08034180:
	ldr r3, =hostageCounts
	ldrb r1, [r5, #2]
	lsl r1, r1, #1
	ldrb r0, [r5, #1]
	lsl r0, r0, #3
	add r1, r1, r0
	ldrb r2, [r5, #0x11]
	lsl r0, r2, #1
	add r0, r0, r2
	lsl r0, r0, #4
	add r1, r1, r0
	add r1, r1, r3
	ldrh r0, [r1]
	cmp r0, #0
	beq _080341A8
	ldr r0, =0x03001F08
	add r0, #0x86
	ldrb r3, [r0]
	cmp r3, #0
	beq _080341D4
_080341A8:
	ldr r1, =stageClear
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =unk_8235F3C
	lsr r2, r6, #0x10
	str r4, [sp]
	b _080341E8
	.align 2, 0
.pool
_080341D4:
	ldr r1, =goodJob
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =unk_8235F3C
	lsr r2, r6, #0x10
	str r3, [sp]
_080341E8:
	mov r3, #0
	bl allocSprite
	ldr r1, =0x03002030
	str r0, [r1]
	ldr r1, =0x03002030
	ldr r0, [r1]
	add r0, #0x22
	mov r4, #0
	mov r3, r8
	strb r3, [r0]
	ldr r1, [r1]
	mov r0, #0xf0
	strh r0, [r1, #0x10]
	mov r0, #0x50
	strh r0, [r1, #0x12]
	mov r0, #0xde
	bl playSong
	ldr r0, =0x03002080
	strh r4, [r0, #0x3a]
	mov r1, sb
	ldrb r0, [r1]
	add r0, #1
	strb r0, [r1]
	b _08034664
	.align 2, 0
.pool
_0803422C:
	ldr r0, =0x03002030
	ldr r1, [r0]
	mov r2, #0x10
	ldrsh r0, [r1, r2]
	sub r0, #8
	cmp r0, #0x77
	bgt _08034260
	mov r0, #0x78
	strh r0, [r1, #0x10]
	ldr r0, =0x03003D40
	ldrb r0, [r0]
	cmp r0, #2
	beq _08034258
	mov r3, sb
	ldrb r0, [r3]
	add r0, #1
	strb r0, [r3]
	b _08034664
	.align 2, 0
.pool
_08034258:
	mov r0, #0xff
_0803425A:
	mov r1, sb
	strb r0, [r1]
	b _08034664
_08034260:
	ldrh r0, [r1, #0x10]
	sub r0, #8
	strh r0, [r1, #0x10]
	b _08034664
_08034268:
	ldr r0, =0x03003D40
	ldrb r2, [r0, #2]
	ldr r1, =stageCount
	ldrb r0, [r0, #1]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r0, [r0]
	sub r0, #1
	cmp r2, r0
	bne _08034294
	ldr r0, =stageTextPal
	mov r1, #1
	bl LoadSpritePalette
	b _080342A0
	.align 2, 0
.pool
_08034294:
	ldr r0, =0x03002030
	ldr r0, [r0]
	add r0, #0x22
	ldrb r0, [r0]
	lsl r0, r0, #0x18
	asr r0, r0, #0x18
_080342A0:
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r8, r0
	mov r0, #0x34
	bl allocateSpriteTiles
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	ldr r0, =0x03003D40
	ldrb r0, [r0, #1]
	cmp r0, #4
	bhi _080342C8
	ldr r1, =rescueBonus
	b _080342CA
	.align 2, 0
.pool
_080342C8:
	ldr r1, =specialBonus
_080342CA:
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =timeTileset
	add r2, r6, #0
	mov r3, #0
	str r3, [sp]
	bl allocSprite
	ldr r1, =0x03002038
	str r0, [r1]
	ldr r0, =0x03002038
	ldr r3, [r0]
	ldr r2, =unk_8239AA8
	ldr r1, =0x03002080
	add r1, #0x2c
	ldrb r0, [r1]
	lsl r0, r0, #2
	add r0, r0, r2
	ldrh r0, [r0]
	mov r7, #0
	strh r0, [r3, #0x10]
	ldrb r0, [r1]
	lsl r0, r0, #2
	add r2, #2
	add r0, r0, r2
	ldrh r0, [r0]
	strh r0, [r3, #0x12]
	add r3, #0x22
	mov r2, r8
	strb r2, [r3]
	mov r0, #0x34
	bl allocateSpriteTiles
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	ldr r4, =0x03003D40
	ldrb r2, [r4, #2]
	ldr r1, =stageCount
	ldrb r5, [r4, #1]
	lsl r0, r5, #1
	add r0, r0, r1
	ldrh r0, [r0]
	sub r0, #1
	cmp r2, r0
	bge _08034360
	ldrb r0, [r4, #0x11]
	cmp r0, #2
	bne _08034358
	ldr r0, =unk_8239628
	b _08034372
	.align 2, 0
.pool
_08034358:
	ldr r0, =unk_8239610
	b _08034372
	.align 2, 0
.pool
_08034360:
	ldr r3, =bonus_numerals
	lsl r1, r5, #2
	ldrb r2, [r4, #0x11]
	lsl r0, r2, #1
	add r0, r0, r2
	lsl r0, r0, #3
	add r1, r1, r0
	add r1, r1, r3
	ldr r0, [r1]
_08034372:
	ldr r1, =timeTileset
	add r2, r6, #0
	str r7, [sp]
	mov r3, #0
	bl allocSprite
	ldr r1, =0x03002040
	str r0, [r1]
	ldr r0, =0x03002040
	ldr r2, [r0]
	ldr r1, =unk_8239AC0
	ldr r0, =0x03002080
	add r4, r0, #0
	add r4, #0x2c
	ldrb r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldrh r0, [r0]
	mov r3, #0
	mov sl, r3
	strh r0, [r2, #0x10]
	ldrb r0, [r4]
	lsl r0, r0, #2
	add r1, #2
	add r0, r0, r1
	ldrh r0, [r0]
	strh r0, [r2, #0x12]
	add r2, #0x22
	mov r0, r8
	strb r0, [r2]
	mov r0, #0x34
	bl allocateSpriteTiles
	add r2, r0, #0
	ldr r6, =pts
	ldrb r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r6
	ldr r0, [r0]
	ldr r7, =timeTileset
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	mov r1, sl
	str r1, [sp]
	add r1, r7, #0
	mov r3, #0
	bl allocSprite
	ldr r1, =0x03002048
	str r0, [r1]
	ldr r2, =unk_8239AD8
	ldrb r1, [r4]
	lsl r1, r1, #2
	add r1, r1, r2
	ldrh r1, [r1]
	strh r1, [r0, #0x10]
	ldrb r1, [r4]
	lsl r1, r1, #2
	add r2, #2
	add r1, r1, r2
	ldrh r1, [r1]
	strh r1, [r0, #0x12]
	add r0, #0x22
	mov r2, r8
	strb r2, [r0]
	ldr r5, =0x03003D40
	ldrb r0, [r5, #1]
	cmp r0, #4
	bls _080343FE
	b _08034534
_080343FE:
	ldr r3, =hostageCounts
	ldrb r1, [r5, #2]
	lsl r1, r1, #1
	lsl r0, r0, #3
	add r1, r1, r0
	ldrb r2, [r5, #0x11]
	lsl r0, r2, #1
	add r0, r0, r2
	lsl r0, r0, #4
	add r1, r1, r0
	add r1, r1, r3
	ldrh r0, [r1]
	cmp r0, #0
	bne _0803441C
	b _0803460C
_0803441C:
	ldr r0, =0x03001F08
	add r0, #0x86
	ldrb r0, [r0]
	cmp r0, #0
	beq _08034428
	b _0803460C
_08034428:
	mov r0, #0x34
	bl allocateSpriteTiles
	add r2, r0, #0
	ldr r1, =clearBonus
	ldrb r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	mov r3, sl
	str r3, [sp]
	add r1, r7, #0
	mov r3, #0
	bl allocSprite
	ldr r1, =0x0300203C
	str r0, [r1]
	ldr r2, =unk_8239AB4
	ldrb r1, [r4]
	lsl r1, r1, #2
	add r1, r1, r2
	ldrh r1, [r1]
	strh r1, [r0, #0x10]
	ldrb r1, [r4]
	lsl r1, r1, #2
	add r2, #2
	add r1, r1, r2
	ldrh r1, [r1]
	strh r1, [r0, #0x12]
	add r0, #0x22
	mov r1, r8
	strb r1, [r0]
	mov r0, #0x34
	bl allocateSpriteTiles
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	ldrb r0, [r5, #0x11]
	cmp r0, #2
	bne _080344C4
	ldr r0, =unk_8239628
	add r2, r6, #0
	mov r3, sl
	str r3, [sp]
	b _080344CC
	.align 2, 0
.pool
_080344C4:
	ldr r0, =unk_8239610
	add r2, r6, #0
	mov r1, sl
	str r1, [sp]
_080344CC:
	add r1, r7, #0
	mov r3, #0
	bl allocSprite
	ldr r1, =0x03002044
	str r0, [r1]
	ldr r0, =0x03002044
	ldr r2, [r0]
	ldr r1, =unk_8239ACC
	ldr r4, =0x03002080
	add r4, #0x2c
	ldrb r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldrh r0, [r0]
	mov r5, #0
	strh r0, [r2, #0x10]
	ldrb r0, [r4]
	lsl r0, r0, #2
	add r1, #2
	add r0, r0, r1
	ldrh r0, [r0]
	strh r0, [r2, #0x12]
	add r2, #0x22
	mov r3, r8
	strb r3, [r2]
	mov r0, #0x34
	bl allocateSpriteTiles
	add r2, r0, #0
	ldr r1, =pts
	ldrb r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r1, =timeTileset
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	str r5, [sp]
	b _080345E4
	.align 2, 0
.pool
_08034534:
	bl getLaunchSiteTime
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x3c
	bl Bios_Div
	cmp r0, #0
	beq _0803460C
	mov r0, #0x34
	bl allocateSpriteTiles
	add r2, r0, #0
	ldr r1, =timeBonus
	ldrb r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	mov r3, sl
	str r3, [sp]
	add r1, r7, #0
	mov r3, #0
	bl allocSprite
	ldr r1, =0x0300203C
	str r0, [r1]
	ldr r2, =unk_8239AB4
	ldrb r1, [r4]
	lsl r1, r1, #2
	add r1, r1, r2
	ldrh r1, [r1]
	strh r1, [r0, #0x10]
	ldrb r1, [r4]
	lsl r1, r1, #2
	add r2, #2
	add r1, r1, r2
	ldrh r1, [r1]
	strh r1, [r0, #0x12]
	add r0, #0x22
	mov r1, r8
	strb r1, [r0]
	bl sub_8034678
	mov r0, #0x34
	bl allocateSpriteTiles
	add r2, r0, #0
	ldr r0, =unk_8239AF0
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	mov r3, sl
	str r3, [sp]
	add r1, r7, #0
	mov r3, #0
	bl allocSprite
	ldr r1, =0x03002044
	str r0, [r1]
	ldr r2, =unk_8239ACC
	ldrb r1, [r4]
	lsl r1, r1, #2
	add r1, r1, r2
	ldrh r1, [r1]
	strh r1, [r0, #0x10]
	ldrb r1, [r4]
	lsl r1, r1, #2
	add r2, #2
	add r1, r1, r2
	ldrh r1, [r1]
	strh r1, [r0, #0x12]
	add r0, #0x22
	mov r1, r8
	strb r1, [r0]
	mov r0, #0x34
	bl allocateSpriteTiles
	add r2, r0, #0
	ldrb r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r6
	ldr r0, [r0]
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	mov r3, sl
	str r3, [sp]
	add r1, r7, #0
_080345E4:
	mov r3, #0
	bl allocSprite
	ldr r1, =0x0300204C
	str r0, [r1]
	ldr r2, =unk_8239AE4
	ldrb r1, [r4]
	lsl r1, r1, #2
	add r1, r1, r2
	ldrh r1, [r1]
	strh r1, [r0, #0x10]
	ldrb r1, [r4]
	lsl r1, r1, #2
	add r2, #2
	add r1, r1, r2
	ldrh r1, [r1]
	strh r1, [r0, #0x12]
	add r0, #0x22
	mov r1, r8
	strb r1, [r0]
_0803460C:
	mov r2, sb
	ldrb r0, [r2]
	add r0, #1
	strb r0, [r2]
	ldr r1, =0x03002080
	mov r0, #0
	strh r0, [r1, #0x3a]
	b _08034664
	.align 2, 0
.pool
_08034640:
	ldr r1, =0x03002080
	ldrh r0, [r1, #0x3a]
	add r0, #1
	strh r0, [r1, #0x3a]
	lsl r0, r0, #0x10
	asr r0, r0, #0x10
	cmp r0, #0x3c
	ble _08034664
	mov r0, #0
	strh r0, [r1, #0x3a]
_08034654:
	mov r0, #0xff
	mov r3, sb
	strb r0, [r3]
	b _08034664
	.align 2, 0
.pool
_08034660:
	mov r0, #1
	b _08034666
_08034664:
	mov r0, #0
_08034666:
	add sp, #4
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
	
	
	
	thumb_func_global sub_8034678
sub_8034678: @ 0x08034678
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	sub sp, #0xc
	add r0, sp, #8
	mov r1, #0
	strh r1, [r0]
	ldr r1, =0x020092C0
	ldr r2, =0x01000024
	bl Bios_memcpy
	bl getLaunchSiteTime
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0x3c
	bl Bios_Div
	add r5, r0, #0
	lsl r0, r5, #5
	sub r0, r0, r5
	lsl r0, r0, #2
	add r0, r0, r5
	lsl r5, r0, #3
	ldr r0, =0x0001869F
	cmp r5, r0
	bhi _08034768
	mov r6, #0
	ldr r7, =unk_8239B08
_080346B4:
	lsl r0, r6, #2
	add r0, r0, r7
	ldr r4, [r0]
	add r0, r5, #0
	add r1, r4, #0
	bl Bios_Div
	mov r2, sp
	add r1, r2, r6
	strb r0, [r1]
	add r0, r5, #0
	add r1, r4, #0
	bl Bios_modulo
	add r5, r0, #0
	add r0, r6, #1
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	cmp r6, #4
	bls _080346B4
	mov r1, #4
	mov r0, sp
	ldrb r0, [r0]
	cmp r0, #0
	beq _080346E8
	mov r1, #5
_080346E8:
	ldr r5, =0x020092C0
	strh r1, [r5]
	add r5, #0xc
	mov r3, #0
	mov r6, #0
	lsl r0, r1, #0x10
	mov ip, r0
	cmp r6, r1
	bhs _08034768
	ldr r1, =off_8239B44
	mov sb, r1
	ldr r7, =unk_8239B1C
	mov r2, #0x31
	neg r2, r2
	mov r8, r2
_08034706:
	mov r0, #4
	sub r0, r0, r6
	mov r1, sp
	add r4, r1, r0
	ldrb r0, [r4]
	lsl r0, r0, #2
	add r0, sb
	ldr r0, [r0]
	ldrh r0, [r0, #0xc]
	strh r0, [r5]
	ldrb r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r7
	ldrh r0, [r0]
	lsl r3, r3, #0x10
	asr r3, r3, #0x10
	add r0, r3, r0
	strh r0, [r5, #2]
	ldrb r0, [r4]
	lsl r0, r0, #2
	add r1, r7, #2
	add r0, r0, r1
	mov r2, #0
	ldrsh r1, [r0, r2]
	mov r0, #3
	and r1, r0
	lsl r1, r1, #4
	ldrb r2, [r5, #7]
	mov r0, r8
	and r0, r2
	orr r0, r1
	mov r1, #0x3f
	and r0, r1
	mov r1, #0x40
	orr r0, r1
	strb r0, [r5, #7]
	ldrb r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r7
	ldrh r0, [r0]
	add r3, r3, r0
	lsl r3, r3, #0x10
	lsr r3, r3, #0x10
	add r5, #0xc
	add r0, r6, #1
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	cmp r0, ip
	blo _08034706
_08034768:
	add sp, #0xc
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

	
	
	
	
	
	

thumb_func_global sub_08034790
sub_08034790: @ 0x08034790
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	sub sp, #8
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r8, r1
	add r1, sp, #4
	mov r0, #0
	strh r0, [r1]
	ldr r5, =0x03006C00
	ldr r2, =0x0100001A
	add r0, r1, #0
	add r1, r5, #0
	bl Bios_memcpy
	ldr r2, =doorArrowPositions
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r2, [r0]
	ldrb r0, [r1, #2]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r2, [r0]
	ldrb r0, [r1, #3]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r4, [r0]
	mov r0, #0
	strh r0, [r5]
	mov r6, #0
	ldrh r0, [r4]
	ldr r1, =0x0000FFFF
	cmp r0, r1
	beq _08034820
	mov sb, r1
_080347E2:
	mov r0, #0xff
	str r0, [sp]
	ldr r0, =doorArrowAnimation
	mov r1, #0
	add r2, r7, #0
	mov r3, #2
	bl allocSprite
	lsl r2, r6, #2
	add r1, r5, #4
	add r2, r2, r1
	str r0, [r2]
	add r0, #0x22
	mov r1, r8
	strb r1, [r0]
	ldr r2, [r2]
	add r2, #0x23
	ldrb r0, [r2]
	mov r1, #0x10
	orr r0, r1
	strb r0, [r2]
	ldrh r0, [r5]
	add r0, #1
	strh r0, [r5]
	add r4, #8
	add r0, r6, #1
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	ldrh r0, [r4]
	cmp r0, sb
	bne _080347E2
_08034820:
	ldr r0, =off_8232808
	mov r4, #0
	str r4, [sp]
	mov r1, #0
	add r2, r7, #0
	mov r3, #1
	bl allocSprite
	ldr r3, =0x03006C00
	str r0, [r3, #0x24]
	add r0, #0x22
	mov r1, r8
	strb r1, [r0]
	ldr r1, [r3, #0x24]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	strh r4, [r3, #0x30]
	strh r4, [r3, #0x2c]
	strh r4, [r3, #0x2e]
	strh r4, [r3, #0x32]
	add sp, #8
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global animateDoorArrows
animateDoorArrows: @ 0x08034878
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #8
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	str r0, [sp]
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov sl, r1
	ldr r2, =doorArrowPositions
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r2, [r0]
	ldrb r0, [r1, #2]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r2, [r0]
	ldrb r0, [r1, #3]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r5, [r0]
	mov r0, #0
	mov r8, r0
	ldr r1, =0x03006C00
	mov ip, r1
	ldrh r2, [r1]
	cmp r8, r2
	bhs _0803498E
	mov r6, #0x10
	mov sb, r6
_080348BE:
	ldrh r0, [r5]
	ldr r2, [sp]
	sub r3, r0, r2
	ldrh r0, [r5, #2]
	mov r6, sl
	sub r0, r6, r0
	add r7, r0, #0
	sub r7, #8
	mov r2, r8
	lsl r0, r2, #2
	add r6, r1, #4
	add r0, r0, r6
	ldr r4, [r0]
	cmp r4, #0
	beq _0803497A
	add r1, r3, #0
	add r1, #0x10
	mov r0, #0x80
	lsl r0, r0, #1
	cmp r1, r0
	bhi _08034942
	mov r0, #0x10
	neg r0, r0
	cmp r7, r0
	blt _08034942
	cmp r7, #0xa0
	bgt _08034942
	mov r0, #5
	ldrsb r0, [r5, r0]
	cmp r0, #0
	blt _08034954
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	str r3, [sp, #4]
	bl hasKey
	lsl r0, r0, #0x10
	ldr r3, [sp, #4]
	cmp r0, #0
	bne _0803492C
	add r1, r4, #0
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, sb
	orr r0, r2
	strb r0, [r1]
	sub r6, #4
	mov ip, r6
	b _08034964
	.align 2, 0
.pool
_0803492C:
	mov r0, #5
	ldrsb r0, [r5, r0]
	sub r6, #4
	mov ip, r6
	cmp r0, #0
	bne _08034954
	ldr r0, =0x03001F08
	add r0, #0x72
	ldrh r0, [r0]
	cmp r0, #0
	beq _08034954
_08034942:
	add r1, r4, #0
	add r1, #0x23
	ldrb r0, [r1]
	mov r6, sb
	orr r0, r6
	strb r0, [r1]
	b _08034964
	.align 2, 0
.pool
_08034954:
	add r2, r4, #0
	add r2, #0x23
	ldrb r0, [r2]
	mov r6, #0x11
	neg r6, r6
	add r1, r6, #0
	and r0, r1
	strb r0, [r2]
_08034964:
	strh r3, [r4, #0x10]
	ldr r2, =doorArrowAnimation_0
	ldr r0, =0x03002080
	ldr r0, [r0]
	mov r1, #0x1f
	and r0, r1
	lsl r0, r0, #1
	add r0, r0, r2
	ldrh r0, [r0]
	add r0, r0, r7
	strh r0, [r4, #0x12]
_0803497A:
	add r5, #8
	mov r0, r8
	add r0, #1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r8, r0
	mov r1, ip
	ldrh r0, [r1]
	cmp r8, r0
	blo _080348BE
_0803498E:
	add sp, #8
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_080349A8
sub_080349A8: @ 0x080349A8
	push {r4, r5, r6, lr}
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	ldr r5, =0x03006C00
	ldrh r0, [r5, #0x2c]
	cmp r0, #0
	bne _08034A34
	ldr r3, =0x030046F0
	ldrh r0, [r3, #0x18]
	sub r0, #8
	strh r0, [r5, #0x28]
	ldrh r0, [r3, #0x1a]
	add r0, #0x34
	strh r0, [r5, #0x2a]
	ldr r1, [r5, #0x24]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	ldr r4, [r5, #0x24]
	ldrh r0, [r3, #0x18]
	sub r0, #8
	ldr r2, =0x03004720
	ldrh r1, [r2]
	sub r0, r0, r1
	strh r0, [r4, #0x10]
	ldr r3, [r5, #0x24]
	ldrh r0, [r2, #2]
	ldrh r1, [r5, #0x2a]
	sub r0, r0, r1
	strh r0, [r3, #0x12]
	cmp r6, #0
	bne _08034A24
	mov r0, #0
	bl hasKey
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08034A24
	ldr r0, [r5, #0x24]
	ldr r1, =off_8232820
	ldrh r2, [r0, #0xc]
	mov r3, #0
	bl setSpriteAnimation
	ldr r0, =(sub_8034AC8+1)
	mov r1, #0xff
	bl createTask
	b _08034A2C
	.align 2, 0
.pool
_08034A24:
	ldr r0, =(sub_8034A44+1)
	mov r1, #0xff
	bl createTask
_08034A2C:
	ldr r1, =0x03006C00
	mov r0, #1
	strh r0, [r1, #0x2c]
	strh r6, [r1, #0x32]
_08034A34:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
	
thumb_func_global sub_8034A44
sub_8034A44: @ 0x08034A44
	push {r4, lr}
	ldr r4, =0x03006C00
	ldr r3, [r4, #0x24]
	ldr r2, =0x03004720
	ldrh r0, [r4, #0x28]
	ldrh r1, [r2]
	sub r0, r0, r1
	strh r0, [r3, #0x10]
	ldr r3, [r4, #0x24]
	ldrh r0, [r2, #2]
	ldrh r1, [r4, #0x2a]
	sub r0, r0, r1
	strh r0, [r3, #0x12]
	ldr r0, [r4, #0x24]
	bl updateSpriteAnimation
	cmp r0, #0
	bne _08034A74
	ldrh r0, [r4, #0x32]
	bl hasKey
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08034AA4
_08034A74:
	ldr r1, [r4, #0x24]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	mov r0, #0
	strh r0, [r4, #0x2c]
	ldr r0, [r4, #0x24]
	ldr r1, =off_8232808
	ldrh r2, [r0, #0xc]
	mov r3, #0
	bl setSpriteAnimation
	bl endCurrentTask
	b _08034ABE
	.align 2, 0
.pool
_08034AA4:
	ldr r3, [r4, #0x24]
	ldr r0, =0x03002080
	ldr r1, [r0]
	mov r0, #8
	and r1, r0
	add r3, #0x23
	lsr r1, r1, #3
	lsl r1, r1, #4
	ldrb r2, [r3]
	sub r0, #0x19
	and r0, r2
	orr r0, r1
	strb r0, [r3]
_08034ABE:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool


thumb_func_global sub_8034AC8
sub_8034AC8: @ 0x08034AC8
	push {r4, lr}
	ldr r4, =0x03006C00
	ldr r3, [r4, #0x24]
	ldr r2, =0x03004720
	ldrh r0, [r4, #0x28]
	ldrh r1, [r2]
	sub r0, r0, r1
	strh r0, [r3, #0x10]
	ldr r3, [r4, #0x24]
	ldrh r0, [r2, #2]
	ldrh r1, [r4, #0x2a]
	sub r0, r0, r1
	strh r0, [r3, #0x12]
	ldr r0, [r4, #0x24]
	bl updateSpriteAnimation
	cmp r0, #0
	beq _08034B1C
	ldr r1, [r4, #0x24]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	mov r0, #0
	strh r0, [r4, #0x2c]
	ldr r0, [r4, #0x24]
	ldr r1, =off_8232808
	ldrh r2, [r0, #0xc]
	mov r3, #0
	bl setSpriteAnimation
	bl endCurrentTask
	b _08034B36
	.align 2, 0
.pool
_08034B1C:
	ldr r3, [r4, #0x24]
	ldr r0, =0x03002080
	ldr r1, [r0]
	mov r0, #8
	and r1, r0
	add r3, #0x23
	lsr r1, r1, #3
	lsl r1, r1, #4
	ldrb r2, [r3]
	sub r0, #0x19
	and r0, r2
	orr r0, r1
	strb r0, [r3]
_08034B36:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
	
thumb_func_global sub_08034B40
sub_08034B40: @ 0x08034B40
	push {lr}
	ldr r2, =0x03006C00
	ldrh r1, [r2, #0x2e]
	cmp r1, #0
	bne _08034B58
	mov r0, #1
	strh r0, [r2, #0x2e]
	strh r1, [r2, #0x30]
	ldr r0, =(sub_8034B64+1)
	mov r1, #0xff
	bl createTask
_08034B58:
	pop {r0}
	bx r0
	.align 2, 0
.pool
	
	
	
thumb_func_global sub_8034B64
sub_8034B64: @ 0x08034B64
	push {lr}
	sub sp, #4
	ldr r1, =0x03006C00
	ldrh r0, [r1, #0x30]
	add r0, #1
	mov r2, #0
	strh r0, [r1, #0x30]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0x30
	bls _08034B8C
	strh r2, [r1, #0x2e]
	strh r2, [r1, #0x30]
	bl drawRescue
	bl endCurrentTask
	b _08034BCC
	.align 2, 0
.pool
_08034B8C:
	ldr r0, =0x03002080
	ldr r0, [r0]
	mov r1, #8
	and r0, r1
	cmp r0, #0
	beq _08034BC8
	mov r1, sp
	mov r2, #0xc1
	lsl r2, r2, #8
	add r0, r2, #0
	strh r0, [r1]
	ldr r1, =0x0600FCE2
	ldr r0, =0x03001F08
	add r0, #0x72
	ldrh r2, [r0]
	add r2, #4
	mov r0, #0x80
	lsl r0, r0, #0x11
	orr r2, r0
	mov r0, sp
	bl Bios_memcpy
	b _08034BCC
	.align 2, 0
.pool
_08034BC8:
	bl drawRescue
_08034BCC:
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0

	
	

thumb_func_global hostageDeathGfx
hostageDeathGfx: @ 0x08034BD4
	push {r4, r5, lr}
	sub sp, #4
	ldr r5, =0x03001F08
	add r4, r5, #0
	add r4, #0x72
	ldrh r0, [r4]
	cmp r0, #0
	beq _08034BE8
	sub r0, #1
	strh r0, [r4]
_08034BE8:
	mov r1, sp
	mov r2, #0xc1
	lsl r2, r2, #8
	add r0, r2, #0
	strh r0, [r1]
	ldrh r1, [r4]
	lsl r1, r1, #1
	ldr r0, =0x0600FCEA
	add r1, r1, r0
	ldr r2, =0x01000001
	mov r0, sp
	bl Bios_memcpy
	ldr r3, =0x03006C00
	ldr r1, [r3, #0x24]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	ldr r1, [r3, #0x24]
	ldrh r0, [r4]
	lsl r0, r0, #3
	add r0, #0xa7
	strh r0, [r1, #0x10]
	ldr r1, [r3, #0x24]
	mov r0, #0x92
	strh r0, [r1, #0x12]
	ldr r0, [r3, #0x24]
	ldr r1, =dword_8232838
	ldrh r2, [r0, #0xc]
	mov r3, #0
	bl setSpriteAnimation
	ldr r0, =(sub_8034C6C+1)
	mov r1, #0xff
	bl createTask
	add r1, r5, #0
	add r1, #0x86
	ldrb r0, [r1]
	add r0, #1
	strb r0, [r1]
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global getByte3001F8E
getByte3001F8E: @ 0x08034C60
	ldr r0, =0x03001F08
	add r0, #0x86
	ldrb r0, [r0]
	bx lr
	.align 2, 0
.pool



thumb_func_global sub_8034C6C
sub_8034C6C: @ 0x08034C6C
	push {r4, lr}
	ldr r4, =0x03006C00
	ldr r0, [r4, #0x24]
	bl updateSpriteAnimation
	cmp r0, #0
	beq _08034CE8
	ldr r1, [r4, #0x24]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	ldr r0, [r4, #0x24]
	ldr r1, =off_8232808
	ldrh r2, [r0, #0xc]
	mov r3, #0
	bl setSpriteAnimation
	bl clearHostage_drawDone
	ldr r0, =0x03001F08
	add r0, #0x72
	ldrh r0, [r0]
	cmp r0, #0
	bne _08034CCC
	mov r0, #0
	bl hasKey
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08034CCC
	ldr r2, =off_8232908
	ldr r1, =0x03003D40
	ldrb r0, [r1, #2]
	lsl r0, r0, #2
	ldrb r1, [r1, #1]
	lsl r1, r1, #4
	add r0, r0, r1
	add r0, r0, r2
	ldr r0, [r0]
	cmp r0, #0
	beq _08034CCC
	mov r1, #0xa0
	lsl r1, r1, #0x13
	mov r2, #0
	bl startPalAnimation
_08034CCC:
	bl endCurrentTask
	b _08034D02
	.align 2, 0
.pool
_08034CE8:
	ldr r3, [r4, #0x24]
	ldr r0, =0x03002080
	ldr r1, [r0]
	mov r0, #8
	and r1, r0
	add r3, #0x23
	lsr r1, r1, #3
	lsl r1, r1, #4
	ldrb r2, [r3]
	sub r0, #0x19
	and r0, r2
	orr r0, r1
	strb r0, [r3]
_08034D02:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global findOpenedDoorPos
findOpenedDoorPos: @ 0x08034D0C
	push {r4, r5, lr}
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldr r2, =doorArrowPositions
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r2, [r0]
	ldrb r0, [r1, #2]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r2, [r0]
	ldrb r0, [r1, #3]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r1, [r0]
	ldrh r2, [r1]
	ldr r0, =0x0000FFFF
	cmp r2, r0
	beq _08034D68
	ldr r3, =0x030046F0
	add r4, r0, #0
_08034D3A:
	ldrb r0, [r1, #4]
	cmp r0, r5
	bne _08034D60
	add r0, r2, #0
	add r0, #8
	strh r0, [r3, #0x18]
	ldrh r0, [r1, #2]
	sub r0, #0x20
	strh r0, [r3, #0x1a]
	b _08034D68
	.align 2, 0
.pool
_08034D60:
	add r1, #8
	ldrh r2, [r1]
	cmp r2, r4
	bne _08034D3A
_08034D68:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global sub_08034D70
sub_08034D70: @ 0x08034D70
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #8
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	ldr r0, =0x03004750
	ldr r0, [r0]
	ldr r0, [r0]
	mov r1, #3
	mov r2, #0x80
	bl setSpritePriority
	ldr r2, =doorArrowPositions
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r2, [r0]
	ldrb r0, [r1, #2]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r2, [r0]
	ldrb r0, [r1, #3]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r6, [r0]
	ldrh r0, [r6]
	ldr r1, =0x0000FFFF
	cmp r0, r1
	beq _08034E34
	ldr r3, =0x03002050
_08034DB0:
	ldrb r0, [r6, #4]
	cmp r0, r4
	bne _08034E2C
	mov r5, #0
	ldr r0, =0x03006C00
	ldrh r1, [r0]
	cmp r5, r1
	bhs _08034DE2
	add r7, r0, #0
_08034DC2:
	lsl r4, r5, #2
	add r0, r7, #4
	add r4, r4, r0
	ldr r0, [r4]
	str r3, [sp, #4]
	bl spriteFreeHeader
	mov r0, #0
	str r0, [r4]
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldr r3, [sp, #4]
	ldrh r2, [r7]
	cmp r5, r2
	blo _08034DC2
_08034DE2:
	ldr r1, =doorTypes
	ldr r0, =0x03003D40
	ldrb r0, [r0, #1]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r2, [r0]
	ldrb r0, [r6, #6]
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r7, [r0]
	ldrh r0, [r6]
	strh r0, [r3]
	ldrh r0, [r6, #2]
	add r0, #0x10
	ldrb r1, [r7, #0x19]
	add r0, r0, r1
	sub r0, #0x28
	strh r0, [r3, #2]
	ldrb r0, [r7, #0x18]
	strb r0, [r3, #4]
	ldrb r0, [r7, #0x19]
	strb r0, [r3, #5]
	b _08034E34
	.align 2, 0
.pool
_08034E2C:
	add r6, #8
	ldrh r0, [r6]
	cmp r0, r1
	bne _08034DB0
_08034E34:
	ldrb r0, [r7, #4]
	cmp r0, #0
	bne _08034E3C
	b _08034F46
_08034E3C:
	bl allocateSpriteTiles
	lsl r6, r0, #0x10
	cmp r6, #0
	bge _08034E48
	b _08034F46
_08034E48:
	ldr r0, =0x03004750
	ldr r0, [r0]
	ldr r0, [r0]
	add r0, #0x22
	mov r2, #0
	ldrsb r2, [r0, r2]
	add r2, #1
	lsl r2, r2, #0x10
	ldrb r1, [r7, #0xc]
	lsl r1, r1, #5
	ldr r0, [r7, #8]
	add r0, r0, r1
	lsr r3, r2, #0x10
	mov r8, r3
	asr r2, r2, #0xb
	ldr r1, =0x05000200
	add r2, r2, r1
	mov r1, #1
	bl memcpy_pal
	ldr r0, [r7, #0x14]
	ldr r1, [r7]
	lsr r6, r6, #0x10
	mov r5, #0xff
	str r5, [sp]
	add r2, r6, #0
	mov r3, #2
	bl allocSprite
	ldr r4, =0x03002050
	str r0, [r4, #0xc]
	ldr r0, [r7, #0x10]
	ldr r1, [r7]
	str r5, [sp]
	add r2, r6, #0
	mov r3, #3
	bl allocSprite
	str r0, [r4, #8]
	ldr r1, [r4, #0xc]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	ldr r1, [r4, #8]
	ldr r0, [r4, #0xc]
	add r0, #0x22
	mov r2, r8
	strb r2, [r0]
	add r1, #0x22
	strb r2, [r1]
	ldr r2, [r4, #8]
	ldr r1, [r4, #0xc]
	add r1, #0x23
	ldrb r0, [r1]
	mov r3, #8
	orr r0, r3
	strb r0, [r1]
	add r2, #0x23
	ldrb r0, [r2]
	orr r0, r3
	strb r0, [r2]
	ldr r5, =0x03004720
	ldrh r1, [r4]
	ldrh r0, [r5]
	sub r1, r1, r0
	ldrb r3, [r4, #4]
	add r0, r3, #0
	sub r0, #0x10
	asr r0, r0, #1
	sub r1, r1, r0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	ldr r2, =0x03003D90
	lsl r0, r1, #8
	add r1, r1, r3
	orr r0, r1
	strh r0, [r2, #2]
	ldr r3, =0x03003D88
	ldrh r2, [r5, #2]
	ldrh r0, [r4, #2]
	sub r2, r2, r0
	add r1, r2, #1
	lsl r1, r1, #8
	ldrb r0, [r4, #5]
	add r0, r0, r2
	add r0, #1
	orr r1, r0
	strh r1, [r3, #2]
	ldr r5, =0x03003D7C
	mov r3, #0xfc
	lsl r3, r3, #6
	add r1, r3, #0
	ldr r3, =0x030046F0
	mov r4, #1
	add r0, r4, #0
	ldrb r2, [r3, #1]
	lsl r0, r2
	lsl r0, r0, #8
	add r2, r1, #0
	bic r2, r0
	strh r2, [r5]
	ldr r0, [r3, #0x24]
	cmp r0, #0
	beq _08034F2A
	ldrb r1, [r3, #1]
	add r1, #1
	add r0, r4, #0
	lsl r0, r1
	lsl r0, r0, #8
	bic r2, r0
	strh r2, [r5]
_08034F2A:
	ldr r1, =0x03003D8C
	mov r0, #0x3f
	strh r0, [r1]
	ldr r2, =0x03003CD4
	ldrh r0, [r2]
	mov r3, #0x80
	lsl r3, r3, #7
	add r1, r3, #0
	orr r0, r1
	strh r0, [r2]
	ldr r0, =(sub_8034F80+1)
	mov r1, #0xff
	bl createTask
_08034F46:
	add sp, #8
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool




thumb_func_global sub_8034F80
sub_8034F80:
	push {r4, r5, lr}
	ldr r4, =0x03002050
	ldr r2, [r4, #8]
	ldr r3, =0x03004720
	ldrh r1, [r4]
	ldrh r0, [r3]
	sub r1, r1, r0
	ldrb r0, [r4, #4]
	sub r0, #0x10
	asr r0, r0, #1
	sub r1, r1, r0
	strh r1, [r2, #0x10]
	ldr r2, [r4, #8]
	ldrh r0, [r3, #2]
	ldrh r1, [r4, #2]
	sub r0, r0, r1
	add r0, #1
	strh r0, [r2, #0x12]
	ldr r0, [r4, #8]
	ldrh r1, [r0, #0x10]
	ldr r5, =0x03003D90
	lsl r2, r1, #8
	ldrb r3, [r4, #4]
	add r1, r1, r3
	orr r2, r1
	strh r2, [r5, #2]
	ldr r5, =0x03003D88
	ldrh r1, [r0, #0x12]
	lsl r2, r1, #8
	ldrb r3, [r4, #5]
	add r1, r1, r3
	orr r2, r1
	strh r2, [r5, #2]
	bl updateSpriteAnimation
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool



	
thumb_func_global doorEnterVisibility
doorEnterVisibility:
	ldr r3, =0x03002050
	ldr r1, [r3, #0xc]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	ldr r1, [r3, #8]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	bx lr
	.align 2, 0
.pool



thumb_func_global setClosingDoorPos
setClosingDoorPos: @ 0x08035000
	push {r4, lr}
	ldr r2, =0x03002050
	ldr r3, [r2, #0xc]
	ldr r4, =0x03004720
	ldrh r1, [r2]
	ldrh r0, [r4]
	sub r1, r1, r0
	ldrb r0, [r2, #4]
	sub r0, #0x10
	asr r0, r0, #1
	sub r1, r1, r0
	strh r1, [r3, #0x10]
	ldr r3, [r2, #0xc]
	ldrh r0, [r4, #2]
	ldrh r1, [r2, #2]
	sub r0, r0, r1
	add r0, #1
	strh r0, [r3, #0x12]
	ldr r0, [r2, #0xc]
	bl updateSpriteAnimation
	cmp r0, #0
	bne _0803503C
	mov r0, #0
	b _0803503E
	.align 2, 0
.pool
_0803503C:
	mov r0, #1
_0803503E:
	pop {r4}
	pop {r1}
	bx r1



thumb_func_global sub_8035044
sub_8035044: @ 0x08035044
	push {r4, r5, r6, r7, lr}
	add r6, r0, #0
	ldr r5, =0x03003D40
	ldrb r0, [r5]
	cmp r0, #2
	beq _080350DA
	ldrb r0, [r5, #0x12]
	cmp r0, #0
	bne _080350DA
	ldr r7, =0x03001F90
	add r0, r6, #0
	mov r1, #0xa
	bl Bios_Div
	add r1, r0, #0
	add r0, r7, #0
	bl sub_08032294
	ldr r0, [r5, #0x1c]
	ldr r4, =0x0000C350
	add r1, r4, #0
	bl Bios_Div
	add r0, #1
	mul r4, r0, r4
	bl getPlayerHealth
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _080350B4
	ldrb r0, [r5, #0x10]
	cmp r0, #0
	bne _080350B4
	ldr r0, [r5, #0x1c]
	cmp r0, r4
	bhs _080350B4
	add r0, r0, r6
	cmp r0, r4
	blo _080350B4
	ldrb r0, [r5, #0x11]
	cmp r0, #1
	bhi _080350B4
	bl getPlayerHealth
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0x4f
	bhi _080350B0
	mov r0, #0x50
	bl modifyPlayerHealth
	ldr r0, =0x00000107
	bl playSong
_080350B0:
	mov r0, #0x20
	strb r0, [r7, #0x1d]
_080350B4:
	ldr r1, =0x03003D40
	ldr r0, [r1, #0x1c]
	add r0, r0, r6
	ldr r2, =0x0016E360
	cmp r0, r2
	bls _080350D8
	str r2, [r1, #0x1c]
	b _080350DA
	.align 2, 0
.pool
_080350D8:
	str r0, [r1, #0x1c]
_080350DA:
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0

thumb_func_global getHword3001F7A
getHword3001F7A: @ 0x080350E0
	ldr r0, =0x03001F08
	add r0, #0x72
	ldrh r0, [r0]
	bx lr
	.align 2, 0
.pool

thumb_func_global DrawPowerLevel
DrawPowerLevel: @ 0x080350EC
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #8
	ldr r6, =0x0600FCC4
	mov r1, sp
	ldr r2, =0x0000C116
	add r0, r2, #0
	strh r0, [r1]
	ldr r2, =0x01000001
	mov r0, sp
	add r1, r6, #0
	bl Bios_memcpy
	add r6, #2
	mov r4, #0
	ldr r2, =dword_8232A10
	ldr r0, =0x03002080
	add r1, r0, #0
	add r1, #0x2c
	ldrb r0, [r1]
	lsl r0, r0, #1
	add r0, r0, r2
	add r3, sp, #4
	mov r8, r3
	ldrh r0, [r0]
	cmp r4, r0
	bhs _0803515E
	mov r5, sp
	add r5, #2
	ldr r0, =unk_8232a0a
	mov sl, r0
	add r7, r1, #0
	mov sb, r2
_08035134:
	ldrb r0, [r7]
	lsl r0, r0, #1
	add r0, sl
	ldrh r0, [r0]
	add r0, r4, r0
	strh r0, [r5]
	add r0, r5, #0
	add r1, r6, #0
	ldr r2, =0x01000001
	bl Bios_memcpy
	add r6, #2
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	ldrb r0, [r7]
	lsl r0, r0, #1
	add r0, sb
	ldrh r0, [r0]
	cmp r4, r0
	blo _08035134
_0803515E:
	bl getPowerUpLevel
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	ldr r1, =0x0000C10B
	add r0, r0, r1
	mov r2, r8
	strh r0, [r2]
	ldr r2, =0x01000001
	mov r0, r8
	add r1, r6, #0
	bl Bios_memcpy
	add sp, #8
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global ClearPowerLevel
ClearPowerLevel: @ 0x080351A4
	push {lr}
	sub sp, #4
	mov r1, sp
	mov r2, #0xc1
	lsl r2, r2, #8
	add r0, r2, #0
	strh r0, [r1]
	ldr r1, =0x0600FCC4
	ldr r2, =dword_8232A10
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r0, [r0]
	lsl r0, r0, #1
	add r0, r0, r2
	ldrh r2, [r0]
	add r2, #2
	mov r0, #0x80
	lsl r0, r0, #0x11
	orr r2, r0
	mov r0, sp
	bl Bios_memcpy
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global task_blinkPowerLevel
task_blinkPowerLevel: @ 0x080351E4
	push {lr}
	ldr r1, =0x03003D40
	ldrb r0, [r1, #0x10]
	cmp r0, #0
	bne _080351F4
	ldrb r0, [r1, #6]
	cmp r0, #8
	bne _080351FC
_080351F4:
	bl DrawPowerLevel
	bl endCurrentTask
_080351FC:
	ldr r0, =0x03002060
	ldrh r1, [r0]
	mov r0, #4
	and r0, r1
	cmp r0, #0
	beq _08035218
	bl ClearPowerLevel
	b _0803521C
	.align 2, 0
.pool
_08035218:
	bl DrawPowerLevel
_0803521C:
	ldr r1, =0x03002060
	ldrh r0, [r1]
	add r0, #1
	strh r0, [r1]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #0x20
	bls _08035234
	bl DrawPowerLevel
	bl endCurrentTask
_08035234:
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global blinkPowerLevel
blinkPowerLevel: @ 0x0803523C
	push {lr}
	ldr r0, =0x03002060
	mov r1, #0
	strh r1, [r0]
	ldr r0, =(task_blinkPowerLevel+1)
	mov r1, #0xff
	bl createTask
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global getPowerUpLevel
getPowerUpLevel: @ 0x08035258
	ldr r0, =0x03001F08
	add r0, #0xa4
	ldrb r0, [r0]
	bx lr
	.align 2, 0
.pool

thumb_func_global setPowerupLevel
setPowerupLevel: @ 0x08035264
	lsl r0, r0, #0x18
	lsr r1, r0, #0x18
	cmp r1, #2
	bls _0803526E
	mov r1, #2
_0803526E:
	ldr r0, =0x03001F08
	add r0, #0xa4
	strb r1, [r0]
	bx lr
	.align 2, 0
.pool

thumb_func_global lowerPowerupLevelIfNeeded
lowerPowerupLevelIfNeeded: @ 0x0803527C
	push {lr}
	ldr r0, =0x03001F08
	add r1, r0, #0
	add r1, #0xa4
	ldrb r0, [r1]
	cmp r0, #0
	beq _0803529E
	sub r0, #1
	strb r0, [r1]
	bl blinkPowerLevel
	ldr r0, =0x0000017B
	bl playSong
	mov r0, #7
	bl setPlayerPaletteMaybe
_0803529E:
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global incPowerupIfNeeded
incPowerupIfNeeded: @ 0x080352AC
	push {lr}
	ldr r0, =0x03001F08
	add r1, r0, #0
	add r1, #0xa4
	ldrb r0, [r1]
	cmp r0, #1
	bhi _080352C8
	add r0, #1
	strb r0, [r1]
	bl blinkPowerLevel
	mov r0, #6
	bl setPlayerPaletteMaybe
_080352C8:
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global isUsingInvulnSpecial
isUsingInvulnSpecial: @ 0x080352D0
	ldr r0, =0x03001F08
	add r0, #0x85
	ldrb r0, [r0]
	lsl r0, r0, #0x1e
	lsr r0, r0, #0x1f
	bx lr
	.align 2, 0
.pool

thumb_func_global EnableInvulnSpecial
EnableInvulnSpecial: @ 0x080352E0
	ldr r0, =0x03001F08
	add r0, #0x85
	ldrb r1, [r0]
	mov r2, #2
	orr r1, r2
	strb r1, [r0]
	bx lr
	.align 2, 0
.pool

thumb_func_global disableInvulnSpecial
disableInvulnSpecial: @ 0x080352F4
	ldr r0, =0x03001F08
	add r0, #0x85
	ldrb r2, [r0]
	mov r1, #3
	neg r1, r1
	and r1, r2
	strb r1, [r0]
	bx lr
	.align 2, 0
.pool

thumb_func_global giveItem
giveItem: @ 0x08035308
	push {r4, lr}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r1, =itemData
	lsl r2, r0, #2
	add r1, r2, r1
	ldr r4, [r1]
	ldrb r1, [r4]
	cmp r1, r0
	bne _080353D0
	cmp r1, #4
	bhi _0803538A
	ldr r0, =off_8035330
	add r0, r2, r0
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
.pool
off_8035330:.4byte bigHealthItem
			.4byte bigHealthItem
			.4byte smallHealthItem
			.4byte SpecialChargeItem
			.4byte powerupItem
bigHealthItem:
	bl getHealthBarValue
	b bigHp
smallHealthItem:
	bl getHealthBarValue
	lsl r0, #0x10
	lsr r0, #0x10
	cmp r0, #0x50
	bne _0803538A
	mov r0,#0xFA
	lsl r0,#3
	bl sub_8035044
	b _0803538A
SpecialChargeItem:
	bl getPlayerSpecialBar
bigHp:
	lsl r0,#0x10
	lsr r0,#0x10
	cmp r0, #0x50
	bne _0803538A
	ldr r0, =0xBB8
	bl sub_8035044
	b _0803538A
.pool
powerupItem:
	bl getPowerUpLevel
	lsl r0, #0x18
	lsr r0, #0x18
	cmp r0, #2
	bne _0803538A
	ldr r0, =0xBB8
	bl sub_8035044	
_0803538A:
	ldrb r0, [r4, #1]
	cmp r0, #1
	beq _080353B4
	cmp r0, #1
	bgt _080353A0
	cmp r0, #0
	beq _080353AA
	b _080353CA
	.align 2, 0
.pool
_080353A0:
	cmp r0, #2
	beq _080353BE
	cmp r0, #3
	beq _080353C6
	b _080353CA
_080353AA:
	mov r1, #2
	ldrsh r0, [r4, r1]
	bl modifyPlayerHealth
	b _080353CA
_080353B4:
	mov r1, #2
	ldrsh r0, [r4, r1]
	bl ModifySpecialCharge
	b _080353CA
_080353BE:
	ldrh r0, [r4, #2]
	bl giveKey
	b _080353CA
_080353C6:
	bl incPowerupIfNeeded
_080353CA:
	ldrh r0, [r4, #4]
	bl playSong
_080353D0:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
