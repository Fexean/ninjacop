.include "asm/macros.inc"

@completely disassembled




@These are used to play sound effects as a part of the background music

thumb_func_global createLevelTask
createLevelTask: @ 0x08013A94
	push {lr}
	ldr r0, =0x03000D50
	mov r1, #0
	str r1, [r0]
	ldr r0, =0x03000D54
	strh r1, [r0]
	ldr r0, =(runLevelCallback+1)
	mov r1, #0xff
	bl createTask
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global runLevelCallback
runLevelCallback: @ 0x08013AB8
	push {lr}
	ldr r1, =levelFunctions
	ldr r0, =0x03003D40
	ldrb r0, [r0, #1]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl call_r0
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global bank_task
bank_task: @ 0x08013AD8
	push {r4, r5, lr}
	ldr r5, =0x03000D54
	ldrh r0, [r5]
	cmp r0, #1
	beq _08013B3E
	cmp r0, #1
	bgt _08013B56
	cmp r0, #0
	bne _08013B56
	ldr r2, =musicPlayer_1
	ldr r0, =m4a_songs
	ldr r1, =0x00000DC4
	add r0, r0, r1
	ldrh r1, [r0]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #2
	add r0, r0, r2
	ldr r0, [r0]
	ldr r4, =0x0000FFFF
	ldrh r0, [r0, #4]
	cmp r0, #0
	beq _08013B0E
	mov r0, #0xdc
	lsl r0, r0, #1
	bl m4aSongNumStop
_08013B0E:
	ldr r0, =0x03003D40
	ldrh r0, [r0, #2]
	and r4, r0
	cmp r4, #0
	beq _08013B38
	mov r0, #2
	strh r0, [r5]
	b _08013B56
	.align 2, 0
.pool
_08013B38:
	mov r0, #1
	strh r0, [r5]
	b _08013B56
_08013B3E:
	ldr r0, =0x03000D50
	ldr r0, [r0]
	mov r1, #0x96
	lsl r1, r1, #2
	bl Bios_modulo
	cmp r0, #0x1e
	bne _08013B56
	mov r0, #0xdc
	lsl r0, r0, #1
	bl playSong
_08013B56:
	ldr r1, =0x03000D50
	ldr r0, [r1]
	add r0, #1
	str r0, [r1]
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global base_task
base_task: @ 0x08013B68
	bx lr
	.align 2, 0

thumb_func_global harbor_task
harbor_task: @ 0x08013B6C
	push {r4, lr}
	mov r4, #0
	mov r0, #4
	bl sub_08010710
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #1
	bne _08013BE0
	mov r0, #4
	bl sub_08010720
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	add r3, r1, #0
	mov r2, #0
	ldr r0, =0x000001AB
	cmp r1, r0
	beq _08013BAA
	cmp r1, r0
	bgt _08013BA4
	sub r0, #1
	cmp r1, r0
	beq _08013BBC
	b _08013BC6
	.align 2, 0
.pool
_08013BA4:
	ldr r0, =0x000001AD
	cmp r3, r0
	bne _08013BC6
_08013BAA:
	mov r0, #0
	bl sub_08013C0C
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
	mov r4, #1
	b _08013BC6
	.align 2, 0
.pool
_08013BBC:
	mov r0, #1
	bl sub_08013C0C
	lsl r0, r0, #0x10
	lsr r2, r0, #0x10
_08013BC6:
	cmp r2, #0
	beq _08013BDC
	ldr r0, =musicPlayer_1
	ldr r0, [r0, #0x30]
	mov r1, #3
	bl sub_08037508
	b _08013BFC
	.align 2, 0
.pool
_08013BDC:
	cmp r4, #0
	beq _08013BFC
_08013BE0:
	mov r0, #4
	bl stopTrack
	ldr r4, =0x03000D50
	ldr r0, [r4]
	add r0, #1
	str r0, [r4]
	cmp r0, #0xb4
	bls _08013BFC
	ldr r0, =0x000001BF
	bl playSong
	mov r0, #0
	str r0, [r4]
_08013BFC:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08013C0C
sub_08013C0C: @ 0x08013C0C
	push {r4, lr}
	lsl r0, r0, #0x10
	ldr r1, =0x03004750
	ldr r2, [r1]
	ldr r1, [r2, #4]
	lsl r1, r1, #8
	lsr r4, r1, #0x10
	ldr r1, [r2, #8]
	lsl r1, r1, #8
	lsr r3, r1, #0x10
	ldr r1, =off_80DDC38
	lsr r0, r0, #0xe
	add r0, r0, r1
	ldr r1, [r0]
	cmp r1, #0
	beq _08013C84
	ldr r2, =0x03003D40
	ldrb r0, [r2, #2]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r1, [r0]
	cmp r1, #0
	beq _08013C84
	ldrb r0, [r2, #3]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r1, [r0]
	cmp r1, #0
	beq _08013C84
	ldrh r0, [r1]
	ldr r2, =0x0000FFFF
	cmp r0, r2
	beq _08013C84
_08013C4E:
	ldrh r0, [r1]
	cmp r4, r0
	blo _08013C66
	ldrh r0, [r1, #4]
	cmp r4, r0
	bhi _08013C66
	ldrh r0, [r1, #2]
	cmp r3, r0
	blo _08013C66
	ldrh r0, [r1, #6]
	cmp r3, r0
	bls _08013C80
_08013C66:
	add r1, #0xc
	ldrh r0, [r1]
	cmp r0, r2
	bne _08013C4E
	b _08013C84
	.align 2, 0
.pool
_08013C80:
	ldrh r0, [r1, #8]
	b _08013C86
_08013C84:
	mov r0, #0
_08013C86:
	pop {r4}
	pop {r1}
	bx r1

thumb_func_global cave_task
cave_task: @ 0x08013C8C
	push {r4, r5, lr}
	mov r0, #2
	bl sub_08013C0C
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	mov r0, #1
	bl sub_08010710
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #1
	bne _08013CAE
	mov r0, #4
	bl stopTrack
	b _08013D3C
_08013CAE:
	mov r5, #0xe1
	lsl r5, r5, #1
	add r0, r5, #0
	bl sub_08010730
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08013CD4
	cmp r4, #0
	beq _08013CF0
	ldr r0, =musicPlayer_1
	ldr r0, [r0, #0x30]
	mov r1, #3
	add r2, r4, #0
	bl sub_08037508
	b _08013CEC
	.align 2, 0
.pool
_08013CD4:
	cmp r4, #0
	beq _08013CF0
	mov r0, #4
	bl sub_08010710
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #1
	beq _08013CEC
	add r0, r5, #0
	bl playSong
_08013CEC:
	cmp r4, #0
	bne _08013D34
_08013CF0:
	ldr r0, =0x03000D50
	ldr r0, [r0]
	mov r1, #0x96
	lsl r1, r1, #2
	bl Bios_modulo
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	add r4, r1, #0
	mov r0, #0x96
	lsl r0, r0, #1
	cmp r1, r0
	bne _08013D18
	add r0, #0x8f
	bl playSong
	b _08013D34
	.align 2, 0
.pool
_08013D18:
	mov r0, #0xf0
	lsl r0, r0, #1
	cmp r1, r0
	bne _08013D26
	sub r0, #0x1f
	bl playSong
_08013D26:
	mov r0, #0x87
	lsl r0, r0, #2
	cmp r4, r0
	bne _08013D34
	sub r0, #0x5b
	bl playSong
_08013D34:
	ldr r1, =0x03000D50
	ldr r0, [r1]
	add r0, #1
	str r0, [r1]
_08013D3C:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global launch_task
launch_task: @ 0x08013D48
	push {r4, r5, lr}
	ldr r0, =0x03004750
	ldr r0, [r0]
	ldr r0, [r0, #8]
	lsl r0, r0, #8
	lsr r4, r0, #0x10
	add r5, r4, #0
	mov r0, #4
	bl sub_08010710
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #1
	bne _08013DD8
	mov r0, #4
	bl sub_08010720
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	add r2, r1, #0
	ldr r0, =0x03003D40
	ldrb r0, [r0, #0x11]
	cmp r0, #0
	beq _08013DBC
	mov r0, #0xd7
	lsl r0, r0, #1
	cmp r1, r0
	bne _08013D96
	ldr r1, =0xFFFFFE1F
	add r0, r4, r1
	lsl r0, r0, #0x10
	mov r1, #0xe7
	lsl r1, r1, #0x11
	cmp r0, r1
	bls _08013DA6
	mov r0, #0xaa
	lsl r0, r0, #3
	cmp r4, r0
	bhi _08013DA6
_08013D96:
	mov r0, #0xd5
	lsl r0, r0, #1
	cmp r2, r0
	bne _08013DD8
	mov r0, #0xb0
	lsl r0, r0, #2
	cmp r5, r0
	bls _08013DD8
_08013DA6:
	mov r0, #4
	bl stopTrack
	b _08013DD8
	.align 2, 0
.pool
_08013DBC:
	mov r0, #0xd7
	lsl r0, r0, #1
	cmp r2, r0
	beq _08013DCA
	sub r0, #4
	cmp r2, r0
	bne _08013DD8
_08013DCA:
	mov r0, #0xb0
	lsl r0, r0, #2
	cmp r5, r0
	bls _08013DD8
	mov r0, #4
	bl stopTrack
_08013DD8:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0

thumb_func_global sub_08013DE0
sub_08013DE0: @ 0x08013DE0
	push {r4, r5, lr}
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	ldr r0, =0x03003D40
	ldrb r1, [r0, #1]
	add r5, r0, #0
	cmp r1, #4
	bls _08013E58
	mov r0, #0xd5
	lsl r0, r0, #1
	cmp r3, r0
	beq _08013DFE
	add r0, #4
	cmp r3, r0
	bne _08013E58
_08013DFE:
	ldr r0, =0x03004750
	ldr r0, [r0]
	ldr r0, [r0, #8]
	lsl r0, r0, #8
	lsr r2, r0, #0x10
	add r4, r2, #0
	ldrb r0, [r5, #0x11]
	cmp r0, #0
	beq _08013E3C
	mov r0, #0xd7
	lsl r0, r0, #1
	cmp r3, r0
	bne _08013E44
	ldr r1, =0xFFFFFE1F
	add r0, r2, r1
	lsl r0, r0, #0x10
	mov r1, #0xe7
	lsl r1, r1, #0x11
	cmp r0, r1
	bls _08013E54
	mov r0, #0xaa
	lsl r0, r0, #3
	cmp r2, r0
	bhi _08013E54
	b _08013E44
	.align 2, 0
.pool
_08013E3C:
	mov r0, #0xd7
	lsl r0, r0, #1
	cmp r3, r0
	beq _08013E4C
_08013E44:
	mov r0, #0xd5
	lsl r0, r0, #1
	cmp r3, r0
	bne _08013E58
_08013E4C:
	mov r0, #0xb0
	lsl r0, r0, #2
	cmp r4, r0
	bls _08013E58
_08013E54:
	mov r0, #1
	b _08013E5A
_08013E58:
	mov r0, #0
_08013E5A:
	pop {r4, r5}
	pop {r1}
	bx r1

thumb_func_global airport_task
airport_task: @ 0x08013E60
	push {lr}
	ldr r2, =0x03000D54
	ldrh r0, [r2]
	cmp r0, #1
	beq _08013EC4
	cmp r0, #1
	bgt _08013E78
	cmp r0, #0
	beq _08013E7E
	b _08013EF2
	.align 2, 0
.pool
_08013E78:
	cmp r0, #2
	beq _08013EDA
	b _08013EF2
_08013E7E:
	ldr r1, =0x03003D40
	ldrb r0, [r1, #2]
	cmp r0, #1
	beq _08013EA8
	cmp r0, #1
	bgt _08013E94
	cmp r0, #0
	beq _08013EA0
	b _08013EF2
	.align 2, 0
.pool
_08013E94:
	cmp r0, #3
	bgt _08013EF2
	ldrb r0, [r1, #3]
	cmp r0, #0
	beq _08013EBE
	b _08013EB8
_08013EA0:
	ldrb r0, [r1, #3]
	cmp r0, #0
	bne _08013EB8
	b _08013EB2
_08013EA8:
	ldrb r0, [r1, #3]
	cmp r0, #1
	beq _08013EB2
	cmp r0, #4
	bne _08013EB8
_08013EB2:
	mov r0, #2
	strh r0, [r2]
	b _08013EF2
_08013EB8:
	mov r0, #3
	strh r0, [r2]
	b _08013EF2
_08013EBE:
	mov r0, #1
	strh r0, [r2]
	b _08013EF2
_08013EC4:
	mov r0, #4
	bl sub_08010710
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _08013EF2
	mov r0, #0xdd
	lsl r0, r0, #1
	bl playSong
	b _08013EF2
_08013EDA:
	ldr r0, =0x03000D50
	ldr r0, [r0]
	mov r1, #0xe1
	lsl r1, r1, #3
	bl Bios_modulo
	cmp r0, #0x64
	bne _08013EF2
	mov r0, #0xe0
	lsl r0, r0, #1
	bl playSong
_08013EF2:
	ldr r1, =0x03000D50
	ldr r0, [r1]
	add r0, #1
	str r0, [r1]
	pop {r0}
	bx r0
	.align 2, 0
.pool





	@These ones run when the level containing their pointers is loaded
	
	
thumb_func_global levelfunc_harbor1
levelfunc_harbor1: @ 0x08013F04
	push {r4, r5, lr}
	sub sp, #4
	ldr r4, =0x03003D40
	ldrb r0, [r4, #1]
	cmp r0, #1
	bne _08013F7C
	ldrb r1, [r4, #2]
	ldr r0, =stageCount
	ldrh r0, [r0, #2]
	sub r0, #1
	cmp r1, r0
	beq _08013F7C
	ldr r5, =0x0201FF50
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r2, =0x01000008
	mov r0, sp
	add r1, r5, #0
	bl Bios_memcpy
	ldrb r0, [r4, #2]
	cmp r0, #1
	bls _08013F3A
	ldrb r0, [r4, #0x11]
	cmp r0, #0
	bne _08013F54
_08013F3A:
	ldr r0, =unk_80E0144
	b _08013F56
	.align 2, 0
.pool
_08013F54:
	ldr r0, =unk_80E024C
_08013F56:
	str r0, [r5, #8]
	mov r0, #1
	strh r0, [r5]
	ldr r0, =(animateHarborConveyor+1)
	mov r1, #0xfe
	bl createTask
	ldr r0, =0x03003D40
	ldrh r1, [r0, #2]
	mov r0, #0x80
	lsl r0, r0, #2
	cmp r1, r0
	bne _08013F7C
	mov r0, #0
	strh r0, [r5, #0xc]
	ldr r0, =(animateHarborTurbine+1)
	mov r1, #0xfe
	bl createTask
_08013F7C:
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global animateHarborConveyor
animateHarborConveyor: @ 0x08013F94
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	ldr r4, =0x000001AB
	ldr r1, =0x03003D40
	ldrb r0, [r1, #2]
	cmp r0, #2
	bne _08013FAC
	ldrb r0, [r1, #0x11]
	cmp r0, #0
	beq _08013FAC
	add r4, #2
_08013FAC:
	ldr r1, =m4a_songs
	lsl r0, r4, #3
	add r0, r0, r1
	ldrh r0, [r0, #4]
	bl sub_08010710
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _08013FC4
	add r0, r4, #0
	bl playSong
_08013FC4:
	ldr r0, =0x0201FF50
	mov r8, r0
	mov r2, #0
	ldrh r1, [r0]
	cmp r2, r1
	bhs _08014066
_08013FD0:
	lsl r0, r2, #3
	add r0, #4
	mov r3, r8
	add r4, r3, r0
	ldrh r0, [r4, #2]
	lsl r0, r0, #3
	ldr r1, [r4, #4]
	add r6, r1, r0
	mov r1, #0
	ldrsh r0, [r6, r1]
	add r7, r2, #1
	cmp r0, #0
	beq _0801405A
	ldrh r0, [r4]
	cmp r0, #0
	bne _0801402C
	mov r5, #0
	ldr r3, [r6, #4]
	ldrh r2, [r3]
	cmp r5, r2
	bhs _0801402C
_08013FFA:
	lsl r2, r5, #1
	add r0, r3, #0
	add r0, #0xa
	add r0, r0, r2
	ldrh r1, [r0]
	lsl r1, r1, #5
	ldr r0, [r3, #0x14]
	add r0, r0, r1
	add r1, r3, #2
	add r1, r1, r2
	ldrh r1, [r1]
	lsl r1, r1, #5
	mov r3, #0xc0
	lsl r3, r3, #0x13
	add r1, r1, r3
	mov r2, #0x10
	bl Bios_memcpy
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldr r3, [r6, #4]
	ldrh r0, [r3]
	cmp r5, r0
	blo _08013FFA
_0801402C:
	ldrh r0, [r4]
	add r0, #1
	mov r2, #0
	strh r0, [r4]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r3, #0
	ldrsh r1, [r6, r3]
	cmp r0, r1
	blt _0801405A
	strh r2, [r4]
	ldrh r0, [r4, #2]
	add r0, #1
	strh r0, [r4, #2]
	ldrh r0, [r4, #2]
	lsl r0, r0, #3
	ldr r1, [r4, #4]
	add r6, r1, r0
	mov r1, #0
	ldrsh r0, [r6, r1]
	cmp r0, #0
	bge _0801405A
	strh r2, [r4, #2]
_0801405A:
	lsl r0, r7, #0x10
	lsr r2, r0, #0x10
	mov r3, r8
	ldrh r3, [r3]
	cmp r2, r3
	blo _08013FD0
_08014066:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

	thumb_func_global animateHarborTurbine
animateHarborTurbine: @ 0x08014080
	push {r4, r5, r6, r7, lr}
	ldr r6, =0x0201FF50
	ldrh r0, [r6, #0xc]
	mov r1, #8
	bl Bios_modulo
	cmp r0, #0
	bne _080140D2
	ldrh r0, [r6, #0xc]
	mov r1, #8
	bl Bios_Div
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r4, #0
	ldr r7, =harborTurbineSrcOffsets
	lsl r1, r0, #1
	add r1, r1, r0
	lsl r5, r1, #2
_080140A6:
	ldr r0, =harborTurbineTiles
	lsl r3, r4, #1
	add r1, r3, r5
	add r1, r1, r7
	ldrh r1, [r1]
	lsl r1, r1, #5
	add r0, r1, r0
	mov r1, #0xc0
	lsl r1, r1, #0x13
	ldr r2, =harborTurbineDstOffsets
	add r3, r3, r2
	ldrh r2, [r3]
	lsl r2, r2, #5
	add r1, r2, r1
	mov r2, #0x60
	bl Bios_memcpy
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #5
	bls _080140A6
_080140D2:
	ldrh r0, [r6, #0xc]
	add r0, #1
	mov r1, #0x20
	bl Bios_modulo
	strh r0, [r6, #0xc]
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool

	thumb_func_global levelfunc_airport1
levelfunc_airport1: @ 0x080140F4
	push {r4, r5, lr}
	sub sp, #4
	ldr r4, =0x0201FF50
	ldr r0, =0x03002080
	ldr r0, [r0]
	bl srand
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r2, =0x01000038
	mov r0, sp
	add r1, r4, #0
	bl Bios_memcpy
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	cmp r0, #2
	bne _080141CE
	ldrb r0, [r1, #2]
	cmp r0, #2
	bhi _080141CE
	cmp r0, #0
	beq _08014140
	cmp r0, #1
	beq _08014148
	ldr r0, =off_80EF1E4
	b _0801414A
	.align 2, 0
.pool
_08014140:
	ldr r0, =off_80EF1B4
	b _0801414A
	.align 2, 0
.pool
_08014148:
	ldr r0, =off_80EF1C4
_0801414A:
	ldrb r1, [r1, #3]
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r0, [r1]
	str r0, [r4, #0x6c]
	ldr r0, =0x03003D40
	ldrb r0, [r0, #1]
	cmp r0, #4
	bhi _08014174
	ldr r0, [r4, #0x6c]
	ldrb r0, [r0, #1]
	lsl r0, r0, #5
	ldr r1, =dword_80E74C8
	b _0801417C
	.align 2, 0
.pool
_08014174:
	ldr r0, [r4, #0x6c]
	ldrb r0, [r0, #1]
	lsl r0, r0, #5
	ldr r1, =dword_80EE424
_0801417C:
	add r0, r0, r1
	mov r1, #1
	bl LoadSpritePalette
	strh r0, [r4]
	mov r5, #0
	b _080141BE
	.align 2, 0
.pool
_08014190:
	mov r0, #0x18
	bl allocateSpriteTiles
	lsl r2, r5, #1
	add r1, r4, #2
	add r1, r1, r2
	strh r0, [r1]
	bl rand
	mov r1, #8
	bl Bios_modulo
	add r1, r4, #0
	add r1, #0x29
	add r1, r1, r5
	ldr r2, =dword_80EF304
	lsl r0, r0, #1
	add r0, r0, r2
	ldrh r0, [r0]
	strb r0, [r1]
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
_080141BE:
	ldr r0, [r4, #0x6c]
	ldrb r0, [r0]
	cmp r5, r0
	blo _08014190
	ldr r0, =(sub_80141E0+1)
	mov r1, #0xfe
	bl createTask
_080141CE:
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_80141E0
sub_80141E0: @ 0x080141E0
	push {r4, r5, r6, lr}
	ldr r6, =0x0201FF50
	mov r1, #0
	ldrsh r0, [r6, r1]
	cmp r0, #0
	bge _08014226
	ldr r0, =0x03003D40
	ldrb r0, [r0, #1]
	cmp r0, #4
	bhi _0801420C
	ldr r0, [r6, #0x6c]
	ldrb r0, [r0, #1]
	lsl r0, r0, #5
	ldr r1, =dword_80E74C8
	b _08014214
	.align 2, 0
.pool
_0801420C:
	ldr r0, [r6, #0x6c]
	ldrb r0, [r0, #1]
	lsl r0, r0, #5
	ldr r1, =dword_80EE424
_08014214:
	add r0, r0, r1
	mov r1, #1
	bl LoadSpritePalette
	strh r0, [r6]
	mov r1, #0
	ldrsh r0, [r6, r1]
	cmp r0, #0
	blt _08014262
_08014226:
	mov r5, #0
	b _0801425A
	.align 2, 0
.pool
_08014230:
	lsl r0, r5, #1
	add r1, r6, #2
	add r4, r1, r0
	mov r1, #0
	ldrsh r0, [r4, r1]
	cmp r0, #0
	bge _0801424C
	mov r0, #0x18
	bl allocateSpriteTiles
	strh r0, [r4]
	lsl r0, r0, #0x10
	cmp r0, #0
	blt _08014254
_0801424C:
	add r0, r5, #0
	add r1, r6, #0
	bl sub_8014268
_08014254:
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
_0801425A:
	ldr r0, [r6, #0x6c]
	ldrb r0, [r0]
	cmp r5, r0
	blo _08014230
_08014262:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	thumb_func_global sub_8014268
sub_8014268: @ 0x08014268
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x10
	add r6, r1, #0
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sb, r0
	ldr r0, [r6, #0x6c]
	mov r2, sb
	lsl r1, r2, #2
	add r0, #4
	add r0, r0, r1
	ldr r4, [r0]
	ldrh r2, [r4]
	ldr r5, =0x03004720
	ldrh r0, [r5]
	sub r2, r2, r0
	ldr r3, =dword_80EF314
	ldrb r1, [r4, #0xc]
	lsl r1, r1, #2
	add r0, r1, r3
	mov r7, #0
	ldrsh r0, [r0, r7]
	add r2, r2, r0
	str r2, [sp, #4]
	ldrh r2, [r5, #2]
	ldrh r0, [r4, #2]
	sub r2, r2, r0
	add r3, #2
	add r1, r1, r3
	mov r3, #0
	ldrsh r0, [r1, r3]
	add r2, r2, r0
	str r2, [sp, #8]
	add r0, r6, #0
	add r0, #0x1c
	add r0, sb
	ldrb r0, [r0]
	cmp r0, #8
	bls _080142C0
	b def_80142C8
_080142C0:
	lsl r0, r0, #2
	ldr r1, =_080142D8
	add r0, r0, r1
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
.pool
_080142D8: @ jump table
	.4byte _080142FC @ case 0
	.4byte _080143D4 @ case 1
	.4byte _0801443C @ case 2
	.4byte _08014584 @ case 3
	.4byte _08014584 @ case 4
	.4byte _08014584 @ case 5
	.4byte _08014704 @ case 6
	.4byte _080147AC
	.4byte _080147AC
_080142FC:
	ldr r0, =0x03003D40
	ldrb r0, [r0, #0x11]
	ldr r5, [r6, #0x6c]
	mov r4, sb
	lsl r7, r4, #2
	cmp r0, #0
	bne _08014318
	add r0, r5, #4
	add r0, r0, r7
	ldr r0, [r0]
	ldrb r0, [r0, #0xc]
	cmp r0, #3
	bls _08014318
	b _080148DC
_08014318:
	add r0, r5, #4
	add r0, r0, r7
	ldr r1, [r0]
	ldr r2, [r1, #8]
	ldr r0, [r2, #0x14]
	cmp r0, #0
	bne _08014342
	ldrb r0, [r1, #4]
	lsl r0, r0, #3
	ldr r1, [sp, #4]
	cmn r1, r0
	bge _08014332
	b _080148DC
_08014332:
	cmp r1, #0xf0
	ble _08014338
	b _080148DC
_08014338:
	ldr r0, [sp, #8]
	add r0, #0x20
	cmp r0, #0xc0
	bls _08014342
	b _080148DC
_08014342:
	ldr r0, [r2]
	ldr r1, [r2, #0x10]
	mov r2, sb
	lsl r3, r2, #1
	add r2, r6, #2
	add r5, r2, r3
	ldrh r2, [r5]
	mov r3, #0
	str r3, [sp]
	mov r3, #2
	bl allocSprite
	add r2, r0, #0
	add r0, r6, #0
	add r0, #0x38
	add r4, r0, r7
	str r2, [r4]
	cmp r2, #0
	bne _0801436A
	b _080148DC
_0801436A:
	add r2, #0x23
	ldrb r0, [r2]
	mov r1, #0x10
	orr r0, r1
	strb r0, [r2]
	ldr r0, [r6, #0x6c]
	add r0, #4
	add r0, r0, r7
	ldr r0, [r0]
	ldr r3, [r0, #8]
	ldr r1, [r3, #0x14]
	cmp r1, #0
	beq _0801439C
	ldr r0, [r4]
	ldrh r2, [r5]
	ldr r3, [r3, #0x10]
	bl setSpriteAnimation
	ldr r1, [r4]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
_0801439C:
	add r1, r6, #0
	add r1, #0x1c
	add r1, sb
	ldrb r0, [r1]
	add r0, #1
	strb r0, [r1]
	ldr r0, [r4]
	ldrh r1, [r6]
	add r0, #0x22
	strb r1, [r0]
	ldr r0, [r6, #0x6c]
	add r0, #4
	add r0, r0, r7
	ldr r0, [r0]
	ldrb r0, [r0, #0xc]
	cmp r0, #2
	beq _080143C0
	b def_80142C8
_080143C0:
	ldr r0, [r4]
	add r0, #0x24
	ldrb r1, [r0]
	mov r2, #2
	orr r1, r2
	strb r1, [r0]
	b def_80142C8
	.align 2, 0
.pool
_080143D4:
	ldr r0, =0x03002080
	ldr r0, [r0]
	add r4, r6, #0
	add r4, #0x29
	add r4, sb
	ldrb r1, [r4]
	bl Bios_modulo
	ldrb r1, [r4]
	sub r1, #1
	cmp r0, r1
	beq _080143EE
	b def_80142C8
_080143EE:
	ldr r0, [r6, #0x6c]
	mov r3, sb
	lsl r7, r3, #2
	add r0, #4
	add r0, r0, r7
	ldr r0, [r0]
	ldr r4, [r0, #8]
	ldr r0, [r4, #0x14]
	add r5, r6, #0
	add r5, #0x38
	cmp r0, #0
	beq _0801441A
	add r0, r5, r7
	ldr r0, [r0]
	ldr r1, [r4]
	lsl r3, r3, #1
	add r2, r6, #2
	add r2, r2, r3
	ldrh r2, [r2]
	ldr r3, [r4, #0x10]
	bl setSpriteAnimation
_0801441A:
	add r0, r5, r7
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	add r0, r6, #0
	add r0, #0x1c
	add r0, sb
	mov r1, #7
	strb r1, [r0]
	b def_80142C8
	.align 2, 0
.pool
_0801443C:
	mov r7, sb
	lsl r4, r7, #2
	add r0, r6, #0
	add r0, #0x38
	add r5, r0, r4
	ldr r0, [r5]
	bl updateSpriteAnimation
	add r7, r4, #0
	cmp r0, #0
	bne _08014454
	b def_80142C8
_08014454:
	ldr r0, [r5]
	ldr r1, [r6, #0x6c]
	add r1, #4
	add r1, r1, r7
	ldr r1, [r1]
	ldr r4, [r1, #8]
	ldr r1, [r4, #8]
	mov r2, sb
	lsl r3, r2, #1
	add r2, r6, #2
	add r2, r2, r3
	ldrh r2, [r2]
	ldr r3, [r4, #0x10]
	bl setSpriteAnimation
	add r1, r6, #0
	add r1, #0x1c
	add r1, sb
	ldrb r0, [r1]
	add r0, #1
	strb r0, [r1]
	ldr r0, [r6, #0x6c]
	add r0, #4
	add r0, r0, r7
	ldr r1, [r0]
	ldrh r0, [r1]
	ldrh r1, [r1, #2]
	add r1, #1
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl sub_801267C
	add r3, r0, #0
	cmp r3, #0
	beq _08014568
	ldr r0, [r6, #0x6c]
	add r1, r0, #4
	add r1, r1, r7
	ldr r1, [r1]
	ldrb r1, [r1, #0xc]
	add r5, r0, #0
	cmp r1, #5
	bhi def_80144B2
	lsl r0, r1, #2
	ldr r1, =_080144B8
	add r0, r0, r1
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
.pool
_080144B8: @ jump table
	.4byte _080144D0 @ case 0
	.4byte _080144FC @ case 1
	.4byte _080144FC @ case 2
	.4byte def_80144B2 @ case 3
	.4byte _080144FC @ case 4
	.4byte _080144FC @ case 5
_080144D0:
	mov r1, #0
	add r2, r7, #0
	add r0, r5, #4
	add r0, r0, r2
	ldr r0, [r0]
	ldrb r0, [r0, #4]
	cmp r1, r0
	bhs _08014568
	mov r4, #0xc4
_080144E2:
	strb r4, [r3]
	add r3, #1
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	ldr r0, [r6, #0x6c]
	add r0, #4
	add r0, r0, r2
	ldr r0, [r0]
	ldrb r0, [r0, #4]
	cmp r1, r0
	blo _080144E2
	b _08014568
_080144FC:
	mov r1, #0
	add r2, r7, #0
	add r0, r5, #4
	add r0, r0, r2
	ldr r0, [r0]
	ldrb r0, [r0, #4]
	cmp r1, r0
	bhs _08014568
	mov r5, #0xc3
	ldr r4, =0x030046F0
_08014510:
	strb r5, [r3]
	ldrh r0, [r4, #0x14]
	lsr r0, r0, #3
	sub r3, r3, r0
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	ldr r0, [r6, #0x6c]
	add r0, #4
	add r0, r0, r2
	ldr r0, [r0]
	ldrb r0, [r0, #4]
	cmp r1, r0
	blo _08014510
	b _08014568
	.align 2, 0
.pool
def_80144B2:
	mov r1, #0
	add r2, r7, #0
	add r0, r5, #4
	add r0, r0, r2
	ldr r0, [r0]
	ldrb r0, [r0, #4]
	cmp r1, r0
	bhs _08014568
	mov r4, #0xc4
	ldr r5, =0x030046F0
_08014548:
	strb r4, [r3]
	sub r0, r3, #1
	strb r4, [r0]
	ldrh r0, [r5, #0x14]
	lsr r0, r0, #3
	sub r3, r3, r0
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	ldr r0, [r6, #0x6c]
	add r0, #4
	add r0, r0, r2
	ldr r0, [r0]
	ldrb r0, [r0, #4]
	cmp r1, r0
	blo _08014548
_08014568:
	mov r0, #4
	bl sub_08010710
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08014576
	b def_80142C8
_08014576:
	mov r0, #0xd7
	lsl r0, r0, #1
	bl playSong
	b def_80142C8
	.align 2, 0
.pool
_08014584:
	mov r3, sb
	lsl r4, r3, #2
	add r5, r6, #0
	add r5, #0x38
	add r7, r5, r4
	mov sl, r7
	ldr r0, [r7]
	bl updateSpriteAnimation
	add r7, r4, #0
	cmp r0, #0
	bne _0801459E
	b def_80142C8
_0801459E:
	ldr r0, =0x03003D40
	ldrb r0, [r0, #0x11]
	add r1, r6, #0
	add r1, #0x1c
	str r1, [sp, #0xc]
	cmp r0, #0
	beq _080145E4
	add r1, sb
	mov r8, r1
	ldrb r0, [r1]
	cmp r0, #4
	bhi _080145E4
	mov r2, sl
	ldr r0, [r2]
	ldr r1, [r6, #0x6c]
	add r1, #4
	add r1, r1, r7
	ldr r1, [r1]
	ldr r4, [r1, #8]
	ldr r1, [r4, #8]
	mov r7, sb
	lsl r3, r7, #1
	add r2, r6, #2
	add r2, r2, r3
	ldrh r2, [r2]
	ldr r3, [r4, #0x10]
	bl setSpriteAnimation
	mov r1, r8
	ldrb r0, [r1]
	add r0, #1
	strb r0, [r1]
	b def_80142C8
	.align 2, 0
.pool
_080145E4:
	add r0, r5, r7
	ldr r0, [r0]
	ldr r1, [r6, #0x6c]
	add r1, #4
	add r1, r1, r7
	ldr r1, [r1]
	ldr r4, [r1, #8]
	ldr r1, [r4, #0xc]
	mov r2, sb
	lsl r3, r2, #1
	add r2, r6, #2
	add r2, r2, r3
	ldrh r2, [r2]
	ldr r3, [r4, #0x10]
	bl setSpriteAnimation
	ldr r1, [sp, #0xc]
	add r1, sb
	mov r0, #6
	strb r0, [r1]
	ldr r0, [r6, #0x6c]
	add r0, #4
	add r0, r0, r7
	ldr r1, [r0]
	ldrh r0, [r1]
	ldrh r1, [r1, #2]
	add r1, #1
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	bl sub_801267C
	add r3, r0, #0
	cmp r3, #0
	bne _0801462A
	b def_80142C8
_0801462A:
	ldr r0, [r6, #0x6c]
	add r1, r0, #4
	add r1, r1, r7
	ldr r1, [r1]
	ldrb r1, [r1, #0xc]
	add r5, r0, #0
	cmp r1, #5
	bhi def_8014642
	lsl r0, r1, #2
	ldr r1, =_08014648
	add r0, r0, r1
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
.pool
_08014648: @ jump table
	.4byte _08014660 @ case 0
	.4byte _0801468E @ case 1
	.4byte _0801468E @ case 2
	.4byte def_8014642 @ case 3
	.4byte _0801468E @ case 4
	.4byte _0801468E @ case 5
_08014660:
	mov r1, #0
	add r2, r7, #0
	add r0, r5, #4
	add r0, r0, r2
	ldr r0, [r0]
	ldrb r0, [r0, #4]
	cmp r1, r0
	blo _08014672
	b def_80142C8
_08014672:
	mov r4, #0
_08014674:
	strb r4, [r3]
	add r3, #1
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	ldr r0, [r6, #0x6c]
	add r0, #4
	add r0, r0, r2
	ldr r0, [r0]
	ldrb r0, [r0, #4]
	cmp r1, r0
	blo _08014674
	b def_80142C8
_0801468E:
	mov r1, #0
	add r2, r7, #0
	add r0, r5, #4
	add r0, r0, r2
	ldr r0, [r0]
	ldrb r0, [r0, #4]
	cmp r1, r0
	blo _080146A0
	b def_80142C8
_080146A0:
	mov r5, #0
	ldr r4, =0x030046F0
_080146A4:
	strb r5, [r3]
	ldrh r0, [r4, #0x14]
	lsr r0, r0, #3
	sub r3, r3, r0
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	ldr r0, [r6, #0x6c]
	add r0, #4
	add r0, r0, r2
	ldr r0, [r0]
	ldrb r0, [r0, #4]
	cmp r1, r0
	blo _080146A4
	b def_80142C8
	.align 2, 0
.pool
def_8014642:
	mov r1, #0
	add r2, r7, #0
	add r0, r5, #4
	add r0, r0, r2
	ldr r0, [r0]
	ldrb r0, [r0, #4]
	cmp r1, r0
	blo _080146DA
	b def_80142C8
_080146DA:
	mov r4, #0
	ldr r5, =0x030046F0
_080146DE:
	strb r4, [r3]
	sub r0, r3, #1
	strb r4, [r0]
	ldrh r0, [r5, #0x14]
	lsr r0, r0, #3
	sub r3, r3, r0
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	ldr r0, [r6, #0x6c]
	add r0, #4
	add r0, r0, r2
	ldr r0, [r0]
	ldrb r0, [r0, #4]
	cmp r1, r0
	blo _080146DE
	b def_80142C8
	.align 2, 0
.pool
_08014704:
	mov r3, sb
	lsl r7, r3, #2
	add r0, r6, #0
	add r0, #0x38
	add r5, r0, r7
	ldr r0, [r5]
	bl updateSpriteAnimation
	cmp r0, #0
	bne _0801471A
	b def_80142C8
_0801471A:
	ldr r0, [r5]
	ldr r1, [r6, #0x6c]
	add r1, #4
	add r1, r1, r7
	ldr r1, [r1]
	ldr r4, [r1, #8]
	ldr r1, [r4]
	mov r2, sb
	lsl r3, r2, #1
	add r2, r6, #2
	add r2, r2, r3
	mov r8, r2
	ldrh r2, [r2]
	ldr r3, [r4, #0x10]
	bl setSpriteAnimation
	ldr r1, [r5]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	ldr r0, [r6, #0x6c]
	add r0, #4
	add r0, r0, r7
	ldr r0, [r0]
	ldr r1, [r0, #8]
	ldr r4, [r1, #0x14]
	cmp r4, #0
	beq _0801477E
	ldr r0, [r5]
	mov r3, r8
	ldrh r2, [r3]
	ldr r3, [r1, #0x10]
	add r1, r4, #0
	bl setSpriteAnimation
	ldr r1, [r5]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	add r0, r6, #0
	add r0, #0x1c
	add r0, sb
	mov r1, #1
	strb r1, [r0]
	b _0801478C
_0801477E:
	ldr r0, [r5]
	bl spriteFreeHeader
	add r0, r6, #0
	add r0, #0x1c
	add r0, sb
	strb r4, [r0]
_0801478C:
	bl rand
	mov r1, #8
	bl Bios_modulo
	add r1, r6, #0
	add r1, #0x29
	add r1, sb
	ldr r2, =dword_80EF304
	lsl r0, r0, #1
	add r0, r0, r2
	ldrh r0, [r0]
	strb r0, [r1]
	b def_80142C8
	.align 2, 0
.pool
_080147AC:
	mov r4, sb
	lsl r7, r4, #2
	add r0, r6, #0
	add r0, #0x38
	add r4, r0, r7
	ldr r0, [r4]
	bl updateSpriteAnimation
	cmp r0, #0
	beq def_80142C8
	ldr r0, =0x03003D40
	ldrb r0, [r0, #0x11]
	cmp r0, #2
	beq _080147D6
	add r0, r6, #0
	add r0, #0x1c
	mov r1, sb
	add r5, r0, r1
	ldrb r0, [r5]
	cmp r0, #8
	bne _08014804
_080147D6:
	ldr r0, [r4]
	ldr r1, [r6, #0x6c]
	add r1, #4
	add r1, r1, r7
	ldr r1, [r1]
	ldr r4, [r1, #8]
	ldr r1, [r4, #4]
	mov r2, sb
	lsl r3, r2, #1
	add r2, r6, #2
	add r2, r2, r3
	ldrh r2, [r2]
	ldr r3, [r4, #0x10]
	bl setSpriteAnimation
	add r0, r6, #0
	add r0, #0x1c
	add r0, sb
	mov r1, #2
	strb r1, [r0]
	b def_80142C8
	.align 2, 0
.pool
_08014804:
	ldr r0, [r4]
	ldr r1, [r6, #0x6c]
	add r1, #4
	add r1, r1, r7
	ldr r1, [r1]
	ldr r4, [r1, #8]
	ldr r1, [r4]
	mov r7, sb
	lsl r3, r7, #1
	add r2, r6, #2
	add r2, r2, r3
	ldrh r2, [r2]
	ldr r3, [r4, #0x10]
	bl setSpriteAnimation
	ldrb r0, [r5]
	add r0, #1
	strb r0, [r5]
def_80142C8:
	ldr r1, [r6, #0x6c]
	mov r0, sb
	lsl r2, r0, #2
	add r0, r1, #4
	add r0, r0, r2
	ldr r0, [r0]
	ldr r0, [r0, #8]
	ldr r0, [r0, #0x14]
	add r5, r1, #0
	add r7, r2, #0
	cmp r0, #0
	bne _0801484E
	add r0, r6, #0
	add r0, #0x1c
	add r0, sb
	ldrb r0, [r0]
	cmp r0, #1
	bhi _0801485A
	b _080148DC
_0801484E:
	add r0, r6, #0
	add r0, #0x1c
	add r0, sb
	ldrb r0, [r0]
	cmp r0, #0
	beq _080148DC
_0801485A:
	add r0, r5, #4
	add r0, r0, r7
	ldr r0, [r0]
	ldrb r0, [r0, #4]
	lsl r0, r0, #3
	ldr r1, [sp, #4]
	cmn r1, r0
	blt _0801486E
	cmp r1, #0xf0
	ble _08014888
_0801486E:
	add r3, r6, #0
	add r3, #0x38
	add r1, r3, r7
	ldr r0, [r1]
	mov r2, sp
	ldrh r2, [r2, #4]
	strh r2, [r0, #0x10]
	ldr r1, [r1]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	b _080148A2
_08014888:
	add r3, r6, #0
	add r3, #0x38
	add r1, r3, r7
	ldr r0, [r1]
	mov r4, sp
	ldrh r4, [r4, #4]
	strh r4, [r0, #0x10]
	ldr r1, [r1]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
_080148A2:
	strb r0, [r1]
	add r5, r3, #0
	ldr r0, [sp, #8]
	add r0, #0x20
	cmp r0, #0xc0
	bls _080148C4
	add r1, r5, r7
	ldr r0, [r1]
	mov r7, sp
	ldrh r7, [r7, #8]
	strh r7, [r0, #0x12]
	ldr r1, [r1]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	b _080148DA
_080148C4:
	add r1, r5, r7
	ldr r0, [r1]
	mov r2, sp
	ldrh r2, [r2, #8]
	strh r2, [r0, #0x12]
	ldr r1, [r1]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
_080148DA:
	strb r0, [r1]
_080148DC:
	add sp, #0x10
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	thumb_func_global levelfunc_cave0
levelfunc_cave0: @ 0x080148EC
	push {r4, lr}
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	cmp r0, #1
	bne _0801490C
	ldr r0, =off_80798B0
	mov r1, #0xa0
	lsl r1, r1, #0x13
	mov r2, #0
	bl startPalAnimation
	b _0801493C
	.align 2, 0
.pool
_0801490C:
	ldrb r1, [r1, #2]
	ldr r0, =stageCount
	ldrh r0, [r0, #6]
	sub r0, #1
	cmp r1, r0
	bne _08014924
	ldr r0, =off_80BA104
	mov r1, #0xa0
	lsl r1, r1, #0x13
	mov r2, #0
	bl startPalAnimation
_08014924:
	ldr r0, =off_80BA380
	mov r4, #0xa0
	lsl r4, r4, #0x13
	add r1, r4, #0
	mov r2, #0
	bl startPalAnimation
	ldr r0, =off_80BA1F4
	add r1, r4, #0
	mov r2, #0
	bl startPalAnimation
_0801493C:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global levelfunc_cave01
levelfunc_cave01: @ 0x08014954
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	sub sp, #8
	ldr r0, =0x0201FF50
	mov r8, r0
	ldr r1, =0x03003D40
	ldrb r0, [r1, #3]
	cmp r0, #0
	bne _08014970
	ldrb r0, [r1, #4]
	cmp r0, #0
	beq _08014976
_08014970:
	ldrb r1, [r1, #0x12]
	cmp r1, #0
	beq _08014994
_08014976:
	add r0, sp, #4
	mov r1, #0
	strh r1, [r0]
	ldr r2, =0x01000022
	mov r1, r8
	bl Bios_memcpy
	b _080149A8
	.align 2, 0
.pool
_08014994:
	mov r3, r8
	ldrh r4, [r3]
	add r0, sp, #4
	strh r1, [r0]
	ldr r2, =0x01000022
	mov r1, r8
	bl Bios_memcpy
	mov r5, r8
	strh r4, [r5]
_080149A8:
	ldr r3, =0x03003D40
	ldrb r0, [r3, #1]
	cmp r0, #3
	beq _080149B2
	b _08014B66
_080149B2:
	ldrb r2, [r3, #2]
	ldr r0, =stageCount
	ldrb r1, [r3, #1]
	lsl r1, r1, #1
	add r1, r1, r0
	ldrh r0, [r1]
	sub r0, #1
	cmp r2, r0
	blt _080149C6
	b _08014B66
_080149C6:
	cmp r2, #1
	beq _080149FC
	cmp r2, #1
	bgt _080149E0
	cmp r2, #0
	beq _080149E6
	b _08014A1E
	.align 2, 0
.pool
_080149E0:
	cmp r2, #2
	beq _08014A10
	b _08014A1E
_080149E6:
	ldr r0, =off_80F26AC
	ldrb r1, [r3, #3]
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r0, [r1]
	mov r1, r8
	str r0, [r1, #0x40]
	b _08014A1E
	.align 2, 0
.pool
_080149FC:
	ldr r0, =off_80F2720
	ldrb r1, [r3, #3]
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r0, [r1]
	mov r3, r8
	str r0, [r3, #0x40]
	b _08014A1E
	.align 2, 0
.pool
_08014A10:
	ldr r0, =off_80F27A4
	ldrb r1, [r3, #3]
	lsl r1, r1, #2
	add r1, r1, r0
	ldr r0, [r1]
	mov r5, r8
	str r0, [r5, #0x40]
_08014A1E:
	ldr r0, =dword_80F0198
	mov r1, #2
	bl LoadSpritePalette
	mov r1, r8
	strh r0, [r1, #2]
	lsl r0, r0, #0x10
	cmp r0, #0
	bge _08014A32
	b _08014B66
_08014A32:
	mov r7, #0
	ldr r0, [r1, #0x40]
	ldrb r0, [r0]
	cmp r7, r0
	blo _08014A3E
	b _08014B5E
_08014A3E:
	mov r3, #1
	mov sb, r3
_08014A42:
	mov r5, r8
	ldrh r2, [r5]
	ldr r0, =0x03003D40
	ldrb r1, [r0, #3]
	lsl r0, r1, #1
	add r0, r0, r1
	add r0, r7, r0
	asr r2, r0
	mov r0, sb
	and r2, r0
	cmp r2, #0
	beq _08014A74
	lsl r0, r7, #2
	add r0, r0, r7
	lsl r0, r0, #2
	add r0, r8
	mov r1, #0
	strb r1, [r0, #4]
	b _08014B4C
	.align 2, 0
.pool
_08014A74:
	lsl r6, r7, #2
	add r5, r6, r7
	lsl r5, r5, #2
	mov r1, r8
	add r4, r1, r5
	mov r3, sb
	strb r3, [r4, #4]
	mov r0, #0x14
	bl allocateSpriteTiles
	strh r0, [r4, #6]
	ldr r1, =off_80F27C4
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r2, [r0]
	lsl r2, r2, #2
	add r1, r2, r1
	ldr r0, [r1]
	ldr r1, =off_80F27B8
	add r2, r2, r1
	ldr r1, [r2]
	ldrh r2, [r4, #6]
	mov r3, #0xff
	str r3, [sp]
	mov r3, #2
	bl allocSprite
	mov r1, r8
	add r1, #0xc
	add r1, r1, r5
	str r0, [r1]
	add r4, r0, #0
	mov r5, r8
	ldrh r0, [r5, #2]
	add r1, r4, #0
	add r1, #0x22
	strb r0, [r1]
	ldr r0, [r5, #0x40]
	add r0, #4
	add r0, r0, r6
	ldr r3, [r0]
	ldrh r2, [r3]
	ldr r1, =0x03004720
	ldrh r0, [r1]
	sub r2, r2, r0
	ldrh r0, [r1, #2]
	ldrh r1, [r3, #2]
	sub r0, r0, r1
	add r3, r0, #1
	add r1, r2, #0
	add r1, #0x20
	mov r0, #0x88
	lsl r0, r0, #1
	cmp r1, r0
	bls _08014B00
	strh r2, [r4, #0x10]
	add r2, r4, #0
	add r2, #0x23
	ldrb r0, [r2]
	mov r1, #0x10
	orr r0, r1
	b _08014B10
	.align 2, 0
.pool
_08014B00:
	strh r2, [r4, #0x10]
	add r2, r4, #0
	add r2, #0x23
	ldrb r0, [r2]
	mov r5, #0x11
	neg r5, r5
	add r1, r5, #0
	and r0, r1
_08014B10:
	strb r0, [r2]
	add r0, r3, #0
	add r0, #0x20
	cmp r0, #0xc0
	bls _08014B28
	strh r3, [r4, #0x12]
	add r0, r4, #0
	add r0, #0x23
	ldrb r1, [r0]
	mov r2, #0x10
	orr r1, r2
	b _08014B38
_08014B28:
	strh r3, [r4, #0x12]
	add r0, r4, #0
	add r0, #0x23
	ldrb r1, [r0]
	mov r3, #0x11
	neg r3, r3
	add r2, r3, #0
	and r1, r2
_08014B38:
	strb r1, [r0]
	ldr r0, =0x03003D40
	ldrb r1, [r0, #2]
	cmp r1, #0
	bne _08014B4C
	add r0, r6, r7
	lsl r0, r0, #2
	add r0, r8
	strh r1, [r0, #0x14]
	strh r1, [r0, #0x16]
_08014B4C:
	add r0, r7, #1
	lsl r0, r0, #0x10
	lsr r7, r0, #0x10
	mov r5, r8
	ldr r0, [r5, #0x40]
	ldrb r0, [r0]
	cmp r7, r0
	bhs _08014B5E
	b _08014A42
_08014B5E:
	ldr r0, =(sub_8014B7C+1)
	mov r1, #0xfe
	bl createTask
_08014B66:
	add sp, #8
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_8014B7C
sub_8014B7C: @ 0x08014B7C
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #4
	ldr r7, =0x0201FF50
	mov r1, #2
	ldrsh r0, [r7, r1]
	cmp r0, #0
	bge _08014B9E
	ldr r0, =dword_80F0198
	mov r1, #2
	bl LoadSpritePalette
	strh r0, [r7, #2]
	lsl r0, r0, #0x10
	cmp r0, #0
	blt _08014C16
_08014B9E:
	mov r6, #0
	ldr r0, [r7, #0x40]
	ldrb r0, [r0]
	cmp r6, r0
	bhs _08014C16
	ldr r0, =off_80F27C4
	mov r8, r0
_08014BAC:
	lsl r0, r6, #2
	add r0, r0, r6
	lsl r5, r0, #2
	add r4, r7, r5
	ldrb r0, [r4, #4]
	cmp r0, #0
	beq _08014C08
	mov r1, #6
	ldrsh r0, [r4, r1]
	cmp r0, #0
	bge _08014BD0
	mov r0, #0x14
	bl allocateSpriteTiles
	strh r0, [r4, #6]
	lsl r0, r0, #0x10
	cmp r0, #0
	blt _08014C08
_08014BD0:
	add r0, r7, #0
	add r0, #0xc
	add r5, r0, r5
	ldr r0, [r5]
	cmp r0, #0
	bne _08014C02
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r2, [r0]
	lsl r2, r2, #2
	mov r1, r8
	add r0, r2, r1
	ldr r0, [r0]
	ldr r1, =off_80F27B8
	add r2, r2, r1
	ldr r1, [r2]
	ldrh r2, [r4, #6]
	mov r3, #0xff
	str r3, [sp]
	mov r3, #2
	bl allocSprite
	str r0, [r5]
	cmp r0, #0
	beq _08014C08
_08014C02:
	add r0, r6, #0
	bl sub_8014C38
_08014C08:
	add r0, r6, #1
	lsl r0, r0, #0x10
	lsr r6, r0, #0x10
	ldr r0, [r7, #0x40]
	ldrb r0, [r0]
	cmp r6, r0
	blo _08014BAC
_08014C16:
	add sp, #4
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_8014C38
sub_8014C38: @ 0x08014C38
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #8
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov sb, r0
	ldr r0, =0x0201FF50
	mov sl, r0
	mov r1, sb
	lsl r0, r1, #2
	add r0, sb
	lsl r3, r0, #2
	mov r0, sl
	add r0, #0xc
	add r0, r3, r0
	ldr r6, [r0]
	mov r0, sl
	add r7, r3, r0
	ldrb r5, [r7, #8]
	cmp r5, #1
	beq _08014CB8
	cmp r5, #1
	bgt _08014C78
	cmp r5, #0
	beq _08014C7E
	b _08014D16
	.align 2, 0
.pool
_08014C78:
	cmp r5, #2
	beq _08014D00
	b _08014D16
_08014C7E:
	mov r1, sl
	ldrh r2, [r1]
	ldr r4, =0x03003D40
	ldrb r1, [r4, #3]
	lsl r0, r1, #1
	add r0, r0, r1
	add r0, sb
	asr r2, r0
	mov r0, #1
	and r2, r0
	cmp r2, #0
	beq _08014CAA
	strb r0, [r7, #8]
	ldrb r0, [r4, #2]
	cmp r0, #0
	bne _08014CAA
	mov r0, sl
	add r0, #0x10
	add r0, r3, r0
	ldr r0, [r0]
	bl spriteFreeHeader
_08014CAA:
	add r0, r6, #0
	bl updateSpriteAnimation
	b _08014D16
	.align 2, 0
.pool
_08014CB8:
	ldr r4, =off_80F27D0
	bl getHword3001F7A
	and r5, r0
	lsl r1, r5, #2
	ldr r0, =0x03002080
	add r0, #0x2c
	ldrb r3, [r0]
	lsl r0, r3, #3
	add r1, r1, r0
	add r1, r1, r4
	ldr r1, [r1]
	ldrh r2, [r7, #6]
	ldr r0, =off_80F27B8
	lsl r3, r3, #2
	add r3, r3, r0
	ldr r3, [r3]
	add r0, r6, #0
	bl setSpriteAnimation
	ldrb r0, [r7, #8]
	add r0, #1
	strb r0, [r7, #8]
	ldr r0, =0x00000141
	bl playSong
	b _08014D16
	.align 2, 0
.pool
_08014D00:
	add r0, r6, #0
	bl updateSpriteAnimation
	cmp r0, #0
	beq _08014D16
	add r0, r6, #0
	bl freeSprite
	ldrb r0, [r7, #8]
	add r0, #1
	strb r0, [r7, #8]
_08014D16:
	mov r1, sl
	ldr r0, [r1, #0x40]
	mov r1, sb
	lsl r3, r1, #2
	add r0, #4
	add r0, r0, r3
	ldr r4, [r0]
	ldrh r2, [r4]
	ldr r1, =0x03004720
	ldrh r0, [r1]
	sub r2, r2, r0
	mov r8, r2
	ldrh r0, [r1, #2]
	ldrh r1, [r4, #2]
	sub r0, r0, r1
	add r0, #1
	str r0, [sp, #4]
	mov r1, sb
	add r0, r3, r1
	lsl r0, r0, #2
	add r0, sl
	ldrb r0, [r0, #8]
	cmp r0, #2
	bhi _08014DA2
	mov r1, r8
	add r1, #0x20
	mov r0, #0x88
	lsl r0, r0, #1
	cmp r1, r0
	bls _08014D68
	mov r0, r8
	strh r0, [r6, #0x10]
	add r0, r6, #0
	add r0, #0x23
	ldrb r1, [r0]
	mov r2, #0x10
	orr r1, r2
	b _08014D78
	.align 2, 0
.pool
_08014D68:
	mov r1, r8
	strh r1, [r6, #0x10]
	add r0, r6, #0
	add r0, #0x23
	ldrb r2, [r0]
	mov r1, #0x11
	neg r1, r1
	and r1, r2
_08014D78:
	strb r1, [r0]
	add r2, r0, #0
	ldr r0, [sp, #4]
	add r0, #0x20
	cmp r0, #0xc0
	bls _08014D92
	mov r0, sp
	ldrh r0, [r0, #4]
	strh r0, [r6, #0x12]
	ldrb r0, [r2]
	mov r1, #0x10
	orr r0, r1
	b _08014DA0
_08014D92:
	mov r1, sp
	ldrh r1, [r1, #4]
	strh r1, [r6, #0x12]
	ldrb r1, [r2]
	mov r0, #0x11
	neg r0, r0
	and r0, r1
_08014DA0:
	strb r0, [r2]
_08014DA2:
	ldr r0, =0x03003D40
	ldrb r0, [r0, #2]
	cmp r0, #0
	bne _08014E94
	mov r1, sb
	add r0, r3, r1
	lsl r6, r0, #2
	mov r0, sl
	add r5, r0, r6
	ldrb r0, [r5, #8]
	cmp r0, #0
	bne _08014E94
	ldrh r7, [r5, #0x14]
	cmp r7, #0
	beq _08014DCC
	cmp r7, #1
	beq _08014E20
	b _08014E94
	.align 2, 0
.pool
_08014DCC:
	ldr r0, =doorArrowAnimation
	ldr r4, =0x03006C00
	ldr r1, [r4, #4]
	ldrh r2, [r1, #0xc]
	mov r1, #0xfe
	str r1, [sp]
	mov r1, #0
	mov r3, #2
	bl allocSprite
	mov r1, sl
	add r1, #0x10
	add r1, r1, r6
	str r0, [r1]
	add r6, r0, #0
	ldr r0, [r4, #4]
	add r0, #0x22
	ldrb r0, [r0]
	add r1, r6, #0
	add r1, #0x22
	strb r0, [r1]
	mov r1, r8
	strh r1, [r6, #0x10]
	ldr r0, [sp, #4]
	add r0, #6
	strh r0, [r6, #0x12]
	add r2, r6, #0
	add r2, #0x23
	ldrb r0, [r2]
	mov r1, #0x10
	orr r0, r1
	strb r0, [r2]
	ldrh r0, [r5, #0x14]
	add r0, #1
	strh r0, [r5, #0x14]
	strh r7, [r5, #0x16]
	b _08014E94
	.align 2, 0
.pool
_08014E20:
	ldrh r0, [r5, #0x16]
	add r0, #1
	mov r1, #0xc0
	bl Bios_modulo
	strh r0, [r5, #0x16]
	mov r0, sl
	add r0, #0xc
	add r0, r0, r6
	ldr r0, [r0]
	add r0, #0x23
	ldrb r1, [r0]
	mov r0, #0x10
	and r0, r1
	cmp r0, #0
	beq _08014E54
	mov r0, sl
	add r0, #0x10
	add r0, r0, r6
	ldr r1, [r0]
	add r1, #0x23
	ldrb r0, [r1]
	mov r2, #0x10
	orr r0, r2
	strb r0, [r1]
	b _08014E94
_08014E54:
	mov r0, sl
	add r0, #0x10
	add r0, r0, r6
	ldr r6, [r0]
	ldr r1, =dword_80F27E8
	ldrh r0, [r5, #0x16]
	add r0, r0, r1
	ldrb r0, [r0]
	add r3, r6, #0
	add r3, #0x23
	and r7, r0
	lsl r2, r7, #4
	ldrb r1, [r3]
	mov r0, #0x11
	neg r0, r0
	and r0, r1
	orr r0, r2
	strb r0, [r3]
	mov r0, r8
	strh r0, [r6, #0x10]
	ldr r2, =doorArrowAnimation_0
	ldr r0, =0x03002080
	ldr r0, [r0]
	mov r1, #0x1f
	and r0, r1
	lsl r0, r0, #1
	add r0, r0, r2
	ldrh r0, [r0]
	add r0, #6
	ldr r1, [sp, #4]
	add r0, r1, r0
	strh r0, [r6, #0x12]
_08014E94:
	add sp, #8
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool






thumb_func_global sub_08014EB0
sub_08014EB0: @ 0x08014EB0
	push {r4, lr}
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	ldr r4, =0x0201FF50
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	cmp r0, #3
	bne _08014EE2
	ldrh r2, [r4]
	ldrb r0, [r1, #3]
	lsl r1, r0, #1
	add r1, r1, r0
	add r1, r3, r1
	add r0, r2, #0
	asr r0, r1
	mov r3, #1
	and r0, r3
	cmp r0, #0
	bne _08014EE2
	add r0, r3, #0
	lsl r0, r1
	orr r2, r0
	strh r2, [r4]
	bl saveHostage
_08014EE2:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global levelfunc_base0
levelfunc_base0: @ 0x08014EF0
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #4
	ldr r4, =0x0201FF50
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r2, =0x0100001A
	mov r0, sp
	add r1, r4, #0
	bl Bios_memcpy
	mov r0, #0xd2
	lsl r0, r0, #1
	strh r0, [r4, #2]
	mov r1, #0
	mov r6, sp
	add r6, #2
	ldr r0, =word_80F3A80
	mov r8, r0
_08014F1A:
	mov r4, #0
	lsl r0, r1, #1
	add r7, r1, #1
	add r0, r0, r1
	lsl r5, r0, #2
_08014F24:
	mov r0, #0
	strh r0, [r6]
	lsl r0, r4, #1
	add r0, r0, r5
	add r0, r8
	ldrh r1, [r0]
	lsl r1, r1, #5
	mov r0, #0xc0
	lsl r0, r0, #0x13
	add r1, r1, r0
	add r0, r6, #0
	ldr r2, =0x01000010
	bl Bios_memcpy
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #5
	bls _08014F24
	lsl r0, r7, #0x10
	lsr r1, r0, #0x10
	cmp r1, #4
	bls _08014F1A
	ldr r0, =(sub_8014F7C+1)
	mov r1, #0xfe
	bl createTask
	add sp, #4
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_8014F7C
sub_8014F7C: @ 0x08014F7C
	push {r4, r5, lr}
	ldr r5, =0x0201FF50
	mov r4, #0
_08014F82:
	add r0, r4, #0
	add r1, r5, #0
	bl sub_8014FC4
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #4
	bls _08014F82
	add r0, r5, #0
	bl sub_8015398
	ldrh r0, [r5, #2]
	add r0, #1
	ldr r2, =dword_80F47C4
	ldr r1, =0x03003D40
	ldrb r1, [r1, #0x11]
	lsl r1, r1, #1
	add r1, r1, r2
	ldrh r1, [r1]
	bl Bios_modulo
	strh r0, [r5, #2]
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool




	thumb_func_global sub_8014FC4
sub_8014FC4: @ 0x08014FC4
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #4
	add r4, r1, #0
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	add r0, r4, #4
	add r0, r0, r5
	ldrb r0, [r0]
	cmp r0, #4
	bls _08014FDE
	b def_8014FE6
_08014FDE:
	lsl r0, r0, #2
	ldr r1, =_08014FEC
	add r0, r0, r1
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
.pool
_08014FEC: @ jump table
	.4byte _08015000 @ case 0
	.4byte _0801505C @ case 1
	.4byte _080150BC @ case 2
	.4byte _08015148 @ case 3
	.4byte _080151B8 @ case 4
_08015000:
	bl sub_8032F80
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, #0
	beq _0801500E
	b def_8014FE6
_0801500E:
	ldr r2, =dword_80F474C
	ldr r0, =0x03003D40
	ldrb r1, [r0, #0x11]
	lsl r0, r1, #2
	add r0, r0, r1
	add r0, r0, r5
	lsl r0, r0, #3
	add r0, r0, r2
	ldrh r1, [r4, #2]
	ldrh r0, [r0]
	cmp r1, r0
	beq _08015028
	b def_8014FE6
_08015028:
	lsl r1, r5, #1
	add r0, r4, #0
	add r0, #0x14
	add r0, r0, r1
	strh r3, [r0]
	add r0, r4, #0
	add r0, #0xa
	add r0, r0, r1
	strh r3, [r0]
	lsl r1, r5, #2
	add r0, r4, #0
	add r0, #0x20
	add r0, r0, r1
	ldr r1, =dword_80F39E8
	str r1, [r0]
	add r1, r4, #4
	add r1, r1, r5
	ldrb r0, [r1]
	add r0, #1
	b _0801521A
	.align 2, 0
.pool
_0801505C:
	add r0, r5, #0
	add r1, r4, #0
	bl sub_801523C
	bl sub_8032F80
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08015080
	add r1, r4, #4
	add r1, r1, r5
	ldrb r0, [r1]
	add r0, #3
	mov r2, #0
	strb r0, [r1]
	mov r1, #0x80
	lsl r1, r1, #1
	b _08015182
_08015080:
	ldrh r3, [r4, #2]
	ldr r2, =dword_80F474C
	ldr r0, =0x03003D40
	ldrb r1, [r0, #0x11]
	lsl r0, r1, #2
	add r0, r0, r1
	add r0, r0, r5
	lsl r0, r0, #3
	add r0, r0, r2
	ldrh r0, [r0]
	add r0, #0xa
	cmp r3, r0
	beq _0801509C
	b def_8014FE6
_0801509C:
	add r1, r4, #4
	add r1, r1, r5
	ldrb r0, [r1]
	add r0, #1
	strb r0, [r1]
	mov r0, #0x80
	lsl r0, r0, #1
	lsl r0, r5
	ldrh r1, [r4]
	orr r0, r1
	strh r0, [r4]
	b def_8014FE6
	.align 2, 0
.pool
_080150BC:
	add r0, r5, #0
	add r1, r4, #0
	bl sub_801523C
	bl sub_8032F80
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, #0
	beq _080150E2
	add r1, r4, #4
	add r1, r1, r5
	ldrb r0, [r1]
	add r0, #2
	mov r2, #0
	strb r0, [r1]
	mov r1, #0x80
	lsl r1, r1, #1
	b _08015182
_080150E2:
	ldr r2, =dword_80F474C
	ldr r0, =0x03003D40
	ldrb r1, [r0, #0x11]
	lsl r0, r1, #2
	add r0, r0, r1
	add r0, r0, r5
	lsl r0, r0, #3
	add r2, #2
	add r0, r0, r2
	ldrh r1, [r4, #2]
	ldrh r0, [r0]
	cmp r1, r0
	beq _080150FE
	b def_8014FE6
_080150FE:
	add r1, r4, #4
	add r1, r1, r5
	ldrb r0, [r1]
	add r0, #1
	strb r0, [r1]
	mov r0, #0x80
	lsl r0, r0, #1
	lsl r0, r5
	ldrh r1, [r4]
	bic r1, r0
	mov r0, #1
	lsl r0, r5
	orr r1, r0
	strh r1, [r4]
	lsl r1, r5, #1
	add r0, r4, #0
	add r0, #0x14
	add r0, r0, r1
	strh r3, [r0]
	add r0, r4, #0
	add r0, #0xa
	add r0, r0, r1
	strh r3, [r0]
	lsl r1, r5, #2
	add r0, r4, #0
	add r0, #0x20
	add r0, r0, r1
	ldr r1, =dword_80F3A20
	str r1, [r0]
	b def_8014FE6
	.align 2, 0
.pool
_08015148:
	add r0, r5, #0
	add r1, r4, #0
	bl sub_801523C
	bl sub_8032F80
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _08015174
	ldr r2, =dword_80F474C
	ldr r0, =0x03003D40
	ldrb r1, [r0, #0x11]
	lsl r0, r1, #2
	add r0, r0, r1
	add r0, r0, r5
	lsl r0, r0, #3
	add r2, #4
	add r0, r0, r2
	ldrh r1, [r4, #2]
	ldrh r0, [r0]
	cmp r1, r0
	bne def_8014FE6
_08015174:
	add r1, r4, #4
	add r1, r1, r5
	ldrb r0, [r1]
	add r0, #1
	mov r2, #0
	strb r0, [r1]
	mov r1, #1
_08015182:
	lsl r1, r5
	ldrh r0, [r4]
	bic r0, r1
	strh r0, [r4]
	lsl r1, r5, #1
	add r0, r4, #0
	add r0, #0x14
	add r0, r0, r1
	strh r2, [r0]
	add r0, r4, #0
	add r0, #0xa
	add r0, r0, r1
	strh r2, [r0]
	lsl r1, r5, #2
	add r0, r4, #0
	add r0, #0x20
	add r0, r0, r1
	ldr r1, =dword_80F3A48
	str r1, [r0]
	b def_8014FE6
	.align 2, 0
.pool
_080151B8:
	add r0, r5, #0
	add r1, r4, #0
	bl sub_801523C
	ldr r2, =dword_80F474C
	ldr r0, =0x03003D40
	ldrb r1, [r0, #0x11]
	lsl r0, r1, #2
	add r0, r0, r1
	add r0, r0, r5
	lsl r0, r0, #3
	add r2, #6
	add r0, r0, r2
	ldrh r1, [r4, #2]
	ldrh r0, [r0]
	cmp r1, r0
	bne def_8014FE6
	mov r3, #0
	add r7, r4, #4
	lsl r0, r5, #1
	mov r8, r3
	ldr r2, =0x040000D4
	ldr r1, =word_80F3A80
	mov ip, r1
	add r0, r0, r5
	lsl r4, r0, #2
	mov r6, #0xc0
	lsl r6, r6, #0x13
	ldr r1, =0x85000008
_080151F2:
	mov r0, r8
	str r0, [sp]
	mov r0, sp
	str r0, [r2]
	lsl r0, r3, #1
	add r0, r0, r4
	add r0, ip
	ldrh r0, [r0]
	lsl r0, r0, #5
	add r0, r0, r6
	str r0, [r2, #4]
	str r1, [r2, #8]
	ldr r0, [r2, #8]
	add r0, r3, #1
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	cmp r3, #5
	bls _080151F2
	add r1, r7, r5
	mov r0, #0
_0801521A:
	strb r0, [r1]
def_8014FE6:
	add sp, #4
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool



	thumb_func_global sub_801523C
sub_801523C: @ 0x0801523C
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r8, r0
	lsl r2, r0, #2
	add r0, r1, #0
	add r0, #0x20
	add r0, r0, r2
	mov ip, r0
	ldr r2, [r0]
	mov r0, r8
	lsl r6, r0, #1
	add r0, r1, #0
	add r0, #0x14
	add r4, r0, r6
	ldrh r0, [r4]
	lsl r0, r0, #3
	add r2, r2, r0
	add r1, #0xa
	add r3, r1, r6
	ldrh r0, [r3]
	add r0, #1
	mov r5, #0
	strh r0, [r3]
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r7, #0
	ldrsh r1, [r2, r7]
	cmp r0, r1
	blt _08015286
	add r2, #8
	ldrh r0, [r4]
	add r0, #1
	strh r0, [r4]
	strh r5, [r3]
_08015286:
	mov r1, #0
	ldrsh r0, [r2, r1]
	cmp r0, #0
	beq _080152E2
	cmp r0, #0
	bge _0801529A
	mov r7, ip
	ldr r2, [r7]
	strh r5, [r4]
	strh r5, [r3]
_0801529A:
	mov r5, #0
	ldr r3, [r2, #4]
	ldrh r0, [r3]
	cmp r5, r0
	bhs _080152E2
	ldr r4, =0x040000D4
	ldr r1, =word_80F3A80
	mov ip, r1
	mov r7, r8
	add r0, r6, r7
	lsl r6, r0, #2
_080152B0:
	lsl r2, r5, #1
	add r0, r3, #2
	add r0, r0, r2
	ldrh r1, [r0]
	lsl r1, r1, #5
	ldr r0, [r3, #0x10]
	add r0, r0, r1
	str r0, [r4]
	add r2, r2, r6
	add r2, ip
	ldrh r0, [r2]
	lsl r0, r0, #5
	mov r1, #0xc0
	lsl r1, r1, #0x13
	add r0, r0, r1
	str r0, [r4, #4]
	ldr r0, =0x84000008
	str r0, [r4, #8]
	ldr r0, [r4, #8]
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldrh r0, [r3]
	cmp r5, r0
	blo _080152B0
_080152E2:
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool




thumb_func_global sub_080152F8
sub_080152F8: @ 0x080152F8
	push {r4, lr}
	lsl r0, r0, #0x10
	lsr r3, r0, #0x10
	ldr r1, =0x03003D40
	ldrb r2, [r1, #2]
	ldr r0, =stageCount
	ldrh r0, [r0, #8]
	sub r0, #2
	add r4, r1, #0
	cmp r2, r0
	ble _0801532C
	ldr r0, =0x03004720
	ldrh r0, [r0, #8]
	cmp r0, #7
	beq _08015390
	ldr r0, =0x0201FF50
	add r0, #0x18
	b _08015392
	.align 2, 0
.pool
_0801532C:
	ldrb r0, [r4, #1]
	ldr r0, =0x0201FF50
	ldrh r2, [r0]
	add r0, r2, #0
	asr r0, r3
	mov r1, #1
	and r0, r1
	cmp r0, #0
	beq _0801535C
	ldrb r0, [r4, #1]
	cmp r0, #5
	bne _08015354
	ldr r0, =off_80F40FC
	ldr r1, [r0, #0xc]
	b _08015382
	.align 2, 0
.pool
_08015354:
	ldr r1, =off_80F40FC
	b _0801537A
	.align 2, 0
.pool
_0801535C:
	mov r0, #0x80
	lsl r0, r0, #1
	lsl r0, r3
	and r2, r0
	cmp r2, #0
	beq _08015390
	ldrb r0, [r4, #1]
	cmp r0, #5
	bne _08015378
	ldr r0, =off_80F473C
	ldr r1, [r0, #0xc]
	b _08015382
	.align 2, 0
.pool
_08015378:
	ldr r1, =off_80F473C
_0801537A:
	ldrb r0, [r4, #2]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r1, [r0]
_08015382:
	lsl r0, r3, #2
	add r0, r0, r1
	ldr r0, [r0]
	b _08015392
	.align 2, 0
.pool
_08015390:
	mov r0, #0
_08015392:
	pop {r4}
	pop {r1}
	bx r1
	
	
	
	
	
	thumb_func_global sub_8015398
	sub_8015398:
thumb_func_global sub_8015398
sub_8015398: @ 0x08015398
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #4
	add r6, r0, #0
	ldr r1, =0x03003D40
	ldrb r2, [r1, #2]
	ldr r0, =stageCount
	ldrh r0, [r0, #8]
	sub r0, #2
	mov sl, r1
	cmp r2, r0
	ble _080153B8
	b _080154EE
_080153B8:
	mov r0, #0
	mov sb, r0
	mov r5, #0
	ldrh r1, [r6]
	str r1, [sp]
	ldr r7, =off_80F40FC
	mov r8, r7
	ldr r0, =0x0000FFFF
	mov ip, r0
_080153CA:
	ldr r0, =0x00000101
	lsl r0, r5
	ldr r1, [sp]
	and r0, r1
	cmp r0, #0
	beq _0801548E
	mov r7, sl
	ldrb r0, [r7, #1]
	cmp r0, #5
	bne _080153F8
	mov r0, r8
	ldr r1, [r0, #0xc]
	b _08015402
	.align 2, 0
.pool
_080153F8:
	mov r1, sl
	ldrb r0, [r1, #2]
	lsl r0, r0, #2
	add r0, r8
	ldr r1, [r0]
_08015402:
	lsl r0, r5, #2
	add r0, r0, r1
	ldr r3, [r0]
	ldrh r0, [r3]
	cmp r0, ip
	beq _08015488
	ldr r4, =0x03004720
_08015410:
	ldrh r1, [r3]
	ldrh r2, [r4]
	add r0, r2, #0
	sub r0, #0x10
	cmp r1, r0
	blt _08015480
	ldrh r1, [r3, #4]
	mov r7, #0x80
	lsl r7, r7, #1
	add r0, r2, r7
	cmp r1, r0
	bgt _08015480
	ldrh r1, [r3, #2]
	ldrh r2, [r4, #2]
	add r0, r2, #0
	add r0, #0x10
	cmp r1, r0
	bgt _08015480
	ldrh r1, [r3, #6]
	sub r0, #0xc0
	cmp r1, r0
	blt _08015480
	bl sub_8032F80
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _080154A0
	ldrh r0, [r6]
	asr r0, r5
	mov r1, #1
	and r0, r1
	cmp r0, #0
	beq _080154A0
	mov r0, #4
	bl sub_08010710
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08015470
	mov r0, #4
	bl sub_08010720
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	mov r1, #0xd4
	lsl r1, r1, #1
	cmp r0, r1
	bne _080154EE
_08015470:
	ldr r0, =0x000001A9
	bl playSong
	b _080154EE
	.align 2, 0
.pool
_08015480:
	add r3, #0x10
	ldrh r0, [r3]
	cmp r0, ip
	bne _08015410
_08015488:
	mov r7, sb
	cmp r7, #0
	bne _080154A0
_0801548E:
	mov r0, sb
	cmp r0, #0
	bne _080154A0
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, #4
	bls _080153CA
	b _080154C0
_080154A0:
	bl sub_8032F80
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _080154C0
	mov r0, #4
	bl sub_08010710
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _080154EE
	mov r0, #0xd4
	lsl r0, r0, #1
	bl playSong
	b _080154EE
_080154C0:
	mov r0, #4
	bl sub_08010710
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #1
	bne _080154EE
	ldr r0, =0x000001A9
	bl sub_08010730
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _080154E8
	mov r0, #0xd4
	lsl r0, r0, #1
	bl sub_08010730
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _080154EE
_080154E8:
	mov r0, #4
	bl stopTrack
_080154EE:
	add sp, #4
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool


	
	
thumb_func_global levelfunc_finalboss1
levelfunc_finalboss1:
	push {r4, r5, lr}
	sub sp, #4
	ldr r0, =dword_80F6458
	mov r1, #1
	bl LoadSpritePalette
	add r4, r0, #0
	lsl r4, r4, #0x10
	lsr r4, r4, #0x10
	mov r0, #0x2e
	bl allocateSpriteTiles
	add r2, r0, #0
	ldr r0, =off_80F6658
	ldr r1, =dword_80F50D8
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	mov r3, #0xfe
	str r3, [sp]
	mov r3, #2
	bl allocSprite
	ldr r1, =0x03000D58
	str r0, [r1]
	add r0, #0x22
	mov r5, #0
	strb r4, [r0]
	ldr r2, [r1]
	ldr r3, =0x03004720
	ldrh r1, [r3]
	mov r0, #0x5c
	sub r0, r0, r1
	strh r0, [r2, #0x10]
	ldr r1, =0x0000FF99
	add r0, r1, #0
	ldrh r3, [r3, #2]
	add r0, r0, r3
	strh r0, [r2, #0x12]
	add r2, #0x23
	ldrb r0, [r2]
	mov r1, #0x10
	orr r0, r1
	strb r0, [r2]
	ldr r0, =0x03000D5C
	strh r5, [r0]
	ldr r0, =(sub_8015590+1)
	mov r1, #0xfe
	bl createTask
	add sp, #4
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global sub_8015590
sub_8015590: @ 0x08015590
	push {r4, r5, lr}
	ldr r1, =0x03003D40
	ldrb r0, [r1, #1]
	cmp r0, #4
	bne _08015610
	ldrb r0, [r1, #2]
	cmp r0, #2
	bne _08015610
	ldr r0, =0x03002080
	ldr r0, [r0]
	mov r1, #5
	bl Bios_modulo
	cmp r0, #0
	beq _080155CC
	ldr r0, =0x03003CD4
	ldrh r1, [r0]
	mov r3, #0x80
	lsl r3, r3, #3
	add r2, r3, #0
	orr r1, r2
	strh r1, [r0]
	mov r5, #1
	b _080155D8
	.align 2, 0
.pool
_080155CC:
	ldr r2, =0x03003CD4
	ldrh r1, [r2]
	ldr r0, =0x0000FBFF
	and r0, r1
	strh r0, [r2]
	mov r5, #0
_080155D8:
	ldr r0, =0x03000D5C
	ldrh r0, [r0]
	cmp r0, #0
	beq _08015610
	ldr r4, =0x03000D58
	ldr r0, [r4]
	bl updateSpriteAnimation
	ldr r2, [r4]
	ldr r3, =0x03004720
	ldrh r1, [r3]
	mov r0, #0x5c
	sub r0, r0, r1
	strh r0, [r2, #0x10]
	ldr r1, =0x0000FF99
	add r0, r1, #0
	ldrh r3, [r3, #2]
	add r0, r0, r3
	strh r0, [r2, #0x12]
	mov r0, #1
	add r2, #0x23
	eor r5, r0
	lsl r3, r5, #4
	ldrb r1, [r2]
	sub r0, #0x12
	and r0, r1
	orr r0, r3
	strb r0, [r2]
_08015610:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global sub_8015630
sub_8015630:
	push	{lr}
	ldr	r1,	=0x03000D5C
	mov	r0,	#1
	strh r0, [r1]
	mov	r0, #0
	bl stopTrack
	ldr r0, =off_80CE540
	mov r1, #0xA0
	lsl r1, #19	@0x5000000
	mov r2, #0
	bl startPalAnimation
	mov r0, #0xD0
	bl playSong
	pop {r0}
	bx r0
.pool

thumb_func_global nullsub_9
nullsub_9: @ 0x0801565C
	bx lr
	.align 2, 0

thumb_func_global nullsub_10
nullsub_10: @ 0x08015660
	bx lr
	.align 2, 0
	
thumb_func_global levelfunc_finalboss
levelfunc_finalboss:
	PUSH		{R4-R6,LR}
	SUB		SP, SP,	#4
	MOV		R4, #0
	LDR		R6, _dword_80F67A8
	LDR		R5, _updateBGMap

loc_801566E:
	LSL		R0, R4,	#2
	ADD		R0, R0,	R6
	LDR		R0, [R0]
	ADD		R1, R4,	#2
	LDRB	R2, [R5,#6]
	ADD		R1, R1,	R2
	LSL		R1, R1,	#0xB
	mov		R2, #0xC0
	lsl		r2,	#19	@MOV		R2, #0x6000000
	ADD		R1, R1,	R2
	LDRH	R3, [R5,#2]
	MOVS	R2, #4
	LDRSB	R2, [R5,R2]
	STR		R2, [SP]
	MOV		R2, #0
	BL		LZ77_mapCpy
	ADD		R0, R4,	#1
	LSL		R0, R0,	#0x10
	LSR		R4, R0,	#0x10
	CMP		R4, #3
	BLS		loc_801566E
	LDR		R1, _bg0cnt
	LDR		R0, _updateBGMap
	LDRB	R3, [R0,#1]
	SUB		R3, #2
	LSL		R3, R3,	#1
	ADD		R3, R3,	R1
	LDRB	R1, [R0,#5]
	LSL		R1, R1,	#2
	LDRB	R0, [R0,#6]
	ADD		R0, #2
	LSL		R0, R0,	#8
	LDR		R4, dword_80156E0
	MOV		R2, R4
	ORR		R0, R2
	ORR		R1, R0
	MOV		R0, #1
	ORR		R1, R0
	STRH	R1, [R3]
	LDR		R1, dword_80156E4
	@MOV		R0, #0x200
	mov		r0,#0x80
	lsl 	r0,#2
	STR		R0, [R1,#8]
	BL		sub_080156E8
	ADD		SP, SP,	#4
	POP		{R4-R6}
	POP		{R0}
	BX		R0
	
	.align 2, 0
	_dword_80F67A8: .4byte dword_80F67A8
	_updateBGMap:	.4byte 0x030046F0
	_bg0cnt:	.4byte 0x03003D00
	dword_80156E0:	.4byte 0xFFFF8000
	dword_80156E4:	.4byte 0x0201FF50

thumb_func_global sub_080156E8
sub_080156E8: @ 0x080156E8
	push {r4, r5, r6, lr}
	sub sp, #4
	ldr r4, =0x0201FF50
	ldr r6, [r4, #8]
	mov r0, sp
	mov r1, #0
	strh r1, [r0]
	ldr r2, =0x01000034
	add r1, r4, #0
	bl Bios_memcpy
	add r0, r4, #0
	bl storeFFFFto_arg18
	add r0, r4, #0
	bl sub_08015744
	mov r1, #0
	ldr r5, =0x0000FFE8
	ldr r3, =0x0000FFE0
	ldr r2, =0x0000FFD8
_08015712:
	lsl r0, r1, #4
	add r0, r4, r0
	strh r5, [r0, #0x20]
	strh r3, [r0, #0x22]
	strh r2, [r0, #0x24]
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	cmp r1, #2
	bls _08015712
	str r6, [r4, #8]
	add sp, #4
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool

thumb_func_global sub_08015744
sub_08015744: @ 0x08015744
	ldr r1, =0xFFFE0000
	str r1, [r0, #0x10]
	mov r1, #0x80
	lsl r1, r1, #0xa
	str r1, [r0, #0x14]
	bx lr
	.align 2, 0
.pool




thumb_func_global sub_08015754
sub_08015754: @ 0x08015754
	push {r4, r5, r6, r7, lr}
	mov r7, sb
	mov r6, r8
	push {r6, r7}
	sub sp, #4
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov r8, r1
	cmp r4, #1
	bls _08015770
	mov r0, #1
	b _08015AC2
_08015770:
	bl getPlayerHealth
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _080157B8
	bl getHealthBarValue
	lsl r0, r0, #0x10
	cmp r0, #0
	bne _080157B8
	ldr r1, =0x03004720
	mov r0, #4
	strh r0, [r1, #8]
	ldr r2, =0x03003CD4
	ldr r0, =0x030046F0
	ldrb r0, [r0, #1]
	sub r0, #2
	mov r1, #0x80
	lsl r1, r1, #1
	lsl r1, r0
	ldrh r0, [r2]
	bic r0, r1
	strh r0, [r2]
	ldr r0, =0x0000018B
	bl stopSong_maybe
	mov r0, #1
	b _08015AC2
	.align 2, 0
.pool
_080157B8:
	ldr r7, =0x0201FF50
	ldrh r1, [r7, #2]
	cmp r1, #1
	beq _0801589C
	cmp r1, #1
	bgt _080157D0
	cmp r1, #0
	beq _080157DE
	b _08015AC0
	.align 2, 0
.pool
_080157D0:
	cmp r1, #2
	bne _080157D6
	b _080158D4
_080157D6:
	cmp r1, #3
	bne _080157DC
	b _08015980
_080157DC:
	b _08015AC0
_080157DE:
	mov r0, #1
	strh r0, [r7, #2]
	add r0, r7, #0
	bl storeFFFFto_arg18
	strb r4, [r7]
	ldr r1, =dword_80F67C0
	mov r2, r8
	lsl r0, r2, #1
	add r0, r0, r1
	ldrh r0, [r0]
	strb r0, [r7, #1]
	mov r0, r8
	sub r0, #2
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	cmp r0, #1
	bhi _08015848
	ldr r6, =dword_80F67A8
	ldr r0, [r6, #0x10]
	ldr r4, =0x030046F0
	ldrb r1, [r4, #6]
	add r1, #2
	lsl r1, r1, #0xb
	mov r5, #0xc0
	lsl r5, r5, #0x13
	add r1, r1, r5
	ldrh r3, [r4, #2]
	mov r2, #4
	ldrsb r2, [r4, r2]
	str r2, [sp]
	mov r2, #0
	bl LZ77_mapCpy
	ldr r0, [r6, #0x14]
	ldrb r1, [r4, #6]
	add r1, #3
	lsl r1, r1, #0xb
	add r1, r1, r5
	ldrh r3, [r4, #2]
	mov r2, #4
	ldrsb r2, [r4, r2]
	str r2, [sp]
	mov r2, #0
	bl LZ77_mapCpy
	b _08015880
	.align 2, 0
.pool
_08015848:
	ldr r6, =dword_80F67A8
	ldr r0, [r6]
	ldr r4, =0x030046F0
	ldrb r1, [r4, #6]
	add r1, #2
	lsl r1, r1, #0xb
	mov r5, #0xc0
	lsl r5, r5, #0x13
	add r1, r1, r5
	ldrh r3, [r4, #2]
	mov r2, #4
	ldrsb r2, [r4, r2]
	str r2, [sp]
	mov r2, #0
	bl LZ77_mapCpy
	ldr r0, [r6, #4]
	ldrb r1, [r4, #6]
	add r1, #3
	lsl r1, r1, #0xb
	add r1, r1, r5
	ldrh r3, [r4, #2]
	mov r2, #4
	ldrsb r2, [r4, r2]
	str r2, [sp]
	mov r2, #0
	bl LZ77_mapCpy
_08015880:
	mov r0, r8
	cmp r0, #3
	bne _08015888
	b _08015AC0
_08015888:
	bl removeCameraPlayerControl
	mov r0, #9
	bl sub_08013A00
	b _08015AC0
	.align 2, 0
.pool
_0801589C:
	ldr r4, =0x03004720
	ldrh r2, [r4]
	add r3, r2, #0
	cmp r3, #8
	bne _080158B0
	mov r0, #2
	strh r0, [r7, #2]
	b _080158CC
	.align 2, 0
.pool
_080158B0:
	ldr r0, =0x03002080
	ldr r0, [r0]
	and r0, r1
	cmp r0, #0
	beq _080158CC
	cmp r3, #7
	bhi _080158C8
	add r0, r2, #1
	b _080158CA
	.align 2, 0
.pool
_080158C8:
	sub r0, r2, #1
_080158CA:
	strh r0, [r4]
_080158CC:
	add r0, r7, #0
	bl storeFFFFto_arg18
	b _08015AC0
_080158D4:
	ldr r0, =0xFFFF6000
	str r0, [r7, #0xc]
	ldrb r0, [r7]
	cmp r0, #0
	beq _080158E2
	mov r0, #0
	str r0, [r7, #0xc]
_080158E2:
	ldr r4, =0x03003D00
	ldr r3, =0x030046F0
	ldrb r1, [r3, #1]
	sub r1, #2
	lsl r1, r1, #1
	add r1, r1, r4
	ldrh r2, [r1]
	ldr r0, =0x0000E0FF
	and r0, r2
	strh r0, [r1]
	ldrb r2, [r3, #1]
	sub r2, #2
	lsl r2, r2, #1
	add r2, r2, r4
	ldrb r1, [r3, #6]
	add r1, #2
	ldrb r0, [r7]
	lsl r0, r0, #1
	add r1, r1, r0
	lsl r1, r1, #8
	ldrh r0, [r2]
	orr r1, r0
	strh r1, [r2]
	ldr r2, =0x03003CD4
	ldrb r1, [r3, #1]
	sub r1, #2
	mov r0, #0x80
	lsl r0, r0, #1
	lsl r0, r1
	ldrh r1, [r2]
	orr r0, r1
	strh r0, [r2]
	ldrb r0, [r7]
	cmp r0, #0
	bne _0801594C
	ldr r0, =(sub_8015D00+1)
	mov r1, #0xfe
	bl createTask
	b _08015954
	.align 2, 0
.pool
_0801594C:
	ldr r0, =(sub_8015D40+1)
	mov r1, #0xfe
	bl createTask
_08015954:
	mov r1, r8
	cmp r1, #3
	beq _08015968
	mov r0, #5
	bl sub_08013A00
	b _0801596E
	.align 2, 0
.pool
_08015968:
	mov r0, #7
	bl sub_08013A00
_0801596E:
	ldrh r0, [r7, #2]
	add r0, #1
	strh r0, [r7, #2]
	ldr r0, =0x0000018B
	bl playSong
	b _08015AC0
	.align 2, 0
.pool
_08015980:
	mov r2, #0
	mov sb, r2
	ldrb r1, [r7]
	add r0, r7, #0
	mov r2, r8
	bl sub_08015ADC
	lsl r0, r0, #0x10
	cmp r0, #0
	beq _08015A88
	ldrb r0, [r7, #1]
	sub r0, #1
	strb r0, [r7, #1]
	lsl r0, r0, #0x18
	cmp r0, #0
	beq _08015A24
	ldr r0, =0x0000018B
	bl playSong
	ldrb r0, [r7]
	mov r1, #1
	eor r0, r1
	strb r0, [r7]
	ldrh r0, [r7, #2]
	sub r0, #1
	strh r0, [r7, #2]
	ldr r2, =0x03003CD4
	ldr r6, =0x030046F0
	ldrb r0, [r6, #1]
	sub r0, #2
	mov r1, #0x80
	lsl r1, r1, #1
	lsl r1, r0
	ldrh r0, [r2]
	bic r0, r1
	strh r0, [r2]
	add r0, r7, #0
	bl sub_08015744
	mov r0, r8
	cmp r0, #4
	bne _08015A6C
	ldrb r0, [r7, #1]
	cmp r0, #2
	bne _08015A6C
	ldr r5, =dword_80F67A8
	ldr r0, [r5, #0x10]
	ldrb r1, [r6, #6]
	add r1, #2
	lsl r1, r1, #0xb
	mov r4, #0xc0
	lsl r4, r4, #0x13
	add r1, r1, r4
	ldrh r3, [r6, #2]
	mov r2, #4
	ldrsb r2, [r6, r2]
	str r2, [sp]
	mov r2, #0
	bl LZ77_mapCpy
	ldr r0, [r5, #0x14]
	ldrb r1, [r6, #6]
	add r1, #3
	lsl r1, r1, #0xb
	add r1, r1, r4
	ldrh r3, [r6, #2]
	mov r2, #4
	ldrsb r2, [r6, r2]
	str r2, [sp]
	mov r2, #0
	bl LZ77_mapCpy
	b _08015A6C
	.align 2, 0
.pool
_08015A24:
	bl sub_080156E8
	mov r1, #1
	mov sb, r1
	ldr r2, =0x03003CD4
	ldr r0, =0x030046F0
	ldrb r0, [r0, #1]
	sub r0, #2
	add r1, #0xff
	lsl r1, r0
	ldrh r0, [r2]
	bic r0, r1
	strh r0, [r2]
	mov r2, r8
	cmp r2, #3
	beq _08015A54
	mov r0, #4
	bl sub_08013A00
	b _08015A60
	.align 2, 0
.pool
_08015A54:
	mov r0, #6
	bl sub_08013A00
	ldr r1, =0x03004720
	mov r0, #8
	strh r0, [r1]
_08015A60:
	ldr r0, =m4a_songs
	ldr r1, =0x00000C5C
	add r0, r0, r1
	ldrh r0, [r0]
	bl stopTrack
_08015A6C:
	ldr r1, =unk_80f67ca
	ldr r0, =0x03003D40
	ldrb r0, [r0, #0x11]
	lsl r0, r0, #1
	add r0, r0, r1
	ldrh r1, [r0]
	ldr r0, [r7, #8]
	add r0, r0, r1
	str r0, [r7, #8]
	mov r1, #0x80
	lsl r1, r1, #4
	cmp r0, r1
	ble _08015A88
	str r1, [r7, #8]
_08015A88:
	ldrb r0, [r7]
	cmp r0, #0
	bne _08015AB0
	ldr r0, =(sub_8015D00+1)
	mov r1, #0xfe
	bl createTask
	b _08015AB8
	.align 2, 0
.pool
_08015AB0:
	ldr r0, =(sub_8015D40+1)
	mov r1, #0xfe
	bl createTask
_08015AB8:
	mov r0, sb
	b _08015AC2
	.align 2, 0
.pool
_08015AC0:
	mov r0, #0
_08015AC2:
	add sp, #4
	pop {r3, r4}
	mov r8, r3
	mov sb, r4
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1

thumb_func_global storeFFFFto_arg18
storeFFFFto_arg18: @ 0x08015AD0
	ldr r1, =0x0000FFFF
	strh r1, [r0, #0x18]
	bx lr
	.align 2, 0
.pool

thumb_func_global sub_08015ADC
sub_08015ADC: @ 0x08015ADC
	push {r4, r5, r6, r7, lr}
	mov r7, sl
	mov r6, sb
	mov r5, r8
	push {r5, r6, r7}
	sub sp, #0x14
	add r6, r0, #0
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	mov ip, r1
	lsl r2, r2, #0x10
	lsr r2, r2, #0x10
	str r2, [sp]
	ldr r0, =0x03003D40
	ldrb r1, [r0, #0x11]
	cmp r1, #0
	bne _08015B2C
	ldr r0, =dword_80F67F4
	lsl r1, r2, #2
	add r1, r1, r0
	ldr r3, =dword_80F67F0
	mov r0, ip
	lsl r4, r0, #1
	add r0, r4, r3
	mov r5, #0
	ldrsh r2, [r0, r5]
	ldr r0, [r1]
	add r1, r0, #0
	mul r1, r2, r1
	ldr r0, [r6, #0xc]
	add r0, r0, r1
	str r0, [r6, #0xc]
	str r4, [sp, #8]
	b _08015B44
	.align 2, 0
.pool
_08015B2C:
	ldr r2, =dword_80F67F0
	mov r7, ip
	lsl r3, r7, #1
	add r0, r3, r2
	mov r4, #0
	ldrsh r1, [r0, r4]
	ldr r0, [r6, #8]
	mul r1, r0, r1
	ldr r0, [r6, #0xc]
	add r0, r0, r1
	str r0, [r6, #0xc]
	str r3, [sp, #8]
_08015B44:
	mov r5, ip
	cmp r5, #0
	bne _08015B5C
	ldr r5, [r6, #0xc]
	ldr r7, =0xFFFEA001
	add r4, r5, r7
	b _08015B62
	.align 2, 0
.pool
_08015B5C:
	ldr r4, [r6, #0xc]
	ldr r0, =0x00015FFF
	add r5, r4, r0
_08015B62:
	ldr r0, =0x03004720
	ldrh r0, [r0, #2]
	lsl r0, r0, #8
	add r4, r4, r0
	add r5, r5, r0
	mov r1, #0x80
	lsl r1, r1, #6
	add r4, r4, r1
	ldr r2, =0xFFFFE000
	add r5, r5, r2
	cmp r4, #0
	bge _08015B7C
	mov r4, #0
_08015B7C:
	asr r4, r4, #8
	mov r0, #0x80
	lsl r0, r0, #1
	cmp r4, r0
	ble _08015B88
	add r4, r0, #0
_08015B88:
	cmp r5, #0
	bge _08015B8E
	mov r5, #0
_08015B8E:
	asr r5, r5, #8
	cmp r5, r0
	ble _08015B96
	add r5, r0, #0
_08015B96:
	ldr r1, [sp]
	sub r1, #2
	lsl r0, r1, #0x10
	lsr r0, r0, #0x10
	add r2, r1, #0
	cmp r0, #1
	bls _08015BB0
	ldr r3, [sp]
	cmp r3, #4
	bne _08015BC4
	ldrb r0, [r6, #1]
	cmp r0, #2
	bhi _08015BC4
_08015BB0:
	mov r7, #4
	mov sb, r7
	b _08015BC8
	.align 2, 0
.pool
_08015BC4:
	mov r0, #2
	mov sb, r0
_08015BC8:
	mov r1, ip
	cmp r1, #0
	beq _08015BD2
	mov r3, #3
	mov sb, r3
_08015BD2:
	mov r1, #0
	mov r7, ip
	lsl r7, r7, #2
	str r7, [sp, #0xc]
	mov r0, #0x10
	add r0, r0, r6
	mov sl, r0
	cmp r1, sb
	bhs _08015C6A
	lsl r0, r2, #0x10
	lsr r0, r0, #0x10
	str r0, [sp, #4]
	mov r2, ip
	lsl r2, r2, #3
	str r2, [sp, #0x10]
	ldr r3, =0x03004720
	mov r8, r3
_08015BF4:
	lsl r0, r1, #4
	add r0, r6, r0
	strh r4, [r0, #0x1a]
	strh r5, [r0, #0x1e]
	ldr r7, [sp, #4]
	cmp r7, #1
	bhi _08015C20
	lsl r0, r1, #1
	ldr r2, [sp, #0x10]
	add r0, r0, r2
	ldr r3, =dword_80F67D0
	add r0, r0, r3
	mov r7, #0
	ldrsh r0, [r0, r7]
	mov r3, r8
	ldrh r3, [r3]
	add r2, r0, r3
	b _08015C34
	.align 2, 0
.pool
_08015C20:
	lsl r0, r1, #1
	ldr r7, [sp, #0x10]
	add r0, r0, r7
	ldr r2, =dword_80F67E0
	add r0, r0, r2
	mov r3, #0
	ldrsh r0, [r0, r3]
	mov r7, r8
	ldrh r7, [r7]
	add r2, r0, r7
_08015C34:
	add r3, r2, #0
	add r3, #0x2f
	add r2, #4
	sub r3, #4
	cmp r2, #0
	bge _08015C42
	mov r2, #0
_08015C42:
	cmp r3, #0
	bge _08015C48
	mov r3, #0
_08015C48:
	mov r0, #0x80
	lsl r0, r0, #1
	cmp r2, r0
	ble _08015C52
	add r2, r0, #0
_08015C52:
	cmp r3, r0
	ble _08015C58
	add r3, r0, #0
_08015C58:
	lsl r0, r1, #4
	add r0, r6, r0
	strh r2, [r0, #0x18]
	strh r3, [r0, #0x1c]
	add r0, r1, #1
	lsl r0, r0, #0x10
	lsr r1, r0, #0x10
	cmp r1, sb
	blo _08015BF4
_08015C6A:
	lsl r0, r1, #4
	add r0, r6, r0
	ldr r1, =0x0000FFFF
	strh r1, [r0, #0x18]
	ldr r1, =0x03003D40
	ldrb r0, [r1, #0x11]
	cmp r0, #0
	beq _08015C80
	ldr r2, [sp]
	cmp r2, #3
	bne _08015CB8
_08015C80:
	ldr r3, [sp, #0xc]
	add r3, sl
	ldr r4, [sp, #8]
	ldr r5, =dword_80F67F0
	add r0, r4, r5
	mov r7, #0
	ldrsh r2, [r0, r7]
	ldr r1, =dword_80F67F4
	ldr r4, [sp]
	lsl r0, r4, #2
	add r0, r0, r1
	ldr r0, [r0]
	add r1, r2, #0
	mul r1, r0, r1
	ldr r0, [r3]
	add r0, r0, r1
	str r0, [r3]
	b _08015CD0
	.align 2, 0
.pool
_08015CB8:
	ldr r2, [sp, #0xc]
	add r2, sl
	ldr r5, [sp, #8]
	ldr r7, =dword_80F67F0
	add r0, r5, r7
	mov r3, #0
	ldrsh r1, [r0, r3]
	ldr r0, [r6, #8]
	mul r1, r0, r1
	ldr r0, [r2]
	add r0, r0, r1
	str r0, [r2]
_08015CD0:
	mov r4, ip
	cmp r4, #0
	beq _08015CE8
	ldr r0, [sp, #0xc]
	add r0, sl
	ldr r0, [r0]
	cmp r0, #0
	bge _08015CEE
_08015CE0:
	mov r0, #1
	b _08015CF0
	.align 2, 0
.pool
_08015CE8:
	ldr r0, [r6, #0x10]
	cmp r0, #0
	bgt _08015CE0
_08015CEE:
	mov r0, #0
_08015CF0:
	add sp, #0x14
	pop {r3, r4, r5}
	mov r8, r3
	mov sb, r4
	mov sl, r5
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	
	@MOVE TO level_callbacks.s
	
	
	
	
	
	thumb_func_global sub_8015D00
sub_8015D00: @ 0x08015D00
	push {lr}
	ldr r2, =0x0201FF50
	ldr r1, =0x030022F8
	ldr r3, =0x030046F0
	ldrb r0, [r3, #1]
	sub r0, #2
	lsl r0, r0, #1
	add r0, r0, r1
	ldr r1, [r2, #0x10]
	asr r1, r1, #8
	neg r1, r1
	mov r2, #0
	strh r1, [r0]
	ldr r1, =0x03002308
	ldrb r0, [r3, #1]
	sub r0, #2
	lsl r0, r0, #1
	add r0, r0, r1
	strh r2, [r0]
	bl endCurrentTask
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_8015D40
sub_8015D40: @ 0x08015D40
	push {r4, lr}
	ldr r2, =0x0201FF50
	ldr r1, =0x030022F8
	ldr r4, =0x030046F0
	ldrb r0, [r4, #1]
	sub r0, #2
	lsl r0, r0, #1
	add r0, r0, r1
	ldr r1, [r2, #0x14]
	asr r1, r1, #8
	ldr r3, =0xFFFFFEA0
	add r2, r3, #0
	sub r2, r2, r1
	mov r3, #0
	strh r2, [r0]
	ldr r1, =0x03002308
	ldrb r0, [r4, #1]
	sub r0, #2
	lsl r0, r0, #1
	add r0, r0, r1
	strh r3, [r0]
	bl endCurrentTask
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global levelfunc_launch0
levelfunc_launch0: @ 0x08015D88
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #4
	ldr r6, =0x0201FF50
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r2, =0x01000054
	mov r0, sp
	add r1, r6, #0
	bl Bios_memcpy
	add r4, r6, #0
	add r4, #0xa0
	ldr r0, =byte_80EF2CC
	str r0, [r4]
	ldrb r0, [r0, #1]
	lsl r0, r0, #5
	ldr r1, =dword_80EE424
	add r0, r0, r1
	mov r1, #1
	bl LoadSpritePalette
	strh r0, [r6, #0x34]
	mov r5, #0
	ldr r0, [r4]
	mov r4, sp
	add r4, #2
	b _08015E0A
	.align 2, 0
.pool
_08015DD4:
	mov r0, #0x18
	bl allocateSpriteTiles
	lsl r2, r5, #1
	add r1, r6, #0
	add r1, #0x36
	add r1, r1, r2
	strh r0, [r1]
	bl rand
	mov r1, #8
	bl Bios_modulo
	add r1, r6, #0
	add r1, #0x5d
	add r1, r1, r5
	ldr r2, =dword_80EF304
	lsl r0, r0, #1
	add r0, r0, r2
	ldrh r0, [r0]
	strb r0, [r1]
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	add r0, r6, #0
	add r0, #0xa0
	ldr r0, [r0]
_08015E0A:
	ldrb r0, [r0]
	cmp r5, r0
	blo _08015DD4
	mov r0, #0xd2
	lsl r0, r0, #1
	strh r0, [r6, #2]
	mov r5, #0
	add r6, r4, #0
	ldr r0, =word_80F3A80
	mov r8, r0
_08015E1E:
	mov r4, #0
	lsl r0, r5, #1
	add r7, r5, #1
	add r0, r0, r5
	lsl r5, r0, #2
_08015E28:
	mov r0, #0
	strh r0, [r6]
	lsl r0, r4, #1
	add r0, r0, r5
	add r0, r8
	ldrh r1, [r0]
	lsl r1, r1, #5
	mov r0, #0xc0
	lsl r0, r0, #0x13
	add r1, r1, r0
	add r0, r6, #0
	ldr r2, =0x01000010
	bl Bios_memcpy
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	cmp r4, #5
	bls _08015E28
	lsl r0, r7, #0x10
	lsr r5, r0, #0x10
	cmp r5, #4
	bls _08015E1E
	ldr r0, =(task_handleLasersAndSparks+1)
	mov r1, #0xfe
	bl createTask
	add sp, #4
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global task_handleLasersAndSparks
task_handleLasersAndSparks: @ 0x08015E7C
	push {r4, r5, r6, r7, lr}
	ldr r6, =0x0201FF50
	mov r1, #0x34
	ldrsh r0, [r6, r1]
	cmp r0, #0
	bge _08015EA4
	add r0, r6, #0
	add r0, #0xa0
	ldr r0, [r0]
	ldrb r0, [r0, #1]
	lsl r0, r0, #5
	ldr r1, =dword_80E74C8
	add r0, r0, r1
	mov r1, #1
	bl LoadSpritePalette
	strh r0, [r6, #0x34]
	lsl r0, r0, #0x10
	cmp r0, #0
	blt _08015EEA
_08015EA4:
	mov r5, #0
	add r0, r6, #0
	add r0, #0xa0
	ldr r1, [r0]
	add r7, r0, #0
	ldrb r1, [r1]
	cmp r5, r1
	bhs _08015EEA
_08015EB4:
	lsl r0, r5, #1
	add r1, r6, #0
	add r1, #0x36
	add r4, r1, r0
	mov r1, #0
	ldrsh r0, [r4, r1]
	cmp r0, #0
	bge _08015ED2
	mov r0, #0x18
	bl allocateSpriteTiles
	strh r0, [r4]
	lsl r0, r0, #0x10
	cmp r0, #0
	blt _08015EDC
_08015ED2:
	add r0, r5, #0
	add r1, r6, #0
	add r1, #0x34
	bl sub_8014268
_08015EDC:
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldr r0, [r7]
	ldrb r0, [r0]
	cmp r5, r0
	blo _08015EB4
_08015EEA:
	mov r5, #0
_08015EEC:
	add r0, r5, #0
	add r1, r6, #0
	bl sub_8014FC4
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	cmp r5, #4
	bls _08015EEC
	add r0, r6, #0
	bl sub_8015398
	ldrh r0, [r6, #2]
	add r0, #1
	ldr r2, =dword_80F47C4
	ldr r1, =0x03003D40
	ldrb r1, [r1, #0x11]
	lsl r1, r1, #1
	add r1, r1, r2
	ldrh r1, [r1]
	bl Bios_modulo
	strh r0, [r6, #2]
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_8015F30
sub_8015F30: @ 0x08015F30
	sub sp, #8
	add sp, #8
	bx lr
	.align 2, 0
	thumb_func_global sub_8015F38
sub_8015F38: @ 0x08015F38
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #4
	ldr r6, =0x0201FF50
	mov r0, #0
	b _08015FDE
	.align 2, 0
.pool
_08015F4C:
	mov r0, r8
	lsl r4, r0, #2
	add r3, r3, r4
	ldrh r2, [r3, #4]
	ldr r1, =0x03004720
	ldrh r0, [r1]
	sub r7, r2, r0
	ldrh r1, [r1, #2]
	ldrh r0, [r3, #6]
	sub r5, r1, r0
	add r1, r7, #0
	add r1, #0x10
	mov r0, #0x88
	lsl r0, r0, #1
	add r2, r4, #0
	cmp r1, r0
	bhi _08015F7A
	mov r0, #0x10
	neg r0, r0
	cmp r5, r0
	blt _08015F7A
	cmp r5, #0xb0
	ble _08015F94
_08015F7A:
	add r0, r6, #0
	add r0, #8
	add r4, r0, r2
	ldr r0, [r4]
	cmp r0, #0
	beq _08015FD6
	bl spriteFreeHeader
	mov r0, #0
	str r0, [r4]
	b _08015FD6
	.align 2, 0
.pool
_08015F94:
	add r0, r6, #0
	add r0, #8
	add r4, r0, r4
	ldr r0, [r4]
	cmp r0, #0
	bne _08015FB6
	ldrh r2, [r6, #2]
	mov r0, #0xff
	str r0, [sp]
	ldr r0, =unk_8265438
	ldr r1, =unk_826020C
	mov r3, #3
	bl allocSprite
	str r0, [r4]
	cmp r0, #0
	beq _08015FD6
_08015FB6:
	ldr r0, [r4]
	strh r7, [r0, #0x10]
	ldr r0, [r4]
	strh r5, [r0, #0x12]
	ldr r0, [r4]
	ldrh r1, [r6]
	add r0, #0x22
	strb r1, [r0]
	ldr r2, [r4]
	add r2, #0x23
	ldrb r0, [r2]
	mov r3, #0x11
	neg r3, r3
	add r1, r3, #0
	and r0, r1
	strb r0, [r2]
_08015FD6:
	mov r0, r8
	add r0, #1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
_08015FDE:
	mov r8, r0
	ldr r3, [r6, #4]
	ldrh r0, [r3]
	cmp r8, r0
	blo _08015F4C
	add sp, #4
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global levelfunc_launch1
levelfunc_launch1: @ 0x08015FFC
	push {r4, r5, lr}
	ldr r2, =0x03003D40
	ldrb r0, [r2, #1]
	cmp r0, #3
	bls _08016082
	cmp r0, #4
	bhi _08016020
	ldr r1, =off_80F68E0
	ldrb r0, [r2, #2]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r4, [r0]
	b _08016024
	.align 2, 0
.pool
_08016020:
	ldr r0, =off_80F68E0
	ldr r4, [r0, #0xc]
_08016024:
	ldr r1, =0x03000D5E
	ldr r2, =0x0000FFFF
	add r0, r2, #0
	strh r0, [r1]
	mov r5, #0
	ldrh r0, [r4]
	cmp r5, r0
	bhs _08016082
_08016034:
	ldr r0, =0x03003D40
	ldrb r0, [r0, #1]
	cmp r0, #5
	bne _08016064
	cmp r5, #3
	bne _08016064
	ldr r0, [r4, #0x10]
	mov r1, #0xa0
	lsl r1, r1, #0x13
	mov r2, #0
	bl startPalAnimation
	ldr r1, =0x03000D5E
	strh r0, [r1]
	b _08016076
	.align 2, 0
.pool
_08016064:
	lsl r0, r5, #2
	add r1, r4, #4
	add r1, r1, r0
	ldr r0, [r1]
	mov r1, #0xa0
	lsl r1, r1, #0x13
	mov r2, #0
	bl startPalAnimation
_08016076:
	add r0, r5, #1
	lsl r0, r0, #0x10
	lsr r5, r0, #0x10
	ldrh r2, [r4]
	cmp r5, r2
	blo _08016034
_08016082:
	pop {r4, r5}
	pop {r0}
	bx r0
	thumb_func_global levelfunc_airport3
levelfunc_airport3: @ 0x08016088
	push {r4, lr}
	ldr r0, =unk_809B55C
	mov r4, #0xa0
	lsl r4, r4, #0x13
	add r1, r4, #0
	mov r2, #0
	bl startPalAnimation
	ldr r0, =byte_809B660
	add r1, r4, #0
	mov r2, #0
	bl startPalAnimation
	ldr r0, =off_809B6B0
	add r1, r4, #0
	mov r2, #0
	bl startPalAnimation
	ldr r0, =off_809B778
	add r1, r4, #0
	mov r2, #0
	bl startPalAnimation
	ldr r0, =off_809B804
	add r1, r4, #0
	mov r2, #0
	bl startPalAnimation
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global levelfunc_activateHarborFlamePal
levelfunc_activateHarborFlamePal: @ 0x080160DC
	push {r4, r5, lr}
	ldr r2, =0x03003D40
	ldrb r0, [r2, #1]
	cmp r0, #1
	bne _08016122
	ldr r1, =off_80F692C
	ldrb r0, [r2, #2]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r5, [r0]
	ldr r1, =0x03000D5E
	ldr r2, =0x0000FFFF
	add r0, r2, #0
	strh r0, [r1]
	mov r4, #0
	ldrh r0, [r5]
	cmp r4, r0
	bhs _08016122
_08016100:
	lsl r1, r4, #2
	add r0, r5, #4
	add r0, r0, r1
	ldr r0, [r0]
	mov r1, #0xa0
	lsl r1, r1, #0x13
	mov r2, #0
	bl startPalAnimation
	ldr r1, =0x03000D5E
	strh r0, [r1]
	add r0, r4, #1
	lsl r0, r0, #0x10
	lsr r4, r0, #0x10
	ldrh r2, [r5]
	cmp r4, r2
	blo _08016100
_08016122:
	pop {r4, r5}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_8016138
sub_8016138: @ 0x08016138
	push {r4, r5, r6, lr}
	ldr r2, =0x03003D40
	ldrb r0, [r2, #1]
	cmp r0, #1
	bne _08016158
	ldr r1, =off_80F698C
	ldrb r0, [r2, #2]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r5, [r0]
	b _0801615C
	.align 2, 0
.pool
_08016158:
	ldr r0, =off_80F698C
	ldr r5, [r0, #0xc]
_0801615C:
	ldr r4, =0x03000D5E
	ldrh r0, [r4]
	ldr r6, =0x0000FFFF
	cmp r0, r6
	beq _08016178
	bl freePalFadeTaskAtIndex
	ldr r0, [r5, #4]
	mov r1, #0xa0
	lsl r1, r1, #0x13
	mov r2, #0
	bl startPalAnimation
	strh r6, [r4]
_08016178:
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool
	thumb_func_global sub_801618C
sub_801618C: @ 0x0801618C
	push {r4, lr}
	ldr r4, =0x03000D5E
	ldrh r1, [r4]
	ldr r0, =0x0000FFFF
	cmp r1, r0
	bne _080161D0
	ldr r2, =0x03003D40
	ldrb r0, [r2, #1]
	cmp r0, #5
	bne _080161B8
	ldr r0, =off_80F68E0
	ldr r0, [r0, #0xc]
	ldr r0, [r0, #0x10]
	b _080161C4
	.align 2, 0
.pool
_080161B8:
	ldr r1, =off_80F692C
	ldrb r0, [r2, #2]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	ldr r0, [r0, #4]
_080161C4:
	mov r1, #0xa0
	lsl r1, r1, #0x13
	mov r2, #0
	bl startPalAnimation
	strh r0, [r4]
_080161D0:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool
