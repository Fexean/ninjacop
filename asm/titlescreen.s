.include "asm/macros.inc"


@completely disassembled


thumb_func_global cb_loadTitleScreen
cb_loadTitleScreen: @ 0x08026540
	push {r4, r5, r6, r7, lr}
	mov r7, r8
	push {r7}
	sub sp, #8
	ldr r0, =0x03003CD4
	mov r8, r0
	mov r1, #0x82
	lsl r1, r1, #5
	add r0, r1, #0
	mov r2, r8
	strh r0, [r2]
	bl FreeAllPalFadeTasks
	bl clearSprites
	bl resetAffineTransformations
	bl clearPalBufs
	bl stopPalFade
	mov r0, #2
	bl endPriorityOrHigherTask
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r5, =0x03002080
	ldrb r1, [r5, #9]
	mov r0, #5
	neg r0, r0
	and r0, r1
	strb r0, [r5, #9]
	ldr r0, =titlePalettes
	mov r2, #0xa0
	lsl r2, r2, #0x13
	mov r1, #0x10
	bl memcpy_pal
	ldr r0, =titlescreenTiles
	mov r1, #0x80
	
	@Title screen tileset is twice as large in US version
	.ifdef VERSION_FIVEO
		lsl r1, r1, #8	@0x8000
	.else
		lsl r1, r1, #7	@0x4000
	.endif
	
	
	mov r2, #0xc0
	lsl r2, r2, #0x13
	bl Bios__memcpy_bytecount
	add r0, sp, #4
	mov r7, #0
	strh r7, [r0]
	ldr r4, =0x06008000
	ldr r2, =0x01000400
	add r1, r4, #0
	bl Bios_memcpy
	ldr r0, =titlescreenMap
	mov r6, #0
	str r6, [sp]
	add r1, r4, #0
	mov r2, #0
	mov r3, #0
	bl tilemapCpy
	ldr r1, =0x03003D00
	mov r0, #0x84
	lsl r0, r0, #5
	strh r0, [r1]
	ldr r0, =pressStart_pal
	ldr r4, =0x05000200
	mov r1, #1
	add r2, r4, #0
	bl memcpy_pal
	ldr r0, =pressStartTiles
	ldr r1, =0x06010000
	bl Bios_LZ77_16_2
	ldr r1, =pressStart
	add r5, #0x2c
	ldrb r0, [r5]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	str r6, [sp]
	mov r1, #0
	mov r2, #0
	mov r3, #0
	bl allocSprite
	ldr r1, =0x03000588
	str r0, [r1]
	
	.ifdef VERSION_FIVEO
		mov r1,#24
		strh r1,[r0, #0x12]	@Sets Y-position of the "press start" sprite
	.endif
	
	add r0, #0x23
	ldrb r1, [r0]
	mov r2, #0x10
	orr r1, r2
	strb r1, [r0]
	ldr r0, =off_814D91C
	add r1, r4, #0
	mov r2, #0
	bl startPalAnimation
	ldr r1, =0x0300058C
	strh r0, [r1]
	mov r1, r8
	ldrh r0, [r1]
	mov r2, #0x80
	lsl r2, r2, #1
	add r1, r2, #0
	orr r0, r1
	mov r1, r8
	strh r0, [r1]
	ldr r0, =0x03000580
	strh r6, [r0]
	ldr r1, =0x03000582
	strh r6, [r1]
	ldr r4, =0x03003D40
	ldrb r0, [r4, #0x12]
	cmp r0, #0
	beq _08026690
	mov r0, #4
	strh r0, [r1]
	bl gfx_related_sub_800F674
	bl stopSoundTracks
	mov r0, #0xe8
	bl song_play_and_auto_config
	strb r7, [r4, #0x12]
	b _08026694
	.align 2, 0
.pool
_08026690:
	bl stopSoundTracks
_08026694:
	ldr r0, =(cb_titleScreen_swtich+1)
	bl setCurrentTaskFunc
	add sp, #8
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6, r7}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global cb_titleScreen_swtich
cb_titleScreen_swtich: @ 0x080266AC
	push {r4, lr}
	ldr r1, =titleScreenFuncs
	ldr r4, =0x03000582
	ldrh r0, [r4]
	lsl r0, r0, #2
	add r0, r0, r1
	ldr r0, [r0]
	bl call_r0
	lsl r0, r0, #0x10
	asr r3, r0, #0x10
	cmp r3, #1
	beq _080266E6
	cmp r3, #1
	bgt _080266DC
	mov r0, #1
	neg r0, r0
	cmp r3, r0
	beq _08026754
	b _0802675A
	.align 2, 0
.pool
_080266DC:
	cmp r3, #2
	beq _080266EE
	cmp r3, #3
	beq _08026738
	b _0802675A
_080266E6:
	ldrh r0, [r4]
	add r0, #1
	strh r0, [r4]
	b _0802675A
_080266EE:
	ldr r2, =0x03002080
	ldrb r0, [r2, #9]
	mov r1, #4
	orr r0, r1
	strb r0, [r2, #9]
	ldr r4, =0x03003D40
	mov r0, #0
	strb r0, [r4, #0x10]
	ldr r0, =0x03003C00
	ldrb r1, [r0]
	lsl r0, r1, #0x19
	lsr r2, r0, #0x1f
	cmp r2, #0
	beq _0802671C
	mov r0, #1
	strb r0, [r4]
	b _0802672A
	.align 2, 0
.pool
_0802671C:
	lsl r0, r1, #0x1a
	lsr r0, r0, #0x1a
	cmp r0, #0x3f
	bne _08026728
	strb r3, [r4]
	b _0802672A
_08026728:
	strb r2, [r4]
_0802672A:
	ldr r0, =(cb_loadMenu+1)
	bl setCurrentTaskFunc
	b _0802675A
	.align 2, 0
.pool
_08026738:
	ldr r0, =0x03000588
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	mov r0, #1
	strh r0, [r4]
	b _0802675A
	.align 2, 0
.pool
_08026754:
	ldr r0, =(cb_konamiLogoLoad+1)
	bl setCurrentTaskFunc
_0802675A:
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool



thumb_func_global titleState0
titleState0: @ 0x08026764
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	bl sub_0800F320
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0802677A
	mov r0, #0
	b _08026792
_0802677A:
	ldr r1, =0x03000580
	mov r0, #0
	strh r0, [r1]
	ldr r0, =0x03000588
	ldr r1, [r0]
	add r1, #0x23
	ldrb r2, [r1]
	mov r0, #0x11
	neg r0, r0
	and r0, r2
	strb r0, [r1]
	mov r0, #1
_08026792:
	pop {r1}
	bx r1
	.align 2, 0
.pool



thumb_func_global titleState1
titleState1: @ 0x080267A0
	push {lr}
	ldr r0, =0x03000580
	ldrh r1, [r0]
	add r1, #1
	strh r1, [r0]
	lsl r1, r1, #0x10
	mov r0, #0x96
	lsl r0, r0, #0x12
	cmp r1, r0
	bls _080267C8
	ldr r1, =0x03000584
	mov r2, #1
	neg r2, r2
	add r0, r2, #0
	b _080267E6
	.align 2, 0
.pool
_080267C8:
	ldr r0, =0x03002080
	ldrh r1, [r0, #0xc]
	mov r0, #9
	and r0, r1
	cmp r0, #0
	bne _080267DC
	mov r0, #0
	b _080267EA
	.align 2, 0
.pool
_080267DC:
	mov r0, #0xca
	bl playSong
	ldr r1, =0x03000584
	mov r0, #2
_080267E6:
	strh r0, [r1]
	mov r0, #1
_080267EA:
	pop {r1}
	bx r1
	.align 2, 0
.pool



thumb_func_global titleState2
titleState2: @ 0x080267F4
	push {lr}
	mov r0, #0x3f
	mov r1, #1
	mov r2, #0x14
	mov r3, #0x10
	bl fadeScreen
	lsl r0, r0, #0x18
	cmp r0, #0
	bne _0802680C
	mov r0, #0
	b _0802680E
_0802680C:
	mov r0, #1
_0802680E:
	pop {r1}
	bx r1
	.align 2, 0



thumb_func_global titleState3
titleState3: @ 0x08026814
	push {lr}
	bl FreeAllPalFadeTasks
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl resetAffineTransformations
	bl clearSprites
	mov r0, #0x3f
	mov r1, #1
	bl setBldCnt
	ldr r0, =0x03000584
	mov r1, #0
	ldrsh r0, [r0, r1]
	pop {r1}
	bx r1
	.align 2, 0
.pool



thumb_func_global titleState4
titleState4: @ 0x08026848
	push {r4, lr}
	ldr r4, =0x03000580
	ldrh r3, [r4]
	add r0, r3, #1
	cmp r0, #0x1f
	ble _08026868
	ldr r1, =0x030022F8
	mov r0, #0
	strh r0, [r1]
	strh r0, [r4]
	mov r0, #1
	b _0802687C
	.align 2, 0
.pool
_08026868:
	ldr r2, =0x030022F8
	ldr r1, =dword_8152024
	lsl r0, r3, #1
	add r0, r0, r1
	ldrh r0, [r0]
	strh r0, [r2]
	ldrh r0, [r4]
	add r0, #1
	strh r0, [r4]
	mov r0, #0
_0802687C:
	pop {r4}
	pop {r1}
	bx r1
	.align 2, 0
.pool



thumb_func_global titleState5
titleState5: @ 0x0802688C
	push {lr}
	ldr r2, =0x03000580
	ldrh r1, [r2]
	add r1, #1
	strh r1, [r2]
	lsl r1, r1, #0x10
	lsr r1, r1, #0x10
	cmp r1, #0x1e
	bls _080268A8
	mov r0, #0
	strh r0, [r2]
	bl stopSoundTracks
	mov r0, #3
_080268A8:
	pop {r1}
	bx r1
	.align 2, 0
.pool
