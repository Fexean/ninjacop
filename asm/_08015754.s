.include "asm/macros.inc"
	


	thumb_func_global sub_80161DC
sub_80161DC: @ 0x080161DC
	push {lr}
	sub sp, #4
	mov r1, sp
	mov r0, #0
	strh r0, [r1]
	ldr r1, =0x03000D60
	ldr r2, =0x01000026
	mov r0, sp
	bl Bios_memcpy
	add sp, #4
	pop {r0}
	bx r0
	.align 2, 0
.pool


	thumb_func_global sub_8016200
sub_8016200: @ 0x08016200
	push {r4, r5, r6, lr}
	mov r6, r8
	push {r6}
	sub sp, #4
	add r4, r0, #0
	mov r8, r1
	add r5, r2, #0
	add r6, r3, #0
	ldr r2, [sp, #0x18]
	lsl r4, r4, #0x18
	lsr r4, r4, #0x18
	lsl r5, r5, #0x18
	lsr r5, r5, #0x18
	lsl r6, r6, #0x18
	lsr r6, r6, #0x18
	lsl r2, r2, #0x18
	lsr r2, r2, #0x18
	add r0, r4, #0
	str r2, [sp]
	bl sub_80162A8
	ldr r3, =0x03000D60
	lsl r1, r4, #1
	add r1, r1, r4
	lsl r1, r1, #2
	add r0, r3, #4
	add r0, r1, r0
	mov r4, r8
	str r4, [r0]
	add r1, r1, r3
	mov r0, #0
	strb r5, [r1, #0xd]
	strb r6, [r1, #0xe]
	ldr r2, [sp]
	strb r2, [r1, #8]
	strb r0, [r1, #9]
	strb r0, [r1, #0xa]
	strb r0, [r1, #0xb]
	strb r0, [r1, #0xc]
	add sp, #4
	pop {r3}
	mov r8, r3
	pop {r4, r5, r6}
	pop {r0}
	bx r0
	.align 2, 0
.pool


	thumb_func_global sub_8016260
sub_8016260: @ 0x08016260
	push {r4, lr}
	lsl r0, r0, #0x18
	lsr r2, r0, #0x18
	lsl r1, r1, #0x18
	lsr r3, r1, #0x18
	ldr r4, =0x03000D60
	ldrb r1, [r4]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #2
	add r1, r0, r4
	ldrb r0, [r1, #0xd]
	cmp r2, r0
	blo _08016282
	sub r0, #1
	lsl r0, r0, #0x18
	lsr r2, r0, #0x18
_08016282:
	ldrb r0, [r1, #0xe]
	cmp r3, r0
	blo _0801628E
	sub r0, #1
	lsl r0, r0, #0x18
	lsr r3, r0, #0x18
_0801628E:
	strb r2, [r1, #9]
	ldrb r1, [r4]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #2
	add r0, r0, r4
	strb r3, [r0, #0xa]
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool


	thumb_func_global sub_80162A8
sub_80162A8: @ 0x080162A8
	lsl r0, r0, #0x18
	lsr r1, r0, #0x18
	cmp r1, #5
	bls _080162B2
	mov r1, #5
_080162B2:
	ldr r0, =0x03000D60
	strb r1, [r0]
	bx lr
	.align 2, 0
.pool


	thumb_func_global sub_80162BC
sub_80162BC: @ 0x080162BC
	ldr r2, =0x03000D60
	ldrb r0, [r2]
	lsl r1, r0, #1
	add r1, r1, r0
	lsl r1, r1, #2
	add r1, r1, r2
	ldrb r2, [r1, #0xd]
	ldrb r0, [r1, #0xa]
	mul r0, r2, r0
	ldrb r1, [r1, #9]
	add r0, r0, r1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	bx lr
	.align 2, 0
.pool


	thumb_func_global sub_80162DC
sub_80162DC: @ 0x080162DC
	push {lr}
	bl sub_80162BC
	ldr r2, =0x03000D60
	ldrb r3, [r2]
	lsl r1, r3, #1
	add r1, r1, r3
	lsl r1, r1, #2
	add r2, #4
	add r1, r1, r2
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r1, [r1]
	add r1, r1, r0
	ldrb r0, [r1]
	pop {r1}
	bx r1
	.align 2, 0
.pool


	thumb_func_global sub_8016304
sub_8016304: @ 0x08016304
	ldr r2, =0x03000D60
	ldrb r0, [r2]
	lsl r1, r0, #1
	add r1, r1, r0
	lsl r1, r1, #2
	add r1, r1, r2
	ldrb r2, [r1, #0xd]
	ldrb r0, [r1, #0xc]
	mul r0, r2, r0
	ldrb r1, [r1, #0xb]
	add r0, r0, r1
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	bx lr
	.align 2, 0
.pool


	thumb_func_global sub_8016324
sub_8016324: @ 0x08016324
	push {lr}
	bl sub_8016304
	ldr r2, =0x03000D60
	ldrb r3, [r2]
	lsl r1, r3, #1
	add r1, r1, r3
	lsl r1, r1, #2
	add r2, #4
	add r1, r1, r2
	lsl r0, r0, #0x10
	lsr r0, r0, #0x10
	ldr r1, [r1]
	add r1, r1, r0
	ldrb r0, [r1]
	pop {r1}
	bx r1
	.align 2, 0
.pool


	thumb_func_global sub_801634C
sub_801634C: @ 0x0801634C
	push {r4, r5, lr}
	ldr r1, =0x03000D60
	ldrb r2, [r1]
	lsl r0, r2, #1
	add r0, r0, r2
	lsl r0, r0, #2
	add r1, #4
	add r4, r0, r1
	ldrb r5, [r4, #5]
	ldrb r0, [r4, #4]
	cmp r0, #0
	bne _08016392
_08016364:
	ldrb r0, [r4, #5]
	cmp r0, #0
	bne _0801636C
	ldrb r0, [r4, #9]
_0801636C:
	sub r0, #1
	strb r0, [r4, #5]
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, r5
	beq _0801638E
	bl sub_80162DC
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0xff
	beq _08016364
	b _080163A8
	.align 2, 0
.pool
_0801638C:
	strb r5, [r4, #5]
_0801638E:
	mov r0, #0
	b _080163AC
_08016392:
	ldrb r0, [r4, #5]
	cmp r0, #0
	beq _0801638C
	sub r0, #1
	strb r0, [r4, #5]
	bl sub_80162DC
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0xff
	beq _08016392
_080163A8:
	strb r5, [r4, #7]
	mov r0, #1
_080163AC:
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
	
	
	thumb_func_global sub_80163B4
sub_80163B4: @ 0x080163B4
	push {r4, r5, lr}
	ldr r1, =0x03000D60
	ldrb r2, [r1]
	lsl r0, r2, #1
	add r0, r0, r2
	lsl r0, r0, #2
	add r1, #4
	add r4, r0, r1
	ldrb r5, [r4, #5]
	ldrb r0, [r4, #4]
	cmp r0, #0
	bne _080163FE
_080163CC:
	ldrb r0, [r4, #5]
	add r1, r0, #1
	mov r0, #0
	ldrb r2, [r4, #9]
	cmp r1, r2
	bge _080163DA
	add r0, r1, #0
_080163DA:
	strb r0, [r4, #5]
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, r5
	beq _080163FA
	bl sub_80162DC
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0xff
	beq _080163CC
	b _08016416
	.align 2, 0
.pool
_080163F8:
	strb r5, [r4, #5]
_080163FA:
	mov r0, #0
	b _0801641A
_080163FE:
	ldrb r0, [r4, #5]
	add r0, #1
	ldrb r1, [r4, #9]
	cmp r0, r1
	bge _080163F8
	strb r0, [r4, #5]
	bl sub_80162DC
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0xff
	beq _080163FE
_08016416:
	strb r5, [r4, #7]
	mov r0, #1
_0801641A:
	pop {r4, r5}
	pop {r1}
	bx r1
	
	
	thumb_func_global sub_8016420
sub_8016420: @ 0x08016420
	push {r4, r5, lr}
	ldr r1, =0x03000D60
	ldrb r2, [r1]
	lsl r0, r2, #1
	add r0, r0, r2
	lsl r0, r0, #2
	add r1, #4
	add r4, r0, r1
	ldrb r5, [r4, #6]
	ldrb r0, [r4, #4]
	cmp r0, #0
	bne _08016466
_08016438:
	ldrb r0, [r4, #6]
	cmp r0, #0
	bne _08016440
	ldrb r0, [r4, #0xa]
_08016440:
	sub r0, #1
	strb r0, [r4, #6]
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, r5
	beq _08016462
	bl sub_80162DC
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0xff
	beq _08016438
	b _0801647C
	.align 2, 0
.pool
_08016460:
	strb r5, [r4, #6]
_08016462:
	mov r0, #0
	b _08016480
_08016466:
	ldrb r0, [r4, #6]
	cmp r0, #0
	beq _08016460
	sub r0, #1
	strb r0, [r4, #6]
	bl sub_80162DC
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0xff
	beq _08016466
_0801647C:
	strb r5, [r4, #8]
	mov r0, #1
_08016480:
	pop {r4, r5}
	pop {r1}
	bx r1
	.align 2, 0
	
	
	thumb_func_global sub_8016488
sub_8016488: @ 0x08016488
	push {r4, r5, lr}
	ldr r1, =0x03000D60
	ldrb r2, [r1]
	lsl r0, r2, #1
	add r0, r0, r2
	lsl r0, r0, #2
	add r1, #4
	add r4, r0, r1
	ldrb r5, [r4, #6]
	ldrb r0, [r4, #4]
	cmp r0, #0
	bne _080164D2
_080164A0:
	ldrb r0, [r4, #6]
	add r1, r0, #1
	mov r0, #0
	ldrb r2, [r4, #0xa]
	cmp r1, r2
	bge _080164AE
	add r0, r1, #0
_080164AE:
	strb r0, [r4, #6]
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, r5
	beq _080164CE
	bl sub_80162DC
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0xff
	beq _080164A0
	b _080164EA
	.align 2, 0
.pool
_080164CC:
	strb r5, [r4, #6]
_080164CE:
	mov r0, #0
	b _080164EE
_080164D2:
	ldrb r0, [r4, #6]
	add r0, #1
	ldrb r1, [r4, #0xa]
	cmp r0, r1
	bge _080164CC
	strb r0, [r4, #6]
	bl sub_80162DC
	lsl r0, r0, #0x18
	lsr r0, r0, #0x18
	cmp r0, #0xff
	beq _080164D2
_080164EA:
	strb r5, [r4, #8]
	mov r0, #1
_080164EE:
	pop {r4, r5}
	pop {r1}
	bx r1
	
	
	thumb_func_global sub_80164F4
sub_80164F4: @ 0x080164F4
	ldr r2, =0x03000D60
	ldrb r1, [r2]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #2
	add r0, r0, r2
	ldrb r0, [r0, #9]
	bx lr
	.align 2, 0
.pool
	thumb_func_global sub_8016508
sub_8016508: @ 0x08016508
	ldr r2, =0x03000D60
	ldrb r1, [r2]
	lsl r0, r1, #1
	add r0, r0, r1
	lsl r0, r0, #2
	add r0, r0, r2
	ldrb r0, [r0, #0xa]
	bx lr
	.align 2, 0
	
	
.pool
	thumb_func_global sub_801651C
sub_801651C: @ 0x0801651C
	push {r4, r5, r6, r7, lr}
	add r7, r1, #0
	mov ip, r2
	lsl r0, r0, #0x18
	lsr r6, r0, #0x18
	ldr r1, =0x03000D60
	ldrb r2, [r1]
	lsl r0, r2, #1
	add r0, r0, r2
	lsl r0, r0, #2
	add r1, #4
	add r4, r0, r1
	mov r5, #0
	b _08016570
	.align 2, 0
.pool
_0801653C:
	mov r3, #0
	ldrb r0, [r4, #9]
	cmp r3, r0
	bhs _0801656A
_08016544:
	ldrb r2, [r4, #9]
	add r0, r5, #0
	mul r0, r2, r0
	ldr r1, [r4]
	add r0, r0, r3
	add r1, r1, r0
	ldrb r0, [r1]
	cmp r0, r6
	bne _08016560
	strb r3, [r7]
	mov r0, ip
	strb r5, [r0]
	mov r0, #1
	b _08016578
_08016560:
	add r0, r3, #1
	lsl r0, r0, #0x18
	lsr r3, r0, #0x18
	cmp r3, r2
	blo _08016544
_0801656A:
	add r0, r5, #1
	lsl r0, r0, #0x18
	lsr r5, r0, #0x18
_08016570:
	ldrb r0, [r4, #0xa]
	cmp r5, r0
	blo _0801653C
	mov r0, #0
_08016578:
	pop {r4, r5, r6, r7}
	pop {r1}
	bx r1
	.align 2, 0
	
	
	thumb_func_global sub_8016580
sub_8016580: @ 0x08016580
	push {r4, lr}
	ldr r1, =0x03003CD4
	mov r2, #0x82
	lsl r2, r2, #5
	add r0, r2, #0
	strh r0, [r1]
	bl resetAffineTransformations
	ldr r4, =0x03003D40
	ldrb r0, [r4, #1]
	ldrb r1, [r4, #2]
	ldrb r2, [r4, #3]
	bl loadMap
	ldrb r0, [r4, #1]
	ldrb r1, [r4, #2]
	ldrb r2, [r4, #3]
	ldrb r3, [r4, #4]
	bl setCameraStruct
	bl loadHUD
	ldr r1, =0x03004720
	ldrh r0, [r1]
	ldrh r1, [r1, #2]
	bl handleParallaxScroll
	bl sub_080124DC
	bl sub_08032128
	bl sub_08032D1C
	bl sub_0803310C
	bl sub_08033164
	pop {r4}
	pop {r0}
	bx r0
	.align 2, 0
.pool


	thumb_func_global sub_80165DC
sub_80165DC: @ 0x080165DC
	push {lr}
	ldr r0, =(player_init_maybe+1)
	bl setCurrentTaskFunc
	pop {r0}
	bx r0
	.align 2, 0
.pool


	thumb_func_global sub_80165EC
sub_80165EC: @ 0x080165EC
	push {lr}
	bl clearSprites
	bl clearPalBufs
	pop {r0}
	bx r0
	.align 2, 0

