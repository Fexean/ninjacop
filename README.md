# Ninja Cop
This is a disassembly of Ninja Cop / Ninja Five-O.

It builds the following roms:

* ninjacop.gba `sha1: 0cfe2bcbf256299060bc4eac51c4dad8ced9939d`  
* ninjafiveo.gba `sha1: 3bc5789035c7400283b121e9fb3801aafae55364`  


## Dependencies
To compile the tools you need libpng, a version of g++ that supports C++11 and gcc that supports c11.  
To compile the roms you need devkitarm.  

## Compiling
1. compile the tools with `make tools`  
2. compile roms with `make` / `make cop` and `make fiveo`  

## Status
- correctly builds both rom versions.  
- all code has been disassembled.  
- (as far as I know) all ROM pointers have been replaced with labels.  
- few bits of data and graphics have been extracted.  

### Todo
- document code / data  
- extract more graphics and other data  
- replace ram addresses with symbols  
- convert strings to a readable & editable format  

