ENTRY(_start)

MEMORY {
	rom	: ORIGIN = 0x08000000, LENGTH = 32M
	iwram	: ORIGIN = 0x03000000, LENGTH = 32K
	ewram	: ORIGIN = 0x02000000, LENGTH = 256K
}

__text_start = ORIGIN(rom);

SECTIONS {
    
	. = ORIGIN(iwram);
	.data :
	{
		INCLUDE "../../sym_iwram.txt"
	} > iwram
	
	
	. = ORIGIN(ewram);
	ewram_data :
	{
		INCLUDE "../../sym_ewram.txt"
	} > ewram
	
	. = ORIGIN(rom);
    .text :
    ALIGN(4)
    {
     	asm/crt0.o(.text);
		data/entity_scripts.o(.text);
		asm/main.o(.text);
		asm/unused_800D6C8.o(.text);
		asm/callback.o(.text);
		asm/gfx_util.o(.text);
		asm/sprite.o(.text);
		asm/clear_ram.o(.text);
		asm/_800D7E8.o(.text);
		asm/palette.o(.text);
		asm/save.o(.text);
		asm/map.o(.text);
		asm/specialMoveBg.o(.text);
		asm/camera.o(.text);
		asm/level_callbacks.o(.text);
		asm/_08015754.o(.text);
		asm/task.o(.text);
		asm/object.o(.text);
		asm/player.o(.text);
		asm/entity.o(.text);
		asm/_801F254.o(.text);
		asm/objc.o(.text);
		asm/titlescreen.o(.text);
		asm/intro.o(.text);
		asm/logos.o(.text);
		asm/language_select.o(.text);
		asm/introscroll.o(.text);
		asm/savingScreen.o(.text);
		asm/timeTrialComplete.o(.text);
		asm/gameOver.o(.text);
		asm/levelSelect.o(.text);
		asm/timeTrial.o(.text);
		asm/string.o(.text);
		asm/menu.o(.text);
		asm/continue.o(.text);
		asm/ending.o(.text);
		asm/level.o(.text);
		asm/bios.o(.text);
		asm/eeprom.o(.text);
		asm/audio.o(.text);
		asm/libc.o(.text);
		
		data/data.o(.text);
		data/mapdata.o(.text);
		data/data_80DD840.o(.text);
		data/level_intro_data.o(.text);
		data/menu_data.o(.text);
		data/font_gfx.o(.text);
		data/data_820938c.o(.text);
		data/door_types.o(.text);
		data/data_8243184.o(.text);
		data/songs.o(.text);
		data/data_83B8264.o(.text);
		
		asm/sprite_renderer.o(.text);
    } > rom

    
	
	.fill :
  {
    FILL(0x0);
    . = ORIGIN(rom) + 4M - 1;
    BYTE(0x0)
  } > rom
	
	
	/* Discard everything not specifically mentioned above. */
    /DISCARD/ :
    {
        *(*);
    }
}