# Sprites
Every frame the game iterates through a doubly linked list of sprite headers located at 0x3002830.  
For each header where the inUse field is 1, a function is called to draw the sprite pointed to by the first field of the header.  
If the type field is set to 0, the first field is considered a pointer to a sprite, and if type is set to 1 the first field is considered a pointer to a SimpleSprite.  
SimpleSprites are drawn by passing them to the function bufferSimpleSprite while sprites are drawn by passing them to the function drawSpriteToBuffer.  

    struct SpriteHeader{
    	union{
    		struct Sprite* sprite;
    		struct SimpleSprite* simple;
    	};
    	struct SpriteHeader* prev;
    	struct SpriteHeader* next;
    	u16 priority;
    	u8 type;
    	bool8 inUse;
    };



## SimpleSprite
This type of sprite is only used to display text and the player's grappling hook.  
If its visible field is non-zero, an OAM will be created from its first three fields.  
The list of these is at 0x30037D0.

    struct SimpleSprite{
    	u16 oam_attr0;
    	u16 oam_attr1;
    	u16 oam_attr2;
    	u16 padding;
    	struct SpriteHeader* header;
    	u8 visible;
    	u8 padding2[3];
    };


## Sprite
Most sprites in the game use this structure.  
These types of sprites can have multiple frames of animation and each frame can consist of multiple OAMs.  
Sprites are stored in an array at 0x3003050, there's space for 48 sprites.  

    struct Sprite{
    	struct SpriteHeader* header;
    	void* animation;	//this data controls how
    	void* tileset;	//pointer to uncompressed tileset, used by all the animation frames. The engine automatically loads the necessary tiles to vram
    	u16 vramTileNum;	//start of the sprite's allocated tile area in vram
    	u16 animFrame;
    	u16 x;
    	u16 y;
    	u16 unk0;
    	u32 unk1;
    	u16 tilecount;	//amount of tiles allocated in vram
    	u16 anim_timer;
    	u8 pal;
    	u8 bitfield1;
    	u8 bitfield1;
    	u8 obj_mode;	//(0=Normal, 1=Semi-Transparent, 2=OBJ Window)
    	u16 unk2;
    };