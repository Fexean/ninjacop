# Callback (asm/callback.s)

at 0x3002110 there are 12 callback structures in a doubly linked list. Their structure is this:  

    struct callback{
    	void (*func)(void);
    	struct callback* prev;
    	struct callback* next;
    	u16 priority;
    	u16 inUse;
    };

Every frame in the vblank interrupt handler the function pointers for each callback are called.  
The game uses this system to run code that must be executed during vblank.  
I've labeled callback functions with "cb_" but currently most functions starting with "cb_" are actually task functions.  
