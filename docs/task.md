# Task (asm/task.s)
"Tasks" are a way to make code run every frame. They are the main way the game executes code outside the main loop.  
Up to 50 tasks can be active at once. All active tasks are executed in runTasks which is called in the main loop every frame.  
Tasks are stored in a doubly linked list at 0x201AF00:

    struct task{
    	struct task* prev;
    	struct task* next;
    	void* _4kBlock;	//contains a pointer to a 4 kilobyte chunk of memory
    	u8 inUse: 1;	//set when a task is created to mark it as used
    	u8 pause: 1;	//must be clear for runTasks to execute the task
    	u8 free4k: 1;	//if set and memoryBlock is non-zero, the 4k block of memory pointed to by memoryBlock is freed
    	u8 kill: 1;		//if set, the task is deleted by manageTasks
    	u8 changePriority: 1;	//if set, manageTasks will replace priority with newPriority
    	u8 linked_task;	//can be an index of another task
    	u8 priority;
    	u8 newPriority;    	
    	u8 unk[2];
    	u16 delay;	//how many frames to wait before the task is called again, defaults to 0
    	u32 data[4];	//maybe general purpose?
    	void (*func)(void);
    	u32 regs[10];	//r4-r13 are stored here
    };